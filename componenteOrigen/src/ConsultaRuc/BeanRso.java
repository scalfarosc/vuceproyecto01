/**
 * BeanRso.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ConsultaRuc;

public class BeanRso  implements java.io.Serializable {
    private java.lang.String cod_cargo;

    private java.lang.String cod_depar;

    private java.lang.String desc_docide;

    private short num_ord_suce;

    private java.lang.String rso_cargoo;

    private java.lang.String rso_docide;

    private java.lang.String rso_fecact;

    private java.lang.String rso_fecnac;

    private java.lang.String rso_nombre;

    private java.lang.String rso_nrodoc;

    private java.lang.String rso_numruc;

    private java.lang.String rso_userna;

    private java.lang.String rso_vdesde;

    public BeanRso() {
    }

    public BeanRso(
           java.lang.String cod_cargo,
           java.lang.String cod_depar,
           java.lang.String desc_docide,
           short num_ord_suce,
           java.lang.String rso_cargoo,
           java.lang.String rso_docide,
           java.lang.String rso_fecact,
           java.lang.String rso_fecnac,
           java.lang.String rso_nombre,
           java.lang.String rso_nrodoc,
           java.lang.String rso_numruc,
           java.lang.String rso_userna,
           java.lang.String rso_vdesde) {
           this.cod_cargo = cod_cargo;
           this.cod_depar = cod_depar;
           this.desc_docide = desc_docide;
           this.num_ord_suce = num_ord_suce;
           this.rso_cargoo = rso_cargoo;
           this.rso_docide = rso_docide;
           this.rso_fecact = rso_fecact;
           this.rso_fecnac = rso_fecnac;
           this.rso_nombre = rso_nombre;
           this.rso_nrodoc = rso_nrodoc;
           this.rso_numruc = rso_numruc;
           this.rso_userna = rso_userna;
           this.rso_vdesde = rso_vdesde;
    }


    /**
     * Gets the cod_cargo value for this BeanRso.
     * 
     * @return cod_cargo
     */
    public java.lang.String getCod_cargo() {
        return cod_cargo;
    }


    /**
     * Sets the cod_cargo value for this BeanRso.
     * 
     * @param cod_cargo
     */
    public void setCod_cargo(java.lang.String cod_cargo) {
        this.cod_cargo = cod_cargo;
    }


    /**
     * Gets the cod_depar value for this BeanRso.
     * 
     * @return cod_depar
     */
    public java.lang.String getCod_depar() {
        return cod_depar;
    }


    /**
     * Sets the cod_depar value for this BeanRso.
     * 
     * @param cod_depar
     */
    public void setCod_depar(java.lang.String cod_depar) {
        this.cod_depar = cod_depar;
    }


    /**
     * Gets the desc_docide value for this BeanRso.
     * 
     * @return desc_docide
     */
    public java.lang.String getDesc_docide() {
        return desc_docide;
    }


    /**
     * Sets the desc_docide value for this BeanRso.
     * 
     * @param desc_docide
     */
    public void setDesc_docide(java.lang.String desc_docide) {
        this.desc_docide = desc_docide;
    }


    /**
     * Gets the num_ord_suce value for this BeanRso.
     * 
     * @return num_ord_suce
     */
    public short getNum_ord_suce() {
        return num_ord_suce;
    }


    /**
     * Sets the num_ord_suce value for this BeanRso.
     * 
     * @param num_ord_suce
     */
    public void setNum_ord_suce(short num_ord_suce) {
        this.num_ord_suce = num_ord_suce;
    }


    /**
     * Gets the rso_cargoo value for this BeanRso.
     * 
     * @return rso_cargoo
     */
    public java.lang.String getRso_cargoo() {
        return rso_cargoo;
    }


    /**
     * Sets the rso_cargoo value for this BeanRso.
     * 
     * @param rso_cargoo
     */
    public void setRso_cargoo(java.lang.String rso_cargoo) {
        this.rso_cargoo = rso_cargoo;
    }


    /**
     * Gets the rso_docide value for this BeanRso.
     * 
     * @return rso_docide
     */
    public java.lang.String getRso_docide() {
        return rso_docide;
    }


    /**
     * Sets the rso_docide value for this BeanRso.
     * 
     * @param rso_docide
     */
    public void setRso_docide(java.lang.String rso_docide) {
        this.rso_docide = rso_docide;
    }


    /**
     * Gets the rso_fecact value for this BeanRso.
     * 
     * @return rso_fecact
     */
    public java.lang.String getRso_fecact() {
        return rso_fecact;
    }


    /**
     * Sets the rso_fecact value for this BeanRso.
     * 
     * @param rso_fecact
     */
    public void setRso_fecact(java.lang.String rso_fecact) {
        this.rso_fecact = rso_fecact;
    }


    /**
     * Gets the rso_fecnac value for this BeanRso.
     * 
     * @return rso_fecnac
     */
    public java.lang.String getRso_fecnac() {
        return rso_fecnac;
    }


    /**
     * Sets the rso_fecnac value for this BeanRso.
     * 
     * @param rso_fecnac
     */
    public void setRso_fecnac(java.lang.String rso_fecnac) {
        this.rso_fecnac = rso_fecnac;
    }


    /**
     * Gets the rso_nombre value for this BeanRso.
     * 
     * @return rso_nombre
     */
    public java.lang.String getRso_nombre() {
        return rso_nombre;
    }


    /**
     * Sets the rso_nombre value for this BeanRso.
     * 
     * @param rso_nombre
     */
    public void setRso_nombre(java.lang.String rso_nombre) {
        this.rso_nombre = rso_nombre;
    }


    /**
     * Gets the rso_nrodoc value for this BeanRso.
     * 
     * @return rso_nrodoc
     */
    public java.lang.String getRso_nrodoc() {
        return rso_nrodoc;
    }


    /**
     * Sets the rso_nrodoc value for this BeanRso.
     * 
     * @param rso_nrodoc
     */
    public void setRso_nrodoc(java.lang.String rso_nrodoc) {
        this.rso_nrodoc = rso_nrodoc;
    }


    /**
     * Gets the rso_numruc value for this BeanRso.
     * 
     * @return rso_numruc
     */
    public java.lang.String getRso_numruc() {
        return rso_numruc;
    }


    /**
     * Sets the rso_numruc value for this BeanRso.
     * 
     * @param rso_numruc
     */
    public void setRso_numruc(java.lang.String rso_numruc) {
        this.rso_numruc = rso_numruc;
    }


    /**
     * Gets the rso_userna value for this BeanRso.
     * 
     * @return rso_userna
     */
    public java.lang.String getRso_userna() {
        return rso_userna;
    }


    /**
     * Sets the rso_userna value for this BeanRso.
     * 
     * @param rso_userna
     */
    public void setRso_userna(java.lang.String rso_userna) {
        this.rso_userna = rso_userna;
    }


    /**
     * Gets the rso_vdesde value for this BeanRso.
     * 
     * @return rso_vdesde
     */
    public java.lang.String getRso_vdesde() {
        return rso_vdesde;
    }


    /**
     * Sets the rso_vdesde value for this BeanRso.
     * 
     * @param rso_vdesde
     */
    public void setRso_vdesde(java.lang.String rso_vdesde) {
        this.rso_vdesde = rso_vdesde;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BeanRso)) return false;
        BeanRso other = (BeanRso) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cod_cargo==null && other.getCod_cargo()==null) || 
             (this.cod_cargo!=null &&
              this.cod_cargo.equals(other.getCod_cargo()))) &&
            ((this.cod_depar==null && other.getCod_depar()==null) || 
             (this.cod_depar!=null &&
              this.cod_depar.equals(other.getCod_depar()))) &&
            ((this.desc_docide==null && other.getDesc_docide()==null) || 
             (this.desc_docide!=null &&
              this.desc_docide.equals(other.getDesc_docide()))) &&
            this.num_ord_suce == other.getNum_ord_suce() &&
            ((this.rso_cargoo==null && other.getRso_cargoo()==null) || 
             (this.rso_cargoo!=null &&
              this.rso_cargoo.equals(other.getRso_cargoo()))) &&
            ((this.rso_docide==null && other.getRso_docide()==null) || 
             (this.rso_docide!=null &&
              this.rso_docide.equals(other.getRso_docide()))) &&
            ((this.rso_fecact==null && other.getRso_fecact()==null) || 
             (this.rso_fecact!=null &&
              this.rso_fecact.equals(other.getRso_fecact()))) &&
            ((this.rso_fecnac==null && other.getRso_fecnac()==null) || 
             (this.rso_fecnac!=null &&
              this.rso_fecnac.equals(other.getRso_fecnac()))) &&
            ((this.rso_nombre==null && other.getRso_nombre()==null) || 
             (this.rso_nombre!=null &&
              this.rso_nombre.equals(other.getRso_nombre()))) &&
            ((this.rso_nrodoc==null && other.getRso_nrodoc()==null) || 
             (this.rso_nrodoc!=null &&
              this.rso_nrodoc.equals(other.getRso_nrodoc()))) &&
            ((this.rso_numruc==null && other.getRso_numruc()==null) || 
             (this.rso_numruc!=null &&
              this.rso_numruc.equals(other.getRso_numruc()))) &&
            ((this.rso_userna==null && other.getRso_userna()==null) || 
             (this.rso_userna!=null &&
              this.rso_userna.equals(other.getRso_userna()))) &&
            ((this.rso_vdesde==null && other.getRso_vdesde()==null) || 
             (this.rso_vdesde!=null &&
              this.rso_vdesde.equals(other.getRso_vdesde())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCod_cargo() != null) {
            _hashCode += getCod_cargo().hashCode();
        }
        if (getCod_depar() != null) {
            _hashCode += getCod_depar().hashCode();
        }
        if (getDesc_docide() != null) {
            _hashCode += getDesc_docide().hashCode();
        }
        _hashCode += getNum_ord_suce();
        if (getRso_cargoo() != null) {
            _hashCode += getRso_cargoo().hashCode();
        }
        if (getRso_docide() != null) {
            _hashCode += getRso_docide().hashCode();
        }
        if (getRso_fecact() != null) {
            _hashCode += getRso_fecact().hashCode();
        }
        if (getRso_fecnac() != null) {
            _hashCode += getRso_fecnac().hashCode();
        }
        if (getRso_nombre() != null) {
            _hashCode += getRso_nombre().hashCode();
        }
        if (getRso_nrodoc() != null) {
            _hashCode += getRso_nrodoc().hashCode();
        }
        if (getRso_numruc() != null) {
            _hashCode += getRso_numruc().hashCode();
        }
        if (getRso_userna() != null) {
            _hashCode += getRso_userna().hashCode();
        }
        if (getRso_vdesde() != null) {
            _hashCode += getRso_vdesde().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

}
