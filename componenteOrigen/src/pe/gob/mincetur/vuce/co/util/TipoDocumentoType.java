package pe.gob.mincetur.vuce.co.util;

public enum TipoDocumentoType {
    RUC (1, "3", "RUC"),
    DNI (2, "1", "DNI"),
    CARNET_EXTRANJERIA (3, "3", "CARNET DE EXTRANJERIA"),
    OTRO (4, "4", "OTRO");
    
    private Integer id;
    private String codigoSunat;
    private String descripcion;
    
    TipoDocumentoType(Integer id, String codigoSunat, String descripcion) {
        this.id = id;
        this.codigoSunat=codigoSunat;
        this.descripcion=descripcion;
    }
    
    public Integer getId() {
        return id;
    }

    public String getCodigoSunat() {
        return codigoSunat;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    public static TipoDocumentoType getTipoDocumentoTypeByCodigoSunat(String codigo) {
        for (TipoDocumentoType type : values()) {
            if (type.codigoSunat.equals(codigo)) {
                return type;
            }
        }
        return null;
    }
    
}
