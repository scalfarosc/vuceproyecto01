package pe.gob.mincetur.vuce.co.util;

import pe.gob.mincetur.vuce.co.util.TipoDocumentoTransaccionType;

public enum TipoDocumentoTransaccionType {	
    NTX ("NTX", "Numero de Transmision");
    
    private String tipoDocumento;
    private String descripcion;
    
    TipoDocumentoTransaccionType(String tipoDocumento, String descripcion) {
        this.tipoDocumento=tipoDocumento;
        this.descripcion=descripcion;
    }
    
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    public static TipoDocumentoTransaccionType getTipoDocumentoTransaccionTypeByCodigo(String codigo) {
        for (TipoDocumentoTransaccionType type : values()) {
            if (type.tipoDocumento.equals(codigo)) {
                return type;
            }
        }
        return null;
    }
    

    
}
