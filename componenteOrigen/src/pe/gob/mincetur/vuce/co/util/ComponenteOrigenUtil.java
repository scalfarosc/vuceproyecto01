package pe.gob.mincetur.vuce.co.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.keys.keyresolver.KeyResolverException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Table;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.util.stream.StreamUtil;

/**
*Objeto :	ComponenteOrigenUtil
*Descripcion :	Clase que contiene funciones utilitarios.
*Fecha de Creacion :	
*Autor :	vuce - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		09/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ
*/
public class ComponenteOrigenUtil {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);

    public static final int ITERACIONES = 1000;

    public static final byte [] SALT = new byte [] { 0x48, 0x29, 0x28, 0x37, 0x76, 0x36, 0x77, 0x15 };

    public static String getClassFieldValue(Class<?> clazz, Object obj, String codigoFormato) {
        String pagina = null;
        try {
            Field field = clazz.getDeclaredField("formato"+codigoFormato);
            field.setAccessible(true);
            pagina = field.get(obj).toString();
            field.setAccessible(false);
        } catch (IllegalArgumentException e) {
            logger.error("Error al intentar acceder a la pantalla del formato "+codigoFormato, e);
        } catch (IllegalAccessException e) {
            logger.error("Error al intentar acceder a la pantalla del formato "+codigoFormato, e);
        } catch (SecurityException e) {
            logger.error("Error al intentar acceder a la pantalla del formato "+codigoFormato, e);
        } catch (NoSuchFieldException e) {
            logger.error("Error al intentar acceder a la pantalla del formato "+codigoFormato, e);
        }
        return pagina;
    }

    public static byte [] getHash(String string) {
        byte [] bytes = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(SALT);

            bytes = string.getBytes("UTF-8");
            for (int i=1; i <= ITERACIONES; i++) {
                digest.reset();
                bytes = digest.digest(bytes);
            }
        } catch (NoSuchAlgorithmException e) {
            logger.debug("Error al obtener el Hash", e);
        } catch (UnsupportedEncodingException e) {
            logger.debug("Error al obtener el Hash", e);
        }
        return bytes;
    }

    public static ErrorMessage getErrorMessage(Exception exception) {
    	ErrorMessage cause = new ErrorMessage();
    	cause.setException(exception);
    	if (exception.getCause()!=null && exception.getCause().getCause()!=null) {
	        String causeOracleError = exception.getCause().getCause().getMessage();
	        if (causeOracleError != null && causeOracleError.length() > 0 && causeOracleError.indexOf("<error>")!=-1) {
	            causeOracleError = causeOracleError.substring((causeOracleError.indexOf("<error>")),
	                    causeOracleError.indexOf("</error>"));
	            if(causeOracleError.length() > 0) {
	                cause.setMessage(causeOracleError);
	                return cause;
	            }
	        }
	        cause.setMessage(causeOracleError);
    	}
        return cause;
    }

    public static boolean usuarioTieneRolSimilar(UsuarioCO usuario, String cadena) {
        boolean ok = false;
        HashUtil roles = usuario.getRoles();
        Enumeration en = roles.keys();
        while (en.hasMoreElements()) {
            String rol = (String)en.nextElement();
            ok |= rol.toUpperCase().startsWith(cadena.toUpperCase());
        }
        return ok;
    }

    public static void cargarInformacionPantallaPrincipal(UsuarioCO usuario, IbatisService ibatisService, HttpServletRequest request) {
        // Obtengo el usuario desde la BD para tener el usuarioID
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            filter.clear();
            filter.put("tipoDocumento", usuario.getTipoDocumento());
            filter.put("numeroDocumento", usuario.getNumeroDocumento());
            filter.put("usuarioSol", usuario.getUsuarioSOL());
            HashUtil<String, Object> usuarioDetail = ibatisService.loadElement("detalle.usuario_sol", filter);
            usuario.setIdUsuario(usuarioDetail.getInt("usuario_id"));
            usuario.setNombreCompleto(usuarioDetail.getString("nombre"));
            usuario.setTipoUsuario(usuarioDetail.getInt("usuario_tipo_id"));
            usuario.setPrincipal(usuarioDetail.getString("principal").equalsIgnoreCase("S"));
            usuario.setTipoPersona(usuarioDetail.getString("persona_tipo"));

        } else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            filter.clear();
            filter.put("tipoDocumento", usuario.getTipoDocumento());
            filter.put("numeroDocumento", usuario.getNumeroDocumento());
            HashUtil<String, Object> usuarioDetail = ibatisService.loadElement("detalle.usuario_extranet", filter);
            usuario.setIdUsuario(usuarioDetail.getInt("usuario_id"));
            usuario.setNombreCompleto(usuarioDetail.getString("nombre"));
            usuario.setTipoUsuario(usuarioDetail.getInt("usuario_tipo_id"));
            usuario.setPrincipal(usuarioDetail.getString("principal").equalsIgnoreCase("S"));
            usuario.setTipoPersona(usuarioDetail.getString("persona_tipo"));

            // Cargamos el ID de la Entidad asociada al usuario, segun la informacion que viene de SUNAT
            // OJO: Esto ya no se obtiene asi, ahora el dato de la entidad se obtiene de lo que este grabado en la BD
            /*filter.clear();
            filter.put("codigoEntidad", usuario.getNroRegistro());
            Integer idEntidad = ibatisService.loadElement("usuario.claveEntidadDesdeCodigoSUNAT", filter).getInt("entidad_id");
            usuario.setIdEntidad(idEntidad);*/
            usuario.setIdEntidad(usuarioDetail.getInt("entidad_id"));

        } else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            filter.clear();
            filter.put("tipoDocumento", usuario.getTipoDocumento());
            filter.put("numeroDocumento", usuario.getNumeroDocumento());
            HashUtil<String, Object> usuarioDetail = ibatisService.loadElement("detalle.usuario_dni", filter);
            usuario.setIdUsuario(usuarioDetail.getInt("usuario_id"));
            usuario.setNombreCompleto(usuarioDetail.getString("nombre"));
            usuario.setTipoUsuario(usuarioDetail.getInt("usuario_tipo_id"));
            usuario.setPrincipal(usuarioDetail.getString("principal").equalsIgnoreCase("S"));
            usuario.setTipoPersona(usuarioDetail.getString("persona_tipo"));
        }

        request.getSession().setAttribute("principal" , usuario.isPrincipal() ? "0" : "1");
        request.getSession().setAttribute("numerodocumento", usuario.getLogin());
        request.getSession().setAttribute(Constantes.USUARIO, usuario);

        // Cargamos las estadisticas
        if (!usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) ||
            (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()))) {
        	HashUtil<String, Object> filterEstadisticas = new HashUtil<String, Object>();
            HashUtil<String, Object> filterEstadisticasBuzon = new HashUtil<String, Object>();

            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
            	// NO CARGA ESTADISTICAS
            	filterEstadisticas.put("usuarioId", -1);
            } else {
            	filterEstadisticas.put("usuarioId", usuario.getIdUsuario());
            }
            if (!usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
                filterEstadisticasBuzon.put("usuarioId", usuario.getIdUsuario());
                //filterEstadisticasBuzon.put("tipoUsuario", usuario.getTipoUsuario());
            }

            String cantidadSolicitudesCertificado = ibatisService.loadElement("co.estadisticas.dj", filterEstadisticas).getString("CANTIDAD");
            String cantidadBorradoresSolicitudesCertificado = ibatisService.loadElement("co.estadisticas.borradoresOrdenes", filterEstadisticas).getString("CANTIDAD");

            filterEstadisticas.put("numeroDocumento", usuario.getNumeroDocumento());
            filterEstadisticas.put("documentoTipo", usuario.getTipoDocumento());

            String cantidadDJRegistradas = ibatisService.loadElement("co.estadisticas.djRegistradas", filterEstadisticas).getString("CANTIDAD");
            String cantidadDJAsignadas = ibatisService.loadElement("co.estadisticas.djAsignadas", filterEstadisticas).getString("CANTIDAD");

            String cantidadBuzon = ibatisService.loadElement("co.estadisticas.buzon", filterEstadisticas).getString("CANTIDAD");
            /*String cantidadCalificacion = ibatisService.loadElement("co.estadisticas.calificacion", filterEstadisticas).getString("CANTIDAD");
            String cantidadCertificado = ibatisService.loadElement("co.estadisticas.certificado", filterEstadisticas).getString("CANTIDAD");
            String cantidadBuzon = ibatisService.loadElement("co.estadisticas.buzon", filterEstadisticasBuzon).getString("CANTIDAD");*/

            String cantidadCalificacion = "0";
            String cantidadCertificado = "0";
            //String cantidadBuzon = "0";

            request.setAttribute("cantidadSolicitudesCertificado", cantidadSolicitudesCertificado);
            request.setAttribute("cantidadBorradoresSolicitudesCertificado", cantidadBorradoresSolicitudesCertificado);
            request.setAttribute("cantidadDJRegistradas", cantidadDJRegistradas);
            request.setAttribute("cantidadDJAsignadas", cantidadDJAsignadas);
            //request.setAttribute("cantidadCalificacion", cantidadCalificacion);
            //request.setAttribute("cantidadCertificado", cantidadCertificado);
            request.setAttribute("cantidadBuzon", cantidadBuzon);
        } else {
        	if ( usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) ) {
            	HashUtil<String, Object> filterEstadisticas = new HashUtil<String, Object>();
            	filterEstadisticas.put("entidadId", usuario.getIdEntidad());
        		// Este filtrado es por entidad
        		if (Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) {
	                String solicitudesSinAsignar = ibatisService.loadElement("co.estadisticas.resolutor.administrador.solicitudesSinAsignar", filterEstadisticas).getString("CANTIDAD");
	                String solicitudesPendientesRespuesta = ibatisService.loadElement("co.estadisticas.entidad.evaluador.solicitudesPendientesRespuesta", filterEstadisticas).getString("CANTIDAD");
	                String solicitudesAsignadas = ibatisService.loadElement("co.estadisticas.resolutor.administrador.solicitudesAsignadas", filterEstadisticas).getString("CANTIDAD");
	                request.setAttribute("solicitudesSinAsignar", "".equals(solicitudesSinAsignar)?"0":solicitudesSinAsignar);
	                request.setAttribute("solicitudesPendientesRespuesta", "".equals(solicitudesPendientesRespuesta)?"0":solicitudesPendientesRespuesta);
	                request.setAttribute("solicitudesAsignadas", "".equals(solicitudesAsignadas)?"0":solicitudesAsignadas);
            	} else if (Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) { // En este caso el filtrado es por usuario
            		filterEstadisticas.put("usuarioId", usuario.getIdUsuario());
	                String solicitudesPendientesRespuesta = ibatisService.loadElement("co.estadisticas.entidad.evaluador.solicitudesPendientesRespuesta", filterEstadisticas).getString("CANTIDAD");
	                String solicitudesAsignadas = ibatisService.loadElement("co.estadisticas.entidad.evaluador.solicitudesAceptadas", filterEstadisticas).getString("CANTIDAD");
	                request.setAttribute("solicitudesPendientesRespuesta", "".equals(solicitudesPendientesRespuesta)?"0":solicitudesPendientesRespuesta);
	                request.setAttribute("solicitudesAsignadas", "".equals(solicitudesAsignadas)?"0":solicitudesAsignadas);
            	} else if (Rol.CO_ENTIDAD_FIRMA.getNombre().equals(usuario.getRolActivo())) { // En este caso el filtrado es por usuario
            		filterEstadisticas.put("usuarioId", usuario.getIdUsuario());
	                String solicitudesAsignadas = ibatisService.loadElement("co.estadisticas.entidad.evaluador.solicitudesAceptadas", filterEstadisticas).getString("CANTIDAD");
	                request.setAttribute("solicitudesAsignadas", "".equals(solicitudesAsignadas)?"0":solicitudesAsignadas);
            	}
            }
        }
    }

    public static String replaceSpecialCharacters(String str) {
        String newString = str.replaceAll("[�]", "a").
                               replaceAll("[�]", "e").
                               replaceAll("[�]", "i").
                               replaceAll("[�]", "o").
                               replaceAll("[�]", "u").
                               replaceAll("[\\^]", "").
                               replaceAll("[~]", "").
                               replaceAll("[�]", "");
        return newString;
    }

    /**
     * Metodo que chequea (Cast) que los elementos de una Coleccion sean del tipo de clase que se espera antes de a�adirlo
     * a una Lista (modo seguro Safe).
     * @param clazz Clase contra la que se realiza la validacion CAST
     * @param c La coleccion a validar
     * @return una lista segura solo con los objetos del tipo "clazz"
     */
	public static <T> List<T> castList(Class<? extends T> clazz, Collection<?> c) {
		List<T> r = new ArrayList<T>(c.size());
		for (Object o : c)
			r.add(clazz.cast(o));
		return r;
	}

	public static String getStackTrace(Throwable throwable) {
	    Writer writer = new StringWriter();
	    PrintWriter printWriter = new PrintWriter(writer);
	    throwable.printStackTrace(printWriter);
	    return writer.toString();
    }

	/**
	 * Verifica, seg�n el acuerdo, las reglas para definir si falta o no llenar informaci�n del consolidado.
	 * @param idAcuerdo Id del acuerdo.
	 * @param djObj Objeto Declaraci�n Jurada.
	 * @return boolean Es v�lido.
	 */
	public static boolean consolidadoCorrectoSegunAcuerdo(Integer idAcuerdo, DeclaracionJurada djObj) {
		boolean result = true;

		//Para cualquier acuerdo
		if ( djObj.getValorUs() == null || djObj.getValorUs().equals(0.00) ) {
				result = false;
		}

		// Si el acuerdo es China, el criterio origen es diferente de 3, y no hay datos, es incorrecto y deberia dar mensaje de validaci�n
		if ( ConstantesCO.AC_CHINA.equals(idAcuerdo) ) {
			if ( djObj.getSecuenciaOrigen()!= null && djObj.getSecuenciaOrigen() == 3 ) {
				if ( djObj.getPorcentajeSegunCriterio() == null || djObj.getPorcentajeSegunCriterio().equals(0.00) ) {
					result = false;
				}
			}
		}
		// Si el acuerdo es EFTA, y no hay datos, es incorrecto y deberia dar mensaje de validaci�n
		if ( ConstantesCO.AC_EFTA.equals(idAcuerdo) ) {
			// || djObj.getPesoNetoMercancia() == null || djObj.getPesoNetoMercancia().equals(0.00)
			if ( djObj.getValorUsFabrica() == null || djObj.getValorUsFabrica().equals(0.00) ) {
				result = false;
			}
		}
		// Si el acuerdo es Mexico, el criterio origen es diferente de 3, y no hay datos, es incorrecto y deberia dar mensaje de validaci�n
		if ( ConstantesCO.AC_MEXICO.equals(idAcuerdo) ) {
			/*if ( djObj.getSecuenciaOrigen() == 3 ) {
				if ( djObj.getPesoNetoMercancia() == null || djObj.getPesoNetoMercancia().equals(0.00) ) {
					result = false;
				}
			}*/
		}

		// Si el acuerdo es TLC JAPON, y no hay datos, es incorrecto y deberia dar mensaje de validaci�n
		if ( ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo) ) {
			if ( djObj.getDemasGasto() == null || djObj.getDemasGasto().equals(0.00) ){
				 result = false;
			}
			if ( djObj.getSecuenciaOrigen() != null && djObj.getSecuenciaOrigen() == 3 ) {
				if ( djObj.getPesoNetoMercancia() == null || djObj.getPesoNetoMercancia().equals(0.00) ) {
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * Generar� la cadena a completar de acuerdo al codigo de pais utilizado
	 * @param codigoPais C�digo de pa�s.
	 * @return String Cadena para etiqueta de ayuda.
	 */
	public static String obtenerCadenaParaEtiquetaAyuda(String acuerdoId, String codigoPais) {
		String result = "";

		if ( "P".equals(codigoPais) ) {
			result = "MATERIAL_PERU";
		} else if ( "N".equals(codigoPais) ) {
			result = "MATERIAL_NO_ORIGINARIO";
		} else {
			if ( "10".equals(acuerdoId) || "15".equals(acuerdoId) ) {
				if ( "SJ".equals(codigoPais) || "J".equals(codigoPais) ) {
					result = "MATERIAL_JAPON";
				}
			} else if ("22".equals(acuerdoId)){
				if ( "GT".equals(codigoPais) ) {
					result = "MATERIAL_GUATEMALA";
				}else if ( "GTO".equals(codigoPais) ) {
					result = "MATERIAL_OTRO_ORIGINARIO";
				}
			} else if ("17".equals(acuerdoId)){
				if ( "C".equals(codigoPais) ) {
					result = "MATERIAL_COREA";
				}
			} else if (ConstantesCO.AC_SGP_NORUEGA.toString().equals(acuerdoId)){
				if ( "NR".equals(codigoPais) ) {
					result = "MATERIAL_NORUEGA";
				}else if ( "NRT".equals(codigoPais) ) {
					result = "MATERIAL_SUIZA_UE";
				}
			} else if (ConstantesCO.AC_TLC_UE.toString().equals(acuerdoId)){
				if ( "UE".equals(codigoPais) ) {
					result = "MATERIAL_COLOMBIA_UE";
				}else if ( "UET".equals(codigoPais) ) {
					result = "MATERIAL_OTROS_PAISES";
				}
			} else if (ConstantesCO.AC_SGP_NZ.toString().equals(acuerdoId)){
				if ( "NZ".equals(codigoPais) ) {
					result = "MATERIAL_NUEVA_ZELANDA";
				}
			} else if (ConstantesCO.AC_SGP_CANADA.toString().equals(acuerdoId)){
				if ( "SC".equals(codigoPais) ) {
					result = "MATERIAL_CANADA";
				}else if ( "SCT".equals(codigoPais) ) {
					result = "MATERIAL_OTROS_PAISES";
				}
			} else if (ConstantesCO.AC_TLC_SI.toString().equals(acuerdoId)){
				if ( "SI".equals(codigoPais) ) {
					result = "MATERIAL_SINGAPUR";
				}
			} else if (ConstantesCO.AC_TLC_TAILANDIA.toString().equals(acuerdoId)){
				if ("T".equals(codigoPais)){
					result = "MATERIAL_TAILANDIA";
				}
			} else if (ConstantesCO.AC_MEXICO.toString().equals(acuerdoId)){
				if ("M".equals(codigoPais)){
					result = "MATERIAL_MEXICO";
				}
			} else if (ConstantesCO.AC_SGPC.toString().equals(acuerdoId)){
				if ("SG".equals(codigoPais)){
					result = "MATERIAL_SGPC";
				}
			} else if (ConstantesCO.AC_SGP_RUSIA.toString().equals(acuerdoId)){
				if ("R".equals(codigoPais)){
					result = "MATERIAL_RUSIA";
				}
			}else if (ConstantesCO.AC_COSTARRICA.toString().equals(acuerdoId)){
				if ("CR".equals(codigoPais)){
					result = "MATERIAL_CRICA";
				} else if ("CRT".equals(codigoPais)){
					result = "MATERIAL_OTRO_ORIGINARIO";
				}
			}else if (ConstantesCO.AC_PANAMA.toString().equals(acuerdoId)){
				if ("PNM".equals(codigoPais)){
					result = "MATERIAL_PANAMA";
				}else if ("PNO".equals(codigoPais)){
					result = "MATERIAL_OTRO_ORIGINARIO";
				}
			} else if ( ConstantesCO.AC_MERCOSUR.toString().equals(acuerdoId) ) {
				if ( "ME".equals(codigoPais) ) {
					result = "MATERIAL_MERCOSUR";
				} else if ( "CA".equals(codigoPais) ) {
					result = "MATERIAL_ANDINA";
				}
			} else if ( ConstantesCO.AC_CAN.toString().equals(acuerdoId) ) {
				if ( "CAN".equals(codigoPais) ) {
					result = "MATERIAL_CAN";
				}
			} else if ( ConstantesCO.AC_TLC_VENEZUELA.toString().equals(acuerdoId) ) {
				if ( "V".equals(codigoPais) ) {
					result = "MATERIAL_VENEZUELA";
				}
			} else if ( ConstantesCO.AC_SGP_TURQUIA.toString().equals(acuerdoId) ) {
				if ( "TQ".equals(codigoPais) ) {
					result = "MATERIAL_AELC";
				}
			}else if ( ConstantesCO.AC_SGP_AUS.toString().equals(acuerdoId) ) {
				if ( "AUS".equals(codigoPais) ) {
					result = "MATERIAL_AUSTRALIA";
				}
			}

		}

		return result;
	}

	public static boolean validarInformacionGeneralSegunAcuerdo(Integer idAcuerdo, CertificadoOrigen certificadoOrigenObj) {
		boolean res = true;
System.out.println("****************************idAcuerdo: "+idAcuerdo);
		if (ConstantesCO.AC_SGP_UE.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				  ( certificadoOrigenObj.getMedioTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getMedioTransporte()) ) &&
				  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				) ){

				res = false;
			}
		} else if (ConstantesCO.AC_MEXICO.equals(idAcuerdo)) {
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais())
				){

				res = false;
			}
		} else if (ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				  ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
				  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				) ){

				res = false;
			}
		} else if (ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais())
				){
				/*||   //04072014_JMC BUG - CO_VUCE Autorizaci�n Impresi�n Certificado de Origen / JAPON
				certificadoOrigenObj.getFechaPartidaTransporte() == null ||
				( certificadoOrigenObj.getNumeroTransporte() == null || "".equals(certificadoOrigenObj.getNumeroTransporte()) ) ||
				( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) ||
				( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				){*/

				res = false;
			}
		} else if (ConstantesCO.AC_SGPC.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				  ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
				  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				) ){

				res = false;
			}
		} else if (ConstantesCO.AC_CHINA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
				( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) ) )
				){

				res = false;
			}
		} else if (ConstantesCO.AC_PANAMA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				certificadoOrigenObj.getImportadorRegistroFiscal() == null || "".equals(certificadoOrigenObj.getImportadorRegistroFiscal()) ||
				certificadoOrigenObj.getImportadorTelefono() == null || "".equals(certificadoOrigenObj.getImportadorTelefono())
				){

				res = false;
			}
		} else if (ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				certificadoOrigenObj.getImportadorRegistroFiscal() == null || "".equals(certificadoOrigenObj.getImportadorRegistroFiscal()) ||
				certificadoOrigenObj.getImportadorTelefono() == null || "".equals(certificadoOrigenObj.getImportadorTelefono())
				){

				res = false;
			}
		} else if (ConstantesCO.AC_CUBA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais())
				){

				res = false;
			}
		} else if (ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				certificadoOrigenObj.getImportadorRegistroFiscal() == null || "".equals(certificadoOrigenObj.getImportadorRegistroFiscal()) ||
				certificadoOrigenObj.getImportadorTelefono() == null || "".equals(certificadoOrigenObj.getImportadorTelefono())
				){

				res = false;
			}
		} else if (ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				  ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
				  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				) ){

				res = false;
			}
		} else if (ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)){
			if ( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				 ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
				 ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				 ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				){

				res = false;
			}
		} else if (ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ){

				res = false;
			}
		} else if (ConstantesCO.AC_COREA.equals(idAcuerdo)){
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
				( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
				  ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
				  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
				  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
				) ){

				res = false;
			}
		} else if ( ConstantesCO.AC_TLC_SI.equals(idAcuerdo) ) {
			if ( certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
				 certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
				 certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ) {
				res = false;
			}
		} else if (ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)) {
			if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
					certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
					certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
					( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
					  ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
					  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
					  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
					) ) {
				res = false;
			}
		} else if ( ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo) ) {
			if ( certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
					 certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
					 certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ) {
					res = false;
				}
		} else if ( ConstantesCO.AC_HONDURAS.equals(idAcuerdo) ) {
				if ( certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
						 certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
						 certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ) {
						res = false;
					}
		}else if (ConstantesCO.AC_SGP_AUS.equals(idAcuerdo)) {
					if (certificadoOrigenObj.getImportadorNombre() == null || "".equals(certificadoOrigenObj.getImportadorNombre()) ||
							certificadoOrigenObj.getImportadorDireccion() == null || "".equals(certificadoOrigenObj.getImportadorDireccion()) ||
							certificadoOrigenObj.getImportadorPais() == null || new Integer("0").equals(certificadoOrigenObj.getImportadorPais()) ||
							( certificadoOrigenObj.getFechaPartidaTransporte() == null &&
							  ( certificadoOrigenObj.getNumeroTransporte() == null || new Integer("0").equals(certificadoOrigenObj.getNumeroTransporte()) ) &&
							  ( certificadoOrigenObj.getPuertoCargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoCargaId()) ) &&
							  ( certificadoOrigenObj.getPuertoDescargaId() == null || new Long("0").equals(certificadoOrigenObj.getPuertoDescargaId()) )
							) ) {
						res = false;
					}
				} 

		return res;
	}

	public static boolean validarFaltaValorFOBMateriales(Integer idAcuerdo, Long djId, IbatisService ibatisService){
		boolean res = false;
		HashUtil<String, Object> filterMateriales = new HashUtil<String, Object>();
        filterMateriales.put("djId", djId);
        
        Table tMaterialesFOB = ibatisService.loadGrid("certificadoOrigen.orden.dj.material.porcentajeValor.list", filterMateriales);
        if (tMaterialesFOB.getTotalTableSize()>0){
        	res=true;
        }        	
        return res;	
	}
	
	public static void validarBotonesProductor(HttpServletRequest request, HashUtil<String, String> disabledButtons) {
        String permiteProductor = request.getAttribute("mostrarBotonProductor")!=null ? (String)request.getAttribute("mostrarBotonProductor"):"";

        // Si la funci�n de validaci�n dice que no se permite m�s de un productor
    	if ("N".equals(permiteProductor)) {
    		disabledButtons.put("nuevoProductorCalificacionButton", Constantes.DISABLING_ACTION_HIDE);
    	}

        String mostrarBtnValidacionProd = (String) request.getAttribute("mostrarBtnValidacionProd");
        if ("0".equals(mostrarBtnValidacionProd)) {
        	disabledButtons.put("solicitarValidacionCalificacionButton", Constantes.DISABLING_ACTION_HIDE);
        }

    	return;
	}
    
	public static void agregarMessageFaltante(MessageList messageListCopia, MessageList messageList) {
		Message msg  = null;
    	for (Object object : messageListCopia) {
			msg = (Message) object;
            
			if (msg.getMessageKey() == null || (!"insert.success".equals(msg.getMessageKey()) && !"update.success".equals(msg.getMessageKey()))) {
				messageList.add(object);
			}
		}
	}
	
		
	public static void main(String [] args) {
		
			
		//verificaFirma();
		obtenerNombreFirmante();
		
	}
	
	public static void verificaFirma(){
		
		try{		
		
		org.bouncycastle.asn1.x500.X500Name x500name = null;
		X509Certificate x509 = null;
			
		org.apache.xml.security.Init.init();
		String signatureFileName = "C:\\logs\\firma\\certificadofirmadoPeru.xml";		 
		//String signatureFileName = "C:\\logs\\firma\\signature.xml";
 
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
 
		dbf.setNamespaceAware(true);
		dbf.setAttribute("http://xml.org/sax/features/namespaces", Boolean.TRUE);
		
		File			f	= new File(signatureFileName);
		DocumentBuilder db	= dbf.newDocumentBuilder();
		Document	 doc	= db.parse(new java.io.FileInputStream(f));
		//Element		 sigElement = (Element) doc.getElementsByTagName("Signature").item(0);
		Element		 sigElement = (Element) doc.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature").item(1);
		
		//XMLSignature signature  = new XMLSignature(sigElement, f.toURL().toString());
		XMLSignature signature  = new XMLSignature(sigElement, "");
 
		KeyInfo keyInfo = signature.getKeyInfo();
		if (keyInfo != null) {
			X509Certificate cert = keyInfo.getX509Certificate();
			if (cert != null) {
				
				x500name = new JcaX509CertificateHolder(cert).getSubject();				
		        RDN cn = x500name.getRDNs(BCStyle.CN)[0];
		        String cnName = IETFUtils.valueToString(cn.getFirst().getValue());
		        System.out.println("cnName::"+cnName);
				
				
				// Validamos la firma usando un certificado X509
				if (signature.checkSignatureValue(cert)){
					System.out.println("V�lido seg�n el certificado");	
				} else {
					System.out.println("Inv�lido seg�n el certificado");	
				}
			} else {
				// No encontramos un Certificado intentamos validar por la cl�ve p�blica
				PublicKey pk = keyInfo.getPublicKey();
				if (pk != null) {
					// Validamos usando la clave p�blica
					if (signature.checkSignatureValue(pk)){
						System.out.println("V�lido seg�n la clave p�blica");	
					} else {
						System.out.println("Inv�lido seg�n la clave p�blica");	
					}
				} else {
					System.out.println("No podemos validar, tampoco hay clave p�blica");
				}
			}
		} else {
			System.out.println("No ha sido posible encontrar el KeyInfo");
		}
	
		} catch(Exception e){
			System.out.println("Exception"+e);
		}
		
	}
	
	public static void obtenerNombreFirmante(){
		try {
			
		File file = new File("C:\\logs\\firma\\certificadofirmadoMexico.xml");
		//File file = new File("C:\\logs\\firma\\certificadofirmadoPeru.xml");
		//File file = new File("C:\\logs\\firma\\signature.xml");
		
		org.apache.xml.security.Init.init();
		
		byte[] bytesXMLTabla = StreamUtil.getFileAsBytes(file);
		
		
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder builder = dbf.newDocumentBuilder();        
        Document doc = builder.parse(new InputSource(new ByteArrayInputStream(bytesXMLTabla)));
		
		//dbf.setAttribute("http://xml.org/sax/features/namespaces", Boolean.TRUE);


        //Obtener el Nombre del Firmante
        Element sigElement = (Element) doc.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature").item(1); //una firma
        XMLSignature signature = null;
        org.bouncycastle.asn1.x500.X500Name x500name = null;
        X509Certificate x509 = null;
        KeyInfo keyInfo = null;
		
		signature = new XMLSignature(sigElement, "");
		//System.out.println("getSignatureValue:::"+ signature.getSignatureValue());
	    keyInfo = signature.getKeyInfo();
	    //keyInfo.get
		x509 = keyInfo.getX509Certificate();
		
		x500name = new JcaX509CertificateHolder(x509).getSubject();
		
        RDN cn = x500name.getRDNs(BCStyle.CN)[0];
        String cnName = IETFUtils.valueToString(cn.getFirst().getValue());

        System.out.println("cnName::"+cnName);
        
        
        
        
		} catch (ParserConfigurationException e) {
			
			System.out.println("dbf.newDocumentBuilder() -> ParserConfigurationException::"+e);
		} catch (FileNotFoundException e) {

			System.out.println("db.parse -> FileNotFoundException::"+e);
		} catch (SAXException e) {
			System.out.println("db.parse -> SAXException::"+e);
			
		} catch (IOException e) {
			System.out.println("db.parse -> IOException::"+e);
		
		} catch (KeyResolverException e) {			
			System.out.println("keyInfo.getX509Certificate -> KeyResolverException::"+e);
			
		}  catch (CertificateEncodingException e) {			
			System.out.println("new JcaX509CertificateHolder -> CertificateEncodingException::"+e);
			
		} catch (NullPointerException e) {
			System.out.println("keyInfo.getX509Certificate -> NullPointerException::"+e);		
			System.out.println("keyInfo.getX509Certificate -> NullPointerException"+e);
		
		} catch (XMLSignatureException e) {			
			System.out.println("new XMLSignature -> XMLSignatureException::"+e);
			
		} catch (XMLSecurityException e) {			
			System.out.println("new XMLSignature -> XMLSecurityException::"+e);
			
		} catch (Exception e) {			
			System.out.println("StreamUtil -> Exception::"+e);
			
		}
		
	}
	
	//Ticket 149343 GCHAVEZ-09/07/2019 
    public static String removerCaracterProhibidoUrl(String input) {
	    String output = "";
	    if(input!=null){
	    	output = input;
		    for (int i=0; i<ConstantesCO.CADENA_URL_PROHIBIDO.length(); i++) {
		        output = output.replace(String.valueOf(ConstantesCO.CADENA_URL_PROHIBIDO.charAt(i)), "");
		    }
	    }
	    return output;
	}
	//Ticket 149343 GCHAVEZ-09/07/2019
	
}
