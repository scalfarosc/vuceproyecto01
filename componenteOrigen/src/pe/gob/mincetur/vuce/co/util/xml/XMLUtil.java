package pe.gob.mincetur.vuce.co.util.xml;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.tools.generic.DateTool;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pe.gob.mincetur.vuce.co.bean.ErroresEmpresa;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionEmpresaVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionIOPVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransmisionVUCE;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.xml.XMLUtil;


public class XMLUtil {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);

	/**
     * Genera un XML a partir de un objeto de tipo RespuestaErrorEmpresa
     * @param respuestaErrorEmpresa
     * @return
     */
	public static byte[] generarMensajeRespuesta(TransaccionEmpresaVUCE respuestaErrorEmpresa) throws Exception {
		HashUtil objects = new HashUtil();
		objects.put("mensaje", respuestaErrorEmpresa);
		String xml = generateXML(respuestaErrorEmpresa.getTemplate(), objects);
		
		return xml.getBytes();
	}
	
	/**
     * Genera un XML a partir de un objeto de tipo ErroresEmpresa
     * @param erroresEmpresa
     * @return
     */
	public static byte[] generarMensajeErroresEmpresa(ErroresEmpresa erroresEmpresa) throws Exception {
		HashUtil objects = new HashUtil();
		objects.put("erroresEmpresa", erroresEmpresa);
		String xml = generateXML(erroresEmpresa.getTemplate(), objects);
		
		return xml.getBytes();
	}

	private static String generateXML(String templateName, HashUtil<String, Object> objects) throws Exception {
	    Properties props = new Properties();		   
	    props.setProperty("file.resource.loader.path", ApplicationConfig.getApplicationConfig().getApplicationResource("ruta.templates", "").getFile().getCanonicalPath());
	    
	    VelocityEngine ve = new VelocityEngine();
	    ve.init(props);
	    Template t = null;
	    if (templateName.toLowerCase().startsWith("empresa")) {
	    	t = ve.getTemplate(templateName, "UTF-8");
	    	t.setEncoding("UTF-8");
	    } else {
	    	t = ve.getTemplate(templateName);
	    }
	    
	    VelocityContext context = new VelocityContext();
	    
	    Enumeration<String> en = objects.keys();
	    while (en.hasMoreElements()) {
	        String key = en.nextElement();
	        Object obj = objects.get(key);
	        context.put(key, obj);
	    }
	    StringWriter writer = new StringWriter();
	    t.merge(context, writer);
	    return writer.toString();
	}
	
    public static AdjuntoFormato generateEBXML(SolicitudEBXML ebXML, HashUtil<String, Object> objects) throws Exception {
        AdjuntoFormato adjunto = null;
        objects.put("dateTool", new DateTool());
        String xml = XMLUtil.generateXML(ebXML.getTemplate(), objects);
        byte [] bytesEBXML = xml.getBytes();
        adjunto = getAdjuntoFormatoFromEBXML(bytesEBXML);
        return adjunto;
    }	
	
    public static AdjuntoFormato getAdjuntoFormatoFromEBXML(byte [] bytesEBXML) {
        AdjuntoFormato adjunto = new AdjuntoFormato();
        adjunto.setArchivo(bytesEBXML);
        adjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_EBXML);
        adjunto.setNombre(ConstantesCO.ADJUNTO_NOMBRE_EBXML);
        return adjunto;
    }
    
    public static TransmisionVUCE getMensajeTransmisionFromXML(Class clazz, InputStream isXMLTransmision) {
        TransmisionVUCE mensajeTransmision = null;
        /*
        if (clazz.equals(NotificacionVUCE.class)) {
            mensajeTransmision = getNotificacionFromXML(isXMLTransmision);
        } else if (clazz.equals(NotificacionSUNAT.class)) {
            mensajeTransmision = getNotificacionSUNATFromXML(isXMLTransmision);
        } else if (clazz.equals(TransaccionVUCE.class)) {
            mensajeTransmision = getTransaccionFromXML(isXMLTransmision);
        } else if (clazz.equals(SincronizacionVUCE.class)) {
            mensajeTransmision = getSincronizacionFromXML(isXMLTransmision);
        } else*/ 
        if (clazz.equals(TransaccionEmpresaVUCE.class)) {
            mensajeTransmision = getTransaccionEmpresaFromXML(isXMLTransmision);
        }
        return mensajeTransmision;
    }
    

    private static TransaccionEmpresaVUCE getTransaccionEmpresaFromXML(InputStream isXMLTransaccion) {
        TransaccionEmpresaVUCE transaccion = null;
        try {
  	      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
  	      dbf.setNamespaceAware(true);
  	      DocumentBuilder builder = dbf.newDocumentBuilder();
  	      builder.setErrorHandler(new SchemaValidationErrorHandler());
  	      Document doc = builder.parse(new InputSource(isXMLTransaccion));

  	      SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
  	      
  	      Schema schema = factory.newSchema(ApplicationConfig.getApplicationConfig().getApplicationResource("ruta.schemas", "TransaccionEmpresa-1.0.xsd").getFile());
  	      Validator validator = schema.newValidator();
  	      validator.validate(new DOMSource(doc));

  	      logger.debug("El documento es valido");

  	      XPathFactory xpFactory = XPathFactory.newInstance();
  	      XPath xPath = xpFactory.newXPath();
  	      xPath.setNamespaceContext(new NamespaceContext() {
  	        public String getNamespaceURI(String prefix) {
  	          return "TransaccionEmpresa-1.0";
  	        }
  	        public String getPrefix(String p) {
  	          return "vuce";
  	        }
  	        public Iterator<Object> getPrefixes(String p) {
  	          return null;
  	        }
  	      });
  	      Node dr = (Node)xPath.evaluate("/vuce:transaccion", doc, XPathConstants.NODE);
  	      transaccion = new TransaccionEmpresaVUCE();
  	      transaccion.setTransaccion(xPath.evaluate("vuce:tipoMensaje", dr, XPathConstants.STRING).toString());
  	      //String empresa = xPath.evaluate("vuce:empresa", dr, XPathConstants.STRING).toString();
  	      //if (!empresa.equals("")) transaccion.setEntidad((Integer.valueOf(empresa).intValue()));
  	      transaccion.setCodigoFormato(xPath.evaluate("vuce:formato", dr, XPathConstants.STRING).toString());
  	      transaccion.setRucUsuario(xPath.evaluate("vuce:rucUsuario", dr, XPathConstants.STRING).toString());
  	      transaccion.setCodigoUsuario(xPath.evaluate("vuce:codigoUsuario", dr, XPathConstants.STRING).toString());

  	      
  	      /** No aplica para el primer alcance de Interoperabilidad con EMPRESAS
  	       *   	       
  	      Node drDocumento = (Node)xPath.evaluate("/vuce:transaccion/:documento", doc, XPathConstants.NODE);
  	      transaccion.setTipoDocumento(xPath.evaluate("vuce:tipo", drDocumento, XPathConstants.STRING).toString());
  	      transaccion.setNumeroDocumento(xPath.evaluate("vuce:numero", drDocumento, XPathConstants.STRING).toString());

  	      Node drDocumentoReferencia = (Node)xPath.evaluate("/vuce:transaccion/:documentoReferencia", doc, XPathConstants.NODE);
  	      transaccion.setTipoDocumentoReferencia(xPath.evaluate("vuce:tipo", drDocumentoReferencia, XPathConstants.STRING).toString());
  	      transaccion.setNumeroDocumentoReferencia(xPath.evaluate("vuce:numero", drDocumentoReferencia, XPathConstants.STRING).toString()); 	      
  	      
          NodeList drNotificacionesResueltas = (NodeList)xPath.evaluate("/vuce:transaccion/:notificacionesResueltas", doc, XPathConstants.NODESET);
  	      transaccion.setNotificacionesResueltas(new ArrayList());
  	      for (int i = 0; i < drNotificacionesResueltas.getLength(); ++i) {
  	          Node drError = drNotificacionesResueltas.item(i);
  	          transaccion.getNotificacionesResueltas().add(xPath.evaluate("vuce:notificacion", drError, XPathConstants.STRING).toString());
  	      }
  	      */
  	  } catch (FileNotFoundException e) {
  	      logger.error("Error al validar el archivo XML de Transaccion", e);
  	  } catch (ParserConfigurationException e) {
  	      logger.error("Error al validar el archivo XML de Transaccion", e);
  	  } catch (SAXException e) {
  	      logger.error("Error al validar el archivo XML de Transaccion. Documento no valido", e);
  	  } catch (IOException e) {
  	      logger.error("Error al validar el archivo XML de Transaccion", e);
  	  } catch (XPathExpressionException e) {
  	      logger.error("Error al validar el archivo XML de Transaccion", e);
  	  }
        return transaccion;
    }
    

    /**
     * Devuelve true si el esquema del archivo xml corresponde con el envio en el archivo esquemaXsd, y false
     * si es todo lo contratio.
     * @param String archivoXml
     * @param String esquemaXsd
     * @return boolean
     */
    public static boolean validarEsquema(File archivoXml, File esquemaXsd) throws Exception {
        DocumentBuilderFactory dbf;
        DocumentBuilder builder;
        Document doc;
        SchemaFactory factory;
        Schema schema;
        Validator validator;
        try {

            dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            builder = dbf.newDocumentBuilder();
            builder.setErrorHandler(new SchemaValidationErrorHandler());
            doc = builder.parse(archivoXml);
            factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = factory.newSchema(esquemaXsd);
            validator = schema.newValidator();
            validator.validate(new DOMSource(doc));

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
            return false;
        } catch (SAXException ex) {
            ex.printStackTrace();
            return false;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    //JMC 07/12/2016 ALIANZA PACIFICO
	public static byte[] generarMensajePack(TransaccionIOPVUCE mensajePack) throws Exception {
		HashUtil objects = new HashUtil();
		objects.put("mensaje", mensajePack);
		String xml = generateXML(mensajePack.getTemplate(), objects);
		
		return xml.getBytes();
	}
    
    
}
