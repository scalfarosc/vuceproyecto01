package pe.gob.mincetur.vuce.co.util.xml;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

import pe.gob.mincetur.vuce.co.exception.XMLParseException;

public class SchemaValidationErrorHandler implements ErrorHandler {
	
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
    
    public void warning(SAXParseException e) throws XMLParseException {
        logger.warn("warning");
        throw new XMLParseException(this, XMLParseException.WARNING, e.getLineNumber(), e.getMessage());
    }
    public void error(SAXParseException e) throws XMLParseException {
        logger.error("error");
        throw new XMLParseException(this, XMLParseException.ERROR, e.getLineNumber(), e.getMessage());
    }
    public void fatalError(SAXParseException e) throws XMLParseException {
        logger.fatal("fatal");
        throw new XMLParseException(this, XMLParseException.FATAL, e.getLineNumber(), e.getMessage());
    }

}
