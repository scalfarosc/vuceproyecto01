package pe.gob.mincetur.vuce.co.util;


public enum Rol {

	CO_USUARIO_OPERACION(23, "CO.USUARIO.OPERACION"),
    CO_ENTIDAD_EVALUADOR(24, "CO.ENTIDAD.EVALUADOR"),
    CO_ENTIDAD_SUPERVISOR(25, "CO.ENTIDAD.SUPERVISOR"),
    CO_ADMIN_HELPDESK(26, "CO.ADMIN.HELP_DESK"),
    CO_USUARIO_SUPERVISOR(27, "CO.USUARIO.SUPERVISOR"),
    CO_ENTIDAD_FIRMA(28, "CO.ENTIDAD.FIRMA"),
    CO_CENTRAL_OPERADOR_FUNCIONAL(29, "CO.CENTRAL.OPERADOR_FUNCIONAL"),
    CO_CENTRAL_OPERADOR_INFRAESTRUCTURA(30, "CO.CENTRAL.OPERADOR_INFRAESTRUCTURA"),
    CO_CENTRAL_SUPERVISOR_TECNICO(31, "CO.CENTRAL.SUPERVISOR_TECNICO"),
    CO_ENTIDAD_CONSULTA_DR(38, "CO.ENTIDAD.CONSULTA_DR"),
    CO_USUARIO_FIRMA(40, "CO.USUARIO.FIRMA"),
    VUCE_SUNAT_ESPECIALISTA (7, "VUCE.SUNAT.ESPECIALISTA");


    private Integer codigo;

    private String nombre;

    private Rol(int codigo, String nombre) {
        this.codigo=codigo;
        this.nombre = nombre;
    }

    public Integer getCodigo() {
        return this.codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public static Integer getCodigoByNombre(String nombre) {
        Rol [] roles = values();
        for (Rol rol : roles) {
            if (rol.getNombre().equals(nombre)) {
                return rol.getCodigo();
            }
        }
        return null;
    }
}
