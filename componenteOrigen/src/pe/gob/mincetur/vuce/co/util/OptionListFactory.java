/*
 * SigverOptionListFactory.java
 *
 * Created on 07/08/2007, 11:02:08 AM
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.gob.mincetur.vuce.co.util;

import org.jlis.core.bean.Option;
import org.jlis.core.list.OptionList;


public class OptionListFactory {

    public static OptionList getListaPlazoInternamiento() {
        OptionList listaPlazoInternamiento = new OptionList();
        listaPlazoInternamiento.add(new Option("0", "0"));
        listaPlazoInternamiento.add(new Option("1", "1"));
        listaPlazoInternamiento.add(new Option("2", "2"));
        listaPlazoInternamiento.add(new Option("3", "3"));
        listaPlazoInternamiento.add(new Option("4", "4"));
        listaPlazoInternamiento.add(new Option("5", "5"));
        listaPlazoInternamiento.add(new Option("6", "6"));
        return listaPlazoInternamiento;
    }
    
    public static OptionList getTipoPago() {
        OptionList listaTipoPago = new OptionList();
        listaTipoPago.add(new Option("T", "TOTAL"));
        listaTipoPago.add(new Option("P", "PARCIAL"));
        return listaTipoPago;
    }
    
    public static OptionList getEstado() {
        OptionList listaEstado = new OptionList();
        listaEstado.add(new Option("A","Activo"));
        listaEstado.add(new Option("I", "Inactivo"));
        return listaEstado;
    }

    public static OptionList getListaSiNo() {
        OptionList listaEstado = new OptionList();
        listaEstado.add(new Option("S","S�"));
        listaEstado.add(new Option("N", "No"));
        return listaEstado;
    }
    
    public static OptionList getListaEstadoCT() {
        OptionList listaEstado = new OptionList();
        listaEstado.add(new Option("T","PENDIENTE DE RESPUESTA"));
        listaEstado.add(new Option("F","RESPONDIDA"));
        return listaEstado;
    }

    public static OptionList getListaControladoNoControlado() {
        OptionList listaEstado = new OptionList();
        listaEstado.add(new Option("C","CONTROLADO"));
        listaEstado.add(new Option("N", "NO CONTROLADO"));
        return listaEstado;
    }
    
    public static OptionList getListaAccionAprobarRechazar() {
        OptionList listaAccionAprobarRechazar = new OptionList();
        listaAccionAprobarRechazar.add(new Option("A", "APROBAR"));
        listaAccionAprobarRechazar.add(new Option("R", "RECHAZAR"));
        return listaAccionAprobarRechazar;
    }
    
    public static OptionList getListaMostrarEnCertificado() {
        OptionList listaMostrarEnCertificado = new OptionList();
        listaMostrarEnCertificado.add(new Option("DC", "Descripci�n Comercial"));
        listaMostrarEnCertificado.add(new Option("DE", "Descripci�n para Certificado"));
        return listaMostrarEnCertificado;
    }
    
    public static OptionList getListaModulosOrigen() {
    	OptionList listaModulosOrigen = new OptionList();
        listaModulosOrigen.add(new Option(ConstantesCO.MODULO_CERTIFICADO_ORIGEN, "Certificados de Origen por Entidad Certificadora"));
        listaModulosOrigen.add(new Option(ConstantesCO.MODULO_SOLICITUD_CALIFICACION_EXPORTADOR, "Solicitudes de Calificaci�n de Exportador"));
        return listaModulosOrigen;
    }
    
    public static OptionList getListaTipoSolicitanteExportador() {
        OptionList listaTipoSolicitanteExportador = new OptionList();
        listaTipoSolicitanteExportador.add(new Option("P", "Productor"));
        listaTipoSolicitanteExportador.add(new Option("E", "Exportador"));
        listaTipoSolicitanteExportador.add(new Option("PE", "Productor y Exportador"));
        return listaTipoSolicitanteExportador;
    }
    
    public static OptionList getListaMetodoPruebaValor() {
        OptionList listaMetodoPruebaValor = new OptionList();
        listaMetodoPruebaValor.add(new Option("T", "Transacci�n"));
        listaMetodoPruebaValor.add(new Option("A", "Aumento"));
        listaMetodoPruebaValor.add(new Option("R", "Reducci�n"));
        listaMetodoPruebaValor.add(new Option("O", "Otro (especificar)"));
        return listaMetodoPruebaValor;
    }
    
    public static OptionList getListaMeses() {
        OptionList listaMeses = new OptionList();
        listaMeses.add(new Option("1", "Enero"));
        listaMeses.add(new Option("2", "Febrero"));
        listaMeses.add(new Option("3", "Marzo"));
        listaMeses.add(new Option("4", "Abril"));
        listaMeses.add(new Option("5", "Mayo"));
        listaMeses.add(new Option("6", "Junio"));
        listaMeses.add(new Option("7", "Julio"));
        listaMeses.add(new Option("8", "Agosto"));
        listaMeses.add(new Option("9", "Septiembre"));
        listaMeses.add(new Option("10", "Octubre"));
        listaMeses.add(new Option("11", "Noviembre"));
        listaMeses.add(new Option("12", "Diciembre"));
        return listaMeses;
    }

    
}