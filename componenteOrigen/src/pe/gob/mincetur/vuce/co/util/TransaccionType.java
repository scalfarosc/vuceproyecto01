package pe.gob.mincetur.vuce.co.util;

public enum TransaccionType {
    N1 (1, "N1", "Registro de Orden"),
    N2 (2, "N2", "Confirmacion de Aceptacion de una Orden (con o sin tasa)"),
    N3 (3, "N3", "Envio de CDA a SUNAT"),
    N4 (4, "N4", "Envio de CDA a Entidad"),
    N5 (5, "N5", "Confirmacion por SUNAT de CDA pagado"),
    N6 (6, "N6", "Notificacion de pago de CDA a Entidad con numeracion SUCE"),
    N7 (7, "N7", "Notificacion de inicio de tramite, consignando numero de Expediente"),
    N8 (8, "N8", "Notificacion de resolucion de aprobacion de una SUCE, consignando Documento Autorizante"),
    N9 (9, "N9", "Notificacion de resolucion de denegacion de una SUCE"),
    N10 (10, "N10", "Envio estado de SUCE, ebxml y/o DA a SUNAT"),
    N11 (11, "N11", "Notificacion de subsanacion de la Orden"),
    N12 (12, "N12", "Solicitar anulacion de CDA a SUNAT"),
    N13 (13, "N13", "Recibir respuesta de anulacion de CDA por parte de la SUNAT a la VUCE Central"),
    N14 (14, "N14", "Envio de Modificacion Temporal de Orden (MTO) a la Entidad"),
    N15 (15, "N15", "Notificacion de rechazo de la MTO"),
    N16 (16, "N16", "Activacion de la MTO como orden vigente"), // YA NO SE UTILIZA
    N17 (17, "N17", "Notificacion de subsanacion de SUCE"),
    N18 (18, "N18", "Envio de Modificacion de una SUCE"),
    N19 (19, "N19", "Rechazo de Modificacion de una SUCE"),
    N20 (20, "N20", "Aceptacion de Modificacion de una SUCE"),
    N21 (21, "N21", "Aprobacion de Modificacion de ampliacion de plazo"),
    N22 (22, "N22", "Aprobacion de Modificacion de una SUCE"),
    N23 (23, "N23", "Notificacion de pago de CDA a Entidad por Modificacion de la SUCE"),
    N24 (24, "N24", "Solicitud de Rectificacion de un Documento Autorizante"),
    N25 (25, "N25", "Rechazo de la Rectificacion de un Documento Autorizante"),
    N26 (26, "N26", "Notificacion de pago de CDA a Entidad por Rectificacion de un Documento Autorizante"),
    N27 (27, "N27", "Aprobacion de Rectificacion de un Documento Autorizante"),
    N28 (28, "N28", "Envio del DA a SUNAT"),
    N29 (29, "N29", "Denegacion de Rectificacion de un Documento Autorizante"),
    N30 (30, "N30", "Aceptacion de Rectificacion de un Documento Autorizante"),
    N31 (31, "N31", "Notificacion Desistir Orden"),
    N32 (32, "N32", "Notificacion Desistir SUCE"),
    N33 (33, "N33", "Enviar Notificacion Ticket"),
    N34 (34, "N34", "Enviar Consulta Tecnica"),
    N35 (35, "N35", "Notificacion de Respuesta de Consulta Tecnica"),
    N36 (36, "N36", "Notificacion de Trazabilidad"),
    N37 (37, "N37", "Notificacion de Confirmacion de Lectura de Buzon"),
    N38 (38, "N38", "Notificacion de Expiracion de Orden"),
    N39 (39, "N39", "Notificacion de Cierre de generacion de DR"),
    N40 (40, "N40", "Envio de numero de DR en VUCE"),
    N41 (41, "N41", "Notificacion de registro en Buzon"),
    N42 (42, "N42", "Notificacion de Extorno de CDA y bloqueo de SUCE"),
    N43 (43, "N43", "Notificacion de pago de CDA extornado anteriormente"),
    N44 (44, "N44", "Envio de archivo de Liquidacion de Pagos"),                    // USO INTERNO
    N45 (45, "N45", "Envio de Sincronizacion de tablas"),                           // USO INTERNO
    N46 (46, "N46", "Aceptacion de Desestimiento de Orden"), // YA NO SE UTILIZA
    N47 (47, "N47", "Rechazo de Desestimiento de Orden"), // YA NO SE UTILIZA
    N48 (48, "N48", "Aceptacion de Desestimiento de SUCE"),
    N49 (49, "N49", "Rechazo de Desestimiento de SUCE"),
    N50 (50, "N50", "Notificacion de Anulacion de CDA (hacia SUNAT para solicitar ANULACION)"),
    N51 (51, "N51", "Notificacion de Anulacion de CDA (hacia la Entidad)"),
    N52 (52, "N52", "Notificacion de Expiracion de Modificacion de SUCE"),
    N53 (53, "N53", "Envio de CPB a Empresa"),
    N54 (54, "N54", "Solicitar numero de Expediente para iniciar tramite de SUCE"),
    N55 (55, "N55", "Solicitar liquidacion de pagos a SUNAT"), 
    N80 (80, "N80", "N�mero de Solicitud"), //20140825_JMC_Intercambio_Empresa
    N81 (81, "N81", "Errores en Recepci�n de Solicitud"),//20140825_JMC_Intercambio_Empresa
    N85 (85, "N85", "Envio de DR desde la Entidad para la Alianza Pacifico"),// JMC 02/12/2016 Alianza Pacifico
	N93 (93, "N93", "Env�o de Respuesta de uso del Certificado de Alianza [Entidad -> VUCE]");
    
    private Integer id;
    
    private String codigo;
    
    private String nombre;
    
    TransaccionType(Integer id, String codigo, String nombre) {
        this.id = id;
        this.codigo=codigo;
        this.nombre=nombre;
    }
    
    public Integer getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }
    
    public static TransaccionType getTransaccionTypeById(Integer id) {
        for (TransaccionType type : values()) {
            if (type.id.equals(id)) {
                return type;
            }
        }
        return null;
    }
    
    public static TransaccionType getTransaccionTypeByCodigo(String codigo) {
        for (TransaccionType type : values()) {
            if (type.codigo.equals(codigo)) {
                return type;
            }
        }
        return null;
    }
    
}
