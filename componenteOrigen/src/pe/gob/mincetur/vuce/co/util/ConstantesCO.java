package pe.gob.mincetur.vuce.co.util;

/**
*Objeto :	ConstantesCO
*Descripcion :	Clase que contiene las Constantes utilitarios.
*Fecha de Creacion :	
*Autor :	vuce - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		09/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ
*/
public class ConstantesCO {
	
	public static final String LISTA_ROLES_USUARIO = "LISTA_ROLES_USUARIO";

	public static final String MODULO_SELECCIONADO = "MODULO_SELECCIONADO";

	public static final String MODULO_CERTIFICADO_ORIGEN = "CO";
	public static final String MODULO_SOLICITUD_CALIFICACION_EXPORTADOR = "SCE";

    // Tipos de Envio en el Buzon
    public static final int BUZON_ENVIO_TIPO_USUARIO_ENTIDAD = 1;
    public static final int BUZON_ENVIO_TIPO_ENTIDAD_USUARIO = 2;
    public static final int BUZON_ENVIO_TIPO_ENTIDAD_ENTIDAD = 3;

    // Tipos de Documento
    public static final String TIPO_DOCUMENTO_TRANSMISION_DJ = "DJ";
    public static final String TIPO_DOCUMENTO_TRANSMISION_CERTIFICADO = "CER";

    // Constantes de Autenticacion
    public static final String AUT_TIPO_USUARIO_ORIGEN_SOL = "IT";
    public static final String AUT_TIPO_USUARIO_ORIGEN_EXT = "ET";
    public static final String AUT_TIPO_USUARIO_ORIGEN_DNI = "DN";

    public static final String AUT_TIPO_USUARIO_SOL_PRINCIPAL = "0";
    public static final String AUT_TIPO_USUARIO_SOL_SECUNDARIO = "1";
    public static final String AUT_TIPO_USUARIO_EXT_PRINCIPAL = "0";
    public static final String AUT_TIPO_USUARIO_EXT_SECUNDARIO = "5";

    public static final int TIPO_DOCUMENTO_RUC = 1;
    public static final int TIPO_DOCUMENTO_DNI = 2;
    public static final int TIPO_DOCUMENTO_CARNET_EXTRANJERIA = 3;
    public static final int TIPO_DOCUMENTO_EXTRANET = 4;

    public static final int USUARIO_TIPO_SOL = 1;
    public static final int USUARIO_TIPO_ENTIDAD = 2;
    public static final int USUARIO_TIPO_DNI = 3;

    // Usuario Tipo
    public static final int USUARIO_SOLICITANTE = 3;
    public static final int USUARIO_REPRESENTANTE = 2;
    public static final int USUARIO_USUARIO = 1;

    // Tipo Persona
    public static final String TIPO_PERSONA_NATURAL = "1";
    public static final String TIPO_PERSONA_JURIDICA ="2";

    public static final String OPCION_ORDEN = "O";
    public static final String OPCION_SUCE = "S";
    public static final String OPCION_DR = "D";
    public static final String OPCION_DR_ENTIDAD = "DE";
    public static final String OPCION_EXPEDIENTE_ENTIDAD = "EX";
    public static final String OPCION_DDJJ = "DJ";
    public static final String OPCION_CERTIFICADOS = "CER";

    // Opcion SI/NO
    public static final String OPCION_NO = "N";
    public static final String OPCION_SI = "S";

    //Recurso Origen
    public static final Integer AUDITORIA_RECURSO_ORIGEN_EXTRANET_SUNAT	= 1;
    public static final Integer AUDITORIA_RECURSO_ORIGEN_EXTRANET_MESA_AYUDA = 2;
    public static final Integer AUDITORIA_RECURSO_ORIGEN_USUARIO_IMPORTADOR_EXPORTADOR = 3;

    //public static final String PREF_DJ = "DJ";
    public static final String PREF_SC = "SC";
    public static final String PREF_CER = "CER";

    // Tipo de Documento Resolutivo (DR)
    public static final String TIPO_DR_APROBACION = "A";
    public static final String TIPO_DR_RECHAZO = "R";

    public static final String PREF_SOLICITUD = "SOLICITUD";
    public static final String PREF_FACTURA = "FACTURA";
    public static final String PREF_MERCANCIA = "MERCANCIA";
    public static final String PREF_DJ = "DJ";
    public static final String PREF_MATERIAL = "MATERIAL";

    //Certificado Origen
    public static final Integer CO_FORMATOID =151;
    public static final Integer CO_TUPAID =152;
    //Duplicado de Certificado Origen
    public static final Integer DCO_FORMATOID =154;
    public static final Integer DCO_TUPAID =154;
    //Anulacion de Certificado Origen
    public static final Integer ACO_FORMATOID =155;
    public static final Integer ACO_TUPAID =155;

    // Formatos de Certificados de Origen
    public static final String CO_SOLICITUD = "mct001";
    public static final String CO_DUPLICADO = "mct002";
    public static final String CO_REEMPLAZO = "mct003";
    public static final String CO_ANULACION = "mct004";
    public static final String CO_CALIFICACION_ANTICIPADA = "mct005";


    // B�squeda de M�dulo de Validaci�n de DJ
    public static final String DJ_FILTRO_CODIGO = "C";
    public static final String DJ_FILTRO_DENOMINACION = "D";
    public static final String DJ_FILTRO_SOLICITUD= "SC";

    public static final String DJ_ESTADO_BORRADOR = "P";
    public static final String DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION = "N";
    public static final String DJ_ESTADO_PENDIENTE_VALIDACION = "O";
    public static final String DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO = "Q";
    public static final String DJ_ESTADO_APROBADA = "A";
    public static final String DJ_ESTADO_PENDIENTE_RESPUESTA_ENTIDAD = "T";
    public static final String DJ_ESTADO_PENDIENTE_GENERACION_DR = "L";
    public static final String DJ_ESTADO_RECHAZADA = "R";

    // Codigos de acuerdos comerciales
    public static final Integer AC_SGPC = 1;
    public static final Integer AC_CAN = 2;
    public static final Integer AC_CUBA  = 3;
    public static final Integer AC_CHILE = 4;
    public static final Integer AC_MERCOSUR = 5;
    public static final Integer AC_MERCOSUR_INT = 5;
    public static final Integer AC_MEXICO = 6;
    public static final Integer AC_TLC_SI = 7;
    public static final Integer AC_CHINA = 8;
    public static final Integer AC_SGP_RUSIA = 9;
    public static final Integer AC_SGP_JAPON = 10;
    public static final Integer AC_SGP_NORUEGA = 11;
    public static final Integer AC_SGP_NZ = 12;
    public static final Integer AC_TLC_VENEZUELA = 13;
    public static final Integer AC_SGP_UE = 14;
    public static final Integer AC_TLC_JAPON = 15;
    public static final Integer AC_TLC_TAILANDIA = 16;
    public static final Integer AC_COREA = 17;
    public static final Integer AC_EFTA = 18;
    public static final Integer AC_TLC_UE = 19;
    public static final Integer AC_COSTARRICA = 20;
    public static final Integer AC_PANAMA = 21;
    public static final Integer AC_GUATEMALA = 22;
    public static final Integer AC_SGP_CANADA = 23;
    public static final Integer AC_SGP_TURQUIA = 24;
    public static final Integer AC_HONDURAS = 25;
    public static final Integer AC_ALIANZA_PACIFICO = 26;
    public static final Integer AC_SGP_AUS = 27;

    public static final String AC_PANAMA_OTROS = "64,88,52,95";
    public static final String AC_GUATEMALA_OTROS = "64,165,52,95";
    public static final String AC_COSTARRICA_OTROS = "64,165,88,95";
    public static final String AC_HONDURAS_OTROS = "165,52";

    // Codigos de paises
    public static final String COD_PAIS_PERU = "168";
    public static final String COD_PAIS_NORUEGA = "160";
    public static final String COD_PAIS_CANADA = "38";
    public static final String COD_PAIS_COLOMBIA = "47";

    public static final String TIPO_REPORTE_AVANCE = "avance.";
    public static final String TIPO_REPORTE_SUCE = "";

    public static final String TIPO_ROL_EXPORTADOR = "1";
    public static final String TIPO_ROL_PRODUCTOR_EXPORTADOR = "2";
    public static final String TIPO_ROL_PRODUCTOR = "3";
    public static final String TIPO_ROL_EXPORTADOR_APODERADO = "4";
    public static final String TIPO_ROL_ACOPIADOR_APODERADO = "5";
    public static final String TIPO_ROL_ACOPIADOR_EXPORTADOR = "6";

    // Constante para el tag personalizado de mostrar ayudas
    public static final String MOSTRAR_AYUDA_ATRIBUTO_MATERIAL_SI = "si";
    public static final String MOSTRAR_AYUDA_ATRIBUTO_MATERIAL_NO = "no";

    // Estados del CO
    public static final String CO_ESTADO_SUBSANACION = "S";
    public static final String CO_ESTADO_PENDIENTE_ENVIO_A_ENTIDAD = "P";

    // Para p�ginas destino de la Calificaci�n de Origen
    public static final String CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA = "M";
    public static final String CO_CALIFICACION_PAGINA_DESTINO_ANTICIPADA = "C";

    // Sustento de Calificacion de Mercancia
    public static final String CALIFICACION_ORIGEN_NUEVA = "1";
    public static final String CALIFICACION_ORIGEN_NUEVA_BASADA_EN_ANTERIOR = "2";
    public static final String CALIFICACION_ORIGEN_EXISTENTE = "3";
    
    public static final String ERROR_HASH_DUPLICADO = "java.sql.SQLException: ORA-20068:";
    
    //<!--20140825_JMC_Intercambio_Empresa-->    
    //Intercambio Electronico - Empresa
    public static final String TIPO_DOCUMENTO_TRANSMISION_NTX = "NTX";    
    public static final int TRANSMISION_ESTADO_VC_POR_PROCESAR = 15;
    public static final int TRANSMISION_ESTADO_VC_RECIBIDO_DE_EMPRESA = 12;
    public static final int TRANSMISION_ESTADO_VC_POR_ENVIAR_A_EMPRESA=9;
    public static final int TRANSMISION_ESTADO_VC_ENVIADO_A_EMPRESA=10;
    public static final int TRANSMISION_ESTADO_VC_PROCESADO_EMPRESA=13;
    
    // Tipos de Ajuntos
    public static final int ADJUNTO_TIPO_EBXML = 1;
    public static final int ADJUNTO_TIPO_XML = 2;
    public static final int ADJUNTO_TIPO_PDF = 3;
    public static final int ADJUNTO_TIPO_ZIP = 4;
    public static final String ADJUNTO_NOMBRE_EBXML = "formatoEBXML.xml";
    public static final String ADJUNTO_NOMBRE_XML = "mensaje.xml";
    public static final String ADJUNTO_NOMBRE_ADJUNTOS = "adjuntos.zip";

    // Tipos de Documento
    public static final String TIPO_DOCUMENTO_TRANSMISION_ORDEN = "O";
    public static final String TIPO_DOCUMENTO_TRANSMISION_MTO = "MTO";
    public static final String TIPO_DOCUMENTO_TRANSMISION_SUCE = "S";
    public static final String TIPO_DOCUMENTO_TRANSMISION_MODIFICACION_SUCE = "MOS";
    public static final String TIPO_DOCUMENTO_TRANSMISION_SOLICITUD_NUEVO_DR = "SND";
    public static final String TIPO_DOCUMENTO_TRANSMISION_DR = "DR";
    public static final String TIPO_DOCUMENTO_TRANSMISION_MODIFICACION_DR = "MDR";
    public static final String TIPO_DOCUMENTO_TRANSMISION_CDA = "CDA";
    public static final String TIPO_DOCUMENTO_TRANSMISION_EXPEDIENTE = "E";
    public static final String TIPO_DOCUMENTO_TRANSMISION_NOTIFICACION = "N";
    public static final String TIPO_DOCUMENTO_TRANSMISION_TICKET_NOTIFICACION = "T";
    public static final String TIPO_DOCUMENTO_TRANSMISION_CONSULTA_TECNICA = "CT";
    public static final String TIPO_DOCUMENTO_TRANSMISION_LIQUIDACION_PAGOS = "LP";
    public static final String TIPO_DOCUMENTO_TRANSMISION_SINCRONIZACION_TABLAS = "ST";
    
    // Tipo de Transmision
    public static final int TIPO_TRANSMISION_TRANSACCION = 1;
    public static final int TIPO_TRANSMISION_LIQUIDACION_PAGOS = 2;
    public static final int TIPO_TRANSMISION_SINCRONIZACION = 3;
    //public static final int TIPO_TRANSMISION_NOTIFICACION_EMPRESA = 4;    
    public static final int TIPO_TRANSMISION_NOTIFICACION_EMPRESA_ORIGEN = 5;
    
    
    public static final String NOMBRE_NOTIFICACION = "Notificacion";
    public static final String NOMBRE_ERROR_EMPRESA = "ErroresEmpresa";

    //Tipo Interoperabilidad Alianza Pacifico   // JMC 02/12/2016 Alianza Pacifico
    public static final String TIPO_DOCUMENTO_IOP_CERTIFICADO_ORIGEN = "CO"; //Certificado de Origen
    public static final String IOP_TIPO_MENSAJE_IOP_CERTIFICADO = "5";

    // Estados para IOP
    public static final int TRANSMISION_ESTADO_VC_POR_ENVIAR_A_IOP = 19;
    //public static final int TRANSMISION_ESTADO_VC_CONFIRMADO_EN_IOP = 20;
    public static final int TRANSMISION_ESTADO_VC_POR_ENVIAR_IOP_A_VE = 21;
    public static final int TRANSMISION_ESTADO_VC_POR_REGISTRAR_XML_IOP_A_BD = 22;
    public static final int TRANSMISION_ESTADO_VC_VALIDACION_PROCESADO_CON_ERRORES = 14;

    public static final int IOP_ENTIDAD_MINCETUR = 28;
    
    public static final String IOP_CODIGO_PAIS_PERU = "PE";
    public static final String IOP_ACK_RESPUESTA_CODIGO_ESTADO_OK="ROK";
    public static final String IOP_ACK_RESPUESTA_CODIGO_ESTADO_NOK="RNTOK";
    public static final String IOP_ACK_RESPUESTA_CODIGO_ESTADO_DECLARADO="DCL";
    
    //Constantes para el contenido del COD    
    public static final String IOP_ACUERDO_ALIANZA_PACIFICO = "1";
        

    
    // Claves de Entidades
    public static final int ENTIDAD_SUNAT_ORIGEN = 7;//Valor pendiente por definir

    
    public static final String FORMATO_PRIMERA_VERSION = "I";    
    //Ticket 149343 GCHAVEZ-09/07/2019
    public static final String CADENA_URL_PROHIBIDO = "#%&*{}\\:<>?/+";
    //Ticket 149343 GCHAVEZ-09/07/2019
}
