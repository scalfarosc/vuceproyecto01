package pe.gob.mincetur.vuce.co.util.file;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream.UnicodeExtraFieldPolicy;
import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.util.stream.StreamUtil;

public class ZipUtil {
	
    static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
    
    public static HashMap<String, byte []> decompressFile(InputStream is) throws FileNotFoundException, IOException, Exception {
        logger.debug("DES-COMPRIMIENDO");
        
        String fileName = "ZIP_FILE_"+(new Random()).nextLong();
        ZipInputStream zipInputStream = new ZipInputStream(is);
        ZipEntry entry = zipInputStream.getNextEntry();
        int indice = 0;
        HashMap<String, byte []> listaArchivos = new HashMap<String, byte []>();
        while (entry!=null) {
            String entryName = entry.getName();
            logger.debug("Procesando archivo interno: "+entryName);
            File newFile = new File(fileName+"_"+indice+"_"+entryName);
            
            String directory = newFile.getParent();
            if (directory==null) {
                if (newFile.isDirectory()) {
                    newFile.delete();
                    break;
                }
            }
            newFile.delete();
            
            byte [] bytes = StreamUtil.getInputStreamAsBytes(zipInputStream);
            
            zipInputStream.closeEntry();
            entry = zipInputStream.getNextEntry();
            
            listaArchivos.put(entryName, bytes);
            indice++;
        }
        zipInputStream.close();
        return listaArchivos;
    }
    
    public static byte [] compressFiles(HashMap<String, byte []> files) throws FileNotFoundException, IOException, Exception {
        if (files==null || files.size()==0) {
            return null;
        }
        String zipName = "ZIP_FILE_"+(new Random()).nextLong()+".zip";
        logger.debug("COMPRIMIENDO "+zipName);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipArchiveOutputStream zos = new ZipArchiveOutputStream(new BufferedOutputStream(baos));
        zos.setUseLanguageEncodingFlag(true);
        zos.setCreateUnicodeExtraFields(UnicodeExtraFieldPolicy.ALWAYS);
        
        Iterator<String> it = files.keySet().iterator();
        while (it.hasNext()) {
            String fileName = it.next();
            byte [] data = files.get(fileName);
            
            ZipArchiveEntry entry = new ZipArchiveEntry(fileName);
            entry.setSize(data.length);
            zos.putArchiveEntry(entry);
            zos.write(data);
            zos.closeArchiveEntry();
        }
        zos.flush();
        zos.close();
        return baos.toByteArray();
    }
    
}
