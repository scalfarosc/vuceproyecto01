package pe.gob.mincetur.vuce.co.util.stream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Util;

public class StreamUtil {
    
    private static Logger logger = Logger.getLogger(org.jlis.core.util.Constantes.LOGGER_UTIL);
    
    public static byte [] getFileAsBytes(File file) throws Exception {
        return getInputStreamAsBytes(new FileInputStream(file));
    }
    
    public static byte [] getInputStreamAsBytes(InputStream is) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int bytex = -1;
        try {
            while ((bytex=is.read())!=-1) {
                baos.write(bytex);
            }
        } catch (IOException e) {
            logger.error("ERROR al intentar procesar el InputStream.", e);
            throw e;
        }
        return baos.toByteArray();
    }
    
    public static File getBytesAsFile(byte [] bytes) throws Exception {
        if (bytes==null) return null;
        
        File file = null;
        FileOutputStream fos = null;
        try {
        	File directorio = ApplicationConfig.getApplicationConfig().getApplicationResource("ruta.temporales", "").getFile();
        	Random randomNumberGenerator = new Random(bytes.length);
        	file = new File(directorio, Util.getDate("yyyyMMddHHmmss")+randomNumberGenerator.nextLong()+"file_temp.tmp");
            //file = File.createTempFile(Util.getDate("yyyyMMddHHmmss"), "file_temp.tmp", ApplicationConfig.getApplicationConfig().getApplicationResource("ruta.temporales", "").getFile());
            fos = new FileOutputStream(file);
            fos.write(bytes);
        } finally {
            if (fos!=null) {
                fos.flush();
                fos.close();
            }
        }
        return file;
    }
    
    public static boolean isExtensionCorrecta(byte [] archivoByte, int tipoArchivo) throws Exception{
		
		switch(tipoArchivo){
			case 3:
				//El numero magico para PDF es "%PDF"
				if ((Character.toString((char)archivoByte[0])+Character.toString((char)archivoByte[1])+
						Character.toString((char)archivoByte[2])+Character.toString((char)archivoByte[3])).equals("%PDF"))
					return true;
				else
					return false;
			case 4:
				if ((Character.toString((char)archivoByte[0])+Character.toString((char)archivoByte[1])).equals("PK"))
					return true;
				else
					return false;
			case 2:
				if ((Character.toString((char)archivoByte[0])+Character.toString((char)archivoByte[1])+
						Character.toString((char)archivoByte[2])+Character.toString((char)archivoByte[3])+
						Character.toString((char)archivoByte[4])).equalsIgnoreCase("<?xml"))
					return true;
				else
					return false;
			default:
				return false;
		}
	}
    
}
