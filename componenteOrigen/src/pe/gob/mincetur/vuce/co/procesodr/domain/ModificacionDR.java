
package pe.gob.mincetur.vuce.co.procesodr.domain;

import java.util.Date;

public class ModificacionDR {
    private Long drId;
    private Integer sdr;
    private Integer modificacionDr;
    private Integer vcId;
    private String transmitido;
    private Integer tasaId;
    private String estadoRegistro;
    private Integer estadoTrazaId;
    private Date fechaRegistro;
    private Date fechaActualizacion;
    private String bloqueado;
    private String mensaje;
    private Integer mensajeId;
    private Long dr;
    private Long suce;
    private Integer tipoModificacionDR;
    
    public Long getDrId() {
        return drId;
    }
    public void setDrId(Long drId) {
        this.drId = drId;
    }
    public Integer getSdr() {
        return sdr;
    }
    public void setSdr(Integer sdr) {
        this.sdr = sdr;
    }
    public Integer getModificacionDr() {
        return modificacionDr;
    }
    public void setModificacionDr(Integer modificacionDrId) {
        this.modificacionDr = modificacionDrId;
    }
    public Integer getVcId() {
        return vcId;
    }
    public void setVcId(Integer vcId) {
        this.vcId = vcId;
    }
    public String getTransmitido() {
        return transmitido;
    }
    public void setTransmitido(String transmitido) {
        this.transmitido = transmitido;
    }
    public Integer getTasaId() {
        return tasaId;
    }
    public void setTasaId(Integer tasaId) {
        this.tasaId = tasaId;
    }
    public String getEstadoRegistro() {
        return estadoRegistro;
    }
    public void setEstadoRegistro(String estadoRegistro) {
        this.estadoRegistro = estadoRegistro;
    }
    public Integer getEstadoTrazaId() {
        return estadoTrazaId;
    }
    public void setEstadoTrazaId(Integer estadoTrazaId) {
        this.estadoTrazaId = estadoTrazaId;
    }
    public Date getFechaRegistro() {
        return fechaRegistro;
    }
    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }
    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }
    public String getBloqueado() {
        return bloqueado;
    }
    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }
    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    public Integer getMensajeId() {
        return mensajeId;
    }
    public void setMensajeId(Integer mensajeId) {
        this.mensajeId = mensajeId;
    }
    public Long getDr() {
        return dr;
    }
    public void setDr(Long dr) {
        this.dr = dr;
    }
	public Long getSuce() {
		return suce;
	}
	public void setSuce(Long suce) {
		this.suce = suce;
	}
	public Integer getTipoModificacionDR() {
		return tipoModificacionDR;
	}
	public void setTipoModificacionDR(Integer tipoModificacionDR) {
		this.tipoModificacionDR = tipoModificacionDR;
	}
    
}
