package pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen;

public class AdjuntoCertificadoOrigenDR {

    private Long suceId;

    private Long drId;

    private Integer sdr;

    private String nombreArchivo;

    private byte [] archivo;

    private Integer adjuntoTipo;

	public Long getSuceId() {
		return suceId;
	}

	public void setSuceId(Long suceId) {
		this.suceId = suceId;
	}

	public Long getDrId() {
		return drId;
	}

	public void setDrId(Long drId) {
		this.drId = drId;
	}

	public Integer getSdr() {
		return sdr;
	}

	public void setSdr(Integer sdr) {
		this.sdr = sdr;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public Integer getAdjuntoTipo() {
		return adjuntoTipo;
	}

	public void setAdjuntoTipo(Integer adjuntoTipo) {
		this.adjuntoTipo = adjuntoTipo;
	}

}
