package pe.gob.mincetur.vuce.co.procesodr.conversor;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.procesodr.conversor.XML2BeanDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.exception.TransmisionException;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.xml.SchemaValidationErrorHandler;

public abstract class EntidadXML2Bean implements XML2BeanDR{
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
	protected String nomArchEsquema;
	
	protected XPath xPath;
	protected String tipoDR;
	protected String categoriaDR;
	
	private static SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	private static HashMap<String, Schema> schemas = new HashMap<String, Schema>();
	
	public void setTipoDR(String tipoDR) {
		this.tipoDR = tipoDR;
	}

	public void setCategoriaDR(String categoriaDR) {
		this.categoriaDR = categoriaDR;
	}
	/*
	public DocResolutivoSecEBXML convertirDr(byte [] ebXMLDR) throws Exception {
		if (ebXMLDR==null) {
			throw new TransmisionException("ERROR: El ebXML del DR de la SUCE vino vacio");
		}
		return convertir(ebXMLDR);
	}
	
	public abstract DocResolutivoSecEBXML convertir(byte[] ebXMLDR) throws Exception;
	*/
	protected Document validarSchema(byte[] ebXMLDR) throws Exception {
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder builder = dbf.newDocumentBuilder();
        builder.setErrorHandler(new SchemaValidationErrorHandler());
        
        if (ebXMLDR==null) throw new Exception("No se encontro el documento EBXML");
        
        Document doc = builder.parse(new InputSource(new ByteArrayInputStream(ebXMLDR)));  
        Schema schema = schemas.get(nomArchEsquema);
        if (schema==null) {
            schema = factory.newSchema(ApplicationConfig.getApplicationConfig().getApplicationResource("ruta.schemas.entidades", nomArchEsquema).getFile());
            schemas.put(nomArchEsquema, schema);
        }
        
        //Schema schema = factory.newSchema(new File("D:\\workspace\\vuce\\WebContent\\WEB-INF\\resource\\schemas\\uncefact\\data\\standard\\" + nomArchEsquema));            
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(doc));
        
        logger.debug("El documento es valido");
        
        XPathFactory xpFactory = XPathFactory.newInstance();
        xPath = xpFactory.newXPath();            
		return doc;
	}
	
	protected void completarAdjuntos(DocResolutivoSecEBXML dr, Node nodo) throws Exception {
		NodeList drAdjuntos;
		drAdjuntos = (NodeList)xPath.evaluate("vuce:AttachedDocument", nodo, XPathConstants.NODESET);
		if(drAdjuntos != null) {
			for (int i=0; i < drAdjuntos.getLength(); i++) {
	        	Node drAdjunto = drAdjuntos.item(i);
	        	AdjuntoFormato adjunto = new AdjuntoFormato();
	        	adjunto.setNombre(xPath.evaluate("vram:AttachedName", drAdjunto, XPathConstants.STRING).toString());
	        	adjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
	        	//dr.addAdjunto(adjunto);
	        } 
		}
	}
	
	public void setNomArchEsquema(String nomArchEsquema) {
		this.nomArchEsquema = nomArchEsquema;
	}

	public byte [] convertir(DR dr) throws Exception {
		return null;
	}

}
