package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo;

import java.util.Date;

public class Acuerdo26FacturaIOPEBXML {

	private	Integer 	codId	;
	private	Integer 	secuenciaOrdenFactura	;
	private	Integer 	formatoEntidadDrId	;
	private	Integer 	secuenciaFactura	;
	private	String 	numero	;
	private	Date 	fecha	;
	private	String 	facturadoPorTercerPais	;
	private	String 	paisTercerOperador	;
	private	String 	nombreOperadorTercerPais	;
	private	String 	direccionOperadorTercerPais ;
	private	String 	numeroFacturaTercerPais	;
	private	Date 	fechaFacturaTercerPais	;
	public Integer getCodId() {
		return codId;
	}
	public void setCodId(Integer codId) {
		this.codId = codId;
	}
	public Integer getSecuenciaOrdenFactura() {
		return secuenciaOrdenFactura;
	}
	public void setSecuenciaOrdenFactura(Integer secuenciaOrdenFactura) {
		this.secuenciaOrdenFactura = secuenciaOrdenFactura;
	}
	public Integer getFormatoEntidadDrId() {
		return formatoEntidadDrId;
	}
	public void setFormatoEntidadDrId(Integer formatoEntidadDrId) {
		this.formatoEntidadDrId = formatoEntidadDrId;
	}
	public Integer getSecuenciaFactura() {
		return secuenciaFactura;
	}
	public void setSecuenciaFactura(Integer secuenciaFactura) {
		this.secuenciaFactura = secuenciaFactura;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getFacturadoPorTercerPais() {
		return facturadoPorTercerPais;
	}
	public void setFacturadoPorTercerPais(String facturadoPorTercerPais) {
		this.facturadoPorTercerPais = facturadoPorTercerPais;
	}
	public String getPaisTercerOperador() {
		return paisTercerOperador;
	}
	public void setPaisTercerOperador(String paisTercerOperador) {
		this.paisTercerOperador = paisTercerOperador;
	}
	public String getNombreOperadorTercerPais() {
		return nombreOperadorTercerPais;
	}
	public void setNombreOperadorTercerPais(String nombreOperadorTercerPais) {
		this.nombreOperadorTercerPais = nombreOperadorTercerPais;
	}
	public String getDireccionOperadorTercerPais() {
		return direccionOperadorTercerPais;
	}
	public void setDireccionOperadorTercerPais(String direccionOperadorTercerPais) {
		this.direccionOperadorTercerPais = direccionOperadorTercerPais;
	}
	public String getNumeroFacturaTercerPais() {
		return numeroFacturaTercerPais;
	}
	public void setNumeroFacturaTercerPais(String numeroFacturaTercerPais) {
		this.numeroFacturaTercerPais = numeroFacturaTercerPais;
	}
	public Date getFechaFacturaTercerPais() {
		return fechaFacturaTercerPais;
	}
	public void setFechaFacturaTercerPais(Date fechaFacturaTercerPais) {
		this.fechaFacturaTercerPais = fechaFacturaTercerPais;
	}
}
