package pe.gob.mincetur.vuce.co.procesodr.conversor;

import pe.gob.mincetur.vuce.co.procesodr.domain.DR;


public interface XML2BeanDR {

	public void setTipoDR(String tipoDR);

	public void setCategoriaDR(String categoriaDR);

	//public DocResolutivoSecEBXML convertirDr(byte [] ebXMLDR) throws Exception;
	
	//public DocResolutivoSecEBXML convertir(byte[] ebXMLDR) throws Exception;
	
	public void setNomArchEsquema(String nomArchEsquema);
	
	public byte [] convertir(DR dr) throws Exception;
}

