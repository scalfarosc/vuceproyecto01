package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;

public class Acuerdo26IOPEBXML extends DocResolutivoSecEBXML {
	
	private	Integer	codId;
	/*private	String 	dr;
	private	String 	drId;*/
	private	String 	sdr;
	private	String 	drIopId;
	private	String 	formato;
	private	String 	formatoEntidadDrId;
	private	String 	codVersion;
	private	String 	tipoSuscriptorCod;
	private	String 	acuerdoInternacionalId;
	private	String 	nombreAcuerdo;
	private	String 	acronimoAcuerdo;
	private	String 	paisExportador;
	private	String 	nombreExportador;
	private	String 	direccionExportador;
	private	String 	ciudadExportador;
	private	String 	registroFiscalExportador;
	private	String 	telefonoExportador;
	private	String 	faxExportador;
	private	String 	emailExportador;
	private	String 	urlExportador;
	private	String 	declaracionExportador;
	private	Date 	fechaFirmaExportador;
	private	String 	paisImportador;
	private	String 	nombreImportador;
	private	String 	direccionImportador;
	private	String 	ciudadImportador;
	private	String 	registroFiscalImportador;
	private	String 	telefonoImportador;
	private	String 	faxImportador;
	private	String 	emailImportador;
	private	String 	urlImportador;
	private	Integer cantidadProductor;
	private	String 	productor;
	private	String 	nombreProductor;
	private	String 	direccionProductor;
	private	String 	registroFiscalProductor;
	private	String 	telefonoProductor;
	private	String 	faxProductor;
	private	String 	emailProductor;
	private	String 	paisProductor;
	private	Integer cantidadFactura;
	private	Integer cantidadMercancia;
	private	String 	observacion;
	private	String 	ehId;
	private	String 	paisEh;
	private	String 	nombreEh;
	private	String 	direccionEh;
	private	String 	localidadEh;
	private	String 	ciudadEh;
	private	String 	telefonoEh;
	private	String 	faxEh;
	private	String 	emailEh;
	private	String 	urlEh;
	private	String 	nombreFirmaAutoridad;
	private	String 	codigoCertificado;
	private	Date 	fechaCertificado;
	private	String 	certificadoId;
	private	String 	vigente;
	private	String 	imprimeAnexo;
	private String  codigoCertificadoReemplazado;
	private String  tipoEmision;
	
	private List<Acuerdo26ProductorIOPEBXML> productores = new ArrayList<Acuerdo26ProductorIOPEBXML>();
	private List<Acuerdo26FacturaIOPEBXML> facturas = new ArrayList<Acuerdo26FacturaIOPEBXML>();	
	private List<Acuerdo26MercanciaIOPEBXML> mercancias = new ArrayList<Acuerdo26MercanciaIOPEBXML>();
	private List<Acuerdo26MercanciaProductorIOPEBXML> mercanciasPorProductor = new ArrayList<Acuerdo26MercanciaProductorIOPEBXML>();

	

	public Integer getCodId() {
		return codId;
	}
	public void setCodId(Integer codId) {
		this.codId = codId;
	}
	public String getSdr() {
		return sdr;
	}
	public void setSdr(String sdr) {
		this.sdr = sdr;
	}
	public String getDrIopId() {
		return drIopId;
	}
	public void setDrIopId(String drIopId) {
		this.drIopId = drIopId;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public String getFormatoEntidadDrId() {
		return formatoEntidadDrId;
	}
	public void setFormatoEntidadDrId(String formatoEntidadDrId) {
		this.formatoEntidadDrId = formatoEntidadDrId;
	}
	public String getCodVersion() {
		return codVersion;
	}
	public void setCodVersion(String codVersion) {
		this.codVersion = codVersion;
	}
	public String getTipoSuscriptorCod() {
		return tipoSuscriptorCod;
	}
	public void setTipoSuscriptorCod(String tipoSuscriptorCod) {
		this.tipoSuscriptorCod = tipoSuscriptorCod;
	}
	public String getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(String acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public String getNombreAcuerdo() {
		return nombreAcuerdo;
	}
	public void setNombreAcuerdo(String nombreAcuerdo) {
		this.nombreAcuerdo = nombreAcuerdo;
	}
	public String getAcronimoAcuerdo() {
		return acronimoAcuerdo;
	}
	public void setAcronimoAcuerdo(String acronimoAcuerdo) {
		this.acronimoAcuerdo = acronimoAcuerdo;
	}
	public String getPaisExportador() {
		return paisExportador;
	}
	public void setPaisExportador(String paisExportador) {
		this.paisExportador = paisExportador;
	}
	public String getNombreExportador() {
		return nombreExportador;
	}
	public void setNombreExportador(String nombreExportador) {
		this.nombreExportador = nombreExportador;
	}
	public String getDireccionExportador() {
		return direccionExportador;
	}
	public void setDireccionExportador(String direccionExportador) {
		this.direccionExportador = direccionExportador;
	}
	public String getCiudadExportador() {
		return ciudadExportador;
	}
	public void setCiudadExportador(String ciudadExportador) {
		this.ciudadExportador = ciudadExportador;
	}
	public String getRegistroFiscalExportador() {
		return registroFiscalExportador;
	}
	public void setRegistroFiscalExportador(String registroFiscalExportador) {
		this.registroFiscalExportador = registroFiscalExportador;
	}
	public String getTelefonoExportador() {
		return telefonoExportador;
	}
	public void setTelefonoExportador(String telefonoExportador) {
		this.telefonoExportador = telefonoExportador;
	}
	public String getFaxExportador() {
		return faxExportador;
	}
	public void setFaxExportador(String faxExportador) {
		this.faxExportador = faxExportador;
	}
	public String getEmailExportador() {
		return emailExportador;
	}
	public void setEmailExportador(String emailExportador) {
		this.emailExportador = emailExportador;
	}
	public String getUrlExportador() {
		return urlExportador;
	}
	public void setUrlExportador(String urlExportador) {
		this.urlExportador = urlExportador;
	}
	public String getDeclaracionExportador() {
		return declaracionExportador;
	}
	public void setDeclaracionExportador(String declaracionExportador) {
		this.declaracionExportador = declaracionExportador;
	}

	public Date getFechaFirmaExportador() {
		return fechaFirmaExportador;
	}
	public void setFechaFirmaExportador(Date fechaFirmaExportador) {
		this.fechaFirmaExportador = fechaFirmaExportador;
	}
	public String getPaisImportador() {
		return paisImportador;
	}
	public void setPaisImportador(String paisImportador) {
		this.paisImportador = paisImportador;
	}
	public String getNombreImportador() {
		return nombreImportador;
	}
	public void setNombreImportador(String nombreImportador) {
		this.nombreImportador = nombreImportador;
	}
	public String getDireccionImportador() {
		return direccionImportador;
	}
	public void setDireccionImportador(String direccionImportador) {
		this.direccionImportador = direccionImportador;
	}
	public String getCiudadImportador() {
		return ciudadImportador;
	}
	public void setCiudadImportador(String ciudadImportador) {
		this.ciudadImportador = ciudadImportador;
	}
	public String getRegistroFiscalImportador() {
		return registroFiscalImportador;
	}
	public void setRegistroFiscalImportador(String registroFiscalImportador) {
		this.registroFiscalImportador = registroFiscalImportador;
	}
	public String getTelefonoImportador() {
		return telefonoImportador;
	}
	public void setTelefonoImportador(String telefonoImportador) {
		this.telefonoImportador = telefonoImportador;
	}
	public String getFaxImportador() {
		return faxImportador;
	}
	public void setFaxImportador(String faxImportador) {
		this.faxImportador = faxImportador;
	}
	public String getEmailImportador() {
		return emailImportador;
	}
	public void setEmailImportador(String emailImportador) {
		this.emailImportador = emailImportador;
	}
	public String getUrlImportador() {
		return urlImportador;
	}
	public void setUrlImportador(String urlImportador) {
		this.urlImportador = urlImportador;
	}
	public Integer getCantidadProductor() {
		return cantidadProductor;
	}
	public void setCantidadProductor(Integer cantidadProductor) {
		this.cantidadProductor = cantidadProductor;
	}
	public String getProductor() {
		return productor;
	}
	public void setProductor(String productor) {
		this.productor = productor;
	}
	public String getNombreProductor() {
		return nombreProductor;
	}
	public void setNombreProductor(String nombreProductor) {
		this.nombreProductor = nombreProductor;
	}
	public String getDireccionProductor() {
		return direccionProductor;
	}
	public void setDireccionProductor(String direccionProductor) {
		this.direccionProductor = direccionProductor;
	}
	public String getRegistroFiscalProductor() {
		return registroFiscalProductor;
	}
	public void setRegistroFiscalProductor(String registroFiscalProductor) {
		this.registroFiscalProductor = registroFiscalProductor;
	}
	public String getTelefonoProductor() {
		return telefonoProductor;
	}
	public void setTelefonoProductor(String telefonoProductor) {
		this.telefonoProductor = telefonoProductor;
	}
	public String getFaxProductor() {
		return faxProductor;
	}
	public void setFaxProductor(String faxProductor) {
		this.faxProductor = faxProductor;
	}
	public String getEmailProductor() {
		return emailProductor;
	}
	public void setEmailProductor(String emailProductor) {
		this.emailProductor = emailProductor;
	}
	public String getPaisProductor() {
		return paisProductor;
	}
	public void setPaisProductor(String paisProductor) {
		this.paisProductor = paisProductor;
	}
	
	public Integer getCantidadFactura() {
		return cantidadFactura;
	}
	public void setCantidadFactura(Integer cantidadFactura) {
		this.cantidadFactura = cantidadFactura;
	}
	public Integer getCantidadMercancia() {
		return cantidadMercancia;
	}
	public void setCantidadMercancia(Integer cantidadMercancia) {
		this.cantidadMercancia = cantidadMercancia;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getEhId() {
		return ehId;
	}
	public void setEhId(String ehId) {
		this.ehId = ehId;
	}
	public String getPaisEh() {
		return paisEh;
	}
	public void setPaisEh(String paisEh) {
		this.paisEh = paisEh;
	}
	public String getNombreEh() {
		return nombreEh;
	}
	public void setNombreEh(String nombreEh) {
		this.nombreEh = nombreEh;
	}
	public String getDireccionEh() {
		return direccionEh;
	}
	public void setDireccionEh(String direccionEh) {
		this.direccionEh = direccionEh;
	}
	public String getLocalidadEh() {
		return localidadEh;
	}
	public void setLocalidadEh(String localidadEh) {
		this.localidadEh = localidadEh;
	}
	public String getCiudadEh() {
		return ciudadEh;
	}
	public void setCiudadEh(String ciudadEh) {
		this.ciudadEh = ciudadEh;
	}
	public String getTelefonoEh() {
		return telefonoEh;
	}
	public void setTelefonoEh(String telefonoEh) {
		this.telefonoEh = telefonoEh;
	}
	public String getFaxEh() {
		return faxEh;
	}
	public void setFaxEh(String faxEh) {
		this.faxEh = faxEh;
	}
	public String getEmailEh() {
		return emailEh;
	}
	public void setEmailEh(String emailEh) {
		this.emailEh = emailEh;
	}
	public String getUrlEh() {
		return urlEh;
	}
	public void setUrlEh(String urlEh) {
		this.urlEh = urlEh;
	}
	public String getNombreFirmaAutoridad() {
		return nombreFirmaAutoridad;
	}
	public void setNombreFirmaAutoridad(String nombreFirmaAutoridad) {
		this.nombreFirmaAutoridad = nombreFirmaAutoridad;
	}
	public String getCodigoCertificado() {
		return codigoCertificado;
	}
	public void setCodigoCertificado(String codigoCertificado) {
		this.codigoCertificado = codigoCertificado;
	}

	public Date getFechaCertificado() {
		return fechaCertificado;
	}
	public void setFechaCertificado(Date fechaCertificado) {
		this.fechaCertificado = fechaCertificado;
	}
	public String getCertificadoId() {
		return certificadoId;
	}
	public void setCertificadoId(String certificadoId) {
		this.certificadoId = certificadoId;
	}
	public String getVigente() {
		return vigente;
	}
	public void setVigente(String vigente) {
		this.vigente = vigente;
	}
	public String getImprimeAnexo() {
		return imprimeAnexo;
	}
	public void setImprimeAnexo(String imprimeAnexo) {
		this.imprimeAnexo = imprimeAnexo;
	}
	public List<Acuerdo26ProductorIOPEBXML> getProductores() {
		return productores;
	}
	public void setProductores(List<Acuerdo26ProductorIOPEBXML> productores) {
		this.productores = productores;
	}
	public List<Acuerdo26FacturaIOPEBXML> getFacturas() {
		return facturas;
	}
	public void setFacturas(List<Acuerdo26FacturaIOPEBXML> facturas) {
		this.facturas = facturas;
	}
	public List<Acuerdo26MercanciaIOPEBXML> getMercancias() {
		return mercancias;
	}
	public void setMercancias(List<Acuerdo26MercanciaIOPEBXML> mercancias) {
		this.mercancias = mercancias;
	}
	
	public boolean addProductor(Acuerdo26ProductorIOPEBXML acuerdo26ProductorIOPEBXML) {
		return productores.add(acuerdo26ProductorIOPEBXML);
	}
	
	public boolean addFactura(Acuerdo26FacturaIOPEBXML acuerdo26FacturaIOPEBXML) {
		return facturas.add(acuerdo26FacturaIOPEBXML);
	}

	public boolean addMercancia(Acuerdo26MercanciaIOPEBXML acuerdo26MercanciaIOPEBXML) {
		return mercancias.add(acuerdo26MercanciaIOPEBXML);
	}
	public String getCodigoCertificadoReemplazado() {
		return codigoCertificadoReemplazado;
	}
	public void setCodigoCertificadoReemplazado(String codigoCertificadoReemplazado) {
		this.codigoCertificadoReemplazado = codigoCertificadoReemplazado;
	}
	public String getTipoEmision() {
		return tipoEmision;
	}
	public void setTipoEmision(String tipoEmision) {
		this.tipoEmision = tipoEmision;
	}
	public List<Acuerdo26MercanciaProductorIOPEBXML> getMercanciasPorProductor() {
		return mercanciasPorProductor;
	}
	public void setMercanciasPorProductor(
			List<Acuerdo26MercanciaProductorIOPEBXML> mercanciasPorProductor) {
		this.mercanciasPorProductor = mercanciasPorProductor;
	}
	
	public boolean addMercanciaPorProductor(Acuerdo26MercanciaProductorIOPEBXML acuerdo26MercanciaProductorIOPEBXML) {
		return mercanciasPorProductor.add(acuerdo26MercanciaProductorIOPEBXML);
	}

}
