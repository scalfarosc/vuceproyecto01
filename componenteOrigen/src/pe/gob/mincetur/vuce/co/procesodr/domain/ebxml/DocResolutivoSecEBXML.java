package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml;

import java.util.ArrayList;
import java.util.List;

import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoEBXML;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;

public class DocResolutivoSecEBXML extends DocResolutivoEBXML {
	
	private Long formatoDrId;
	
	private Integer secuenciaDr;
	
	private String controlVigencia;
	
	private String controlUso;
	
	private String controlSaldo;
	
	private Long drBorradorId;

	//JMC 02/12/2016  Alianza Pacifico
	private Long drIOP;

	private Integer drIOPId;
	
	private List<AdjuntoFormato> adjuntos = new ArrayList<AdjuntoFormato>();

	public Long getFormatoDrId() {
		return formatoDrId;
	}

	public void setFormatoDrId(Long formatoDrId) {
		this.formatoDrId = formatoDrId;
	}

	public Integer getSecuenciaDr() {
		return secuenciaDr;
	}

	public void setSecuenciaDr(Integer secuenciaDr) {
		this.secuenciaDr = secuenciaDr;
	}

	public String getControlVigencia() {
		return controlVigencia;
	}

	public void setControlVigencia(String controlVigencia) {
		this.controlVigencia = controlVigencia;
	}

	public String getControlUso() {
		return controlUso;
	}

	public void setControlUso(String controlUso) {
		this.controlUso = controlUso;
	}

	public String getControlSaldo() {
		return controlSaldo;
	}

	public void setControlSaldo(String controlSaldo) {
		this.controlSaldo = controlSaldo;
	}

	public Long getDrBorradorId() {
		return drBorradorId;
	}

	public void setDrBorradorId(Long drBorradorId) {
		this.drBorradorId = drBorradorId;
	}

	public List<AdjuntoFormato> getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(List<AdjuntoFormato> adjuntos) {
		this.adjuntos = adjuntos;
	}

	public Long getDrIOP() {
		return drIOP;
	}

	public void setDrIOP(Long drIOP) {
		this.drIOP = drIOP;
	}

	public Integer getDrIOPId() {
		return drIOPId;
	}

	public void setDrIOPId(Integer drIOPId) {
		this.drIOPId = drIOPId;
	}
	
	

}
