package pe.gob.mincetur.vuce.co.procesodr.conversor;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.exception.TransmisionException;
import pe.gob.mincetur.vuce.co.util.xml.SchemaValidationErrorHandler;
import pe.gob.mincetur.vuce.co.util.xml.XMLUtil;

public class Mct001XML2Bean extends EntidadXML2Bean{

	private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
	
	public byte [] convertir(DR dr) throws Exception {
		/*
		ITP001Logic itp001Logic = (ITP001Logic)SpringContext.getApplicationContext().getBean("itp001Logic");
		HashMap<String, Object> drId = itp001Logic.getITP001DRById(dr.getDrId().intValue(), dr.getSecuenciaDr().intValue());
		Itp001DrEBXML itp001Obj = itp001Logic.getItp001DrByDrId(Long.valueOf(drId.get("DETALLE.FORMATODRID").toString()));
		itp001Obj.setSuce(dr.getSuce().intValue());
		List<ITP001CertificadoDrEBXML> listaCertificados = itp001Logic.getITP001CertificadoDRList(itp001Obj.getFormatoDrId());
		for (ITP001CertificadoDrEBXML certificado : listaCertificados) {
			List<ITP001ConocimientoEmbDrEBXML> listaConocimientoEmbarque = itp001Logic.getITP001ConocimientoEmbDRList(itp001Obj.getFormatoDrId(), certificado.getSecuenciaCertificado());
			certificado.setConocimientoEmbarque(listaConocimientoEmbarque);
			List<ITP001PlantaDrEBXML> listaPlantas = itp001Logic.getITP001PlantaDRList(itp001Obj.getFormatoDrId(), certificado.getSecuenciaCertificado());
			certificado.setPlanta(listaPlantas);
		}
		List<ITP001RegionMoluscoDrEBXML> listaRegionMolusco = itp001Logic.getITP001RegionMoluscoDRList(itp001Obj.getFormatoDrId());
		itp001Obj.setRegionMolusco(listaRegionMolusco);
		List<ITP001UnionEuropeaDrEBXML> listaUnionEuropea = itp001Logic.getITP001UnionEuropeaDRList(itp001Obj.getFormatoDrId());
		itp001Obj.setUnionEuropea(listaUnionEuropea);
		
		itp001Obj.setCertificado(listaCertificados);
		*/
		HashUtil<String, Object> objectsDR = new HashUtil<String, Object>();
		/*objectsDR.put("dr", itp001Obj);
		AdjuntoFormato ebxml = XMLUtil.generateEBXML(itp001Obj, objectsDR);		
		return ebxml.getArchivo();
		*/
		return null;
		
	}
	
}
