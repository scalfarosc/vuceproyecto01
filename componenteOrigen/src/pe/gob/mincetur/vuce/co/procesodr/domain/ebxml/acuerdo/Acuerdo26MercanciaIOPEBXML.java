package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo;

import java.math.BigDecimal;

public class Acuerdo26MercanciaIOPEBXML {

	private	Integer 	codId	;
	private	Integer 	secuenciaOrdenMercancia	;
	private	Integer 	secuenciaOrdenFactura	;
	private	Long 	formatoEntidadDrId	;
	private	Integer 	secuenciaMercancia	;
	private	Integer 	itemCoMercancia	;
	private	Integer 	partidaArancelaria	;
	private	String 	partidaSegunAcuerdo	;
	private	String 	partidaFormateada	;
	private	String 	descripcionMercancia	;
	private	BigDecimal 	pesoCantidad	;
	private	String 	unidadMedidaMercancia	;
	private	String 	valorContenidoRegional	;
	private	String 	normaReglaOrigen	;
	private	String 	otrosCriterios	;
	private	String 	calificacionUoId	;
	private	String 	criterioOrigenCertificado;
	public Integer getCodId() {
		return codId;
	}
	public void setCodId(Integer codId) {
		this.codId = codId;
	}
	public Integer getSecuenciaOrdenMercancia() {
		return secuenciaOrdenMercancia;
	}
	public void setSecuenciaOrdenMercancia(Integer secuenciaOrdenMercancia) {
		this.secuenciaOrdenMercancia = secuenciaOrdenMercancia;
	}
	public Integer getSecuenciaOrdenFactura() {
		return secuenciaOrdenFactura;
	}
	public void setSecuenciaOrdenFactura(Integer secuenciaOrdenFactura) {
		this.secuenciaOrdenFactura = secuenciaOrdenFactura;
	}
	public Long getFormatoEntidadDrId() {
		return formatoEntidadDrId;
	}
	public void setFormatoEntidadDrId(Long formatoEntidadDrId) {
		this.formatoEntidadDrId = formatoEntidadDrId;
	}
	public Integer getSecuenciaMercancia() {
		return secuenciaMercancia;
	}
	public void setSecuenciaMercancia(Integer secuenciaMercancia) {
		this.secuenciaMercancia = secuenciaMercancia;
	}

	public Integer getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Integer partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public String getPartidaSegunAcuerdo() {
		return partidaSegunAcuerdo;
	}
	public void setPartidaSegunAcuerdo(String partidaSegunAcuerdo) {
		this.partidaSegunAcuerdo = partidaSegunAcuerdo;
	}
	public String getPartidaFormateada() {
		return partidaFormateada;
	}
	public void setPartidaFormateada(String partidaFormateada) {
		this.partidaFormateada = partidaFormateada;
	}
	public String getDescripcionMercancia() {
		return descripcionMercancia;
	}
	public void setDescripcionMercancia(String descripcionMercancia) {
		this.descripcionMercancia = descripcionMercancia;
	}

	public BigDecimal getPesoCantidad() {
		return pesoCantidad;
	}
	public void setPesoCantidad(BigDecimal pesoCantidad) {
		this.pesoCantidad = pesoCantidad;
	}
	public String getUnidadMedidaMercancia() {
		return unidadMedidaMercancia;
	}
	public void setUnidadMedidaMercancia(String unidadMedidaMercancia) {
		this.unidadMedidaMercancia = unidadMedidaMercancia;
	}
	public String getValorContenidoRegional() {
		return valorContenidoRegional;
	}
	public void setValorContenidoRegional(String valorContenidoRegional) {
		this.valorContenidoRegional = valorContenidoRegional;
	}
	public String getNormaReglaOrigen() {
		return normaReglaOrigen;
	}
	public void setNormaReglaOrigen(String normaReglaOrigen) {
		this.normaReglaOrigen = normaReglaOrigen;
	}
	public String getOtrosCriterios() {
		return otrosCriterios;
	}
	public void setOtrosCriterios(String otrosCriterios) {
		this.otrosCriterios = otrosCriterios;
	}
	public String getCalificacionUoId() {
		return calificacionUoId;
	}
	public void setCalificacionUoId(String calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	public String getCriterioOrigenCertificado() {
		return criterioOrigenCertificado;
	}
	public void setCriterioOrigenCertificado(String criterioOrigenCertificado) {
		this.criterioOrigenCertificado = criterioOrigenCertificado;
	}
	public Integer getItemCoMercancia() {
		return itemCoMercancia;
	}
	public void setItemCoMercancia(Integer itemCoMercancia) {
		this.itemCoMercancia = itemCoMercancia;
	}

}
