package pe.gob.mincetur.vuce.co.procesodr.dao.ibatis;

import org.jlis.core.util.Util;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26FacturaIOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26IOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26MercanciaIOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26MercanciaProductorIOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26ProductorIOPEBXML;


public class Mct001Acuerdo26DAOImpl  extends SqlMapClientDaoSupport  implements Mct001Acuerdo26DAO{
	
	public DocResolutivoSecEBXML registrarIOP(DocResolutivoSecEBXML obj)throws Exception {
		Acuerdo26IOPEBXML acuerdo26IOPEBXML = (Acuerdo26IOPEBXML) obj;
		
		try {
			getSqlMapClientTemplate().queryForObject("acuerdo26.iop.registra", acuerdo26IOPEBXML);			
		    
		    for (Acuerdo26ProductorIOPEBXML prod : acuerdo26IOPEBXML.getProductores()) {
		    	prod.setCodId (Util.integerValueOf( acuerdo26IOPEBXML.getCodId()));
		        getSqlMapClientTemplate().queryForObject("acuerdo26.iop.productor.registra", prod);
		    }

		    for (Acuerdo26FacturaIOPEBXML factura : acuerdo26IOPEBXML.getFacturas()) {
		    	factura.setCodId(Util.integerValueOf( acuerdo26IOPEBXML.getCodId()));
		        getSqlMapClientTemplate().queryForObject("acuerdo26.iop.factura.registra", factura);
		    }
		    
		    for (Acuerdo26MercanciaIOPEBXML mercancia : acuerdo26IOPEBXML.getMercancias()) {
		    	mercancia.setCodId(Util.integerValueOf( acuerdo26IOPEBXML.getCodId()));
		        getSqlMapClientTemplate().queryForObject("acuerdo26.iop.mercancia.registra", mercancia);
		    }
		    
		    for (Acuerdo26MercanciaProductorIOPEBXML mercanciaProductor : acuerdo26IOPEBXML.getMercanciasPorProductor()) {
		    	mercanciaProductor.setCodId(Util.integerValueOf( acuerdo26IOPEBXML.getCodId()));
		        getSqlMapClientTemplate().queryForObject("acuerdo26.iop.mercanciaxproductor.registra", mercanciaProductor);
		    }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return acuerdo26IOPEBXML;		
	}
	
	public DocResolutivoSecEBXML registrarIOPUpdate(Integer codId, DocResolutivoSecEBXML obj)throws Exception {
		Acuerdo26IOPEBXML acuerdo26IOPEBXML = (Acuerdo26IOPEBXML) obj;
		
		try {		
		    
		    for (Acuerdo26MercanciaIOPEBXML mercancia : acuerdo26IOPEBXML.getMercancias()) {
		    	mercancia.setCodId(codId);
		        getSqlMapClientTemplate().queryForObject("acuerdo26.iop.mercancia.registra", mercancia);
		    }
		    
		    for (Acuerdo26MercanciaProductorIOPEBXML mercanciaProductor : acuerdo26IOPEBXML.getMercanciasPorProductor()) {
		    	mercanciaProductor.setCodId(codId);
		        getSqlMapClientTemplate().queryForObject("acuerdo26.iop.mercanciaxproductor.registra", mercanciaProductor);
		    }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return acuerdo26IOPEBXML;		
	}

}
