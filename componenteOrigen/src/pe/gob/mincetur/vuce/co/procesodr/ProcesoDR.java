package pe.gob.mincetur.vuce.co.procesodr;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;


public interface ProcesoDR {
	
	public DocResolutivoSecEBXML procesarIOPRecepcionVUCEs(Transmision transmision, byte[] ebXMLDR) throws Exception;
	
	//NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
	public DocResolutivoSecEBXML procesarIOPRecepcionVUCEsUpdate(Integer codId, byte[] ebXMLDR) throws Exception;

}
