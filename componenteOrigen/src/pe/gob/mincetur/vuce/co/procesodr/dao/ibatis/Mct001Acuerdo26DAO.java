package pe.gob.mincetur.vuce.co.procesodr.dao.ibatis;

import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;

public interface Mct001Acuerdo26DAO {

	public DocResolutivoSecEBXML registrarIOP(DocResolutivoSecEBXML obj)throws Exception;
	
	public DocResolutivoSecEBXML registrarIOPUpdate(Integer codId, DocResolutivoSecEBXML obj)throws Exception;
	
}
