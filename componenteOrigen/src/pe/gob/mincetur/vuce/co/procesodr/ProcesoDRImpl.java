package pe.gob.mincetur.vuce.co.procesodr;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.procesodr.conversor.Mct001Acuerdo26XML2Bean;
import pe.gob.mincetur.vuce.co.procesodr.dao.ibatis.Mct001Acuerdo26DAO;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;


public class ProcesoDRImpl implements ProcesoDR{

	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);	
	private Mct001Acuerdo26XML2Bean conversor;
	private Mct001Acuerdo26DAO almacenador;
			
	public void setConversor(Mct001Acuerdo26XML2Bean conversor) {
		this.conversor = conversor;
	}
	public void setAlmacenador(Mct001Acuerdo26DAO almacenador) {
		this.almacenador = almacenador;
	}

	/**
	 * Registra en BD el Certificado de Origen del Otro pais de la Alianza
	 */
	public DocResolutivoSecEBXML procesarIOPRecepcionVUCEs(
			Transmision transmision, byte[] ebXMLDR) throws Exception {

		DocResolutivoSecEBXML certificadoImpo = null;
		try {			
			DocResolutivoSecEBXML certificado = null;
			certificado =  conversor.convertirDr(ebXMLDR);
			certificadoImpo = almacenador.registrarIOP(certificado);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return certificadoImpo;
	}
	
	public DocResolutivoSecEBXML procesarIOPRecepcionVUCEsUpdate(Integer codId, byte[] ebXMLDR) throws Exception {

		DocResolutivoSecEBXML certificadoImpo = null;
		try {			
			DocResolutivoSecEBXML certificado = null;
			certificado =  conversor.convertirDr(ebXMLDR);
			
			certificadoImpo = almacenador.registrarIOPUpdate(codId, certificado);			
		} catch (Exception e) {
			logger.error("Ocurrio un error al intentar procesarIOPRecepcionVUCEsUpdate ", e);
		}
		
		return certificadoImpo;
	}

}
