package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo;

public class Acuerdo26ProductorIOPEBXML {
	
	private	Integer 	codId;
	private	Integer 	secuenciaOrdenProductor;
	private	Integer 	formatoEntidadDrId;
	private	Integer 	calificacionUoId;
	private	String 	secuenciaProductor;
	private	String 	productorExportador;
	private	String 	confidencialidad;
	private	String 	paisProductor;
	private	String 	nombreProductor;
	private	String 	direccionProductor;
	private	String 	ciudadProductor;
	private	String 	registroFiscalProductor;
	private	String 	telefonoProductor;
	private	String 	faxProductor;
	private	String 	emailProductor;
	private	String 	urlProductor;
	public Integer getCodId() {
		return codId;
	}
	public void setCodId(Integer codId) {
		this.codId = codId;
	}
	public Integer getSecuenciaOrdenProductor() {
		return secuenciaOrdenProductor;
	}
	public void setSecuenciaOrdenProductor(Integer secuenciaOrdenProductor) {
		this.secuenciaOrdenProductor = secuenciaOrdenProductor;
	}
	public Integer getFormatoEntidadDrId() {
		return formatoEntidadDrId;
	}
	public void setFormatoEntidadDrId(Integer formatoEntidadDrId) {
		this.formatoEntidadDrId = formatoEntidadDrId;
	}
	public Integer getCalificacionUoId() {
		return calificacionUoId;
	}
	public void setCalificacionUoId(Integer calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	public String getSecuenciaProductor() {
		return secuenciaProductor;
	}
	public void setSecuenciaProductor(String secuenciaProductor) {
		this.secuenciaProductor = secuenciaProductor;
	}
	public String getProductorExportador() {
		return productorExportador;
	}
	public void setProductorExportador(String productorExportador) {
		this.productorExportador = productorExportador;
	}
	public String getConfidencialidad() {
		return confidencialidad;
	}
	public void setConfidencialidad(String confidencialidad) {
		this.confidencialidad = confidencialidad;
	}
	public String getPaisProductor() {
		return paisProductor;
	}
	public void setPaisProductor(String paisProductor) {
		this.paisProductor = paisProductor;
	}
	public String getNombreProductor() {
		return nombreProductor;
	}
	public void setNombreProductor(String nombreProductor) {
		this.nombreProductor = nombreProductor;
	}
	public String getDireccionProductor() {
		return direccionProductor;
	}
	public void setDireccionProductor(String direccionProductor) {
		this.direccionProductor = direccionProductor;
	}
	public String getCiudadProductor() {
		return ciudadProductor;
	}
	public void setCiudadProductor(String ciudadProductor) {
		this.ciudadProductor = ciudadProductor;
	}
	public String getRegistroFiscalProductor() {
		return registroFiscalProductor;
	}
	public void setRegistroFiscalProductor(String registroFiscalProductor) {
		this.registroFiscalProductor = registroFiscalProductor;
	}
	public String getTelefonoProductor() {
		return telefonoProductor;
	}
	public void setTelefonoProductor(String telefonoProductor) {
		this.telefonoProductor = telefonoProductor;
	}
	public String getFaxProductor() {
		return faxProductor;
	}
	public void setFaxProductor(String faxProductor) {
		this.faxProductor = faxProductor;
	}
	public String getEmailProductor() {
		return emailProductor;
	}
	public void setEmailProductor(String emailProductor) {
		this.emailProductor = emailProductor;
	}
	public String getUrlProductor() {
		return urlProductor;
	}
	public void setUrlProductor(String urlProductor) {
		this.urlProductor = urlProductor;
	}

	



}
