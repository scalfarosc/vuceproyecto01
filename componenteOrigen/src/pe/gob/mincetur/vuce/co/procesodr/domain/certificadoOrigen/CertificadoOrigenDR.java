package pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen;

import java.util.Date;

import pe.gob.mincetur.vuce.co.domain.Orden;

/**
 * Clase de datos basicos del Certificado de Origen DR
 * @author Ricardo Montes
 */

public class CertificadoOrigenDR extends Orden {

	private Long coDrId;
	private Integer drId;
	private Integer sdr;
	private String tipoDr;
	private Integer acuerdoInternacionalId;
	private Integer paisIsoIdAcuerdoInt;
	private String importadorNombre;
	private String importadorDireccion;
	private String importadorCiudad;
	private Integer importadorPais;
	private Integer medioTransporte;
	private Long suceId;
	private String estadoRegistro;
	private String observacion;
	private String idUsuarioEvaluador;
	private Date fechaEmision;
	private Date fechaVigencia;
	private String observacionEvaluador;
	private String estadoSDR;

	private String expedienteEntidad;
	private String entidadNombre;
	private String sedeNombre;
	private String drEntidad;
	private Long drIdOrigen;
	private Date fechaDrEntidad;
	private String causalDuplicadoCo;
	private String sustentoAdicional;
	private String aceptacion;

	// Para el mct005dr
    private String djCodigo;
	private String djDenominacion;
	private String djCaracteristica;
	private Date djFechaInicioVigencia;
	private Date djFechaFinVigencia;
	private String djPartidaArancelaria;
	private String djNumeroDocumento;

	private String rectificaDr;

	private String strImportadorPais;
	private Date fechaPartidaTransporte; // Acuerdo China
	private String numeroTransporte; // Acuerdo China
	private String modoTransCarga; // 20140520_JFR: Se agrega por el PQT-06
	private String puertoCarga; // Acuerdo China
	private String modoTransDescarga; // 20140520_JFR: Se agrega por el PQT-06
	private String puertoDescarga; // Acuerdo China
	private String paisDescarga; // 20140520_JFR: Se agrega por el PQT-06
	private String incluyeProductor; // Acuerdo China
	private String emitidoPosteriori; // Acuerdo UE
	
	private String nroReferenciaCertificado; // 20140520_JRF: Se agrega por el paquete PQT-06
	private String nroReferenciaCertificadoOrigen; // 20160526_NPC: Se agrega por ticket 98945 

	private String usuarioFirmanteExp;
	private String usuarioFirmanteEev;
	private Date fechaFirmaEev;
	private String secuenciaFuncionario;

	private String numeroExpediente;
	private String dr;

	private String registroFiscalImportador;
	private String telefonoImportador;
	private String faxImportador;
	private String emailImportador;
	private String cargoEmpresaExportador; 	//20140611_JMCA PQT-08
	private String telefonoEmpresaExportador; //20140611_JMCA PQT-08

	private String disponible;

	private String esConfidencial;

	private String esRectificacion = "N";
	
	private String drEntidadNuevo; //<!-- 20140717_JMC NuevoReq - No se Visualiza N�mero de Certificado - Uni�n Europea-->
	private Integer entidadId;    //20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
	private Date fechaFirmaExportador; // 20150612_RPC: Acta.34:Firmas
	private String emitidoPosteriori2; // 20150612_RPC: Acta.34:Firmas

	public Long getCoDrId() {
		return coDrId;
	}
	public void setCoDrId(Long coDrId) {
		this.coDrId = coDrId;
	}
	public Integer getDrId() {
		return drId;
	}
	public void setDrId(Integer drId) {
		this.drId = drId;
	}
	public Integer getSdr() {
		return sdr;
	}
	public void setSdr(Integer sdr) {
		this.sdr = sdr;
	}
	public String getTipoDr() {
		return tipoDr;
	}
	public void setTipoDr(String tipoDr) {
		this.tipoDr = tipoDr;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public Integer getPaisIsoIdAcuerdoInt() {
		return paisIsoIdAcuerdoInt;
	}
	public void setPaisIsoIdAcuerdoInt(Integer paisIsoIdAcuerdoInt) {
		this.paisIsoIdAcuerdoInt = paisIsoIdAcuerdoInt;
	}
	public String getImportadorNombre() {
		return importadorNombre;
	}
	public void setImportadorNombre(String importadorNombre) {
		this.importadorNombre = importadorNombre;
	}
	public String getImportadorDireccion() {
		return importadorDireccion;
	}
	public void setImportadorDireccion(String importadorDireccion) {
		this.importadorDireccion = importadorDireccion;
	}
	public Integer getImportadorPais() {
		return importadorPais;
	}
	public void setImportadorPais(Integer importadorPais) {
		this.importadorPais = importadorPais;
	}
	public Integer getMedioTransporte() {
		return medioTransporte;
	}
	public void setMedioTransporte(Integer medioTransporte) {
		this.medioTransporte = medioTransporte;
	}
	public Long getSuceId() {
		return suceId;
	}
	public void setSuceId(Long suceId) {
		this.suceId = suceId;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getIdUsuarioEvaluador() {
		return idUsuarioEvaluador;
	}
	public void setIdUsuarioEvaluador(String idUsuarioEvaluador) {
		this.idUsuarioEvaluador = idUsuarioEvaluador;
	}
	public Date getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
	public String getObservacionEvaluador() {
		return observacionEvaluador;
	}
	public void setObservacionEvaluador(String observacionEvaluador) {
		this.observacionEvaluador = observacionEvaluador;
	}
	public String getEstadoSDR() {
		return estadoSDR;
	}
	public void setEstadoSDR(String estadoSDR) {
		this.estadoSDR = estadoSDR;
	}
	public String getExpedienteEntidad() {
		return expedienteEntidad;
	}
	public void setExpedienteEntidad(String expedienteEntidad) {
		this.expedienteEntidad = expedienteEntidad;
	}
	public String getEntidadNombre() {
		return entidadNombre;
	}
	public void setEntidadNombre(String entidadNombre) {
		this.entidadNombre = entidadNombre;
	}
	public String getSedeNombre() {
		return sedeNombre;
	}
	public void setSedeNombre(String sedeNombre) {
		this.sedeNombre = sedeNombre;
	}
	public String getDrEntidad() {
		return drEntidad;
	}
	public void setDrEntidad(String drEntidad) {
		this.drEntidad = drEntidad;
	}
	public Long getDrIdOrigen() {
		return drIdOrigen;
	}
	public void setDrIdOrigen(Long drIdOrigen) {
		this.drIdOrigen = drIdOrigen;
	}
	public Date getFechaDrEntidad() {
		return fechaDrEntidad;
	}
	public void setFechaDrEntidad(Date fechaDrEntidad) {
		this.fechaDrEntidad = fechaDrEntidad;
	}
	public String getCausalDuplicadoCo() {
		return causalDuplicadoCo;
	}
	public void setCausalDuplicadoCo(String causalDuplicadoCo) {
		this.causalDuplicadoCo = causalDuplicadoCo;
	}
	public String getSustentoAdicional() {
		return sustentoAdicional;
	}
	public void setSustentoAdicional(String sustentoAdicional) {
		this.sustentoAdicional = sustentoAdicional;
	}
	public String getAceptacion() {
		return aceptacion;
	}
	public void setAceptacion(String aceptacion) {
		this.aceptacion = aceptacion;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getDjCodigo() {
		return djCodigo;
	}
	public void setDjCodigo(String djCodigo) {
		this.djCodigo = djCodigo;
	}
	public String getDjDenominacion() {
		return djDenominacion;
	}
	public void setDjDenominacion(String djDenominacion) {
		this.djDenominacion = djDenominacion;
	}
	public String getDjCaracteristica() {
		return djCaracteristica;
	}
	public void setDjCaracteristica(String djCaracteristica) {
		this.djCaracteristica = djCaracteristica;
	}
	public Date getDjFechaInicioVigencia() {
		return djFechaInicioVigencia;
	}
	public void setDjFechaInicioVigencia(Date djFechaInicioVigencia) {
		this.djFechaInicioVigencia = djFechaInicioVigencia;
	}
	public Date getDjFechaFinVigencia() {
		return djFechaFinVigencia;
	}
	public void setDjFechaFinVigencia(Date djFechaFinVigencia) {
		this.djFechaFinVigencia = djFechaFinVigencia;
	}
	public String getRectificaDr() {
		return rectificaDr;
	}
	public void setRectificaDr(String rectificaDr) {
		this.rectificaDr = rectificaDr;
	}
	public Date getFechaPartidaTransporte() {
		return fechaPartidaTransporte;
	}
	public void setFechaPartidaTransporte(Date fechaPartidaTransporte) {
		this.fechaPartidaTransporte = fechaPartidaTransporte;
	}
	public String getNumeroTransporte() {
		return numeroTransporte;
	}
	public void setNumeroTransporte(String numeroTransporte) {
		this.numeroTransporte = numeroTransporte;
	}
	public String getIncluyeProductor() {
		return incluyeProductor;
	}
	public void setIncluyeProductor(String incluyeProductor) {
		this.incluyeProductor = incluyeProductor;
	}
	public String getEmitidoPosteriori() {
		return emitidoPosteriori;
	}
	public void setEmitidoPosteriori(String emitidoPosteriori) {
		this.emitidoPosteriori = emitidoPosteriori;
	}
	public String getStrImportadorPais() {
		return strImportadorPais;
	}
	public void setStrImportadorPais(String strImportadorPais) {
		this.strImportadorPais = strImportadorPais;
	}
	public String getPuertoCarga() {
		return puertoCarga;
	}
	public void setPuertoCarga(String puertoCarga) {
		this.puertoCarga = puertoCarga;
	}
	public String getPuertoDescarga() {
		return puertoDescarga;
	}
	public void setPuertoDescarga(String puertoDescarga) {
		this.puertoDescarga = puertoDescarga;
	}
	public String getUsuarioFirmanteExp() {
		return usuarioFirmanteExp;
	}
	public void setUsuarioFirmanteExp(String usuarioFirmanteExp) {
		this.usuarioFirmanteExp = usuarioFirmanteExp;
	}
	public String getUsuarioFirmanteEev() {
		return usuarioFirmanteEev;
	}
	public void setUsuarioFirmanteEev(String usuarioFirmanteEev) {
		this.usuarioFirmanteEev = usuarioFirmanteEev;
	}
	public Date getFechaFirmaEev() {
		return fechaFirmaEev;
	}
	public void setFechaFirmaEev(Date fechaFirmaEev) {
		this.fechaFirmaEev = fechaFirmaEev;
	}
	public String getSecuenciaFuncionario() {
		return secuenciaFuncionario;
	}
	public void setSecuenciaFuncionario(String secuenciaFuncionario) {
		this.secuenciaFuncionario = secuenciaFuncionario;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	public String getDr() {
		return dr;
	}
	public void setDr(String dr) {
		this.dr = dr;
	}
	public String getRegistroFiscalImportador() {
		return registroFiscalImportador;
	}
	public void setRegistroFiscalImportador(String registroFiscalImportador) {
		this.registroFiscalImportador = registroFiscalImportador;
	}
	public String getTelefonoImportador() {
		return telefonoImportador;
	}
	public void setTelefonoImportador(String telefonoImportador) {
		this.telefonoImportador = telefonoImportador;
	}
	public String getFaxImportador() {
		return faxImportador;
	}
	public void setFaxImportador(String faxImportador) {
		this.faxImportador = faxImportador;
	}
	public String getEmailImportador() {
		return emailImportador;
	}
	public void setEmailImportador(String emailImportador) {
		this.emailImportador = emailImportador;
	}
	public String getDisponible() {
		return disponible;
	}
	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}
	public String getEsConfidencial() {
		return esConfidencial;
	}
	public void setEsConfidencial(String esConfidencial) {
		this.esConfidencial = esConfidencial;
	}
	public String getEsRectificacion() {
		return esRectificacion;
	}
	public void setEsRectificacion(String esRectificacion) {
		this.esRectificacion = esRectificacion;
	}
	public String getDjPartidaArancelaria() {
		return djPartidaArancelaria;
	}
	public void setDjPartidaArancelaria(String djPartidaArancelaria) {
		this.djPartidaArancelaria = djPartidaArancelaria;
	}
	public String getDjNumeroDocumento() {
		return djNumeroDocumento;
	}
	public void setDjNumeroDocumento(String djNumeroDocumento) {
		this.djNumeroDocumento = djNumeroDocumento;
	}
	public String getModoTransCarga() {
		return modoTransCarga;
	}
	public void setModoTransCarga(String modoTransCarga) {
		this.modoTransCarga = modoTransCarga;
	}
	public String getModoTransDescarga() {
		return modoTransDescarga;
	}
	public void setModoTransDescarga(String modoTransDescarga) {
		this.modoTransDescarga = modoTransDescarga;
	}
	public String getPaisDescarga() {
		return paisDescarga;
	}
	public void setPaisDescarga(String paisDescarga) {
		this.paisDescarga = paisDescarga;
	}
	public String getNroReferenciaCertificado() {
		return nroReferenciaCertificado;
	}
	public void setNroReferenciaCertificado(String nroReferenciaCertificado) {
		this.nroReferenciaCertificado = nroReferenciaCertificado;
	}
	public String getCargoEmpresaExportador() {
		return cargoEmpresaExportador;
	}
	public void setCargoEmpresaExportador(String cargoEmpresaExportador) {
		this.cargoEmpresaExportador = cargoEmpresaExportador;
	}
	public String getTelefonoEmpresaExportador() {
		return telefonoEmpresaExportador;
	}
	public void setTelefonoEmpresaExportador(String telefonoEmpresaExportador) {
		this.telefonoEmpresaExportador = telefonoEmpresaExportador;
	}
	public String getDrEntidadNuevo() {
		return drEntidadNuevo;
	}
	public void setDrEntidadNuevo(String drEntidadNuevo) {
		this.drEntidadNuevo = drEntidadNuevo;
	}
	public Integer getEntidadId() {
		return entidadId;
	}
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}
	
	public Date getFechaFirmaExportador() {
		return fechaFirmaExportador;
	}
	public void setFechaFirmaExportador(Date fechaFirmaExportador) {
		this.fechaFirmaExportador = fechaFirmaExportador;
	}

	public String getEmitidoPosteriori2() {
		return emitidoPosteriori2;
	}
	public void setEmitidoPosteriori2(String emitidoPosteriori2) {
		this.emitidoPosteriori2 = emitidoPosteriori2;
	}
	public String getNroReferenciaCertificadoOrigen() {
		return nroReferenciaCertificadoOrigen;
	}
	public void setNroReferenciaCertificadoOrigen(
			String nroReferenciaCertificadoOrigen) {
		this.nroReferenciaCertificadoOrigen = nroReferenciaCertificadoOrigen;
	}
	public String getImportadorCiudad() {
		return importadorCiudad;
	}
	public void setImportadorCiudad(String importadorCiudad) {
		this.importadorCiudad = importadorCiudad;
	}
	
	
}
