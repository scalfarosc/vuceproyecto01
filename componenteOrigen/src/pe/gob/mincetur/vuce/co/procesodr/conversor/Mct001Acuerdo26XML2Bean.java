package pe.gob.mincetur.vuce.co.procesodr.conversor;

import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Iterator;


//import javax.xml.crypto.dsig.XMLSignature;
//import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathConstants;

import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.signature.XMLSignature;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.jlis.core.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26FacturaIOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26IOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26MercanciaIOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26MercanciaProductorIOPEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26ProductorIOPEBXML;


public class Mct001Acuerdo26XML2Bean extends EntidadIOPXML2Bean{

	@Override
	public DocResolutivoSecEBXML convertir(byte[] ebXMLDR) throws Exception {
		Acuerdo26IOPEBXML certificado = null;
		Document doc = super.validarSchema(ebXMLDR);
        xPath.setNamespaceContext(new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
            	if (prefix.equals("m4")) {
                	return "http://www.w3.org/2000/09/xmldsig#";
                }
                return "http://www.w3.org/2001/12/soap-envelope";
            }                
            public String getPrefix(String p) {
                return "tns";
            }
            public Iterator<Object> getPrefixes(String p) {
                return null;
            }
        });
		
        Node nodo = (Node)xPath.evaluate("/*", doc, XPathConstants.NODE);
        
        certificado = new Acuerdo26IOPEBXML();
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        
        //certificado.setNumeroCertificado(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter", nodo, XPathConstants.STRING).toString());
        certificado.setAcronimoAcuerdo(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/Agreement/AgreementAcronym", nodo, XPathConstants.STRING).toString());
        //certificado.setAcuerdoInternacionalId(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        certificado.setCantidadFactura(Util.integerValueOf(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Invoices/InvoiceQty", nodo, XPathConstants.STRING).toString()));
        certificado.setCantidadMercancia(Util.integerValueOf(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/GoodsList/GoodsQty", nodo, XPathConstants.STRING).toString()));
        certificado.setCantidadProductor(Util.integerValueOf(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/ProducerQty", nodo, XPathConstants.STRING).toString()));
        //certificado.setCertificadoId(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        //certificado.setCiudadEh(xPath.evaluate("/tns:CertOrigin/CODEH/EH/EHCountry", nodo, XPathConstants.STRING).toString());
        certificado.setCiudadExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterCity", nodo, XPathConstants.STRING).toString());
        certificado.setCiudadImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterCity", nodo, XPathConstants.STRING).toString());
        //certificado.setCodId(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        certificado.setCodigoCertificado(xPath.evaluate("/tns:CertOrigin/CODEH/CertificationEH/CertificateControlCode", nodo, XPathConstants.STRING).toString());
        certificado.setCertificadoId(xPath.evaluate("/tns:CertOrigin/CODEH/CertificationEH/CertificateID", nodo, XPathConstants.STRING).toString());
        certificado.setCodVersion(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/CODVer", nodo, XPathConstants.STRING).toString());
        certificado.setDeclaracionExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterDeclaration", nodo, XPathConstants.STRING).toString());
        certificado.setDireccionEh(xPath.evaluate("/tns:CertOrigin/CODEH/EH/EHAddress", nodo, XPathConstants.STRING).toString());
        certificado.setDireccionExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterAddress", nodo, XPathConstants.STRING).toString());
        certificado.setDireccionImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterAddress", nodo, XPathConstants.STRING).toString());
        certificado.setDireccionProductor(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/Producer/ProducerAddress", nodo, XPathConstants.STRING).toString());
        //certificado.setDr(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        //certificado.setDrIopId(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        certificado.setEhId(xPath.evaluate("/tns:CertOrigin/CODEH/EH/EHId", nodo, XPathConstants.STRING).toString());
        //certificado.setEmailEh(xPath.evaluate("", nodo, XPathConstants.STRING).toString());        
        certificado.setEmailExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterEmail", nodo, XPathConstants.STRING).toString());
        certificado.setEmailImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterEmail", nodo, XPathConstants.STRING).toString());
        certificado.setEmailProductor(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/Producer/ProducerEmail", nodo, XPathConstants.STRING).toString());
        //certificado.setFaxEh(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        certificado.setFaxExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterFax", nodo, XPathConstants.STRING).toString());
        certificado.setFaxImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterFax", nodo, XPathConstants.STRING).toString());
        

    	if (!"".equals(xPath.evaluate("/tns:CertOrigin/CODEH/CertificationEH/CertificateDate", nodo, XPathConstants.STRING).toString())){
    		certificado.setFechaCertificado(sdf.parse(xPath.evaluate("/tns:CertOrigin/CODEH/CertificationEH/CertificateDate", nodo, XPathConstants.STRING).toString()));
    	}
    	
    	if (!"".equals(xPath.evaluate("/tns:CertOrigin/CODEH/CertificationEH/CertificateDate", nodo, XPathConstants.STRING).toString())){
    		certificado.setFechaFirmaExportador(sdf.parse(xPath.evaluate("/tns:CertOrigin/CODEH/CertificationEH/CertificateDate", nodo, XPathConstants.STRING).toString()));
    	}
        //certificado.setFormato(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        //certificado.setFormatoEntidadDrId(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        //certificado.setLocalidadEh(xPath.evaluate("", nodo, XPathConstants.STRING).toString());

        certificado.setNombreAcuerdo(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/Agreement/AgreementName", nodo, XPathConstants.STRING).toString());
        certificado.setNombreEh(xPath.evaluate("/tns:CertOrigin/CODEH/EH/EHName", nodo, XPathConstants.STRING).toString());
        certificado.setNombreExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterBusinessName", nodo, XPathConstants.STRING).toString());

        certificado.setNombreImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterBusinessName", nodo, XPathConstants.STRING).toString());
        certificado.setNombreProductor(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/Producer/ProducerBusinessName", nodo, XPathConstants.STRING).toString());
        certificado.setObservacion(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/GeneralComments", nodo, XPathConstants.STRING).toString());
        certificado.setPaisEh(xPath.evaluate("/tns:CertOrigin/CODEH/EH/EHCountry", nodo, XPathConstants.STRING).toString());
        certificado.setPaisExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterCountry", nodo, XPathConstants.STRING).toString());
        certificado.setPaisImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterCountry", nodo, XPathConstants.STRING).toString());
        certificado.setRegistroFiscalExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterTaxIdentification", nodo, XPathConstants.STRING).toString());
        certificado.setRegistroFiscalImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterTaxIdentification", nodo, XPathConstants.STRING).toString());
        certificado.setRegistroFiscalProductor(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/Producer/ProducerTaxIdentification", nodo, XPathConstants.STRING).toString());
        //certificado.setTelefonoEh(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        certificado.setTelefonoExportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Exporter/ExporterTelephone", nodo, XPathConstants.STRING).toString());
        certificado.setTelefonoImportador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Importer/ImporterTelephone", nodo, XPathConstants.STRING).toString());
        certificado.setTelefonoProductor(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/Producer/ProducerTelephone", nodo, XPathConstants.STRING).toString());
        certificado.setTipoSuscriptorCod(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/CODSubmitterType", nodo, XPathConstants.STRING).toString());
        //certificado.setVigente(xPath.evaluate("", nodo, XPathConstants.STRING).toString());
        certificado.setTipoEmision(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/Issuance/IssuanceType", nodo, XPathConstants.STRING).toString());
        certificado.setCodigoCertificadoReemplazado(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/Issuance/IssuanceReplacedCertificateControlCode", nodo, XPathConstants.STRING).toString());
        
        NodeList productores = (NodeList)xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Producers/Producer", nodo, XPathConstants.NODESET);        
        for (int contProd=0; contProd < productores.getLength(); contProd++) {
        	Node productorNodo = productores.item(contProd);
        	Acuerdo26ProductorIOPEBXML productor = new Acuerdo26ProductorIOPEBXML();
        	productor.setCiudadProductor(xPath.evaluate("ProducerCity", productorNodo, XPathConstants.STRING).toString());
        	productor.setConfidencialidad(xPath.evaluate("ProducerConfidenciality", productorNodo, XPathConstants.STRING).toString());
        	productor.setDireccionProductor(xPath.evaluate("ProducerBusinessName", productorNodo, XPathConstants.STRING).toString());
        	productor.setEmailProductor(xPath.evaluate("ProducerEmail", productorNodo, XPathConstants.STRING).toString());
        	productor.setFaxProductor(xPath.evaluate("ProducerFax", productorNodo, XPathConstants.STRING).toString());
        	productor.setNombreProductor(xPath.evaluate("ProducerBusinessName", productorNodo, XPathConstants.STRING).toString());
        	productor.setProductorExportador(xPath.evaluate("ProducerExporter", productorNodo, XPathConstants.STRING).toString());
        	productor.setRegistroFiscalProductor(xPath.evaluate("ProducerTaxIdentification", productorNodo, XPathConstants.STRING).toString());
        	productor.setTelefonoProductor(xPath.evaluate("ProducerTelephone", productorNodo, XPathConstants.STRING).toString());
        	productor.setUrlProductor(xPath.evaluate("ProducerURL", productorNodo, XPathConstants.STRING).toString());        	        	
        	productor.setPaisProductor(xPath.evaluate("ProducerCountry", productorNodo, XPathConstants.STRING).toString());
        	certificado.addProductor(productor);
        }
        
        NodeList facturas = (NodeList)xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Invoices/Invoice", nodo, XPathConstants.NODESET);        
        for (int contFact=0; contFact < facturas.getLength(); contFact++) {
        	Node facturaNodo = facturas.item(contFact);
        	Acuerdo26FacturaIOPEBXML fact = new Acuerdo26FacturaIOPEBXML();
        	
        	fact.setSecuenciaOrdenFactura(Util.integerValueOf(xPath.evaluate("InvoiceOrderNo", facturaNodo, XPathConstants.STRING).toString()));
        	if (!"".equals(xPath.evaluate("InvoiceDate", facturaNodo, XPathConstants.STRING).toString())){
        		fact.setFecha(sdf.parse(xPath.evaluate("InvoiceDate", facturaNodo, XPathConstants.STRING).toString()));
        	}
        	fact.setNumero(xPath.evaluate("InvoiceNo", facturaNodo, XPathConstants.STRING).toString());
        	
        	certificado.addFactura(fact);
        }
        
        //Factura del Tercer Operador        
        if ( !"".equals((xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpStatement", nodo, XPathConstants.STRING).toString())) &&
        	 "true".equals((xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpStatement", nodo, XPathConstants.STRING).toString()))
        	) {
        	
        	Acuerdo26FacturaIOPEBXML tercerOperador = new Acuerdo26FacturaIOPEBXML();    
            tercerOperador.setDireccionOperadorTercerPais(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpAddress", nodo, XPathConstants.STRING).toString());
            tercerOperador.setFacturadoPorTercerPais(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpStatement", nodo, XPathConstants.STRING).toString());
            
            if (!"".equals(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpInvoiceDate", nodo, XPathConstants.STRING).toString())){
            	//tercerOperador.setFechaFacturaTercerPais(sdf.parse(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpInvoiceDate", nodo, XPathConstants.STRING).toString()));        	
            	tercerOperador.setFecha(sdf.parse(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpInvoiceDate", nodo, XPathConstants.STRING).toString()));
        	}
            tercerOperador.setNombreOperadorTercerPais(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpBusinessName", nodo, XPathConstants.STRING).toString());
            //tercerOperador.setNumeroFacturaTercerPais(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpInvoiceNo", nodo, XPathConstants.STRING).toString());
            tercerOperador.setNumero(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpInvoiceNo", nodo, XPathConstants.STRING).toString());
            tercerOperador.setPaisTercerOperador(xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/Comments/ThirdOpComments/ThirdOpCountry", nodo, XPathConstants.STRING).toString());
            
            certificado.addFactura(tercerOperador);        	
        }
        
        NodeList mercancias = (NodeList)xPath.evaluate("/tns:CertOrigin/CODEH/CODExporter/COD/FormP01/GoodsList/Goods", nodo, XPathConstants.NODESET);        
        
        for (int contMerc=0; contMerc < mercancias.getLength(); contMerc++) {
        	Node mercanciaNodo = mercancias.item(contMerc);
        	Acuerdo26MercanciaIOPEBXML mercancia = new Acuerdo26MercanciaIOPEBXML();
        
        	mercancia.setDescripcionMercancia(xPath.evaluate("GoodsItemName", mercanciaNodo, XPathConstants.STRING).toString());
        	mercancia.setNormaReglaOrigen(xPath.evaluate("GoodsItemOriginRules", mercanciaNodo, XPathConstants.STRING).toString());
        	mercancia.setOtrosCriterios(xPath.evaluate("GoodsItemOtherInstances", mercanciaNodo, XPathConstants.STRING).toString());
        	/*
            String[] partidaArancelaria = xPath.evaluate("GoodsItemCode", mercanciaNodo, XPathConstants.STRING).toString().split(".");            
            mercancia.setPartidaArancelaria(Util.integerValueOf(partidaArancelaria[0]+partidaArancelaria[1]));
            mercancia.setPartidaSegunAcuerdo(partidaArancelaria[0]+partidaArancelaria[1]);
            */
        	mercancia.setPartidaFormateada(xPath.evaluate("GoodsItemCode", mercanciaNodo, XPathConstants.STRING).toString());
        	mercancia.setPesoCantidad(Util.bigDecimalValueOf(xPath.evaluate("GoodsItemWeightAmount", mercanciaNodo, XPathConstants.STRING).toString()));
        	mercancia.setSecuenciaOrdenFactura(Util.integerValueOf(xPath.evaluate("GoodsInvoiceOrderNo", mercanciaNodo, XPathConstants.STRING).toString()));
        	mercancia.setSecuenciaOrdenMercancia(Util.integerValueOf(xPath.evaluate("GoodsOrderNo", mercanciaNodo, XPathConstants.STRING).toString()));
        	mercancia.setUnidadMedidaMercancia(xPath.evaluate("GoodsItemMeasureUnit", mercanciaNodo, XPathConstants.STRING).toString());
        	mercancia.setValorContenidoRegional(xPath.evaluate("GoodsItemRVC", mercanciaNodo, XPathConstants.STRING).toString());        	
        	
        	//Relaciona la Mercancia con el Productor        	
        	Acuerdo26MercanciaProductorIOPEBXML mercanciaPorProductor = new Acuerdo26MercanciaProductorIOPEBXML();        	
        	mercanciaPorProductor.setSecuenciaOrdenMercancia(Util.integerValueOf(xPath.evaluate("GoodsOrderNo", mercanciaNodo, XPathConstants.STRING).toString()));
        	mercanciaPorProductor.setSecuenciaOrdenProductor(Util.integerValueOf(xPath.evaluate("GoodsProducerOrderNo", mercanciaNodo, XPathConstants.STRING).toString()));
        	certificado.addMercanciaPorProductor(mercanciaPorProductor);
        	
        	
        	certificado.addMercancia(mercancia);
        }
        
        //Obtener el Nombre del Firmante
        org.apache.xml.security.Init.init();
        Element sigElement = (Element) doc.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature").item(1); //segunda firma
        XMLSignature signature = new XMLSignature(sigElement, "");
        KeyInfo keyInfo = signature.getKeyInfo();
        X509Certificate x509 = keyInfo.getX509Certificate();
        org.bouncycastle.asn1.x500.X500Name x500name = new JcaX509CertificateHolder(x509).getSubject();
        RDN cn = x500name.getRDNs(BCStyle.CN)[0];
        String cnName = IETFUtils.valueToString(cn.getFirst().getValue());

        certificado.setNombreFirmaAutoridad(cnName);
        
		return certificado;
	}

}
