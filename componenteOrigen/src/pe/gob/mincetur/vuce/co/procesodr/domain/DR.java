package pe.gob.mincetur.vuce.co.procesodr.domain;

public class DR {
	private Long drId;
	private Long dr;
	private Integer sdr;
	private Integer secuenciaDr;
	private Long suceId;
	private Long suce;
	private String tipoDr;
	private Integer categoriaDr;
	private String rectificacion;
	private String vigente;
	private Long drIdOrigen;
	private Integer sdrOrigen;
	private Long borradorDrId;
	private String anulado;
	private Integer version;
	private String esFraccionamiento;
	private String esReemplazo;
	private String drEntidad;// 20170120 JMC Alianza Pacifico


	public Long getSuceId() {
		return suceId;
	}

	public void setSuceId(Long suceId) {
		this.suceId = suceId;
	}

	public Long getSuce() {
		return suce;
	}

	public void setSuce(Long suce) {
		this.suce = suce;
	}

	public String getTipoDr() {
		return tipoDr;
	}

	public void setTipoDr(String tipoDr) {
		this.tipoDr = tipoDr;
	}

	public Integer getCategoriaDr() {
		return categoriaDr;
	}

	public void setCategoriaDr(Integer categoriaDr) {
		this.categoriaDr = categoriaDr;
	}

	public Long getDrId() {
		return drId;
	}

	public void setDrId(Long drId) {
		this.drId = drId;
	}

	public Integer getSecuenciaDr() {
		return secuenciaDr;
	}

	public void setSecuenciaDr(Integer secuenciaDr) {
		this.secuenciaDr = secuenciaDr;
	}

	public Long getDr() {
		return dr;
	}

	public void setDr(Long dr) {
		this.dr = dr;
	}

	public Long getBorradorDrId() {
		return borradorDrId;
	}

	public void setBorradorDrId(Long borradorDrId) {
		this.borradorDrId = borradorDrId;
	}

	public String getRectificacion() {
		return rectificacion;
	}

	public void setRectificacion(String rectificacion) {
		this.rectificacion = rectificacion;
	}

	public String getVigente() {
		return vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public Long getDrIdOrigen() {
		return drIdOrigen;
	}

	public void setDrIdOrigen(Long drIdOrigen) {
		this.drIdOrigen = drIdOrigen;
	}

	public Integer getSdrOrigen() {
		return sdrOrigen;
	}

	public void setSdrOrigen(Integer sdrOrigen) {
		this.sdrOrigen = sdrOrigen;
	}

	public String getAnulado() {
		return anulado;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public void setAnulado(String anulado) {
		this.anulado = anulado;
	}

	public String getEsFraccionamiento() {
		return esFraccionamiento;
	}

	public void setEsFraccionamiento(String esFraccionamiento) {
		this.esFraccionamiento = esFraccionamiento;
	}

	public String getEsReemplazo() {
		return esReemplazo;
	}

	public void setEsReemplazo(String esReemplazo) {
		this.esReemplazo = esReemplazo;
	}

	public Integer getSdr() {
		return sdr;
	}

	public void setSdr(Integer sdr) {
		this.sdr = sdr;
	}

	public String getDrEntidad() {
		return drEntidad;
	}

	public void setDrEntidad(String drEntidad) {
		this.drEntidad = drEntidad;
	}

}
