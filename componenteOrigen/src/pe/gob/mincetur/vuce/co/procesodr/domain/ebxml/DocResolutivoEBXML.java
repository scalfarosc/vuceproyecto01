package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml;

public class DocResolutivoEBXML {

	private Long drId;
	
	private Integer ano;
	
	private Long dr;
	
	private String tipo;
	
	private Integer categoriaDr;
	
	private String template;
	
	private Integer suce;
	
	private Integer anoSuce;

	// Atributos para interoperabilidad entre VUCEs - JMC 02/12/2016  Alianza Pacifico
	private String numeroCertificado;
	
	private String tipoDocumentoIOP;
	
	private String paisDestinoIOP;
	

	public Long getDrId() {
		return drId;
	}

	public void setDrId(Long drId) {
		this.drId = drId;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Long getDr() {
		return dr;
	}

	public void setDr(Long dr) {
		this.dr = dr;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getCategoriaDr() {
		return categoriaDr;
	}

	public void setCategoriaDr(Integer categoriaDr) {
		this.categoriaDr = categoriaDr;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Integer getSuce() {
		return suce;
	}

	public void setSuce(Integer suce) {
		this.suce = suce;
	}

	public Integer getAnoSuce() {
		return anoSuce;
	}

	public void setAnoSuce(Integer anoSuce) {
		this.anoSuce = anoSuce;
	}

	public String getNumeroCertificado() {
		return numeroCertificado;
	}

	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}

	public String getTipoDocumentoIOP() {
		return tipoDocumentoIOP;
	}

	public void setTipoDocumentoIOP(String tipoDocumentoIOP) {
		this.tipoDocumentoIOP = tipoDocumentoIOP;
	}

	public String getPaisDestinoIOP() {
		return paisDestinoIOP;
	}

	public void setPaisDestinoIOP(String paisDestinoIOP) {
		this.paisDestinoIOP = paisDestinoIOP;
	}
	
	
}
