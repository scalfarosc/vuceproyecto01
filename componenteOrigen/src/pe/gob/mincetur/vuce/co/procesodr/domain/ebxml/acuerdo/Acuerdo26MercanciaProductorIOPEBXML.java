package pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo;

public class Acuerdo26MercanciaProductorIOPEBXML {

	private	Integer 	codId	;
	private	Integer 	secuenciaOrdenMercancia	;
	private	Integer 	secuenciaOrdenProductor;
	
	public Integer getCodId() {
		return codId;
	}
	public void setCodId(Integer codId) {
		this.codId = codId;
	}
	public Integer getSecuenciaOrdenMercancia() {
		return secuenciaOrdenMercancia;
	}
	public void setSecuenciaOrdenMercancia(Integer secuenciaOrdenMercancia) {
		this.secuenciaOrdenMercancia = secuenciaOrdenMercancia;
	}
	public Integer getSecuenciaOrdenProductor() {
		return secuenciaOrdenProductor;
	}
	public void setSecuenciaOrdenProductor(Integer secuenciaOrdenProductor) {
		this.secuenciaOrdenProductor = secuenciaOrdenProductor;
	}
}
