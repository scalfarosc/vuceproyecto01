package pe.gob.mincetur.vuce.co.service;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;

import pe.gob.mincetur.vuce.co.dao.TransmisionDAO;
import pe.gob.mincetur.vuce.co.dao.ibatis.TransmisionPackDAOImpl;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.ContenidoRespuestaOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.EnvioRespuestaCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.ErrorDetalle;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;
import pe.gob.mincetur.vuce.co.logic.SuceLogic;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.TransmisionService;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionIOPVUCE;
import pe.gob.mincetur.vuce.co.service.resolutor.ResolutorService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.TransaccionType;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;


public class TransmisionPackServiceImpl implements TransmisionPackService{
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);	
	private ResolutorService resolutorService;
	private TransmisionPackDAOImpl transmisionPackDAO;
	private IbatisService ibatisService;
	private SuceLogic suceLogic;
	private TransmisionDAO transmisionDAO;
	
	public void setResolutorService(ResolutorService resolutorService) {
		this.resolutorService = resolutorService;
	}

	public void setTransmisionPackDAO(TransmisionPackDAOImpl transmisionPackDAO) {
		this.transmisionPackDAO = transmisionPackDAO;
	}

	public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public void setSuceLogic(SuceLogic suceLogic) {
		this.suceLogic = suceLogic;
	}
	
	public void setTransmisionDAO(TransmisionDAO transmisionDAO) {
		this.transmisionDAO = transmisionDAO;
	}

	// JMC 02/12/2016 Alianza Pacifico
	public DocResolutivoSecEBXML procesarTransmisionParaPack(String token, InteroperabilidadFirma iop, Integer secuenciaMaxima) throws Exception{
		genera_N85(token, iop, secuenciaMaxima);
		return null;
	}	
	
	// JMC 02/12/2016 Alianza Pacifico
	public DocResolutivoSecEBXML procesarIOPVUCEs(InteroperabilidadFirma iop) throws Exception {
		
		DocResolutivoSecEBXML docResol = null;
		DR dr = null;
        dr = resolutorService.getDRById(iop.getDrId(),iop.getSdr());
        return docResol;
		
	}
 
	// JMC 02/12/2016 Alianza Pacifico
	public DocResolutivoSecEBXML genera_N85(String token, InteroperabilidadFirma iop, Integer secuenciaMaxima) throws Exception {
		logger.debug("GENERA N85: "+token+"-"+secuenciaMaxima);
		DocResolutivoSecEBXML docResol = new DocResolutivoSecEBXML();
		DR dr = null;
        dr = resolutorService.getDRById(iop.getDrId(),iop.getSdr());    
        
        // Se invoca al SP para grabar el DR de Alianza Pacifico
        //docResol = transmisionPackDAO.registrarDrIOP(docResol, dr.getSuce());
        
        Integer vcId = transmisionPackDAO.generaN85(iop.getDrId(),iop.getSdr());        

    	HashUtil<String, Object> filterDatosEnvio = new HashUtil<String, Object>();
    	filterDatosEnvio.put("vcID", vcId);    	
    	HashUtil<String, Object> datosEnvio = ibatisService.loadElement("firma.obtenerDatosEnvioTceAlianza.firma", filterDatosEnvio);
    	
        // Creamos la transmision para el Pack
		TransaccionIOPVUCE nuevoMensaje = new TransaccionIOPVUCE();
		Suce suceObj = suceLogic.getSuceById(dr.getSuceId().intValue());		
        
    	nuevoMensaje.setTransaccion(TransaccionType.N85.getCodigo());
    	nuevoMensaje.setIdTransaccion(vcId);
    	nuevoMensaje.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_DR);
    	nuevoMensaje.setNumeroDocumento(dr.getDr().toString());
        nuevoMensaje.setTceId(suceObj.getTce());
        nuevoMensaje.setDrIOPId(docResol.getDrIOPId());
        nuevoMensaje.setTipoDocumentoReferencia(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_SUCE);
        nuevoMensaje.setNumeroDocumentoReferencia(String.valueOf(suceObj.getNumSuce()));
        
        nuevoMensaje.setCodigoAcuerdo(ConstantesCO.IOP_ACUERDO_ALIANZA_PACIFICO);
        nuevoMensaje.setTipoDocumentoAcuerdo(datosEnvio.getString("TIPO_DOCUMENTO"));
        nuevoMensaje.setCodigoPaisOrigen(datosEnvio.getString("PAIS_ORIGEN"));
        nuevoMensaje.setCodigoPaisDestino(datosEnvio.getString("PAIS_DESTINO"));
        nuevoMensaje.setFechaGeneracion(datosEnvio.getString("FECHA_GENERACION"));

        TransmisionService transmisionService = (TransmisionService)SpringContext.getApplicationContext().getBean("transmisionService");
        HashUtil<String, Object> filter = new HashUtil<String, Object>();        
        filter.put("entidad", ConstantesCO.IOP_ENTIDAD_MINCETUR);
        Integer idEntidad = ibatisService.loadElement("comun.entidadByCode", filter).getInt("id");
        
        //Transmision transmision = transmisionService.registrarTransmisionIOPSalienteParaPack(idEntidad, nuevoMensaje); // Se registra el N85 para enviar a Pack-VUCE
        
        Transmision transmision = transmisionDAO.loadTransmision(vcId);
        
        transmisionService.encolarMensajePack(token, secuenciaMaxima, transmision, nuevoMensaje, null, null);
        
        /*
		// Se registra en TCE_ALIANZA
		filter.put("tceAlianzaId", null);
		filter.put("tceId", suceObj.getTce());
		filter.put("vcId", transmision.getId());  
		filter.put("drIOPId", docResol.getDrIOPId());		
		ibatisService.executeSPWithObject("transmision.iop.registrarTCEAlianza", filter);		
		Integer tceAlianzaId = filter.getInt("tceAlianzaId");
		*/
        
		//Obtener Certificado
        filter.put("drId", iop.getDrId());
        filter.put("sdr", iop.getSdr());
    	CertificadoOrigen certificadoOrigen = (CertificadoOrigen)ibatisService.loadObject("certificado_origen.mct001.dr.datos.empresa", filter);
        
        
        // Se actualizan los datos del registro en TCE_ALIANZA para el N85
        filter.clear();
		filter.put("tceAlianzaId", nuevoMensaje.getTceAlianzaId());
		filter.put("paisDestino", certificadoOrigen.getPaisIsoCodigo());//notificacion.getCodigoPaisDestino()); //dr.getPaisDestinoIOP());
		filter.put("paisOrigen", ConstantesCO.IOP_CODIGO_PAIS_PERU);//notificacion.getCodigoPaisOrigen());//ConstantesVUCE.IOP_CODIGO_PAIS_PERU);
		filter.put("codigoDocumentoIOP", ConstantesCO.TIPO_DOCUMENTO_IOP_CERTIFICADO_ORIGEN); //notificacion.getTipoDocumentoAcuerdo()/*tipoDocumentoIOP*/);
		if (dr!=null) {
			filter.put("documentoIOPId", docResol.getDrIOPId());
			filter.put("numeroCertificado", dr.getDrEntidad());//20170120 JMC Alianza Pacifico
		}
		filter.put("tipoMensajeIOP", ConstantesCO.IOP_TIPO_MENSAJE_IOP_CERTIFICADO);
		
		ibatisService.executeSPWithObject("transmision.iop.actualizarTCEAlianza_01", filter);		
        //gestorProcesos.procesarMensaje(transmision, nuevoMensaje);
		
		// Se actualiza el estado de la transmision a "19" para que sea enviada a IOPack
		//TransmisionService transmisionService = (TransmisionService)SpringContext.getApplicationContext().getBean("transmisionService");
		Transmision transmisionEntidadEntrante = transmisionService.loadTransmision(transmision.getId().intValue());
		transmisionEntidadEntrante.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_IOP);
        transmisionService.updateEstadoTransmision(transmisionEntidadEntrante);
		      
        logger.debug("FINALIZA - GENERA N85: "+token+"-"+secuenciaMaxima);
		return docResol;
		
		
	}
	
	// JMC 02/12/2016 Alianza Pacifico
	public DocResolutivoSecEBXML procesarTransmisionRespuestaParaPack(Integer codId, String resultadoValidacion) throws Exception {
		
		DocResolutivoSecEBXML docResol = null;
		Integer vcId = transmisionPackDAO.generaTransmisionN93(codId);				
		genera_N93(codId, vcId, resultadoValidacion);
		
		return docResol;
	}	

	/**
	 * Genera la primera respuesta de Recibido en VUCE
	 */
	public DocResolutivoSecEBXML genera_N93(Integer codId, Integer vcId, String resultadoValidacion) throws Exception {
	
		DocResolutivoSecEBXML docResol = null;
		RespuestaCert respuesta = null;
		String mensajeDeErrores = "";
		String codigoEstado = null;
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("codId", codId);
	    //Obtener datos del error si no paso la validacion
		if(ConstantesCO.OPCION_NO.equals(resultadoValidacion)){			
	        
			List<ErrorDetalle> errorDetalles = (List<ErrorDetalle>)ibatisService.loadGenericList("respuesta_origen.obtenerErrores", filter);			
			mensajeDeErrores = errorDetalles.get(0).getMensajeError();			
			for (int i=1; i <= errorDetalles.size(); i++) {
				mensajeDeErrores = mensajeDeErrores + "|" + errorDetalles.get(i).getMensajeError() ;
			}
		}
		respuesta = (RespuestaCert)ibatisService.loadObject("respuesta_origen.obtenerPrimeraRespuesta", filter);
		codigoEstado = ConstantesCO.OPCION_NO.equals(resultadoValidacion)?ConstantesCO.IOP_ACK_RESPUESTA_CODIGO_ESTADO_NOK:ConstantesCO.IOP_ACK_RESPUESTA_CODIGO_ESTADO_OK;
		respuesta.setCodigoEstado(codigoEstado);		
		respuesta.setRazon(mensajeDeErrores);
		respuesta.setRemitente("PE");
		
		//Creamos el XML de Respuesta
		byte [] xmlRespuestaCertificado = generarRespuestaCertificado(codId, respuesta);
		
		//Creamos la transmision para el Pack
		
		HashUtil<String, Object> filterDatosEnvio = new HashUtil<String, Object>();
    	filterDatosEnvio.put("vcID", vcId);    	
    	HashUtil<String, Object> datosEnvio = ibatisService.loadElement("firma.obtenerDatosEnvioTceAlianza.firma", filterDatosEnvio);
    	
		TransaccionIOPVUCE nuevoMensaje = new TransaccionIOPVUCE();
    	nuevoMensaje.setTransaccion(TransaccionType.N93.getCodigo());
    	nuevoMensaje.setIdTransaccion(vcId);

        nuevoMensaje.setCodigoAcuerdo(ConstantesCO.IOP_ACUERDO_ALIANZA_PACIFICO);
        nuevoMensaje.setTipoDocumentoAcuerdo(datosEnvio.getString("TIPO_DOCUMENTO"));
        nuevoMensaje.setCodigoPaisOrigen(datosEnvio.getString("PAIS_ORIGEN"));
        nuevoMensaje.setCodigoPaisDestino(datosEnvio.getString("PAIS_DESTINO"));
        nuevoMensaje.setFechaGeneracion(datosEnvio.getString("FECHA_GENERACION"));	    	
        
        
        //Encolar la transmision para el Pack
        TransmisionService transmisionService = (TransmisionService)SpringContext.getApplicationContext().getBean("transmisionService");                
        filter.put("entidad", ConstantesCO.IOP_ENTIDAD_MINCETUR);
        Integer idEntidad = ibatisService.loadElement("comun.entidadByCode", filter).getInt("id");


        Transmision transmision = transmisionDAO.loadTransmision(vcId);        
        transmisionService.encolarMensajeRespuestaPack(transmision, nuevoMensaje, xmlRespuestaCertificado, null);
        
        
		// Se actualiza el estado de la transmision a "19" para que sea enviada a IOPack
		//TransmisionService transmisionService = (TransmisionService)SpringContext.getApplicationContext().getBean("transmisionService");
		Transmision transmisionEntidadEntrante = transmisionService.loadTransmision(transmision.getId().intValue());
		transmisionEntidadEntrante.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_IOP);
        transmisionService.updateEstadoTransmision(transmisionEntidadEntrante);
        
		return docResol;
	}
	
	public byte[] generarRespuestaCertificado(Integer codId, RespuestaCert respuestaCertificado) throws Exception {
		byte [] resultado = null;
		EnvioRespuestaCertificadoOrigen respuesta = obtenerInformacionRespuesta(codId,respuestaCertificado);
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
		cfg.setClassLoaderForTemplateLoading(this.getClass().getClassLoader(), "/");
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		
		Template temp = cfg.getTemplate("origin-respuesta-soap-request-template.xml");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Writer out = new BufferedWriter(new OutputStreamWriter(baos,"UTF-8"));

		temp.process(respuesta, out);    	
        resultado = baos.toByteArray();
        
        return resultado;
	}

    public EnvioRespuestaCertificadoOrigen obtenerInformacionRespuesta(Integer codId, RespuestaCert respuestaCertificado) throws Exception {
    	EnvioRespuestaCertificadoOrigen result = new EnvioRespuestaCertificadoOrigen();    	
    	ContenidoRespuestaOrigen contenidoRespuesta = new ContenidoRespuestaOrigen();

    	contenidoRespuesta.setRespuestaCertificado(respuestaCertificado);    	
    	result.setContenidoRespuesta(contenidoRespuesta);
    	
    	return result;
    	
    }
}
