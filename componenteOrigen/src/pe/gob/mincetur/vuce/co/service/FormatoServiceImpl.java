package pe.gob.mincetur.vuce.co.service;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.AdjuntoDAO;
import pe.gob.mincetur.vuce.co.dao.FormatoDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.AdjuntoRequerido;


public class FormatoServiceImpl implements FormatoService {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

    private FormatoDAO formatoDAO;

    private AdjuntoDAO adjuntoDAO;

    private AdjuntoService adjuntoService;

	public void setAdjuntoDAO(AdjuntoDAO adjuntoDAO) {
		this.adjuntoDAO = adjuntoDAO;
	}

	public void setFormatoDAO(FormatoDAO formatoDAO) {
        this.formatoDAO = formatoDAO;
    }

	public void setAdjuntoService(AdjuntoService adjuntoService) {
		this.adjuntoService = adjuntoService;
	}

	 public HashMap<String, String> loadUbigeo(HashUtil<String, Object> filter) throws Exception{
		 return formatoDAO.loadUbigeo(filter);
	 }

	 public HashMap<String, String> loadUbigeoByDistrito(HashUtil<String, Object> filter) throws Exception{
		 return formatoDAO.loadUbigeoByDistrito(filter);
	 }

	 public Solicitante loadSolicitanteConEmpresa(HashUtil<String, Object> filter)throws Exception{
	      return formatoDAO.loadSolicitanteConEmpresa(filter);
     }

	 public Solicitante loadSolicitanteSinEmpresa(HashUtil<String, Object> filter)throws Exception{
	      return formatoDAO.loadSolicitanteSinEmpresa(filter);
     }

	 public String loadNombreUsuario(HashUtil<String, Object> filter) throws Exception {
	      return formatoDAO.loadNombreUsuario(filter);
	 }

	 public String loadPartidaArancelaria(HashUtil<String, Object> filter) throws Exception{
		 return formatoDAO.loadPartidaArancelaria(filter);
	 }

	 public Formato loadFormatoByTupa(HashUtil<String, Object> filter) throws Exception {
		 return formatoDAO.loadFormatoByTupa(filter);
	 }

	 public Solicitante loadUsuarioFormato(HashUtil<String, Object> filter)throws Exception{
	    	return formatoDAO.loadUsuarioFormato(filter);
	 }

	 public Solicitante loadSolicitanteFormato(HashUtil<String, Object> filter)throws Exception{
	    	return formatoDAO.loadSolicitanteFormato(filter);
	 }

	 public RepresentanteLegal loadRepresentanteFormato(HashUtil<String, Object> filter)throws Exception{
	    	return formatoDAO.loadRepresentanteFormato(filter);
	 }

	public JasperPrint ejecutarReporteConConexionBD(JasperReport report, Map parameters) throws Exception {
		return formatoDAO.ejecutarReporteConConexionBD(report, parameters);
	}

    public AdjuntoRequerido loadRequerido(HashUtil<String, Object> filter) throws Exception {
        return formatoDAO.loadRequeridoSolicitudCertificadoOrigen(filter);
    }

	public String obtenerAyuda(Map<String, Object> filtros) throws Exception {
		return formatoDAO.obtenerAyuda(filtros);
	}

	public String permiteCrearFactura(Long ordenId, Long codigoCertificado) throws Exception {
		return formatoDAO.permiteCrearFactura(ordenId, codigoCertificado);
	}

	public String permiteCrearProductor(Long calificacionUoId) throws Exception {
		return formatoDAO.permiteCrearProductor(calificacionUoId);
	}

	public String permiteConfirmarFinEval(Long coId, Long ordenId, Integer mto) throws Exception {
		return formatoDAO.permiteConfirmarFinEval(coId, ordenId, mto);
	}

    public Long getFormatoDrId(HashUtil<String, Object> filter) throws Exception{
    	return formatoDAO.getFormatoDrId(filter);
    }

	public int cuentaAdjuntosRequeridos(HashUtil<String, Object> filter) throws Exception{
		return formatoDAO.cuentaAdjuntosRequeridos(filter);
	}

	public int cuentaNotificacionesPendientes(HashUtil<String, Object> filter) throws Exception{
		return formatoDAO.cuentaNotificacionesPendientes(filter);
	}

	public int cuentaAdjuntosRequeridosDJ(HashUtil<String, Object> filter) throws Exception{
		return formatoDAO.cuentaAdjuntosRequeridosDJ(filter);
	}

	public int cuentaNotificacionesSolicitudPendientes(HashUtil<String, Object> filter) throws Exception{
		return formatoDAO.cuentaNotificacionesSolicitudPendientes(filter);
	}

	public String permiteCrearSolicitudRectif(Long suceId) throws Exception{
		return formatoDAO.permiteCrearSolicitudRectif(suceId);
	}

	public String permiteApoderamientoXFrmt(Integer acuerdoInternacionalId) throws Exception{
		return formatoDAO.permiteApoderamientoXFrmt(acuerdoInternacionalId);
	}

	public int cuentaAdjuntosFacturaRequeridos(HashUtil<String, Object> filter) throws Exception{
		return formatoDAO.cuentaAdjuntosFacturaRequeridos(filter);
	}

	public String acuerdoConValidacionProductor(Integer codigoAcuerdo) throws Exception {
		return formatoDAO.acuerdoConValidacionProductor(codigoAcuerdo);
	}

	public void validacionPreTransmision(HashUtil<String, Object> filter) throws Exception {
		this.formatoDAO.validacionPreTransmision(filter);
	}

    public void validacionPreFormato(HashUtil<String, Object> filter) throws Exception { //20140526 - JMC - BUG 1.PersonaNatural
    	formatoDAO.validacionPreFormato(filter);
    }
    
	public String es_version_antigua(HashUtil<String, Object> filter) throws Exception {
		return formatoDAO.es_version_antigua(filter);
	}
	
	public String es_dj_version_diferente(HashUtil<String, Object> filter) throws Exception {
		return formatoDAO.es_dj_version_diferente(filter);
	}
	
	public String es_reemplazo_version_diferente(HashUtil<String, Object> filter) throws Exception {
		return formatoDAO.es_reemplazo_version_diferente(filter);
	}

}
