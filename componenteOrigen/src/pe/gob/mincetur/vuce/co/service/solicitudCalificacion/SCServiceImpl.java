package pe.gob.mincetur.vuce.co.service.solicitudCalificacion;

import java.util.List;
import java.util.Map;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.solicitudCalificacion.SCDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;

import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.AdjuntoRequeridoSC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCAcuerdo;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCDetalleDj;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCMaterial;

public class SCServiceImpl implements SCService{
	
	private static SCService instance;
	
	private SCDAO scDAO;
	
	public SCServiceImpl() {
		instance = this;
	}

	public static SCService getInstance() {
		return instance;
	}

	public static void setInstance(SCService instance) {
		SCServiceImpl.instance = instance;
	}

	public SCDAO getScDAO() {
		return scDAO;
	}

	public void setScDAO(SCDAO scDAO) {
		this.scDAO = scDAO;
	}

	public HashUtil<String, Object> insertSC(HashUtil<String, Object> filter) throws Exception {
        return scDAO.insertSC(filter);
    }

    public void insertUsuarioSC(HashUtil<String, Object> filter) throws Exception {
        scDAO.insertUsuarioSC(filter);
    }
	
    public void transmiteSC(HashUtil<String, Object> filter) throws Exception {
        scDAO.transmiteSC(filter);
    }
    
	public void updateSC(SC sc) throws Exception {
		scDAO.updateSC(sc);
	}
    
	public SC getSCById(HashUtil<String, Object> filter) throws Exception {
		return scDAO.getSCById(filter);
	}
	
	public SCMaterial getSCMaterialById(HashUtil<String, Object> filter) throws Exception {
		return scDAO.getSCMaterialById(filter);
	}

	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter)throws Exception{
		return scDAO.getUsuarioFormatoById(filter);
	}
	
	public Solicitud getSolicitudById(HashUtil<String, Object> filter)throws Exception{
		return scDAO.getSolicitudById(filter);
	}
	
	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter)throws Exception{
		return scDAO.getSolicitanteDetail(filter);
	}

	/*public void updateMaterial(SCMaterial obj) throws Exception{
		scDAO.updateMaterial( obj);
	}*/
    public void updateRepresentante(HashUtil<String, Object> filter) throws Exception {
        scDAO.updateRepresentante(filter);
    }
    
    public void insertAcuerdoPais(HashUtil<String, Object> filter) throws Exception {
    	scDAO.insertAcuerdoPais(filter);
    }
    
    public void deleteAcuerdoPais(HashUtil<String, Object> filter) throws Exception {
    	scDAO.deleteAcuerdoPais(filter);
    }
    
	public List<SCMaterial> getMaterialList(HashUtil<String, Object> filter) throws Exception{
		return scDAO.getMaterialList(filter);
	}
	
    public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception {
        scDAO.updateCargoDeclarante(filter);
    }
    
    public AdjuntoRequeridoSC getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception {
        return scDAO.getAdjuntoRequeridoById(filter);
    }

	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception{
		return scDAO.getAdjuntoRequeridoCount(filter);
	}
	
	
    // RESOLUTOR
	public void asignarEvaluador(Long ordenId, Integer usuarioId, Integer rol) throws Exception {
		scDAO.asignarEvaluador(ordenId, usuarioId, rol);
	}
	
	public void iniciarEvaluacionCalificacion(Long ordenId) throws Exception {
		scDAO.iniciarEvaluacionCalificacion(ordenId);
	}
	
    public SC insertBorradorResolutivoDr(SC sc) throws Exception {
    	return scDAO.insertBorradorResolutivoDr(sc);
    }
    
    public void deleteBorradorResolutivoDr(SC sc) throws Exception {
    	scDAO.deleteBorradorResolutivoDr(sc);
    }
    
	public Solicitante getSolicitanteDRDetail(HashUtil<String, Object> filter)throws Exception{
		return scDAO.getSolicitanteDRDetail(filter);
	}
	
	public Solicitante getSolicitanteDRBorradorDetail(HashUtil<String, Object> filter)throws Exception{
		return scDAO.getSolicitanteDRBorradorDetail(filter);
	}
	
	public SC getSCDRById(HashUtil<String, Object> filter) throws Exception {
		return scDAO.getSCDRById(filter);
	}
	
	public SCMaterial getSCMaterialDRById(HashUtil<String, Object> filter) throws Exception {
		return scDAO.getSCMaterialDRById(filter);
	}
	
	public void updateSCDR(SC sc) throws Exception {
		scDAO.updateSCDR(sc);
	}
	
	public SCAcuerdo getSCAcuerdoDRById(HashUtil<String, Object> filter) throws Exception {
		return scDAO.getSCAcuerdoDRById(filter);
	}
    
	public void updateAcuerdoDR(SCAcuerdo obj) throws Exception{
		scDAO.updateAcuerdoDR( obj);
	}
	
    public void transmiteResolutor(HashUtil<String, Object> filter) throws Exception {
        scDAO.transmiteResolutor(filter);
    }
    
	public List<SCAcuerdo> getAcuerdoList(HashUtil<String, Object> filter) throws Exception{
		return scDAO.getAcuerdoList(filter);
	}
	
	public SCDetalleDj getDatosDJParaCalificacion(HashUtil<String, Object> filter) throws Exception {
		return scDAO.getDatosDJParaCalificacion(filter);
	}
	
}
