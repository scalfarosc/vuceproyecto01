package pe.gob.mincetur.vuce.co.service.solicitudCalificacion;

import java.util.List;
import java.util.Map;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.AdjuntoRequeridoSC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCAcuerdo;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCDetalleDj;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCMaterial;

public interface SCService  {
	
	public HashUtil<String, Object> insertSC(HashUtil<String, Object> filter) throws Exception;
    public void insertUsuarioSC(HashUtil<String, Object> filter) throws Exception;
    public void transmiteSC(HashUtil<String, Object> filter) throws Exception;
	public void updateSC(SC sc) throws Exception;
	public SC getSCById(HashUtil<String, Object> filter) throws Exception;
	public SCMaterial getSCMaterialById(HashUtil<String, Object> filter) throws Exception;
	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter)throws Exception;
	public Solicitud getSolicitudById(HashUtil<String, Object> filter)throws Exception;
	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter)throws Exception;
    //public void updateMaterial(SCMaterial obj) throws Exception;
    public void updateRepresentante(HashUtil<String, Object> filter) throws Exception;
    public void insertAcuerdoPais(HashUtil<String, Object> filter) throws Exception;
    public void deleteAcuerdoPais(HashUtil<String, Object> filter) throws Exception;
    public List<SCMaterial> getMaterialList(HashUtil<String, Object> filter) throws Exception;
    public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception;
    
    public AdjuntoRequeridoSC getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception;
    
    public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception;
    
    
    // RESOLUTOR
    public void asignarEvaluador(Long ordenId, Integer usuarioId, Integer rol) throws Exception;
    
    public void iniciarEvaluacionCalificacion(Long ordenId) throws Exception;
    
    public SC insertBorradorResolutivoDr(SC sc) throws Exception;
    
    public void deleteBorradorResolutivoDr(SC sc) throws Exception;
    
    public Solicitante getSolicitanteDRDetail(HashUtil<String, Object> filter)throws Exception;
    
    public Solicitante getSolicitanteDRBorradorDetail(HashUtil<String, Object> filter)throws Exception;
    
    public SC getSCDRById(HashUtil<String, Object> filter) throws Exception;
    
    public SCMaterial getSCMaterialDRById(HashUtil<String, Object> filter) throws Exception;
    
    public void updateSCDR(SC sc) throws Exception;
    
    public SCAcuerdo getSCAcuerdoDRById(HashUtil<String, Object> filter) throws Exception;
    
    public void updateAcuerdoDR(SCAcuerdo obj) throws Exception;
    
    public void transmiteResolutor(HashUtil<String, Object> filter) throws Exception;
    
    public List<SCAcuerdo> getAcuerdoList(HashUtil<String, Object> filter) throws Exception;
    
    public SCDetalleDj getDatosDJParaCalificacion(HashUtil<String, Object> filter) throws Exception;
    
}
