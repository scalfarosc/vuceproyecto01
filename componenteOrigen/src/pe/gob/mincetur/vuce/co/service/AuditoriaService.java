package pe.gob.mincetur.vuce.co.service;

import pe.gob.mincetur.vuce.co.domain.Auditoria;


public interface AuditoriaService {
	
	public void insertInicioSesion(Auditoria auditoria) throws Exception;
	
	public void insertInicioSesionContingencia(Auditoria auditoria) throws Exception;
	
	public void insertFinSesionContingencia(Auditoria auditoria) throws Exception;
	
	public void insertAccesoSolicitud(Auditoria auditoria) throws Exception;
	
	public void insertAccesoSuce(Auditoria auditoria) throws Exception;
	
	public void insertAccesoDr(Auditoria auditoria) throws Exception;
	
	

}
