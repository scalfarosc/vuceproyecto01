package pe.gob.mincetur.vuce.co.service;

import org.jlis.core.util.HashUtil;

/**
 * Service para Contingencia VUCE
 * @author 
 *
 */
public interface ContingenciaAuthService {
    
	public Integer obtenerModoOperacion() throws Exception;

	public String registraConsulta(Integer componente, String ruc, String usuario, Integer claveSolDisponible, Integer indOperacion) throws Exception;

	public String solicitaContingencia(Integer componente, Integer tipoUsuario, String ruc, String usuario) throws Exception;
	
	public HashUtil<String, Object> validaToken(String token, Integer componente) throws Exception;	
	
	public String obtenerMensajeIndexContingencia() throws Exception;
	
	public String obtenerMensajeCorreoEnviado() throws Exception;
	
	public String obtenerMensajeValidacionToken() throws Exception;
	
	public String obtenerMensajeValidacionCaptura() throws Exception;	
    
}
