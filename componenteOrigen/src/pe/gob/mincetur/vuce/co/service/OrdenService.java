package pe.gob.mincetur.vuce.co.service;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Orden;


public interface OrdenService {

    public Orden loadOrdenByNumero(Long numero) throws Exception;
    
    public Orden loadOrdenById(Long idOrden) throws Exception;

    public Orden loadOrdenDetail(HashUtil<String, Object> filter) throws Exception;

    public HashUtil<String, Object> insertOrdenCab(HashUtil<String, Object> filter) throws Exception;
    
    public void insertOrdenUsuario(HashUtil<String, Object> filter) throws Exception;

    public void insertOrdenSol(HashUtil<String, Object> filter) throws Exception;

    public void insertOrdenRep(HashUtil<String, Object> filter) throws Exception;
    
    public void updateOrdenRepresentante(HashUtil<String, Object> filter) throws Exception;

    public HashUtil<String, Object> creaModifOrden(HashUtil<String, Object> filter) throws Exception;

    public HashUtil<String, Object> transmiteOrden(HashUtil<String, Object> filter) throws Exception;

    public void desistirOrden(HashUtil<String, Object> filter) throws Exception;

    //public int existeModificaciones(HashUtil<String, Object> filter)throws Exception;
    
}
