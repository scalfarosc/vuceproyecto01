package pe.gob.mincetur.vuce.co.service.certificado;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoRequeridoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificado.Certificado;

public interface CertificadoService  {
	
	public HashUtil<String, Object> insertCertificado(HashUtil<String, Object> filter) throws Exception;
	
    public void insertUsuarioCertificado(HashUtil<String, Object> filter) throws Exception;
    
    public void transmiteCertificado(HashUtil<String, Object> filter) throws Exception;
	
	public Certificado getCertificadoById(HashUtil<String, Object> filter) throws Exception;
	
	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter) throws Exception;
	
	public Solicitud getSolicitudById(HashUtil<String, Object> filter) throws Exception;
	
	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter) throws Exception;
	
	public AdjuntoRequeridoCertificado getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception;
	
	public void updateRepresentante(HashUtil<String, Object> filter) throws Exception;
	
	public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception;
	
	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception;
	
	// RESOLUTOR
	public void asignarEvaluador(Long ordenId, Integer usuarioId, Integer rol) throws Exception;
	
	public void iniciarEvaluacionCertificado(Long ordenId) throws Exception;
	
	public HashUtil<String, Object> insertBorradorResolutivoDr(HashUtil<String, Object> filter) throws Exception;
	
    public void deleteBorradorResolutivoDr(HashUtil<String, Object> filter) throws Exception;
    
    public void transmiteResolutor(HashUtil<String, Object> filter) throws Exception;
    
	public Solicitante getSolicitanteDRDetail(HashUtil<String, Object> filter)throws Exception;
	
	public Solicitante getSolicitanteDRBorradorDetail(HashUtil<String, Object> filter)throws Exception;
	
	public Certificado getCertificadoDRById(HashUtil<String, Object> filter) throws Exception;
	
}
