package pe.gob.mincetur.vuce.co.service;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.AdjuntoRequerido;

public interface FormatoService {

	public HashMap<String, String> loadUbigeo(HashUtil<String, Object> filter) throws Exception;

    public HashMap<String, String> loadUbigeoByDistrito(HashUtil<String, Object> filter) throws Exception;

    public Solicitante loadSolicitanteConEmpresa(HashUtil<String, Object> filter)throws Exception;

    public Solicitante loadSolicitanteSinEmpresa(HashUtil<String, Object> filter)throws Exception;

    public String loadNombreUsuario(HashUtil<String, Object> filter) throws Exception;

    public String loadPartidaArancelaria(HashUtil<String, Object> filter) throws Exception;

    public Formato loadFormatoByTupa(HashUtil<String, Object> filter) throws Exception;

    public Solicitante loadUsuarioFormato(HashUtil<String, Object> filter) throws Exception;

    public Solicitante loadSolicitanteFormato(HashUtil<String, Object> filter)throws Exception;

    public RepresentanteLegal loadRepresentanteFormato(HashUtil<String, Object> filter)throws Exception;

	public JasperPrint ejecutarReporteConConexionBD(JasperReport report, Map parameters) throws Exception;

    public AdjuntoRequerido loadRequerido(HashUtil<String, Object> filter) throws Exception;

	public String obtenerAyuda(Map<String, Object> filtros) throws Exception;

	public String permiteCrearFactura(Long ordenId, Long codigoCertificado) throws Exception;

	public String permiteCrearProductor(Long calificacionUoId) throws Exception;

	public String permiteConfirmarFinEval(Long coId, Long ordenId, Integer mto) throws Exception;

    public Long getFormatoDrId(HashUtil<String, Object> filter) throws Exception;

	public int cuentaAdjuntosRequeridos(HashUtil<String, Object> filter) throws Exception;

	public int cuentaNotificacionesPendientes(HashUtil<String, Object> filter) throws Exception;

	public int cuentaAdjuntosRequeridosDJ(HashUtil<String, Object> filter) throws Exception;

	public int cuentaNotificacionesSolicitudPendientes(HashUtil<String, Object> filter) throws Exception;

	public String permiteCrearSolicitudRectif(Long suceId) throws Exception;

	public String permiteApoderamientoXFrmt(Integer acuerdoInternacionalId) throws Exception;

	public int cuentaAdjuntosFacturaRequeridos(HashUtil<String, Object> filter) throws Exception;

	public String acuerdoConValidacionProductor(Integer codigoAcuerdo) throws Exception;

	public void validacionPreTransmision(HashUtil<String, Object> filter) throws Exception;
	
	public void validacionPreFormato(HashUtil<String, Object> filter) throws Exception; //20140526 - JMC - BUG 1.PersonaNatural
	
	public String es_version_antigua(HashUtil<String, Object> filter) throws Exception;
	
	public String es_dj_version_diferente(HashUtil<String, Object> filter) throws Exception;
	
	public String es_reemplazo_version_diferente(HashUtil<String, Object> filter) throws Exception;
}
