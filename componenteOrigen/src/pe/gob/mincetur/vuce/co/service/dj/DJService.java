package pe.gob.mincetur.vuce.co.service.dj;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.dj.AdjuntoRequeridoDJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJExportador;
import pe.gob.mincetur.vuce.co.domain.dj.DJMaterial;
import pe.gob.mincetur.vuce.co.domain.dj.DJProductor;

public interface DJService {

	public HashUtil<String, Object> insertDJ(HashUtil<String, Object> filter) throws Exception;

	public void insertUsuarioDJ(HashUtil<String, Object> filter) throws Exception;

	public void updateDJ(DJ dj) throws Exception;

	public void transmiteDJ(HashUtil<String, Object> filter) throws Exception;

	public DJ getDJById(HashUtil<String, Object> filter) throws Exception;

	public DJMaterial getDJMaterialById(HashUtil<String, Object> filter) throws Exception;

	public DJProductor getDJProductorById(HashUtil<String, Object> filter) throws Exception;

	public DJExportador getDJExportadorById(HashUtil<String, Object> filter) throws Exception;

	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter) throws Exception;

	public Solicitud getSolicitudById(HashUtil<String, Object> filter) throws Exception;

	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter) throws Exception;

	public Integer insertMaterial(DJMaterial obj) throws Exception;

	public void updateMaterial(DJMaterial obj) throws Exception;

	public void deleteMaterial(DJMaterial obj) throws Exception;

	public Integer insertProductor(DJProductor obj) throws Exception;

	public void updateProductor(DJProductor obj) throws Exception;

	public void deleteProductor(DJProductor obj) throws Exception;

	public Integer insertExportador(DJExportador obj) throws Exception;

	public void updateExportador(DJExportador obj) throws Exception;

	public void deleteExportador(DJExportador obj) throws Exception;

	public void actualizarSeleccionEsExportador(HashUtil<String, Object> filter) throws Exception;

	public AdjuntoRequeridoDJ getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception;

	public void updateRepresentante(HashUtil<String, Object> filter) throws Exception;

	public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception;

	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception;

	public void modificarRol(long djId, int tipoUsuarioDj) throws Exception;

}
