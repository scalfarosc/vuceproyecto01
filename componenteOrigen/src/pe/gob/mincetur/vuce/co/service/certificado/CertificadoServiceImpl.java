package pe.gob.mincetur.vuce.co.service.certificado;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.certificado.CertificadoDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoRequeridoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificado.Certificado;


public class CertificadoServiceImpl implements CertificadoService {
	
	private CertificadoDAO certificadoDAO;
	
	public void setCertificadoDAO(CertificadoDAO certificadoDAO) {
		this.certificadoDAO = certificadoDAO;
	}
	
	public HashUtil<String, Object> insertCertificado(HashUtil<String, Object> filter) throws Exception {
        return certificadoDAO.insertCertificado(filter);
    }

    public void insertUsuarioCertificado(HashUtil<String, Object> filter) throws Exception {
        certificadoDAO.insertUsuarioCertificado(filter);
    }
	
    public void transmiteCertificado(HashUtil<String, Object> filter) throws Exception {
        certificadoDAO.transmiteCertificado(filter);
    }
    
	public Certificado getCertificadoById(HashUtil<String, Object> filter) throws Exception {
		return certificadoDAO.getCertificadoById(filter);
	}
	
	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter) throws Exception{
		return certificadoDAO.getUsuarioFormatoById(filter);
	}
	
	public Solicitud getSolicitudById(HashUtil<String, Object> filter) throws Exception{
		return certificadoDAO.getSolicitudById(filter);
	}
	
	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter) throws Exception{
		return certificadoDAO.getSolicitanteDetail(filter);
	}
	
    public AdjuntoRequeridoCertificado getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception {
        return certificadoDAO.getAdjuntoRequeridoById(filter);
    }
	    
	public void updateRepresentante(HashUtil<String, Object> filter) throws Exception {
        certificadoDAO.updateRepresentante(filter);
    }
	
	public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception {
        certificadoDAO.updateCargoDeclarante(filter);
    }
	
	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception{
		return certificadoDAO.getAdjuntoRequeridoCount(filter);
	}
	
	
    // RESOLUTOR
	public void asignarEvaluador(Long ordenId, Integer usuarioId, Integer rol) throws Exception {
		certificadoDAO.asignarEvaluador(ordenId, usuarioId, rol);
	}
	
	public void iniciarEvaluacionCertificado(Long ordenId) throws Exception {
		certificadoDAO.iniciarEvaluacionCertificado(ordenId);
	}
	
	public HashUtil<String, Object> insertBorradorResolutivoDr(HashUtil<String, Object> filter) throws Exception {
    	return certificadoDAO.insertBorradorResolutivoDr(filter);
    }
    
    public void deleteBorradorResolutivoDr(HashUtil<String, Object> filter) throws Exception {
    	certificadoDAO.deleteBorradorResolutivoDr(filter);
    }
    
    public void transmiteResolutor(HashUtil<String, Object> filter) throws Exception {
    	certificadoDAO.transmiteResolutor(filter);
    }
    
	public Solicitante getSolicitanteDRDetail(HashUtil<String, Object> filter)throws Exception{
		return certificadoDAO.getSolicitanteDRDetail(filter);
	}
	
	public Solicitante getSolicitanteDRBorradorDetail(HashUtil<String, Object> filter)throws Exception{
		return certificadoDAO.getSolicitanteDRBorradorDetail(filter);
	}
	
	public Certificado getCertificadoDRById(HashUtil<String, Object> filter) throws Exception {
		return certificadoDAO.getCertificadoDRById(filter);
	}
    
}
