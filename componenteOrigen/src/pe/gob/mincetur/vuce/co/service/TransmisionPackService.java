package pe.gob.mincetur.vuce.co.service;

import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;

public interface TransmisionPackService {
	
	public DocResolutivoSecEBXML procesarIOPVUCEs(InteroperabilidadFirma iop) throws Exception ;
	
	public DocResolutivoSecEBXML genera_N85(String token, InteroperabilidadFirma iop, Integer secuenciaMaxima) throws Exception ;
	
	public DocResolutivoSecEBXML procesarTransmisionParaPack(String token, InteroperabilidadFirma iop, Integer secuenciaMaxima) throws Exception ;

	public DocResolutivoSecEBXML procesarTransmisionRespuestaParaPack(Integer codId, String resultadoValidacion) throws Exception ;	
	
	public DocResolutivoSecEBXML genera_N93(Integer codId, Integer vcId, String resultadoValidacion) throws Exception ;
}
