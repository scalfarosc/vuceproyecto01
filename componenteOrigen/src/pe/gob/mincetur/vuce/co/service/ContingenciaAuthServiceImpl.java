package pe.gob.mincetur.vuce.co.service;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.ContingenciaAuthDAO;

/**
 * ServiceImpl para Contingencia VUCE
 * @author 
 *
 */
public class ContingenciaAuthServiceImpl implements ContingenciaAuthService {
	
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

    private ContingenciaAuthDAO dao;

	@Override
	public Integer obtenerModoOperacion() throws Exception {
		logger.debug("obtenerModoOperacion");
		return dao.obtenerModoOperacion();
	}

	@Override
	public String registraConsulta(Integer componente, String ruc, String usuario, Integer claveSolDisponible, Integer indOperacion) throws Exception {
		logger.debug("registraConsulta");
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("componente", componente);		
        filter.put("ruc", ruc);
        filter.put("usuario", usuario);
        filter.put("claveSolDisponible", claveSolDisponible);		
        filter.put("indOperacion", indOperacion);		
		return dao.registraConsulta(filter);
	}

	@Override
	public String solicitaContingencia(Integer componente, Integer tipoUsuario, String ruc, String usuario) throws Exception {
		logger.debug("solicitaContingencia");
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("componente", componente);		
        filter.put("tipoUsuario", tipoUsuario);		
        filter.put("ruc", ruc);
        filter.put("usuario", usuario);
		return dao.solicitaContingencia(filter);
	}

	@Override
	public HashUtil<String, Object> validaToken(String token, Integer componente) throws Exception {
		logger.debug("registraConsulta");
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("componente", componente);	
		return dao.validaToken(filter);
	}


	public void setDao(ContingenciaAuthDAO dao) {
		this.dao = dao;
	}

	@Override
	public String obtenerMensajeIndexContingencia() throws Exception {
		return dao.obtenerMensajeIndexContingencia();
	}

	@Override
	public String obtenerMensajeCorreoEnviado() throws Exception {
		return dao.obtenerMensajeCorreoEnviado();
	}

	@Override
	public String obtenerMensajeValidacionToken() throws Exception {
		return dao.obtenerMensajeValidacionToken();
	}

	@Override
	public String obtenerMensajeValidacionCaptura() throws Exception {
		return dao.obtenerMensajeValidacionCaptura();
	}
    
}
