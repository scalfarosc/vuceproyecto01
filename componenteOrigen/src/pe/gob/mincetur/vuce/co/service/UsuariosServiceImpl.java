package pe.gob.mincetur.vuce.co.service;

import java.util.Enumeration;

import org.jlis.core.bean.Option;
import org.jlis.core.list.OptionList;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.dao.UsuariosDAO;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Usuario;

public class UsuariosServiceImpl implements UsuariosService {
	
	private UsuariosDAO usuariosDAO;
	
	public void setUsuariosDAO(UsuariosDAO usuariosDAO) {
		this.usuariosDAO = usuariosDAO;
	}

	public int insertUsuario(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.insertUsuario(filter);
    }
	
	/*public int insertUsuarioEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.insertUsuarioEmpresaExterna(filter);
    }
	*/
	public int insertEmpresa(HashUtil<String, Object> filter, Table representantesLegales) throws Exception {
		int idEmpresa = usuariosDAO.insertEmpresa(filter);
		
        // Insertar Representantes Legales
		if (representantesLegales!=null) {
			RepresentanteLegal repLegal = null;
			for (int i=0; i < representantesLegales.size(); i++) {
				Row row = representantesLegales.getRow(i);
				if (row.getCell("SELECCIONE").getValue()!=null && row.getCell("SELECCIONE").getValue().toString().equalsIgnoreCase("S")) {
					repLegal = new RepresentanteLegal();
					
			        HashUtil<String, Object> filterRepresentanteLegal = new HashUtil<String, Object>();
			        filterRepresentanteLegal.put("empresa_id", idEmpresa);
			        filterRepresentanteLegal.put("numero_documento", row.getCell("NUMERO_DOCUMENTO").getValue().toString().trim());
			        filterRepresentanteLegal.put("nombre", row.getCell("NOMBRE").getValue().toString().trim());
			        
			        insertRepresentanteLegal(filterRepresentanteLegal);
				}
			}
		}
		return idEmpresa;
    }
	
	public int insertEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.insertEmpresaExterna(filter);
	}
	
	public void insertRepresentanteLegal(HashUtil<String, Object> filter) throws Exception {
		usuariosDAO.insertRepresentanteLegal(filter);
	}
	
	public void updateUsuario(HashUtil<String, Object> filter) throws Exception {
		usuariosDAO.updateUsuario(filter);
    }
	
	/*public void updateUsuarioEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
		usuariosDAO.updateUsuarioEmpresaExterna(filter);
    }
	*/
	public void updateEmpresa(HashUtil<String, Object> filter, Table representantesLegales) throws Exception {
		usuariosDAO.updateEmpresa(filter);
		int idEmpresa = filter.getInt("empresa_id");
		
        // Insertar Representantes Legales
		updateRepresentantesLegalesEmpresa(idEmpresa, representantesLegales);
    }
	
	public void updateRepresentantesLegalesEmpresa(int idEmpresa, Table representantesLegales) throws Exception {
		// Actualizar Representantes Legales
		if (representantesLegales!=null) {
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("empresa_id", idEmpresa);
			usuariosDAO.inactivarRepresentantesLegales(filter);
			
			for (int i=0; i < representantesLegales.size(); i++) {
				Row row = representantesLegales.getRow(i);
				if (row.getCell("SELECCIONE").getValue()!=null && row.getCell("SELECCIONE").getValue().toString().equalsIgnoreCase("S")) {
					HashUtil<String, Object> filterRepresentanteLegal = new HashUtil<String, Object>();
			        filterRepresentanteLegal.put("empresa_id", idEmpresa);
			        filterRepresentanteLegal.put("numero_documento", row.getCell("NUMERO_DOCUMENTO").getValue().toString().trim());
			        filterRepresentanteLegal.put("nombre", row.getCell("NOMBRE").getValue().toString().trim());
			        
			        insertRepresentanteLegal(filterRepresentanteLegal);
				}
			}
		}
	}
	
	public void updateEmpresa(HashUtil<String, Object> filter) throws Exception {
		usuariosDAO.updateEmpresa(filter);
	}
	public void updateEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
		usuariosDAO.updateEmpresaExterna(filter);
	}
	
	public int existeUsuarioSol(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.existeUsuarioSol(filter);
	}

	public int existeUsuarioExtranet(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.existeUsuarioExtranet(filter);
	}

	public int existeUsuarioDNI(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.existeUsuarioDNI(filter);
	}

	public boolean existeEmpresa(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.existeEmpresa(filter);
	}
	
	public boolean usuarioTieneTramites(HashUtil<String, Object> filter) throws Exception {
		return usuariosDAO.usuarioTieneTramites(filter);
	}
	
	public HashUtil actualizarRolesUsuario(int idUsuario, HashUtil roles) throws Exception {
		HashUtil nuevosRoles = new HashUtil();
		// Primero borramos todos los roles asociados al usuario
		usuariosDAO.eliminarRolesUsuario(idUsuario);
		
		// Luego, registramos sus roles uno por uno
		Enumeration en = roles.keys();
		while (en.hasMoreElements()) {
			String codigoRol = (String)en.nextElement();
			usuariosDAO.registrarRolUsuario(idUsuario, codigoRol);
		}
		
		return cargarRolesUsuario(idUsuario);
	}
	
	public HashUtil cargarRolesUsuario(int idUsuario) {
		HashUtil nuevosRoles = new HashUtil();
		IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idUsuario", idUsuario);
        OptionList listaRoles = ibatisService.loadList("usuario.roles", filter);
		for (int i=0; i < listaRoles.size(); i++) {
            Option rol = listaRoles.getOption(i);
            nuevosRoles.put(rol.getDescripcion(), rol.getDescripcion());
		}
		return nuevosRoles;
	}

    public void testGrabacionUsuarioContextIdentifier(HashUtil<String, Object> filter) {
        usuariosDAO.testGrabacionUsuarioContextIdentifier(filter);
    }

	public UsuarioCO loadUsuarioCOById(int idUsuario) throws Exception {
		return usuariosDAO.loadUsuarioCOById(idUsuario);
	}

	public Usuario loadUsuarioById(int idUsuario) throws Exception {
		return usuariosDAO.loadUsuarioById(idUsuario);
	}

	public Usuario loadEmpresaById(int idUsuario) throws Exception {
		return usuariosDAO.loadEmpresaById(idUsuario);
	}

	public UsuarioCO loadUsuarioVUCEByRucAndCodigoSol(String ruc, String codigoUsuario) throws Exception{
		return usuariosDAO.loadUsuarioVUCEByRucAndCodigoSol(ruc, codigoUsuario);
	}
	
}
