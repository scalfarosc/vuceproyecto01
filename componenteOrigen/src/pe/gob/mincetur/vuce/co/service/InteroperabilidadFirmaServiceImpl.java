package pe.gob.mincetur.vuce.co.service;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pe.gob.mincetur.vuce.co.dao.InteroperabilidadFirmaDAO;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.firma.BienOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.CabeceraOrigenEnvio;
import pe.gob.mincetur.vuce.co.domain.firma.CodExporter;
import pe.gob.mincetur.vuce.co.domain.firma.ContenidoOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.Eh;
import pe.gob.mincetur.vuce.co.domain.firma.EhCert;
import pe.gob.mincetur.vuce.co.domain.firma.EnteComercio;
import pe.gob.mincetur.vuce.co.domain.firma.EnvioCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.FacturaOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.FacturaTercerOpOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;
import pe.gob.mincetur.vuce.co.logic.certificadoOrigen.CertificadoOrigenLogic;
import pe.gob.mincetur.vuce.co.util.stream.StreamUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;


public class InteroperabilidadFirmaServiceImpl implements InteroperabilidadFirmaService {

	InteroperabilidadFirmaDAO iopDAO;
	TransmisionPackService iopService; // JMC 02/12/2016 Alianza Pacifico
	private static Logger logger = Logger.getLogger(org.jlis.core.util.Constantes.LOGGER_SERVICE);
	
	// En general no es una buena practica obtener un logic desde un service, pero el logic es legacy y genera los PDFs
    CertificadoOrigenLogic certificadoOrigenLogic;
	private IbatisService ibatisService;
    
	public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}
	
    public void setIopService(TransmisionPackService iopService) {
		this.iopService = iopService;
	}

	public void setIopDAO(InteroperabilidadFirmaDAO iopDAO) {
		this.iopDAO = iopDAO;
	}

    public void setCertificadoOrigenLogic(CertificadoOrigenLogic certificadoOrigenLogic) {
		this.certificadoOrigenLogic = certificadoOrigenLogic;
	}

    public InteroperabilidadFirma obtenerToken(Long drId, Integer sdr) throws Exception {
    	return iopDAO.obtenerToken(drId, sdr);
    }
    
    public Integer registrarInteroperabilidad(InteroperabilidadFirma iop) throws Exception {
    	return iopDAO.registrarInteroperabilidad(iop);
    }
    
    public InteroperabilidadFirma obtenerEtapa(String token, Integer secuencia)  throws Exception {
    	return iopDAO.obtenerEtapa(token, secuencia);
    }
    

    
    public EnvioCertificadoOrigen obtenerInformacionDocumentoResolutivo(long drId, int sdr) throws Exception {
    	EnvioCertificadoOrigen result = new EnvioCertificadoOrigen();
    	/*
    	String formato = "";
    	if (iopDAO.esMct001(drId, sdr)) {
    		formato = "mct001";
    	} else if (iopDAO.esMct003(drId, sdr)) {
    		formato = "mct003";
    	}
    	*/
		CodExporter codExporter = new CodExporter();
		codExporter = iopDAO.obtenerDetalleCertificado(drId, sdr); 
		Long codigoId = codExporter.getCodigoId();

    	EnteComercio exportador = iopDAO.obtenerExportador(codigoId);    	
    	EnteComercio importador = iopDAO.obtenerImportador(codigoId); 
    	List<EnteComercio> productores = iopDAO.obtenerProductores(codigoId);    	
    	List<FacturaOrigen> facturas = iopDAO.obtenerFacturas(codigoId);
    	List<BienOrigen> bienes = iopDAO.obtenerBienes(codigoId);    	
    	List<FacturaTercerOpOrigen> facturasTercerOp = iopDAO.obtenerFacturasTercerOp(codigoId);
		ContenidoOrigen contenido = new ContenidoOrigen();		

		codExporter.setImportador(importador);
		codExporter.setExportador(exportador);
		codExporter.setProductores(productores);
		codExporter.setFacturas(facturas);
		codExporter.setBienes(bienes);
		codExporter.setTercerOperadores(facturasTercerOp);		
		
		contenido.setCodExporter(codExporter);
		Eh eh = new Eh();
		contenido.setEh(eh);
		EhCert ehCert = iopDAO.obtenerCertificacionEH(codigoId); 
		ehCert.setFechaCertificado(Util.getDate(new Date(),"yyyy-MM-dd'T'HH:mm:ss'Z'"));
		
		contenido.setEhCert(ehCert);		
		result.setContenido(contenido);
		CabeceraOrigenEnvio cabecera = new CabeceraOrigenEnvio();
		result.setCabecera(cabecera);
    	
    	return result;
    }
    
    public byte[] generarXml(String token) throws Exception {
    	byte [] resultado = null;
    	logger.debug("*PEDRO**GenerarXml****");
    	// Con el token, obtener la etapa 1 siempre, para tener los datos principales
    	InteroperabilidadFirma primeraEtapa = iopDAO.obtenerEtapa(token, 1);
    	
    	Integer secuenciaMaxima = iopDAO.getEtapa(token);
    	
    	// Solo este metodo genera las etapas por eso es necesario que sea invocado siempre antes que el de pdf
    	if (secuenciaMaxima == 1) { // Grabamos la etapa 2, y no hemos realizado ninguna firma, asi que necesitamos el XML desde la base de datos de origen
    		logger.debug("*PEDRO**etapa 1****");
    		//Llena las tablas de negocio de Alianza Origen
    		Long codigoRegistroDrAlianzaId = iopDAO.generaDrAlianza(primeraEtapa.getDrId(),primeraEtapa.getSdr());
    		
    		//if (codigoRegistroDrAlianzaId)
    		
    		// Graba etapa 2
    		InteroperabilidadFirma iop = new InteroperabilidadFirma();
    		iop.setToken(primeraEtapa.getToken());
    		iop.setDrId(primeraEtapa.getDrId());
    		iop.setSdr(primeraEtapa.getSdr());
    		iop.setAcuerdoInternacionalId(primeraEtapa.getAcuerdoInternacionalId());
    		Integer secuencia = iopDAO.registrarInteroperabilidad(iop);
    		iop.setSecuencia(secuencia);
    		
        	// Con los datos, obtener la informacion
        	
        	// Con la informacion, llenar el modelo de dominio 
    		EnvioCertificadoOrigen origen = obtenerInformacionDocumentoResolutivo(iop.getDrId(), iop.getSdr()); 
        	
        	// Con el modelo de dominio, generar el XML
    		// Create your Configuration instance, and specify if up to what FreeMarker
    		// version (here 2.3.25) do you want to apply the fixes that are not 100%
    		// backward-compatible. See the Configuration JavaDoc for details.
    		Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);

    		// Specify the source where the template files come from. Here I set a
    		// plain directory for it, but non-file-system sources are possible too:
    		//	cfg.setDirectoryForTemplateLoading(new File("/"));
    		cfg.setClassLoaderForTemplateLoading(this.getClass().getClassLoader(), "/");

    		// Set the preferred charset template files are stored in. UTF-8 is
    		// a good choice in most applications:
    		//cfg.setDefaultEncoding("UTF-8");

    		// Sets how errors will appear.
    		// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
    		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

    		// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
    		cfg.setLogTemplateExceptions(false);
    		
    		Map root = new HashMap();
    	    root.put("cabecera", origen.getCabecera());
    	    root.put("contenido", origen.getContenido());
    	    //this.getClass().getResourceAsStream("/damtojava-digester-rules.xml")
    		Template temp = cfg.getTemplate("origin-cert-soap-request-template.xml");
    		ByteArrayOutputStream baos = new ByteArrayOutputStream();
    		Writer out = new BufferedWriter(new OutputStreamWriter(baos,"UTF-8"));

    		temp.process(origen, out);
        	
            resultado = baos.toByteArray();  
    	} else if (secuenciaMaxima == 2) { // Grabamos la etapa 3, y el XML se obtiene desde la etapa 2 (porque se va a firmar una vez m�s)
    		// Graba etapa 2
    		logger.debug("*PEDRO**etapa 2****");
    		InteroperabilidadFirma iop = new InteroperabilidadFirma();
    		iop.setToken(primeraEtapa.getToken());
    		iop.setDrId(primeraEtapa.getDrId());
    		iop.setSdr(primeraEtapa.getSdr());
    		iop.setAcuerdoInternacionalId(primeraEtapa.getAcuerdoInternacionalId());
    		Integer secuencia = iopDAO.registrarInteroperabilidad(iop);
    		iop.setSecuencia(secuencia);    		
    		InteroperabilidadFirma iopGet = iopDAO.obtenerXml(token, 2);

    		//resultado = new String(iopGet.getXml()).getBytes("UTF-8");
    		
	        //Actualiza datos de la fecha de la firma del Evaluador    		
    		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("drId", iop.getDrId());	        
	        Integer codId = ibatisService.loadElement("firma.obtenerCodId", filter).getInt("cod_id");
	        logger.debug("generarXml::");
    		String certificadoId = iopDAO.actualizaDrAlianza(codId);
    		logger.debug("generarXml::"+certificadoId);
    		
    		//Actualizar en el XML el CertficadoID y la Fecha de Emision
    		EnvioCertificadoOrigen origenActualizado = obtenerInformacionDocumentoResolutivo(iop.getDrId(), iop.getSdr());
    		origenActualizado.getContenido().getEhCert().setCertificadoId(certificadoId);
    		resultado = actualizarXML(origenActualizado.getContenido(), iopGet.getXml());
    	}
    	
  	
        return resultado;
    }
    
    public byte[] generarPdf(String token)  throws Exception {
    	byte [] resultado = null;
    	
    	// Con el token, obtener la etapa 1 siempre, para tener los datos principales
    	InteroperabilidadFirma primeraEtapa = iopDAO.obtenerEtapa(token, 1);
    	
    	Integer secuenciaMaxima = iopDAO.getEtapa(token);
    	
    	if (secuenciaMaxima == 2) { // Generamos el pdf desde la aplicaci�n de origen
	        Long drId = primeraEtapa.getDrId();
	        Long sdr = primeraEtapa.getSdr().longValue();
	        String formato = "MCT001";
	        Integer idAcuerdo = primeraEtapa.getAcuerdoInternacionalId();
	        
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("drId", drId);
	        
	        Integer codId = ibatisService.loadElement("firma.obtenerCodId", filter).getInt("cod_id");
	        
	    	resultado = certificadoOrigenLogic.generarReporteAPBytes(codId);
	    	
    	} else if (secuenciaMaxima == 3) { // El PDF se obtiene desde la etapa 2 (porque se va a firmar una vez m�s)
    		InteroperabilidadFirma iop = iopDAO.obtenerPdf(token, 2);
    		resultado = iop.getPdf();
    	}
    		
        return resultado;
    }

    /**
     * Este metodo solo deberia ser invocado cuando exista etapa 2 o etapa 3 solamente
     */
    public Integer enviarXmlFirmado(String token, byte[] xml) throws Exception {
    	Integer result = 1;
    	logger.debug("*PEDRO**enviarXMLFirmado****");
    	Integer secuenciaMaxima = iopDAO.getEtapa(token);
    	
    	InteroperabilidadFirma iop = iopDAO.obtenerEtapa(token, 1);
    	
    	logger.debug("*PEDRO**etapa**** "+secuenciaMaxima);
    	
    	if (secuenciaMaxima == 2) { // Si es etapa 2, actualizamos
    		iopDAO.actualizaXml(token, secuenciaMaxima, xml);
    		iopDAO.actualizaEstadoFirma(iop.getDrId(), iop.getSdr(), "F1");//F1->Primera Firma
    	} else if (secuenciaMaxima == 3) { // Si es etapa 3, actualizamos
    		iopDAO.actualizaXml(token, secuenciaMaxima, xml);
    		//Actualiza datos de la fecha de la firma del Evaluador
	        /*HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("drId", iop.getDrId());	        
	        Integer codId = ibatisService.loadElement("firma.obtenerCodId", filter).getInt("cod_id");
	        */
	        //Actualiza la fecha_certificado, certificado_id y el nombre_firma_autoridad para firmar
    		//iopDAO.actualizaDrAlianza(codId);    		
    		//Genera N85 y lo encola para el Pack
    		iopService.procesarTransmisionParaPack(token, iop, secuenciaMaxima);// JMC 02/12/2016 Alianza Pacifico
    	}
    	
        return result;
    }
    
    public Integer enviarPdfFirmado(String token, byte[] pdf) throws Exception {
    	Integer result = 1;
    	
    	Integer secuenciaMaxima = iopDAO.getEtapa(token);
    	
    	InteroperabilidadFirma iop = iopDAO.obtenerEtapa(token, 1);

    	if (secuenciaMaxima == 2) { // Si es etapa 2, actualizamos
    		iopDAO.actualizaPdf(token, secuenciaMaxima, pdf);
    	} else if (secuenciaMaxima == 3) { // Si es etapa 3, actualizamos
    		iopDAO.actualizaPdf(token, secuenciaMaxima, pdf);
    		iopDAO.actualizaEstadoFirma(iop.getDrId(), iop.getSdr(), "F");//F->Segunda Firma
    	}
        return result;
    }    
    
    public void actualizaFirmaExportador(Long drId, Integer sdr, String nombreExp, String formato, String posteriori) throws Exception {
    	iopDAO.actualizaFirmaExportador(drId, sdr, nombreExp, formato, posteriori);
    }    
    
    public InteroperabilidadFirma obtenerPdf(String token, Integer secuencia) throws Exception {
    	return iopDAO.obtenerPdf(token, secuencia);
    }
    
    public byte[] actualizarXML(ContenidoOrigen certificadoOrigen, byte[] bytesXML) throws Exception{
    	logger.debug("*PEDRO**actualizaXML****");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder builder = dbf.newDocumentBuilder();        
        Document doc = builder.parse(new InputSource(new ByteArrayInputStream(bytesXML)));
		
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
            	if (prefix.equals("m4")) {
                	return "http://www.w3.org/2000/09/xmldsig#";
                }
                return "http://www.w3.org/2001/12/soap-envelope";
            }                
            public String getPrefix(String p) {
                return "tns";
            }
            public Iterator<Object> getPrefixes(String p) {
                return null;
            }
        });	   
                
        Node node = ((Node) xpath.compile("/tns:CertOrigin/CODEH/CertificationEH/CertificateID/text()").evaluate(doc, XPathConstants.NODE));        
        node.setNodeValue(certificadoOrigen.getEhCert().getCertificadoId());
        node = ((Node) xpath.compile("/tns:CertOrigin/CODEH/CertificationEH/CertificateDate/text()").evaluate(doc, XPathConstants.NODE));
        node.setNodeValue(certificadoOrigen.getEhCert().getFechaCertificado());

        logger.debug("actualizarXML::CertificateID::"+certificadoOrigen.getEhCert().getCertificadoId());
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();        
        OutputStream out = new ByteArrayOutputStream();
        StreamResult streamResult = new StreamResult(); 
        streamResult.setOutputStream(out);
        transformer.transform(new DOMSource(doc), streamResult);
        
        return streamResult.getOutputStream().toString().getBytes();
    	
    }
    
    public String produccionControlada(Long drId) throws Exception {
    	return iopDAO.produccionControlada(drId);
    }
    
    public RespuestaCert obtenerCertificado(HashUtil<String, Object> filter) throws Exception {
		return iopDAO.obtenerCertificado(filter);
    }
    
    public String tipoFirma(Long drId) throws Exception {
    	return iopDAO.tipoFirma(drId);
    }

    public String permiteFirmaDigital(Long ordenId) throws Exception {
    	return iopDAO.permiteFirmaDigital(ordenId);
    }

}
