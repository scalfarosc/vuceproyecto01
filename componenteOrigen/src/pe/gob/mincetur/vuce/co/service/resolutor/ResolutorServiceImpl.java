package pe.gob.mincetur.vuce.co.service.resolutor;

import java.util.Map;

import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.resolutor.ResolutorDAO;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.AdjuntoCertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;

/**
 * Clase implementadora de servicios para el Certificado de Origen.
 * @author Ricardo montes
 */

public class ResolutorServiceImpl implements ResolutorService {

	private ResolutorDAO resolutorDAO;

	/**
	 * Registra la aceptación de la designación
	 */
	public void designaEvaluador(Map<String,Object> parametros) throws Exception {
    	resolutorDAO.designaEvaluador(parametros);
    	return;
	}

	/**
	 * Registra la aceptación de la designación TCE
	 */
    public CertificadoOrigen aceptaDesignacionTCE(CertificadoOrigen co) throws Exception {
    	return resolutorDAO.aceptaDesignacionTCE(co);
	}

    /**
	 * Registra el rechazo de la designación TCE
	 */
    public CertificadoOrigen rechazaDesignacionTCE(CertificadoOrigen co) throws Exception {
    	return resolutorDAO.rechazaDesignacionTCE(co);
	}

    /**
     * Registra la calificación del evaluador
     */
	public DeclaracionJurada evaluadorDJCalifica(DeclaracionJurada dj) throws Exception {
    	return resolutorDAO.evaluadorDJCalifica(dj);
	}

    /**
     * Registra la calificación del evaluador, Aprueba y Transmite
     */
	public DeclaracionJurada evaluadorDJCalificaApruebaTransmite(DeclaracionJurada dj) throws Exception {
    	return resolutorDAO.evaluadorDJCalificaApruebaTransmite(dj);
	}
	
    /**
     * Registra la no calificación del evaluador
     */
	public DeclaracionJurada evaluadorDJNoCalifica(DeclaracionJurada dj) throws Exception {
    	return resolutorDAO.evaluadorDJNoCalifica(dj);
	}
	
    /**
     * Registra la no calificación del evaluador, Deniega y Transmite
     */
	public DeclaracionJurada evaluadorDJNoCalificaDeniegaTransmite(DeclaracionJurada dj) throws Exception {
    	return resolutorDAO.evaluadorDJNoCalificaDeniegaTransmite(dj);
	}

	/**
	 * Inserta el registro de un dr en borrador
	 */
	public void registrarBorradorDr(CertificadoOrigenDR coDR) throws Exception {
		this.resolutorDAO.registrarBorradorDr(coDR);
	}

	/**
	 * Genera el registro de un dr en borrador
	 */
	public void generarBorradorDr(HashUtil params) throws Exception {
		this.resolutorDAO.generarBorradorDr(params);
	}

	/**
	 * Genera el registro de un dr en borrador
	 */
	public void eliminarBorradorDr(HashUtil params) throws Exception {
		this.resolutorDAO.eliminarBorradorDr(params);
	}

	/**
	 * Genera el registro de un dr en borrador
	 */
	public void actualizarObsBorradorDr (HashUtil params) throws Exception {
		this.resolutorDAO.actualizarObsBorradorDr(params);
	}

	/**
	 * Confirma el registro de un dr en borrador
	 */
	public HashUtil<String, Object> confirmarBorradorDr(HashUtil<String, Object> parametros) throws Exception {//2018.06.06 JMC Produccion Controlada Alianza Pacifico
		return this.resolutorDAO.confirmarBorradorDr(parametros);
	}

	/**
	 * Inserta el registro de un dr en borrador
	 */
	public CertificadoOrigenDR getCODrResolutorById(Map<String,Object> parametros) throws Exception {
		return this.resolutorDAO.getCODrResolutorById(parametros);
	}

	/**
	 * Registra un adjunto de la DR
	 */
	public void cargarAdjuntoCODR(AdjuntoCertificadoOrigenDR param) throws Exception {
		this.resolutorDAO.cargarAdjuntoCODR(param);
	}

    public DR getDRById(Long drId, Integer sdr) throws Exception {
    	return resolutorDAO.getDRById(drId, sdr);
    }

	/**
	 * Cierra la suce
	 */
	public void cerrarSuce(Map<String,Object> parametros) throws Exception {
		this.resolutorDAO.cerrarSuce(parametros);
	}

	public void denegarSolicitud(Map<String, Object> filtros) throws Exception {
		this.resolutorDAO.denegarSolicitud(filtros);
		return;
	}

	/**
	 * Metodo para que el evaluador actualice los datos de la dj
	 */
    public void actualizarDjXEvaluador(Map<String,Object> parametros) throws Exception {
    	this.resolutorDAO.actualizarDjXEvaluador(parametros);
    }

	/**
	 * Finaliza la evaluacion de DJs
	 */
	public void confirmarFinDJEval(Map<String,Object> parametros) throws Exception {
		this.resolutorDAO.confirmarFinDJEval(parametros);
	}

    public String obtenerTipoDr(Long drId, Integer sdr) throws Exception {
    	return this.resolutorDAO.obtenerTipoDr(drId, sdr);
    }

    public String obtenerTipoDrBorrador(Long borradorDrId) throws Exception {
    	return this.resolutorDAO.obtenerTipoDrBorrador(borradorDrId);
    }

    public boolean drEsRectificacion(Long drId, Integer sdr) throws Exception {
        return this.resolutorDAO.drEsRectificacion(drId, sdr);
    }

    public DR getDRBorradorById(Long drBorradorId) throws Exception {
        return this.resolutorDAO.getDRBorradorById(drBorradorId);
    }

	/**
	 * Inserta el registro de una rectificacion de dr en borrador
	 */
	public CertificadoOrigenDR getCODrRectificacionById(Map<String,Object> parametros) throws Exception {
		return this.resolutorDAO.getCODrRectificacionById(parametros);
	}

	public void setResolutorDAO(ResolutorDAO resolutorDAO) {
		this.resolutorDAO = resolutorDAO;
	}

	/**
	 * Registra Borrador de Notificación de Subsanación.
	 */
	public Integer registrarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
		return this.resolutorDAO.registrarNotificacionSubsanacionOrden(filter);
	}

	/**
	 * Actualiza Borrador de Notificación de Subsanación.
	 */
	public void actualizarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
		this.resolutorDAO.actualizarNotificacionSubsanacionOrden(filter);
	}

	/**
	 * Envia la Notificación de Subsanación
	 */
	public void enviarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
		this.resolutorDAO.enviarNotificacionSubsanacionOrden(filter);
	}

	/**
	 * Elimina una Notificación de Subsanación
	 */
	public void eliminarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
		this.resolutorDAO.eliminarNotificacionSubsanacionOrden(filter);
	}

	/**
	 * Registra los datos de la firma de adjunto de la DR
	 */
	public void cargarDatosFirmaAdjuntoCODR(HashUtil<String, Object> filter) throws Exception {
		this.resolutorDAO.cargarDatosFirmaAdjuntoCODR(filter);
	}

	/**
	 * Devuelve 0 si no hay subsanaciones por atender y 1 si es que existen subsanaciones pendientes
	 *
	 * @param suceId
	 * @return
	 * @throws Exception
	 */
	public String subsanacionNoAtendida(Long suceId) throws Exception {
		return resolutorDAO.subsanacionNoAtendida(suceId);
	}

	/**
	 * Registra la trazabilidad antes de finalizar el DR
	 *
	 * @param drId
	 * @throws Exception
	 */
    public void confirmarDRFinFirma(Long drId, Long sdr) throws Exception {
    	this.resolutorDAO.confirmarDRFinFirma(drId, sdr);
    }

    /**
     * Devuelve N si no se debe mostrar el botón de impresión y S si se debe mostrar dicho botón
     *
     * @param suceId
     * @param drId
     * @param sdr
     * @return
     * @throws Exception
     */
	public String habilitarBtnFirma(Long suceId, Long drId, Long sdr) throws Exception {
		return this.resolutorDAO.habilitarBtnFirma(suceId, drId, sdr);
	}

	/**
	 * Devuelve el numero de DJs que el evaluador haya indicado que no califican
	 * 
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
    public int numDJsRechazadas(Map<String,Object> parametros) throws Exception{
    	return this.resolutorDAO.numDJsRechazadas(parametros);
    }

	/**
	 * Devuelve un indicador para mostrar u ocultar el boton de finalización de la SUCE
	 * 
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
    public String mostrarBtnFinalizacion(Map<String,Object> parametros) throws Exception {
    	return this.resolutorDAO.mostrarBtnFinalizacion(parametros);
    }

}
