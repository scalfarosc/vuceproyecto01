package pe.gob.mincetur.vuce.co.service.certificadoOrigen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.certificadoOrigen.CertificadoOrigenDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.AdjuntoRequeridoDJMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;

/**
 * Clase implementadora de servicios para el Certificado de Origen.
 * @author Erick Oscátegui
 */

public class CertificadoOrigenServiceImpl implements CertificadoOrigenService {

	private CertificadoOrigenDAO certificadoOrigenDAO;

	/**
	 * Obtiene un Certificado de Origen por su id
	 */
	public CertificadoOrigen getCertificadoOrigenById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCertificadoOrigenById(filter);
	}

	/**
	 * Obtiene una factura por su id
	 */
	public COFactura getCOFacturaById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCOFacturaById(filter);
	}

	/**
	 * Obtiene una factura por su id
	 */
	public CertificadoOrigenMercancia getCOMercanciaById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCOMercanciaById(filter);
	}

	/**
	 * Actualiza el Certificado de Origen
	 */
    public void updateCertificadoOrigen(CertificadoOrigen certificadoOrigen) throws Exception {
    	certificadoOrigenDAO.updateCertificadoOrigen(certificadoOrigen);
	}

	/**
	 * Carga adjuntos de Certificado de Origen
	 */
    public void cargarAdjuntoCertificadoOrigen(AdjuntoCertificadoOrigen adjunto) throws Exception {
    	certificadoOrigenDAO.cargarAdjuntoCertificadoOrigen(adjunto);
	}

	/**
	 * Inserta una factura del Certificado de Origen
	 */
    public void insertCOFactura(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.insertCOFactura(filter);
	}


	/**
	 * Actualiza una factura del Certificado de Origen
	 */
    public void updateCOFactura(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.updateCOFactura(filter);
	}


	/**
	 * Elimina una factura del Certificado de Origen
	 */
    public void deleteCOFactura(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.deleteCOFactura(filter);
	}

	/**
	 * Obtiene información de Mercancía
	 */
	public CertificadoOrigenMercancia getCertificadoOrigenMercanciaById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCertificadoOrigenMercanciaById(filter);
	}

	/**
	 * Inserta una Mercancía con DJ existente
	 */
    public void insertCertificadoOrigenMercanciaDjExistente(HashUtil<String, Object> filter) throws Exception {
		certificadoOrigenDAO.insertCertificadoOrigenMercanciaDjExistente(filter);
	}

	/**
	 * Inserta una Mercancía con DJ nueva
	 */
    public void insertCertificadoOrigenMercanciaDjNueva(CertificadoOrigenMercancia certificadoOrigenMercancia) throws Exception {
		certificadoOrigenDAO.insertCertificadoOrigenMercanciaDjNueva(certificadoOrigenMercancia);
	}
    /**
     * Inserta una Mercancía
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
    public void insertCertificadoOrigenMercancia(HashUtil<String, Object> filter) throws Exception {
		certificadoOrigenDAO.insertCertificadoOrigenMercancia(filter);
	}
    /**
     * Modifica una Mercancía
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
    public void updateCertificadoOrigenMercancia(HashUtil<String, Object> filter) throws Exception {
		certificadoOrigenDAO.updateCertificadoOrigenMercancia(filter);
	}

    /**
     * Modifica una DJ
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
    public void updateMercanciaDeclaracionJurada(DeclaracionJurada dj) throws Exception {
		certificadoOrigenDAO.updateMercanciaDeclaracionJurada(dj);
	}

    /**
     * Elimina una Mercancía
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
	public void deleteCertificadoOrigenMercancia(HashUtil<String, Object> filter) throws Exception {
		certificadoOrigenDAO.deleteCertificadoOrigenMercancia(filter);
	}

    /**
     * Elimina una Mercancía
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
	public void deleteMercanciaDeclaracionJurada(DeclaracionJurada dj) throws Exception {
		certificadoOrigenDAO.deleteMercanciaDeclaracionJurada(dj);
	}

    /**
     * Inserta una Declaración jurada
     * @param declaracionJurada
     * @throws Exception
     */
    public void insertDeclaracionJurada(DeclaracionJurada declaracionJurada) throws Exception {
		certificadoOrigenDAO.insertDeclaracionJurada(declaracionJurada);
	}

    /**
     * Inserta una Declaración jurada Sin Mercancia
     * @param declaracionJurada
     * @throws Exception
     */
    public void insertDeclaracionJuradaSinMercancia(DeclaracionJurada declaracionJurada) throws Exception {
		certificadoOrigenDAO.insertDeclaracionJuradaSinMercancia(declaracionJurada);
	}

    /**
     * Modifica una Declaración jurada
     * @param declaracionJurada
     * @throws Exception
     */
    public void updateDeclaracionJurada(DeclaracionJurada declaracionJurada) throws Exception {
		certificadoOrigenDAO.updateDeclaracionJurada(declaracionJurada);
	}

    /**
     * Modifica el consolidado de una Declaración jurada
     * @param declaracionJurada
     * @throws Exception
     */
    public void updateDeclaracionJuradaConsolidado(DeclaracionJurada declaracionJurada) throws Exception {
		certificadoOrigenDAO.updateDeclaracionJuradaConsolidado(declaracionJurada);
	}

	/**
	 * Carga adjuntos de Declaración Jurada y de Materiales de Declaración Jurada
	 */
	public void cargarAdjuntoDeclaracionjurada(AdjuntoCertificadoOrigen adjunto) throws Exception {
		certificadoOrigenDAO.cargarAdjuntoDeclaracionJurada(adjunto);
	}

	/**
	 * Obtiene información de Material de Declaración Jurada
	 */
	public DeclaracionJuradaMaterial getDeclaracionJuradaMaterialById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getDeclaracionJuradaMaterialById(filter);
	}

	/**
	 * Inserta un Material de Declaración Jurada
	 */
    public void insertDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		certificadoOrigenDAO.insertDeclaracionJuradaMaterial(declaracionJuradaMaterial);
	}

    /**
     * Modifica un Material de Declaración Jurada
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
    public void updateDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		certificadoOrigenDAO.updateDeclaracionJuradaMaterial(declaracionJuradaMaterial);
	}

    /**
     * Elimina un Material de Declaración Jurada
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
	public void deleteDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		certificadoOrigenDAO.deleteDeclaracionJuradaMaterial(declaracionJuradaMaterial);
	}

	/**
	 * Obtiene información de Productor de Declaración Jurada
	 */
	public DeclaracionJuradaProductor getDeclaracionJuradaProductorById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getDeclaracionJuradaProductorById(filter);
	}

	/**
	 * Inserta una Productor de Declaración Jurada
	 */
    public void insertDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		certificadoOrigenDAO.insertDeclaracionJuradaProductor(declaracionJuradaProductor);
	}

    /**
     * Modifica un Productor de Declaración Jurada
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
    public void updateDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		certificadoOrigenDAO.updateDeclaracionJuradaProductor(declaracionJuradaProductor);
	}

    /**
     * Elimina un Productor de Declaración Jurada
     * @param certificadoOrigenMercancia
     * @throws Exception
     */
	public void deleteDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		certificadoOrigenDAO.deleteDeclaracionJuradaProductor(declaracionJuradaProductor);
	}

	/**
	 * Obtiene una declaracion jurada por su id
	 */
	public DeclaracionJurada getCODeclaracionJuradaById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCertificadoOrigenDeclaracionJuradaById(filter);
	}

	/**
	 * Obtiene la lista de productores por el id de la declaracion jurada
	 */
	public List<DeclaracionJuradaProductor> getDJProductorByDJId(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getDJProductorByDJId(filter);
	}

	/**
	 * Obtiene el DR del Certificado de Origen
	 */
	public HashMap<String, Object> getCertificadoOrigenDRById(HashUtil<String, Object> filter) throws Exception {
    	return certificadoOrigenDAO.getCertificadoOrigenDRById(filter);
    }

	/**
	 * Obtiene el DR de las Mercancias del Certificado de Origen
	 */
	public HashMap<String, Object> getCOMercanciaDRById(HashUtil<String, Object> filter) throws Exception {
    	return certificadoOrigenDAO.getCOMercanciaDRById(filter);
    }

	/**
	 * Obtiene el DR de las Facturas del Certificado de Origen
	 */
	public HashMap<String, Object> getCOFacturaDRById(HashUtil<String, Object> filter) throws Exception {
    	return certificadoOrigenDAO.getCOFacturaDRById(filter);
    }

	/**
	 * Obtiene los datos de un adjunto requerido
	 */
    public AdjuntoRequeridoDJMaterial loadRequerido(HashUtil<String, Object> filter) throws Exception {
        return certificadoOrigenDAO.loadAdjuntoRequeridoDJ(filter);
    }

    /**
     *
     * @param adjunto
     * @throws Exception
     */
    public void insertAdjuntoDJ(AdjuntoCertificadoOrigen adjunto) throws Exception {
    	certificadoOrigenDAO.cargarAdjuntoDeclaracionJurada(adjunto);
    }

    /**
     *
     * @param filter
     * @throws Exception
     */
    public void eliminarAdjuntoDJ(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.eliminarAdjuntoDJ(filter);
    }

    /**
     * Duplicado
     */

	public CODuplicado getCertificadoOrigenMTC002ById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCertificadoOrigenMTC002ById(filter);
	}

    public void updateCertificadoOrigenDuplicado(CODuplicado coDuplicado) throws Exception {
    	certificadoOrigenDAO.updateCertificadoOrigenDuplicado(coDuplicado);
	}

    public void insertCertificadoOrigenDuplicadoOrigen(CODuplicado coDuplicado) throws Exception {
    	certificadoOrigenDAO.insertCertificadoOrigenDuplicadoOrigen(coDuplicado);
	}

	public void setCertificadoOrigenDAO(CertificadoOrigenDAO certificadoOrigenDAO) {
		this.certificadoOrigenDAO = certificadoOrigenDAO;
	}

	public String getCausalById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCausalById(filter);
	}

	/**
     * Anulacion
     */

	public COAnulacion getCertificadoOrigenMTC004ById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCertificadoOrigenMTC004ById(filter);
	}

    public void updateCertificadoOrigenAnulacion(COAnulacion coAnulacion) throws Exception {
    	certificadoOrigenDAO.updateCertificadoOrigenAnulacion(coAnulacion);
	}

    public void insertCertificadoOrigenAnulacionOrigen(COAnulacion coAnulacion) throws Exception {
    	certificadoOrigenDAO.insertCertificadoOrigenAnulacionOrigen(coAnulacion);
	}

    /**
     * Vaiidación de DJ
     */

    public void solicitaValidacionProductor(Map<String,Object> parametros) throws Exception {
    	certificadoOrigenDAO.solicitaValidacionProductor(parametros);
    }

    public void aceptaSolicitudValidacion(Map<String,Object> parametros) throws Exception {
    	certificadoOrigenDAO.aceptaSolicitudValidacion(parametros);
    }

    public void rechazaSolicitudValidacion(Map<String,Object> parametros) throws Exception {
    	certificadoOrigenDAO.rechazaSolicitudValidacion(parametros);
    }

    public void transmiteDjValidada(Map<String,Object> parametros) throws Exception {
    	certificadoOrigenDAO.transmiteDjValidada(parametros);
    }

    public void insertCertificadoOrigenReemplazoOrigen(COReemplazo coReemplazo) throws Exception {
    	certificadoOrigenDAO.insertCertificadoOrigenReemplazoOrigen(coReemplazo);
	}

    public void updateCertificadoOrigenReemplazo(COReemplazo coReemplazo) throws Exception {
    	certificadoOrigenDAO.updateCertificadoOrigenReemplazo(coReemplazo);
	}

    public HashUtil<String, Object> insertSubsanacionSolicitud(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.insertSubsanacionSolicitud(filter);

    	return filter;
	}

    public void deleteSubsanacionSolicitud(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.deleteSubsanacionSolicitud(filter);
	}

    public int mtoCertificadoVigente(Long ordenId) throws Exception {
    	return certificadoOrigenDAO.mtoCertificadoVigente(ordenId);
	}

	public Long getAdjunFirmaId(HashUtil<String, Object> filter) throws Exception {
    	return certificadoOrigenDAO.getAdjunFirmaId(filter);
	}

    public void registraExportadorAutorizadoDj(HashUtil<String, Object> parametros) throws Exception {
    	certificadoOrigenDAO.registraExportadorAutorizadoDj(parametros);
	}

    public void revocaExportadorAutorizadoDj(HashUtil<String, Object> parametros) throws Exception {
    	certificadoOrigenDAO.revocaExportadorAutorizadoDj(parametros);
	}

	public String devuelveEmpresaId(String tipoDoc, String numeroDoc) throws Exception {
		return certificadoOrigenDAO.devuelveEmpresaId(tipoDoc, numeroDoc);
	}

	public String devuelveEmpresaIdOUsuPrincipalIdByUsuId(String tipoDoc, String numeroDoc) throws Exception {
		return certificadoOrigenDAO.devuelveEmpresaIdOUsuPrincipalIdByUsuId(tipoDoc, numeroDoc);
	}
	
	/**
	 * Obtiene una declaracion jurada por su id
	 */
	public void actualizarFlgDjCompleta(CalificacionOrigen filter) throws Exception {
		certificadoOrigenDAO.actualizarFlgDjCompleta(filter);
	}

	/**
	 * Realiza la busqueda de partidas arancelarias
	 */
	public void obtenerPartidas(HashUtil<String, Object> filter) throws Exception{
		this.certificadoOrigenDAO.obtenerPartidas(filter);
	}

	/**
	 * Invoca la funcion que determina si se debe mostrar el boton para requerir la validacion del productor para la dj
	 */
	public String djBtnValidacionProd(HashUtil<String, Object> filter) throws Exception {
		return this.certificadoOrigenDAO.djBtnValidacionProd(filter);
	}

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	public void updateDireccionAdicionalCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
		certificadoOrigenDAO.updateDireccionAdicionalCalificacionOrigen(filter);
	}
	
	/**
	 * Obtiene una Calificación de Origen por su Id
	 */
	public CalificacionOrigen getCalificacionOrigenById(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.getCalificacionOrigenById(filter);
	}

    /**
     * Registra una Calificación de Origen
     */
    public void insertCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.insertCalificacionOrigen(filter);
	}

    /**
     * Actualiza información de Rol de una Calificación de Origen
     */
    public void updateRolCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.updateRolCalificacionOrigen(filter);
	}

    /**
     * Actualiza información de Criterio de una Calificación de Origen
     */
    public void updateCriterioCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.updateCriterioCalificacionOrigen(filter);
	}

    /**
     * Registra una Declaración Jurada que tiene asociada una Calificación de Origen
     */
    public void insertDeclaracionJuradaCalificacion(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.insertDeclaracionJuradaCalificacion(filter);
	}

    /**
     * Devuelve 1 si la dj esta en proceso de validacion
     */
	public String djEnProcesoValidacion(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.djEnProcesoValidacion(filter);
	}

    /**
     * Registra una Calificación de Origen junto a una Mercancia y una Declaración Jurada
     */
    public void insertCalificacionOrigenConMercanciaDeclaracionJurada(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.insertCalificacionOrigenConMercanciaDeclaracionJurada(filter);
	}

    /**
     * Registra un mercancía a partir de una calificacion existente
     */
    public HashUtil<String, Object> enlazaDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception {
    	return this.certificadoOrigenDAO.enlazaDJCalificacionMercancia(filter);
    }

    /**
     * Registra un mercancía nueva a partir de una calificacion existente
     */
    public HashUtil<String, Object> copiarDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception {
    	return this.certificadoOrigenDAO.copiarDJCalificacionMercancia(filter);
    }

    /*
     * Devuelve el número de materiales registrados para una DJ determinada
     */
    public int cuentaDJMateriales(HashUtil<String, Object> filter){
    	return this.certificadoOrigenDAO.cuentaDJMateriales(filter);
    }


    public DeclaracionJurada getDjByCalificacionUoId(HashUtil<String, Object> filter, String enIngles) throws Exception{
    	return this.certificadoOrigenDAO.getDjByCalificacionUoId(filter, enIngles);

    }

    /**
     * Registra un calificación nueva a partir de una calificacion existente (para MCT005)
     */
    public HashUtil<String, Object> copiarDJCalificacionXCalificacion(HashUtil<String, Object> filter) throws Exception {
    	return this.certificadoOrigenDAO.copiarDJCalificacionXCalificacion(filter);
    }

    /**
     * Devuelve S si todos los materiales originarios de la dj tienen archivos adjuntos
     */
	public String djMaterialTieneAdj(HashUtil<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.djMaterialTieneAdj(filter);
	}
	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	/**
	 * Indica si una dj requiere ser validada por el productor
	 */
	public String reqValidacionProductor(HashMap<String, Object> filter) throws Exception{
		return certificadoOrigenDAO.reqValidacionProductor(filter);
	}

	/**
	 * Indica si una dj ha sido validada
	 */
	public String djValidada(HashMap<String, Object> filter) throws Exception{
		return certificadoOrigenDAO.djValidada(filter);
	}

	/**
	 * Indica si existe un productor validador
	 * NPCS: 08/02/2018
	 */
	public String existeProductorValidador(HashMap<String, Object> filter) throws Exception {
		return certificadoOrigenDAO.existeProductorValidador(filter);
	}

	/**
	 * Devuelve un mensaje de error si el hash de la calificación es igual al hash de la calificacion original
	 */
	public void validaHashCalificacion(HashMap<String, Object> filter) throws Exception{

		filter.put("hash", certificadoOrigenDAO.obtenerCalificacionHash(filter));

		certificadoOrigenDAO.obtenerComparaCalificacionHash(filter);
	}

	/******************************************************************************************************************************/
    public String obtenerIdiomaLlenado(Integer idAcuerdo) throws Exception {
		return certificadoOrigenDAO.obtenerIdiomaLlenado(idAcuerdo);
    }

	/*
	 * Ejecuta la validación que verifica que no se exceda el numero de lineas para la impresion de las mercancias
	 */
	public void validarCertificadoEspacioDet(HashMap<String, Object> filter) throws Exception{

		certificadoOrigenDAO.validarCertificadoEspacioDet(filter);
	}

	/**
	 * Obtiene la suma de los valores US$ de los materiales de una dj
	 */
	public double totalValorUSMateriales(HashMap<String, Object> filter) throws Exception{
		return certificadoOrigenDAO.totalValorUSMateriales(filter);
	}

	/**
	 * Obtiene los datos del propietario de la DJ calificada
	 */
	public HashUtil<String, Object> obtenerDatosPropietarioDJ(HashUtil<String, Object> filter) throws Exception {
		
		return certificadoOrigenDAO.obtenerDatosPropietarioDJ(filter);
	}
	
	
	//20140825_JMC_Intercambio_Empresa
	/**
	 * Actualiza el Certificado de Origen de Empresa
	 */
    public int insertCertificadoOrigenEmpresa(CertificadoOrigen certificadoOrigen) throws Exception {
    	return certificadoOrigenDAO.insertCertificadoOrigenEmpresa(certificadoOrigen);
	}
    
	/**
	 * Actualiza el Factura de Origen de Empresa
	 */
    public void insertCOFacturaEmpresa(COFactura factura) throws Exception {
    	certificadoOrigenDAO.insertCOFacturaEmpresa(factura);
	}
    
	/**
	 * Actualiza el Mercancia de Origen de Empresa
	 */
    public void insertCertificadoOrigenMercanciaEmpresa(CertificadoOrigenMercancia certificadoOrigenMercancia) throws Exception {
    	certificadoOrigenDAO.insertCertificadoOrigenMercanciaEmpresa(certificadoOrigenMercancia);
	}
    
	/**
	 * Inserta la Declaracion Jurada
	 */
    public int insertDeclaracionJuradaEmpresa(DeclaracionJurada declaracionJurada) throws Exception {
    	return certificadoOrigenDAO.insertDeclaracionJuradaEmpresa(declaracionJurada);
	}
    
	/**
	 * Actualiza el Factura de Origen de Empresa
	 */
    public void insertDeclaracionJuradaProductorEmpresa(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
    	certificadoOrigenDAO.insertDeclaracionJuradaProductorEmpresa(declaracionJuradaProductor);
	}
    
	/**
	 * Actualiza el Mercancia de Origen de Empresa
	 */
    public void insertDeclaracionJuradaMaterialEmpresa(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
    	certificadoOrigenDAO.insertDeclaracionJuradaMaterialEmpresa(declaracionJuradaMaterial);
	}

    public byte[] loadCertificadoFD(HashUtil<String, Object> filter) throws Exception{
		return certificadoOrigenDAO.loadCertificadoFD(filter);
	}
    
    //NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
    public byte[] loadCertificadoFDXML(HashUtil<String, Object> filter) throws Exception{
		return certificadoOrigenDAO.loadCertificadoFDXML(filter);
	}
    
	public String validaCOFD(HashUtil<String, Object> filter) throws Exception{
		return certificadoOrigenDAO.validaCOFD(filter);
	}
	
	public void insertArchivoCOFD(HashMap<String, Object> filter) throws Exception {
		certificadoOrigenDAO.insertArchivoCOFD(filter);
	}
	
	public void habilitaFirma(HashUtil<String, Object> filter) throws Exception{
		certificadoOrigenDAO.habilitaFirma(filter);
	}

	/**
	 * NPCS: 12/02/2018
	 * Nos dice que usuario solicito la calificación
	 */
	public HashUtil<String, Object> solicitoCalificacion(HashUtil<String, Object> filter) throws Exception {
		
		return certificadoOrigenDAO.solicitanteCalificacion(filter);
	}
	
	//NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
    public void eliminarProductorMercanciasXML(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.eliminarProductorMercanciasXML(filter);
    }
    
    public void eliminarMercanciasXML(HashUtil<String, Object> filter) throws Exception {
    	certificadoOrigenDAO.eliminarMercanciasXML(filter);
    }
	
}
