package pe.gob.mincetur.vuce.co.service.dj;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.dj.DJDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.dj.AdjuntoRequeridoDJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJExportador;
import pe.gob.mincetur.vuce.co.domain.dj.DJMaterial;
import pe.gob.mincetur.vuce.co.domain.dj.DJProductor;


public class DJServiceImpl implements DJService{

	private static DJService instance;

	private DJDAO djDAO;

	public DJServiceImpl() {
		instance = this;
	}

	public static DJService getInstance() {
		return instance;
	}

	public static void setInstance(DJService instance) {
		DJServiceImpl.instance = instance;
	}

	public DJDAO getDjDAO() {
		return djDAO;
	}

	public void setDjDAO(DJDAO dJDAO) {
		djDAO = dJDAO;
	}

	public HashUtil<String, Object> insertDJ(HashUtil<String, Object> filter) throws Exception {
        return djDAO.insertDJ(filter);
    }

    public void insertUsuarioDJ(HashUtil<String, Object> filter) throws Exception {
        djDAO.insertUsuarioDJ(filter);
    }

    public void transmiteDJ(HashUtil<String, Object> filter) throws Exception {
        djDAO.transmiteDJ(filter);
    }

	public void updateDJ(DJ dj) throws Exception {
		djDAO.updateDJ(dj);
	}

	public DJ getDJById(HashUtil<String, Object> filter) throws Exception {
		return djDAO.getDJById(filter);
	}

	public DJMaterial getDJMaterialById(HashUtil<String, Object> filter) throws Exception {
		return djDAO.getDJMaterialById(filter);
	}

	public DJProductor getDJProductorById(HashUtil<String, Object> filter) throws Exception {
		return djDAO.getDJProductorById(filter);
	}

	public DJExportador getDJExportadorById(HashUtil<String, Object> filter) throws Exception {
		return djDAO.getDJExportadorById(filter);
	}

	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter)throws Exception{
		return djDAO.getUsuarioFormatoById(filter);
	}

	public Solicitud getSolicitudById(HashUtil<String, Object> filter)throws Exception{
		return djDAO.getSolicitudById(filter);
	}

	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter)throws Exception{
		return djDAO.getSolicitanteDetail(filter);
	}

	public Integer insertMaterial(DJMaterial obj) throws Exception{
		return djDAO.insertMaterial( obj);
	}
	public void updateMaterial(DJMaterial obj) throws Exception{
		djDAO.updateMaterial( obj);
	}
	public void deleteMaterial(DJMaterial obj) throws Exception{
		djDAO.deleteMaterial(obj);
	}

	public Integer insertProductor(DJProductor obj) throws Exception{
		return djDAO.insertProductor( obj);
	}
	public void updateProductor(DJProductor obj) throws Exception{
		djDAO.updateProductor( obj);
	}
	public void deleteProductor(DJProductor obj) throws Exception{
		djDAO.deleteProductor(obj);
	}

	public Integer insertExportador(DJExportador obj) throws Exception{
		return djDAO.insertExportador( obj);
	}
	public void updateExportador(DJExportador obj) throws Exception{
		djDAO.updateExportador( obj);
	}
	public void deleteExportador(DJExportador obj) throws Exception{
		djDAO.deleteExportador(obj);
	}

	public void actualizarSeleccionEsExportador(HashUtil<String, Object> filter) throws Exception{
		djDAO.actualizarSeleccionEsExportador(filter);
	}

    public AdjuntoRequeridoDJ getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception {
        return djDAO.getAdjuntoRequeridoById(filter);
    }

    public void updateRepresentante(HashUtil<String, Object> filter) throws Exception {
        djDAO.updateRepresentante(filter);
    }

    public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception {
        djDAO.updateCargoDeclarante(filter);
    }

	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception{
		return djDAO.getAdjuntoRequeridoCount(filter);
	}

	public void modificarRol(long djId, int tipoUsuarioDj) throws Exception {
		djDAO.modificarRol(djId, tipoUsuarioDj);
		return;
	}



}
