package pe.gob.mincetur.vuce.co.service;

import org.jlis.core.util.HashUtil;


import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;

public interface InteroperabilidadFirmaService {

    public InteroperabilidadFirma obtenerToken(Long drId, Integer sdr) throws Exception;

    public Integer registrarInteroperabilidad(InteroperabilidadFirma iop) throws Exception;
		
    public InteroperabilidadFirma obtenerEtapa(String token, Integer secuencia)  throws Exception;
	
    public byte[] generarXml(String token)  throws Exception;
    
    public byte[]  generarPdf(String token)  throws Exception;

    public Integer enviarXmlFirmado(String token, byte[] xml) throws Exception;
    
    public Integer enviarPdfFirmado(String token, byte[] pdf) throws Exception;
    
    public void actualizaFirmaExportador(Long drId, Integer sdr, String nombreExp, String formato, String posteriori) throws Exception;
    
    public InteroperabilidadFirma obtenerPdf(String token, Integer secuencia) throws Exception;
    
    public String produccionControlada(Long drId) throws Exception;
    
    public RespuestaCert obtenerCertificado(HashUtil<String, Object> filter) throws Exception;
    
    public String tipoFirma(Long drId) throws Exception;
    
    public String permiteFirmaDigital(Long ordenId) throws Exception;
   
}
