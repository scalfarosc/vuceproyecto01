package pe.gob.mincetur.vuce.co.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.AdjuntoDAO;
import pe.gob.mincetur.vuce.co.dao.SuceDAO;
import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class SuceServiceImpl implements SuceService {

	private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

	private SuceDAO suceDAO;
	private AdjuntoDAO adjuntoDAO;

	/**
	 * Obtiene Suce por Id.
	 */
	public Suce loadSuceById(long suce) throws Exception {
		return suceDAO.loadSuceById(suce);
	}

	/**
	 * Obtiene Suce por Id y Usuario.
	 */
	public Suce loadSuceByIdAndUsuarioId(long suce, long usuarioId, long rol) throws Exception {
		return suceDAO.loadSuceByIdAndUsuarioId(suce, usuarioId, rol);
	}

	/**
	 * Registra Borrador de Notificación de Subsanación.
	 */
	public Integer registrarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
		return suceDAO.registrarBorradorNotificacionSubsanacion(filter);
	}

	/**
	 * Actualiza Borrador de Notificación de Subsanación.
	 */
	public void actualizarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
		suceDAO.actualizarBorradorNotificacionSubsanacion(filter);
	}

	/**
	 * Envia Borrador de Notificación de Subsanación
	 */
	public void enviarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
		suceDAO.enviarBorradorNotificacionSubsanacion(filter);
	}

	/**
	 * Elimina Borrador de Notificación de Subsanación
	 */
	public void eliminarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
		suceDAO.eliminarBorradorNotificacionSubsanacion(filter);
	}

	/**
	 * Obtiene una Modificación de Suce por id y id de mensaje
	 */
	public ModificacionSuce loadModificacionSuceByIdMensajeId(HashUtil<String, Object> filter) throws Exception {
		return suceDAO.loadModificacionSuceByIdMensajeId(filter);
	}

    public ModificacionDR loadModificacionDrById(HashUtil<String, Object> filter) throws Exception {
        return suceDAO.loadModificacionDrById(filter);
    }

	/**
	 * Aprueba una Modificación de Subsanación
	 * @param suce
	 * @param mts
	 * @throws Exception
	 */
	public void aprobarModificacion(int suce, int modificacionSuce) throws Exception {
		suceDAO.aprobarModificacion(suce, modificacionSuce);
	}

	/**
	 * Rechaza una Modificación de Subsanación
	 */
	public void rechazarModificacion(Orden orden, Suce suce, Integer modificacionSuce, String mensaje, List<Adjunto> adjuntos) throws Exception {
		Integer [] mensajesId = suceDAO.rechazarModificacion(suce.getSuce(), modificacionSuce, mensaje);
        
		if (adjuntos != null && adjuntos.size() > 0) {
	        for (Adjunto adjunto : adjuntos) {
				AdjuntoFormato adjuntoF = new AdjuntoFormato();
				adjuntoF.setOrdenId(suce.getIdOrden());
				adjuntoF.setMto(orden.getMto());
				adjuntoF.setFormatoId(suce.getIdFormato());
				adjuntoF.setArchivo(adjunto.getArchivo());
				adjuntoF.setNombre(adjunto.getNombre());
				adjuntoF.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);

				// Insertamos el adjunto relacionado al buzon
				adjuntoF.setMensajeId(mensajesId[0]);
				adjuntoDAO.insertAdjuntoSuce(adjuntoF);

				// Insertamos el adjunto relacionado al mensaje de rechazo
				adjuntoF.setMensajeId(mensajesId[1]);
				adjuntoDAO.insertAdjuntoSuce(adjuntoF);
			}
		}
	}

	public HashUtil<String, Object> insertSubsanacion(HashUtil<String, Object> filter, List<String> notificaciones) throws Exception {
		HashUtil<String, Object> element = suceDAO.insertSubsanacion(filter);
		int modifSuceId = element.getInt("modificacionSuceId");
		if (modifSuceId != 0) {
			for (String notificacion : notificaciones) {
				HashUtil<String, Object> filterNotificaciones = new HashUtil<String, Object>();
				filterNotificaciones.put("notificacion", Integer.valueOf(notificacion).intValue());
				filterNotificaciones.put("modificacionSuce", modifSuceId);

				suceDAO.atiendeNotificacion(filterNotificaciones);
			}
		}
		return element;
	}

    public HashUtil<String, Object> insertModificacionDr(HashUtil<String, Object> filter) throws Exception {
        HashUtil<String, Object> element = suceDAO.insertModificacionDr(filter);
        return element;
    }

    public void transmiteModificacionDr(HashUtil<String, Object> filter) throws Exception {
        suceDAO.transmiteModificacionDr(filter);
    }

    public void actualizaModificacionDr(HashUtil<String, Object> filter) throws Exception {
        suceDAO.actualizaModificacionDr(filter);
    }

    public void eliminaModificacionDr(HashUtil<String, Object> filter) throws Exception {
        suceDAO.eliminaModificacionDr(filter);
    }

    public void transmiteSubsanacion(HashUtil<String, Object> filter) throws Exception {
        suceDAO.transmiteSubsanacion(filter);
    }

    public void actualizaSubsanacion(int modifSuceId, HashUtil<String, Object> filter, List<String> notificaciones) throws Exception {
        suceDAO.actualizaSubsanacion(filter);

        // filter.clear();
        // filter.put("modificacionSuce", modifSuceId);
        // suceDAO.eliminarNotificacionesModificacionSuce(filter);
        for (String notificacion : notificaciones) {
            filter.clear();
            filter.put("notificacion", Integer.valueOf(notificacion).intValue());
            filter.put("modificacionSuce", modifSuceId);

            suceDAO.atiendeNotificacion(filter);
        }
    }

    public String suceFlgPendienteCalif(Integer suceId){
    	try {
			return this.suceDAO.suceFlgPendienteCalif(suceId);
		} catch (Exception e) {
			return ConstantesCO.OPCION_NO;
		}
    }

	public void aceptacionDesestimientoSuce(Integer vcId, int suce) throws Exception {
		suceDAO.aceptacionDesestimientoSuce(vcId, suce);
	}

	public void rechazoDesestimientoSuce(int suce) throws Exception {
		suceDAO.rechazoDesestimientoSuce(suce);
	}

	public void desistirSuce(HashUtil<String, Object> filter) throws Exception {
		suceDAO.desistirSuce(filter);
	}

    public void eliminaSubsanacion(HashUtil<String, Object> filter) throws Exception {
        suceDAO.eliminaSubsanacion(filter);
    }

    public void aprobarModificacionDr(HashUtil<String, Object> filter) throws Exception {
    	suceDAO.aprobarModificacionDr(filter);
    }

    public void rechazarModificacionDr(HashUtil<String, Object> filter) throws Exception {
    	suceDAO.rechazarModificacionDr(filter);
    }

    public HashUtil<String, Object> registrarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
    	return suceDAO.registrarRectificacionDrEval(filter);
    }

    public HashUtil<String, Object> eliminarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
    	return suceDAO.eliminarRectificacionDrEval(filter);
    }

    public void modificarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
    	suceDAO.modificarRectificacionDrEval(filter);
    }

    public void confirmarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
    	suceDAO.confirmarRectificacionDrEval(filter);
    }

	public Suce loadSuceModificacionById(int suceId, int mto) throws Exception {
		return suceDAO.loadSuceModificacionById(suceId, mto);
	}

    public Suce loadSuceByNumeroAndUsuarioId(int numSuce, int usuarioId, int rol) throws Exception {
        return suceDAO.loadSuceByNumeroAndUsuarioId(numSuce, usuarioId, rol);
    }

    public Suce loadSuceByNumero(int numSuce) throws Exception{
    	return suceDAO.loadSuceByNumero(numSuce);
    }

	public SuceDAO getSuceDAO() {
		return suceDAO;
	}

	public void setSuceDAO(SuceDAO suceDAO) {
		this.suceDAO = suceDAO;
	}

	public AdjuntoDAO getAdjuntoDAO() {
		return adjuntoDAO;
	}

	public void setAdjuntoDAO(AdjuntoDAO adjuntoDAO) {
		this.adjuntoDAO = adjuntoDAO;
	}

}