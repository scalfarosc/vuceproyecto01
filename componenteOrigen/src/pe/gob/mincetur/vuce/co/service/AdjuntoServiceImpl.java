package pe.gob.mincetur.vuce.co.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.AdjuntoDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoFormatoCertificado;
import pe.gob.mincetur.vuce.co.procesodr.domain.AdjuntoMensaje;

public class AdjuntoServiceImpl implements AdjuntoService {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

    private AdjuntoDAO adjuntoDAO;

    public void setAdjuntoDAO(AdjuntoDAO adjuntoDAO) {
        this.adjuntoDAO = adjuntoDAO;
    }

    public List<AdjuntoFormatoCertificado> loadList(int orden, int mto) throws Exception {
    	return adjuntoDAO.loadList(orden, mto);
    }

	public Adjunto loadAdjunto(HashUtil<String, Object> filter) throws Exception {
		return adjuntoDAO.loadAdjunto(filter);
    }

    public void insertAdjunto(Adjunto adjunto) throws Exception {
        adjuntoDAO.insertAdjunto(adjunto);
    }

    public void insertAdjuntoSuce(Adjunto adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoSuce(adjunto);
    }

    public void insertAdjuntoResolutorDR(AdjuntoFormatoCertificado adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoResolutorDR(adjunto);
    }

    public void eliminarAdjuntoFormato(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.eliminarAdjuntoFormato(filter);
    }
    public void eliminarAdjuntoById(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.eliminarAdjuntoById(filter);
    }

    public List<AdjuntoFormatoCertificado> loadModifSuceList(int mensajeId) throws Exception{
    	return adjuntoDAO.loadModifSuceList(mensajeId);
    }

    public void insertAdjuntoResolutorBorradorDR(Adjunto adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoResolutorBorradorDR(adjunto);
    }

	public Adjunto loadAdjuntoDR(HashUtil<String, Object> filter) throws Exception {
		return adjuntoDAO.loadAdjuntoDR(filter);
    }

    public void eliminarAdjuntoCalificacionById(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.eliminarAdjuntoCalificacionById(filter);
    }

    public void eliminarAdjuntoCertificadoById(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.eliminarAdjuntoCertificadoById(filter);
    }

    public void insertAdjuntoNotificacion(Adjunto adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoNotificacion(adjunto);
    }

    public void insertAdjuntoResolutorDR(AdjuntoMensaje adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoResolutorDR(adjunto);
    }

    public void insertAdjuntoSubsanacion(AdjuntoMensaje adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoSubsanacion(adjunto);
    }

    public void insertAdjuntoFactura(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.insertAdjuntoFactura(filter);
    }

    public void eliminarAdjuntoFactura(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.eliminarAdjuntoFactura(filter);
    }

    public void insertAdjuntoProductor(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.insertAdjuntoProductor(filter);
    }

    public void eliminarAdjuntoProductor(HashUtil<String, Object> filter) throws Exception {
        adjuntoDAO.eliminarAdjuntoProductor(filter);
    }
    
    //20140825_JMC_Intercambio_Empresa
    public List<Adjunto> loadListForTransmision(String keyQuery, int idTransmision) throws Exception {
        return adjuntoDAO.loadListForTransmision(keyQuery, idTransmision);
    }
    
    public void insertAdjuntoForTransmision(Adjunto adjunto) throws Exception {
        adjuntoDAO.insertAdjuntoForTransmision(adjunto);
    }

	public int insertAdjuntoForTransmisionEmpresa(Adjunto adjunto)
			throws Exception {
        return adjuntoDAO.insertAdjuntoForTransmisionEmpresa(adjunto);
		
	}
    
	public void insertTransmisionForAdjunto(int transmisionEmpresaId,
			int idAdjunto) throws Exception {
		adjuntoDAO.insertTransmisionForAdjunto(transmisionEmpresaId,idAdjunto);
		
	}	
	
    public void insertNombreAdjunto(int mct001TmpId, AdjuntoFormato adjunto) throws Exception {
        adjuntoDAO.insertNombreAdjuntoForTransmision(mct001TmpId, adjunto);
    }

    public void insertNombreAdjuntoDJForTransmision(int mct005TmpId, AdjuntoFormato adjunto) throws Exception {
        adjuntoDAO.insertNombreAdjuntoDJForTransmision(mct005TmpId, adjunto);
    }

    public void insertNombreAdjuntoDJMaterialForTransmision(int mct005TmpId, int secuenciaMaterial, Adjunto adjunto) throws Exception {
        adjuntoDAO.insertNombreAdjuntoDJMaterialForTransmision(mct005TmpId, secuenciaMaterial, adjunto);
    }    

}
