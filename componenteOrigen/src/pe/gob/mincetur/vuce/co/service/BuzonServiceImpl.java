package pe.gob.mincetur.vuce.co.service;

import java.util.HashMap;
import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.AdjuntoDAO;
import pe.gob.mincetur.vuce.co.dao.MensajeDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.Mensaje;

public class BuzonServiceImpl implements BuzonService {

	private MensajeDAO mensajeDAO;
	
	private AdjuntoDAO adjuntoDAO;
	
	public void setMensajeDAO(MensajeDAO mensajeDAO) {
		this.mensajeDAO = mensajeDAO;
	}

	public void setAdjuntoDAO(AdjuntoDAO adjuntoDAO) {
		this.adjuntoDAO = adjuntoDAO;
	}

	public Mensaje loadMensaje(HashUtil<String, Object> filter) throws Exception {
		return mensajeDAO.loadMensaje(filter);
	}
	
	public Mensaje loadMensajeExtranet(HashUtil<String, Object> filter) throws Exception {
		return mensajeDAO.loadMensajeExtranet(filter);
	}
	
	public String loadNotificacion(HashUtil<String, Object> filter) throws Exception {
        return mensajeDAO.loadNotificacion(filter);
    }

	public void updateMensajeLeido(HashUtil<String, Object> filter) throws Exception {
		mensajeDAO.updateMensajeLeido(filter);
    }
	
	public void inactivaMensaje(HashUtil<String, Object> filter) throws Exception{
		mensajeDAO.inactivaMensaje(filter);
	}
	
	public Adjunto loadAdjunto(HashUtil<String, Object> filter) throws Exception{
		return adjuntoDAO.loadAdjunto(filter);
	}
	
	public HashMap<String, Integer> getNotificacion(Integer idTransmision) throws Exception{
		return mensajeDAO.getNotificacion(idTransmision);
	}
	
	public List<String> getNotificacionesFromMensaje(Integer idNotificacion) throws Exception{
		return mensajeDAO.getNotificacionesFromMensaje(idNotificacion); 
	}

}

