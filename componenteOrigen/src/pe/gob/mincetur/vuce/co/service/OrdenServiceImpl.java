package pe.gob.mincetur.vuce.co.service;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.dao.OrdenDAO;
import pe.gob.mincetur.vuce.co.domain.Orden;

public class OrdenServiceImpl implements OrdenService {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

    private OrdenDAO ordenDAO;

    public void setOrdenDAO(OrdenDAO ordenDAO) {
        this.ordenDAO = ordenDAO;
    }

    public Orden loadOrdenByNumero(Long numero) throws Exception {
        return ordenDAO.loadOrdenByNumero(numero);
    }

    public Orden loadOrdenById(Long idOrden) throws Exception {
        return ordenDAO.loadOrdenById(idOrden);
    }

    public HashUtil<String, Object> insertOrdenCab(HashUtil<String, Object> filter) throws Exception {
        return ordenDAO.insertOrdenCab(filter);
    }

    public void insertOrdenUsuario(HashUtil<String, Object> filter) throws Exception {
        ordenDAO.insertOrdenUsuario(filter);
    }
    
    public void insertOrdenSol(HashUtil<String, Object> filter) throws Exception {
        ordenDAO.insertOrdenSolicitante(filter);
    }

    public void insertOrdenRep(HashUtil<String, Object> filter) throws Exception {
        ordenDAO.insertOrdenRepresentante(filter);
    }
    
    public void updateOrdenRepresentante(HashUtil<String, Object> filter) throws Exception {
        ordenDAO.updateOrdenRepresentante(filter);
    }

    public HashUtil<String, Object> creaModifOrden(HashUtil<String, Object> filter) throws Exception {
        return ordenDAO.creaModifOrden(filter);
    }

    public HashUtil<String, Object> transmiteOrden(HashUtil<String, Object> filter) throws Exception {
        return ordenDAO.transmiteOrden(filter);
    }

    public Orden loadOrdenDetail(HashUtil<String, Object> filter) throws Exception {
        return ordenDAO.loadOrdenDetail(filter);
    }

    public void desistirOrden(HashUtil<String, Object> filter) throws Exception {
        ordenDAO.desistirOrden(filter);
    }

    /*public int existeModificaciones(HashUtil<String, Object> filter)throws Exception{
        return ordenDAO.existeModificaciones(filter);
    }
    */

}
