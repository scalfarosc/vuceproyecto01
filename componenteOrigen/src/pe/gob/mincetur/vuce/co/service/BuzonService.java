package pe.gob.mincetur.vuce.co.service;

import java.util.HashMap;
import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.Mensaje;

public interface BuzonService {
	
	public Mensaje loadMensaje(HashUtil<String, Object> filter) throws Exception;
	
	public Mensaje loadMensajeExtranet(HashUtil<String, Object> filter) throws Exception;
	
	public String loadNotificacion(HashUtil<String, Object> filter) throws Exception;
	
	public void updateMensajeLeido(HashUtil<String, Object> filter) throws Exception;
	
	public void inactivaMensaje(HashUtil<String, Object> filter) throws Exception;
	
	public Adjunto loadAdjunto(HashUtil<String, Object> filter) throws Exception;
	
	public HashMap<String, Integer> getNotificacion(Integer idTransmision) throws Exception;
	
	public List<String> getNotificacionesFromMensaje(Integer idNotificacion) throws Exception;
	
}
