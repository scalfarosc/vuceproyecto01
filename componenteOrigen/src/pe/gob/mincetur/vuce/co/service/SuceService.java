package pe.gob.mincetur.vuce.co.service;

import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.Orden;

public interface SuceService {

    public Suce loadSuceById(long suce) throws Exception;

    public Suce loadSuceByIdAndUsuarioId(long suce, long usuarioId, long rol) throws Exception;

    public Integer registrarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception;

    public void actualizarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception;

    public void enviarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception;

    public void eliminarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception;

    public ModificacionSuce loadModificacionSuceByIdMensajeId(HashUtil<String, Object> filter) throws Exception;

    public ModificacionDR loadModificacionDrById(HashUtil<String, Object> filter) throws Exception;

    public void aprobarModificacion(int suce, int modificacionSuce) throws Exception;

    public void rechazarModificacion(Orden orden, Suce suce, Integer modificacionSuce, String mensaje, List<Adjunto> adjuntos) throws Exception;

    public HashUtil<String, Object> insertSubsanacion(HashUtil<String, Object> filter, List<String> notificaciones) throws Exception;

    public void transmiteSubsanacion(HashUtil<String, Object> filter) throws Exception;

    public void actualizaSubsanacion(int modifSuceId, HashUtil<String, Object> filter, List<String> notificaciones) throws Exception;

    public void eliminaSubsanacion(HashUtil<String, Object> filter) throws Exception;

    public HashUtil<String, Object> insertModificacionDr(HashUtil<String, Object> filter) throws Exception;

    public void transmiteModificacionDr(HashUtil<String, Object> filter) throws Exception;

    public void actualizaModificacionDr(HashUtil<String, Object> filter) throws Exception;

    public void eliminaModificacionDr(HashUtil<String, Object> filter) throws Exception;

    public void aprobarModificacionDr(HashUtil<String, Object> filter) throws Exception;

    public void rechazarModificacionDr(HashUtil<String, Object> filter) throws Exception;

    public HashUtil<String, Object> registrarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception;

    public HashUtil<String, Object> eliminarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception;

    public void modificarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception;

    public void confirmarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception;

    public Suce loadSuceModificacionById(int suceId, int mto) throws Exception;

    public Suce loadSuceByNumeroAndUsuarioId(int numSuce, int usuarioId, int rol) throws Exception;

    public Suce loadSuceByNumero(int numSuce) throws Exception;

	public void desistirSuce(HashUtil<String, Object> filter) throws Exception;

	public void aceptacionDesestimientoSuce(Integer vcId, int suce) throws Exception;

	public void rechazoDesestimientoSuce(int suce) throws Exception;

    public String suceFlgPendienteCalif(Integer suceId);

}