package pe.gob.mincetur.vuce.co.service;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.dao.AuditoriaDAO;
import pe.gob.mincetur.vuce.co.domain.Auditoria;

public class AuditoriaServiceImpl implements AuditoriaService{

	private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);
	
	private AuditoriaDAO auditoriaDAO;
	
	public void setAuditoriaDAO(AuditoriaDAO auditoriaDAO) {
		this.auditoriaDAO = auditoriaDAO;
	}
	
	
	public void insertInicioSesion(Auditoria auditoria) throws Exception{
		auditoriaDAO.insertInicioSesion(auditoria);
		
	}
	
	public void insertInicioSesionContingencia(Auditoria auditoria) throws Exception{
		auditoriaDAO.insertInicioSesionContingencia(auditoria);
	}
	
	public void insertFinSesionContingencia(Auditoria auditoria) throws Exception{
		auditoriaDAO.insertFinSesionContingencia(auditoria);
	}
	
	public void insertAccesoSolicitud(Auditoria auditoria) throws Exception{
		auditoriaDAO.insertAccesoSolicitud(auditoria);
		
	}
	
	public void insertAccesoSuce(Auditoria auditoria) throws Exception{
		auditoriaDAO.insertAccesoSuce(auditoria);
		
	}
	
	public void insertAccesoDr(Auditoria auditoria) throws Exception{
		auditoriaDAO.insertAccesoDr(auditoria);
		
	}

}
