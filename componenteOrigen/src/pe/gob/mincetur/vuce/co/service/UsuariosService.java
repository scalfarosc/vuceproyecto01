package pe.gob.mincetur.vuce.co.service;

import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Usuario;

public interface UsuariosService {
    
    public void testGrabacionUsuarioContextIdentifier(HashUtil<String, Object> filter);
	
    public int insertUsuario(HashUtil<String, Object> filter) throws Exception;
    
    //public int insertUsuarioEmpresaExterna(HashUtil<String, Object> filter) throws Exception;
	
    public int insertEmpresa(HashUtil<String, Object> filter, Table representantesLegales) throws Exception;
    
    public int insertEmpresaExterna(HashUtil<String, Object> filter) throws Exception;
    
	//public void insertRepresentanteLegal(HashUtil<String, Object> filter) throws Exception;
	
    public void updateUsuario(HashUtil<String, Object> filter) throws Exception;
    
    //public void updateUsuarioEmpresaExterna(HashUtil<String, Object> filter) throws Exception;
    
    public void updateEmpresa(HashUtil<String, Object> filter, Table representantesLegales) throws Exception;
    
    public void updateRepresentantesLegalesEmpresa(int idEmpresa, Table representantesLegales) throws Exception;
    
    public void updateEmpresa(HashUtil<String, Object> filter) throws Exception;
    
    public void updateEmpresaExterna(HashUtil<String, Object> filter) throws Exception;
	
	public int existeUsuarioSol(HashUtil<String, Object> filter) throws Exception;
	
	public int existeUsuarioExtranet(HashUtil<String, Object> filter) throws Exception;
	
	public int existeUsuarioDNI(HashUtil<String, Object> filter) throws Exception;
	
	public boolean existeEmpresa(HashUtil<String, Object> filter) throws Exception;
	
	public boolean usuarioTieneTramites(HashUtil<String, Object> filter) throws Exception;
	
	public HashUtil actualizarRolesUsuario(int idUsuario, HashUtil roles) throws Exception;
	
	public UsuarioCO loadUsuarioCOById(int idUsuario) throws Exception;
	
	public Usuario loadUsuarioById(int idUsuario) throws Exception;
	
	public Usuario loadEmpresaById(int idUsuario) throws Exception;

	public UsuarioCO loadUsuarioVUCEByRucAndCodigoSol(String ruc, String codigoUsuario) throws Exception;
	
	public HashUtil cargarRolesUsuario(int idUsuario);
	
}
