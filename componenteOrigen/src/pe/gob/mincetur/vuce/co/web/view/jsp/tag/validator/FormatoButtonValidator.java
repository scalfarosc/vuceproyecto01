package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.logic.OrdenLogic;
import pe.gob.mincetur.vuce.co.logic.SuceLogic;
import pe.gob.mincetur.vuce.co.util.Rol;

public class FormatoButtonValidator {

    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        return null;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> informacionGeneral(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        Object id = request.getAttribute("idFormatoEntidad");
        Object orden = request.getAttribute("orden");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");
        //String estadoRegistro = (String) request.getAttribute("estadoRegistro");

        if (bloqueado==null) bloqueado = "";
        if (puedeTransmitir==null) puedeTransmitir = "";

        if (id == null) {
        	
        	//20140526 - JMC - BUG 1.PersonaNatural
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("nuevaButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        	
            disabledButtons.put("transButton", Constantes.DISABLING_ACTION_DISABLE);
        }
        
        if (orden != null) {
        	if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("transButton", Constantes.DISABLING_ACTION_DISABLE);
            	
            	
            	OrdenLogic ordenLogic = (OrdenLogic)SpringContext.getApplicationContext().getBean("ordenLogic");
            	Orden ordenObj = ordenLogic.loadOrdenById(Util.longValueOf(orden));
            	String estadoRegistro = ordenObj.getEstadoRegistro();
            	
            	if (ordenObj.getSuce()!=null) {
            		SuceLogic suceLogic = (SuceLogic)SpringContext.getApplicationContext().getBean("suceLogic");
                	Suce suceObj = suceLogic.getSuceById(ordenObj.getSuce());
                	estadoRegistro = suceObj.getEstadoRegistro();
            	}
            	
        		if ("A".equals(estadoRegistro) || "H".equals(estadoRegistro) || "B".equals(estadoRegistro) || "I".equals(estadoRegistro) || 
    				"E".equals(estadoRegistro) || "R".equals(estadoRegistro) || "D".equals(estadoRegistro)) {
            	    disabledButtons.put("desisteButton", Constantes.DISABLING_ACTION_DISABLE);
        		}
            	
            }else {
            	if(puedeTransmitir.equalsIgnoreCase("N")) disabledButtons.put("transButton", Constantes.DISABLING_ACTION_DISABLE);
            }

        }
        
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        if (Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) {
        	if (request.getAttribute("notifPendientes") != null && "1".equals(request.getAttribute("notifPendientes").toString())){
                disabledButtons.put("asignarButton", Constantes.DISABLING_ACTION_DISABLE);
        	}
    	}

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> usuariosDJ(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");

        if (bloqueado==null) bloqueado = "";
        if (puedeTransmitir==null) puedeTransmitir = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("cargoDeclaranteButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        } else {
        	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("cargoDeclaranteButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> informacionGeneralSC(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");

        if (bloqueado==null) bloqueado = "";
        if (puedeTransmitir==null) puedeTransmitir = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("transButton", Constantes.DISABLING_ACTION_HIDE);
            }else {
            	if(puedeTransmitir.equalsIgnoreCase("N")) disabledButtons.put("transButton", Constantes.DISABLING_ACTION_HIDE);
            }

        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> usuariosSC(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        //String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");

        if (bloqueado==null) bloqueado = "";
        //if (puedeTransmitir==null) puedeTransmitir = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("cargoDeclaranteButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        } else {
        	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("cargoDeclaranteButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> usuariosCertificado(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");

        if (bloqueado==null) bloqueado = "";
        if (puedeTransmitir==null) puedeTransmitir = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("cargoDeclaranteButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        } else {
        	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("cargoDeclaranteButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> informacionGeneralCertificado(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");

        if (bloqueado==null) bloqueado = "";
        if (puedeTransmitir==null) puedeTransmitir = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("transButton", Constantes.DISABLING_ACTION_HIDE);
            } else {
            	if(puedeTransmitir.equalsIgnoreCase("N")) disabledButtons.put("transButton", Constantes.DISABLING_ACTION_HIDE);
            }

        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    public HashUtil<String, HashUtil<String, String>> adjuntosDocumentoResolutivo(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = (HashUtil<String, String>)buttonSet.get(Constantes.BUTTON_SET_DISABLE);
        if (disabledButtons==null) disabledButtons = new HashUtil<String, String>();

        String bloqueado = (String)request.getAttribute("bloqueado");

        if (bloqueado.equals("S")) {
            disabledButtons.put("cargarAdjuntoButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarAdjuntoButton", Constantes.DISABLING_ACTION_HIDE);
        }

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> representante(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> enabledButtons = new HashUtil<String, String>();
        Object numOrden = request.getAttribute("numOrden");
        String bloqueado = (String)request.getAttribute("bloqueado");
        if ((numOrden!=null && !numOrden.equals("")) && bloqueado.equalsIgnoreCase("N")) {
            enabledButtons.put("representanteButton", "representanteButton");
        }
        buttonSet.put(Constantes.BUTTON_SET_ENABLE, enabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> modificacionesSuce(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        Object idEntidad = request.getAttribute("idEntidad");
        String formato = (String)request.getAttribute("formato");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String estadoRegistro = (String)request.getAttribute("estadoRegistro");
        String utilizaModificacionSuceXMto = (String)request.getAttribute("utilizaModificacionSuceXMto");
        String historico = (String)request.getAttribute("historico");

        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        HashUtil<String, String> enabledButtons = new HashUtil<String, String>();

        int numNotifPendientes = Integer.parseInt( ( request.getAttribute("notifPendientes") != null ? request.getAttribute("notifPendientes").toString().trim() : "0" ) );

        if (disabledButtons.get("nuevaSubsabacionButton") == null && numNotifPendientes == 0){
        	//Table notifPendientes = request.getAttribute("notifPendientes") != null?(Table) request.getAttribute("notifPendientes"): new Table();

        	//if (numNotifPendientes == 0) {
        		disabledButtons.put("nuevaSubsanacionButton", Constantes.DISABLING_ACTION_DISABLE);
        	//}
        }
        buttonSet.put(Constantes.BUTTON_SET_ENABLE, enabledButtons);
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }


}
