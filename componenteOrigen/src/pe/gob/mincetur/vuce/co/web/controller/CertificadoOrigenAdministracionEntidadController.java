package pe.gob.mincetur.vuce.co.web.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.config.LabelConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.service.ibatis.IbatisServiceImpl;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.jlis.web.util.TableUtil;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.logic.AdjuntoLogic;
import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.InteroperabilidadFirmasLogic;
import pe.gob.mincetur.vuce.co.logic.OrdenLogic;
import pe.gob.mincetur.vuce.co.logic.SuceLogic;
import pe.gob.mincetur.vuce.co.logic.certificadoOrigen.CertificadoOrigenLogic;
import pe.gob.mincetur.vuce.co.logic.resolutor.ResolutorLogic;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.AdjuntoCertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.OptionListFactory;
import pe.gob.mincetur.vuce.co.util.Rol;

/**
 * Clase Controladora para el Resolutor de Certificados de Origen - Segunda Versi�n
 * @author Erick Osc�tegui
 */

public class CertificadoOrigenAdministracionEntidadController extends MultiActionController {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);

    private ResolutorLogic resolutorLogic;
    private OrdenLogic ordenLogic;
    private FormatoLogic formatoLogic;
    private SuceLogic suceLogic;
    private AdjuntoLogic adjuntoLogic;
    private CertificadoOrigenLogic certificadoOrigenLogic;
    private InteroperabilidadFirmasLogic iopLogic;

	private String bandejaSolicitudes;
    private String solicitudesCertificado;
    private String asignacionEvaluadorSolicitudCertificado;
    private String abrirSolicitudCertificado;
    private String abrirSuce;
    private String notificacionSubsanacionSuce;
    private String formCertificadoOrigenDR;
    private String modificacionDR;
    private String modificacionSubsanacion;
    private String rechazoModificacionSubsanacion;
    private String rechazoModificacionDR;
    private String rectificacionDR;
    private String notificacionSubsanacionOrden;
    private String datosDocResolMercancia;

    protected IbatisService ibatisService;

    /*public ModelAndView cargarListaSolicitudesCertificadoOrigenOld(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        String key = "";
        // Se est� removiendo el filtro por SUCE, ya que las solicitudes sin asignar NO TIENEN SUCE.
        if (datos.getString("opcionFiltro").equals(ConstantesCO.OPCION_ORDEN)) {
        	if (!datos.getString("numeroOrden").equals("")) {
	    		String numOrd = datos.getString("numeroOrden").replaceAll("[-]", "");
	    		if (!NumberUtils.isNumber(numOrd)) {
	    			numOrd = "0";
	    		}

	    		filter.put("numeroOrden", numOrd);
        	}
        } else if (datos.getString("opcionFiltro").equals(ConstantesCO.OPCION_SUCE)) {
        	if (!datos.getString("numeroSUCE").equals("")) {
	    		String numSUCE = datos.getString("numeroSUCE").replaceAll("[-]", "");
	    		if (!NumberUtils.isNumber(numSUCE)) {
	    			numSUCE = "0";
	    		}

	            filter.put("numeroSUCE", numSUCE);
        	}
        }
        request.setAttribute("numeroOrden", datos.getString("numeroOrden"));
        request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
        request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));
        request.setAttribute("tipoDocumento", datos.getString("tipoDocumento"));
        request.setAttribute("numeroDocumento", datos.getString("numeroDocumento"));
        request.setAttribute("nombre", datos.getString("nombre"));

    	filter.put("entidadId", usuario.getIdEntidad());
    	// Agregando los filtros
        filter.put("tipoDocumento", datos.getString("tipoDocumento"));
        filter.put("numeroDocumento", datos.getString("numeroDocumento"));
        filter.put("nombre", datos.getString("nombre"));

        // Primero las SUCES sin asignar a un Evaluador (CO.ENTIDAD.ADMINISTRADOR)
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
            (usuario.getRolActivo().equals(Rol.CO_ENTIDAD_SUPERVISOR.getNombre()) ||
             usuario.getRolActivo().equals(Rol.CO_ENTIDAD_ADMINISTRADOR.getNombre()))
            ) {
            key = "administracionEntidad.calificacionesSinAsignar.grilla";

            //Table tSucesSinAsignar = ibatisService.loadGrid(key, filter);
            //request.setAttribute("tCalificacionesSinAsignar", tSucesSinAsignar);
        }

        // Ahora las SUCES asignadas a un usuario de la entidad (CO.ENTIDAD.EVALUADOR)
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
            (usuario.getRolActivo().equals(Rol.CO_ENTIDAD_EVALUADOR.getNombre()) ||
             usuario.getRolActivo().equals(Rol.CO_ENTIDAD_SUPERVISOR.getNombre()))
            ) {
            // Ahora las SUCES asignadas a un usuario de la entidad (CO.ENTIDAD.EVALUADOR)
            key = "administracionEntidad.calificaciones.grilla";
            if (!usuario.getRolActivo().equals(Rol.CO_ENTIDAD_SUPERVISOR.getNombre())) {
                filter.put("usuarioId", usuario.getIdUsuario());
            }

            //Table tSuces = ibatisService.loadGrid(key, filter);
            //request.setAttribute("tCalificaciones", tSuces);
        }

        return new ModelAndView(bandejaSolicitudes);
    }*/

	/**
     * Muestra la pantalla de solicitudes de certificados de origen
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarListaSolicitudesCertificadoOrigen(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
           		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS
        } else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
    		filter.put("entidadId", usuario.getIdEntidad());
			if (Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) { // Si es supervisor, el filtro adicional es por entidad
	    		if (datos.contains("filtroEvaluador") && !"".equals(datos.getString("filtroEvaluador"))) {
	        		filter.put("usuarioIdEvaluador", datos.getString("filtroEvaluador"));
	    		}
	    	}
       	    
			if (Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) { // En este caso el filtrado es por entidad y usuario
	    		filter.put("usuarioIdEvaluador", usuario.getIdUsuario());
	    	}
        }
        
        HashUtil<String, Object> filtroAcuerdos = new HashUtil<String, Object>();
        String keyAcuerdos = "comun.acuerdo.select";
        if (datos.contains("filtroPais") && !"".equals(datos.getString("filtroPais"))){
            filter.put("filtroPais", datos.getString("filtroPais"));
            
            //keyAcuerdos = "certificado_origen.acuerdos_pais.select";
            
            //20170206_GBT ACTA CO-004-16 Y ACTA CO 009-16 PARA MOSTRAR TODOS LOS ACUERDOS POR PAIS
            keyAcuerdos = "certificado_origen.acuerdos_all_pais.select";
            filtroAcuerdos.put("pais", datos.getString("filtroPais"));
        }
        request.setAttribute("keyAcuerdos", keyAcuerdos);
        request.setAttribute("filtroAcuerdos", filtroAcuerdos);
        
        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
            filter.put("acuerdoInternacionalId", datos.getString("filtroAcuerdo"));
        }
        
        if(datos.contains("filtroFormato") && !"".equals(datos.getString("filtroFormato"))){
        	filter.put("filtroFormato", datos.getString("filtroFormato"));
        	request.setAttribute("filtroFormato", datos.getString("filtroFormato"));
        }
        
        if (datos.contains("filtroEstado") && !"T".equals(datos.getString("filtroEstado"))){
        	String estado = datos.getString("filtroEstado");
        	if (estado.indexOf("|")==-1) { // Es un estado que AGRUPA otros
        		if (estado.equalsIgnoreCase("E")) {
        			filter.put("enProceso", "S");
        		} else if (estado.equalsIgnoreCase("C")) {
        			filter.put("concluidos", "S");
        		}
        	} else {
        		filter.put("estadoRegistro", estado.split("[|]")[1]);
        	}
        }
        
        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))) {
            if ("O".equals(datos.getString("opcionFiltro")) && datos.contains("numeroOrden") && !"".equals(datos.getString("numeroOrden"))){
                filter.put("numeroOrden", datos.getString("numeroOrden"));
            } else if ("S".equals(datos.getString("opcionFiltro")) && datos.contains("numeroSUCE") && !"".equals(datos.getString("numeroSUCE"))){
                filter.put("numeroSUCE", datos.getString("numeroSUCE"));
            } else if ("C".equals(datos.getString("opcionFiltro")) && datos.contains("numeroCO") && !"".equals(datos.getString("numeroCO"))){
                filter.put("numeroCO", datos.getString("numeroCO"));
                request.setAttribute("numeroCO", datos.getString("numeroCO"));
            }
        }
        
		try {
	        if (datos.contains("fechaDesde") && !"".equals(datos.getString("fechaDesde"))){
	            filter.put("fechaDesde", Util.getTimestampObject(datos.getString("fechaDesde")+" 00:00:00", "dd/MM/yyyy HH:mm:ss"));
	        }
	        if (datos.contains("fechaHasta") && !"".equals(datos.getString("fechaHasta"))){
	            filter.put("fechaHasta", Util.getTimestampObject(datos.getString("fechaHasta")+" 23:59:59", "dd/MM/yyyy HH:mm:ss"));
	        }
		} catch (Exception e) {
		}
		
		if (datos.contains("filtroRUC") && !"".equals(datos.getString("filtroRUC"))) {
			filter.put("numeroDocumento", datos.getString("filtroRUC"));
		}
		int flagDenominacionPartida=0;
		if (datos.contains("filtroDenominacion") && !"".equals(datos.getString("filtroDenominacion"))) {
			filter.put("denominacion", datos.getString("filtroDenominacion"));
			flagDenominacionPartida=1;
		}
		
		if (datos.contains("filtroPartida") && !"".equals(datos.getString("filtroPartida"))) {
			filter.put("partida", datos.getString("filtroPartida"));
			flagDenominacionPartida=1;
		}
		
		filter.put("flagDenominacionPartida", flagDenominacionPartida);	
		
		/*if (request.getParameter("indiceTab") != null && "".equals(request.getParameter("indiceTab"))){
			request.setAttribute("seleccionado", request.getParameter("indiceTab"));
		}*/
        
		RequestUtil.setAttributes(request);
        
        request.setAttribute("filterBandeja", filter);
        
    	HashUtil<String, Object> filterEval = new HashUtil<String, Object>();
    	filterEval.put("entidadId", usuario.getIdEntidad());
    	request.setAttribute("filterEvaluadores", filterEval);

    	String seleccionado = "0";
    	if (request.getParameter("seleccionado")!=null) {
    		seleccionado = request.getParameter("seleccionado");
    	} else if (request.getAttribute("seleccionado") != null) {
    		seleccionado = (String) request.getAttribute("seleccionado");
    	} else if (!Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) {
    		seleccionado = "2";
    	}
        
		request.setAttribute("seleccionado", seleccionado);
        
        return new ModelAndView(bandejaSolicitudes);
    }

    public ModelAndView asignacionEvaluadorSolicitudCertificado(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
    	RequestUtil.setAttributes(request);
        
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("entidadId", usuario.getIdEntidad());
        
	    request.setAttribute("numeroSolicitud", datos.getString("numeroSolicitud"));
	    request.setAttribute("ordenId", datos.getString("orden"));
	    request.setAttribute("mto", datos.getString("mto"));
    	
	    request.setAttribute("filterEvaluadores", filter);
	    
	            
    	return new ModelAndView(asignacionEvaluadorSolicitudCertificado);
    }
    
    public ModelAndView asignarEvaluadorSolicitudCertificado(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int contador = 0;
        if ("S".equals(datos.getString("masivo"))) {
        	HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));
            
            String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
			Table tablaDetalle = (Table) tables.get("CO_SIN_ASIGNAR");
			boolean yaAsignado=false;
			MessageList messageListAsignado = new MessageList();
			contador = 0;
			String estadoAcuerdoPais3 = "";
			for (int i = 0; i < deleteColumns.length; i++) {
				Row row = tablaDetalle.obtainRowByKeyFields(deleteColumns[i]);
				long ordenId = Util.longValueOf(row.getCell("ORDENID").getValue());
				long mto = Util.longValueOf(row.getCell("MTO").getValue());
				long evaluadorId = datos.getLong("evaluadorId");
			    
				
				//20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
		        estadoAcuerdoPais3 = validaAcuerdoPais(ordenId);
		        if ("I".equals(estadoAcuerdoPais3)) {
		        	contador = contador + 1;
		        }
			
				
				Map<String,Object> parametros = new HashMap<String,Object>();
		        parametros.put("ordenId",ordenId);
		        parametros.put("mto",mto);
		        parametros.put("evaluadorId",evaluadorId);
		        parametros.put("prioridad", "N");
		        
		        
		        
		        MessageList messageList = resolutorLogic.designaEvaluador(parametros);
		        
            	if (messageList != null) {
            		Message msg = null;
            		for (Object object : messageList) {
            			if (object != null) {
            				msg = (Message) object;
            				if ( msg.getMessageKey() == null || !"update.success".equalsIgnoreCase(msg.getMessageKey()) ) {
            					messageListAsignado.add(object);
            		        	yaAsignado=true;
            				}
            			}
					}
            	}		        
            	
            	
			}
			
			if(!yaAsignado){
                Message message = new Message("update.success");
                MessageList messageList = new MessageList();
                messageList.add(message);
                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
			} else {
				request.setAttribute(Constantes.MESSAGE_LIST, messageListAsignado);
			}
		
			
        } else {
	        long ordenId = datos.getLong("ordenId");
	        long mto = datos.getLong("mto");
	        long evaluadorId = datos.getLong("evaluadorId");
	        String prioridad = datos.getString("prioridad");
	        
	        Map<String,Object> parametros = new HashMap<String,Object>();
	        parametros.put("ordenId",ordenId);
	        parametros.put("mto",mto);
	        parametros.put("evaluadorId",evaluadorId);
	        parametros.put("prioridad",prioridad);
	
	        MessageList messageList = resolutorLogic.designaEvaluador(parametros);
	
	        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	        
	        	
        }
      
        //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
		if (contador != 0) {
			request.setAttribute("estadoAcuerdoPais3","I");
		}
		
        
        return cargarListaSolicitudesCertificadoOrigen(request, response);
    }

    public ModelAndView abrirSolicitudCertificado(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        MessageList messageList = new MessageList();
        Long ordenId = datos.getLong("ordenId");
		Integer mto = datos.getInt("mto");

		//Solicitud solicitudObj = scLogic.getSolicitudById(ordenId, mto);

		/*if(solicitudObj.getTransmitido().equals("S")||solicitudObj.getAnulado().equals("S")) solicitudObj.setBloqueado("S");
        else solicitudObj.setBloqueado("N");

		RequestUtil.setAttributes(request, "", solicitudObj);*/

		/*Solicitante solicitanteObj = scLogic.getSolicitanteDetail(ordenId, mto,ConstantesCO.USUARIO_SOLICITANTE);
    	RequestUtil.setAttributes(request, "SOLICITANTE.", solicitanteObj);

    	Solicitante declaranteObj = scLogic.getSolicitanteDetail(ordenId, mto, ConstantesCO.USUARIO_USUARIO);
    	RequestUtil.setAttributes(request, "DECLARANTE.", declaranteObj);*/

    	//if (solicitanteObj.getUbigeo() != null) {
            // Nombres del departamento, provincia y distrito
            /*HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitanteObj.getUbigeo());
            request.setAttribute("departamento", element.get("DEPARTAMENTO"));
            request.setAttribute("provincia", element.get("PROVINCIA"));
            request.setAttribute("distrito", element.get("DISTRITO"));*/
        //}
        //Cargar datos del representante

        /*if (solicitanteObj.getPersonaTipo().toString().equals(ConstantesCO.TIPO_PERSONA_JURIDICA)){
            HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
            filterRepresentante.put("usuarioId", solicitanteObj.getUsuarioId());
            request.setAttribute("filterRepresentante", filterRepresentante);

            //Solicitante representante = scLogic.getSolicitanteDetail(ordenId, mto, ConstantesCO.USUARIO_REPRESENTANTE);
            //RequestUtil.setAttributes(request, "REPRESENTANTE.", representante);
        }*/

        HashUtil<String, Object> filterDRs = new HashUtil<String, Object>();
        //filterDRs.put("suceId", solicitudObj.getSuceId());
        //Table tDocResolutivos = ibatisService.loadGrid("sc.resolutor.calificacion.grilla", filterDRs);
        //request.setAttribute("tDocResolutivos", tDocResolutivos);

        request.setAttribute("ordenId", ordenId);
        request.setAttribute("mto", mto);
        //request.setAttribute("suceId", solicitudObj.getSuceId());

        return new ModelAndView(abrirSolicitudCertificado);
    }

     public ModelAndView iniciarEvaluacionSolicitudCertificado(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = null;//administracionEntidadLogic.iniciarEvaluacionCalificacion(datos);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return abrirSolicitudCertificado(request, response);
    }

    public ModelAndView aceptarDesignacion(HttpServletRequest request, HttpServletResponse response) {
    	//HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	
     	CertificadoOrigen co = new CertificadoOrigen();
     	co.setOrden(new Long(request.getParameter("orden")));
     	co.setMto(Integer.parseInt(request.getParameter("mto")));
     	// 15/07/2013 -JMC - Comentado porque recien cuando acepta el evaluador se genera la SUCE
    	//co.setSuce(Integer.parseInt(request.getParameter("suceId")));
     	
     	co.setSuce(new Integer("0"));
     	
     	
        MessageList messageList = resolutorLogic.aceptaDesignacionTCE(co);//administracionEntidadLogic.iniciarEvaluacionCalificacion(datos);

        request.getSession().setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        //20170203_GBT ACTA CO-004-16 Y ACTA CO 009-16
        //Se cambi� a estadoAcuerdoPais2 para diferenciar de estadoAcuerdoPais por que mostraba alertas repetidas
     	
      	String estadoAcuerdoPais2 = "";
      	
        estadoAcuerdoPais2 = validaAcuerdoPais(Long.parseLong(request.getParameter("orden")));
        if ("I".equals(estadoAcuerdoPais2)) {
          	request.setAttribute("estadoAcuerdoPais2","I");
        }

        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        
        //return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
        if ("I".equals(estadoAcuerdoPais2)) {
        	return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen&estadoAcuerdoPais2=I", true));
        }
        else {
        	return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
        }
        	
    }

    public ModelAndView rechazarDesignacion(HttpServletRequest request, HttpServletResponse response) {
     	CertificadoOrigen co = new CertificadoOrigen();
     	co.setOrden(new Long(request.getParameter("orden")));
     	co.setMto(Integer.parseInt(request.getParameter("mto")));
     	UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        co.setIdUsuario(usuario.getIdUsuario());

        MessageList messageList = resolutorLogic.rechazaDesignacionTCE(co);//administracionEntidadLogic.iniciarEvaluacionCalificacion(datos);

        request.getSession().setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        //20170203_GBT ACTA CO-004-16 Y ACTA CO 009-16
        //Se cambi� a estadoAcuerdoPais2 para diferenciar de estadoAcuerdoPais por que mostraba alertas repetidas
     	
      	String estadoAcuerdoPais2 = "";
      	
        estadoAcuerdoPais2 = validaAcuerdoPais(Long.parseLong(request.getParameter("orden")));
        if ("I".equals(estadoAcuerdoPais2)) {
          	request.setAttribute("estadoAcuerdoPais2","I");
        }


        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        //return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
        if ("I".equals(estadoAcuerdoPais2)) {
        	return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen&estadoAcuerdoPais2=I", true));
        }
        else {
        	return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
        }
        
    }

    /*public ModelAndView rechazarDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
     	DeclaracionJurada dj = new DeclaracionJurada();
     	dj.setDjId(new Long(request.getParameter("djId")));

        MessageList messageList = resolutorLogic.evaluadorDJNoCalifica(dj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
    }*/

    public ModelAndView abrirSuce(HttpServletRequest request, HttpServletResponse response) {
        MessageList messageList = request.getAttribute(Constantes.MESSAGE_LIST)!=null? (MessageList)request.getAttribute(Constantes.MESSAGE_LIST) : new MessageList();
        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden")), Integer.parseInt(request.getParameter("mto")));
        messageList.setObject(ordenObj);

        // Cargamos datos del usuario del formato como el declarante
        RequestUtil.setAttributes(request, "DECLARANTE.", formatoLogic.getUsuarioDetail(ordenObj.getOrden(),ordenObj.getMto()));
        
        String estadoAcuerdoPais = "";
        estadoAcuerdoPais = validaAcuerdoPais(Long.parseLong(request.getParameter("orden")));
        //if ("I".equals(estadoAcuerdoPais)) {
        //  	//request.setAttribute("estadoAcuerdoPais","I");
        //  	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
        //}
        
        

        cargarInformacionEspecificaSuce(request, messageList);
        if("I".equals(estadoAcuerdoPais.toString())){            	
        	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
        	request.setAttribute("estadoAcuerdoPais","I");
        }
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("por_evaluacion", ConstantesCO.OPCION_SI);

       	String paginaPrev = request.getParameter("pagActual");
       	int numRecargas = -1;
       	if ( this.abrirSuce.equals(paginaPrev) && request.getParameter("numRecargas") != null && !"".equals(request.getParameter("numRecargas")) ) {
       		numRecargas =  Integer.parseInt(request.getParameter("numRecargas"));
       	}
       	request.setAttribute("numRecargas", numRecargas + 1);
       	request.setAttribute("pagActual", this.abrirSuce);
       	request.setAttribute("esReexportacion", ordenObj.getCertificadoReexportacion());
       	request.setAttribute("tipoCertificado", !"S".equals(ordenObj.getCertificadoReexportacion()) ? "Origen" : "Reexportaci�n");
       	
       	
        return new ModelAndView(this.abrirSuce);
    }

    /**
     *  Carga la informaci�n espec�fica del Formato de Certificado de Origen
     */
    public void cargarInformacionEspecificaSuce(HttpServletRequest request, MessageList messageList) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String puedeTransmitir = ConstantesCO.OPCION_NO;

        Orden ordenObj = (Orden) messageList.getObject();

        // Carga los datos del detalle del formato
        if (ordenObj.getSuce() != null){ // Si existe la SUCE

            // Cargar Datos de la SUCE
            long rol = -1;

            if (Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) {
                rol = Rol.CO_ENTIDAD_EVALUADOR.getCodigo();
            } else if (Rol.CO_ENTIDAD_FIRMA.getNombre().equals(usuario.getRolActivo())) {
                rol = Rol.CO_ENTIDAD_FIRMA.getCodigo();
            } 
            
            if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase( datos.getString("formato")) ||
            	ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase( datos.getString("formato")) ||
            	ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase( datos.getString("formato"))) {
            	Map prmtDJsRechazadas = new HashMap<String, Object>();
            	prmtDJsRechazadas.put("formato", datos.getString("formato").toLowerCase());
            	prmtDJsRechazadas.put("ordenId", ordenObj.getOrden());
            	prmtDJsRechazadas.put("mto", ordenObj.getMto());
            	try {
            		request.setAttribute("numDJsRechazadas", this.resolutorLogic.numDJsRechazadas(prmtDJsRechazadas));
				} catch (Exception e) {
					request.setAttribute("numDJsRechazadas", 0);
				}

            }

            if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase( datos.getString("formato"))) {
            	HashUtil<String, Object> filter = new HashUtil<String, Object>();
                filter.put("ordenId", ordenObj.getOrden());
                filter.put("mto", ordenObj.getMto());

                HashUtil<String, Object> solicitudCACO = IbatisServiceImpl.getIbatisService().loadElement("certificadoOrigen.mct005.element", filter);
                if (solicitudCACO != null && solicitudCACO.size()>0){
                	request.setAttribute("MCT005.djId", solicitudCACO.getString("dj_id"));
                }
            }

            // Obtenemos la SUCE
            Suce suceObj = suceLogic.getSuceByIdAndUsuarioId(ordenObj.getSuce(), usuario.getIdUsuario(), rol);

            /** Esta secci�n fue extraida de la funci�n m�s general para poder tener la data del solicitante **/
            //Cargar datos del usuario creador del formato
            Solicitante usuarioFormato = formatoLogic.getUsuarioDetail(ordenObj.getOrden(),ordenObj.getMto());

            // Si el Usuario logueado no es el Usuario que hizo la SUCE (osea, el Usuario logueado es un Usuario Principal que puede VER la Solicitud/SUCE)
            if (usuarioFormato.getUsuarioId().intValue() != usuario.getIdUsuario().intValue()) {
                ordenObj.setBloqueado("S");
            }

            //Cargar datos del solicitante
            Solicitante solicitante = formatoLogic.getSolicitanteDetail(ordenObj.getOrden(),ordenObj.getMto());
            RequestUtil.setAttributes(request, "SOLICITANTE.", solicitante);

            RequestUtil.setAttributes(request, "", ordenObj);

            HashUtil<String, Object> filterTupa = new HashUtil<String, Object>();
            filterTupa.put("idTupa", ordenObj.getIdTupa());
            String tupa = ibatisService.loadElement("comun.tupa.element", filterTupa).getString("TUPA");
            request.setAttribute("nombreFormato", ordenObj.getNombreFormato() + " (TUPA: "+tupa+")");

            request.setAttribute("tipoPersona", solicitante.getPersonaTipo());

            //Cargar datos del representante
            HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
            filterRepresentante.put("usuarioId", solicitante.getUsuarioId());
            request.setAttribute("filterRepresentante", filterRepresentante);
            RepresentanteLegal representante = formatoLogic.getRepresentanteDetail(ordenObj.getOrden(),ordenObj.getMto());
            RequestUtil.setAttributes(request, "REPRESENTANTE.", representante);
            if ( "2".equals(solicitante.getPersonaTipo()) && ( representante == null || representante.getId() == null) ) {
                messageList.add(new Message("falta.representanteLegal"));
                request.setAttribute("puedeTransmitir", ConstantesCO.OPCION_NO);
            }

            if (solicitante.getUbigeo() != null && !solicitante.getUbigeo().equals("")) {
                //Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
                request.setAttribute("departamento", element.get("DEPARTAMENTO"));
                request.setAttribute("provincia", element.get("PROVINCIA"));
                request.setAttribute("distrito", element.get("DISTRITO"));
                request.setAttribute("departamentoId", element.get("DEPARTAMENTOID"));
                request.setAttribute("provinciaId", element.get("PROVINCIAID"));
                request.setAttribute("distritoId", element.get("DISTRITOID"));
            }

            /** Esta secci�n fue extraida de la funci�n m�s general para poder tener la data del solicitante **/

            // Cargar las notificaciones de subsanacion de la SUCE
            HashUtil<String, Object> filterNotificacion = new HashUtil<String, Object>();
            filterNotificacion.put("suceId", suceObj.getSuce());

            Table tNotificacionesSuce = ibatisService.loadGrid("suce.resolutor.notificaciones.grilla", filterNotificacion);
            request.setAttribute("tNotificacionesSuce", tNotificacionesSuce);

            // Carga las subsanaciones
            HashUtil<String, Object> filterSubsanacion = new HashUtil<String, Object>();
            filterSubsanacion.put("suceId", suceObj.getSuce());

            Table tSubsanacionesSuce = ibatisService.loadGrid("suce.resolutor.subsanaciones.grilla", filterSubsanacion);
            request.setAttribute("tSubsanacionesSuce", tSubsanacionesSuce);
            int numSubsanacionRecib = 0;
            if (tSubsanacionesSuce != null){
            	Row fila;
            	for (Object object : tSubsanacionesSuce) {
            		fila = (Row) object;

            		if ( "T".equals(fila.getCell("VER").getValue().toString()) ){
            			numSubsanacionRecib++;
            		}
				}
            }

            HashUtil filter = new HashUtil();
            filter.put("suceId", ordenObj.getSuce());
            Table tDocResolutivos = ibatisService.loadGrid("resolutor." + datos.getString("formato") + ".co.dr.grilla", filter);
            request.setAttribute("tDocResolutivos", tDocResolutivos);
            int numDRBorrador = 0;
            if (tDocResolutivos != null){
            	Row fila;
            	for (Object object : tDocResolutivos) {
            		fila = (Row) object;

            		if ( "W".equals(fila.getCell("DATOS").getValue().toString()) ){
            			numDRBorrador++;
            		}
				}
            }

            /*Verificamos que no haya notificaciones pendientes por transmitir*/
            filter.put("ordenId", null);
            filter.put("mto", null);
            filter.put("result",null);
            
            try {
                ibatisService.executeSPWithObject("resolutor.tiene_notificaciones_pendientes", filter);
                System.out.println("******************Notificacion Pendientes (456): "+filter.getString("RESULT"));
                request.setAttribute("notificacionPendiente", filter.getString("RESULT"));
            } catch (Exception e) {
            	logger.error(e);
            }
            // Seteamos variables de request importantes para el flujo de todo el certificado
            // Variables de suce
            request.setAttribute("suceId", suceObj.getSuce());
            request.setAttribute("tupaId", suceObj.getIdTupa());
            request.setAttribute("numSuce", suceObj.getNumSuce());
            request.setAttribute("suceCerrada", suceObj.getCerrada());
            request.setAttribute("fechaRegistroSuce", suceObj.getFechaRegistro());
            request.setAttribute("numExpediente", suceObj.getExpediente());
            request.setAttribute("evaluador", suceObj.getEvaluador());
            request.setAttribute("estadoRegistro", suceObj.getEstadoRegistro());
            request.setAttribute("edicion", suceObj.getEdicion()!=null ? suceObj.getEdicion() : "S");
            request.setAttribute("rectificaDr", suceObj.getRectificaDr()!=null ? suceObj.getRectificaDr() : "S");
            request.setAttribute("modificacionSuceXMts", suceObj.getModificacionSuceXMts()!=null ? suceObj.getModificacionSuceXMts() : "S");//(
            request.setAttribute("pendienteCalificacion", suceObj.getPendienteCalificacion());
            // Variables de orden
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("coId", ordenObj.getIdFormatoEntidad());
            request.setAttribute("ordenId", ordenObj.getOrden());
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("formato", datos.getString("formato"));
            request.setAttribute("transmitido", ordenObj.getTransmitido());
            request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());
            request.setAttribute("editable", ordenObj.getBloqueado().equals("S")?"no":"yes");
            request.setAttribute("numDRBorrador", numDRBorrador);
            request.setAttribute("numSubsanacionRecib", numSubsanacionRecib);
        }
        request.setAttribute("puedeTransmitir", puedeTransmitir);
        request.setAttribute("esReexportacion", ordenObj.getCertificadoReexportacion());
        //request.setAttribute("formato", request.getParameter("formato"));
        //request.setAttribute("fechaActual", ibatisService.loadElement("comun.fechaActual", null).getString("FECHA"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

    }

    public ModelAndView editarNotificacionSubsanacionSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);

        if (!datos.getString("notificacionId").equals("")) {
            int notificacionId = datos.getInt("notificacionId");
            cargarDatosNotificacionSubsanacionSuce(request, datos, notificacionId);
        } else {
            request.setAttribute("estado", "P");
        }

        return new ModelAndView(notificacionSubsanacionSuce);
    }

    private void cargarDatosNotificacionSubsanacionSuce(HttpServletRequest request, HashUtil<String, String> datos, Integer notificacionId) {
        // Cargamos los datos de la notificacion
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("notificacionId", notificacionId);
        HashUtil<String, Object> datosNotificacion = ibatisService.loadElement("resolutor.evaluador.suce.notificacionSubsanacionSuce.element", filter);
        request.setAttribute("notificacionId", datosNotificacion.getString("notificacionId"));
        request.setAttribute("mensajeId", datosNotificacion.getString("mensajeId"));
        request.setAttribute("mensaje", datosNotificacion.getString("mensaje"));
        request.setAttribute("estado", datosNotificacion.getString("estado"));
        //request.setAttribute("formato", datos.getString("formato"));

        // Cargamos los adjuntos asociados a la notificacion
        Table tAdjuntosNotificacion = ibatisService.loadGrid("resolutor.evaluador.suce.notificacionSubsanacionSuce.adjuntos", filter);
        request.setAttribute("tAdjuntosNotificacion", tAdjuntosNotificacion);
    }

    /**
     * Registra una Notificaci�n de Subsanaci�n
     * @param request
     * @param response
     * @return
     */
    public ModelAndView registrarNotificacionSubsanacionSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);

        MessageList messageList = resolutorLogic.registrarNotificacionSubsanacionSuce(datos);

        Integer notificacionId = (Integer)messageList.getObject();
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        cargarDatosNotificacionSubsanacionSuce(request, datos, notificacionId);

        return new ModelAndView(notificacionSubsanacionSuce);
    }

    /**
     * Envia una Notificaci�n de Subsanaci�n
     * @param request
     * @param response
     * @return
     */
    public ModelAndView enviarNotificacionSubsanacionSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = resolutorLogic.enviarNotificacionSubsanacionSuce(datos);
        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("ordenId"), datos.getInt("mto"));
        messageList.setObject(ordenObj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        cargarInformacionEspecificaSuce(request, messageList);

        return new ModelAndView(this.abrirSuce);
    }

    /**
     * Elimina una Notificaci�n de Subsanaci�n
     * @param request
     * @param response
     * @return
     */
    public ModelAndView eliminarNotificacionSubsanacionSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = resolutorLogic.eliminarNotificacionSubsanacionSuce(datos);
        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("ordenId"), datos.getInt("mto"));
        messageList.setObject(ordenObj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        cargarInformacionEspecificaSuce(request, messageList);

        return new ModelAndView(this.abrirSuce);
    }

    /**
     * Carga un archivo de Notificaci�n de Subsanaci�n
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarArchivoNotificacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        int ordenId = datos.getInt("ordenId");
        int mto = datos.getInt("mto");
        int notificacionId = datos.getInt("notificacionId");

        MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                byte [] bytes = multipartFile.getBytes();
                String nombre = multipartFile.getOriginalFilename();
                messageList = resolutorLogic.cargarArchivoNotificacion(ordenId,mto,nombre,notificacionId,bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("formato", datos.getString("formato"));

        if (datos.get("suceId") == null) {
            cargarDatosNotificacionSubsanacionOrden(request, datos, notificacionId);
        } else {
        	cargarDatosNotificacionSubsanacionSuce(request, datos, notificacionId);
        }

        if (datos.get("suceId") != null) {
        return new ModelAndView(notificacionSubsanacionSuce);
        } else {
        	return new ModelAndView(notificacionSubsanacionOrden);
        }

    }

    /**
     * Elimina un adjunto de Notificaci�n de Subsanaci�n
     * @param request
     * @param response
     * @return
     */
    public ModelAndView eliminarAdjuntoNotificacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        int notificacionId = datos.getInt("notificacionId");
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        int id;
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            id = Integer.parseInt(row.getCell("ADJUNTO_ID").getValue().toString());

            adjuntoLogic.eliminarAdjuntoById(id);
        }

        request.setAttribute("formato", datos.getString("formato"));
        if (datos.get("suceId") == null) {
        	cargarDatosNotificacionSubsanacionOrden(request, datos, notificacionId);
        	return new ModelAndView(notificacionSubsanacionOrden);
        } else {
        cargarDatosNotificacionSubsanacionSuce(request, datos, notificacionId);
        return new ModelAndView(notificacionSubsanacionSuce);
    }
    }

    /**
     * Carga la pantalla de registro de un borrador de dr
     *
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarDatosDocumentoResolutivo(HttpServletRequest request, HttpServletResponse response) {
    	
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        request.setAttribute("usuarioCo", usuario.getIdUsuario());
        request.setAttribute("usuarioSol", usuario.getNumeroDocumento());//20170803_JMC Alianza Pacifico: Para que el usuarioSOL se pase como parametro al agente firmador
        logger.debug("usuarioSol::::"+ usuario.getNumeroDocumento());
        //String controller = datos.getString("controller");
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String ordenId = (datos.getString("ordenId") != null ? datos.getString("ordenId"): datos.getString("orden"));
        String mto = datos.getString("mto");
        Long numSuce = datos.getLong("numSuce");
        String formato = datos.getString("formato").toLowerCase();

        CertificadoOrigenDR drObj;
        Long mct001DrId = null;
        Long drId = null;
        Integer sdr = null;
        Long formatoDrId = null;
        Long borradorDrId = null;

        String bloqueado = null;
        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();

        formatoDrId = (datos.get("formatoDrId")!=null && !datos.get("formatoDrId").toString().trim().equals(""))?datos.getLong("formatoDrId"):(Long)request.getAttribute("formatoDrId");
        borradorDrId = (datos.get("borradorDrId")!=null && !datos.get("borradorDrId").toString().trim().equals(""))?datos.getLong("borradorDrId"):(request.getAttribute("borradorDrId") == null || "".equals(request.getAttribute("borradorDrId").toString())?null:(Long)request.getAttribute("borradorDrId"));

        String tipoDr = resolutorLogic.obtenerTipoDrBorrador(borradorDrId);
        request.setAttribute("tipoDr", tipoDr);

        Orden ordenObj = ordenLogic.getOrdenById( ( datos.getLong("orden") != null ? datos.getLong("orden") : datos.getLong("ordenId") ) , datos.getInt("mto"));

        if (request.getAttribute("sdr") != null || (request.getParameter("sdrSelect") != null && !"".equals(request.getParameter("sdrSelect")))) {
        	drId = new Long(request.getParameter("drId") != null ? request.getParameter("drId").toString() : request.getParameter("DR.drId") );
        	sdr = new Integer(request.getAttribute("sdr") != null ? request.getAttribute("sdr").toString() : ((request.getParameter("sdrSelect") != null && !"".equals(request.getParameter("sdrSelect"))) ? request.getParameter("sdrSelect") : request.getParameter("DR.sdr")));
        } else {
        	mct001DrId = new Long(request.getParameter("mct001DrId") != null ? request.getParameter("mct001DrId") : request.getParameter("DR.coDrId"));
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("coDrId", mct001DrId);
        params.put("drId", drId);
        params.put("sdr", sdr);
        params.put("formato", formato);
        
        drObj = resolutorLogic.getCODrResolutorById(params);

        String transmitido = "N";
        MessageList messageList = (MessageList)request.getAttribute(Constantes.MESSAGE_LIST);
        if (messageList==null) messageList = new MessageList();
        
        boolean tieneSegundaFirma = false;
    	boolean tieneFirmaDigital = false;
    	boolean produccionControlada = false;
    	String tipoFirma = "false";
        
        if (drObj != null) {
        	
        	 if (drObj.getDrId()!=null) {
	        	//InteroperabilidadFirma etapa = null;
	            try {
	            	InteroperabilidadFirma etapa = ( drObj.getDrId()!=null && drObj.getSdr()!=null ) ? iopLogic.obtenerToken(drObj.getDrId().longValue(), drObj.getSdr()): null;
	    			//etapa = iopLogic.obtenerToken(drObj.getDrId().longValue(), drObj.getSdr());
	    			// Aqui importa si en algun momento hizo la firma por digital o no
	            	produccionControlada = iopLogic.produccionControlada(drObj.getDrId().longValue());
	            	
	    			if (etapa != null) {
	    				tipoFirma = iopLogic.tipoFirma(drObj.getDrId().longValue());
	
	    				if(!produccionControlada && "2".equals(iopLogic.tipoFirma(drObj.getDrId().longValue()))) {
	    					messageList.add(new Message("vuce.entidad.firmado.administrado"));
	    					tieneFirmaDigital = true;
	        				InteroperabilidadFirma etapaTres = iopLogic.obtenerEtapa(etapa.getToken(), 3);
	        				if (etapaTres != null) { // Solo podemos visualizar
	        					tieneSegundaFirma = true;
	        				} 
	    				}    				
	    				
	    			} 
	    			
	    			
	    		} catch (Exception e) {
	    			// TODO Auto-generated catch block
	    			logger.error("Error en CertificadoOrigneAdministracionEntidadController.cargarDatosDocumentoResolutivo", e);
	    		}
        	 }

            request.setAttribute("fechaFirma", drObj.getFechaFirmaEev());
            request.setAttribute("tieneFirmaDigital", tieneFirmaDigital);
			request.setAttribute("tipoFirma", tipoFirma);
			request.setAttribute("tieneSegundaFirma", tieneSegundaFirma);
			request.setAttribute("produccionControlada", produccionControlada);
			
            request.setAttribute("vigente", drObj.getVigente());
            
        	// Colocamos aqu� la informaci�n que corresponde a los datos del firmante
            request.setAttribute("secuenciaFuncionario",drObj.getSecuenciaFuncionario());
            request.setAttribute("usuarioFirmante",drObj.getUsuarioFirmanteExp());
            request.setAttribute("nroReferenciaCertificado", drObj.getNroReferenciaCertificado());
            request.setAttribute("nroReferenciaCertificadoOrigen", drObj.getNroReferenciaCertificadoOrigen());//20160526_NPC: Se agrega por ticket 98945
            request.setAttribute("cargoEmpresaExportador", drObj.getCargoEmpresaExportador()); //20140611_JMCA PQT-08
            request.setAttribute("telefonoEmpresaExportador", drObj.getTelefonoEmpresaExportador()); //20140611_JMCA PQT-08
            request.setAttribute("fechaFirmaExportador", drObj.getFechaFirmaExportador()); // 20150612_RPC: Acta.34:Firmas
            request.setAttribute("emitidoPosteriori2", drObj.getEmitidoPosteriori2()); // 20150612_RPC: Acta.34:Firmas
            
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            if (tieneFirmaDigital && !produccionControlada) { // Si tiene firma, precargamos datos
                Date today = Calendar.getInstance().getTime();        
                String strToday = df.format(today);
                request.setAttribute("usuarioExportador",drObj.getUsuarioFirmanteExp());
                
                request.setAttribute("usuarioEntidad", usuario.getNombreCompleto());
                request.setAttribute("fechaEntidad", strToday);
                try {
                	request.setAttribute("fechaExportador", drObj.getFechaFirmaExportador()!=null?df.format(drObj.getFechaFirmaExportador()):"");
                } catch (NullPointerException e) {
                    logger.error("getFechaFirmaExportador -> NullPointerException ", e);
                }
                
            }            
            
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("drId", drObj.getDrId());
            filter.put("sdr", drObj.getSdr());
            
        	if ("S".equals(drObj.getRectificaDr())){
        		if (drObj.getDrId() != null && request.getAttribute("drId") == null && request.getAttribute("DR.drId") == null && request.getParameter("DR.drId") == null) {

    		        CertificadoOrigenDR drRectifObj = resolutorLogic.getCODrRectificacionById(new Long(drObj.getDrId()), formato);

    		        if (drRectifObj != null) {
    		        	drObj = drRectifObj;
    		        }

            	}

                Table tModificacionesDR = ibatisService.loadGrid("suse.resolutor.solicitud.grilla", filter);
                request.setAttribute("tModificacionesDR", tModificacionesDR);

            	//Cargamos el filtro de versiones
                HashUtil<String, Object> filterVersionesDR = new HashUtil<String, Object>();
                filterVersionesDR.put("drId", drObj.getDrId());
                request.setAttribute("filterVersionesDR", filterVersionesDR);
                request.setAttribute("version", sdr);
        	}

            Table tAdjuntosCODR = ibatisService.loadGrid("resolutor.co.dr.adjuntos.grilla", filter);
            request.setAttribute("tAdjuntosCODR", tAdjuntosCODR);
            request.setAttribute("numAdjuntosCODR", tAdjuntosCODR!=null?tAdjuntosCODR.size():0);

            request.setAttribute("mostrarBtnFinalizacion", this.resolutorLogic.mostrarBtnFinalizacion(filter));

            if (drObj.getDrId() != null) {
            	transmitido = "S";
            }

            //this.cargarDocumentoResolutivo(ordenObj, drObj, request);

    		HashUtil<String, Object> filterAdicionales = new HashUtil<String, Object>();
    		filterAdicionales.put("coDrId", drObj.getCoDrId());

        	Table tFacturas = ibatisService.loadGrid("certificado_origen.factura.dr.datos", filterAdicionales);
        	request.setAttribute("tFacturasDr", tFacturas);

        	Table tMercancias = ibatisService.loadGrid("certificado_origen.mercancia.dr.datos", filterAdicionales);
        	request.setAttribute("tMercanciasDr", tMercancias);

        	request.setAttribute("rectificaDr", drObj.getRectificaDr());
        	request.setAttribute("idAcuerdo", drObj.getAcuerdoInternacionalId());

            if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato) || ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase(formato)){
            	request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(drObj.getAcuerdoInternacionalId()) );
            }

        	if (drObj.getSuceId() != null && drObj.getDrId() != null && drObj.getSdr() != null) {
        		request.setAttribute("requiereFirma", this.resolutorLogic.habilitarBtnFirma(drObj.getSuceId(), new Long(drObj.getDrId()), new Long(drObj.getSdr())));
        	}
        }

        Suce suceObj = this.suceLogic.getSuceById(drObj.getSuceId().intValue());
        request.setAttribute("suceCerrada", suceObj.getCerrada());
        request.setAttribute("transmitido", transmitido);

        RequestUtil.setAttributes(request, "DR.", drObj);
        request.setAttribute("codTipoDr", request.getParameter("codTipoDr"));
        request.setAttribute("tipoDr", drObj.getTipoDr());
        request.setAttribute("orden", request.getParameter("ordenId"));
        request.setAttribute("mto", request.getParameter("mto"));
        request.setAttribute("estadoRegistro", ordenObj.getEstadoRegistro());
        request.setAttribute("estadoSDR", drObj.getEstadoSDR());
        request.setAttribute("esRectificacion", drObj.getEsRectificacion());

        HashUtil<String, Object> filterEstadisticas = new HashUtil<String, Object>();
        if (drObj.getDrId() == null) {
        	request.setAttribute("numSDRPendientes", "0");
        } else {
            filterEstadisticas.put("drId", drObj.getDrId());
            request.setAttribute("numSDRPendientes", ibatisService.loadElement("co.numero.sdr.pendientes", filterEstadisticas).getString("CANTIDAD"));
        }

        DR objDR = resolutorLogic.getDRBorradorById(borradorDrId); //drObj = resolutorLogic.getDRBorradorById(borradorDrId);
        if (objDR == null) {
        	request.setAttribute("esReemplazo", "N");
            //request.setAttribute("tipoDr", "");
        } else {
        	request.setAttribute("esReemplazo", objDR.getEsReemplazo());
            //request.setAttribute("tipoDr", objDR.getTipoDr());
        }


        String puedeCerrar = "S";

        request.setAttribute("puedeCerrar", puedeCerrar);

        suceObj = suceLogic.loadSuceByNumero(numSuce.intValue());

        request.setAttribute("ordenId", ordenId);
        request.setAttribute("mto", mto);
        request.setAttribute("suceId", suceObj.getSuce());
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        //cargarDocumentoResolutivo(request, formatoDrId);
        puedeCerrar = (String)request.getAttribute("puedeCerrar");

        if (bloqueado==null) bloqueado = (String)request.getAttribute("bloqueado");
        if (bloqueado==null) bloqueado = "N";

        // Validamos que el DR este completo, listo para Confirmar
//        if (puedeCerrar.equalsIgnoreCase("S")) {
//            //puedeCerrar = (validateRequiredFields(formatoDrId)) ? "S" : "N";
//
//            // Si el DR tiene fraccionamientos, debemos validar que sus fraccionamientos esten aptos para confirmar
//            //puedeCerrar = (validateDrsFraccionamiento(formatoDrId)) ? "S" : "N";
//
//            if (suceLogic.tieneModificacionesSUCEPendientes(suceObj.getSuce())) {
//            	request.setAttribute("tieneModificacionesSUCEPendientes", "S");
//            }
//            //request.setAttribute("puedeCerrar", puedeCerrar);
//        }

        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            // Ahora validamos que la SUCE este en modo edicion para el usuario. Si no esta en edicion, se bloquea la edicion de los datos del DR
            int rol = -1;
            if (Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) {
                rol = Rol.CO_ENTIDAD_EVALUADOR.getCodigo();
            }
            
            if (Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) {
            	String edicion = null;
                Suce suceObjEdicion = suceLogic.getSuceByNumeroAndUsuarioId(numSuce.intValue(), usuario.getIdUsuario(), rol);
                if (suceObjEdicion != null) {
                    edicion = suceObjEdicion.getEdicion();
                }
                if (edicion==null) edicion = "N";

                if (edicion.equalsIgnoreCase("N")) bloqueado = "S";
            }
        } else {
            // Si es un usuario importador/exportador, se bloquea el DR
            bloqueado = "S";
        }
        
        String esRectificacion = "N";
        if (drObj.getSdr() != null) {
        	if (resolutorLogic.drEsRectificacion(new Long(drObj.getDrId()), drObj.getSdr())) {
        		esRectificacion = "S";
        	}
        }

        RepresentanteLegal representante = formatoLogic.getRepresentanteDetail(ordenObj.getOrden(),ordenObj.getMto());
    	Solicitante solicitante = formatoLogic.getSolicitanteDetail(ordenObj.getOrden(),ordenObj.getMto());


        if (ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(formato) ){

        	request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
            		new Object [] {""+solicitante.getNombre(), ""+solicitante.getNumeroDocumento(),
    				               /*""+(representante!=null?representante.getNombre():""), */ ""+ (drObj.getDrEntidad()!=null ? drObj.getDrEntidad():""),
    				               "<span id='idCausal'>"+drObj.getCausalDuplicadoCo().toLowerCase()+"</span>"}));

    	}
        if (ConstantesCO.CO_ANULACION.equalsIgnoreCase(formato) ){

    		request.setAttribute("descTextoAnulacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.textoAnulacion",
            		new Object [] {""+solicitante.getNombre(), ""+solicitante.getNumeroDocumento(),
    				""+(representante!=null?representante.getNombre():""), ""+ (drObj.getDrEntidad()!=null?drObj.getDrEntidad():"")}));

    	}


        HashUtil<String, Object> filterFirmate = new HashUtil<String, Object>();
        filterFirmate.put("idEntidad", usuario.getIdEntidad());
        
        System.out.println("**********certificadoOrigenAdministracionEntidadController");
        System.out.println("**********request.getAttribute(drId)"+request.getAttribute("drId"));
        if(request.getAttribute("drId") != null ) {

        //20/12/2017 NPCS si el mct003 viene de un mct001 sin firma digital, no deber� dejarle firmar digitalmente
        String habilitarFirmas = "S";
        
        if(formato.equals("mct001")||formato.equals("MCT003")){
        	Message messageFD =  certificadoOrigenLogic.habilitaFirma(drId,sdr);
        	
        	if(messageFD != null && messageFD.getMessage() != null){
        		habilitarFirmas = "N";
        		messageList.add(messageFD);
        	}
        	
        }
        request.setAttribute("habilitarFirmas", habilitarFirmas);
        }
   
        
        request.setAttribute("bloqueado", bloqueado);
        request.setAttribute("orden", ( datos.getLong("orden") != null ? datos.getLong("orden") : datos.getLong("ordenId")));
        request.setAttribute("tupaId", suceObj.getIdTupa());
        request.setAttribute("formatoId", suceObj.getIdFormato());
        request.setAttribute("numSuce", numSuce);
        request.setAttribute("formatoDrId", formatoDrId);
        request.setAttribute("formato", formato);
        request.setAttribute("esRectificacion", esRectificacion);
        request.setAttribute("filterFirmantesCertificador", filterFirmate);
        request.setAttribute("borradorDrId", borradorDrId);
        request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo()); // 20150612_RPC: Acta.34:Firmas

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idTupa", suceObj.getIdTupa());
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");

        Formato objFormato = (Formato)formatoLogic.getFormatoByTupa(suceObj.getIdFormato(), suceObj.getIdTupa());

        request.setAttribute("nombreFormato", objFormato.getFormato()+" - "+objFormato.getNombre()+" (TUPA: "+tupa+")");
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        return new ModelAndView(formCertificadoOrigenDR);
    }

    public ModelAndView generarBorrador(HttpServletRequest request, HttpServletResponse response) {
        MessageList messageList = this.resolutorLogic.generarBorradorDr(new Long(request.getParameter("suce")), request.getParameter("tipoDr"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("tipoDr", request.getParameter("tipoDr"));

        return this.abrirSuce(request, response);
    }

    public ModelAndView eliminarDRBorrador(HttpServletRequest request, HttpServletResponse response) {
        MessageList messageList = this.resolutorLogic.eliminarBorradorDr(new Long(request.getParameter("DR.coDrId")),new Long(request.getParameter("DR.suceId")) );
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return this.abrirSuce(request, response);
    }

    public ModelAndView grabarDrBorrador(HttpServletRequest request, HttpServletResponse response) {
        MessageList messageList = this.resolutorLogic.actualizarObsBorradorDr(new Long(request.getParameter("DR.coDrId")), request.getParameter("DR.observacionEvaluador"), request.getParameter("formato"), new Integer(request.getParameter("idAcuerdo")));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return this.cargarDatosDocumentoResolutivo(request, response);
    }

    public ModelAndView confirmarDrBorrador(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, Object> element = null;
    	logger.debug("JUMAN - confirmarDrBorrador");
    	element = this.resolutorLogic.confirmarBorradorDr(new Long(request.getParameter("DR.coDrId")), new Long(request.getParameter("DR.suceId")));
    	
        request.getSession().setAttribute(Constantes.MESSAGE_LIST, (MessageList)element.get("messageList"));

        //JMC_20180518_PRODUCCION CONTROLADA : Genera automaticamente el certificado origen electronico para Alianza
        Boolean produccionControlada = true;
        
        try {
			produccionControlada = iopLogic.produccionControlada(element.getLong("drId"));
			//InteroperabilidadFirma etapa = iopLogic.obtenerToken(element.getLong("drId"), element.getInt("sdr"));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error en CertificadoOrigneAdministracionEntidadController.confirmarDrBorrador", e);
		}
        
        if (produccionControlada && request.getParameter("idAcuerdo").equals(String.valueOf(ConstantesCO.AC_ALIANZA_PACIFICO))) {
        	iopLogic.generarCertificadoOrigenElectronicoPC(element.getLong("drId"), element.getInt("sdr"));
        	logger.debug("generarCertificadoOrigenElectronicoPC - Produccion Controlada: "+produccionControlada +" DRID: "+element.getLong("drId")+" sdr"+element.getInt("sdr"));
        }

        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen&seleccionado=1", true));
    }

    public ModelAndView cerrarSuce(HttpServletRequest request, HttpServletResponse response) {
        MessageList messageList = this.resolutorLogic.cerrarSuce(new Long(request.getParameter("DR.suceId")),
        														 new Long(request.getParameter("DR.drId")),
        														 new Long(request.getParameter("DR.sdr")),
        														 request.getParameter("tipoDr"),
        														 request.getParameter("formato"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return this.abrirSuce(request, response);
    }
    
    /**
	 * Guardar los datos de la Firma de DR
	 * @param request
	 * @param response
	 * @return
	 */
    //20140611_JMCA PQT-08
    public ModelAndView guardarDatosFirmaCertificadoOrigenDRForm(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long suceId = datos.getLong("DR.suceId");
    	long drId = new Long(request.getParameter("DR.drId").toString());
    	int sdr = datos.getInt("DR.sdr");    	
        
     	MessageList messageList = null;    
            
        try {
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("suceId", suceId);
        	filter.put("drId", drId);
        	filter.put("sdr", sdr);
        	filter.put("usuarioFirmante", datos.getString("usuarioFirmante"));
        	filter.put("secuenciaFuncionario", datos.getInt("secuenciaFuncionario"));
        	filter.put("fechaFirma", Util.getTimestampObject(datos.getString("fechaFirma")+" 00:00:00", "dd/MM/yyyy HH:mm:ss"));
        	filter.put("validaUsuario", "N");
        	filter.put("nroReferenciaCertificado", datos.getString("nroReferenciaCertificado"));
        	
        	filter.put("cargoEmpresaExportador", datos.getString("cargoEmpresaExportador"));
        	filter.put("telefonoEmpresaExportador", datos.getString("telefonoEmpresaExportador"));
        	
        	// 20150612_RPC: Acta.34:Firmas
        	try {
        		filter.put("fechaFirmaExportador",  datos.getString("fechaFirmaExportador") != null ? Util.getDateObject(datos.getString("fechaFirmaExportador")) : null);
        	} catch (Exception exc) {
        		System.err.println();
        	}
        	filter.put("emitidoPosteriori2", datos.getString("emitidoPosteriori2"));
        	
        	messageList = this.resolutorLogic.cargarDatosFirmaAdjuntoCODR(filter);
            
        } catch (Exception e) {
            e.printStackTrace();
        }        
        
        // Llenamos todo lo que vino para mantener la informaci�n en request
        RequestUtil.setAttributes(request);        
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        return this.cargarDatosDocumentoResolutivo(request, response);
    }

	/**
	 * Carga los Adjuntos de DR
	 * @param request
	 * @param response
	 * @return
	 */
    public ModelAndView cargarArchivoCertificadoOrigenDRForm(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long suceId = datos.getLong("DR.suceId");
    	long drId = new Long(request.getParameter("DR.drId").toString());
    	int sdr = datos.getInt("DR.sdr");
    	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;
        
     	MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
            	/* //20140611_JMCA se comenta por el PQT-08
            	HashUtil<String, Object> filter = new HashUtil<String, Object>();
            	filter.put("suceId", suceId);
            	filter.put("drId", drId);
            	filter.put("sdr", sdr);
            	filter.put("usuarioFirmante", datos.getString("usuarioFirmante"));
            	filter.put("secuenciaFuncionario", datos.getInt("secuenciaFuncionario"));
            	filter.put("fechaFirma", Util.getTimestampObject(datos.getString("fechaFirma")+" 00:00:00", "dd/MM/yyyy HH:mm:ss"));
            	filter.put("validaUsuario", "N");
            	filter.put("nroReferenciaCertificado", datos.getString("nroReferenciaCertificado"));
            	
            	filter.put("cargoEmpresaExportador", datos.getString("cargoEmpresaExportador"));
            	filter.put("telefonoEmpresaExportador", datos.getString("telefonoEmpresaExportador"));
            	
                
            	this.resolutorLogic.cargarDatosFirmaAdjuntoCODR(filter);
                */
                // Llenamos el objeto adjunto
                AdjuntoCertificadoOrigenDR adjunto = new AdjuntoCertificadoOrigenDR();
                adjunto.setSuceId(suceId);
                adjunto.setDrId(drId);
                adjunto.setSdr(sdr);
                adjunto.setNombreArchivo(ComponenteOrigenUtil.replaceSpecialCharacters(multipartFile.getOriginalFilename()));
                adjunto.setArchivo(multipartFile.getBytes());
                adjunto.setAdjuntoTipo(adjuntoTipo);
                
                messageList = this.resolutorLogic.cargarAdjuntoCODR(adjunto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Llenamos todo lo que vino para mantener la informaci�n en request
        RequestUtil.setAttributes(request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        //cargarDeclaracionJuradaMaterial(ordenObj,null,request);

        return this.cargarDatosDocumentoResolutivo(request, response);
    }

    @SuppressWarnings("unchecked")
    public ModelAndView cargarModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String controller = datos.getString("controller");
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int modificacionDrId = datos.getInt("modificacionDrId") != null ? datos.getInt("modificacionDrId") : Integer.parseInt(request.getAttribute("modificacionDrId").toString());
        int suceId = datos.getInt("suceId");
        int mensajeId = datos.getInt("mensajeId");
        String formato = datos.getString("formato");

        Suce suceObj = suceLogic.getSuceById(suceId);
        request.setAttribute("numSuce", suceObj.getNumSuce());
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

    	DR drObj = resolutorLogic.obtenerDRById(drId, sdr);
        request.setAttribute("vigente", drObj.getVigente());
        request.setAttribute("tipoDr", drObj.getTipoDr());

        ModificacionDR modificacionDrObj = suceLogic.loadModificacionDrById(drId, sdr, modificacionDrId, mensajeId);
        if (modificacionDrObj!=null) {

            request.setAttribute("modificacionDrId", modificacionDrId);
            request.setAttribute("mensaje", modificacionDrObj.getMensaje());
            request.setAttribute("mensajeId", modificacionDrObj.getMensajeId());
            request.setAttribute("transmitido", modificacionDrObj.getTransmitido());
            request.setAttribute("estadoRegistro", modificacionDrObj.getEstadoRegistro());
            request.setAttribute("tipoModificacionDR", modificacionDrObj.getTipoModificacionDR());

            String hide = modificacionDrObj.getTransmitido().equals("N")?"no":"yes";
            request.setAttribute("hide", hide);
        } else {
            request.setAttribute("transmitido", "N");
        }

        request.setAttribute("controller", controller);
        request.setAttribute("drId", drId);
        request.setAttribute("sdr", sdr);
        request.setAttribute("ordenId", datos.getInt("ordenId"));
        request.setAttribute("mto", datos.getInt("mto"));
        request.setAttribute("suceId", datos.getInt("suceId"));
        request.setAttribute("mct001DrId", datos.get("mct001DrId"));
        request.setAttribute("formato", formato);

        HashUtil<String, Object> filterEstadisticas = new HashUtil<String, Object>();
        filterEstadisticas.put("drId", drId);
        request.setAttribute("numSDRPendientes", ibatisService.loadElement("co.numero.sdr.pendientes", filterEstadisticas).getString("CANTIDAD"));


        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);

        return new ModelAndView(modificacionDR);
    }

    /**
     * Carga el detalle de la modificaci�n en el resolutor
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarModificacionSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        long ordenId = datos.getLong("ordenId");
        int mto = datos.getInt("mto");
        int mensajeId = datos.getInt("mensajeId");
        int modificacionSuceId = datos.getInt("modificacionSuceId");
        String estadoRegistroSUCE = datos.getString("estadoRegistro");
        String opcion = datos.getString("opcion");

        Orden ordenObj = ordenLogic.getOrdenById(ordenId, mto);
        int suceId = ordenObj.getSuce();
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", ordenObj.getSuce());
        filter.put("modifsuce", modificacionSuceId);
        filter.put("mensajeId", mensajeId);
        ModificacionSuce modifSuce = suceLogic.loadModificacionSuceByIdMensajeId(suceId, modificacionSuceId, mensajeId);
        
                
        //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
     	
      	String estadoAcuerdoPais = "";
        estadoAcuerdoPais = validaAcuerdoPais(ordenId);
        if ("I".equals(estadoAcuerdoPais)) {
          	request.setAttribute("estadoAcuerdoPais","I");
        }

        request.setAttribute("mensaje", modifSuce.getMensaje());
        request.setAttribute("suceId", suceId);
        request.setAttribute("modificacionSuceId", modificacionSuceId);
        request.setAttribute("ordenId", ordenId);
        request.setAttribute("orden", ordenId);
        request.setAttribute("idAcuerdo", datos.getInt("idAcuerdo"));
        request.setAttribute("mto", mto);
        request.setAttribute("estadoRegistro", modifSuce.getEstadoRegistro());
        request.setAttribute("estadoRegistroSUCE", estadoRegistroSUCE);
        request.setAttribute("controller", datos.getString("controller"));
        request.setAttribute("utilizaModificacionSuceXMto", ordenObj.getModificacionSuceXMto());
        request.setAttribute("mtoSUCE", modifSuce.getMto());
        request.setAttribute("opcion", opcion);
        request.setAttribute("mensajeId", mensajeId);
        request.setAttribute("motivoRechazo", modifSuce.getMotivoRechazo());
        request.setAttribute("formato", datos.getString("formato"));
        request.setAttribute("numDRBorrador", datos.getString("numDRBorrador"));

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("suce.resolutor.evaluador.adjuntos_subsanacion.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);

        // Obtenemos los adjuntos del rechazo
        filterADJ.remove("mensajeId");
        filterADJ.put("mensajeId", modifSuce.getMensajeIdRechazo());
        Table tAdjuntosRechazo = ibatisService.loadGrid("suce.resolutor.evaluador.adjuntos_subsanacion.grilla", filterADJ);
        request.setAttribute("tAdjuntosRechazo", tAdjuntosRechazo);

        // Notificaciones que atiende la Modificacion de SUCE (en caso sea de Subsanacion)
        filter.clear();
        filter.put("suceId", ordenObj.getSuce());
        filter.put("modificacionSuceId", modificacionSuceId);
        Table tNotificaciones = ibatisService.loadGrid("suce.resolutor.evaluador.subsanacion.notificacionesAtendidas.grilla", filter);
        request.setAttribute("tNotificaciones", tNotificaciones);

        if (modifSuce.getTipoModificacionSuce() == 2) {
            request.setAttribute("tipo", "S");
        } else {
            request.setAttribute("tipo", "M");
        }

        return new ModelAndView(modificacionSubsanacion);
    }

    /*public ModelAndView aprobacionModificacionSuce(HttpServletRequest request, HttpServletResponse response) {
        RequestUtil.setAttributes(request);
        return new ModelAndView(aprobacionModificacionSuce);
    }*/

    public ModelAndView aprobarModificacionSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = resolutorLogic.aprobarModificacionSubsanacion(datos);

        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("ordenId"), datos.getInt("mto"));
        messageList.setObject(ordenObj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        cargarInformacionEspecificaSuce(request, messageList);

        return new ModelAndView(this.abrirSuce);
    }

    public ModelAndView rechazoModificacionSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        RequestUtil.setAttributes(request);
        return new ModelAndView(rechazoModificacionSubsanacion);
    }

    public ModelAndView rechazarModificacionSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                byte [] bytes = multipartFile.getBytes();
                String nombreArchivo = multipartFile.getOriginalFilename();
                messageList = resolutorLogic.rechazarModificacionSubsanacion(datos, bytes, nombreArchivo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("ordenId"), datos.getInt("mto"));
        messageList.setObject(ordenObj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        cargarInformacionEspecificaSuce(request, messageList);

        return new ModelAndView(this.abrirSuce);
    }

    public ModelAndView denegarSolicitud(HttpServletRequest request, HttpServletResponse response) {
    	Map <String,Object> filtros = new HashMap<String,Object>();
    	filtros.put("ordenId", Long.valueOf(request.getParameter("orden")));
    	filtros.put("mto", Long.valueOf(request.getParameter("mto")));

        MessageList messageList = resolutorLogic.denegarSolicitud(filtros);

        request.getSession().setAttribute(Constantes.MESSAGE_LIST, messageList);

        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
    }

    public ModelAndView aprobarModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int modificacionDrId = datos.getInt("modificacionDrId");
        //double monto = datos.getDouble("monto");

        MessageList messageList = suceLogic.aprobarModificacionDr(drId, sdr, modificacionDrId);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        //return abrirSuce(request, response);
        return cargarModificacionDR(request, response);
    }

    public ModelAndView rechazoModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        RequestUtil.setAttributes(request);
        return new ModelAndView(rechazoModificacionDR);
    }

    public ModelAndView rechazarModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = null;
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int modificacionDrId = datos.getInt("modificacionDrId");
        String mensaje = datos.getString("mensaje");
        //byte [] bytes = multipartFile.getBytes();
        //String nombreArchivo = multipartFile.getOriginalFilename();

        messageList = suceLogic.rechazarModificacionDr(drId, sdr, modificacionDrId, mensaje);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        //return abrirSuce(request, response);
        return cargarModificacionDR(request, response);
    }

    public ModelAndView rectificacionDR(HttpServletRequest request, HttpServletResponse response) {
        RequestUtil.setAttributes(request);
        return new ModelAndView(rectificacionDR);
    }

    public ModelAndView rectificarDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Long dr = datos.getLong("drId");
        Integer sdr = datos.getInt("sdr");
        //Integer tipoModificacionDR = datos.getInt("tipoModificacionDR");
        String mensaje = null;//datos.getString("mensaje");

        HashUtil<String, Object> data = suceLogic.registrarRectificacionDrEval(dr, sdr, mensaje);
        MessageList messageList = (MessageList) data.get("messageList");
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        //DR drObj = (DR)messageList.getObject();
        if (data.get("sdrNew") == null) {
        	request.setAttribute("drId", dr);
	        request.setAttribute("sdr", sdr);
        } else {
	        request.setAttribute("drId", data.get("drId"));
	        request.setAttribute("sdr", data.get("sdrNew"));
        }
        request.setAttribute("formato", datos.getString("formato"));
        //RequestUtil.setAttributes(request);
        return cargarDatosDocumentoResolutivo(request, response);
    }

    public ModelAndView eliminarRectificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Long dr = new Long (request.getParameter("DR.drId").toString());
        Integer sdr = 2; //datos.getInt("DR.sdr");
        Integer mensajeId = 1;//datos.getInt("DR.mensajeId");

        HashUtil<String, Object> data = suceLogic.eliminarRectificacionDrEval(dr, sdr, mensajeId);
        MessageList messageList = (MessageList) data.get("messageList");
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        //DR drObj = (DR)messageList.getObject();
        //request.setAttribute("drId", data.get("drId"));
        //request.setAttribute("sdr", data.get("sdrNew"));
        //RequestUtil.setAttributes(request);
        return abrirSuce(request, response);
    }

    public ModelAndView confirmarRectificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Long dr = new Long (request.getParameter("DR.drId").toString());
        Integer sdr = new Integer (request.getParameter("DR.sdr").toString());
        Integer mensajeId = 1;//datos.getInt("DR.mensajeId");

        HashUtil<String, Object> data = suceLogic.confirmarRectificacionDrEval(dr, sdr);
        MessageList messageList = (MessageList) data.get("messageList");
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        //DR drObj = (DR)messageList.getObject();
        //request.setAttribute("drId", data.get("drId"));
        //request.setAttribute("sdr", data.get("sdrNew"));
        //RequestUtil.setAttributes(request);
        return abrirSuce(request, response);
    }

    public ModelAndView eliminarAdjunto(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        int id = 0;
        int mto = datos.getInt("mto");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            id = Integer.parseInt(row.getCell("ADJUNTO_ID").getValue().toString());

        	logger.debug("Se eliminara adjunto_id = "+id+", mto = "+mto);

        	adjuntoLogic.eliminarAdjuntoFormato(id, mto);
        }

        return cargarDatosDocumentoResolutivo(request, response);
    }

    public ModelAndView editarNotificacionSubsanacionOrden(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);

        if (!datos.getString("notificacionId").equals("") || datos.get("suceId") == null) {
            int notificacionId = -1;
            if (datos.get("notificacionId") != null && !datos.getString("notificacionId").equals("") ) {
            	notificacionId = datos.getInt("notificacionId");
            }
            cargarDatosNotificacionSubsanacionOrden(request, datos, notificacionId);
        } else {
            request.setAttribute("estado", "P");
            request.setAttribute("formato", datos.getString("formato"));
        }

        return new ModelAndView(notificacionSubsanacionOrden);
    }

    public ModelAndView registrarNotificacionSubsanacionOrden(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);

        MessageList messageList = resolutorLogic.registrarNotificacionSubsanacionOrden(datos);

        Integer notificacionId = (Integer)messageList.getObject();
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        cargarDatosNotificacionSubsanacionOrden(request, datos, notificacionId);

        return new ModelAndView(notificacionSubsanacionOrden);
    }

    private void cargarDatosNotificacionSubsanacionOrden(HttpServletRequest request, HashUtil<String, String> datos, Integer notificacionId) {
        // Cargamos los datos de la notificacion
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        if (notificacionId > -1){
            filter.put("notificacionId", notificacionId);
        }

        if (datos.get("suceId") == null) {
        	filter.put("ordenId", datos.getLong("ordenId"));
        	filter.put("mto", datos.getInt("mto"));
        }

        HashUtil<String, Object> datosNotificacion = ibatisService.loadElement("resolutor.evaluador.suce.notificacionSubsanacionSuce.element", filter);
        if (datosNotificacion != null && datosNotificacion.get("notificacionId") != null) {
            request.setAttribute("notificacionId", datosNotificacion.getString("notificacionId"));
            request.setAttribute("mensajeId", datosNotificacion.getString("mensajeId"));
            request.setAttribute("mensaje", datosNotificacion.getString("mensaje"));
            request.setAttribute("estado", datosNotificacion.getString("estado"));

            // Cargamos los adjuntos asociados a la notificacion
            filter.put("notificacionId", datosNotificacion.getInt("notificacionId"));
            Table tAdjuntosNotificacion = ibatisService.loadGrid("resolutor.evaluador.suce.notificacionSubsanacionSuce.adjuntos", filter);
            request.setAttribute("tAdjuntosNotificacion", tAdjuntosNotificacion);
        } else {
            request.setAttribute("estado", "P");
        }

        request.setAttribute("formato", datos.getString("formato"));
    }

	public ModelAndView cargarMercanciaFormDR(HttpServletRequest request,HttpServletResponse response) {
		HashUtil<String, String> datos = RequestUtil.getParameter(request);


		int drId = datos.getInt("drId");
		Integer secuencia = datos.getInt("secuencia");
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("drId", drId);
		filter.put("secuencia", secuencia);

		HashUtil<String, Object> dataMercancia = ibatisService.loadElement("dr.mercancia.datos", filter);
		dataMercancia.addPrefix("MERCANCIA");
		RequestUtil.setAttributes(request, dataMercancia);
		request.setAttribute("idAcuerdo", datos.getString("idAcuerdo"));
		request.setAttribute("codTipoDr", datos.getString("codTipoDr"));

	    return new ModelAndView(datosDocResolMercancia);

	}





    public ModelAndView aceptarDesestimientoSUCE(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = suceLogic.aceptarDesestimientoSUCE(datos);

        request.getSession().setAttribute(Constantes.MESSAGE_LIST, messageList);

        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
    }

    public ModelAndView rechazarDesestimientoSUCE(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = suceLogic.rechazarDesestimientoSUCE(datos);

        request.getSession().setAttribute(Constantes.MESSAGE_LIST, messageList);

        //return this.cargarListaSolicitudesCertificadoOrigen(request, response);
        return new ModelAndView(new RedirectView("admentco.htm?method=cargarListaSolicitudesCertificadoOrigen", true));
    }




 	public ResolutorLogic getResolutorLogic() {
 		return resolutorLogic;
 	}

 	public void setResolutorLogic(ResolutorLogic resolutorLogic) {
 		this.resolutorLogic = resolutorLogic;
 	}

    public OrdenLogic getOrdenLogic() {
		return ordenLogic;
	}

	public void setOrdenLogic(OrdenLogic ordenLogic) {
		this.ordenLogic = ordenLogic;
	}

	public FormatoLogic getFormatoLogic() {
		return formatoLogic;
	}

	public void setFormatoLogic(FormatoLogic formatoLogic) {
		this.formatoLogic = formatoLogic;
	}

	public SuceLogic getSuceLogic() {
		return suceLogic;
	}

	public void setSuceLogic(SuceLogic suceLogic) {
		this.suceLogic = suceLogic;
	}

	public CertificadoOrigenLogic getCertificadoOrigenLogic() {
		return certificadoOrigenLogic;
	}

	public void setCertificadoOrigenLogic(
			CertificadoOrigenLogic certificadoOrigenLogic) {
		this.certificadoOrigenLogic = certificadoOrigenLogic;
	}

	public IbatisService getIbatisService() {
		return ibatisService;
	}

	public AdjuntoLogic getAdjuntoLogic() {
		return adjuntoLogic;
	}

	public void setAdjuntoLogic(AdjuntoLogic adjuntoLogic) {
		this.adjuntoLogic = adjuntoLogic;
	}

 	public String getBandejaSolicitudes() {
 		return bandejaSolicitudes;
 	}

 	public void setBandejaSolicitudes(String bandejaSolicitudes) {
 		this.bandejaSolicitudes = bandejaSolicitudes;
 	}

 	public String getSolicitudesCertificado() {
 		return solicitudesCertificado;
 	}

 	public void setSolicitudesCertificado(String solicitudesCertificado) {
 		this.solicitudesCertificado = solicitudesCertificado;
 	}

 	public String getAsignacionEvaluadorSolicitudCertificado() {
 		return asignacionEvaluadorSolicitudCertificado;
 	}

 	public void setAsignacionEvaluadorSolicitudCertificado(
 			String asignacionEvaluadorSolicitudCertificado) {
 		this.asignacionEvaluadorSolicitudCertificado = asignacionEvaluadorSolicitudCertificado;
 	}

 	public String getAbrirSolicitudCertificado() {
 		return abrirSolicitudCertificado;
 	}

 	public void setAbrirSolicitudCertificado(String abrirSolicitudCertificado) {
 		this.abrirSolicitudCertificado = abrirSolicitudCertificado;
 	}

	public String getAbrirSuce() {
		return abrirSuce;
	}

	public void setAbrirSuce(String abrirSuce) {
		this.abrirSuce = abrirSuce;
	}

	public String getNotificacionSubsanacionSuce() {
		return notificacionSubsanacionSuce;
	}

	public void setNotificacionSubsanacionSuce(String notificacionSubsanacionSuce) {
		this.notificacionSubsanacionSuce = notificacionSubsanacionSuce;
	}

	public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public void setFormCertificadoOrigenDR(String formCertificadoOrigenDR) {
		this.formCertificadoOrigenDR = formCertificadoOrigenDR;
	}

	public void setModificacionDR(String modificacionDR) {
		this.modificacionDR = modificacionDR;
	}

	public String getModificacionSubsanacion() {
		return modificacionSubsanacion;
	}

	public void setModificacionSubsanacion(String modificacionSubsanacion) {
		this.modificacionSubsanacion = modificacionSubsanacion;
	}

	public String getRechazoModificacionSubsanacion() {
		return rechazoModificacionSubsanacion;
	}

	public void setRechazoModificacionSubsanacion(
			String rechazoModificacionSubsanacion) {
		this.rechazoModificacionSubsanacion = rechazoModificacionSubsanacion;
	}

	public void setRechazoModificacionDR(String rechazoModificacionDR) {
		this.rechazoModificacionDR = rechazoModificacionDR;
	}

	public void setRectificacionDR(String rectificacionDR) {
		this.rectificacionDR = rectificacionDR;
	}

	public String getNotificacionSubsanacionOrden() {
		return notificacionSubsanacionOrden;
	}

	public void setNotificacionSubsanacionOrden(String notificacionSubsanacionOrden) {
		this.notificacionSubsanacionOrden = notificacionSubsanacionOrden;
	}

	public void setDatosDocResolMercancia(String datosDocResolMercancia) {
		this.datosDocResolMercancia = datosDocResolMercancia;
	}
	
	public void setIopLogic(InteroperabilidadFirmasLogic iopLogic) {
		this.iopLogic = iopLogic;
	}

	//20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
    protected String validaAcuerdoPais (Long ordenId){
    	String res="";
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("result", null);
		filter.put("ordenId", ordenId);
		
    	try {
			ibatisService.executeSPWithObject("valida_acuerdo_pais_vigente",filter);
			if(filter.get("result")!=null) {
				res= filter.get("result").toString();
			}
		} catch (Exception e) {
			logger.error("Error en CertificadoOrigneAdministracionEntidadController.validaAcuerdoPais", e);
			
		}
       	return res;
    }

}