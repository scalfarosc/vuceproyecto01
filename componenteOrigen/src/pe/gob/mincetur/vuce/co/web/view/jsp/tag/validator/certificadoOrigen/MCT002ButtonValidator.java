package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;
import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;

public class MCT002ButtonValidator extends FormatoButtonValidator {

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        Object idFormatoEntidad = request.getAttribute("idFormatoEntidad");
        String bloqueado = (String)request.getAttribute("bloqueado");
        
        disabledButtons.put("actualizarDireccionAdicionalButton", Constantes.DISABLING_ACTION_DISABLE);
        
        if (bloqueado==null) bloqueado = "";

        if (idFormatoEntidad != null && bloqueado.equalsIgnoreCase("S")) {
        	disabledButtons.put("GrabarDetalleButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        if (!(usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
        		Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo()))) {
        	disabledButtons.put("confirmarFinEvalButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
        	String permiteConfirmarFinEval = request.getAttribute("mostrarBotonConfirmarFinEval") != null ? (String) request.getAttribute("mostrarBotonConfirmarFinEval") : "N";

        	if ("N".equals(permiteConfirmarFinEval)) {
        		disabledButtons.put("confirmarFinEvalButton", Constantes.DISABLING_ACTION_HIDE);
        	}
        }

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }



}
