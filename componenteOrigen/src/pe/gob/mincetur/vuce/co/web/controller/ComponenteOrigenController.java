package pe.gob.mincetur.vuce.co.web.controller;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.list.OptionList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.util.RequestUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import pe.gob.mincetur.vuce.co.bean.EntidadCertificadora;
import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Auditoria;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Usuario;
import pe.gob.mincetur.vuce.co.logic.AuditoriaLogic;
import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.UsuariosLogic;
import pe.gob.mincetur.vuce.co.remoting.ws.client.ConsultaRUCCWS;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;
import ConsultaRuc.BeanDdp;

public class ComponenteOrigenController extends MultiActionController {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);

    private UsuariosLogic usuariosLogic;

    private IbatisService ibatisService;

    private FormatoLogic formatoLogic;

    private AuditoriaLogic auditoriaLogic;

    private String registroUsuarioForm;

    private String modificacionUsuarioForm;

    private String mensajesPostLogueo;

    private String principal;

    private String principalVUCECentral;
    
    public void setUsuariosLogic(UsuariosLogic usuariosLogic) {
        this.usuariosLogic = usuariosLogic;
    }

    public void setIbatisService(IbatisService ibatisService) {
        this.ibatisService = ibatisService;
    }

    public void setFormatoLogic(FormatoLogic formatoLogic) {
        this.formatoLogic = formatoLogic;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

	public void setRegistroUsuarioForm(String registroUsuarioForm) {
        this.registroUsuarioForm = registroUsuarioForm;
    }

    public void setModificacionUsuarioForm(String modificacionUsuarioForm) {
		this.modificacionUsuarioForm = modificacionUsuarioForm;
	}

	public void setPrincipalVUCECentral(String principalVUCECentral) {
        this.principalVUCECentral = principalVUCECentral;
    }

    public void setMensajesPostLogueo(String mensajesPostLogueo) {
        this.mensajesPostLogueo = mensajesPostLogueo;
    }

    public void setAuditoriaLogic(AuditoriaLogic auditoriaLogic) {
        this.auditoriaLogic = auditoriaLogic;
    }

    public ModelAndView principalSimulacion(HttpServletRequest request, HttpServletResponse response) {
    	HttpSession session = request.getSession();
        UsuarioCO usuario = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
        
        String detalleConexionHeader = null;
        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            detalleConexionHeader = headerName + " : "+request.getHeader(headerName)+ "\n" + detalleConexionHeader;
        }

        String tipoLogueo = (String)request.getSession().getAttribute("tipoLogueo");
        //String tipoUsuario = (String)request.getSession().getAttribute("tipoUsuario");
        String rolUsuarioSOL = (String)request.getSession().getAttribute("rolUsuarioSOL");
        if (rolUsuarioSOL==null) rolUsuarioSOL = "O";
        String [] roles = (String [])request.getSession().getAttribute("roles");
        
        boolean usuarioPrincipalRegistrado = false;
        if (tipoLogueo.equalsIgnoreCase("R")) {
            // SIMULANDO USUARIO SOL
            usuario.getMap().put("tipOrigen", ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL);
            //usuario.getMap().put("tipUsuario", tipoUsuario.equalsIgnoreCase("P") ? ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL : ConstantesCO.AUT_TIPO_USUARIO_SOL_SECUNDARIO);
            usuario.getMap().put("opeComex", "");

            // Consultamos a SUNAT para obtener el Tipo de Persona del usuario SOL logueado
            BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(usuario.getNumRUC());
            
            //////// OJO: La siguiente linea se comento porque como se ingresa por un simulador, no se puede tener a ciencia cierta un numero RUC correcto
            // Setear el tipo de persona aqui solo sirve si el usuario no existe en BD
            if (datosRUC!=null && datosRUC.getDdp_identi()!=null) {
                usuario.setTipoPersona(""+Integer.parseInt(datosRUC.getDdp_identi()));
            } else {
                usuario.setTipoPersona(ConstantesCO.TIPO_PERSONA_JURIDICA);
            }
        } else if (tipoLogueo.equalsIgnoreCase("E")) {
            // SIMULANDO USUARIO EXTRANET
            usuario.getMap().put("tipOrigen", ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT);
            //usuario.getMap().put("tipUsuario", tipoUsuario.equalsIgnoreCase("P") ? ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL : ConstantesCO.AUT_TIPO_USUARIO_SOL_SECUNDARIO);
            usuario.getMap().put("opeComex", "");
        } else if (tipoLogueo.equalsIgnoreCase("D")) {
            // SIMULANDO USUARIO DNI
            usuario.getMap().put("tipOrigen", ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI);
            usuario.getMap().put("opeComex", "");

            // Setear el tipo de persona aqui solo sirve si el usuario no existe en BD
            usuario.setTipoPersona(ConstantesCO.TIPO_PERSONA_NATURAL);
        }
        
        int existe = 0;
        if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_RUC);
            usuario.setNumeroDocumento(usuario.getNumRUC());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL);
            //usuario.setPrincipal(usuario.getMap().get("tipUsuario").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL));

            // Asignarle el rol SUPERVISOR si corresponde
            if (rolUsuarioSOL.equalsIgnoreCase("A") || rolUsuarioSOL.equalsIgnoreCase("L")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("O")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("S")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("F")) {
                usuario.getRoles().put(Rol.CO_USUARIO_FIRMA.getNombre(), Rol.CO_USUARIO_FIRMA.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("T")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
                usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
                usuario.getRoles().put(Rol.CO_USUARIO_FIRMA.getNombre(), Rol.CO_USUARIO_FIRMA.getNombre());
            }
            
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_SOL);
            
            if (!usuario.isPrincipal()) {
                HashUtil<String, Object> filter = new HashUtil<String, Object>();
                filter.put("tipoDocumento", usuario.getTipoDocumento());
                filter.put("numeroDocumento", usuario.getNumeroDocumento());
                usuarioPrincipalRegistrado = ibatisService.loadGrid("co.usuarioPrincipalRegistrado", filter).size() > 0;
                /*if (usuarioPrincipalRegistrado) {
                    existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());
                }*/
            /*} else {
                existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());*/
            }
            existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());

        } else if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_EXTRANET);
            usuario.setNumeroDocumento(usuario.getLogin());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT);
            //usuario.setPrincipal(usuario.getMap().get("tipUsuario").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_EXT_PRINCIPAL));
            usuario.setNroRegistro(usuario.getLogin().substring(3,4));
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_ENTIDAD);

            // Asigno los roles seleccionados
            if (roles!=null) {
                for (int i=0; i < roles.length; i++) {
                    usuario.getRoles().put(roles[i], roles[i]);
                }
            }

            existe = usuariosLogic.existeUsuarioExtranet(usuario.getLogin());
        } else if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            //usuario = UsuarioFactory.getUsuarioDNI(usuario.getLogin());

            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_DNI);
            usuario.setNumeroDocumento(usuario.getLogin());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI);
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_DNI);
            existe = usuariosLogic.existeUsuarioDNI(usuario.getLogin());

            usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
        }
        
        if (existe!=0) {
            usuario.setIdUsuario(existe);

            //   LOG AUDITORIA - SESION   //
            Integer recursoOrigen = null;
            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) || usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
                recursoOrigen = ConstantesCO.AUDITORIA_RECURSO_ORIGEN_USUARIO_IMPORTADOR_EXPORTADOR;
            }
            if (recursoOrigen!=null) {
                Auditoria auditoria = new Auditoria();
                auditoria.setDetalleConexion(detalleConexionHeader);
                auditoria.setRecursoOrigen(recursoOrigen);
                auditoriaLogic.grabarInicioSesion(auditoria);
            }
        }

        usuario.imprimir();

    	// Asignamos el rol activo
    	asignarRolActivoUsuario(usuario, session);
    	
    	ModelAndView redirectPage = validarInformacionUsuarioPostLogueo(usuario, existe, usuarioPrincipalRegistrado, request);
        
        // Si el usuario es SOL y Principal, ponerle siempre el Rol "CO.USUARIO.OPERACION"
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.isPrincipal()) {
            usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
        }
        
        // OJO: ESTO ES PARA PRUEBAS
        if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            // Asignarle el rol SUPERVISOR si corresponde
            if (rolUsuarioSOL.equalsIgnoreCase("A") || rolUsuarioSOL.equalsIgnoreCase("L")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("O")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            }
        }
        
        request.getSession().setAttribute("logoutController", "logout.htm?method=logoutTest");
        
        return redirectPage;
    }

    private void asignarRolActivoUsuario(UsuarioCO usuario, HttpSession session) {
    	// Asignamos el rol activo
    	if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
        	OptionList listaRoles = usuario.getListaRolesSeleccionables();
        	
    		if (listaRoles.size() > 0) {
    	        usuario.setRolActivo(listaRoles.getOption(0).getCodigo());
    		}
    		
    	    session.setAttribute(ConstantesCO.LISTA_ROLES_USUARIO, listaRoles);
    	}
    }
    
    private ModelAndView validarInformacionUsuarioPostLogueo(UsuarioCO usuario, int existe, boolean usuarioPrincipalRegistrado, HttpServletRequest request) {
        ModelAndView redirectPage = null;
    	
    	// Si el usuario es EXTRANET
    	if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
        	boolean entidadCertificadoraOK = true;
        	
	    	// Si el usuario ya existe en BD, hay que buscar el estado de la entidad en BD
	    	if (existe!=0) {
	    		Usuario usuarioBD = usuariosLogic.getUsuarioById(usuario.getIdUsuario());
	    		
	    		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    		filter.put("entidadId", usuarioBD.getEntidadId());
	    		filter.put("usuarioId", usuarioBD.getUsuarioId());
	    		
	            // Obtener los datos de la Entidad
	            //HashUtil<String, Object> datosEntidad = ibatisService.loadElement("comun.entidad.element", filter);
	            
	            HashUtil<String, Object> datosEntidad = ibatisService.loadElement("comun.entidad.tramite.pendiente.element", filter);
	            
	            // Si la entidad del usuario ha sido desactivada, no permitirle entrar
	    		if ("I".equals(datosEntidad.getString("ESTADO"))) {
	    			Message message = new Message("co.logueo.entidadDesactivada");
	                MessageList messageList = new MessageList();
	                messageList.add(message);
	                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	                request.getSession().invalidate();
	                redirectPage = new ModelAndView(mensajesPostLogueo);
	                
	                entidadCertificadoraOK = false;
	    		}
	    	}
	    	// Si el usuario aun no existe en BD, se debe validar contra el nombre del usuario que viene desde SUNAT
	    	else {
	    		EntidadCertificadora entidadCertificadora = usuariosLogic.obtenerEntidadCertificadora(usuario.getNombreCompleto());
	    		
	    		// Si la entidad del usuario no existe o ha sido desactivada, no permitirle entrar
	    		if (entidadCertificadora==null || (entidadCertificadora!=null && entidadCertificadora.getEstado().equals("I"))) {
	    			Message message = new Message("co.logueo.entidadDesactivada");
	                MessageList messageList = new MessageList();
	                messageList.add(message);
	                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	                request.getSession().invalidate();
	                redirectPage = new ModelAndView(mensajesPostLogueo);
	                
	                entidadCertificadoraOK = false;
		    	}
	    	}
	    	
	    	// Si la entidad del usuario no paso la validacion, redirigir a la pantalla de mensajes post-logueo
	    	if (!entidadCertificadoraOK) {
	    		return redirectPage;
	    	}
    	}
    	
        if (usuario.getIdUsuario()!=null && !validarRolesUsuario(usuario, request)) {//NPCS 28012020 Sunat ya no envia Roles
            request.getSession().invalidate();
            redirectPage = new ModelAndView(mensajesPostLogueo);
        } else {
        	Usuario usuarioBD = null;
        	// Si el usuario Existe entonces cargar sus datos desde la BD
        	if (usuario.getIdUsuario()!=null) {
        		usuarioBD = usuariosLogic.getUsuarioById(usuario.getIdUsuario());
        		usuario.setIngresoMR(usuarioBD.getIngresoMR());
        		usuario.setIngresoUO(usuarioBD.getIngresoUO());
                usuario.setTipoDocumentoUS(usuarioBD.getTipoDocumentoUS());
                usuario.setNumeroDocumentoUS(usuarioBD.getNumeroDocumentoUS());
                usuario.setCargo(usuarioBD.getCargo());
                usuario.setNombreCompleto(usuarioBD.getNombre());
        	}
            if (existe!=0 && validarCambioRolUsuario(usuario)) {
                // Actualizamos los roles que vienen de SUNAT
                usuariosLogic.actualizarRolesUsuario(usuario);

                request.getSession().setAttribute(Constantes.USUARIO, usuario);
                cargarInformacionUsuarioEnFormularioDatos(request);
                request.setAttribute("noMostrarHeader", "S");
                redirectPage = new ModelAndView(registroUsuarioForm);
            } else {
                if (existe!=0 && "S".equals(usuarioBD.getIngresoUO())) {
                    // Validamos si es que el usuario tiene NOMBRE y CARGO
                    if (usuarioBD.getNombre()==null || usuarioBD.getTipoDocumentoUS()==null || usuarioBD.getNumeroDocumentoUS()==null || usuarioBD.getCargo()==null) {
                        request.getSession().setAttribute(Constantes.USUARIO, usuario);
                        cargarInformacionUsuarioEnFormularioDatos(request);
                        request.setAttribute("noMostrarHeader", "S");
                    	return new ModelAndView(modificacionUsuarioForm);
                    } else {
	                    // Actualizamos los roles que vienen de SUNAT
	                    usuariosLogic.actualizarRolesUsuario(usuario);

	                    ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuario, ibatisService, request);

	                    if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.CENTRAL")) {
	                        redirectPage = new ModelAndView(principalVUCECentral);
	                    } else if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD.CONSULTA_DR") && 
	                    		   !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") && 
	                    		   !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.CENTRAL")) {
	                        redirectPage = new ModelAndView(new RedirectView("consultaDR.htm?method=inicioConsultaDR", true));
	                    } else {
	                        // Actualizamos la informacion del RUC que viene de SUNAT
	                        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.isPrincipal()) {
	                            /*HashUtil<String, Object> datosFichaRUC = obtenerDatosFichaRUC(usuario);
	                            usuariosLogic.actualizarDatosRUCUsuario(usuario, datosFichaRUC);*/
	                        }

	                        redirectPage = new ModelAndView(principal);
	                    }
                    }
                }
                // EL USUARIO ESTA EN BD PERO NUNCA ENTRO A ORIGEN
                else if (existe!=0 && !"S".equals(usuarioBD.getIngresoUO())) {
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(modificacionUsuarioForm);
                } else if (usuario.getRoles()==null || usuario.getRoles().isEmpty()) {
                	//NPCS 28012020 Sunat ya no envia Roles
                    //Message message = new Message("co.logueo.usuarioSinRoles");
                    //MessageList messageList = new MessageList();
                    //messageList.add(message);
                    //request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    //request.getSession().invalidate();
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(registroUsuarioForm); 
                /*} else if (existe==0 && usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
                    Message message = new Message("co.logueo.usuarioExtranetNoRegistrado");
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    request.getSession().invalidate();
                    redirectPage = new ModelAndView(mensajesPostLogueo);*/
                } else {
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(registroUsuarioForm);
                }
            }
        }

        return redirectPage;
    }

    private boolean validarCambioRolUsuario(UsuarioCO usuario) {
        boolean cambio = false;
        return cambio;
    }
    
    private boolean validarRolesUsuario(UsuarioCO usuario, HttpServletRequest request) {
        boolean ok = true;
        HashUtil roles = usuario.getRoles();

        
        // Si no tiene un rol que comience con CO entonces no dejarle entrar, salvo que sea secundario y no tenga ni un rol
        if (!ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO") && !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "VUCE.SUNAT.ESPECIALISTA")) { //09.10.2017 JMC Alianza 
        	ok = false;
            Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
            MessageList messageList = new MessageList();
            messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        }
        
        // Validamos la correspondencia entre login de USUARIO y ROLES
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") || ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "VUCE.SUNAT")) {//09.10.2017 JMC Alianza
                if (usuario.getNumeroDocumento()==null || !usuario.getNumeroDocumento().startsWith("EXTA")) {
                    ok = false;
                    Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                }
            }
            
            // Rol que comienza con CO.ENTIDAD.*, debe comenzar con EXT
            if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD")) {
                if (usuario.getNumeroDocumento()==null || !usuario.getNumeroDocumento().startsWith("EXTA")) {
                    ok = false;
                    Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                }
            }
            
        } else if (!usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            // No debe tener ningun rol que comience con CO.ENTIDAD.* o CO.ADMIN.* o CO.SUNAT
            if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD") || ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") || ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "VUCE.SUNAT")) { //09.10.2017 JMC Alianza
                ok = false;
                Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
                MessageList messageList = new MessageList();
                messageList.add(message);
                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
            }
        }

        return ok;
    }
    
    /*private HashUtil<String, Object> obtenerDatosFichaRUC(UsuarioCO usuario) {
        HashUtil<String, Object> datosFichaRUC = new HashUtil<String, Object>();
        
        if (!ConsultaRUCCWS.servicioDisponible()) {
        	return datosFichaRUC;
        }
        
        // OJO: Obtener de la Consulta RUC para obtener los datos del RUC. Nos importan:
        // - Tipo Contribuyente: Persona Natural o Persona Juridica para saber si pedir "Datos de Empresa" y establecer "USUARIO.nombre" o "EMPRESA.nombre"
        // - Demas datos del RUC: Direccion, Telefono, Email, Fax
        String numeroDocumento = usuario.getNumeroDocumento();
        BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(numeroDocumento);
        String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(numeroDocumento);
        BeanDds datosSecundariosRUC = ConsultaRUCCWS.getDatosSecundarios(numeroDocumento);
        BeanT1144 datosT1144 = ConsultaRUCCWS.getDatosT1144(numeroDocumento);

        if (datosRUC!=null && domicilioLegal!=null && datosSecundariosRUC!=null && datosT1144!=null &&
            datosRUC.getDdp_numruc()!=null && datosRUC.getDdp_identi()!=null) {
            String tipoPersona = Integer.valueOf(datosRUC.getDdp_identi()).toString();
            usuario.setTipoPersona(tipoPersona);
            if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_NATURAL)) {
                datosFichaRUC.put("USUARIO.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("USUARIO.direccion", domicilioLegal.trim());
                datosFichaRUC.put("USUARIO.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("USUARIO.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("USUARIO.email", datosT1144.getCod_correo1().trim());

                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamento", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provincia", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distrito", datosUbigeo.get("DISTRITOID"));

                datosFichaRUC.put("ubigeoUsuarioEditable", "no");

            } else if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA)) {
                datosFichaRUC.put("EMPRESA.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("EMPRESA.direccion", domicilioLegal.trim());
                datosFichaRUC.put("EMPRESA.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("EMPRESA.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("EMPRESA.email", datosT1144.getCod_correo1().trim());

                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamentoE", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provinciaE", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distritoE", datosUbigeo.get("DISTRITOID"));

                // Obtenemos los Representantes Legales de la Empresa
                Table tablaRepresentantesLegales = usuariosLogic.obtenerTablaRepresentantesLegalesInicial(numeroDocumento);
                datosFichaRUC.put("tablaRepresentantesLegales", tablaRepresentantesLegales);

                datosFichaRUC.put("ubigeoUsuarioEditable", "yes");
            }
        }
        return datosFichaRUC;
    }
    */
    public void cargarInformacionUsuarioEnFormularioDatos(HttpServletRequest request) {
        //HashUtil<String, String> datos = RequestUtil.getParameter(request);
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        boolean pantallaRegistroDatosUsuario = usuario.getIdUsuario()==null;

        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            request.setAttribute("USUARIO.usuariosol", usuario.getUsuarioSOL());
            request.setAttribute("ubigeoUsuarioEditable", "yes");

            // El usuario es Principal o no es Agente de Aduana ni Laboratorio
            // 20130711: Se quito la validacion ya que tanto en usuario principal como secundario debe cargarse los datos de la empresa 
            //if (usuario.isPrincipal()) {
                HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
                Enumeration<String> keys = datosFichaRUC.keys();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    request.setAttribute(key, datosFichaRUC.get(key));
                }
            //}

            request.setAttribute("EMPRESA.numdoc", usuario.getNumRUC());
        } else {
            //request.setAttribute("USUARIO.nombre", usuario.getNombres());
            request.setAttribute("ubigeoUsuarioEditable", "yes");
        }

        if (!pantallaRegistroDatosUsuario) {
            HashUtil<String, Object> filterProvincia = new HashUtil<String, Object>();
            HashUtil<String, Object> filterDistrito = new HashUtil<String, Object>();
            
            Solicitante solicitante = formatoLogic.getUsuarioDetail(usuario);
            
            RequestUtil.setAttributes(request, "USUARIO.", solicitante);

            request.setAttribute("USUARIO.idUsuario", solicitante.getUsuarioId());
	    	request.setAttribute("USUARIO.idEntidad", usuario.getIdEntidad());

	    	if (solicitante.getUbigeo() != null) {
                // Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
                request.setAttribute("departamento", element.get("DEPARTAMENTOID"));
                request.setAttribute("provincia", element.get("PROVINCIAID"));
                request.setAttribute("USUARIO.distrito", element.get("DISTRITOID"));

                filterProvincia.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistrito.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistrito.put("provincia",element.get("PROVINCIAID"));
            } else {
                filterProvincia.put("departamento",0);
                filterDistrito.put("departamento",0);
                filterDistrito.put("provincia",0);
            }
            request.setAttribute("filterProvincia", filterProvincia);
            request.setAttribute("filterDistrito", filterDistrito);
        }
        request.setAttribute("USUARIO.tipodocumento", usuario.getTipoDocumento());
        request.setAttribute("USUARIO.numdoc", usuario.getNumeroDocumento());
        request.setAttribute("USUARIO.usuarioTipoId", usuario.getTipoUsuario());
        request.setAttribute("USUARIO.principal", usuario.isPrincipal() ? "S" : "N");
        request.setAttribute("USUARIO.tipoPersona", usuario.getTipoPersona());
        request.setAttribute("USUARIO.tipodocumentoUS", usuario.getTipoDocumentoUS());
        request.setAttribute("USUARIO.numdocUS", usuario.getNumeroDocumentoUS());
        request.setAttribute("USUARIO.cargo", usuario.getCargo());

        request.setAttribute("nombreDatosEmpresa", "EMPRESA");

        boolean mostrarDatosEmpresa = usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.getTipoPersona().equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA) &&
                                      !usuariosLogic.existeEmpresa(usuario.getNumRUC());

        request.setAttribute("mostrarDatosEmpresa", mostrarDatosEmpresa);

        if (mostrarDatosEmpresa && !pantallaRegistroDatosUsuario) {
            HashUtil<String, Object> filterProvinciaE = new HashUtil<String, Object>();
            HashUtil<String, Object> filterDistritoE = new HashUtil<String, Object>();

            //Cargar datos del Agente de Aduana o Laboratorio
            Solicitante empresa = formatoLogic.getEmpresaDetail(usuario);
            RequestUtil.setAttributes(request, "EMPRESA.", empresa);
            if (empresa.getUbigeo() != null) {
                // Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(empresa.getUbigeo());
                request.setAttribute("departamentoE", element.get("DEPARTAMENTOID"));
                request.setAttribute("provinciaE", element.get("PROVINCIAID"));
                request.setAttribute("EMPRESA.distrito", element.get("DISTRITOID"));

                filterProvinciaE.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistritoE.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistritoE.put("provincia",element.get("PROVINCIAID"));
            } else {
                filterProvinciaE.put("departamento",0);
                filterDistritoE.put("departamento",0);
                filterDistritoE.put("provincia",0);
            }
            request.setAttribute("filterProvinciaE", filterProvinciaE);
            request.setAttribute("filterDistritoE", filterDistritoE);
        }
    }

    public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuario, ibatisService, request);
        return new ModelAndView(principal);
    }

    public ModelAndView principalVUCECentral(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuario, ibatisService, request);
        return new ModelAndView(principalVUCECentral);
    }

}