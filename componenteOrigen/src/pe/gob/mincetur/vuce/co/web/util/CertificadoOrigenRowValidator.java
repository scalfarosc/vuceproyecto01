package pe.gob.mincetur.vuce.co.web.util;

import java.util.Hashtable;

import org.jlis.core.util.HashUtil;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.RowValidator;

public class CertificadoOrigenRowValidator implements RowValidator {

	public void validate(Hashtable cellFormatters, Row row, HashUtil arg2) {

        Cell cell1 = row.getCell("ORDEN");
        Cell cell2 = row.getCell("SUCE");
    	GenericCellFormatter cellFormatter1 = (GenericCellFormatter)cellFormatters.get(cell1.getColumnName());
    	GenericCellFormatter cellFormatter2 = (GenericCellFormatter)cellFormatters.get(cell2.getColumnName());
        if (row.getCell("SUCE").getValue() == null || row.getCell("SUCE").getValue().toString().equals("")){
        	cellFormatter1.setEditable(true);
        	cellFormatter2.setEditable(false);
        } else {
        	cellFormatter1.setEditable(false);
        	cellFormatter2.setEditable(true);
        }

    }
}
