package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class ProductorValidadorAsignadosEstadosCellValidator implements CellValidator {

    public void mostrarAccion(GenericCellFormatter cellFormatter, Cell cell, ButtonCell buttonCell) {

    	String estado = null;

        if (cell.getValue() != null && !cell.getValue().toString().equals("")) {
        	// Obtenemos el estado
        	estado = cell.getValue().toString();
        }

        if (estado != null) {
        	if (ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(estado)) { // Si esta en estado PENDIENTE DE ACEPTACION DE VALIDACION
        		buttonCell.setOnClick("calificarValidacion");
        		buttonCell.setImage("/co/imagenes/flecha_asignar.gif");
        	} else if (ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(estado)) { // Si esta en estado PENDIENTE DE VALIDACION
        		buttonCell.setOnClick("editarDeclaracionJuradaPendienteValidacion");
        		buttonCell.setImage("/co/imagenes/editar.gif");
        	} else if (ConstantesCO.DJ_ESTADO_APROBADA.equals(estado)) {
        		buttonCell.setOnClick("verDJAprobada");
        		buttonCell.setImage("/co/imagenes/editar.gif");
        	} else { // Si no est� en ninguno de esos estados, no se muestra nada
        		buttonCell.setOnClick("verDJModoLectura");
        		buttonCell.setImage("/co/imagenes/ver.gif");
        	}
        }

    }

    public void mostrarAlertNotif(GenericCellFormatter cellFormatter, Cell cell, ButtonCell buttonCell) {

    	String esModif = null;

        if (cell.getValue() != null && !cell.getValue().toString().equals("")) {
        	// Obtenemos el estado
        	esModif = cell.getValue().toString();
        }

        if (esModif != null) {
        	if (ConstantesCO.OPCION_SI.equals(esModif)) { // Si viene de una notificacion de subsanaci�n
        		buttonCell.setImage("/co/imagenes/tip.gif");
                buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.dj.notificacionPendiente"));
        	} else {
        		buttonCell.setImage("");
        		buttonCell.setAlt("");
        	}
        }

    }

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
	}

}
