package pe.gob.mincetur.vuce.co.web.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.jlis.web.util.TableUtil;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.dao.DBSessionContext;
import pe.gob.mincetur.vuce.co.dao.InteroperabilidadFirmaDAO;
import pe.gob.mincetur.vuce.co.domain.AdjuntoCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.AdjuntoRequeridoDJMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;
import pe.gob.mincetur.vuce.co.service.TransmisionPackService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.OptionListFactory;
import pe.gob.mincetur.vuce.co.util.Rol;

/**
*Objeto :	CertificadoOrigenController
*Descripcion :	Clase Controladora para los Certificados de Origen - Segunda Versión.
*Fecha de Creacion :	
*Autor :	E. Oscátegui - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		05/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ
*/

public class CertificadoOrigenController extends CertificadoOrigenFormatoController {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);

    private String formFactura;

    private String formatoMercancia;

    private String formatoMercanciaDeclaracionJurada;

    private String formMaterial;

    private String formatoBusqDj;

	private String formProductor;

	private String formDJExportadorAut;

	private String formDJHistoricoExportador;

    private String adjuntosDJ;

    private String adjuntosMaterialDJ;

    private String modificacionDR;

    private String calificarValidacionDj;

    private String formDJRechazo;

    private String prevSubsanacionSolicitud;

    private String formProductoDj;

    private String listadoPaisesG2;
    
    InteroperabilidadFirmaDAO iopDAO; // 06/12/2016 JMC ELIMINAR AP
    
    TransmisionPackService iopService; // 06/12/2016 JMC ELIMINAR AP    
    
    public void setIopDAO(InteroperabilidadFirmaDAO iopDAO) {
		this.iopDAO = iopDAO;
	}

	public void setIopService(TransmisionPackService iopService) {
		this.iopService = iopService;
	}

    public ModelAndView cargarFormCOFactura(HttpServletRequest request, HttpServletResponse response) {
    	return cargarFormCOFactura(request, response, null);
    }

    /**
     * Carga la pantalla de registro de una factura
     *
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarFormCOFactura(HttpServletRequest request, HttpServletResponse response, HashUtil<String, Object> datos){
    	MessageList messagelist = null;
    	if (datos == null || datos.size() == 0){
    		datos = RequestUtil.getParameter(request);
    	}
    	if(request.getAttribute(Constantes.MESSAGE_LIST)!=null)
    		messagelist = (MessageList)request.getAttribute(Constantes.MESSAGE_LIST);

        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden")!=null?request.getParameter("orden").toString():request.getParameter("ordenId").toString()),
        										 new Integer(request.getParameter("mto")!=null? request.getParameter("mto").toString(): request.getParameter("mtoAdj").toString()));

        int secuencia = Integer.parseInt(request.getParameter("secuencia")!=null? request.getParameter("secuencia").toString():request.getParameter("secuenciaFactura"));
        String formato = (request.getParameter("formato")!=null?request.getParameter("formato").toString():request.getParameter("formatoAdj").toString());

        if (secuencia!=0) {
            COFactura facturaObj = certificadoOrigenLogic.getCOFacturaById(ordenObj.getIdFormatoEntidad(), secuencia, formato);
            this.cargarCOFactura(ordenObj, facturaObj, request, datos, messagelist);
        }else{
            request.setAttribute("secuencia", new Integer(secuencia));
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("faltaAdjunto", ConstantesCO.OPCION_SI);
        }

        String idAcuerdo = request.getParameter("idAcuerdo")!=null?request.getParameter("idAcuerdo").toString():request.getParameter("idAcuerdoAdj").toString();
        		
        request.setAttribute("CO_FACTURA.coId", ordenObj.getIdFormatoEntidad());
        request.setAttribute("formato", formato);
        request.setAttribute("idFormato", (request.getParameter("idFormato")!=null?request.getParameter("idFormato").toString():request.getParameter("formatoId").toString()));
        request.setAttribute("idAcuerdo", idAcuerdo);
        request.setAttribute("bloqueado", (request.getParameter("bloqueado")!= null ? request.getParameter("bloqueado").toString():(request.getParameter("bloqueadoAdj")!=null?request.getParameter("bloqueadoAdj").toString():"N")));

        // Colocamos la lista de si y no para los combos
        request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());
        
        //JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", idAcuerdo+versionAcuerdo);                

        return new ModelAndView(formFactura);
    }

    public void cargarCOFactura(Orden ordenObj, COFactura facturaObj, HttpServletRequest request, HashUtil<String, Object> datos, MessageList messageList){
        //HashUtil<String, String> datos = RequestUtil.getParameter(request);

    	if (datos == null || datos.size() == 0){
    		datos = RequestUtil.getParameter(request);
    	}

        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        RequestUtil.setAttributes(request, "CO_FACTURA.", facturaObj);

         request.setAttribute("secuencia", facturaObj.getSecuencia());

         //Validamos si la factura ya fue registrada y muestra un mensaje informativo
         String mensajeFactura = validaFacturaRegistrada(ordenObj.getOrden(), ordenObj.getMto(), facturaObj.getNumero());
         if(!"".equals(mensajeFactura.toString())){ 
     		if (messageList == null) {
     			messageList = new MessageList();
     		}
         	messageList.add(new Message("message", mensajeFactura));
         	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
         }

         
        // Adjuntos
        HashUtil<String, Object> filterAdj = new HashUtil<String, Object>();
        filterAdj.put("adjuntoId", facturaObj.getAdjuntoId());
        Table tAdjuntos = ibatisService.loadGrid("adjunto.factura.grilla", filterAdj);
        request.setAttribute("tAdjuntosFactura", tAdjuntos);


        if ( tAdjuntos == null || tAdjuntos.isEmpty() ) {

    		Message message = new Message("co.falta.adjunto.factura.individual");
    		if (messageList == null) {
    			messageList = new MessageList();
    		}
    		messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

            request.setAttribute("faltaAdjunto", ConstantesCO.OPCION_SI);

        }


        request.setAttribute("orden", ordenObj.getOrden());
        request.setAttribute("mto", ordenObj.getMto());
        request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
        request.setAttribute("CO_FACTURA.codId", ordenObj.getIdFormatoEntidad());
        request.setAttribute("formato", datos.getString("formato"));
        request.setAttribute("idAcuerdo", datos.getString("idAcuerdo"));
        request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());

    }

    /**
     * Graba una factura de una solicitud de certificado de origen.
     * @param request Objeto request.
     * @param response Objeto response.
     * @return ModelAndView Vista de página.
     */
    public ModelAndView grabarCOFactura(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CO_FACTURA.");
            HashUtil<String, String> elements = RequestUtil.getParameter(request);

            Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));

            // Cargamos la información de la factura
            COFactura facturaObj = certificadoOrigenLogic.convertMapToCOFactura(datos);
            facturaObj.setAcuerdoInternacionalId(ordenObj.getIdAcuerdo());
            MessageList messageList = null;

            // Si la factura no existe
            if ( elements.get("secuencia").toString().equals("0") ) {
                messageList = certificadoOrigenLogic.insertCOFactura(facturaObj, elements.getString("formato"));
                facturaObj = (COFactura) messageList.getObject();
            } else { // Estamos ante un modificar
                facturaObj.setSecuencia(Integer.parseInt(elements.get("secuencia").toString()));
                messageList = certificadoOrigenLogic.updateCOFactura(facturaObj, elements.getString("formato"));
            }

            if ( facturaObj != null ) {

            	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            	MultipartFile multipartFile = multipartRequest.getFile("archivo");
                MessageList messageListAdj = null;
            	if (multipartFile != null && multipartFile.getSize() > 0) {
	            	HashUtil<String, Object> datosAdj = RequestUtil.getParameter(request);
	            	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;
	            	Message message = null;
	                if (request instanceof MultipartHttpServletRequest) {
	                	try {

	                        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	                        filter.put("coId", datosAdj.getLong("CO_FACTURA.coId"));
	                        filter.put("secuenciaFactura", facturaObj.getSecuencia());
	                        filter.put("ordenId", datosAdj.getLong("orden"));
	                        filter.put("mto", datosAdj.getInt("mto"));
	                        filter.put("formatoId", datosAdj.getInt("idFormato"));
	                        filter.put("adjuntoRequerido", null);
	                        filter.put("nombreArchivo", ComponenteOrigenUtil.replaceSpecialCharacters(multipartFile.getOriginalFilename()));
	                        filter.put("archivo", multipartFile.getBytes());
	                        filter.put("formato", datosAdj.getString("formato"));

	                        messageListAdj = formatoLogic.insertAdjuntoFactura(filter);
	                    } catch (Exception e) {
	                    	message = ComponenteOrigenUtil.getErrorMessage(e);
	                    	messageList.add(message);
	                        e.printStackTrace();
	                    }
	                }
            	}

            	if (messageListAdj != null) {
            		Message msg = null;
            		for (Object object : messageListAdj) {
            			if (object != null) {
            				msg = (Message) object;
            				if ( msg.getMessageKey() == null || !"insert.success".equals(msg.getMessageKey()) ) {
            					messageList.add(object);
            				}
            			}
					}
            	}

            	facturaObj = this.certificadoOrigenLogic.getCOFacturaById(ordenObj.getIdFormatoEntidad(), facturaObj.getSecuencia(), elements.getString("formato"));
            	cargarCOFactura(ordenObj, facturaObj, request, null, messageList);
            } else {
            	cargarFormCOFactura(request, response);
            }

            request.setAttribute("CO_FACTURA.coId", datos.get("coId"));
            request.setAttribute("formato", elements.getString("formato"));
            request.setAttribute("idFormato", elements.getString("idFormato"));
            request.setAttribute("idAcuerdo", elements.getString("idAcuerdo"));
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
            // Colocamos la lista de si y no para los combos
            request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());
            
        	//JMC 23/06/2017 Alianza
        	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
        	request.setAttribute("idAcuerdoVersion", elements.getString("idAcuerdo")+versionAcuerdo);

            return new ModelAndView(formFactura);
    }

    /**
     * Elimina una factura de una solicitud de certificado de origen.
     * @param request Objeto request.
     * @param response Objeto response.
     * @return ModelAndView Vista de página.
     */
    public ModelAndView eliminarCOFactura(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CO_FACTURA.");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        COFactura facturaObj = (COFactura) this.certificadoOrigenLogic.convertMapToCOFactura(datos);
        facturaObj.setSecuencia(Integer.parseInt(elements.get("secuencia").toString()));
        MessageList messageList = this.certificadoOrigenLogic.deleteCOFactura(facturaObj, elements.getString("formato"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden").toString()), Integer.parseInt(request.getParameter("mto").toString()));
        messageList.setObject(ordenObj);
        
        cargarInformacionFormato(request, messageList);

        return identificarPantallaFormato(elements.getString("formato"));
    }

    public ModelAndView grabarCOMercancia(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CO_MERCANCIA.");
            HashUtil<String, String> elements = RequestUtil.getParameter(request);

            Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));
            
            String certificadoReexportacion = elements.getString("certificadoReexportacion");

            if (datos.get("djId") == null || "".equals(datos.get("djId").toString().trim())){
            	datos.put("djId", elements.get("djId"));
            }

            CertificadoOrigenMercancia mercanciaObj = certificadoOrigenLogic.convertMapToCOMercancia(datos);
            mercanciaObj.setCoId(new Long(elements.get("coId").toString()));
            mercanciaObj.setAcuerdoInternacionalId(ordenObj.getIdAcuerdo());
            mercanciaObj.setOrdenId(elements.getLong("orden"));
            mercanciaObj.setMto(elements.getInt("mto"));
            mercanciaObj.setCalificacionUoId(Util.longValueOf((request.getParameter("CALIFICACION.calificacionUoId"))));
            MessageList messageList = null;

            if (elements.get("secuencia").toString().equals("0")) {
            	if(!certificadoReexportacion.equals("S")){
            		messageList = certificadoOrigenLogic.insertCertificadoOrigenMercanciaDjExistente(mercanciaObj, elements.getString("formato"));
            	}else{
            		System.out.println("**************Reexportacion Insert");
            		messageList = certificadoOrigenLogic.insertCOMercancia(mercanciaObj, elements.getString("formato"));
            	}
                mercanciaObj = (CertificadoOrigenMercancia) messageList.getObject();
            } else {
                mercanciaObj.setSecuenciaMercancia(Integer.parseInt(elements.get("secuencia").toString()));
                if(!certificadoReexportacion.equals("S")){
                	messageList = certificadoOrigenLogic.updateCOMercancia(mercanciaObj, elements.getString("formato"));
                }else{
                	//System.out.println("**************Reex Update");
                	messageList = certificadoOrigenLogic.updateCOMercancia(mercanciaObj, elements.getString("formato"));
                }
            }
            
            if (mercanciaObj!=null) {
                mercanciaObj = this.certificadoOrigenLogic.getCOMercanciaById(ordenObj.getIdFormatoEntidad(), mercanciaObj.getSecuenciaMercancia(), elements.getString("formato"));
                cargarCertificadoOrigenMercancia(ordenObj, mercanciaObj, request);
                
                if (!certificadoReexportacion.equals("S")) {
	            	// Actualizamos la calificación sin datos generales
	           		cargarInformacionCalificacionOrigen(ordenObj, elements.getLong("CALIFICACION.calificacionUoId"), request, false);
	           		verificarCalifDJCompleta(mercanciaObj.getCalificacionUoId(), messageList, request, elements.getInt("idAcuerdo"), ordenObj.getOrden());
                }
            }

            request.setAttribute("coId", elements.get("coId"));
            request.setAttribute("formato", elements.get("formato"));
            request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(Integer.valueOf(request.getParameter("idAcuerdo"))) );
            
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
            
            return new ModelAndView(formatoMercancia);
    }

    public ModelAndView eliminarCOMercancia(HttpServletRequest request, HttpServletResponse response) {
        //Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CO_FACTURA.");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        CertificadoOrigenMercancia mercanciaObj = new CertificadoOrigenMercancia();  //this.certificadoOrigenLogic.convertMapToCOMercancia(datos);
        mercanciaObj.setCoId(new Long(elements.get("coId").toString()));
        mercanciaObj.setSecuenciaMercancia(Integer.parseInt(elements.get("secuencia").toString()));
        MessageList messageList = this.certificadoOrigenLogic.deleteCertificadoOrigenMercancia(mercanciaObj, elements.getString("formato"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden").toString()), Integer.parseInt(request.getParameter("mto").toString()));
        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);
        
    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);

        return identificarPantallaFormato(elements.getString("formato"));//new ModelAndView(getFormato());
    }

    /**
     * Registra una Declaración Jurada
     * @param request
     * @param response
     * @return
     */
    public ModelAndView grabarMercanciaDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ.");
            HashUtil<String, String> elements = RequestUtil.getParameter(request);

            Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));

            DeclaracionJurada djObj = certificadoOrigenLogic.convertMapToDeclaracionJurada(datos);            
            String modoFormato = elements.getString("modoFormato");
            
            MessageList messageList = new MessageList();
            
            djObj.setDjId(new Long(elements.get("djId").toString()));
        	messageList = certificadoOrigenLogic.updateMercanciaDeclaracionJurada(djObj);
            
        	request.setAttribute("secuenciaMercancia", elements.get("secuenciaMercancia"));        
        	request.setAttribute("coId", elements.get("coId"));
        	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        	return cargarDeclaracionJuradaForm(request,response);
        }

    public ModelAndView eliminarMercanciaDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
        //Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CO_FACTURA.");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        CertificadoOrigenMercancia mercanciaObj = new CertificadoOrigenMercancia();  //this.certificadoOrigenLogic.convertMapToCOMercancia(datos);
        mercanciaObj.setCoId(new Long(elements.get("coId").toString()));
        mercanciaObj.setSecuenciaMercancia(Integer.parseInt(elements.get("secuencia").toString()));
        MessageList messageList = this.certificadoOrigenLogic.deleteCertificadoOrigenMercancia(mercanciaObj, elements.getString("formato"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden").toString()), Integer.parseInt(request.getParameter("mto").toString()));
        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);
        
    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);

        return new ModelAndView(getFormato());
    }


    /**
     * Actualiza la Direccion Adicional de la Calificación de Origen
     * @param request Objeto request.
     * @param response Objeto response.
     * @return ModelAndView
     */
    public ModelAndView actualizarDireccionAdicionalOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CERTIFICADO_ORIGEN.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        //long idFormatoEntidad = elements.getLong("idFormatoEntidad");
        
        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);
        
        Long coId = ordenObj.getIdFormatoEntidad();
        String direccionAdicional = (( datos.get("direccionAdicional") != null ) ? datos.get("direccionAdicional").toString() : null);
        // Creamos un objeto detalle en base a lo que recibimos de request
        //CalificacionOrigen detalleObj = certificadoOrigenLogic.convertMapToCalificacionOrigen(datos);

        // Asignamos el id de calificación que viene de la página
        //detalleObj.setCalificacionUoId(Long.valueOf((String) datos.get("calificacionUoId")));

        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateDireccionAdicionalCalificacionOrigen(coId,direccionAdicional);
        messageList.setObject(ordenObj);
        //Identificamos a donde redireccionar
        String paginaDestino = elements.getString("paginaDestino");
        cargarInformacionFormato(request, messageList);
        
        ModelAndView destino = new ModelAndView(ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino) ? this.formatoMercancia : getCalificacionAnticipadaCertificadoOrigen());

        return destino;
    }    

    
    /**
     * Actualiza la información del Formato
     *
     * @param request
     * @param response
     * @return
     */
    public ModelAndView actualizaFormatoCertificadoOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CERTIFICADO_ORIGEN.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);
        
        
        // Creamos un objeto detalle en base a lo que recibimos de request
        CertificadoOrigen detalleObj = certificadoOrigenLogic.convertMapToCertificadoOrigen(datos);

        detalleObj.setOrden(orden);
        detalleObj.setMto(mto);
        detalleObj.setCoId(idFormatoEntidad);
        detalleObj.setAcuerdoInternacionalId(ordenObj.getIdAcuerdo());
        
        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateCertificadoOrigen(detalleObj);

        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);

    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);


        return new ModelAndView(getFormato());
    }
    
    /**
     * Actualiza la información del Formato
     *
     * @param request
     * @param response
     * @return
     */
    public ModelAndView consolidarProductores(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CERTIFICADO_ORIGEN.");

        MessageList messageList = new MessageList();

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");
        int tProductoresSize = elements.getInt("tProductoresSize");
        String formato = elements.getString("formato").toString().toLowerCase();
        boolean ok = false;

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Actualizamos el listado de Direcciones de los Productores
        
        try {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("coId", idFormatoEntidad);
    	
    	for (int i=0; i < tProductoresSize; i++) {
    								
	    	filter.put("secuencia", elements.getLong("item"+i));
	    	filter.put("direccion", elements.getString("direccion"+i).toString());
	    	filter.put("telefono", elements.getString("telefono"+i).toString());
	    	filter.put("fax", elements.getString("fax"+i).toString());
	    	filter.put("email", elements.getString("email"+i).toString());
	    	filter.put("nombre", elements.getString("nombre"+i).toString());
	    	filter.put("ciudadProductor", elements.getString("ciudadProductor"+i).toString());
	    	filter.put("paisProductor", elements.getString("paisProductor"+i).toString());
	    	
	    	System.out.println( "********************"+elements.getString("**********************direccion"+i).toString()+"-******************");
	    	
	    	System.out.println( "***********ciudadProductor****"+elements.getString("ciudadProductor"+i).toString());
	    	System.out.println( "***********paisProductor***"+elements.getString("paisProductor"+i).toString());
	    	
	    	/*
	    	System.out.println( "********************"+elements.getString("email"+i).toString());
	    	*/
	    	ibatisService.executeSPWithObject("certificadoOrigen."+formato+".consolidaProductor.modifica", filter);
	    	ok = true;
	    	
    	}
        }catch(Exception e) {
        	logger.error("Error en CertificadoOrigenController.consolidarProductores", e);
        }
        if(ok) {
        	messageList.add(new Message("co.consolidacion.productores.success"));
        }
        messageList.setObject(ordenObj);
        
        cargarInformacionFormato(request, messageList);

    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);

    	System.out.println("******Fin NPCS*******");

        return new ModelAndView(getFormato());
    }

    /**
     * Carga la información de Mercancía del formulario
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarCertificadoOrigenMercanciaForm(HttpServletRequest request, HttpServletResponse response) {
    		
        MessageList messageList = (request.getAttribute(Constantes.MESSAGE_LIST) != null ? (MessageList) request.getAttribute(Constantes.MESSAGE_LIST) : new MessageList());
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        Orden ordenObj = ordenLogic.getOrdenById(Util.longValueOf(request.getParameter("orden")), Util.integerValueOf(request.getParameter("mto")));
        
        int secuenciaMercancia = Util.integerValueOf(request.getAttribute("secuencia")!=null? (request.getAttribute("secuencia").toString().trim()) :
        									         (request.getAttribute("secuenciaMercancia")!=null? (request.getAttribute("secuenciaMercancia").toString().trim()) : request.getParameter("secuencia")));
        
        Integer idAcuerdo = Util.integerValueOf(request.getParameter("idAcuerdo"));
        
        //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
        String estadoAcuerdoPais = validaAcuerdoPais(Long.parseLong(request.getParameter("orden")));
        if ("I".equals(estadoAcuerdoPais)) {
          	request.setAttribute("estadoAcuerdoPais","I");
        }
        //
        
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        
        
        if(usuario.getIdUsuario().equals(ordenObj.getIdUsuario())){
        	request.setAttribute("isOwner","S");
        }else{
        	request.setAttribute("isOwner","N");
        }
        
        Long calificacionUoId = null;
        
        if ( secuenciaMercancia != 0 ) { // Si ya existe la Mercancia
            CertificadoOrigenMercancia mercanciaObj = certificadoOrigenLogic.getCertificadoOrigenMercanciaById(ordenObj.getIdFormatoEntidad(), secuenciaMercancia, request.getParameter("formato"));
            cargarCertificadoOrigenMercancia(ordenObj, mercanciaObj, request);
            CalificacionOrigen calificacionOrigenObj = cargarInformacionCalificacionOrigen(ordenObj, mercanciaObj.getCalificacionUoId(), request, false);
            calificacionUoId = mercanciaObj.getCalificacionUoId();
            
            // Si la Mercancia NO esta utilizando una Calificacion origen ya existente, entonces debo validar la Calificacion origen
        	if (mercanciaObj.getDescripcion() == null) {
        		messageList.add(new Message("co.falta.registro.mercancia.individual"));
        	}
            
        	// Si no es Reexportacion
        	if (ordenObj.getCertificadoReexportacion().equals("N") && !ConstantesCO.CALIFICACION_ORIGEN_EXISTENTE.equals(mercanciaObj.getSustentoCalifMercancia())) {
        		verificarCalifDJCompleta(mercanciaObj.getCalificacionUoId(), messageList, request, idAcuerdo, ordenObj.getOrden());
            }
        	
        	// 20140523_JFR: Se agrega por PQT-05
        	if (mercanciaObj.getPartidaSegunAcuerdo()==null) {
	        	try {
	            	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	            	filter.put("djId", calificacionOrigenObj.getDjId());
	            	filter.put("acuerdoId", calificacionOrigenObj.getAcuerdoInternacionalId());
	        	    request.setAttribute("CO_MERCANCIA.partidaSegunAcuerdo", (String)ibatisService.loadObject("certificadoOrigen.datosMercanciaCertificado.partidaSegunAcuerdo", filter));
	        	} catch (Exception e) {
	        		logger.debug(e);
	        	}
        	}
        } else {
        	request.setAttribute("secuencia", Util.integerValueOf(request.getParameter("secuencia")));
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("coId", ordenObj.getIdFormatoEntidad());
            request.setAttribute("idAcuerdo", idAcuerdo);
            request.setAttribute("idPais", request.getParameter("idPais"));
            request.setAttribute("idEntidadCertificadora", request.getParameter("idEntidadCertificadora"));
            request.setAttribute("estadoDJ", request.getParameter("estadoDJ"));
            request.setAttribute("esDJRegistrada", request.getParameter("esDJRegistrada"));
            
            //Filtro para la Reexportacion
            HashUtil filterReexportacion = new HashUtil();
            filterReexportacion.put("acuerdoId", idAcuerdo);
            filterReexportacion.put("secuenciaNorma", null);
            filterReexportacion.put("secuenciaOrigen", null);
            request.setAttribute("filterReexportacion", filterReexportacion);
            
            //Filtro para las facturas
            HashUtil filterFacturas = new HashUtil();
            filterFacturas.put("coId", ordenObj.getIdFormatoEntidad());
            request.setAttribute("filterFacturas", filterFacturas);
            
            // Si no es Reexportacion
            if (!"S".equals(ordenObj.getCertificadoReexportacion())) {
	            calificacionUoId = datos.get("calificacionUoId")!=null?datos.getLong("calificacionUoId"):datos.getLong("CALIFICACION.calificacionUoId");
	            
	            // Si no se logro grabar, entonces colocar NULL a calificacionUoId
	            //NC:24/06/2016 CO-173
	            //if (messageList.containsError()) calificacionUoId = null;
	            
	            // Para el caso que exista una calificación, debemos cargar la información de calificación
	            if (calificacionUoId!=null && calificacionUoId != 0) {
	            	// Sólo para este caso seteamos la variable
	                request.setAttribute("esCalificacionOrigenStandAlone", "S");
	                cargarInformacionCalificacionOrigen(ordenObj, calificacionUoId, request, false);
	            } else {
	                // En caso no existe mercancía no importa lo que setee, porque nunca se podrá grabar
	                cargarInformacionCalificacionOrigen(ordenObj, null, request, false);
	            }
            }
        }
        
        // Si no es Reexportacion
        if (!"S".equals(ordenObj.getCertificadoReexportacion())) {
	        // Se verifica que el usuario logeado sea el usuario productor, con el fin de habilitarle la autorizacion a otros exportadores // 20140529_JMC : BUG - Error Autorización productor
	        request.setAttribute("modoProductorValidador", "N");
	        
	        if (calificacionUoId!=null && calificacionUoId != 0) {
	            // Verifica si el usuario logeado es el propietario de los datos de la Calificacion        
	            request.setAttribute("esPropietarioDatos", certificadoOrigenLogic.esPropietarioDatos(calificacionUoId, usuario.getNumeroDocumento()));
	            
		        CalificacionOrigen calificacion = certificadoOrigenLogic.getCalificacionOrigenById(calificacionUoId); 
		        String numeroDocPropietarioDatos = calificacion.getNumDocPropietarioDatos()!=null?calificacion.getNumDocPropietarioDatos():"0";
		        if (usuario.getNumeroDocumento().toString().equals(numeroDocPropietarioDatos)) {
		        	request.setAttribute("modoProductorValidador", "S");
		        }
	        }
        }
        
        if ( ConstantesCO.AC_SGPC.equals(idAcuerdo) || ConstantesCO.AC_SGP_UE.equals(idAcuerdo) || 
        	 ConstantesCO.AC_COREA.equals(idAcuerdo) || ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo) ||
        	 ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)){
        	request.setAttribute("lblSufijo", "."+idAcuerdo.toString());
        } else {
        	request.setAttribute("lblSufijo", "");
        }

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("formato", request.getParameter("formato"));
        request.setAttribute("bloqueado", request.getParameter("bloqueado"));
        request.setAttribute("CO_MERCANCIA.coId", ordenObj.getIdFormatoEntidad());
        /*if (request.getAttribute("djEnProcesoValidacion") != null && ConstantesCO.OPCION_SI.equalsIgnoreCase(request.getAttribute("djEnProcesoValidacion").toString())) {
        	request.setAttribute("bloqueado", ConstantesCO.OPCION_SI);
        } else {
        	request.setAttribute("bloqueado", request.getParameter("bloqueado"));
        }*/
        request.setAttribute("puedeTransmitir", request.getParameter("puedeTransmitir"));
        request.setAttribute("acuerdo", request.getParameter("acuerdo"));
        request.setAttribute("entidad", request.getParameter("entidad"));
        request.setAttribute("nombrePais", request.getParameter("nombrePais"));
        // Colocamos la lista de si y no para los combos
        request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());
        request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(idAcuerdo) );
        request.setAttribute("certificadoReexportacion", ordenObj.getCertificadoReexportacion());
        // 20170118_GBT ACTA CO-008-16. se agrego la variable muestraDJ
        request.setAttribute("muestraDJ", request.getParameter("muestraDJ"));
        return new ModelAndView(formatoMercancia);
    }

    public void cargarCertificadoOrigenMercancia(Orden ordenObj, CertificadoOrigenMercancia mercanciaObj, HttpServletRequest request){
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        Integer idAcuerdo = Util.integerValueOf(datos.getString("idAcuerdo"));
        
        //Filtro para la Reexportacion
        HashUtil<String, Object> filterReexportacion = new HashUtil<String, Object>();
        filterReexportacion.put("acuerdoId", idAcuerdo);
        filterReexportacion.put("secuenciaNorma", mercanciaObj.getSecuenciaNorma());
        filterReexportacion.put("secuenciaOrigen", mercanciaObj.getSecuenciaOrigen());
        request.setAttribute("filterReexportacion", filterReexportacion);
        
        RequestUtil.setAttributes(request, "CO_MERCANCIA.", mercanciaObj);
        
        DeclaracionJurada dj = null;
        if (mercanciaObj != null) {
            if (mercanciaObj.getDjId() != null) {
                dj = this.certificadoOrigenLogic.getCODeclaracionJuradaById(mercanciaObj.getDjId());

                if (dj != null) {
                    RequestUtil.setAttributes(request, "DJ.", dj);
                    request.setAttribute("dj", dj.getDj());
                }
            }

            request.setAttribute("secuencia", mercanciaObj.getSecuenciaMercancia());

        } else {
        	 request.setAttribute("secuencia", 0);
        }

        //Filtro para las facturas
        HashUtil filterFacturas = new HashUtil();
        filterFacturas.put("coId", ordenObj.getIdFormatoEntidad());
        request.setAttribute("filterFacturas", filterFacturas);

        request.setAttribute("orden", ordenObj.getOrden());
        request.setAttribute("mto", ordenObj.getMto());
        request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
        request.setAttribute("coId", ordenObj.getIdFormatoEntidad());
        request.setAttribute("idAcuerdo", datos.get("idAcuerdo"));
        request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());
        request.setAttribute("estadoDJ", datos.getString("estadoDJ"));
        request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(Integer.valueOf(datos.get("idAcuerdo").toString())) );
        request.setAttribute("certificadoReexportacion", request.getParameter("certificadoReexportacion"));
        if ( ConstantesCO.AC_SGPC.equals(ordenObj.getIdAcuerdo())
            	|| ConstantesCO.AC_SGP_UE.equals(ordenObj.getIdAcuerdo())
            	|| ConstantesCO.AC_COREA.equals(ordenObj.getIdAcuerdo())
            	|| ConstantesCO.AC_SGP_TURQUIA.equals(ordenObj.getIdAcuerdo())) {
           	request.setAttribute("lblSufijo", "."+ordenObj.getIdAcuerdo());
        } else {
        	request.setAttribute("lblSufijo", "");
        }

        // Colocamos la lista de si y no para los combos
        request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());

    }

    public ModelAndView cargarCertificadoOrigenBuscDj(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = new MessageList();
        Message message = new Message("co.mercancias.busqueda.dj.info");
        messageList.add(message);
        request.setAttribute("entidad",request.getParameter("entidad"));
        request.setAttribute("entidadCertificadora",request.getParameter("entidad"));
        request.setAttribute("acuerdo",request.getParameter("acuerdo"));
        request.setAttribute("orden",request.getParameter("orden"));
        request.setAttribute("mto",request.getParameter("mto"));
        request.setAttribute("formato",request.getParameter("formato"));
        request.setAttribute("borrador",request.getParameter("borrador")!=null?request.getParameter("borrador"):"");
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        return new ModelAndView(formatoBusqDj);
    }

    public ModelAndView obtenerDJs(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Table tDJs = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        if (datos.containsKey("dj") && datos.get("dj") != null) {
            filter.put("dj", datos.getString("dj").toUpperCase());
        }

        if (datos.containsKey("denominacion") && datos.get("denominacion") != null) {
            filter.put("denominacion", datos.getString("denominacion").toLowerCase());
        }

        if (datos.containsKey("partida") && datos.get("partida") != null) {
            filter.put("partida", datos.getString("partida"));
        }

        if (datos.containsKey("fechaDesde") && datos.get("fechaDesde") != null) {
            filter.put("fechaDesde", datos.getString("fechaDesde"));
        }

        if (datos.containsKey("fechaHasta") && datos.get("fechaHasta") != null) {
            filter.put("fechaHasta", datos.getString("fechaHasta"));
        }
        
        if (datos.containsKey("entidadCertificadora") && datos.get("entidadCertificadora") != null) {
            filter.put("entidadCertificadora", datos.getString("entidadCertificadora"));
        }

        if (datos.containsKey("borrador") && datos.get("borrador") != null) {
            filter.put("borrador", datos.getString("borrador"));
        }

        filter.put("entidad", datos.getString("entidad"));
        if (!datos.containsKey("borrador") || !datos.getString("borrador").equals("1")) {
            filter.put("acuerdo", datos.getString("acuerdo"));
        }
        filter.put("orden", datos.getString("orden"));
        filter.put("mto", datos.getString("mto"));

        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        filter.put("usuTDoc", usuario.getTipoDocumento());
        filter.put("usuNumDoc", usuario.getNumeroDocumento());

        tDJs = ibatisService.loadGrid("certificado_origen.declaracion_jurada.grilla", filter);

        request.setAttribute("tDJs", tDJs);
        request.setAttribute("entidad", datos.getString("entidad"));
        request.setAttribute("entidadCertificadora", datos.getString("entidadCertificadora"));
        request.setAttribute("acuerdo", datos.getString("acuerdo"));
        request.setAttribute("orden",datos.getString("orden"));
        request.setAttribute("mto",datos.getString("mto"));
        request.setAttribute("denominacion",datos.get("denominacion"));
        request.setAttribute("dj",datos.get("dj"));
        request.setAttribute("partida",datos.get("partida"));
        request.setAttribute("borrador",datos.get("borrador"));
        request.setAttribute("formato",datos.get("formato"));
        MessageList messageList = new MessageList();
        Message message = new Message("co.mercancias.busqueda.dj.info");
        messageList.add(message);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        return new ModelAndView(formatoBusqDj);
    }

    /*public ModelAndView editarDeclaracionJuradaPendienteValidacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

    }*/

    /**
     * Carga la información de Declaración Jurada del formulario
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarDeclaracionJuradaForm(HttpServletRequest request, HttpServletResponse response) {

        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        Orden ordenObj = null;
        String strOrden = request.getParameter("orden");//NPCS 03/01/2018
        String strMto = request.getParameter("mto");
        Integer codigoAcuerdo =  datos.getInt("idAcuerdo");

        if (strOrden != null && !"".equals(strOrden)) { // Basta comparar la orden pues si esta viene llena, ambas vienen llenas
            ordenObj = ordenLogic.getOrdenById(new Long(strOrden), new Integer(strMto));
            
            

            /*HashUtil<String, Object> filterNotificacion = new HashUtil<String, Object>();
            filterNotificacion.put("suceId", ordenObj.getSuce());

            Table tNotificacionesSuce = ibatisService.loadGrid("suce.resolutor.notificaciones.grilla", filterNotificacion);
            if (tNotificacionesSuce != null) {
            	request.setAttribute("numNotif", tNotificacionesSuce.size());
            }*/
        }

        
        long djId = new Long(request.getParameter("djId"));
        Object datDj = null;
        String calificacion = request.getParameter("calificacion") != null ? request.getParameter("calificacion") : "";

        Long id = (Long) request.getAttribute("djId"); // Este valor solo llegará lleno cuando regrese de una grabación
        if (id != null) {
        	djId = id.longValue();
        }

        int tipoRol = 0;

        request.setAttribute("formato", request.getAttribute("formato") != null ? request.getAttribute("formato") : datos.get("formato"));
        //T:70272 NPCS 21/02/2018
        //Integer acuerdoId = new Integer("0");

        if (djId != 0 ) { // Si ya existe la Declaración Jurada

        	// Obtenemos el detalle de la DJ de Base de Datos
            DeclaracionJurada djObj = certificadoOrigenLogic.getCODeclaracionJuradaById(djId);
            
            try {
				if(djObj.getCantidadUmRefer()!=null && !djObj.getCantidadUmRefer().isEmpty()){
					HashUtil<String, Object> filter = new HashUtil<String, Object>();
		    		filter.put("item", djObj.getUmFisicaId());
		    		String unidadMedida = "";
					unidadMedida = (String)ibatisService.loadObject("comun.um_fisica.selectItem", filter);
	            	String nota = "Nota: Para elaborar "+djObj.getCantidad()+" "+unidadMedida+ " de " +djObj.getDenominacion() +" en presentación de " +djObj.getCantidadUmRefer()+ ", con costo de US$ "+djObj.getValorUs()+" se utilizan los siguientes materiales: ";
	                request.setAttribute("nota", ComponenteOrigenUtil.removerCaracterProhibidoUrl(nota));	
	            }				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            //T:70272 NPCS 21/02/2018
            // acuerdoId = djObj.getAcuerdoInternacionalId();
            cargarMercanciaDeclaracionJurada(ordenObj, djObj, request);
            tipoRol = djObj.getTipoRolDj() == null ? 0 : djObj.getTipoRolDj();
            request.setAttribute("esDJRegistrada", request.getParameter("esDJRegistrada"));
            
            //calificacion = djObj.getCalificacion();

        }/* else { // Si es una Declaración Jurada nueva

        	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/
            // Después del cambio realizado, nunca debería entrar a este else
        	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/
            /*HashUtil<String, Object> cmbFilter = new HashUtil<String, Object>();
            cmbFilter.put("acuerdoId", datos.get("idAcuerdo"));
            acuerdoId = new Integer(datos.get("idAcuerdo").toString());

            //request.setAttribute("secuenciaMercancia", datos.getInt("secuenciaMercancia"));
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("djId", djId);
            request.setAttribute("cmbFilter", cmbFilter);

        }*/

        if (datos.get("secuenciaMercancia") != null && request.getAttribute("secuenciaMercancia") == null){
        	request.setAttribute("secuenciaMercancia", datos.get("secuenciaMercancia"));
        }
        
        Integer idAcuerdo = request.getAttribute("idAcuerdo") != null ? new Integer(request.getAttribute("idAcuerdo").toString()) : new Integer(request.getParameter("idAcuerdo")!=null?request.getParameter("idAcuerdo"):datos.get("idAcuerdo").toString());
        
        // El formato es importante para poder regresar al listado de certificados
        request.setAttribute("idAcuerdo", idAcuerdo);
        request.setAttribute("modoFormato", request.getAttribute("modoFormato") != null ? request.getAttribute("modoFormato") : datos.get("modoFormato"));
        request.setAttribute("modoLectura", request.getAttribute("modoLectura") != null ? request.getAttribute("modoLectura") : datos.get("modoLectura"));
        request.setAttribute("modoValidadorLectura", request.getAttribute("modoValidadorLectura") != null ? request.getAttribute("modoValidadorLectura") : (request.getParameter("modoValidadorLectura") != null ? request.getParameter("modoValidadorLectura") : datos.get("modoValidadorLectura")));
        request.setAttribute("permiteApoderamientoXFrmt", formatoLogic.permiteApoderamientoXFrmt(new Integer( request.getParameter("idAcuerdo"))));
        request.setAttribute("utilizaModificacionSuceXMto", (ordenObj!=null ? ordenObj.getModificacionSuceXMto():"N"));
        request.setAttribute("ordenVigente", (ordenObj!=null?ordenObj.getVigente():"N"));
        request.setAttribute("calificacion", calificacion);
        request.setAttribute("puedeTransmitir", datos.getString("puedeTransmitir"));
        //request.setAttribute("paisIsoIdPorAcuerdoInt", ( request.getParameter("paisIsoIdPorAcuerdoInt") != null && !"".equals(request.getParameter("paisIsoIdPorAcuerdoInt")) ) ? request.getParameter("paisIsoIdPorAcuerdoInt") : request.getParameter("DJ.paisIsoIdPorAcuerdoInt") );
        
        String idPaisIso = (request.getParameter("paisIsoIdPorAcuerdoInt") != null && !"".equals(request.getParameter("paisIsoIdPorAcuerdoInt")) ) ? request.getParameter("paisIsoIdPorAcuerdoInt") : request.getParameter("DJ.paisIsoIdPorAcuerdoInt"); 
        request.setAttribute("paisIsoIdPorAcuerdoInt", idPaisIso!=null?idPaisIso:datos.getString("idPaisSegundoComponente"));
        request.setAttribute("secuenciaOrigen", ( request.getParameter("secuenciaOrigen") != null && !"".equals(request.getParameter("secuenciaOrigen")) ) ? request.getParameter("secuenciaOrigen") : request.getParameter("DJ.secuenciaOrigen") );
        request.setAttribute("tipoRolCalificacion",  request.getParameter("tipoRolCalificacion"));
        request.setAttribute("esPropietarioDatos",  request.getParameter("esPropietarioDatos"));
        request.setAttribute("acuerdo", request.getParameter("acuerdo"));
        request.setAttribute("entidad", request.getParameter("entidad"));
        request.setAttribute("nombrePais", request.getParameter("nombrePais"));
        

        //20170203_GBT ACTA CO-004-16 Y ACTA CO 009-16
       String estadoAcuerdoPais = validaAcuerdoPais(ordenObj.getOrden());
        
        System.out.println(estadoAcuerdoPais);
        if ("I".equals(estadoAcuerdoPais)) {
          	request.setAttribute("estadoAcuerdoPais","I");
        }
        //

    	HashUtil filter = new HashUtil<String, Object>();
    	Long isd = null;
    	if(request.getAttribute("orden")!=null){
    		isd = new Long(request.getAttribute("orden").toString());
    	}
    	
    	filter.put("ordenId", (isd!=null?isd:datos.getLong("orden")) );//datos.getLong("orden"));

    	HashUtil element = ibatisService.loadElement("certificadoOrigen.certifPrincipal.mto.vigente", filter);

    	String mtoOriginal = strMto;
    	if (element != null && element.get("MTO") != null) {
    		mtoOriginal = element.get("MTO").toString().trim();
    	}

    	//String acuerdoConValidacionProductor = formatoLogic.acuerdoConValidacionProductor(acuerdoId);
    	//request.setAttribute("reqValidacion", acuerdoConValidacionProductor);

    	if ("S".equals(datos.getString("bloqueado")) && strMto.equals(mtoOriginal)){
        	request.setAttribute("bloqueado", request.getAttribute("bloqueado") != null ? request.getAttribute("bloqueado") : datos.get("bloqueado"));
        } else {

        	if (mtoOriginal != null && !strMto.equals(mtoOriginal) && tipoRol == Integer.parseInt(ConstantesCO.TIPO_ROL_EXPORTADOR) &&
        		/*ConstantesCO.OPCION_SI.equals(acuerdoConValidacionProductor) &&*/ ( request.getAttribute("esModificacion")==null || !ConstantesCO.OPCION_SI.equals(request.getAttribute("esModificacion")))) {
        		request.setAttribute("bloqueado", ConstantesCO.OPCION_SI);
        	}
        }

    	request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(idAcuerdo));	
    	
    	
    	//Excepcion para el acuerdo Alianza Pacifico 
    	String versionAcuerdo = "";    	
    	if(ConstantesCO.AC_ALIANZA_PACIFICO != codigoAcuerdo){
    		versionAcuerdo = this.getFormatoVersionSufijo(ordenObj);
    		request.setAttribute("idAcuerdoVersion", datos.getString("idAcuerdo")+versionAcuerdo);
        	request.setAttribute("versionFormato", ordenObj.getVersionFormato());// ordenObj.getVersionFormato()); NPCS 03/01/2018
        	request.setAttribute("versionJs", versionAcuerdo);
    	}
    	
        return new ModelAndView(formatoMercanciaDeclaracionJurada+versionAcuerdo);
        //return new ModelAndView(formatoMercanciaDeclaracionJurada);
    }

    /**
     * Carga información de la Declaración Jurada cuando existe.
     * @param ordenObj El objeto orden.
     * @param djObj El objeto Declaración Jurada.
     * @param request El objeto request.
     */
    public void cargarMercanciaDeclaracionJurada(Orden ordenObj, DeclaracionJurada djObj, HttpServletRequest request){

        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        String permiteValidar = ConstantesCO.OPCION_NO;
        MessageList messageList = null;

    	if ( request.getAttribute(Constantes.MESSAGE_LIST) == null ) {
    		messageList = new MessageList();
    	} else {
    		messageList = (MessageList) request.getAttribute(Constantes.MESSAGE_LIST);
    	}

    	String puedeTransmitirDJValidada = ConstantesCO.OPCION_SI;
        /*if ("1".equals(djObj.getTipoRolDj().toString())){
        	//obtener productor
        	List<DeclaracionJuradaProductor> lstProd = this.certificadoOrigenLogic.getDJProductorByDJId(djObj.getDjId());
        	if (lstProd != null && lstProd.size() > 0) {
        		RequestUtil.setAttributes(request, "USUARIO_DJ.", lstProd.get(0));
        	}
        } else {*/

        /*HashUtil<String, Object> filterDetalles = new HashUtil<String, Object>();
        filterDetalles.put("djId", djObj.getDjId());
        Table tExportadores = ibatisService.loadGrid("certificadoOrigen.dj.productor.grilla", filterDetalles);
        request.setAttribute("tProductores", tExportadores);*/

        //if (ConstantesCO.TIPO_ROL_EXPORTADOR.equals(djObj.getTipoRolDj().toString()) && tExportadores.size()>0){
        	//request.setAttribute("esValidadorDJ", esValidadorDJ(usuario.getTipoDocumento(), usuario.getNumeroDocumento(), tExportadores));
        //}

    	// Verificamos si el acuerdo tiene validación del productor para realizar una validación adicional, caso contrario no se realiza
    	/*String acuerdoConValidacionProductor = formatoLogic.acuerdoConValidacionProductor(djObj.getAcuerdoInternacionalId());
    	if (ConstantesCO.TIPO_ROL_EXPORTADOR.toString().equals(djObj.getTipoRolDj().toString()) && ConstantesCO.OPCION_SI.equals(acuerdoConValidacionProductor)) {

            // Hallamos si ya se ingreso un productor validador
            for(int i = 0; i< tExportadores.size(); i++ ){
                Row row = tExportadores.getRow(i);
                String validar = row.getCell("VALIDAR").getValue().toString();
                if(validar.equalsIgnoreCase("SI")) {
                	permiteValidar = ConstantesCO.OPCION_SI;
                	break;
                }
            }
            // Si aun no ingresamos al validador
            if ("N".equals(permiteValidar) && ConstantesCO.TIPO_ROL_EXPORTADOR.equals(""+djObj.getTipoRolDj()) &&
            	!ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(request.getAttribute("formato")!=null? request.getAttribute("formato").toString():(request.getParameter("formato")!=null?request.getParameter("formato"):""))) {
            	messageList.add(new Message("co.falta.dj.validador"));
            	puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
            }
    	}

    	//request.setAttribute("mostrarBotonProductor", formatoLogic.permiteCrearProductor(djObj.getDjId()));
    	*/

    	//}
        RequestUtil.setAttributes(request, "DJ.", djObj);
        request.setAttribute("permiteValidar", permiteValidar);
        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("djId", djObj.getDjId());
        filterADJ.put("tipoRolDj", djObj.getTipoRolDj());
        request.setAttribute("filterADJ", filterADJ);

        HashUtil<String, Object> cmbFilter = new HashUtil<String, Object>();
        cmbFilter.put("acuerdoId", request.getParameter("idAcuerdo"));

        request.setAttribute("numDj", djObj.getDj());
        request.setAttribute("secuenciaOrigen", djObj.getSecuenciaOrigen());
        //request.setAttribute("idAcuerdo", datos.get("idAcuerdo"));
        request.setAttribute("djId", djObj.getDjId());
        request.setAttribute("umFisicaId", djObj.getUmFisicaId());
        request.setAttribute("cmbFilter", cmbFilter);
        request.setAttribute("dj.dj", djObj.getDj());
        request.setAttribute("dj", djObj.getDj());

        //request.setAttribute("djId", djId);
        request.setAttribute("esTotalMenteObtenida", djObj.getCumpleTotalmenteObtenido());
        request.setAttribute("tipoRol", djObj.getTipoRolDj());
        request.setAttribute("estadoDj", djObj.getEstadoRegistro());
        //request.setAttribute("numeroProductores", tExportadores.size());

    	CalificacionOrigen calificacionOrigen = this.certificadoOrigenLogic.getCalificacionOrigenById(djObj.getCalificacionUoId());
        request.setAttribute("tipoRol", calificacionOrigen.getTipoRolCalificacion());
        request.setAttribute("esModificacion", djObj.getCopiaXModificacion());
        request.setAttribute("esDJRegistrada", request.getParameter("esDJRegistrada") != null ? request.getParameter("esDJRegistrada") : "");
        request.setAttribute("criterioArancelario", datos.getInt("criterioArancelario"));

		HashMap<String, Object> filterVal = new HashMap<String, Object>();
		filterVal.put("ordenId", calificacionOrigen.getOrdenIdInicial());
		filterVal.put("tipoRolCalificacion", calificacionOrigen.getTipoRolCalificacion());
		request.setAttribute("reqValidacion", certificadoOrigenLogic.reqValidacionProductor(filterVal));

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("idAcuerdo", request.getParameter("idAcuerdo"));
        String reqValidacionProd = "N";
        try {
        	reqValidacionProd = (String) ibatisService.loadObject("tipo.validacion.productor.acuerdo", filter);
		} catch (Exception e) {
			// TODO: handle exception
		}

        request.setAttribute("reqValidacionProd", reqValidacionProd);

        if (ordenObj != null) {

            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());
            request.setAttribute("estadoRegistroCO", ordenObj.getEstadoRegistro());
            request.setAttribute("ordTransmitida", ordenObj.getTransmitido());
            if (ordenObj.getSuce() != null) {
            	request.setAttribute("suceFlgPendienteCalif", this.suceLogic.suceFlgPendienteCalif(ordenObj.getSuce()));
            }
        } else {
            request.setAttribute("orden", "0");
            request.setAttribute("mto", "0");
            request.setAttribute("idFormatoEntidad", "0");

        }

        request.setAttribute("modoProductorValidador", datos.get("modoProductorValidador"));
        if (ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(djObj.getEstadoRegistro())) {
        	if (usuario.getIdUsuario().intValue() == djObj.getUsuarioIdProductorValidador().intValue()) { // Si son iguales se puede editar
                request.setAttribute("bloqueado", ConstantesCO.OPCION_NO);
                request.setAttribute("esValidador", ConstantesCO.OPCION_SI);
        	} else {
                request.setAttribute("bloqueado", ConstantesCO.OPCION_SI);
                request.setAttribute("esValidador", ConstantesCO.OPCION_NO);
        	}
        } else if (ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(djObj.getEstadoRegistro()) ||
            	ConstantesCO.DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO.equals(djObj.getEstadoRegistro())) {
        	request.setAttribute("bloqueado", "S");
        } else if (ConstantesCO.DJ_ESTADO_APROBADA.equals(djObj.getEstadoRegistro())) {
        	// 20150707: Acta 35: Asignados como Productor: puede visualizar informacion de la DJ aprobada
        	if (Integer.valueOf(ConstantesCO.TIPO_ROL_EXPORTADOR). equals(calificacionOrigen.getTipoRolCalificacion()) && djObj.getUsuarioIdProductorValidador() != null && usuario.getIdUsuario().intValue() == djObj.getUsuarioIdProductorValidador().intValue()) {
        		// Se reusa variable modoValidadorLectura: Similar a estado DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO 
        		request.setAttribute("modoValidadorLectura", ConstantesCO.OPCION_SI);
        	}
        }

        // Si la DJ ya ha sido aprobada y tiene un código generado, ya no se puede modificar
        if  (djObj.getDj() != null || (datos.getString("modoLectura") != null && "S".equals(datos.getString("modoLectura")))) {
        	request.setAttribute("bloqueado", ConstantesCO.OPCION_SI);
        }

/*        public static final String DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION = "N";
        public static final String DJ_ESTADO_PENDIENTE_VALIDACION = "O";
        public static final String DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO = "Q"; */

        // Si la DJ esta en estado "Pendiente de Aceptación Validación", no se puede modificar
        // Tampoco se puede modificar cuando se encuentra en el estado "Validado y pendiente de envio a Entidad"
        /*if (ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(djObj.getEstadoRegistro()) ||
        	ConstantesCO.DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO.equals(djObj.getEstadoRegistro()) ||
        	ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(djObj.getEstadoRegistro())) {
        	request.setAttribute("bloqueado", "S");
        }*/

        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        	Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo()) && 
            !ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(datos.getString("formato")) ) {

            request.setAttribute("mostrarBotonConfirmarFinEval", formatoLogic.permiteConfirmarFinEval(ordenObj.getIdFormatoEntidad(), ordenObj.getOrden(), ordenObj.getMto()));

        }

        // Materiales No Originarios
        HashUtil<String, Object> filterMateriales = new HashUtil<String, Object>();
        filterMateriales.put("djId", djObj.getDjId());
        // Materiales No Originarios
        Table tMaterialesNoOriginarios = ibatisService.loadGrid("certificadoOrigen.orden.dj.material.no_origen.grilla", filterMateriales);
        request.setAttribute("tMaterialesNoOriginarios", tMaterialesNoOriginarios);

        // Materiales Originarios de Perú
        filterMateriales.put("idPais", ConstantesCO.COD_PAIS_PERU);
        Table tMaterialesPeru = ibatisService.loadGrid("certificadoOrigen.orden.dj.material.origen_pais.grilla", filterMateriales);
        request.setAttribute("tMaterialesPeru", tMaterialesPeru);
        request.setAttribute("matPeruEmpty", ((tMaterialesPeru == null || tMaterialesPeru.size() == 0)?"1":"0"));

        // Exportadores Autorizados
        HashUtil<String, Object> filterAutoriz = new HashUtil<String, Object>();
        filterAutoriz.put("djId", djObj.getDjId());  //NPCS 03/01/2018
        filterAutoriz.put("calificacionUoId", djObj.getCalificacionUoId());  //NPCS 03/01/2018
        //filterAutoriz.put("tipDocumento", ConstantesCO.TIPO_DOCUMENTO_RUC);
        //filterAutoriz.put("numDocumento", usuario.getNumRUC());
        Table tAutorizaciones = ibatisService.loadGrid("calificacionOrigen.exportador.autorizado.grilla", filterAutoriz);
        request.setAttribute("tAutorizaciones", tAutorizaciones);

        // Materiales Originarios del Segundo Componente
        filterMateriales.remove("idPais");
        Integer idAcuerdo = new Integer(request.getParameter("idAcuerdo")); //djObj.getAcuerdoInternacionalId();
        filterMateriales.put("idAcuerdo", idAcuerdo);
    	if (ConstantesCO.AC_TLC_UE.equals(idAcuerdo)){
    		filterMateriales.put("paisesBeneficiarios", "1");
        	filterMateriales.put("ind2doComp", ConstantesCO.OPCION_SI);
    	} else {
    		filterMateriales.put("paisesBeneficiarios", "0");
    	}
        Table tMaterialesSegundoComponente = ibatisService.loadGrid("certificadoOrigen.orden.dj.material.origen_pais.grilla", filterMateriales);
        request.setAttribute("tMaterialesSegundoComponente", tMaterialesSegundoComponente);
        request.setAttribute("mat2doCompEmpty", ((tMaterialesSegundoComponente == null || tMaterialesSegundoComponente.size() == 0)?"1":"0"));

        Table tMaterialesTercerComponente = null;
        // El Tercer Componente solo existe en algunos acuerdos
        if (ConstantesCO.AC_MERCOSUR_INT.equals(idAcuerdo) || ConstantesCO.AC_PANAMA.equals(idAcuerdo) || ConstantesCO.AC_GUATEMALA.equals(idAcuerdo) ||ConstantesCO.AC_HONDURAS.equals(idAcuerdo) || ConstantesCO.AC_COSTARRICA.equals(idAcuerdo) || ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo) || ConstantesCO.AC_TLC_UE.equals(idAcuerdo) || ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)|| ConstantesCO.AC_SGP_UE.equals(idAcuerdo) ) {

        	filterMateriales.remove("idAcuerdo");
            if (ConstantesCO.AC_MERCOSUR_INT.equals(idAcuerdo)) {
	            filterMateriales.put("idAcuerdo",ConstantesCO.AC_CAN);
	        } else if (ConstantesCO.AC_PANAMA.equals(idAcuerdo)){
	        	filterMateriales.put("idOtros",ConstantesCO.AC_PANAMA_OTROS);
	        } else if (ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)){
	        	filterMateriales.put("idOtros",ConstantesCO.AC_GUATEMALA_OTROS);
	        } else if (ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)) {
	        	filterMateriales.put("idAcuerdoBeneficiario",ConstantesCO.AC_SGP_NORUEGA);
	        	filterMateriales.put("idPaisExcluido",ConstantesCO.COD_PAIS_NORUEGA);
	        } else if (ConstantesCO.AC_TLC_UE.equals(idAcuerdo)) {
	        	filterMateriales.put("idAcuerdo", ConstantesCO.AC_TLC_UE);
	        	filterMateriales.remove("ind2doComp");
	        	filterMateriales.put("ind3erComp", ConstantesCO.OPCION_SI);
	        } else if ( ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)){
	        	filterMateriales.put("idAcuerdoBeneficiario",ConstantesCO.AC_SGP_CANADA);
	        	List<String> lstPaisesExcluidos = new ArrayList<String>();
	        	lstPaisesExcluidos.add(ConstantesCO.COD_PAIS_PERU);
	        	lstPaisesExcluidos.add(ConstantesCO.COD_PAIS_CANADA);
	        	filterMateriales.put("lstPaisesExcluidos", lstPaisesExcluidos);
	        } else if (ConstantesCO.AC_SGP_UE.equals(idAcuerdo)) {
	        	filterMateriales.put("idAcuerdoBeneficiario",ConstantesCO.AC_SGP_UE);
	        	filterMateriales.put("idAcuerdoExcluido",ConstantesCO.AC_SGP_UE);
	        } else if (ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)) {
	        	filterMateriales.put("idOtros",ConstantesCO.AC_COSTARRICA_OTROS);
	        }else if (ConstantesCO.AC_HONDURAS.equals(idAcuerdo)) {
	        	filterMateriales.put("idOtros",ConstantesCO.AC_HONDURAS_OTROS);
	        }

        	// Materiales Originarios del Tercer Componente
            tMaterialesTercerComponente = ibatisService.loadGrid("certificadoOrigen.orden.dj.material.origen_pais.grilla", filterMateriales);
            request.setAttribute("tMaterialesTercerComponente", tMaterialesTercerComponente);
            request.setAttribute("mat3erCompEmpty", ((tMaterialesTercerComponente == null || tMaterialesTercerComponente.size() == 0)?"1":"0"));
        }

        int numMateriales = (tMaterialesNoOriginarios != null ? tMaterialesNoOriginarios.size() : 0) + (tMaterialesPeru != null ? tMaterialesPeru.size() : 0) + (tMaterialesSegundoComponente != null ? tMaterialesSegundoComponente.size() : 0) + (tMaterialesTercerComponente != null ? tMaterialesTercerComponente.size() : 0);

        // El Tercer Componente sólo existe en algunos acuerdos
        if (ConstantesCO.AC_MERCOSUR_INT.equals(idAcuerdo)) {
        	numMateriales += (tMaterialesTercerComponente != null ? tMaterialesTercerComponente.size() : 0);
        }

        puedeTransmitirDJValidada = validarDJCompleta(djObj, messageList, idAcuerdo, numMateriales);

        if (ConstantesCO.OPCION_SI.equals(datos.getString("modoProductorValidador")) || (!ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(djObj.getEstadoRegistro()) &&
        	!ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(djObj.getEstadoRegistro()))){
        	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        }

        /*String mostrarBtnValidacionProd = "0";
        if ("N".equals(request.getAttribute("bloqueado")!=null?request.getAttribute("bloqueado").toString():"N") && ordenObj != null) {
        	mostrarBtnValidacionProd = this.certificadoOrigenLogic.djBtnValidacionProd(djObj.getDjId(), ordenObj.getOrden(), ordenObj.getMto());
        }
        request.setAttribute("mostrarBtnValidacionProd", mostrarBtnValidacionProd);*/

        request.setAttribute("puedeTransmitirDJValidada", puedeTransmitirDJValidada);
        request.setAttribute("sumaValorUSMaterial", this.certificadoOrigenLogic.totalValorUSMateriales(filterAutoriz));

    }

    private String esValidadorDJ(int tDoc, String numDoc, Table tProductores) {
    	String res = ConstantesCO.OPCION_NO;

    	Row objRow;
    	for (Object object : tProductores) {
    		objRow = (Row) object;

    		if (objRow.getCell("DOCUMENTOTIPO").getStringValue().equals(""+tDoc) &&
    			objRow.getCell("NUMERODOCUMENTO").getStringValue().trim().equals(numDoc) ){
    			res = ConstantesCO.OPCION_SI;
    			break;
    		}
		}

    	return res;
    }

    public ModelAndView actualizarConsolidado(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);

    	HashUtil<String, String> element = new HashUtil<String, String>();


        long djId = datos.getLong("djId");
        BigDecimal valorUs = new BigDecimal( datos.getString("DJ.valorUs") );
        BigDecimal prctjSegunCriterio = null;
        String strPrctjSegunCriterio = datos.getString("DJ.porcentajeSegunCriterio");
        if (!ConstantesCO.OPCION_NO.equals(strPrctjSegunCriterio) && !"".equals(strPrctjSegunCriterio)) {
        	prctjSegunCriterio = new BigDecimal(strPrctjSegunCriterio);
        }
        BigDecimal pesoNetoMercancia = null;
        String strPesoNetoMercancia = datos.getString("DJ.pesoNetoMercancia");
        if (!ConstantesCO.OPCION_NO.equals(strPesoNetoMercancia) && !"".equals(strPesoNetoMercancia)) {
        	pesoNetoMercancia = new BigDecimal(strPesoNetoMercancia);
        }
        BigDecimal valorUsFabrica = null;
        String strValorUsFabrica = datos.getString("DJ.valorUsFabrica");
        if (!ConstantesCO.OPCION_NO.equals(strValorUsFabrica) && !"".equals(strValorUsFabrica)) {
        	valorUsFabrica = new BigDecimal(strValorUsFabrica);
        }
       
        Message message = null;
        MessageList messageList = new MessageList();
    	try {

        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("djId", djId);
        	filter.put("valorUs", valorUs);    		
        	filter.put("pesoNetoMercancia", pesoNetoMercancia);
        	filter.put("porcentajeSegunCriterio", prctjSegunCriterio);
        	filter.put("valorUsFabrica", valorUsFabrica);

        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	ibatisService.loadElement("certificadoOrigen.orden.dj.consolidado.update", filter);

        	//element.put("CONSOLIDADO.mensaje", "El consolidado ha sido grabado con éxito");

        	message = new Message("co.mercancia.dj.consolidado.guardar.success");

        } catch(Exception e) {
        	//element.put("CONSOLIDADO.mensaje", "ERROR al intentar actualizar el consolidado del Material");
        	message = new Message("co.mercancia.dj.consolidado.guardar.error");
        	logger.error("ERROR al intentar actualizar el consolidado del Material", e);
        } finally {
        	messageList.add(message);
        	request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        	return cargarDeclaracionJuradaForm(request,response);
        }
    }

    /**
     * Carga la información de Material del formulario
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarDeclaracionJuradaMaterialForm(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        MessageList messageList = new MessageList();
        DeclaracionJuradaMaterial materialObj = null;

        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("orden"), datos.getInt("mto"));
      //Ticket 149343 GCHAVEZ-05/07/2019
        String versionAcuerdo = "";
        int secuenciaMaterial = datos.getInt("secuenciaMaterial");
        request.setAttribute("disabledGrabar", false);
        if(datos.get("djId")!=null){
        //if(false){
	        if (secuenciaMaterial != 0 ) { // Si ya existe el Material
	            materialObj = certificadoOrigenLogic.getDeclaracionJuradaMaterialById(datos.getInt("djId"), secuenciaMaterial);
	        } else {
	            request.setAttribute("secuenciaMaterial", secuenciaMaterial);
	        }
	
	        request.setAttribute("nota", datos.getString("nota"));
	        request.setAttribute("versionFormato", datos.getString("versionFormato"));
	        cargarDeclaracionJuradaMaterial(ordenObj, materialObj, datos.getInt("idAcuerdo"), messageList, request);
	        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	        
	        Integer sa = datos.getInt("idAcuerdo");
	    	if(ConstantesCO.AC_ALIANZA_PACIFICO != sa){
	    		versionAcuerdo = this.getFormatoVersionSufijo(ordenObj);
	    		request.setAttribute("idAcuerdoVersion", datos.getString("idAcuerdo")+versionAcuerdo);
	        	request.setAttribute("versionJs", versionAcuerdo);
	    	}
        }else{
	    	Message msg = new Message("co.dj.material.idDj.nulo");
	    	messageList.add(msg);
	    	request.setAttribute("disabledGrabar", true);
	    	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        }
      //Ticket 149343 GCHAVEZ-05/07/2019
        return new ModelAndView(formMaterial+versionAcuerdo);
    }

    /**
     * Carga la información de material
     * @param ordenObj
     * @param productoObj
     * @param request
     */
    public void cargarDeclaracionJuradaMaterial(Orden ordenObj, DeclaracionJuradaMaterial materialObj, Integer idAcuerdo, MessageList messageList, HttpServletRequest request){
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Long djId = datos.getLong("djId");

        HashUtil<String, Object> filterAdjuntoMaterial = new HashUtil<String, Object>();
        filterAdjuntoMaterial.put("djId", djId);

        if (materialObj != null) {
            RequestUtil.setAttributes(request, "DJ_MATERIAL.", materialObj);
            request.setAttribute("secuenciaMaterial", materialObj.getSecuenciaMaterial());
            request.setAttribute("umFisicaId", materialObj.getUmFisicaId());
        	filterAdjuntoMaterial.put("secuenciaMaterial", materialObj.getSecuenciaMaterial());
        } else {
        	filterAdjuntoMaterial.put("secuenciaMaterial", datos.getInt("secuenciaMaterial"));
        }


        Table tAdjuntosMaterial = ibatisService.loadGrid("certificadoOrigen.orden.dj.material.adjuntos.grilla", filterAdjuntoMaterial);
        request.setAttribute("tAdjuntosMaterial", tAdjuntosMaterial);

        /*if ((idAcuerdo.equals(ConstantesCO.AC_COSTARRICA) ) && !materialObj.getSecuenciaMaterial().equals(new Integer("0")) && tAdjuntosMaterial.size() == 0) {
        	Message msg = new Message("co.dj.material.falta.adjunto");
        	messageList.add(msg);
        }*/

        request.setAttribute("orden", datos.getLong("orden"));
        request.setAttribute("mto", datos.getInt("mto"));
        request.setAttribute("djId", djId);
        request.setAttribute("DJ_MATERIAL.djId", djId);
        request.setAttribute("tipoPais", datos.getString("tipoPais"));
        request.setAttribute("criterioArancelario", datos.get("criterioArancelario"));
    	request.setAttribute("bloqueado", datos.get("bloqueado"));
    	request.setAttribute("modoProductorValidador", datos.get("modoProductorValidador"));
    	request.setAttribute("esValidador", datos.get("esValidador"));
    	request.setAttribute("modoFormato", datos.get("modoFormato"));
    	request.setAttribute("idAcuerdo", datos.get("idAcuerdo"));
    	request.setAttribute("estadoDj", datos.get("estadoDj"));
    	request.setAttribute("idPaisSegundoComponente", datos.get("idPaisSegundoComponente"));
        request.setAttribute("secuenciaOrigen", datos.get("secuenciaOrigen"));
        request.setAttribute("tipoRol", datos.get("tipoRol"));

        if (ordenObj != null) {
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("coId", ordenObj.getIdFormatoEntidad());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            //request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());
        }
        
        request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(idAcuerdo) );

    }

	/**
	 * Graba un Material de Declaración Jurada
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView grabarDeclaracionJuradaMaterialForm(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ_MATERIAL.");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));

        DeclaracionJuradaMaterial declaracionJuradaMaterialObj = certificadoOrigenLogic.convertMapToDeclaracionJuradaMaterial(datos);
        MessageList messageList = null;

        // Verificamos si es un insert o update
        if (request.getParameter("secuenciaMaterial").toString().equals("0")) {
            messageList = certificadoOrigenLogic.insertDeclaracionJuradaMaterial(declaracionJuradaMaterialObj);
            declaracionJuradaMaterialObj = (DeclaracionJuradaMaterial) messageList.getObject();
        } else {
        	declaracionJuradaMaterialObj.setSecuenciaMaterial(Integer.parseInt(datos.get("secuenciaMaterial").toString()));
            messageList = certificadoOrigenLogic.updateDeclaracionJuradaMaterial(declaracionJuradaMaterialObj);
        }
        
      //Excepcion para el acuerdo Alianza Pacifico 
    	String versionAcuerdo = "";    	
    	Integer codigoAcuerdo = elements.getInt("idAcuerdo"); 
    	if(ConstantesCO.AC_ALIANZA_PACIFICO != codigoAcuerdo){
    		versionAcuerdo = this.getFormatoVersionSufijo(ordenObj);    		
    		request.setAttribute("idAcuerdoVersion", elements.getString("idAcuerdo")+versionAcuerdo);
        	request.setAttribute("versionFormato", ordenObj.getVersionFormato());
        	request.setAttribute("versionJs", versionAcuerdo);
    	}

    	request.setAttribute("nota", elements.getString("nota"));
    	//Ticket 149343 GCHAVEZ-05/07/2019
    	request.setAttribute("disabledGrabar", false);
    	//Ticket 149343 GCHAVEZ-05/07/2019
        cargarDeclaracionJuradaMaterial(ordenObj, declaracionJuradaMaterialObj, elements.getInt("idAcuerdo"), messageList, request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return new ModelAndView(formMaterial+versionAcuerdo);
    }

	/**
	 * Carga los Adjuntos de Declaración Jurada
	 * @param request
	 * @param response
	 * @return
	 */
    public ModelAndView cargarArchivoDeclaracionJuradaMaterialForm(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long orden = datos.getLong("orden");
    	int mto = datos.getInt("mto");
    	long djId = datos.getLong("DJ_MATERIAL.djId");
    	int secuenciaMaterial = datos.getInt("DJ_MATERIAL.secuenciaMaterial");
    	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;

        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

     	MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                // Llenamos el objeto adjunto
                AdjuntoCertificadoOrigen adjuntoCertificadoOrigen = new AdjuntoCertificadoOrigen();
                adjuntoCertificadoOrigen.setDjId(djId);
                adjuntoCertificadoOrigen.setSecuenciaMaterial(secuenciaMaterial);
                adjuntoCertificadoOrigen.setAdjuntoRequeridoUo(null);
                adjuntoCertificadoOrigen.setAdjuntoTipo(adjuntoTipo);
                adjuntoCertificadoOrigen.setNombreArchivo(ComponenteOrigenUtil.replaceSpecialCharacters(multipartFile.getOriginalFilename()));
                adjuntoCertificadoOrigen.setArchivo(multipartFile.getBytes());

                messageList = certificadoOrigenLogic.cargarAdjuntoDeclaracionJurada(adjuntoCertificadoOrigen);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Llenamos todo lo que vino para mantener la información en request
        RequestUtil.setAttributes(request);
        cargarDeclaracionJuradaMaterial(ordenObj, null, datos.getInt("idAcuerdo"), messageList, request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
      //Ticket 149343 GCHAVEZ-05/07/2019
        request.setAttribute("disabledGrabar", false);
      //Ticket 149343 GCHAVEZ-05/07/2019
        return new ModelAndView(formMaterial);
    }

	/**
	 * Elimina un Material de Declaración Jurada
	 * @param request
	 * @param response
	 * @return
	 */
	/* public ModelAndView eliminarDeclaracionJuradaMaterialForm(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        long id = 0;
        //int djId = datos.getInt("djId");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        if (deleteColumns.length > 0) {
            for(int i = 0; i< deleteColumns.length; i++ ){
                Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
                id = new Long(row.getCell("ADJUNTO_ID").getValue().toString());

            	logger.debug("Se eliminara adjunto_id = "+id);

            	this.certificadoOrigenLogic.eliminarAdjuntoDJ(id);
            }

            MessageList messageList = new MessageList();
            Message message = new Message("delete.success");
            messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        }

        return cargarDeclaracionJuradaForm(request, response);
    } */

	public ModelAndView eliminarDeclaracionJuradaMaterialForm(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ_MATERIAL.");
		DeclaracionJuradaMaterial declaracionJuradaMaterialObj = (DeclaracionJuradaMaterial) certificadoOrigenLogic.convertMapToDeclaracionJuradaMaterial(datos);
		declaracionJuradaMaterialObj.setSecuenciaMaterial(Integer.parseInt(datos.get("secuenciaMaterial").toString()));
		MessageList messageList = certificadoOrigenLogic.deleteDeclaracionJuradaMaterial(declaracionJuradaMaterialObj);


		/*Orden ordenObj = ordenLogic.getOrdenById(Long.parseLong(request.getParameter("orden").toString()), Integer.parseInt(request.getParameter("mto").toString()));
		messageList.setObject(ordenObj);*/
		//cargarInformacionFormato(request, messageList);

		request.setAttribute(Constantes.MESSAGE_LIST, messageList);

		return cargarDeclaracionJuradaForm(request, response);
	}

	/**
	 * Elimina un Material de Declaración Jurada
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView eliminarArchivoDeclaracionJuradaMaterialForm(HttpServletRequest request, HttpServletResponse response) {
		HashUtil datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        long id = 0;
        //int djId = datos.getInt("djId");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        MessageList messageList = new MessageList();

        if (deleteColumns.length > 0) {
            for(int i = 0; i< deleteColumns.length; i++ ){
                Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
                id = new Long(row.getCell("ADJUNTOID").getValue().toString());

            	logger.debug("Se eliminara adjunto_id = "+id);

            	//this.certificadoOrigenLogic.eliminarAdjuntoDJ(id);            	
            }

            Message message = new Message("delete.success");
            messageList.add(message);

        }

        // Llenamos todo lo que vino para mantener la información en request
        RequestUtil.setAttributes(request);
        //request.setAttribute(Constantes.MESSAGE_LIST, messageList);
		Orden ordenObj = ordenLogic.getOrdenById(Long.parseLong(request.getParameter("orden").toString()), Integer.parseInt(request.getParameter("mto").toString()));
        cargarDeclaracionJuradaMaterial(ordenObj,null, datos.getInt("idAcuerdo"), messageList,request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
      //Ticket 149343 GCHAVEZ-05/07/2019
        request.setAttribute("disabledGrabar", false);
      //Ticket 149343 GCHAVEZ-05/07/2019
        return new ModelAndView(formMaterial);
	}


    /**
     * Carga la información de Material del formulario
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarDeclaracionJuradaProductorForm(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        MessageList messageList = new MessageList();

        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden")), new Integer(request.getParameter("mto")));

        int secuenciaProductor = new Integer(request.getParameter("secuenciaProductor")); //datos.getInt("secuenciaProductor");
        long calificacionUoId = new Long(request.getParameter("calificacionUoId"));//datos.getInt("djId");

        //DeclaracionJurada djObj = certificadoOrigenLogic.getCODeclaracionJuradaById(djId);
        CalificacionOrigen calOrigenObj = certificadoOrigenLogic.getCalificacionOrigenById(calificacionUoId);
        request.setAttribute("rolExportador", calOrigenObj.getTipoRolCalificacion());

        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        String bloqueado = (String) datos.get("bloqueado");
        if (secuenciaProductor != 0 ) { // Si ya existe el Productor
            DeclaracionJuradaProductor productorObj = certificadoOrigenLogic.getDeclaracionJuradaProductorById(calificacionUoId, secuenciaProductor);
            cargarDeclaracionJuradaProductor(ordenObj, productorObj, request);

            if ( ConstantesCO.TIPO_ROL_PRODUCTOR.equals( "" + calOrigenObj.getTipoRolCalificacion() ) && usuario.getTipoDocumento() == productorObj.getDocumentoTipo().intValue() && usuario.getNumeroDocumento().equals(productorObj.getNumeroDocumento())){
            	bloqueado = ConstantesCO.OPCION_SI;
            }
        }else{
            request.setAttribute("secuenciaProductor", datos.getInt("secuenciaProductor"));
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
        }

        request.setAttribute("exportador", datos.getString("exportador"));
        /*if ("0".equals(datos.getString("exportador"))) {
        	request.setAttribute("bloqueado", ConstantesCO.OPCION_SI);
        }*/

        request.setAttribute("calificacionUoId", calificacionUoId);
        request.setAttribute("DJ_PRODUCTOR.calificacionUoId", calificacionUoId);
    	request.setAttribute("bloqueado", bloqueado);
    	request.setAttribute("modoProductorValidador", datos.get("modoProductorValidador"));
    	request.setAttribute("esValidador", datos.get("esValidador"));
    	request.setAttribute("modoFormato", datos.get("modoFormato"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
    	request.setAttribute("idAcuerdo", datos.getString("idAcuerdo"));
    	
    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = this.getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", datos.getString("idAcuerdo")+versionAcuerdo);
    	
    	request.setAttribute("formato", datos.getString("formato"));
    	request.setAttribute("secuenciaMercancia", datos.getString("secuenciaMercancia"));

        request.setAttribute("tipoDocumentoSolicitante", usuario.getTipoDocumento());
    	request.setAttribute("numDocumentoSolicitante", usuario.getNumeroDocumento());
        return new ModelAndView(formProductor);
    }

    /**
     * Carga la información de material
     * @param ordenObj
     * @param productoObj
     * @param request
     */
    public void cargarDeclaracionJuradaProductor(Orden ordenObj, DeclaracionJuradaProductor productorObj, HttpServletRequest request){
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        RequestUtil.setAttributes(request, "DJ_PRODUCTOR.", productorObj);

        request.setAttribute("secuenciaProductor", productorObj.getSecuenciaProductor());
    	request.setAttribute("bloqueado", datos.get("bloqueado"));
    	//request.setAttribute("modoProductorValidador", datos.get("modoProductorValidador"));
    	//request.setAttribute("modoFormato", datos.get("modoFormato"));
        request.setAttribute("esValidador", datos.get("esValidador"));
    	request.setAttribute("formato", datos.get("formato"));

        if (ordenObj != null) {
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());
        }

        HashUtil<String, Object> adjData = new HashUtil<String, Object>();
        adjData.put("calificacionUoId", request.getParameter("calificacionUoId"));
        adjData.put("secuenciaProductor", productorObj.getSecuenciaProductor());

        cargarAdjuntosDJProductor(request, adjData);
    }

    public ModelAndView grabarDJProductor(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ_PRODUCTOR.");
            HashUtil<String, String> elements = RequestUtil.getParameter(request);

            Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));


            //DeclaracionJurada djObj = certificadoOrigenLogic.convertMapToDeclaracionJurada(datos);
            //djObj.setDjId(new Long(elements.get("djId").toString()));
            String secuencia = elements.get("secuenciaProductor").toString();
            MessageList messageList = null;

        	DeclaracionJuradaProductor djProdObj = this.certificadoOrigenLogic.convertMapToDeclaracionJuradaProductor(datos);
        	djProdObj.setCalificacionUoId(elements.getLong("calificacionUoId"));
        	djProdObj.setSecuenciaProductor(elements.getInt("secuenciaProductor"));

        	CalificacionOrigen calOrigenObj = certificadoOrigenLogic.getCalificacionOrigenById(elements.getLong("calificacionUoId"));
        	djProdObj.setTipoRolCalificacionNew(calOrigenObj.getTipoRolCalificacion());

            if ("0".equals(secuencia)) {
            	messageList = this.certificadoOrigenLogic.insertDeclaracionJuradaProductor(djProdObj);
            	djProdObj = (DeclaracionJuradaProductor) messageList.getObject();
            } else {
            	messageList = this.certificadoOrigenLogic.updateDeclaracionJuradaProductor(djProdObj);
            }


            if (djProdObj!=null) {
            	djProdObj = this.certificadoOrigenLogic.getDeclaracionJuradaProductorById(elements.getLong("calificacionUoId"), djProdObj.getSecuenciaProductor());
                cargarDeclaracionJuradaProductor(ordenObj, djProdObj, request);
            }

            RequestUtil.setAttributes(request);
            request.setAttribute("secuenciaProductor", djProdObj != null ? djProdObj.getSecuenciaProductor() : 0);
            request.setAttribute("rolExportador", elements.getString("rolExportador"));

            request.setAttribute("djId", elements.get("djId"));
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
            request.setAttribute("idAcuerdo", ordenObj.getIdAcuerdo());
            
        	//JMC 23/06/2017 Alianza
        	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
        	request.setAttribute("idAcuerdoVersion", ordenObj.getIdAcuerdo()+versionAcuerdo);

            return new ModelAndView(formProductor);
    }

    public ModelAndView eliminarDJProductor(HttpServletRequest request, HttpServletResponse response) {
        //Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CO_FACTURA.");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        DeclaracionJuradaProductor prodObj = new DeclaracionJuradaProductor();  //this.certificadoOrigenLogic.convertMapToCOMercancia(datos);
        prodObj.setCalificacionUoId(elements.getLong("calificacionUoId"));
        prodObj.setSecuenciaProductor(Integer.parseInt(elements.get("secuenciaProductor").toString()));

        RequestUtil.setAttributes(request);

        MessageList messageList = this.certificadoOrigenLogic.deleteDeclaracionJuradaProductor(prodObj);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        /*Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden").toString()), Integer.parseInt(request.getParameter("mto").toString()));
        messageList.setObject(ordenObj);*/
        //cargarCertificadoOrigenMercancia(request, messageList);

        //return new ModelAndView(formatoMercanciaDeclaracionJurada);
        if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(request.getParameter("formato"))){
        	return cargarInformacionOrden(request, response);
        } else {
        	return this.cargarCertificadoOrigenMercanciaForm(request, response);
        }

    }

    public ModelAndView solicitarValidacionDJProductor(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ.");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));
        DeclaracionJurada djObj = null;

        long djId = elements.getLong("CALIFICACION.djId");
        MessageList messageList = null;

        messageList = certificadoOrigenLogic.solicitaValidacionProductor(djId);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(request.getParameter("formato"))){
        	return cargarInformacionOrden(request, response);
        } else {
        	return this.cargarCertificadoOrigenMercanciaForm(request, response);
        }
    }

    public ModelAndView mostrarCalificarValidacionDJ(HttpServletRequest request, HttpServletResponse response) {
    	// Traspasamos variables de request
        RequestUtil.setAttributes(request);

        return new ModelAndView(this.calificarValidacionDj);
    }

    public ModelAndView resolverCalificacionValidacionDJ(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        MessageList messageList = null;

        long djId = elements.getLong("djId");

        String respuesta = elements.getString("respuesta");

        if (ConstantesCO.OPCION_SI.equals(respuesta)) { // Si se acepta la validación
        	messageList = this.certificadoOrigenLogic.aceptaSolicitudValidacion(djId);
        } else { // Si se rechaza la validación
        	messageList = this.certificadoOrigenLogic.rechazaSolicitudValidacion(djId); // No existe aun
        }

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return listarDeclaracionJurada(request, response);
    }

    public ModelAndView transmitirDjValidada(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        MessageList messageList = null;

        long djId = elements.getLong("djId");

    	messageList = this.certificadoOrigenLogic.transmiteDjValidada(djId);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return listarDeclaracionJurada(request, response);
    }

    /***********************************************************************/
    /** Impresiones **/
    /***********************************************************************/
    public ModelAndView imprimirHojaResumenSUCE(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        Integer idAcuerdo = datos.getInt("idAcuerdo");
        Long drId = new Long(datos.get("DETALLE.dr_id")!=null?datos.getString("DETALLE.dr_id"):datos.getString("DR.drId"));
        Long sdr = new Long(datos.get("DETALLE.sdr")!=null?datos.getString("DETALLE.sdr"):datos.getString("DR.sdr"));
        
        try {
			certificadoOrigenLogic.setearResumenReporte(drId, sdr, idAcuerdo, datos, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
      
        return null;
    }
    
    public ModelAndView verPrimeraFirma(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        Long drId = new Long(datos.get("DETALLE.dr_id")!=null?datos.getString("DETALLE.dr_id"):datos.getString("DR.drId"));
        Long sdr = new Long(datos.get("DETALLE.sdr")!=null?datos.getString("DETALLE.sdr"):datos.getString("DR.sdr"));

        try {
    		InteroperabilidadFirma etapa = iopLogic.obtenerToken(drId, sdr.intValue());
    		InteroperabilidadFirma firma = iopLogic.obtenerPdf(etapa.getToken(), 2);
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline;filename=DocumentoFirmado.pdf");
            OutputStream os = response.getOutputStream();
            os.write(firma.getPdf());
            os.close();    		
		} catch (Exception e) {
			e.printStackTrace();
		}
      
        return null;
    }    

    public ModelAndView verSegundaFirma(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        Long drId = new Long(datos.get("DETALLE.dr_id")!=null?datos.getString("DETALLE.dr_id"):datos.getString("DR.drId"));
        Long sdr = new Long(datos.get("DETALLE.sdr")!=null?datos.getString("DETALLE.sdr"):datos.getString("DR.sdr"));

        try {
        	//--- 06/12/2016 JMC - ELIMINAR
        	/*InteroperabilidadFirma _etapa = iopLogic.obtenerToken(drId, sdr.intValue());
        	InteroperabilidadFirma iop = iopDAO.obtenerEtapa(_etapa.getToken(), 1);
        	iopService.procesarTransmisionParaPack(_etapa.getToken(), iop, 3);
        	*/
        	//JMC - ELIMINAR-----//
        	
    		InteroperabilidadFirma etapa = iopLogic.obtenerToken(drId, sdr.intValue());
    		InteroperabilidadFirma firma = iopLogic.obtenerPdf(etapa.getToken(), 3);
            response.setContentType("application/pdf");
            //response.setHeader("Content-Disposition", "inline;filename=DocumentoFirmado.pdf");
            response.setHeader("Content-Disposition", "inline;filename=pdf_que_se_debe_mostrar.pdf"); //    25/01/2017 JMC Alianza Pacifico        
            OutputStream os = response.getOutputStream();
            os.write(firma.getPdf());
            os.close();    		
		} catch (Exception e) {
			e.printStackTrace();
		}
      
        
        
        return null;
    }    
    
    public ModelAndView imprimirHojaResumenAvance(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        Integer idAcuerdo = datos.getInt("idAcuerdo");
        String formato = datos.getString("formato").toUpperCase();
        Long ordenId = new Long(datos.getString("orden"));
        Long mto = new Long(datos.getString("mto"));

        try {
			certificadoOrigenLogic.setearAvanceReporte(ordenId, mto, idAcuerdo, datos, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
      
        return null;
    }
    

    private void setearSubReportes(String tipoReporte, Integer idAcuerdo, Map parameters, HashUtil<String, String> datos) throws JRException, IOException{

    	parameters.put("P_VISTA_CAB_CLAUSE", datos.getString("formato"));
    	if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(datos.getString("formato"))) {
    		parameters.put("P_VISTA_DET_CLAUSE", ConstantesCO.CO_SOLICITUD.toUpperCase());
    	} else if (ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(datos.getString("formato"))) {
    		parameters.put("P_VISTA_DET_CLAUSE", ConstantesCO.CO_DUPLICADO.toUpperCase());
    	} else if (ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase(datos.getString("formato"))) {
    		parameters.put("P_VISTA_DET_CLAUSE", ConstantesCO.CO_REEMPLAZO.toUpperCase());
    	} else if (ConstantesCO.CO_ANULACION.equalsIgnoreCase(datos.getString("formato"))) {
    		parameters.put("P_VISTA_DET_CLAUSE", ConstantesCO.CO_ANULACION.toUpperCase());
    	}
    	
    	Resource jasperFileSubReport1;
    	
    	if((ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo) ||ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)||ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)||
    			ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)||ConstantesCO.AC_SGP_UE.equals(idAcuerdo)||ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)||
    			ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)||ConstantesCO.AC_SGP_AUS.equals(idAcuerdo))){
    		    parameters.put("P_ACUERDO", idAcuerdo.toString());
    			jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+ tipoReporte +"sgp.subreporte1", "");
    	} else{
	    	jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+ tipoReporte +"acuerdo" + idAcuerdo + ".subreporte1", "");
    	}
    	
    	if (jasperFileSubReport1!=null && jasperFileSubReport1.exists()) {
    		JasperReport jReportSubReport1 = (JasperReport)JRLoader.loadObject(jasperFileSubReport1.getInputStream());
    		parameters.put("P_SUB_REPORTE_1", jReportSubReport1);
    	}

    	if(ConstantesCO.AC_CHILE.equals(idAcuerdo) || ConstantesCO.AC_CUBA.equals(idAcuerdo) || ConstantesCO.AC_MEXICO.equals(idAcuerdo)|| ConstantesCO.AC_MERCOSUR.equals(idAcuerdo)||
    			ConstantesCO.AC_CAN.equals(idAcuerdo)|| ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)|| ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)|| ConstantesCO.AC_PANAMA.equals(idAcuerdo)||
    			ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)|| ConstantesCO.AC_COREA.equals(idAcuerdo)|| ConstantesCO.AC_CHINA.equals(idAcuerdo)|| ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)){
        	Resource jasperFileSubReport2 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte2", "");
        	System.out.println("******Subreporte2: "+"reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte2");
        	if (jasperFileSubReport2!=null && jasperFileSubReport2.exists()) {
        		JasperReport jReportSubReport2 = (JasperReport)JRLoader.loadObject(jasperFileSubReport2.getInputStream());
        		parameters.put("P_SUB_REPORTE_2", jReportSubReport2);
        	}
    	}

    	if(ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)){
        	Resource jasperFileSubReport3 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte3", "");
        	System.out.println("******Subreporte3: "+"reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte3");
        	if (jasperFileSubReport3!=null && jasperFileSubReport3.exists()) {
        		JasperReport jReportSubReport3 = (JasperReport)JRLoader.loadObject(jasperFileSubReport3.getInputStream());
        		parameters.put("P_SUB_REPORTE_3", jReportSubReport3);
        	}
    	}
    }

    public ModelAndView imprimirResumenAnulacionSUCE(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);
        Integer idAcuerdo = datos.getInt("idAcuerdo");
        try {
        	Map parameters = new HashMap();
        	parameters.put("DRID", new Long(datos.getString("DETALLE.dr_id")));
        	parameters.put("SDR", new Long(datos.getString("DETALLE.sdr")));

        	Solicitante solicitante = formatoLogic.getSolicitanteDetail(usuario);

        	parameters.put("SOLICITANTE.NOMBRE", solicitante.getNombre() );
        	parameters.put("SOLICITANTE.TIPO_DOCUMENTO", solicitante.getDocumentoTipoDesc());
        	parameters.put("SOLICITANTE.NUM_DOCUMENTO", solicitante.getNumeroDocumento());
        	
        	HashUtil filter = new HashUtil<String, Object>();
        	filter.put("usuarioId", solicitante.getUsuarioId());
        	filter.put("drId", new Long(datos.getString("DETALLE.dr_id")));
        	filter.put("sdr", new Long(datos.getString("DETALLE.sdr")));

        	HashUtil element = ibatisService.loadElement("certificadoOrigen.titularDireccion.element", filter);
        	
        	parameters.put("SOLICITANTE.DOMICILIO", element.get("TITULAR_DIRECCION"));
        	
        	//this.setearSubReportes(ConstantesCO.TIPO_REPORTE_SUCE,idAcuerdo, parameters, datos);

        	Resource jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen.anulacion", "");
        	//ctx.getResource("WEB-INF/resource/reportes/hojaResumen.jasper");
        	JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());

	        DBSessionContext db = (DBSessionContext)SpringContext.getApplicationContext().getBean("dbSessionContext");
		    JasperPrint print = formatoLogic.ejecutarReporteConConexionBD(jReport, parameters);

            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline;filename=HojaResumen.pdf");
            OutputStream os = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, os);
            os.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }

    public ModelAndView cargarAdjuntosDJ(HttpServletRequest request, HttpServletResponse response) {
    	cargarInformacionAdjunto(request);
        return new ModelAndView(adjuntosDJ);
    }

    public ModelAndView cargarArchivoDJ(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long djId = datos.getLong("djId");
    	int adjuntoRequeridoDj = datos.getInt("adjuntoRequeridoDj");
//    	int formato = datos.getInt("formato");
//    	int adjunto = datos.getInt("adjunto");
//    	Integer mensajeId = (datos.get("mensajeId")!=null && !datos.get("mensajeId").equals("")) ? datos.getInt("mensajeId") : null;
//    	String esDetalle = (datos.get("esDetalle")!=null && !datos.get("esDetalle").equals("")) ? datos.getString("esDetalle") : null;
    	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;

     	MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                byte [] bytes = multipartFile.getBytes();
                String nombre = multipartFile.getOriginalFilename();
                messageList = this.certificadoOrigenLogic.uploadFileDJ(djId,-1,adjuntoRequeridoDj,adjuntoTipo,nombre,bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cargarInformacionAdjunto(request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return new ModelAndView(adjuntosDJ);
    }

    public ModelAndView eliminarAdjuntoDJ(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        long adjuntoId = 0;
        int djId = datos.getInt("djId");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        if (deleteColumns.length > 0) {
            for(int i = 0; i< deleteColumns.length; i++ ){
                Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
                adjuntoId = new Long(row.getCell("ADJUNTO_ID").getValue().toString());

            	logger.debug("Se eliminara adjunto_id = "+adjuntoId);

            	//this.certificadoOrigenLogic.eliminarAdjuntoDJ(id);
            	//28/01/2014 Resuelve problema de eliminar el adjunto de la Modificaicon de Solicitud
            	this.certificadoOrigenLogic.eliminarAdjuntoDJ(adjuntoId,djId);
            }

            MessageList messageList = new MessageList();
            Message message = new Message("delete.success");
            messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        }

        cargarInformacionAdjunto(request);
        return new ModelAndView(adjuntosDJ);
    }

    private void cargarInformacionAdjunto(HttpServletRequest request) {
 	   HashUtil<String, String> datos = RequestUtil.getParameter(request);
 	   HashUtil<String, Object> filter = new HashUtil<String, Object>();

	   int djId = datos.getInt("djId");

	   int idRequerido = datos.getInt("adjuntoRequeridoDj");

	   AdjuntoRequeridoDJMaterial adjunto = this.certificadoOrigenLogic.getRequeridoDJById(idRequerido);

	   filter.put("djId", djId);
	   filter.put("adjuntoRequeridoDj", new Integer(idRequerido));

 	   Table tAdjuntos = ibatisService.loadGrid("dj.adjuntos.grilla", filter);

 	   //HashUtil<String, Object> adjuntoDetalle = ibatisService.loadElement("dj.adjunto_requerido.element", filter);
 	   HashUtil<String, Object> adjuntoDetalle = ibatisService.loadElement("dj.adjuntos.detalle.element", filter);

 	   if (adjuntoDetalle!=null && adjuntoDetalle.size() > 0) {
 		   request.setAttribute("idAdjuntoDetalle", adjuntoDetalle.get("ID"));
 		   request.setAttribute("nombreAdjuntoDetalle", adjuntoDetalle.get("NOMBRE"));
 	   }

 	   String hide = datos.getString("bloqueado").equals("S")?"yes":
 		             datos.getString("transmitido").equals("S")?"yes":"no";

 	   request.setAttribute("tAdjuntos", tAdjuntos);
        request.setAttribute("djId", djId);
        request.setAttribute("adjuntoRequeridoDj", idRequerido);
        request.setAttribute("titulo", adjunto.getDescripcion());
        //request.setAttribute("permiteAdicional", adjunto.getPermiteAdicional());
        request.setAttribute("bloqueado", datos.getString("bloqueado"));
        request.setAttribute("transmitido", datos.getString("transmitido"));
        request.setAttribute("hide", hide);
        request.setAttribute("esValidador", datos.get("esValidador"));
    	//request.setAttribute("modoProductorValidador", datos.get("modoProductorValidador"));
    	//request.setAttribute("modoFormato", datos.get("modoFormato"));

    }

    public ModelAndView aprobarDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ.");

     	DeclaracionJurada dj = new DeclaracionJurada();
     	dj.setCalificacionUoId(new Long(request.getParameter("calificacionUoId").toString()));
     	/*if ( datos.get("secuenciaNorma") != null && !"".equals(datos.get("secuenciaNorma").toString())) {
         	dj.setSecuenciaNorma(Integer.parseInt(datos.get("secuenciaNorma").toString()));
     	}
     	if ( datos.get("secuenciaOrigen") != null && !"".equals(datos.get("secuenciaOrigen").toString()) ) {
         	dj.setSecuenciaOrigen(Integer.parseInt(datos.get("secuenciaOrigen").toString()));
     	}
     	dj.setPartidaSegunAcuerdo(datos.get("partidaSegunAcuerdo").toString());*/

        MessageList messageList = resolutorLogic.evaluadorDJCalifica(dj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        // Obtenemos los datos basicos del formato
        /*HashUtil<String, String> elements = RequestUtil.getParameter(request);
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");*/

        // Obtenemos la orden
        //Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        //messageList.setObject(ordenObj);
        //cargarInformacionFormato(request, messageList);

        //return new ModelAndView(getFormato());
        request.setAttribute("djCalificada", ConstantesCO.OPCION_SI);
        return cargarDeclaracionJuradaForm(request, response);//cargarInformacionOrden(request,response);
    }

    public ModelAndView aprobarTransmitirDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ.");

     	DeclaracionJurada dj = new DeclaracionJurada();
     	dj.setCalificacionUoId(new Long(request.getParameter("calificacionUoId").toString()));
        MessageList messageList = resolutorLogic.evaluadorDJCalificaApruebaTransmite(dj);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("djCalificada", ConstantesCO.OPCION_SI);
        return cargarDeclaracionJuradaForm(request, response);//cargarInformacionOrden(request,response);
    }

    public ModelAndView cargarDJRechazoForm(HttpServletRequest request, HttpServletResponse response) {
    	RequestUtil.setAttributes(request);

        return new ModelAndView(formDJRechazo);
    }
    
    public ModelAndView rechazarDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
     	DeclaracionJurada dj = new DeclaracionJurada();
     	dj.setCalificacionUoId(new Long(request.getParameter("calificacionUoId")));
     	dj.setDetalleDenegacion(request.getParameter("observacion"));
     	//dj.set

        MessageList messageList = resolutorLogic.evaluadorDJNoCalifica(dj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        // Obtenemos los datos basicos del formato
        /*HashUtil<String, String> elements = RequestUtil.getParameter(request);
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");*/

        // Obtenemos la orden
        //Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        /*messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);*/

        //return new ModelAndView(getFormato());
        return cargarInformacionOrden(request,response);
    }
    
    public ModelAndView rechazarTransmitirDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
     	DeclaracionJurada dj = new DeclaracionJurada();
     	dj.setCalificacionUoId(new Long(request.getParameter("calificacionUoId")));
     	dj.setDetalleDenegacion(request.getParameter("observacion"));
        MessageList messageList = resolutorLogic.evaluadorDJNoCalificaDeniegaTransmite(dj);

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        return cargarInformacionOrden(request,response);
    }    

    @SuppressWarnings("unchecked")
    public ModelAndView cargarModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String controller = datos.getString("controller");
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int modificacionDrId = datos.getInt("modificacionDrId");
        int suceId = datos.getInt("suceId");
        int mensajeId = datos.getInt("mensajeId");

        Suce suceObj = suceLogic.getSuceById(suceId);
        request.setAttribute("numSuce", suceObj.getNumSuce());
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

    	DR drObj = resolutorLogic.obtenerDRById(drId, sdr);
        request.setAttribute("vigente", drObj.getVigente());
        request.setAttribute("tipoDr", drObj.getTipoDr());

        ModificacionDR modificacionDrObj = suceLogic.loadModificacionDrById(drId, sdr, modificacionDrId, mensajeId);
        if (modificacionDrObj!=null) {

            request.setAttribute("modificacionDrId", modificacionDrId);
            request.setAttribute("mensaje", modificacionDrObj.getMensaje());
            request.setAttribute("mensajeId", modificacionDrObj.getMensajeId());
            request.setAttribute("transmitido", modificacionDrObj.getTransmitido());
            request.setAttribute("estadoRegistro", modificacionDrObj.getEstadoRegistro());
            request.setAttribute("tipoModificacionDR", modificacionDrObj.getTipoModificacionDR());

            String hide = modificacionDrObj.getTransmitido().equals("N")?"no":"yes";
            request.setAttribute("hide", hide);
        } else {
            request.setAttribute("transmitido", "N");
        }

        request.setAttribute("controller", controller);
        request.setAttribute("drId", drId);
        request.setAttribute("sdr", sdr);
        request.setAttribute("ordenId", datos.getInt("ordenId"));
        request.setAttribute("mto", datos.getInt("mto"));
        request.setAttribute("suceId", datos.getInt("suceId"));

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);
        request.setAttribute("formato", datos.getString("formato"));

        return new ModelAndView(modificacionDR);
    }

    public ModelAndView guardarModificacionDR(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        RequestUtil.setAttributes(request);

        Long drId = datos.getLong("drId");
        Integer sdr = datos.getInt("sdr");
        Long suceId = datos.getLong("suceId");
        //String formato = datos.getString("formato");
        String mensaje = datos.getString("mensaje");
        String esSubsanacion = "S";

        HashUtil<String, Object> element = suceLogic.crearModificacionDr(drId, sdr, mensaje, esSubsanacion);
        MessageList messageList = new MessageList();
        messageList = (MessageList)element.get("messageList");
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        cargarDatosModificacionDR(request, suceId, drId, sdr, element.getInt("modificacionDrId"), element.getInt("mensajeId"));

        return new ModelAndView(modificacionDR);
    }

    private void cargarDatosModificacionDR(HttpServletRequest request, Long suceId, Long drId, Integer sdr, Integer modificacionDrId, Integer mensajeId) {
    	request.setAttribute("mensajeId", mensajeId);
        if (modificacionDrId!=0){
            request.setAttribute("modificacionDrId", modificacionDrId);
        }

        ModificacionDR modificacionDRObj = suceLogic.loadModificacionDrById(drId, sdr, modificacionDrId, mensajeId);
        if(modificacionDRObj!=null){
        	request.setAttribute("estadoRegistro", modificacionDRObj.getEstadoRegistro());
        	request.setAttribute("transmitido", modificacionDRObj.getTransmitido());

        	DR drObj = resolutorLogic.obtenerDRById(drId, sdr);
            request.setAttribute("vigente", drObj.getVigente());
            request.setAttribute("tipoDr", drObj.getTipoDr());
        }

        Suce suceObj = suceLogic.getSuceById(suceId.intValue());
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);
        request.setAttribute("formato", request.getParameter("formato"));
    }

    public ModelAndView transmiteModificacionDRResolutor(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        RequestUtil.setAttributes(request);

        Long drId = datos.getLong("drId");
        Integer sdr = datos.getInt("sdr");
        Integer suceId = datos.getInt("suceId");
        Integer modificacionDrId = datos.getInt("modificacionDrId");
        Integer mensajeId = datos.getInt("mensajeId");
        //String formato = datos.getString("formato");
        String mensaje = datos.getString("mensaje");

        ModificacionDR modificacionDr = suceLogic.loadModificacionDrById(drId, sdr, modificacionDrId, mensajeId);
        suceLogic.transmiteModificacionDr(modificacionDr);

        //TODO : Verificar si esto debe descomentarse
        // Ahora aprobamos automaticamente con tasa CERO (0)
        //MessageList messageList = administracionEntidadLogic.aprobarModificacionDR(drId, sdr, modificacionDrId, 0);
        //request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        cargarDatosModificacionDR(request, new Long(suceId), drId, sdr, modificacionDrId, mensajeId);

        return new ModelAndView(modificacionDR);
    }

    public ModelAndView transmiteModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int modificacionDrId = datos.getInt("modificacionDrId");
        int mensajeId = datos.getInt("mensajeId");

        ModificacionDR modificacionDr = suceLogic.loadModificacionDrById(drId, sdr, modificacionDrId, mensajeId);
        MessageList messageList = suceLogic.transmiteModificacionDr(modificacionDr);
        modificacionDr = (ModificacionDR)messageList.getObject();

        RequestUtil.setAttributes(request);
        request.setAttribute("transmitido", modificacionDr.getTransmitido());
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);
        request.setAttribute("formato", datos.getString("formato"));

        return new ModelAndView(modificacionDR);
    }

	@SuppressWarnings("unchecked")
	public ModelAndView actualizaModificacionDR(HttpServletRequest request, HttpServletResponse response) {
	    HashUtil<String, String> datos = RequestUtil.getParameter(request);
	    //String formato = datos.getString("formato");

	    RequestUtil.setAttributes(request);
	    long drId = datos.getLong("drId");
	    int sdr = datos.getInt("sdr");
	    int modificacionDrId = datos.getInt("modificacionDrId");
	    int mensajeId = datos.getInt("mensajeId");
	    int suceId = datos.getInt("suceId");
        String mensaje = datos.getString("mensaje");

	    MessageList messageList = suceLogic.actualizaModificacionDr(mensajeId, mensaje);
	    request.setAttribute(Constantes.MESSAGE_LIST, messageList);

	    Suce suceObj = suceLogic.getSuceById(suceId);
	    request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

	    HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
	    filterADJ.put("mensajeId", mensajeId);
	    Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
	    request.setAttribute("tAdjuntos", tAdjuntos);

	    return new ModelAndView(modificacionDR);
	}

    public ModelAndView eliminarModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int mensajeId = datos.getInt("mensajeId");
        int modificacionDrId = datos.getInt("modificacionDrId");

        // Eliminar modificacion
        suceLogic.eliminaModificacionDr(drId, sdr, modificacionDrId, mensajeId);

        return this.cargarDatosDocumentoResolutivo(request, response);
    }

    public ModelAndView cargarArchivoModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int suceId = datos.getInt("suceId");
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int mensajeId = datos.getInt("mensajeId");

        MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                byte [] bytes = multipartFile.getBytes();
                String nombre = multipartFile.getOriginalFilename();

                messageList = formatoLogic.cargarArchivoDR(mensajeId, nombre, bytes);

            } catch (Exception e) {
                logger.error("ERROR: Al cargar el adjunto en el DR", e);
            }
        }
        RequestUtil.setAttributes(request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        Suce suceObj = suceLogic.getSuceById(suceId);
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);

        return new ModelAndView(modificacionDR);
    }

    public ModelAndView eliminarAdjuntoModificacionDR(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        int suceId = datos.getInt("suceId");
        long drId = datos.getLong("drId");
        int sdr = datos.getInt("sdr");
        int mensajeId = datos.getInt("mensajeId");

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            int id = Integer.parseInt(row.getCell("ADJUNTO_ID").getValue().toString());

            adjuntoLogic.eliminarAdjuntoById(id);
        }
        RequestUtil.setAttributes(request);

        Suce suceObj = suceLogic.getSuceById(suceId);
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("mensajeId", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.dr.modificacionDR.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);

        return new ModelAndView(modificacionDR);
    }

    public ModelAndView evaluadorActualizaDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "DJ.");
            HashUtil<String, String> elements = RequestUtil.getParameter(request);
            Map<String, Object> datProd = WebUtils.getParametersStartingWith(request, "USUARIO_DJ.");

            Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));

            long djId = elements.getLong("djId");
            MessageList messageList = null;


            messageList = this.resolutorLogic.actualizarDjXEvaluador(elements.getLong("djId"), datos.get("partidaSegunAcuerdo").toString(),
            														 Integer.parseInt(datos.get("secuenciaNorma").toString()),
            														 Integer.parseInt(datos.get("secuenciaOrigen").toString()));

            request.setAttribute("secuenciaMercancia", elements.get("secuenciaMercancia"));

            if ("1".equals(datos.get("tipoRolDj"))) {
            	DeclaracionJuradaProductor djProdObj = this.certificadoOrigenLogic.convertMapToDeclaracionJuradaProductor(datProd);
            	djProdObj.setCalificacionUoId(elements.getLong("calificacionUoId"));
            	this.certificadoOrigenLogic.insertDeclaracionJuradaProductor(djProdObj);
            }


            DeclaracionJurada djObj = this.certificadoOrigenLogic.getCODeclaracionJuradaById(elements.getLong("djId"));
            cargarMercanciaDeclaracionJurada(ordenObj, djObj, request);

            HashUtil<String, Object> cmbFilter = new HashUtil<String, Object>();
            cmbFilter.put("acuerdoId", elements.get("idAcuerdo"));

            request.setAttribute("cmbFilter", cmbFilter);
            request.setAttribute("idAcuerdo", elements.get("idAcuerdo"));
            request.setAttribute("coId", elements.get("coId"));
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
            request.setAttribute("formato", elements.get("formato"));
            request.setAttribute("enIngles", elements.get("idAcuerdo") );

            return new ModelAndView(formatoMercanciaDeclaracionJurada);
    }


    /**
     * Actualiza la información del Formato
     *
     * @param request
     * @param response
     * @return
     */
    public ModelAndView confirmarFinDJEval(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CERTIFICADO_ORIGEN.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Actualizamos la información
        messageList = this.resolutorLogic.confirmarFinDJEval(idFormatoEntidad, new Long(ordenObj.getSuce()));

        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);
        
    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);

        return new ModelAndView(getFormato());
    }

    //Duplicado Certificado Origen

    public ModelAndView actualizaDuplicadoCertificadoOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "MCT002.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");
        int formatoId = elements.getInt("idFormato");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Creamos un objeto detalle en base a lo que recibimos de request
        CODuplicado detalleObj = certificadoOrigenLogic.convertMapToCertificadoOrigenMCT002(datos);

        detalleObj.setOrden(orden);
        detalleObj.setMto(mto);
        detalleObj.setCoId(idFormatoEntidad);

        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateCertificadoOrigenDuplicado(detalleObj);

        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);

        return new ModelAndView(getDuplicadoCertificadoOrigen());
    }

  //Anulacion Certificado Origen

    public ModelAndView actualizaAnulacionCertificadoOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "MCT004.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");
        int formatoId = elements.getInt("idFormato");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Creamos un objeto detalle en base a lo que recibimos de request
        COAnulacion detalleObj = certificadoOrigenLogic.convertMapToCertificadoOrigenMCT004(datos);

        detalleObj.setOrden(orden);
        detalleObj.setMto(mto);
        detalleObj.setCoId(idFormatoEntidad);

        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateCertificadoOrigenAnulacion(detalleObj);

        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);

        return new ModelAndView(getAnulacionCertificadoOrigen());
    }


    /***********************************************************************/
    /** Impresiones **/
    /***********************************************************************/

    public ModelAndView actualizaReemplazoCertificadoOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CERTIFICADO_ORIGEN.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");
        int formatoId = elements.getInt("idFormato");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Creamos un objeto detalle en base a lo que recibimos de request
        COReemplazo detalleObj = certificadoOrigenLogic.convertMapToCertificadoOrigenMCT003(datos);

        detalleObj.setOrden(orden);
        detalleObj.setMto(mto);
        detalleObj.setCoId(idFormatoEntidad);
        detalleObj.setAcuerdoInternacionalId(ordenObj.getIdAcuerdo());

        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateCertificadoOrigenReemplazo(detalleObj);

        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);

        return new ModelAndView(getReemplazoCertificadoOrigen());
    }

    /**
     * Envia una Notificación de Subsanación
     * @param request
     * @param response
     * @return
     */
    public ModelAndView enviarNotificacionSubsanacionOrden(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = resolutorLogic.enviarNotificacionSubsanacionOrden(datos);
        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("ordenId"), datos.getInt("mto"));
        messageList.setObject(ordenObj);

        //request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        element.put(Constantes.MESSAGE_LIST, messageList);
        element.put("ordenId", datos.getLong("ordenId"));
        element.put("mto", datos.getInt("mto"));

        return this.cargarInformacionOrden(request, response, element);
    }

    /**
     * Elimina una Notificación de Subsanación
     * @param request
     * @param response
     * @return
     */
    public ModelAndView eliminarNotificacionSubsanacionOrden(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = resolutorLogic.eliminarNotificacionSubsanacionOrden(datos);
        Orden ordenObj = ordenLogic.getOrdenById(datos.getLong("ordenId"), datos.getInt("mto"));
        messageList.setObject(ordenObj);

        //request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        HashUtil<String, Object> element = new HashUtil<String, Object>();
        element.put(Constantes.MESSAGE_LIST, messageList);
        element.put("ordenId", datos.getLong("ordenId"));
        element.put("mto", datos.getInt("mto"));

        return this.cargarInformacionOrden(request, response, element);

    }

    public ModelAndView cargarNotifXSubsanacion(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        RequestUtil.setAttributes(request);

        Long ordenId = datos.getLong("orden");
        Integer mto = datos.getInt("mto");
        String formato = datos.getString("formato");

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        Table tNotificaciones = ibatisService.loadGrid("certificadoOrigen.solicitud.notificaciones.subsanacion", filter);
        request.setAttribute("tNotificaciones", tNotificaciones);

        return new ModelAndView(this.prevSubsanacionSolicitud);
    }

    public ModelAndView crearSubsanacionSolicitud(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        RequestUtil.setAttributes(request);

        Long ordenId = datos.getLong("orden");
        Integer mto = datos.getInt("mto");
        String formato = datos.getString("formato");

        HashUtil<String, Object> element = certificadoOrigenLogic.insertSubsanacionSolicitud(ordenId, mto);

        return this.cargarInformacionOrden(request, response, element);
    }

    public ModelAndView eliminarSubsanacionSolicitud(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        RequestUtil.setAttributes(request);

        Long ordenId = datos.getLong("orden");
        Integer mto = datos.getInt("mto");
        String formato = datos.getString("formato");
        HashUtil<String, Object> element = new HashUtil<String, Object>();

        try {
        	 element.put(Constantes.MESSAGE_LIST, certificadoOrigenLogic.deleteSubsanacionSolicitud(ordenId, mto));
             element.put("ordenId", ordenId);
             element.put("mto", certificadoOrigenLogic.mtoCertificadoVigente(ordenId));
             element.put("formato", formato);
		} catch (Exception e) {
			// TODO: handle exception
		}

        return this.cargarInformacionOrden(request, response, element);
    }

	/**
	 * Carga los Adjuntos de una factura de solicitud de certificado de origen-
	 * @param request Objeto request.
	 * @param response Objeto response.
	 * @return ModelAndView Vista de página
	 */
    public ModelAndView cargarAdjuntoFactura(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, Object> datos = RequestUtil.getParameter(request);
    	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;
    	Message message = null;
        MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {

                HashUtil<String, Object> filter = new HashUtil<String, Object>();
                filter.put("coId", datos.getLong("coId"));
                filter.put("secuenciaFactura", datos.getInt("secuenciaFactura"));
                filter.put("ordenId", datos.getLong("ordenId"));
                filter.put("mto", datos.getInt("mtoAdj"));
                filter.put("formatoId", datos.getInt("formatoId"));
                filter.put("adjuntoRequerido", null);
                filter.put("nombreArchivo", ComponenteOrigenUtil.replaceSpecialCharacters(multipartFile.getOriginalFilename()));
                filter.put("archivo", multipartFile.getBytes());
                filter.put("formato", datos.getString("formatoAdj"));

                messageList = formatoLogic.insertAdjuntoFactura(filter);
            } catch (Exception e) {
            	message = ComponenteOrigenUtil.getErrorMessage(e);
            	messageList.add(message);
                e.printStackTrace();
            }
        }

        // Llenamos todo lo que vino para mantener la información en request
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        RequestUtil.setAttributes(request);
        return cargarFormCOFactura(request, response, setearDataFactura(datos));
    }

    public ModelAndView eliminarAdjuntoFactura(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil datos = RequestUtil.getParameter(request);
    	Long coId = datos.getLong("CO_FACTURA.coId");
    	String formato = datos.getString("formato");
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        Long id = 0L;
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        MessageList messageList = null;
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            id = new Long(row.getCell("ADJUNTO_ID").getValue().toString());

        	logger.debug("Se eliminara adjunto_id = "+id);
        	try {
        		messageList = formatoLogic.eliminarAdjuntoFactura(id, coId, formato);
			} catch (Exception e) {
                e.printStackTrace();
			}

        }

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return cargarFormCOFactura(request, response, setearDataFactura(datos));
    }

    private HashUtil<String, Object> setearDataFactura(HashUtil<String, Object> datos){
    	HashUtil<String, Object> datosFactura = new HashUtil<String, Object>();
        datosFactura.put("orden", datos.getLong("ordenId"));
        datosFactura.put("mto", datos.getInt("mtoAdj"));
        datosFactura.put("secuencia", datos.getInt("secuenciaFactura"));
        datosFactura.put("formato", datos.getString("formatoAdj"));
        datosFactura.put("idFormato", datos.getString("formatoId"));
        datosFactura.put("idAcuerdo", datos.getString("idAcuerdoAdj"));
        datosFactura.put("bloqueado", datos.getString("bloqueadoAdj"));

        return datosFactura;
    }

    public ModelAndView cargarRegistroDJAutorizacionExp(HttpServletRequest request, HttpServletResponse response) {
    	request.setAttribute("djId", request.getParameter("djId"));
    	request.setAttribute("calificacionUoId", request.getParameter("calificacionUoId"));
    	request.setAttribute("idAcuerdo", request.getParameter("idAcuerdo"));
    	request.setAttribute("idEntidad", request.getParameter("idEntidad"));
    	request.setAttribute("iniVigencia", request.getParameter("iniVigencia"));
    	request.setAttribute("finVigencia", request.getParameter("finVigencia"));
    	request.setAttribute("modoProductorValidador", request.getParameter("modoProductorValidador"));
    	request.setAttribute("esDJRegistrada", request.getParameter("esDJRegistrada"));
    	request.setAttribute("tipoRol", request.getParameter("tipoRol"));

    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
    	request.setAttribute("usuTDoc", ConstantesCO.TIPO_DOCUMENTO_RUC);
    	request.setAttribute("usuNumDoc", usuario.getNumRUC());

        return new ModelAndView(this.formDJExportadorAut);
    }

    public ModelAndView registrarDJAutorizacionExp(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil datos = RequestUtil.getParameter(request);

    	try {

        	MessageList messageList = this.certificadoOrigenLogic.registraExportadorAutorizadoDj(datos.getLong("calificacionUoId"), datos.getString("documentoTipo"), datos.getString("numeroDocumento"),
        															   						 Util.getDateObject(datos.getString("fechaInicio")), Util.getDateObject(datos.getString("fechaFin")));

        	RequestUtil.setAttributes(request);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

    	} catch (Exception e) {
			// TODO: handle exception
		}

        return cargarRegistroDJAutorizacionExp(request, response);
    }



    public ModelAndView reevocarDJAutorizacionExp(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CALIFICACION.");
        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Creamos un objeto detalle en base a lo que recibimos de request
       // CalificacionOrigen detalleObj = certificadoOrigenLogic.convertMapToCalificacionOrigen(datos);
    	MessageList messageList = this.certificadoOrigenLogic.revocaExportadorAutorizadoDj(Util.longValueOf(datos.get("calificacionUoId")), elements.getString("usuarioEmpId"));
    	messageList.setObject(ordenObj);
        // Identificamos a donde redireccionar
        String paginaDestino = elements.getString("paginaDestino");

        // Si estamos en una mercancía, debemos refrescar como si estuvieramos en una grabación de la pantalla de mercancías, con su correspondiente refresco de variables.
        if ( ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino) ) {
        	// Actualizamos la información de mercancía
            /*CertificadoOrigenMercancia mercanciaObj = this.certificadoOrigenLogic.getCOMercanciaById(ordenObj.getIdFormatoEntidad(), elements.getInt("secuencia"), elements.getString("formato"));
            cargarCertificadoOrigenMercancia(ordenObj, mercanciaObj, request);
        	// Actualizamos la calificación sin datos generales
       		cargarInformacionCalificacionOrigen(ordenObj, detalleObj.getCalificacionUoId(), request, true);*/
            //request.setAttribute("esCalificacionOrigenStandAlone", "N");
        	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        	//NC return cargarCertificadoOrigenMercanciaForm(request, response);
        	//RequestUtil.setAttributes(request);
        	return cargarCertificadoOrigenMercanciaForm(request, response);
        } else { // Estamos en un mct005
             cargarInformacionFormato(request, messageList);
        	
        }

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return null;
    }

    public ModelAndView cargarHistoricoDJAutorizacionExp(HttpServletRequest request, HttpServletResponse response) {

    	HashUtil<String, Object> filtro = new HashUtil<String, Object>();
    	filtro.put("calificacionUoId", request.getParameter("calificacionUoId"));
    	filtro.put("empresaId", request.getParameter("usuarioEmpId"));
    	Table tDJHistoricoExp = ibatisService.loadGrid("calificacionOrigen.historico.exportador.autorizado.grilla", filtro);

        RequestUtil.setAttributes(request);
        request.setAttribute("tDJHistoricoExp", tDJHistoricoExp);

        request.setAttribute("numeroDocumento", request.getParameter("numeroDocumento"));
        request.setAttribute("razonSocial", request.getParameter("razonSocial"));

        return new ModelAndView(this.formDJHistoricoExportador);
    }

	/**
	 * Carga los Adjuntos de Declaración Jurada
	 * @param request
	 * @param response
	 * @return
	 */
    public ModelAndView cargarArchivoDeclaracionJuradaProductorForm(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long orden = datos.getLong("orden");
    	int mto = datos.getInt("mto");
    	Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

    	long calificacionUoId = datos.getLong("calificacionUoId");
    	int secuenciaProductor = datos.getInt("secuenciaProductor");

        int idFormato = ordenObj.getIdFormato();
    	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;

        MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                // Llenamos el map adjunto

                HashUtil<String, Object> adjData = new HashUtil<String, Object>();
                adjData.put("calificacionUoId", calificacionUoId);
                adjData.put("secuenciaProductor", secuenciaProductor);
                adjData.put("ordenId", orden);
                adjData.put("mto", mto);
                adjData.put("formatoId", idFormato);
                adjData.put("tipo", adjuntoTipo);
                adjData.put("nombre", ComponenteOrigenUtil.replaceSpecialCharacters(multipartFile.getOriginalFilename()));
                adjData.put("archivo", multipartFile.getBytes());

                messageList = formatoLogic.insertAdjuntoProductor(adjData);
                cargarAdjuntosDJProductor(request, adjData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Llenamos todo lo que vino para mantener la información en request
        RequestUtil.setAttributes(request);

		request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("exportador", datos.getString("exportador"));
        request.setAttribute("rolExportador", datos.getString("rolExportador"));

        request.setAttribute("djId", datos.get("djId"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        request.setAttribute("idAcuerdo", ordenObj.getIdAcuerdo());

        return new ModelAndView(formProductor);
    }


	public ModelAndView eliminarDeclaracionJuradaProductorForm(HttpServletRequest request, HttpServletResponse response) {
		HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long orden = datos.getLong("orden");
    	int mto = datos.getInt("mto");

    	HashUtil<String, Object> adjData = new HashUtil<String, Object>();
        adjData.put("calificacionUoId", datos.getLong("calificacionUoId"));
        adjData.put("secuenciaProductor", datos.getInt("secuenciaProductor"));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        MessageList messageList = new MessageList();

        if (deleteColumns.length > 0) {
        	HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));
            Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
            Row row;
            for(int i = 0; i< deleteColumns.length; i++ ){
                row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);

                try {
                	this.formatoLogic.eliminarAdjuntoProductor(adjData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Message message = new Message("delete.success");
            messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        }

        // Llenamos todo lo que vino para mantener la información en request
        RequestUtil.setAttributes(request);

        request.setAttribute("exportador", datos.getString("exportador"));
        request.setAttribute("rolExportador", datos.getString("rolExportador"));

        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        request.setAttribute("djId", datos.get("djId"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        request.setAttribute("idAcuerdo", ordenObj.getIdAcuerdo());
        cargarAdjuntosDJProductor(request, adjData);

        return new ModelAndView(formProductor);
	}

    private void cargarAdjuntosDJProductor(HttpServletRequest request,HashUtil<String, Object> adjFilter) {
    	Table tAdjDJProductor = ibatisService.loadGrid("certificadoOrigen.orden.dj.productor.adjuntos.grilla", adjFilter);
        request.setAttribute("tAdjDJProductor", tAdjDJProductor);
    	request.setAttribute("numAdjDJProductor", (tAdjDJProductor != null ? tAdjDJProductor.size(): 0));
    }

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

    /**
     * Actualiza el Rol de Calificación de Origen
     * @param request Objeto request.
     * @param response Objeto response.
     * @return ModelAndView
     */
    public ModelAndView actualizaRolCalificacionOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CALIFICACION.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Creamos un objeto detalle en base a lo que recibimos de request
        CalificacionOrigen detalleObj = certificadoOrigenLogic.convertMapToCalificacionOrigen(datos);

        // Asignamos el id de calificación que viene de la página
        detalleObj.setCalificacionUoId(Long.valueOf((String) datos.get("calificacionUoId")));

        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateRolCalificacionOrigen(detalleObj);

        messageList.setObject(ordenObj);

        // Identificamos a donde redireccionar
        String paginaDestino = elements.getString("paginaDestino");
        String esCalificacionOrigenStandAlone = elements.getString("esCalificacionOrigenStandAlone");

        // Si estamos en una mercancía, debemos refrescar como si estuvieramos en una grabación de la pantalla de mercancías, con su correspondiente refresco de variables.
        if ( ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino) ) {
        	if (!ConstantesCO.OPCION_SI.equals(esCalificacionOrigenStandAlone)){
	        	// Actualizamos la información de mercancía
	            CertificadoOrigenMercancia mercanciaObj = this.certificadoOrigenLogic.getCOMercanciaById(ordenObj.getIdFormatoEntidad(), elements.getInt("secuencia"), elements.getString("formato"));
	            cargarCertificadoOrigenMercancia(ordenObj, mercanciaObj, request);
        	}
        	// Actualizamos la calificación sin datos generales
       		cargarInformacionCalificacionOrigen(ordenObj, detalleObj.getCalificacionUoId(), request, true);
        } else { // Estamos en un mct005
            cargarInformacionFormato(request, messageList);
        }

        if (!ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(request.getParameter("formato"))) {
        	verificarCalifDJCompleta(detalleObj.getCalificacionUoId(), messageList, request, elements.getInt("idAcuerdo"), ordenObj.getOrden());
        }

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        ModelAndView destino = new ModelAndView(ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino) ? this.formatoMercancia : getCalificacionAnticipadaCertificadoOrigen());

        if (ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino)) {
            request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(Integer.valueOf(request.getParameter("idAcuerdo"))) );
        }
        
        return destino;
    }

    /**
     * Actualiza el Criterio de Calificación de Origen
     * @param request Objeto request.
     * @param response Objeto response.
     * @return ModelAndView
     */
    public ModelAndView actualizaCriterioCalificacionOrigen(HttpServletRequest request,HttpServletResponse response) {

        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        Map<String, Object> datos = WebUtils.getParametersStartingWith(request, "CALIFICACION.");

        MessageList messageList = null;

        // Obtenemos los datos basicos del formato
        long orden = elements.getLong("orden");
        int mto = elements.getInt("mto");
        long idFormatoEntidad = elements.getLong("idFormatoEntidad");

        // Obtenemos la orden
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Creamos un objeto detalle en base a lo que recibimos de request
        CalificacionOrigen detalleObj = certificadoOrigenLogic.convertMapToCalificacionOrigen(datos);

        // Asignamos el id de calificación que viene de la página
        detalleObj.setCalificacionUoId(Long.valueOf((String) datos.get("calificacionUoId")));

        // Actualizamos la información
        messageList = certificadoOrigenLogic.updateCriterioCalificacionOrigen(detalleObj);

        messageList.setObject(ordenObj);

        // Identificamos a donde redireccionar
        String paginaDestino = elements.getString("paginaDestino");

        // Si estamos en una mercancía, debemos refrescar como si estuvieramos en una grabación de la pantalla de mercancías, con su correspondiente refresco de variables.
        if ( ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino) ) {
        	// Actualizamos la información de mercancía
            CertificadoOrigenMercancia mercanciaObj = this.certificadoOrigenLogic.getCOMercanciaById(ordenObj.getIdFormatoEntidad(), elements.getInt("secuencia"), elements.getString("formato"));
            cargarCertificadoOrigenMercancia(ordenObj, mercanciaObj, request);
        	// Actualizamos la calificación sin datos generales
       		cargarInformacionCalificacionOrigen(ordenObj, detalleObj.getCalificacionUoId(), request, true);
        } else { // Estamos en un mct005
            cargarInformacionFormato(request, messageList);
        }

        if (!ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(request.getParameter("formato"))) {
        	verificarCalifDJCompleta(detalleObj.getCalificacionUoId(), messageList, request, elements.getInt("idAcuerdo"), ordenObj.getOrden());
        }
        
        Integer idAcuerdo = new Integer(request.getParameter("idAcuerdo"));

        if ( ConstantesCO.AC_SGPC.equals(idAcuerdo) || ConstantesCO.AC_SGP_UE.equals(idAcuerdo) || 
        	 ConstantesCO.AC_COREA.equals(idAcuerdo) || ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo) ||
        	 ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)){
        	request.setAttribute("lblSufijo", "."+idAcuerdo.toString());
        } else {
        	request.setAttribute("lblSufijo", "");
        }

        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        // Seleccionamos el destino correcto, entre la página de mercancía o el formato mct005
        ModelAndView destino = new ModelAndView(ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino) ? this.formatoMercancia : getCalificacionAnticipadaCertificadoOrigen());

        if (ConstantesCO.CO_CALIFICACION_PAGINA_DESTINO_MERCANCIA.equals(paginaDestino)) {
            request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(idAcuerdo) );
        }
        
        return destino;
    }

    public ModelAndView grabarCalificacionOrigenMercanciaDeclaracion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> elements = RequestUtil.getParameter(request);

        Orden ordenObj = ordenLogic.getOrdenById(elements.getLong("orden"), elements.getInt("mto"));

        /*
        idEntidadCertificadora=16,
        		idAcuerdo=4
        		idPais=43,
        */
        Integer idAcuerdo = elements.getInt("idAcuerdo");

   		CalificacionOrigen calificacionOrigen = new CalificacionOrigen();
   		calificacionOrigen.setCoId(ordenObj.getIdFormatoEntidad());
   		calificacionOrigen.setOrdenIdInicial(ordenObj.getOrden());
   		calificacionOrigen.setMtoInicial(ordenObj.getMto());
   		calificacionOrigen.setEntidadId(elements.getInt("idEntidadCertificadora"));
   		calificacionOrigen.setAcuerdoInternacionalId(idAcuerdo);
   		calificacionOrigen.setPaisIsoIdxAcuerdoInt(elements.getInt("idPais"));

   		MessageList messageList = certificadoOrigenLogic.insertCalificacionOrigenConMercanciaDeclaracionJurada(calificacionOrigen);

   		cargarInformacionCalificacionOrigen(ordenObj, calificacionOrigen.getCalificacionUoId(), request, true);

        verificarCalifDJCompleta(calificacionOrigen.getCalificacionUoId(), messageList, request, elements.getInt("idAcuerdo"), ordenObj.getOrden());
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        if (calificacionOrigen.getSecuenciaMercancia() != null) {
        	request.setAttribute("secuencia", calificacionOrigen.getSecuenciaMercancia());
        }
        if (calificacionOrigen.getDjId() != null) {
        	request.setAttribute("djId", calificacionOrigen.getDjId());
        }

        if ( ConstantesCO.AC_SGPC.equals(idAcuerdo) || ConstantesCO.AC_SGP_UE.equals(idAcuerdo)
        	 || ConstantesCO.AC_COREA.equals(idAcuerdo) || ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)
        	 ||  ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)){
        	request.setAttribute("lblSufijo", "."+request.getParameter("idAcuerdo"));
        } else {
        	request.setAttribute("lblSufijo", "");
        }

        request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());

        //Filtro para las facturas
        HashUtil filterFacturas = new HashUtil();
        filterFacturas.put("coId", ordenObj.getIdFormatoEntidad());
        request.setAttribute("filterFacturas", filterFacturas);
        request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(idAcuerdo) );
        
        return new ModelAndView(this.formatoMercancia);
    }

    public ModelAndView enlazaDJCalificacionMercancia(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
    	filter.put("coId", new Long(request.getParameter("coId")));
    	filter.put("calificacionUoId", new Long(request.getParameter("CALIFICACION.calificacionUoId")));
    	filter.put("ordenId", new Long(request.getParameter("orden")));
    	filter.put("mto", new Integer(request.getParameter("mto")));
    	
    	MessageList messageList = null;
    	try {
    	    messageList = this.certificadoOrigenLogic.enlazaDJCalificacionMercancia(filter);
        } catch (Exception e) {
        	Message message = ComponenteOrigenUtil.getErrorMessage(e);
        	if (messageList==null) {
        		messageList = new MessageList();
        	}
        	messageList.add(message);
            logger.error("Error en CertificadoOrigenLogicImpl.enlazaDJCalificacionMercancia", e);
    	}
    	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
    	
    	if (!messageList.containsError()) {
        	HashUtil<String, Object> res = (HashUtil<String, Object>) messageList.getObject();
        	request.setAttribute("secuencia", res.get("secuenciaMercancia"));
       	}
        
    	return cargarCertificadoOrigenMercanciaForm(request, response);
    }

    public ModelAndView copiarDJCalificacionMercancia(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();

    	filter.put("coId", new Long(request.getParameter("coId")));
    	filter.put("calificacionUoId", new Long(request.getParameter("CALIFICACION.calificacionUoId")));
    	filter.put("ordenId", new Long(request.getParameter("orden")));
    	filter.put("mto", new Integer(request.getParameter("mto")));
        
    	MessageList messageList = null;
    	try {
    	    messageList = this.certificadoOrigenLogic.copiarDJCalificacionMercancia(filter);
        } catch (Exception e) {
        	Message message = ComponenteOrigenUtil.getErrorMessage(e);
        	if (messageList==null) {
        		messageList = new MessageList();
        	}
        	messageList.add(message);
            logger.error("Error en CertificadoOrigenLogicImpl.copiarDJCalificacionMercancia", e);
    	}
    	
    	if (!messageList.containsError()) {
        	HashUtil<String, Object> res = (HashUtil<String, Object>) messageList.getObject();
        	request.setAttribute("secuencia", res.get("secuenciaMercancia"));
       	}
        
    	return cargarCertificadoOrigenMercanciaForm(request, response);
    }

    public ModelAndView cargarPaisesXComponente(HttpServletRequest request, HttpServletResponse response) {

    	String idAcuerdo = request.getParameter("idAcuerdo");
    	HashUtil<String, Object> filtro = new HashUtil<String, Object>();
    	filtro.put("idAcuerdo", idAcuerdo);

    	String key = "comun.pais_beneficiario_x_acuerdo_excluyendo_acuerdo.select";
    	if (ConstantesCO.AC_TLC_UE.toString().trim().equals(idAcuerdo)) {
    		filtro.put("idPaisExcluido", ConstantesCO.COD_PAIS_COLOMBIA);
    	}

    	if (ConstantesCO.AC_SGPC.toString().trim().equals(idAcuerdo)) {
    		filtro.put("idPaisExcluido", ConstantesCO.COD_PAIS_COLOMBIA);
    		key = "comun.pais_iso_x_acuerdo.select";
    	}

        request.setAttribute("filterG2", filtro);
        request.setAttribute("key", key);

        return new ModelAndView(this.listadoPaisesG2);
    }

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/


    public ModelAndView cargarFormProductoDj(HttpServletRequest request, HttpServletResponse response ){
    	String enIngles =  certificadoOrigenLogic.obtenerIdiomaLlenado(Integer.valueOf(request.getParameter("idAcuerdo")));
    	DeclaracionJurada dj = certificadoOrigenLogic.getDjByCalificacionUoId(new Long(request.getParameter("calificacionUoId")),enIngles);
    	RequestUtil.setAttributes(request, "PRODUCTO.", dj);
    	
        return new ModelAndView(formProductoDj);
    }

    public String getFormFactura() {
        return formFactura;
    }

    public void setFormFactura(String formFactura) {
        this.formFactura = formFactura;
    }

    public void setFormatoMercancia(String formatoMercancia) {
        this.formatoMercancia = formatoMercancia;
    }

    public void setFormMaterial(String formMaterial) {
        this.formMaterial = formMaterial;
    }

    public void setFormatoMercanciaDeclaracionJurada(
            String formatoMercanciaDeclaracionJurada) {
        this.formatoMercanciaDeclaracionJurada = formatoMercanciaDeclaracionJurada;
    }

    public void setFormatoBusqDj(String formatoBusqDj) {
		this.formatoBusqDj = formatoBusqDj;
	}

    public void setFormProductor(String formProductor) {
		this.formProductor = formProductor;
	}

    public void setAdjuntosDJ(String adjuntosDJ) {
		this.adjuntosDJ = adjuntosDJ;
	}

    public void setAdjuntosMaterialDJ(String adjuntosMaterialDJ) {
		this.adjuntosMaterialDJ = adjuntosMaterialDJ;
	}

	public void setModificacionDR(String modificacionDR) {
		this.modificacionDR = modificacionDR;
	}

	public void setCalificarValidacionDj(String calificarValidacionDj) {
		this.calificarValidacionDj = calificarValidacionDj;
	}

	public void setFormDJExportadorAut(String formDJExportadorAut) {
		this.formDJExportadorAut = formDJExportadorAut;
	}

	public void setFormDJHistoricoExportador(String formDJHistoricoExportador) {
		this.formDJHistoricoExportador = formDJHistoricoExportador;
	}

	public void setFormDJRechazo(String formDJRechazo) {
		this.formDJRechazo = formDJRechazo;
	}
	
	public void setPrevSubsanacionSolicitud(String prevSubsanacionSolicitud) {
		this.prevSubsanacionSolicitud = prevSubsanacionSolicitud;
	}

	public void setFormProductoDj(String formProductoDj) {
		this.formProductoDj = formProductoDj;
	}

	public void setListadoPaisesG2(String listadoPaisesG2) {
		this.listadoPaisesG2 = listadoPaisesG2;
	}
	
	//20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
    protected String validaAcuerdoPais (Long ordenId){
    	String res="";
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("result", null);
		filter.put("ordenId", ordenId);
    	try {
			ibatisService.executeSPWithObject("valida_acuerdo_pais_vigente",filter);
			if(filter.get("result")!=null) {
				res= filter.get("result").toString();
			}
		} catch (Exception e) {
			logger.error("Error en CertificadoOrigneController.validaAcuerdoPais", e);
			
		}
       	return res;
    }

}