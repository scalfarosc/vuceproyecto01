package pe.gob.mincetur.vuce.co.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class CaptchaController extends MultiActionController {

	private ImageCaptchaService captchaService;

	public void setCaptchaService(ImageCaptchaService imageCaptchaService) {
		this.captchaService = imageCaptchaService;
	}

	public ModelAndView generarCodigo(HttpServletRequest request, HttpServletResponse response) {
		byte[] captchaChallengeAsJpeg = null;
		// the output stream to render the captcha image as jpeg into
		ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
		try {
			// get the session id that will identify the generated captcha.
			// the same id must be used to validate the response, the session id is a good candidate!
			// Se consulta por parametro captchaId sino, se genera segun sessionId de usuario
			String captchaId = request.getParameter("captchaId") != null ? request.getParameter("captchaId"): request.getSession().getId();
			// call the ImageCaptchaService getChallenge method
			BufferedImage challenge = captchaService.getImageChallengeForID(captchaId, request.getLocale());

			// a jpeg encoder
			JPEGImageEncoder jpegEncoder = JPEGCodec.createJPEGEncoder(jpegOutputStream);
			jpegEncoder.encode(challenge);
		} catch (IllegalArgumentException iae) {
			logger.error("Error en CaptchaController.generarCodigo iae:", iae);
			// response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		} catch (CaptchaServiceException cse) {
			logger.error("Error en CaptchaController.generarCodigo cse:", cse);
			// response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		} catch (Exception exc) {
			logger.error("Error en CaptchaController.generarCodigo exc:", exc);
			// response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}

		captchaChallengeAsJpeg = jpegOutputStream.toByteArray();

		// flush it in the response
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/jpeg");
		try {
			ServletOutputStream responseOutputStream = response.getOutputStream();
			responseOutputStream.write(captchaChallengeAsJpeg);
			responseOutputStream.flush();
			responseOutputStream.close();
		} catch (Exception exc2) {
			logger.error("Error en CaptchaController.generarCodigo exc2:", exc2);
		}
		return null;
	}
}
