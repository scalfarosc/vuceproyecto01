package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;
import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;

public class DJButtonValidator extends FormatoButtonValidator {

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        Object id = request.getAttribute("djId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String permiteProductor = (String)request.getAttribute("mostrarBotonProductor");
        String exportador = (String)request.getAttribute("exportador");
        String estadoRegistroCO = (String) request.getAttribute("estadoRegistroCO");
        String modoProductorValidador = (String) request.getAttribute("modoProductorValidador");
        String esValidador = (String) request.getAttribute("esValidador");
        //Integer tipoRol = (Integer) request.getAttribute("tipoRol");
        String permiteValidar = (String)request.getAttribute("permiteValidar");
        String vigente  = (request.getAttribute("ordenVigente") != null? (String) request.getAttribute("ordenVigente"):"");
        String suceFlgPendienteCalif = request.getAttribute("suceFlgPendienteCalif") != null ? (String) request.getAttribute("suceFlgPendienteCalif"): ConstantesCO.OPCION_NO;
        //String puedeTransmitir = request.getAttribute("puedeTransmitir") != null? request.getAttribute("puedeTransmitir").toString():ConstantesCO.OPCION_NO;
        //int numNotif = request.getAttribute("numNotif") != null ? Integer.parseInt(request.getAttribute("numNotif").toString()) : 0;
        
      //  //20170109_GBT ACTA CO-004-16
      //  Integer idAcuerdo = (Integer)request.getAttribute("idAcuerdo");

        if (bloqueado==null) bloqueado = "";
        if (permiteValidar==null) permiteValidar = "";
        if (permiteProductor==null) permiteProductor ="";

        // Si aun no se ha grabado
        String estadoDj = ( request.getAttribute("DJ.estadoRegistro") != null ? request.getAttribute("DJ.estadoRegistro").toString() : "" );

        if (id!= null && "0".equals(id.toString())) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("salirButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("transmitirDjButton", Constantes.DISABLING_ACTION_HIDE);
        } else { // Si se ha grabado
        	if (ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(estadoDj) || ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(estadoDj)) {
                //disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
        	} else if (ConstantesCO.DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO.equals(estadoDj)) {
        		disabledButtons.put("nuevoProductorButton", Constantes.DISABLING_ACTION_HIDE);
                //disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
        	}
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);
                //disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);

                /*
                 * Comentado por los cambios de calificacion

                if ((!ConstantesCO.CO_ESTADO_SUBSANACION.equals(estadoRegistroCO)
                	 && (ConstantesCO.OPCION_NO.equals(vigente) && !ConstantesCO.CO_ESTADO_PENDIENTE_ENVIO_A_ENTIDAD.equals(estadoRegistroCO) ) ) ||
                	tipoRol != Integer.parseInt(ConstantesCO.TIPO_ROL_EXPORTADOR) ||
                	!ConstantesCO.OPCION_NO.equals(vigente) ){
                	disabledButtons.put("nuevoProductorButton", Constantes.DISABLING_ACTION_DISABLE);
                }
				*/
            }
            // Si la funci�n de validaci�n dice que no se permite m�s de un productor
        	if ("N".equals(permiteProductor)) {
        		disabledButtons.put("nuevoProductorButton", Constantes.DISABLING_ACTION_HIDE);
        	}

            Object datDj = request.getAttribute("dj.dj");
            if (datDj != null || "S".equalsIgnoreCase(bloqueado)) { // El bloqueo es indistinto entre exportadores y evaluadores
                disabledButtons.put("nuevoMaterialPeruButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("nuevoMaterialSegundoComponenteButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("nuevoMaterialTercerComponenteButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("nuevoMaterialNoOriginarioButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("grabarConsolidadoButton", Constantes.DISABLING_ACTION_HIDE);
            }

            UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
            // Si es un usuario evaluador, debe manejarse la aparici�n de ciertos botones
            if (Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) {
                //disabledButtons.remove("grabarButton");
            	if (datDj != null || ConstantesCO.OPCION_NO.equalsIgnoreCase(suceFlgPendienteCalif)) {
                //if (datDj != null || ( !"O".equals(estadoRegistroCO) && !"S".equals(estadoRegistroCO))) {
            	//if (datDj != null || ( !"O".equals(estadoRegistroCO) && !"S".equals(estadoRegistroCO)) || (numNotif > 0 && ConstantesCO.OPCION_NO.equals(puedeTransmitir))) {
                    disabledButtons.put("calificaButton", Constantes.DISABLING_ACTION_HIDE);
                    disabledButtons.put("noCalificaButton", Constantes.DISABLING_ACTION_HIDE);
                    disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
                } else {
                	String calificacion = (String) request.getAttribute("calificacion");//( request.getAttribute("DJ.calificacion") != null ? request.getAttribute("DJ.calificacion").toString() : "" );
                	if ("A".equalsIgnoreCase(calificacion)){
                		disabledButtons.put("calificaButton", Constantes.DISABLING_ACTION_HIDE);
                	} else if ("R".equalsIgnoreCase(calificacion)){
                		disabledButtons.put("noCalificaButton", Constantes.DISABLING_ACTION_HIDE);
                	}
                }

                /*if (ConstantesCO.DJ_ESTADO_PENDIENTE_RESPUESTA_ENTIDAD.equals(estadoDj)) {
                	if (disabledButtons.get("grabarButton") != null ){
                		disabledButtons.remove("grabarButton");
                	}
                }*/

                // Si entro un evaluador, estos botones nunca deben existir
                //disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("nuevoProductorButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("transmitirDjButton", Constantes.DISABLING_ACTION_HIDE);
            } else { // Si es un usuario exportador (o productor)
                // Si ha entrado el productor validador, se deben manejar los botones
                if (ConstantesCO.OPCION_SI.equals(modoProductorValidador)) {
                    //disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
            		disabledButtons.put("nuevoProductorButton", Constantes.DISABLING_ACTION_HIDE);
            		if (ConstantesCO.OPCION_NO.equals(esValidador) || ConstantesCO.DJ_ESTADO_APROBADA.equals(estadoDj)) { // Si el usuario no es validador, ocultamos el bot�n de transmitir
                    	disabledButtons.put("transmitirDjButton", Constantes.DISABLING_ACTION_HIDE);
            		}
                } else {
                	disabledButtons.put("transmitirDjButton", Constantes.DISABLING_ACTION_HIDE);
                }

                /*
                 * Comentado por el cambio a calificacion

                if (tipoRol == Integer.parseInt(ConstantesCO.TIPO_ROL_PRODUCTOR_EXPORTADOR) || tipoRol == Integer.parseInt(ConstantesCO.TIPO_ROL_PRODUCTOR) ) { // Si es productor/exportador desaparecemos el bot�n de solicitar validaci�n
                    //disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
                } else { // Si es exportador solo, tenemos que verificar si ya se ingresaron productores
                	Integer numeroProductores = (Integer) request.getAttribute("numeroProductores");
                	if (numeroProductores.intValue() == 0 || !permiteValidar.equals("S")) { // Si no tiene productores, o ninguno es para validar quitamos el boton
                		//disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
                	}
                }
                */

                // Si entro un exportador (o productor), estos botones nunca deben existir
        		disabledButtons.put("calificaButton", Constantes.DISABLING_ACTION_HIDE);
        		disabledButtons.put("noCalificaButton", Constantes.DISABLING_ACTION_HIDE);

        		if ("N".equals(request.getAttribute("puedeTransmitirDJValidada").toString().trim())) {
        			disabledButtons.put("transmitirDjButton", Constantes.DISABLING_ACTION_HIDE);
        		}
            }


        }

        String mostrarBtnValidacionProd = (String) request.getAttribute("mostrarBtnValidacionProd");
        if ("0".equals(mostrarBtnValidacionProd)) {
        	disabledButtons.put("solicitarValidacionButton", Constantes.DISABLING_ACTION_HIDE);
        }
      
        ////20170113_GBT ACTA CO-004-16 Y CO-009-16
        //
        //       	if (idAcuerdo==24 || idAcuerdo==17) {
        //      		disabledButtons.put("calificaButton", Constantes.DISABLING_ACTION_DISABLE);
        //     		disabledButtons.put("noCalificaButton", Constantes.DISABLING_ACTION_DISABLE);
        //    }      
        //   //FIN DE CAMBIO GBT
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        
        return buttonSet;
    }

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formMaterial(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        Object id = request.getAttribute("DJ_MATERIAL.secuenciaMaterial");
        String bloqueado = (String)request.getAttribute("bloqueado");
        if (bloqueado == null) bloqueado = "";

        if (id == null) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("cargarButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("salirButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);

                disabledButtons.put("cargarButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        }

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formDJProductor(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        Object id = request.getAttribute("secuenciaProductor");
        String bloqueado = (String)request.getAttribute("bloqueado");
        int numAdj = Integer.parseInt(request.getAttribute("numAdjDJProductor").toString());

        if (bloqueado == null) bloqueado = "";

        /*if (ConstantesCO.TIPO_ROL_PRODUCTOR_EXPORTADOR.equals(request.getAttribute("rolExportador").toString()) &&
        	request.getAttribute("DJ_PRODUCTOR.documentoTipo").toString().equals(request.getAttribute("tipoDocumentoSolicitante").toString()) &&
        	request.getAttribute("DJ_PRODUCTOR.numeroDocumento").toString().equals(request.getAttribute("numDocumentoSolicitante").toString())) {
        	bloqueado = "S";
        }*/

        if (id == null) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("salirButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
            if (bloqueado.equalsIgnoreCase(ConstantesCO.OPCION_SI)) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);

                disabledButtons.put("cargarAdjButton", Constantes.DISABLING_ACTION_HIDE);
            	disabledButtons.put("eliminarAdjButton", Constantes.DISABLING_ACTION_HIDE);
            } else {

                if (numAdj > 0) {
                	disabledButtons.put("cargarAdjButton", Constantes.DISABLING_ACTION_HIDE);
                } else {
                	disabledButtons.put("eliminarAdjButton", Constantes.DISABLING_ACTION_HIDE);
                }
            }
        }

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formDJAdjunto(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        String bloqueado = (String)request.getAttribute("bloqueado");

        if (bloqueado == null) bloqueado = "";

        if (bloqueado.equalsIgnoreCase("S")) {
            disabledButtons.put("cargarButton", Constantes.DISABLING_ACTION_DISABLE);
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

}
