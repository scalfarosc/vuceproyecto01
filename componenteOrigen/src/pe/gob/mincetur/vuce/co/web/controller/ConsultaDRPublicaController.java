package pe.gob.mincetur.vuce.co.web.controller;

import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.config.MessageConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.logic.AdjuntoLogic;

public class ConsultaDRPublicaController extends MultiActionController {
	private static final String FORMATO_FECHA_ES = "dd/MM/yyyy HH:mm:ss";
	private static final String FORMATO_FECHA_EN = "MM/dd/yyyy HH:mm:ss";
	private String consultaDRPub;
	private IbatisService ibatisService;
	private AdjuntoLogic adjuntoLogic;
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);

	public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public void setConsultaDRPub(String consultaDRPub) {
		this.consultaDRPub = consultaDRPub;
	}

	public void setAdjuntoLogic(AdjuntoLogic adjuntoLogic) {
		this.adjuntoLogic = adjuntoLogic;
	}

	/**
	 * Consulta DR de acceso p�blico
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView inicioConsultaDR(HttpServletRequest request, HttpServletResponse response) {
		// Idioma predeterminado: espannol
		Date fecha = (Date) ibatisService.loadElement("comun.fechaActual", null).get("FECHA");
		String fechaHoraActual = Util.getDate(fecha, FORMATO_FECHA_ES);
		String captchaId = Long.toHexString(new Random().nextLong());
		request.setAttribute("random", Math.random());
		request.setAttribute("formatoFecha", "dd/MM/yyyy");
		request.setAttribute("formatoCalendario", "%d/%m/%Y");
		request.setAttribute("idioma", "es");
		request.setAttribute("fechaActual", Util.getDate(fecha, "yyyyMMdd"));
		request.setAttribute("fechaHoraActual", fechaHoraActual);
		request.setAttribute("captchaId", captchaId);
		return new ModelAndView(consultaDRPub);
	}

	/**
	 * Permite cambiar de idioma de Consulta DR
	 **/
	public ModelAndView cambiarIdioma(HttpServletRequest request, HttpServletResponse response) {
		String idioma = request.getParameter("idioma");
		Date fecha = (Date) ibatisService.loadElement("comun.fechaActual", null).get("FECHA");
		String fechaHoraActual = Util.getDate(fecha, idioma.equals("en") ? FORMATO_FECHA_EN : FORMATO_FECHA_ES);
		request.setAttribute("opcionFiltro", request.getParameter("opcionFiltro"));
		request.setAttribute("drEntidad", request.getParameter("drEntidad"));
		request.setAttribute("nroReferencia", request.getParameter("nroReferencia"));
		request.setAttribute("filtroPais", request.getParameter("filtroPais"));
		//request.setAttribute("filtroAcuerdo", request.getParameter("filtroAcuerdo"));
		
		// Fecha (fechaDrEntidad)
		try {
			if (Util.getDateObject(request.getParameter("fechaDrEntidad"), idioma.equals("en") ? "MM/dd/yyyy" : "dd/MM/yyyy") != null) {
				request.setAttribute("fechaDrEntidad", request.getParameter("fechaDrEntidad"));
			}
		} catch (Exception ignore) {
		}
		request.setAttribute("random", Math.random());
		request.setAttribute("formatoFecha", "en".equals(request.getParameter("idioma")) ? "MM/dd/yyyy" : "dd/MM/yyyy");
		request.setAttribute("formatoCalendario", "en".equals(request.getParameter("idioma")) ? "%m/%d/%Y" : "%d/%m/%Y");
		request.setAttribute("idioma", idioma);
		request.setAttribute("fechaHoraActual", fechaHoraActual);
		request.setAttribute("fechaActual", Util.getDate(fecha, "yyyyMMdd"));
		return new ModelAndView(consultaDRPub);
	}

	/**
	 * Consulta de DR, de acceso p�blico
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView consultarDR(HttpServletRequest request, HttpServletResponse response) {
		HashUtil<String, Object> filterDR = new HashUtil<String, Object>();
		HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
		//UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
		String drEntidad = request.getParameter("drEntidad");
		String pais = request.getParameter("filtroPais");
		//String acuerdo = request.getParameter("filtroAcuerdo");
		Date fechaDrEntidad = null;
		int permiteConsultaDr = -1;
		Message message = null;
		MessageList messageList = null;
		String codigoError = null;
		String formatoFecha = null;
		String idioma = request.getParameter("idioma");
		Date fecha = (Date) ibatisService.loadElement("comun.fechaActual", null).get("FECHA");
		String fechaHoraActual = Util.getDate(fecha, idioma.equals("en") ? FORMATO_FECHA_EN : FORMATO_FECHA_ES);
		try {
			formatoFecha = request.getParameter("idioma").equals("en") ? "MM/dd/yyyy": "dd/MM/yyyy";
			try {
				fechaDrEntidad = Util.getDateObject(request.getParameter("fechaDrEntidad"), formatoFecha);	
			} catch (Exception excfecha) {
				logger.error("Error en ConsultaDRController.consultarDR: excfecha:", excfecha);
				message = new ErrorMessage();
				message.setMessage(MessageConfig.getMessageConfig().getProperty("consulta_dr.error.formato_fecha", request.getParameter("idioma")));
			}
			
			if (fechaDrEntidad != null) {
				// 1. Validacion SP: proceso_certificado.permite_consulta_dr_publica
				permiteConsultaDr = this.permiteConsultaDRPublica(drEntidad, fechaDrEntidad, Long.valueOf(pais));
                
				if (permiteConsultaDr == 1) {
					// 2. Consultar DR Principal
					filterDR.put("drEntidad", drEntidad);
					filterDR.put("fechaDrEntidad", Util.getDate(fechaDrEntidad, "yyyyMMdd"));
					filterDR.put("pais", pais);
					//filterDR.put("acuerdo", acuerdo);
					filterDR.put("consultaInterna", "N");
					Table tDRPrincipal = ibatisService.loadGrid("resolutor.dr_pincipal.grilla", filterDR);
					// 3. Consultar Adjunto DR & SDR vigente
					if (!tDRPrincipal.isEmpty()) {
						Row row = tDRPrincipal.getRow(0);
						Long drId = Util.longValueOf(row.getCell("DR_ID").getValue());
						
						filterADJ.put("drId", drId);
						Table tAdjuntosDR = ibatisService.loadGrid("formato.adjuntos.sdr.grilla", filterADJ);
						request.setAttribute("tAdjuntosDR", tAdjuntosDR);
					}
					request.setAttribute("mostrarResultado", Boolean.TRUE);
				} else {
					// 1.1 No permite Consultar DR
					codigoError = getCodigoError(permiteConsultaDr);
				}
			}
		} catch (Exception exc) {
			logger.error("Error en ConsultaDRController.consultarDR: exc:", exc);
			message = new ErrorMessage();
			message.setMessage(MessageConfig.getMessageConfig().getProperty("consulta_dr.error.error_generico",new String[]{exc.getMessage()}, request.getParameter("idioma")));
		}

		if (message != null) {
			messageList = new MessageList();
			messageList.add(message);
			request.setAttribute(Constantes.MESSAGE_LIST, messageList);
		}

		request.setAttribute("opcionFiltro", request.getParameter("opcionFiltro"));
		request.setAttribute("drEntidad", request.getParameter("drEntidad"));
		request.setAttribute("fechaDrEntidad", request.getParameter("fechaDrEntidad"));
		request.setAttribute("filtroPais", request.getParameter("filtroPais"));
		request.setAttribute("random", Math.random());
		request.setAttribute("formatoFecha", "en".equals(request.getParameter("idioma")) ? "MM/dd/yyyy" : "dd/MM/yyyy");
		request.setAttribute("formatoCalendario", "en".equals(request.getParameter("idioma")) ? "%m/%d/%Y" : "%d/%m/%Y");
		request.setAttribute("codigoError", codigoError);
		request.setAttribute("idioma", idioma);
		request.setAttribute("fechaHoraActual", fechaHoraActual);
		request.setAttribute("fechaActual", Util.getDate(fecha, "yyyyMMdd"));
		//request.setAttribute("fechaActual", Util.getDate(fecha, FORMATO_FECHA_ES));
		return new ModelAndView(consultaDRPub);
	}

	/**
	 * Valida si permite consulta DR resultados:
	 * 1: Permite Consulta
	 * -1: Error generico
	 * -20113: No permite Consulta DR (e_certif_no_permite_consulta)
	 * -20114: No existe DR (e_certif_no_existe)
	 * -20115: Fecha no coincide (e_fecha_certif_no_corresponde)
	 * -20116: DR anulado (e_certificado_anulado)
	 * @param drEntidad
	 * @param fechaDrEntidad
	 * @param paisAcuerdo
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public int permiteConsultaDRPublica(String drEntidad,Date fechaDrEntidad, long paisAcuerdo) throws Exception {
		int resultado = -1;
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("drEntidad", drEntidad);
		filter.put("fechaDrEntidad", fechaDrEntidad);
		filter.put("paisAcuerdo", paisAcuerdo);
		try {
			ibatisService.executeSPWithObject("certificadoOrigen.permite_consulta_dr_publica", filter);
			// Caso no permite consulta: mostrar error 20113: No permite Consulta DR
			resultado = filter.get("resultado").equals("S") ? 1 : 20113;
		} catch (Exception exc) {
			if (exc.getCause() instanceof SQLException) {
				resultado = ((SQLException) exc.getCause()).getErrorCode();
				logger.error("ConsultaDRController.permiteConsultaDR sql.errorCode: " + resultado + ": " + exc.getMessage());
			} else {
				logger.error("Error en ConsultaDRController.permiteConsultaDR exc:", exc);
			}
			// Lanzar excepcion y mostrar mensaje a usuario
			if (getCodigoError(resultado).equals("consulta_dr.error.error_generico")) {
				throw exc;
			}
		}
		return resultado;
	}

	private String getCodigoError(int sqlErrorCode) {
		String condigoMensajeError = "consulta_dr.error.error_generico";
		switch (sqlErrorCode) {
		// No se permite Consulta DR (Tupa Formato)
		case 20110:
			condigoMensajeError = "consulta_dr.error.no_permite_consulta";
			break;
		// No se permite Consulta DR (Acuerdo)	
		case 20113:
			condigoMensajeError = "consulta_dr.error.no_permite_consulta";
			break;
		// No existe certiificado
		case 20114:
			condigoMensajeError = "consulta_dr.error.no_existe_dr";
			break;
		// Fecha Incorrecta
		case 20115:
			condigoMensajeError = "consulta_dr.error.fecha_dr_entidad";
			break;
		// Certificado Anulado
		case 20116:
			condigoMensajeError = "consulta_dr.error.dr_anulado";
			break;
		// Certificado Anulado
		case 20130:
			condigoMensajeError = "consulta_dr.error.pais_acuerdo";
			break;
		default:
			condigoMensajeError = "consulta_dr.error.error_generico";
		}
		return condigoMensajeError;
	}
	
	/**
	 * Usado �nicamente por Consulta DR P�blica
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView descargarConsultaDR(HttpServletRequest request, HttpServletResponse response) {
		HashUtil<String, String> datos = RequestUtil.getParameter(request);
		int claveAdjunto = datos.getInt("idAdjunto");
		Adjunto adjunto = adjuntoLogic.descargarAdjuntoById(claveAdjunto);
        
	    byte [] bytes = adjunto.getArchivo();
	    try {
	    	if (bytes != null) {
	    		String nombreDocumento = adjunto.getNombre();
	            response.setContentType("application/pdf");
	            response.setHeader("Content-Disposition", "inline;filename=\""+nombreDocumento+"\"");
	            response.setContentLength(bytes.length);
	            ServletOutputStream ouputStream = response.getOutputStream();
	            ouputStream.write(bytes, 0, bytes.length);
	            ouputStream.flush();
	            ouputStream.close();
	    	}
	    } catch (Exception e) {
	    	logger.error("Ocurrio un error al intentar descargar el archivo", e);
	    }

	    return null;
	}

}