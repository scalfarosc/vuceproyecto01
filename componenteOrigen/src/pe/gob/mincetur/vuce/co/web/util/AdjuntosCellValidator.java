package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class AdjuntosCellValidator implements CellValidator{

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
		if (cell.getValue().toString().equals("N")) {
        	cell.getCellFormatter().setEditable(false);

        } else {
            cell.getCellFormatter().setEditable(true);

        }
    }
}
