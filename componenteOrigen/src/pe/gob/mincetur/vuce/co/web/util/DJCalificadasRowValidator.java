package pe.gob.mincetur.vuce.co.web.util;

import java.util.Date;
import java.util.Hashtable;

import org.jlis.core.util.HashUtil;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.util.TableUtil;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.RowValidator;


public class DJCalificadasRowValidator implements RowValidator {
	
	public void validate(Hashtable cellFormatters, Row row, HashUtil arg2) {
    	Cell cellVigenciaDJ = row.getCell("VIGENCIA_DJ");

        Cell cellEnlace = row.getCell("CALIFICACION_UO");
    	GenericCellFormatter cellFormatterEnlace = (GenericCellFormatter)cellFormatters.get(cellEnlace.getColumnName());
    	

    	if (cellVigenciaDJ.getValue()!=null && "S".equals(cellVigenciaDJ.getValue())) {
    		cellFormatterEnlace.setEditable(true);
    	} else {
    		cellFormatterEnlace.setEditable(false);
    	}
	}

}
