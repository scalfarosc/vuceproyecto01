package pe.gob.mincetur.vuce.co.web.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.config.LabelConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.list.OptionList;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.jlis.web.util.TableUtil;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoRequerido;
import pe.gob.mincetur.vuce.co.domain.Auditoria;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.Usuario;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COCalificacionAnticipada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.logic.AdjuntoLogic;
import pe.gob.mincetur.vuce.co.logic.AuditoriaLogic;
import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.InteroperabilidadFirmasLogic;
import pe.gob.mincetur.vuce.co.logic.OrdenLogic;
import pe.gob.mincetur.vuce.co.logic.SuceLogic;
import pe.gob.mincetur.vuce.co.logic.UsuariosLogic;
import pe.gob.mincetur.vuce.co.logic.certificadoOrigen.CertificadoOrigenLogic;
import pe.gob.mincetur.vuce.co.logic.resolutor.ResolutorLogic;
import pe.gob.mincetur.vuce.co.procesodr.ProcesoDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26IOPEBXML;
import pe.gob.mincetur.vuce.co.remoting.rest.ws.util.SpringApplicationContext;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.OptionListFactory;
import pe.gob.mincetur.vuce.co.util.Rol;
import pe.gob.mincetur.vuce.co.web.ajax.DatosRUCElementLoader;

/**
*Objeto :	CertificadoOrigenFormatoController
*Descripcion :	Clase Controladora Gen�rica para los Certificados de Origen - Segunda Versi�n.
*Fecha de Creacion :	
*Autor :	E. Osc�tegui - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*
*/

public abstract class CertificadoOrigenFormatoController extends MultiActionController {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);

    protected IbatisService ibatisService;

    protected UsuariosLogic usuariosLogic;

    protected FormatoLogic formatoLogic;

    protected OrdenLogic ordenLogic;

    protected AdjuntoLogic adjuntoLogic;

    protected CertificadoOrigenLogic certificadoOrigenLogic;

    protected SuceLogic suceLogic;

	protected ResolutorLogic resolutorLogic;

	private String listadoCertificadoOrigen;

    private String listadoTupas;

    private String seleccionarAcuerdo;

    private String formato;

    private String partidas;

    private String datosDocResolutivo;

    private String adjuntos;

    private String subsanacion;

    private String busquedaCO;

    private String duplicadoCertificadoOrigen;

	private String listadoDeclaracionJurada;

	private String anulacionCertificadoOrigen;

	private String calificacionAnticipadaCertificadoOrigen;

	private String reemplazoCertificadoOrigen;

	private String otraInformacionTramite;

	private String documentosResolutivos;

	private String documentosResolutivosFirma;
	
	private String documentosResolutivosFirmaResolutor;
	
	private String datosDocResolutivoResolutor;	
	
	private String busquedaCOFD;
	
	private ProcesoDR procesoDR;
	
	protected AuditoriaLogic auditoriaLogic;
	
    protected InteroperabilidadFirmasLogic iopLogic;
    
	private static final String FORMATO_FECHA_ES = "dd/MM/yyyy HH:mm:ss";
	private static final String FORMATO_FECHA_EN = "MM/dd/yyyy HH:mm:ss";

	/**
     * Muestra la pantalla de certificados, borradores, busqueda, y de creaci�n de nuevos certificados.
     * @param request
     * @param response
     * @return
     */
    public ModelAndView listarCertificadoOrigen(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        System.out.println(" rolActivo " + usuario.getRolActivo());
        System.out.println(" roles " + usuario.getRoles());
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        ///Integer acuerdo = ConstantesCO.AC_CHILE;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
        if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre())) {
        	filter.put("esSupervisor", "1");
        	filter.put("usuarioId", usuario.getIdUsuario());
        	request.setAttribute("hideDatosSoliciante", "yes");
        }// Si el usuario es de EXTRANET y tiene el rol HELP DESK, VUCE_CENTRAL_OPERADOR_FUNCIONA o VUCE_CENTRAL_SUPERVISOR_TECNICO
        else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
           		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS
            filter.put("tipoDocumento", datos.getString("tipoDocumento"));
            filter.put("numeroDocumento", datos.getString("numeroDocumento"));
            filter.put("nombre", datos.getString("nombre"));
            
            request.setAttribute("tipoDocumento", datos.getString("tipoDocumento"));
            request.setAttribute("numeroDocumento", datos.getString("numeroDocumento"));
            request.setAttribute("nombre", datos.getString("nombre"));
            request.setAttribute("hideDatosSoliciante", "no");
            
        } else {
            filter.put("usuarioId", usuario.getIdUsuario());
            request.setAttribute("hideDatosSoliciante", "yes");
        }
        
        String acuerdo = datos.getString("filtroAcuerdo");
        boolean tieneFiltro = false;
        
        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
        	//acuerdo = datos.getInt("filtroAcuerdo");
            tieneFiltro = true;
            filter.put("filtroAcuerdo", acuerdo);
            request.setAttribute("filtroAcuerdo", acuerdo);
        }
        
        if(datos.contains("filtroFormato") && !"".equals(datos.getString("filtroFormato"))){
            tieneFiltro = true;
        	filter.put("filtroFormato", datos.getString("filtroFormato"));
        	request.setAttribute("filtroFormato", datos.getString("filtroFormato"));
        }

        if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
            tieneFiltro = true;
            filter.put("filtroEntidad", datos.getString("filtroEntidad"));
            request.setAttribute("filtroEntidad", datos.getString("filtroEntidad"));
        }
        if (datos.contains("filtroEstado") && !"".equals(datos.getString("filtroEstado")) && !"T".equals(datos.getString("filtroEstado"))){
            String datoEstado = datos.getString("filtroEstado");
            if((!"E".equalsIgnoreCase(datoEstado)) && (!"C".equalsIgnoreCase(datoEstado))){
                tieneFiltro = true;
            	datoEstado =  datoEstado.split("[|]")[1];
            	filter.put("filtroEstado", datoEstado);
            }else{
                tieneFiltro = true;
	            if("E".equalsIgnoreCase(datoEstado)) filter.put("enProceso", "S");
	            if("C".equalsIgnoreCase(datoEstado)) filter.put("concluidos", "C");
            }
            request.setAttribute("filtroEstado", datos.getString("filtroEstado"));
        }
        
        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))){
            request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));
            
            if ("O".equals(datos.getString("opcionFiltro")) && datos.contains("numeroOrden") && !"".equals(datos.getString("numeroOrden"))){
                tieneFiltro = true;
                filter.put("numeroOrden", datos.getString("numeroOrden"));
                request.setAttribute("numeroOrden", datos.getString("numeroOrden"));
            } else if ("S".equals(datos.getString("opcionFiltro")) && datos.contains("numeroSUCE") && !"".equals(datos.getString("numeroSUCE"))){
                tieneFiltro = true;
                filter.put("numeroSUCE", datos.getString("numeroSUCE"));
                request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
            }
        }
        /*
        HashUtil<String, Object> filterPC = new HashUtil<String, Object>();
        filterPC.put("tupaId", datos.getInt("idTupa"));//int idTupa = datos.getInt("idTupa");
        filterPC.put("formatoId", datos.getInt("idFormato"));
        filterPC.put("acuerdoId", datos.getString("numeroSUCE"));
        String esProduccionControlada ="";
        request.setAttribute("esProduccionControlada", esProduccionControlada);
        */
        
        //String key = "certificadoOrigen.certificados.grilla";
        //String keyBorradores = "certificadoOrigen.certificados.borradores.grilla";
        
        //Table tCertificados = ibatisService.loadGrid(key, filter);
        //Table tCertificadosBorradores = ibatisService.loadGrid(keyBorradores, filter);
        
        //request.setAttribute("tCertificados", tCertificados);
        //request.setAttribute("tCertificadosBorradores", tCertificadosBorradores);
        request.setAttribute("filterGrilla", filter);
        request.setAttribute("seleccionado", request.getParameter("seleccionado")!=null?request.getParameter("seleccionado"):"0");
        
        // Si se trata de un usuario HELPDESK o de VUCE, tiene que haber escogido algun filtro
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
                 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
               	 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	filter.put("tieneFiltro", tieneFiltro ? "S" : "N");
        }
        
        return new ModelAndView(listadoCertificadoOrigen);
    }

    /**
     * Muestra la p�gina de selecci�n de TUPAS (cada tupa representa una acci�n sobre un Certificado de Origen)
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarTupas(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String key = "formato.entidad.tupas.grilla";

        //if (!datos.getString("entidad").equals("")){
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("entidad", 9/*datos.getInt("entidad")*/);
            // Este llenado extra de filtros se deja por si luego se necesita al crear la parte de la entidad
            if (Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) {
                filter.put("tipoDocumento", usuario.getTipoDocumento());
                filter.put("numeroDocumento", usuario.getNumeroDocumento());
            }
            // Si el usuario es de EXTRANET y tiene el rol HELP DESK
            else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
                // VE TODOS LOS REGISTROS
            } else {
                filter.put("usuarioId", usuario.getIdUsuario());
            }
            Table tFormatos = ibatisService.loadGrid(key, filter);
            request.setAttribute("tFormatos", tFormatos);
            request.setAttribute("entidad", 9/*datos.getInt("entidad")*/);
        //}

        return new ModelAndView(listadoTupas);
    }

    /**
     * Muestra la p�gina de Selecci�n de Acuerdos
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarFormatoTupa(HttpServletRequest request, HttpServletResponse response) {
    	ModelAndView cargarFormatoView = null;
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        /*int idFormato = datos.getInt("idFormato");
        int idTupa = datos.getInt("idTupa");*/
        String nombreFormato = datos.getString("formato");

       	if (ConstantesCO.CO_SOLICITUD.equals(nombreFormato.toLowerCase())) {
       		cargarFormatoView = cargarSeleccionAcuerdo(request,response);
       	} else if (ConstantesCO.CO_DUPLICADO.equals(nombreFormato.toLowerCase())) {
       		cargarFormatoView = nuevaOrden(request,response);
       	} else if (ConstantesCO.CO_ANULACION.equals(nombreFormato.toLowerCase())) {
       		cargarFormatoView = nuevaOrden(request,response);
       	} else if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(nombreFormato.toLowerCase())) {
       		cargarFormatoView = cargarSeleccionAcuerdo(request,response);
       	} else if (ConstantesCO.CO_REEMPLAZO.equals(nombreFormato.toLowerCase())) {
       		cargarFormatoView = nuevaOrden(request,response);
       	}

        /*if((idFormato==ConstantesCO.CO_FORMATOID)&&(idTupa==ConstantesCO.CO_TUPAID)){
        	return cargarSeleccionAcuerdo(request,response);
        }
        if((idFormato==ConstantesCO.DCO_FORMATOID)&&(idTupa==ConstantesCO.DCO_TUPAID)){
        	return nuevoDuplicado(request,response);
        }*/

        return cargarFormatoView;
    }

	/**
     * Muestra la pantalla de certificados, borradores, busqueda, y de creaci�n de nuevos certificados.
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarListaDRs(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
        int numParams = 0;
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
           		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS
        } else {
	    	filter.put("usuarioId", usuario.getIdUsuario());
	        
	        if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre())) {
	        	filter.put("esSupervisor", "1");
	    	}
        }
        filter.put("filtrarUsuarioTipo", "filtrarUsuarioTipo");
        
        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
            filter.put("filtroAcuerdo", datos.getString("filtroAcuerdo"));
            numParams++;
            request.setAttribute("filtroAcuerdo", datos.getString("filtroAcuerdo"));
        }

        if(datos.contains("filtroFormato") && !"".equals(datos.getString("filtroFormato"))){
        	filter.put("filtroFormato", datos.getString("filtroFormato"));
        	numParams++;
        	request.setAttribute("filtroFormato", datos.getString("filtroFormato"));
        }

        if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
            filter.put("filtroEntidad", datos.getString("filtroEntidad"));
            numParams++;
            request.setAttribute("filtroEntidad", datos.getString("filtroEntidad"));
        }
        if (datos.contains("filtroEstado") && !"".equals(datos.getString("filtroEstado")) && !"T".equals(datos.getString("filtroEstado"))){
            String datoEstado = datos.getString("filtroEstado");
            if((!"E".equalsIgnoreCase(datoEstado)) && (!"C".equalsIgnoreCase(datoEstado))){
            	datoEstado =  datoEstado.split("[|]")[1];
            	filter.put("filtroEstado", datoEstado);
            }else{
	            if("E".equalsIgnoreCase(datoEstado)) filter.put("enProceso", "S");
	            if("C".equalsIgnoreCase(datoEstado)) filter.put("concluidos", "C");
            }
            numParams++;
            request.setAttribute("filtroEstado", datos.getString("filtroEstado"));
        }

        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))){
            request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));
            numParams++;
            if ("O".equals(datos.getString("opcionFiltro")) && datos.contains("numeroOrden") && !"".equals(datos.getString("numeroOrden"))){
                filter.put("numeroOrden", datos.getString("numeroOrden"));
                request.setAttribute("numeroOrden", datos.getString("numeroOrden"));
           } else {
        	   if ("S".equals(datos.getString("opcionFiltro")) && datos.contains("numeroSUCE") && !"".equals(datos.getString("numeroSUCE"))){
                filter.put("numeroSUCE", datos.getString("numeroSUCE"));
                request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
                }else{
                	if ("C".equals(datos.getString("opcionFiltro")) && datos.contains("numeroCO") && !"".equals(datos.getString("numeroCO"))){
                        filter.put("numeroCO", datos.getString("numeroCO"));
                        request.setAttribute("numeroCO", datos.getString("numeroCO"));
                    }
                }
           }

        }

        //String key = "formato.drs.grilla";

        /*if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre())) {
        	filter.put("tipoDocumento", usuario.getTipoDocumento());
        	filter.put("numeroDocumento", usuario.getNumeroDocumento());
        }
        // Si el usuario es de EXTRANET y tiene el rol HELP DESK
        else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) ||
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) ||
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS
        	filter.put("filtrarUsuarioTipo", "filtrarUsuarioTipo");
        	if (datos.getString("entidad").equals("")) {
        		filter.put("entidad", -1);
        	}
        } else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && usuario.getRoles().containsKey(Rol.CO_ENTIDAD_EVALUADOR.getNombre())) {
        	filter.put("filtrarUsuarioTipo", "filtrarUsuarioTipo");
        	filter.put("entidad", usuario.getIdEntidad());
        	filter.put("usuarioIdEvaluador", usuario.getIdUsuario());
        } else {
            filter.put("usuarioId", usuario.getIdUsuario());
        }

        if (!datos.getString("entidad").equals("")) {
    		filter.put("entidad", datos.getInt("entidad"));
    		request.setAttribute("entidad", datos.getInt("entidad"));
        }

        if (datos.getString("opcionFiltro").equals(ConstantesCO.OPCION_SUCE)) {
        	if (!datos.getString("numeroSUCE").equals("")) {
        		String numSUCE = datos.getString("numeroSUCE").replaceAll("[-]", "");
        		if (!NumberUtils.isNumber(numSUCE)) {
        			numSUCE = "0";
        		}

                filter.put("numeroSUCE", numSUCE);
                filter.put("numeroDR", "0");
                filter.put("expedienteEntidad", "0");
                request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
            }
        } else if (datos.getString("opcionFiltro").equals(ConstantesCO.OPCION_DR)) {
        	if (!datos.getString("numeroDR").equals("")) {
        		String numDR = datos.getString("numeroDR").replaceAll("[-]", "");
        		if (!NumberUtils.isNumber(numDR)) {
        			numDR = "0";
        		}

        		filter.put("numeroSUCE", "0");
                filter.put("numeroDR", numDR);
                filter.put("expedienteEntidad", "0");
                request.setAttribute("numeroDR", datos.getString("numeroDR"));
            }
        } else if (datos.getString("opcionFiltro").equals(ConstantesCO.OPCION_EXPEDIENTE_ENTIDAD)) {
        	if (!datos.getString("expedienteEntidad").equals("")) {
        		String expedienteEntidad = datos.getString("expedienteEntidad");
        		filter.put("numeroSUCE", "0");
                filter.put("numeroDR", "0");
                filter.put("expedienteEntidad", expedienteEntidad);
                request.setAttribute("expedienteEntidad", datos.getString("expedienteEntidad"));
            }
        }
        request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));
        */
        if (request.getParameter("pantallaDocumentosResolutivos")==null) {
        	datos.put("soloPendientes", "S");
        }
        if (!datos.getString("soloPendientes").equals("")) {
        	filter.put("soloPendientes", datos.getString("soloPendientes"));
        }
        request.setAttribute("soloPendientes", datos.getString("soloPendientes"));

        filter.put("numParams", numParams);
        System.out.println("******************num Params: "+numParams);
        filter.imprimir();
        
        request.setAttribute("filterDRs", filter);
        request.setAttribute("filterSolicitudesRectificacionDR", filter);

        //Table tDRs = ibatisService.loadGrid(key, filter);
        //request.setAttribute("tDRs", tDRs);

        // Ahora cargamos las Solicitudes de Rectificacion de DR
        //Table tSolicitudesRectificacionDR = ibatisService.loadGrid("formato.solicitudesRectificacionDR.grilla", filter);
        //request.setAttribute("tSolicitudesRectificacionDR", tSolicitudesRectificacionDR);

    	return new ModelAndView(documentosResolutivos);
        /*UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Integer acuerdo = ConstantesCO.AC_CHILE;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("usuarioId", usuario.getIdUsuario());

        if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre())) {
        	filter.put("esSupervisor", "1");
    	}

        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
        	acuerdo = datos.getInt("filtroAcuerdo");
        }

        filter.put("filtroAcuerdo", acuerdo);
        request.setAttribute("filtroAcuerdo", acuerdo);

        if(datos.contains("filtroFormato") && !"".equals(datos.getString("filtroFormato"))){
        	filter.put("filtroFormato", datos.getString("filtroFormato"));
        	request.setAttribute("filtroFormato", datos.getString("filtroFormato"));
        }

        if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
            filter.put("filtroEntidad", datos.getString("filtroEntidad"));
            request.setAttribute("filtroEntidad", datos.getString("filtroEntidad"));
        }
        if (datos.contains("filtroEstado") && !"".equals(datos.getString("filtroEstado")) && !"T".equals(datos.getString("filtroEstado"))){
            String datoEstado = datos.getString("filtroEstado");
            if((!"E".equalsIgnoreCase(datoEstado)) && (!"C".equalsIgnoreCase(datoEstado))){
            	datoEstado =  datoEstado.split("[|]")[1];
            	filter.put("filtroEstado", datoEstado);
            }else{
	            if("E".equalsIgnoreCase(datoEstado)) filter.put("enProceso", "S");
	            if("C".equalsIgnoreCase(datoEstado)) filter.put("concluidos", "C");
            }
            request.setAttribute("filtroEstado", datos.getString("filtroEstado"));
        }

        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))){
            request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));

            if ("O".equals(datos.getString("opcionFiltro")) && datos.contains("numeroOrden") && !"".equals(datos.getString("numeroOrden"))){
                filter.put("numeroOrden", datos.getString("numeroOrden"));
                request.setAttribute("numeroOrden", datos.getString("numeroOrden"));
           } else if ("S".equals(datos.getString("opcionFiltro")) && datos.contains("numeroSUCE") && !"".equals(datos.getString("numeroSUCE"))){
                filter.put("numeroSUCE", datos.getString("numeroSUCE"));
                request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
           }

        }

        String key = "certificadoOrigen.certificados.grilla";
        String keyBorradores = "certificadoOrigen.certificados.borradores.grilla";

        Table tCertificados = ibatisService.loadGrid(key, filter);
        Table tCertificadosBorradores = ibatisService.loadGrid(keyBorradores, filter);

        request.setAttribute("tCertificados", tCertificados);
        request.setAttribute("tCertificadosBorradores", tCertificadosBorradores);
        request.setAttribute("seleccionado", datos.get("seleccionado")!=null?datos.get("seleccionado"):"0");

        return new ModelAndView(listadoCertificadoOrigen);*/
    }

	/**
     * Muestra la pantalla de certificados, borradores, busqueda, y de creaci�n de nuevos certificados.
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarListaDRsFirma(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
           		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS
        } else {
	    	filter.put("usuarioId", usuario.getIdUsuario());
	        
	        if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre())) {
	        	filter.put("esSupervisor", "1");
	    	}
        }
        filter.put("filtrarUsuarioTipo", "filtrarUsuarioTipo");
        
        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
            filter.put("filtroAcuerdo", datos.getString("filtroAcuerdo"));
            request.setAttribute("filtroAcuerdo", datos.getString("filtroAcuerdo"));
        }

        if(datos.contains("filtroFormato") && !"".equals(datos.getString("filtroFormato"))){
        	filter.put("filtroFormato", datos.getString("filtroFormato"));
        	request.setAttribute("filtroFormato", datos.getString("filtroFormato"));
        }

        if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
            filter.put("filtroEntidad", datos.getString("filtroEntidad"));
            request.setAttribute("filtroEntidad", datos.getString("filtroEntidad"));
        }
        if (datos.contains("filtroEstado") && !"".equals(datos.getString("filtroEstado")) && !"T".equals(datos.getString("filtroEstado"))){
            String datoEstado = datos.getString("filtroEstado");
            if((!"E".equalsIgnoreCase(datoEstado)) && (!"C".equalsIgnoreCase(datoEstado))){
            	datoEstado =  datoEstado.split("[|]")[1];
            	filter.put("filtroEstado", datoEstado);
            }else{
	            if("E".equalsIgnoreCase(datoEstado)) filter.put("enProceso", "S");
	            if("C".equalsIgnoreCase(datoEstado)) filter.put("concluidos", "C");
            }
            request.setAttribute("filtroEstado", datos.getString("filtroEstado"));
        }

        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))){
            request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));

            if ("O".equals(datos.getString("opcionFiltro")) && datos.contains("numeroOrden") && !"".equals(datos.getString("numeroOrden"))){
                filter.put("numeroOrden", datos.getString("numeroOrden"));
                request.setAttribute("numeroOrden", datos.getString("numeroOrden"));
           } else {
        	   if ("S".equals(datos.getString("opcionFiltro")) && datos.contains("numeroSUCE") && !"".equals(datos.getString("numeroSUCE"))){
                filter.put("numeroSUCE", datos.getString("numeroSUCE"));
                request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
                }else{
                	if ("C".equals(datos.getString("opcionFiltro")) && datos.contains("numeroCO") && !"".equals(datos.getString("numeroCO"))){
                        filter.put("numeroCO", datos.getString("numeroCO"));
                        request.setAttribute("numeroCO", datos.getString("numeroCO"));
                    }
                }
           }

        }


        if (request.getParameter("pantallaDocumentosResolutivos")==null) {
        	datos.put("soloPendientes", "S");
        }
        if (!datos.getString("soloPendientes").equals("")) {
        	filter.put("soloPendientes", datos.getString("soloPendientes"));
        }
        request.setAttribute("soloPendientes", datos.getString("soloPendientes"));

        request.setAttribute("filterDRsFirma", filter);
        request.setAttribute("filterSolicitudesRectificacionDR", filter);

    	return new ModelAndView(documentosResolutivosFirma);
    }
    
	/**
     * Muestra la pantalla de certificados, borradores, busqueda, y de creaci�n de nuevos certificados.
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarListaDRsFirmaResolutor(HttpServletRequest request, HttpServletResponse response) {
    	UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
           		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS
        } else {
	    	filter.put("usuarioId", usuario.getIdUsuario());
	        
	        if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre())) {
	        	filter.put("esSupervisor", "1");
	    	}
        }
        filter.put("filtrarUsuarioTipo", "filtrarUsuarioTipo");
        
        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
            filter.put("filtroAcuerdo", datos.getString("filtroAcuerdo"));
            request.setAttribute("filtroAcuerdo", datos.getString("filtroAcuerdo"));
        }

        if(datos.contains("filtroFormato") && !"".equals(datos.getString("filtroFormato"))){
        	filter.put("filtroFormato", datos.getString("filtroFormato"));
        	request.setAttribute("filtroFormato", datos.getString("filtroFormato"));
        }

        if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
            filter.put("filtroEntidad", datos.getString("filtroEntidad"));
            request.setAttribute("filtroEntidad", datos.getString("filtroEntidad"));
        }
        if (datos.contains("filtroEstado") && !"".equals(datos.getString("filtroEstado")) && !"T".equals(datos.getString("filtroEstado"))){
            String datoEstado = datos.getString("filtroEstado");
            if((!"E".equalsIgnoreCase(datoEstado)) && (!"C".equalsIgnoreCase(datoEstado))){
            	datoEstado =  datoEstado.split("[|]")[1];
            	filter.put("filtroEstado", datoEstado);
            }else{
	            if("E".equalsIgnoreCase(datoEstado)) filter.put("enProceso", "S");
	            if("C".equalsIgnoreCase(datoEstado)) filter.put("concluidos", "C");
            }
            request.setAttribute("filtroEstado", datos.getString("filtroEstado"));
        }

        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))){
            request.setAttribute("opcionFiltro", datos.getString("opcionFiltro"));

            if ("O".equals(datos.getString("opcionFiltro")) && datos.contains("numeroOrden") && !"".equals(datos.getString("numeroOrden"))){
                filter.put("numeroOrden", datos.getString("numeroOrden"));
                request.setAttribute("numeroOrden", datos.getString("numeroOrden"));
           } else {
        	   if ("S".equals(datos.getString("opcionFiltro")) && datos.contains("numeroSUCE") && !"".equals(datos.getString("numeroSUCE"))){
                filter.put("numeroSUCE", datos.getString("numeroSUCE"));
                request.setAttribute("numeroSUCE", datos.getString("numeroSUCE"));
                }else{
                	if ("C".equals(datos.getString("opcionFiltro")) && datos.contains("numeroCO") && !"".equals(datos.getString("numeroCO"))){
                        filter.put("numeroCO", datos.getString("numeroCO"));
                        request.setAttribute("numeroCO", datos.getString("numeroCO"));
                    }
                }
           }

        }


        if (request.getParameter("pantallaDocumentosResolutivos")==null) {
        	datos.put("soloPendientes", "S");
        }
        if (!datos.getString("soloPendientes").equals("")) {
        	filter.put("soloPendientes", datos.getString("soloPendientes"));
        }
        request.setAttribute("soloPendientes", datos.getString("soloPendientes"));

        request.setAttribute("filterDRsFirma", filter);
        request.setAttribute("filterSolicitudesRectificacionDR", filter);

    	return new ModelAndView(documentosResolutivosFirmaResolutor);
    }

    /**
     * Muestra la p�gina de Selecci�n de Acuerdos
     * @param request
     * @param response
     * @return
     */
    public ModelAndView cargarSeleccionAcuerdo(HttpServletRequest request, HttpServletResponse response) {
    	// Obtenemos informaci�n para cargar el nombre del formato y del tupa
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int idTupa = datos.getInt("idTupa");
        int idFormato = datos.getInt("idFormato");
        Formato formato = (Formato)formatoLogic.getFormatoByTupa(idFormato, idTupa);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idTupa", idTupa);
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");
        // Pasamos todos los elementos de request obtenidos
        RequestUtil.setAttributes(request);
        // Agregamos el nombre del formato a mostrar en el request
        request.setAttribute("nombreFormato", formato.getFormato()+" - "+formato.getNombre()+" (TUPA: "+tupa+")");

        return new ModelAndView(seleccionarAcuerdo);
    }

/*    public ModelAndView nuevoDuplicado(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        request.setAttribute("activeSUCE", "active");

        // Se actualiza los datos del RUC
        HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
        if (datosFichaRUC.size() > 0) {
            usuariosLogic.actualizarDatosRUCUsuario(usuario, datosFichaRUC);
        }

        //Se obtienen los datos de la grilla
        int idTupa = datos.getInt("idTupa");
        int idEntidad = datos.getInt("idEntidad");
        int idFormato = datos.getInt("idFormato");
        //String formato = datos.getString("formato");
        Formato formato = (Formato)formatoLogic.getFormatoByTupa(idFormato, idTupa);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idTupa", idTupa);
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");
        //String tupa = "FALTA TUPA";

        String controller = formato.getFormato().toLowerCase()+".htm";

        request.setAttribute("idAcuerdo", datos.getString("acuerdo"));
        request.setAttribute("idPais", datos.getString("pais"));
        request.setAttribute("idEntidadCertificadora", datos.getString("entidadCertificadora"));
        request.setAttribute("idTupa", idTupa);
        request.setAttribute("idEntidad", idEntidad);
        request.setAttribute("idFormato", idFormato);
        request.setAttribute("formato", formato);

        request.setAttribute("nombreFormato", formato+" - "+formato+" (TUPA: "+tupa+")");
        request.setAttribute("bloqueado", "N");
        request.setAttribute("controller", controller);
        request.setAttribute("tipoPersona", usuario.getTipoPersona());

        //Cargar datos del solicitante
        Solicitante solicitante = formatoLogic.getSolicitanteDetail(usuario);
        RequestUtil.setAttributes(request, "SOLICITANTE.", solicitante);
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)){

            if (usuario.getTipoPersona().equals(ConstantesCO.TIPO_PERSONA_JURIDICA)){
                //Cargar datos del representante
                HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
                filterRepresentante.put("usuarioId", solicitante.getUsuarioId());
                request.setAttribute("filterRepresentante", filterRepresentante);
            }
        }

        if (solicitante.getUbigeo() != null) {
            // Nombres del departamento, provincia y distrito
            HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
            request.setAttribute("departamento", element.get("DEPARTAMENTO"));
            request.setAttribute("provincia", element.get("PROVINCIA"));
            request.setAttribute("distrito", element.get("DISTRITO"));
        }

        request.setAttribute("opcion", ConstantesCO.OPCION_ORDEN);

        return new ModelAndView(duplicadoCertificadoOrigen);
    }
*/
    /**
     * Muestra la p�gina inicial de creaci�n del certificado
     * @param request
     * @param response
     * @return
     */
    public ModelAndView nuevaOrden(HttpServletRequest request, HttpServletResponse response) {

        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        request.setAttribute("activeSUCE", "active");

        // Se actualiza los datos del RUC
        HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
        if (datosFichaRUC.size() > 0) {
            usuariosLogic.actualizarDatosRUCUsuario(usuario, datosFichaRUC);
        }

        //Se obtienen los datos de la grilla
        int idTupa = datos.getInt("idTupa");
        int idEntidad = datos.getInt("idEntidad");
        int idFormato = datos.getInt("idFormato");
        String nombreFormato = datos.getString("formato");

        Formato formato = (Formato)formatoLogic.getFormatoByTupa(idFormato, idTupa);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idTupa", idTupa);
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");
        String controller = formato.getFormato().toLowerCase()+".htm";

        request.setAttribute("idAcuerdo", datos.getString("acuerdo"));
        request.setAttribute("idPais", datos.getString("pais"));
        request.setAttribute("idEntidadCertificadora", datos.getString("entidadCertificadora"));
        request.setAttribute("idTupa", idTupa);
        request.setAttribute("idEntidad", idEntidad);
        request.setAttribute("idFormato", idFormato);
        request.setAttribute("idSede", datos.getString("sede"));
        request.setAttribute("formato", formato.getFormato());
        request.setAttribute("nombreFormato", formato.getFormato()+" - "+formato.getNombre()+" (TUPA: "+tupa+")");
        request.setAttribute("bloqueado", "N");
        request.setAttribute("controller", controller);
        request.setAttribute("tipoPersona", usuario.getTipoPersona());
        
        request.setAttribute("certificadoOrigen", datos.getString("certificadoOrigen"));
        request.setAttribute("certificadoReexportacion", datos.getString("certificadoReexportacion"));

        //Cargar datos del solicitante
        Solicitante solicitante = formatoLogic.getSolicitanteDetail(usuario);
        RequestUtil.setAttributes(request, "SOLICITANTE.", solicitante);

        // Cargar datos del usuario creador del formato
        Solicitante usuarioFormato = formatoLogic.getUsuarioDetail(usuario);

        // Cargamos datos del usuario del formato como el declarante
        RequestUtil.setAttributes(request, "DECLARANTE.", usuarioFormato);

        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)){

            if (usuario.getTipoPersona().equals(ConstantesCO.TIPO_PERSONA_JURIDICA)){
                //Cargar datos del representante
                HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
                //filterRepresentante.put("usuarioId", "346");
                filterRepresentante.put("usuarioId", solicitante.getUsuarioId());
                //filterRepresentante.put("usuarioId", usuario.getIdUsuario());
                request.setAttribute("filterRepresentante", filterRepresentante);
            }
        }

        if (solicitante.getUbigeo() != null) {
            // Nombres del departamento, provincia y distrito
            HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
            request.setAttribute("departamento", element.get("DEPARTAMENTO"));
            request.setAttribute("provincia", element.get("PROVINCIA"));
            request.setAttribute("distrito", element.get("DISTRITO"));
        }

        if (usuarioFormato.getUbigeo() != null) {
            // Nombres del departamento, provincia y distrito
            HashMap<String, String> element = formatoLogic.getUbigeoDetail(usuarioFormato.getUbigeo());
            request.setAttribute("DECLARANTE.departamento", element.get("DEPARTAMENTO"));
            request.setAttribute("DECLARANTE.provincia", element.get("PROVINCIA"));
            request.setAttribute("DECLARANTE.distrito", element.get("DISTRITO"));
        }

        /*if (usuario.isAgente() || usuario.isLaboratorio()) {
            request.setAttribute("empresa", "S");
            Solicitante empresa = formatoLogic.getEmpresaExternaDetail(usuario);

            if (empresa==null) {
                MessageList messageList = new MessageList();
                String rol = usuario.isAgente() ? Rol.VUCE_USUARIO_AGENTE_ADUANA.getNombre() : Rol.VUCE_USUARIO_LABORATORIO.getNombre();
                String textoNombreEmpresa = usuario.isAgente() ? "de la Agencia de Aduana" : "del Laboratorio";
                Message message = new Message("vuce.validacionPreFormato.rolSinEmpresaExterna", rol, textoNombreEmpresa);
                messageList.add(message);
                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                request.setAttribute("bloqueado", "S");
            } else {
                RequestUtil.setAttributes(request, "EMPRESA.", empresa);

                if (empresa.getUbigeo() != null) {
                    // Nombres del departamento, provincia y distrito
                    HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
                    request.setAttribute("EMPRESA.departamento", element.get("DEPARTAMENTO"));
                    request.setAttribute("EMPRESA.provincia", element.get("PROVINCIA"));
                    request.setAttribute("EMPRESA.distrito", element.get("DISTRITO"));
                }
            }
        }*/

        
        //20140526 - JMC - BUG 1.PersonaNatural  
        Message message = validacionPreFormato(solicitante.getUsuarioId().intValue(), formato);
        if (message!=null) {
            MessageList messageList = new MessageList();
        	messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
            request.setAttribute("bloqueado", "S");
        }
           
        request.setAttribute("opcion", ConstantesCO.OPCION_ORDEN);

       	return identificarPantallaFormato(nombreFormato.toLowerCase());
    }

    public ModelAndView crearOrdenFormato(HttpServletRequest request, HttpServletResponse response) {
    	ModelAndView crearOrdenView = null;
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        String nombreFormato = datos.getString("formato");
        MessageList messageList = null;
        
        try {
   		    messageList = certificadoOrigenLogic.crearSolicitudCertificado(usuario, datos);
        } catch (Exception e) {
        	Message message = ComponenteOrigenUtil.getErrorMessage(e);
        	if (messageList==null) {
        		messageList = new MessageList();
        	}
        	messageList.add(message);
    		logger.error("Error en CertificadoOrigenLogicImpl.insertCertificado", e);
    	}
        
   		if (ConstantesCO.CO_SOLICITUD.equals(nombreFormato.toLowerCase())) {
   			crearOrdenView = new ModelAndView(this.formato);
   		} else if (ConstantesCO.CO_DUPLICADO.equals(nombreFormato.toLowerCase())) {
       		crearOrdenView = new ModelAndView(duplicadoCertificadoOrigen);
       	} else if (ConstantesCO.CO_ANULACION.equals(nombreFormato.toLowerCase())) {
       		crearOrdenView = new ModelAndView(anulacionCertificadoOrigen);
       	} else if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(nombreFormato.toLowerCase())) {
       		crearOrdenView = new ModelAndView(this.calificacionAnticipadaCertificadoOrigen);
       	} else if (ConstantesCO.CO_REEMPLAZO.equals(nombreFormato.toLowerCase())) {
       		crearOrdenView = new ModelAndView(reemplazoCertificadoOrigen);
       	}
       	
       	if (messageList.containsError()) {
       		request.setAttribute(Constantes.MESSAGE_LIST, messageList);
       		return nuevaOrden(request, response);
       	}
        
       	cargarInformacionFormato(request, messageList);
        
        return crearOrdenView;
    }
    
    public ModelAndView cargarInformacionOrdenVigente(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);

    	HashUtil filter = new HashUtil<String, Object>();
    	filter.put("ordenId", datos.getLong("orden"));

    	HashUtil element = ibatisService.loadElement("certificadoOrigen.certifPrincipal.mto.vigente", filter);

    	if (element != null && element.get("MTO") != null) {
    		element.put("orden", datos.getLong("orden"));
    		element.put("mto", element.get("MTO"));
    	} else {
    		element = null;
    	}

    	return cargarInformacionOrden(request, response, element);
    }

    public ModelAndView cargarInformacionOrden(HttpServletRequest request, HttpServletResponse response) {
    	return cargarInformacionOrden(request, response, null);
    }

    public ModelAndView cargarInformacionOrden(HttpServletRequest request, HttpServletResponse response, HashUtil element) {
    	ModelAndView cargarInformacionOrdenView = null;
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        MessageList messageList = ((element != null && element.get(Constantes.MESSAGE_LIST) != null) ? (MessageList) element.get(Constantes.MESSAGE_LIST): new MessageList());

        long orden = ((element != null && element.get("ordenId") != null) ? element.getLong("ordenId"): datos.getLong("orden"));//NPCS 03/01/2018 correccion orden - ordenId
        int mto = ((element != null && element.get("mto") != null) ? element.getInt("mto"): datos.getInt("mto"));
        //int idFormato = datos.getInt("idFormato");
        //int idTupa = datos.getInt("idTupa");
        String nombreFormato = datos.getString("formato");

        Orden ordenObj = null;
        if (datos.get("numOrden") != null && "S".equalsIgnoreCase(datos.getString("versionOriginal"))) {
        	ordenObj = ordenLogic.loadOrdenByNumero(datos.getLong("numOrden"));
        } else {
            ordenObj = ordenLogic.getOrdenById(orden, mto);
        }
        messageList.setObject(ordenObj);

        //if((idFormato==ConstantesCO.CO_FORMATOID)&&(idTupa==ConstantesCO.CO_TUPAID)){
       	cargarInformacionFormato(request,messageList);
        /*	return new ModelAndView(this.formato);
        }
        if((idFormato==ConstantesCO.DCO_FORMATOID)&&(idTupa==ConstantesCO.DCO_TUPAID)){
        	cargarInformacionDuplicado(request, messageList);
        	return new ModelAndView(duplicadoCertificadoOrigen);
        }*/

       	if (ordenObj.getIdUsuario().intValue() != usuario.getIdUsuario().intValue()) {
       		request.setAttribute("bloqueado", "S");
       	}
       	int numRecargas = -1;
       	String paginaPrev = request.getParameter("pagActual");
       	String pagActual = identificarNombrePantallaFormato(nombreFormato);
       	if ( pagActual.equals(paginaPrev) && request.getParameter("numRecargas") != null && !"".equals(request.getParameter("numRecargas")) ) {
       		numRecargas =  Integer.parseInt(request.getParameter("numRecargas"));
       	}
       	request.setAttribute("numRecargas", numRecargas + 1);
       	request.setAttribute("pagActual", pagActual);


    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);
        return identificarPantallaFormato(nombreFormato);


        /*MessageList messageList = new MessageList();
        Orden ordenObj = ordenLogic.getOrdenById(new Long(request.getParameter("orden")), Integer.parseInt(request.getParameter("mto")));
        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return new ModelAndView(this.formato);*/
    }
    
    public String getFormatoVersionSufijo(Orden orden) {
    	//NPCS 03/01/2018
    	if (orden.getVersionFormato()!=null && !ConstantesCO.FORMATO_PRIMERA_VERSION.equalsIgnoreCase(orden.getVersionFormato())) {
    		return "_"+orden.getVersionFormato();
    	}
    	return "";
    }

    public void cargarInformacionFormato(HttpServletRequest request, MessageList messageList) {
        cargarInformacionGeneralFormato(request, messageList);
        cargarInformacionEspecificaFormato(request, messageList);
        //request.setAttribute(Constantes.MESSAGE_LIST, messageList);
    }

    private boolean acuerdoRequiereFactura(int acuerdoId) {
    	boolean res = true;

    	if (acuerdoId == ConstantesCO.AC_EFTA.intValue()) {
    		res = true;
    	}

    	return res;
    }

    private void cargarDatosFormatoOrigen(Orden ordenObj, String formato, HttpServletRequest request, MessageList messageList) {

    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        String puedeTransmitir = "N";
        int cuenta = 0;

    	// Esta funci�n tiene un key diferente por cada formato, y el objeto que devuelva debe heredar de CertificadoOrigen
        CertificadoOrigen certificadoOrigenObj = certificadoOrigenLogic.getCertificadoOrigenById(ordenObj.getIdFormatoEntidad(), formato);
        
        Solicitante usuarioSolicitanteFormato = formatoLogic.getUsuarioDetail(ordenObj.getOrden(),ordenObj.getMto());
             
        
        if (ConstantesCO.CO_SOLICITUD.equals(formato.toLowerCase())) { // Si estamos ante una solicitud de certificado
        	
            puedeTransmitir = "S";
            RequestUtil.setAttributes(request, "CERTIFICADO_ORIGEN.", certificadoOrigenObj);

        	/********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/

            //cargarInformacionCalificacionOrigen(ordenObj, certificadoOrigenObj.getCalificacionUoId(), request, false);

        	/********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/
           
            //Se carga las direcciones anexas(adicionales) de SUNAT             
            //if (usuario.getNumRUC()!=null) {
            if (usuarioSolicitanteFormato.getNumeroDocumento()!=null) {
    	        HashUtil<String, String> filtro = new HashUtil<String, String>();
    	        filtro.put("filter", "ruc="+usuarioSolicitanteFormato.getNumeroDocumento());    	        
    	        OptionList listaDireccionesEA = DatosRUCElementLoader.cargarListaEstablecimientosAnexosYDomicilioLegal(filtro);
    	        if(listaDireccionesEA!=null && listaDireccionesEA.size()>0){
    	        	request.setAttribute("listaDireccionesEA", listaDireccionesEA);	
    	        } else {    
    	        		if(certificadoOrigenObj.getDireccionAdicional()==null||certificadoOrigenObj.getDireccionAdicional().toString().equals("")) {
    	        			request.setAttribute("CERTIFICADO_ORIGEN.direccionAdicional", usuarioSolicitanteFormato.getDireccion());
    	        		}
    	        }
            }
            
            HashUtil<String, Object> filterDetalles = new HashUtil<String, Object>();
            filterDetalles.put("id", ordenObj.getIdFormatoEntidad());
           
            // Facturas
            Table tFacturas = ibatisService.loadGrid("certificadoOrigen.factura.grilla", filterDetalles);
            request.setAttribute("tFacturas", tFacturas);

            //Mercancias
            Table tMercancias = ibatisService.loadGrid("certificadoOrigen.mercancia.grilla", filterDetalles);
            request.setAttribute("tMercancias", tMercancias);
            request.setAttribute("tMercanciasSize", tMercancias.size());

            //Productores  NPCS: A:CO-008-2017 16/04/2018
            if(ordenObj.getIdAcuerdo()==26 && (ConstantesCO.CO_SOLICITUD.equals(formato.toLowerCase())||ConstantesCO.CO_REEMPLAZO.equals(formato.toLowerCase()))) {
            	boolean ok = true;
            	Table tProductores = ibatisService.loadGrid("certificadoOrigen."+formato.toLowerCase()+".productores.grilla", filterDetalles);
	            for(int i=0; i<tProductores.size(); i++ ) {
	            	 Row row = tProductores.getRow(i);
                     Object direccionObj = row.getCell("DIRECCION").getValue();
                     if(direccionObj==null || direccionObj.toString()=="") {
                    	 ok = false;
                     }
	            }
	            
	            if(!ok) {
	            	puedeTransmitir = "N";
	            	messageList.add(new Message("co.falta.productores"));
	            }
	            
	            request.setAttribute("tProductores", tProductores);
	            request.setAttribute("tProductoresSize", tProductores.size());
            }
            //Validaciones para la transmisi�n
          //NPCS 26/02/2019 CO-2019-001 IO Alianza
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("formato", ordenObj.getIdFormato());
        	filter.put("pais", certificadoOrigenObj.getPaisIsoIdAcuerdoInt());
        	filter.put("acuerdo", certificadoOrigenObj.getAcuerdoInternacionalId());
        	filter.put("entidad", certificadoOrigenObj.getEntidadId());
        	
        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	ibatisService.loadElement("comun.mostrar_mensaje_FD.element", filter);
        	
        	if("S".equals(filter.getString("mostrarMensajeFD"))) {
        		messageList.add(new Message("co.certificado.entidad.en.proceso.fd", new String[]{""+certificadoOrigenObj.getNombreEntidad()}));
        	}
            
            //Direccion Adicional
            if (certificadoOrigenObj.getDireccionAdicional() == null || certificadoOrigenObj.getDireccionAdicional().toString().equals("")){
            	messageList.add(new Message("co.certificado.falta.direccionadicional.certificado"));
                puedeTransmitir = "N";            	
            }

            if (!ComponenteOrigenUtil.validarInformacionGeneralSegunAcuerdo(ordenObj.getIdAcuerdo(), certificadoOrigenObj)) {
            	messageList.add(new Message("co.certificado.falta.datos.certificado"));
                puedeTransmitir = "N";
            }

            // Validamos la cantidad de Facturas
            if (tFacturas.size() == 0 && acuerdoRequiereFactura(ordenObj.getIdAcuerdo())) {
                messageList.add(new Message("co.certificado.falta.factura"));
                puedeTransmitir = "N";
            }
            
            //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
            String estadoAcuerdoPais = validaAcuerdoPais(ordenObj.getOrden());
            if("I".equals(estadoAcuerdoPais.toString())){            	
            	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
            }
            request.setAttribute("estadoAcuerdoPais",estadoAcuerdoPais);
            
            //Validamos si la factura ya fue registrada y muestra un mensaje informativo
            String mensajeFactura = validaFacturaRegistrada(ordenObj.getOrden(), ordenObj.getMto(), null);
            if(!"".equals(mensajeFactura.toString())){            	
            	messageList.add(new Message("message", mensajeFactura));
            }
            
            // Verificar si hay adjuntos
            cuenta = formatoLogic.cuentaAdjuntosRequeridos(ordenObj);
            if(cuenta > 0){
                messageList.add(new Message("falta.adjuntos"));
                puedeTransmitir = "N";
            }
            request.setAttribute("mostrarBotonFactura", formatoLogic.permiteCrearFactura(ordenObj.getOrden(), ordenObj.getIdFormatoEntidad()));

            if (ordenObj.getSuce() != null){
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesPendientes(new Long(ordenObj.getSuce())));
            } else {
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesSolicitudPendientes(ordenObj.getOrden(), ordenObj.getMto()));
            }

            int adjFacturasFaltantes = this.formatoLogic.cuentaAdjuntosFacturaRequeridos(ordenObj.getIdFormatoEntidad(), formato);
            if (adjFacturasFaltantes > 0){
            	 messageList.add(new Message("co.falta.adjunto.factura", adjFacturasFaltantes));
                 puedeTransmitir = "N";
            }

            //puedeTransmitir = alertasDJsPendientes(puedeTransmitir, ordenObj.getEstadoRegistro(), certificadoOrigenObj.getAcuerdoInternacionalId(), tMercancias, messageList);

            String infCalifCompletas = verificarTableCalifDJCompleta(ordenObj.getIdFormatoEntidad(), formato, tMercancias, messageList, request, ordenObj.getIdAcuerdo(), ordenObj.getOrden());

            if (ConstantesCO.OPCION_NO.equalsIgnoreCase(infCalifCompletas)) {
                puedeTransmitir = infCalifCompletas;
            }

            if (ConstantesCO.OPCION_SI.equals(request.getParameter("djCalificada"))) {
            	request.setAttribute("mostrarTabMercancias", ConstantesCO.OPCION_SI);
            }

            request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(ordenObj.getIdAcuerdo()) );

        } else if (ConstantesCO.CO_DUPLICADO.equals(formato.toLowerCase())) { // Si estamos ante una solicitud de duplicado de certificado

        	//20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
            String estadoAcuerdoPais = validaAcuerdoPais(ordenObj.getOrden());
            if("I".equals(estadoAcuerdoPais.toString())){            	
            	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
            }
            
        	CODuplicado duplicadoObj = (CODuplicado) certificadoOrigenObj;
            RequestUtil.setAttributes(request, formato.toUpperCase() + ".", duplicadoObj); // En este caso ser�a "MCT002."

            request.setAttribute("nombreAcuerdo",duplicadoObj.getNombreAcuerdo());
            request.setAttribute("drEntidad", duplicadoObj.getDrEntidad());
            request.setAttribute("nombrePais",duplicadoObj.getNombrePais());
            request.setAttribute("drOrigen",duplicadoObj.getDrOrigen());
            request.setAttribute("nombreEntidad",duplicadoObj.getNombreEntidad());
            request.setAttribute("fechaGeneracion",duplicadoObj.getFechaDrEntidad());
            request.setAttribute("ordenIdOrigen",duplicadoObj.getOrdenIdOrigen());
            request.setAttribute("mtoOrigen",duplicadoObj.getMtoOrigen());
            request.setAttribute("formatoOrigen",duplicadoObj.getFormatoOrigen());
            // 20141222_RPC: Acta 31 AELC: Mostrar Nro certificado Origen en duplicado
            request.setAttribute("nroReferenciaCertificado", duplicadoObj.getNroReferenciaCertificado());
            request.setAttribute("CERTIFICADO_ORIGEN.direccionAdicional",duplicadoObj.getDireccionAdicional());
            /*request.setAttribute("idAcuerdo",duplicadoObj.getAcuerdoInternacionalId());
            request.setAttribute("idPais",duplicadoObj.getPaisIsoIdAcuerdoInt());
            request.setAttribute("idEntidadCertificadora",duplicadoObj.getEntidadId());
            request.setAttribute("idSede", duplicadoObj.getSedeId());  */

            String descCausal = duplicadoObj.getCausalDuplicadoCo()!=null? certificadoOrigenLogic.getCausalById(duplicadoObj.getCausalDuplicadoCo().toString()):"";

            //RepresentanteLegal representante = formatoLogic.getRepresentanteDetail(ordenObj.getOrden(),ordenObj.getMto());
        	Solicitante solicitante = formatoLogic.getSolicitanteDetail(ordenObj.getOrden(),ordenObj.getMto());
            //Solicitante usuarioFormato = formatoLogic.getUsuarioDetail(usuario);

            /*
            request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
            		new Object [] {""+usuario.getNombreCompleto(), ""+usuario.getNumeroDocumento(), ""+duplicadoObj.getDrEntidad(), "<span id='idCausal'>"+descCausal+"</span>"}));
             */
            request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
            		new Object [] {""+solicitante.getNombre(), ""+solicitante.getNumeroDocumento(),
    				               ""+ (duplicadoObj.getDrEntidad()!=null ? duplicadoObj.getDrEntidad():""),
    				               "<span id='idCausal'>"+descCausal+"</span>"}));

            //Validaciones para la transmisi�n
            puedeTransmitir = "S";//2018.06.09 JMC ProduccionControlada

            if((duplicadoObj.getAceptacion()==null) || (duplicadoObj.getAceptacion().equals("N"))){
            	puedeTransmitir="N";
            	messageList.add(new Message("co.falta.aceptacion.duplicado"));
            }

            try {
            	Long adjuntoIdFirma = this.certificadoOrigenLogic.getAdjunFirmaId(null, duplicadoObj.getDrIdOrigen());
            	request.setAttribute("adjuntoIdFirma", adjuntoIdFirma);
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
			}


            /*if (ordenObj.getSuce() != null){
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesPendientes(new Long(ordenObj.getSuce())));
            } else {
            	request.setAttribute("notifPendientes", 0);
            }*/
            
            //Validamos si la factura ya fue registrada y muestra un mensaje informativo
            String mensajeFactura = validaFacturaRegistrada(ordenObj.getOrden(), ordenObj.getMto(), null);
            if(!"".equals(mensajeFactura.toString())){            	
            	messageList.add(new Message("message", mensajeFactura));
            }
            
            // Verificar si hay adjuntos
            cuenta = formatoLogic.cuentaAdjuntosRequeridos(ordenObj);
            if(cuenta > 0){
                messageList.add(new Message("falta.adjuntos"));
                puedeTransmitir = "N";
            }

        } else if (ConstantesCO.CO_ANULACION.equals(formato.toLowerCase())) {
        	
        	//20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
            String estadoAcuerdoPais = validaAcuerdoPais(ordenObj.getOrden());
            if("I".equals(estadoAcuerdoPais.toString())){            	
            	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
            }
            
        	COAnulacion anulacionObj = (COAnulacion) certificadoOrigenObj;

            RequestUtil.setAttributes(request, formato.toUpperCase() + ".", anulacionObj); // En este caso ser�a "MCT004."

            request.setAttribute("nombreAcuerdo",anulacionObj.getNombreAcuerdo());
            request.setAttribute("drEntidad", anulacionObj.getDrEntidad());
            request.setAttribute("nombrePais",anulacionObj.getNombrePais());
            request.setAttribute("drOrigen",anulacionObj.getDrOrigen());
            request.setAttribute("nombreEntidad",anulacionObj.getNombreEntidad());
            request.setAttribute("fechaGeneracion",anulacionObj.getFechaDrEntidad());
            request.setAttribute("ordenIdOrigen",anulacionObj.getOrdenIdOrigen());
            request.setAttribute("mtoOrigen",anulacionObj.getMtoOrigen());
            request.setAttribute("formatoOrigen",anulacionObj.getFormatoOrigen());
            request.setAttribute("CERTIFICADO_ORIGEN.direccionAdicional",anulacionObj.getDireccionAdicional());
            /*request.setAttribute("idAcuerdo",anulacionObj.getAcuerdoInternacionalId());
            request.setAttribute("idPais",anulacionObj.getPaisIsoIdAcuerdoInt());
            request.setAttribute("idEntidadCertificadora",anulacionObj.getEntidadId());
            request.setAttribute("idSede", anulacionObj.getSedeId());  */

            //Validaciones para la transmisi�n
            puedeTransmitir = "S";
            if((anulacionObj.getAceptacion()==null) || (anulacionObj.getAceptacion().equals("N"))){
            	puedeTransmitir="N";
            	messageList.add(new Message("co.falta.aceptacion.anulacion"));
            }

            Solicitante solicitante = formatoLogic.getSolicitanteDetail(ordenObj.getOrden(),ordenObj.getMto());
            UsuarioCO usrFilter = new UsuarioCO();

            if (ordenObj.getIdUsuario().equals(usuario.getIdUsuario())) {
            	 usrFilter.setIdUsuario(usuario.getIdUsuario());
            } else {
            	usrFilter.setIdUsuario(ordenObj.getIdUsuario());
            }
            //RepresentanteLegal representante = formatoLogic.getRepresentanteDetail(ordenObj.getOrden(),ordenObj.getMto());
            Solicitante usuarioFormato = formatoLogic.getUsuarioDetail(usrFilter);

            request.setAttribute("descAnulacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.textoAnulacion",
            		new Object [] {""+solicitante.getNombre(), ""+solicitante.getNumeroDocumento(),
    				""+(usuarioFormato!=null?usuarioFormato.getNombre():""),
    				""+ (anulacionObj.getDrEntidad()!=null?anulacionObj.getDrEntidad():"")}));

            try {
            	Long adjuntoIdFirma = this.certificadoOrigenLogic.getAdjunFirmaId(null, anulacionObj.getDrIdOrigen());
            	request.setAttribute("adjuntoIdFirma", adjuntoIdFirma);
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
			}
        } else if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(formato.toLowerCase())) { // Si estamos ante una solicitud de certificado
        	
        	RequestUtil.setAttributes(request, "CERTIFICADO_ORIGEN.", certificadoOrigenObj);

            COCalificacionAnticipada calificacionObj = (COCalificacionAnticipada) certificadoOrigenObj;
            RequestUtil.setAttributes(request, formato.toUpperCase() + ".", calificacionObj); // En este caso ser�a "MCT005."

        	/********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/

            cargarInformacionCalificacionOrigen(ordenObj, calificacionObj.getCalificacionUoId(), request, false);

        	/********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/

            //Validaciones para la transmisi�n
            puedeTransmitir = "S";
            
            //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
            String estadoAcuerdoPais = validaAcuerdoPais(ordenObj.getOrden());
            if("I".equals(estadoAcuerdoPais.toString())){            	
            	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
            }
            request.setAttribute("estadoAcuerdoPais",estadoAcuerdoPais);
            
            //Se carga las direcciones anexas(adicionales) de SUNAT            
            //if (usuario.getNumRUC()!=null) {
            if (usuarioSolicitanteFormato.getNumeroDocumento()!=null) {
    	        HashUtil<String, String> filtro = new HashUtil<String, String>();
    	        filtro.put("filter", "ruc="+usuarioSolicitanteFormato.getNumeroDocumento());    	        
    	        OptionList listaDireccionesEA = DatosRUCElementLoader.cargarListaEstablecimientosAnexosYDomicilioLegal(filtro);
    	        if(listaDireccionesEA!=null && listaDireccionesEA.size()>0){
    	        	request.setAttribute("listaDireccionesEA", listaDireccionesEA);	
    	        } else {    	
    	        	if(certificadoOrigenObj.getDireccionAdicional()==null||certificadoOrigenObj.getDireccionAdicional().toString().equals("")) {
    	        		request.setAttribute("CERTIFICADO_ORIGEN.direccionAdicional", usuarioSolicitanteFormato.getDireccion());
    	        	}
    	        }
            }
    	    
            //Direccion Adicional
            if (certificadoOrigenObj.getDireccionAdicional() == null || certificadoOrigenObj.getDireccionAdicional().toString().equals("")){
            	messageList.add(new Message("co.certificado.falta.direccionadicional.certificado"));
                puedeTransmitir = "N";            	
            }

            if (calificacionObj.getCalificacionUoId() == null) { // Si no se ha creado una DJ para el formato no se puede transmitir
            	messageList.add(new Message("co.certificado.falta.calificacion"));
            	puedeTransmitir = "N";

            } else {
	            /********************* Agregado para la calificacion (Inicio) ********************/

	            String califDJCompleta = verificarCalifDJCompleta(calificacionObj.getCalificacionUoId(), messageList, request, ordenObj.getIdAcuerdo(), ordenObj.getOrden());

	            if (ConstantesCO.OPCION_SI.equals(puedeTransmitir)) {
	            	puedeTransmitir = califDJCompleta;
	            }

	            /********************* Agregado para la calificacion (Fin)   ********************/

				/*if ( ConstantesCO.OPCION_NO.equals(djObj.getInformacionCompleta()) ) {
					puedeTransmitir = "N";
					messageList.add(new Message("co.certificado.falta.dj.completa", new String[]{"1"}));
				}*/










				// TO DO ORIGEN: Verificar la l�gica de este bloque
				/* if ( !ConstantesCO.DJ_ESTADO_PENDIENTE_RESPUESTA_ENTIDAD.equals(djObj.getEstadoRegistro()) &&
					 !ConstantesCO.DJ_ESTADO_PENDIENTE_GENERACION_DR.equals(djObj.getEstadoRegistro()) &&
					 !ConstantesCO.DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO.equals(djObj.getEstadoRegistro()) &&
					 !ConstantesCO.DJ_ESTADO_APROBADA.equals(djObj.getEstadoRegistro()) ) {

					puedeTransmitir = "N";
					if ( ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(djObj.getEstadoRegistro()) ||
						 ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(djObj.getEstadoRegistro()) ) {

						messageList.add(new Message("co.certificado.falta.dj.sin.aprobar", new String[]{"1"}));

					} else if ( ConstantesCO.DJ_ESTADO_BORRADOR.equals(djObj.getEstadoRegistro()) ||
								ConstantesCO.DJ_ESTADO_RECHAZADA.equals(djObj.getEstadoRegistro()) ) {

						HashUtil<String, Object> filter = new HashUtil<String, Object>();
				    	filter.put("idAcuerdo", certificadoOrigenObj.getAcuerdoInternacionalId());

			            String tValidacion = "N";
			            try {
			            	tValidacion = (String) ibatisService.loadObject("tipo.validacion.productor.acuerdo", filter);
						} catch (Exception e) {
							// TODO: handle exception
						}

			            if (ConstantesCO.OPCION_SI.equals(tValidacion)) {
			            	if ( ConstantesCO.DJ_ESTADO_BORRADOR.equals(djObj.getEstadoRegistro()) ) {
			            		messageList.add(new Message("co.certificado.falta.dj.estado.pendiente.envio.validacion", new String[]{"1"}));
			            	} else {
			            		messageList.add(new Message("co.certificado.falta.dj.estado.rechazado", new String[]{"1"}));
			            	}
			            }

					} else {

						messageList.add(new Message("co.certificado.falta.dj.estado.invalido", new String[]{"1"}));

					}
				}*/

            }

            if (ordenObj.getSuce() != null){
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesPendientes(new Long(ordenObj.getSuce())));
            } else {
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesSolicitudPendientes(ordenObj.getOrden(), ordenObj.getMto()));
            }

            request.setAttribute("esRecarga", request.getParameter("esRecarga")!=null?request.getParameter("esRecarga"):"");

        } else if (ConstantesCO.CO_REEMPLAZO.equals(formato.toLowerCase())) {
        	//20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
            String estadoAcuerdoPais = validaAcuerdoPais(ordenObj.getOrden());
            if("I".equals(estadoAcuerdoPais.toString())){            	
            	messageList.add(new Message("consulta_orden.mensaje.formato.acuerdo_no_vigente"));
            }
            request.setAttribute("estadoAcuerdoPais",estadoAcuerdoPais);
            
        	COReemplazo reemplazoObj = (COReemplazo) certificadoOrigenObj;

            //RequestUtil.setAttributes(request, formato.toUpperCase() + ".", reemplazoObj); // En este caso ser�a "MCT003."
            RequestUtil.setAttributes(request, "CERTIFICADO_ORIGEN.", reemplazoObj); // En este caso ser�a "MCT003."
            
            //Se carga las direcciones anexas(adicionales) de SUNAT
            if (usuarioSolicitanteFormato.getNumeroDocumento()!=null) {
    	        HashUtil<String, String> filtro = new HashUtil<String, String>();
    	        filtro.put("filter", "ruc="+usuarioSolicitanteFormato.getNumeroDocumento());    	        
    	        OptionList listaDireccionesEA = DatosRUCElementLoader.cargarListaEstablecimientosAnexosYDomicilioLegal(filtro);
    	        if(listaDireccionesEA!=null && listaDireccionesEA.size()>0){
    	        	request.setAttribute("listaDireccionesEA", listaDireccionesEA);	
    	        } else {   
    	        	if(certificadoOrigenObj.getDireccionAdicional()==null||certificadoOrigenObj.getDireccionAdicional().toString().equals("")) {
    	        		request.setAttribute("CERTIFICADO_ORIGEN.direccionAdicional", usuarioSolicitanteFormato.getDireccion());    
    	        	}
    	        }
            }
    	    
            //Direccion Adicional
            if (certificadoOrigenObj.getDireccionAdicional() == null || certificadoOrigenObj.getDireccionAdicional().toString().equals("")){
            	messageList.add(new Message("co.certificado.falta.direccionadicional.certificado"));
                puedeTransmitir = "N";            	
            }
            
            HashUtil<String, Object> filterDetalles = new HashUtil<String, Object>();
            filterDetalles.put("id", ordenObj.getIdFormatoEntidad());
            
            System.out.println("Imprimiendo listado productores para el acuerdo: "+ordenObj.getIdAcuerdo());
 
            //Productores  NPCS: A:CO-008-2017 16/04/2018
            if(ordenObj.getIdAcuerdo()==26) {
            	boolean ok = true;
            	Table tProductores = ibatisService.loadGrid("certificadoOrigen."+formato.toLowerCase()+".productores.grilla", filterDetalles);
	            for(int i=0; i<tProductores.size(); i++ ) {
	            	 Row row = tProductores.getRow(i);
                     Object direccionObj = row.getCell("DIRECCION").getValue();
                     if(direccionObj==null || direccionObj.toString()=="") {
                    	 ok = false;
                     }
	            }
	            
	            if(!ok) {
	            	puedeTransmitir = "N";
	            	messageList.add(new Message("co.falta.productores"));
	            }
	            
	            request.setAttribute("tProductores", tProductores);
	            request.setAttribute("tProductoresSize", tProductores.size());
            }
            
            //NPCS 26/02/2019 CO-2019-001 IO Alianza
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("formato", ordenObj.getIdFormato());
        	filter.put("pais", certificadoOrigenObj.getPaisIsoIdAcuerdoInt());
        	filter.put("acuerdo", certificadoOrigenObj.getAcuerdoInternacionalId());
        	filter.put("entidad", certificadoOrigenObj.getEntidadId());
        	
        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	ibatisService.loadElement("comun.mostrar_mensaje_FD.element", filter);
        	
        	if("S".equals(filter.getString("mostrarMensajeFD"))) {
        		messageList.add(new Message("co.certificado.entidad.en.proceso.fd", new String[]{""+certificadoOrigenObj.getNombreEntidad()}));
        	}
            

            request.setAttribute("nombreAcuerdo",reemplazoObj.getNombreAcuerdo());
            request.setAttribute("drEntidad", reemplazoObj.getDrEntidad());
            request.setAttribute("nombrePais",reemplazoObj.getNombrePais());
            request.setAttribute("drOrigen",reemplazoObj.getDrOrigen());
            request.setAttribute("nombreEntidad",reemplazoObj.getNombreEntidad());
            request.setAttribute("fechaGeneracion",reemplazoObj.getFechaDrEntidad());
            request.setAttribute("ordenIdOrigen",reemplazoObj.getOrdenIdOrigen());
            request.setAttribute("mtoOrigen",reemplazoObj.getMtoOrigen());
            request.setAttribute("formatoOrigen",reemplazoObj.getFormatoOrigen());
            // 20141222_RPC: Acta 31 AELC: Mostrar Nro certificado Origen en duplicado
            request.setAttribute("nroReferenciaCertificado", reemplazoObj.getNroReferenciaCertificado());
            request.setAttribute("MCT003.observacionEvalOrigen", reemplazoObj.getObservacionEvalOrigen());
            
            /*request.setAttribute("idAcuerdo",anulacionObj.getAcuerdoInternacionalId());
            request.setAttribute("idPais",anulacionObj.getPaisIsoIdAcuerdoInt());
            request.setAttribute("idEntidadCertificadora",anulacionObj.getEntidadId());
            request.setAttribute("idSede", anulacionObj.getSedeId());  */

            // Facturas
            Table tFacturas = ibatisService.loadGrid("certificadoOrigen.factura.mct003.grilla", filterDetalles);
            request.setAttribute("tFacturas", tFacturas);

            //Mercancias
            Table tMercancias = ibatisService.loadGrid("certificadoOrigen.mercancia.grilla", filterDetalles);
            request.setAttribute("tMercancias", tMercancias);
            request.setAttribute("tMercanciasSize", tMercancias.size());


            //Validaciones para la transmisi�n
            puedeTransmitir = "S";
            /*if((reemplazoObj.getAceptacion()==null) || (reemplazoObj.getAceptacion().equals("N"))){
            	puedeTransmitir="N";
            	messageList.add(new Message("co.falta.aceptacion.anulacion"));
            }*/

            if (reemplazoObj.getCausalReemplazoCo() == null){
                messageList.add(new Message("co.reemplazo.falta.motivo"));
                puedeTransmitir = "N";
            }
      
            //Validamos si la factura ya fue registrada y muestra un mensaje informativo
            String mensajeFactura = validaFacturaRegistrada(ordenObj.getOrden(), ordenObj.getMto(), null);
            if(!"".equals(mensajeFactura.toString())){            	
            	messageList.add(new Message("message", mensajeFactura));
            }
            
            // Verificar si hay adjuntos
            cuenta = formatoLogic.cuentaAdjuntosRequeridos(ordenObj);
            if(cuenta > 0){
                messageList.add(new Message("falta.adjuntos"));
                puedeTransmitir = "N";
            }
            request.setAttribute("mostrarBotonFactura", formatoLogic.permiteCrearFactura(ordenObj.getOrden(), ordenObj.getIdFormatoEntidad()));

            puedeTransmitir = alertasDJsPendientes(puedeTransmitir, ordenObj.getEstadoRegistro(), "S".equals(ordenObj.getCertificadoReexportacion()), certificadoOrigenObj.getAcuerdoInternacionalId(), tMercancias, messageList);

            if (ConstantesCO.OPCION_SI.equals(request.getParameter("djCalificada"))) {
            	request.setAttribute("mostrarTabMercancias", ConstantesCO.OPCION_SI);
            }

            if (ordenObj.getSuce() != null){
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesPendientes(new Long(ordenObj.getSuce())));
            } else {
            	request.setAttribute("notifPendientes", formatoLogic.cuentaNotificacionesSolicitudPendientes(ordenObj.getOrden(), ordenObj.getMto()));
            }

            request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(ordenObj.getIdAcuerdo()) );

            try {
            	Long adjuntoIdFirma = this.certificadoOrigenLogic.getAdjunFirmaId(null, new Long( reemplazoObj.getDrIdOrigen()));
            	request.setAttribute("adjuntoIdFirma", adjuntoIdFirma);
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
			}
        }

       	if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato) || ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase(formato)) {
       		HashMap<String, Object> filter = new HashMap<String, Object>();
       		filter.put("acuerdoId", ordenObj.getIdAcuerdo());
       		filter.put("ordenId", ordenObj.getOrden());
       		filter.put("mto", ordenObj.getMto());
       		Message msg = this.certificadoOrigenLogic.validarCertificadoEspacioDet(filter);
			if (msg != null) {
				messageList.add(msg);
			}

        }


       	if (Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) {
        	if (request.getAttribute("notifPendientes") != null && "1".equals(request.getAttribute("notifPendientes").toString())){
        		boolean notifEnviada = false;
        		for (Object object : messageList) {
					if (object instanceof Message) {
						if (((Message) object).getMessageKey().equals("co.resolutor.evaluador.enviarBorradorNotificacionSubsanacionOrden.success")){
							notifEnviada = true;
							break;
						}
					}
				}

        		if (!notifEnviada) {
        			messageList.add(new Message("co.resolutor.supervisor.notificacion.pendiente"));
        		}
        	}
    	}
        
        request.setAttribute("coId", certificadoOrigenObj.getCoId());
        request.setAttribute("idAcuerdo", certificadoOrigenObj.getAcuerdoInternacionalId());
        request.setAttribute("idPais", certificadoOrigenObj.getPaisIsoIdAcuerdoInt());
        request.setAttribute("idEntidadCertificadora", certificadoOrigenObj.getEntidadId());
        request.setAttribute("tipoCertificado", !"S".equals(ordenObj.getCertificadoReexportacion()) ? "Origen" : "Reexportaci�n");
        request.setAttribute("idSede", certificadoOrigenObj.getIdSede());
        request.setAttribute("nombreSede", certificadoOrigenObj.getNombreSede());
        request.setAttribute("puedeTransmitir", puedeTransmitir);
    }

	/********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/

    protected String verificarTableCalifDJCompleta(Long idFormatoEntidad, String formato, Table tMercancias, MessageList messageList, HttpServletRequest request, Integer idAcuerdo, Long ordenId){
    	String res = ConstantesCO.OPCION_SI;

        // Validamos la cantidad de Mercanc�as
        if (tMercancias.size() == 0) {
            messageList.add(new Message("co.certificado.falta.mercancia"));
            res = "N";
        } else {

	    	int numDataProductorIncompleta = 0;
	    	int numDataDJIncompleta = 0;
	    	int numDataMercanciaIncompleta = 0;

	    	Row objRow;
	    	Map resVerifCalifCompleta;
	    	Map resVerifDJCompleta;
	    	CalificacionOrigen calificacionOrigen;
	    	DeclaracionJurada djObj;
	    	for (Object object : tMercancias) {
	    		objRow = (Row) object;

	    		CertificadoOrigenMercancia mercanciaObj = certificadoOrigenLogic.getCertificadoOrigenMercanciaById(idFormatoEntidad, new Integer(objRow.getCell("SECUENCIAMERCANCIA").getValue().toString()), formato);
                
	    		// Si la mercancia no tiene descripcion o es el Acuerdo Honduras y su factura tiene el indicador de Tercer pais apagado y la mercancia no tiene Valor Facturado
            	if ((mercanciaObj!=null && mercanciaObj.getDescripcion() == null)) {
            		numDataMercanciaIncompleta++;
            	}

	    		// Si la Mercancia NO esta utilizando una Calificacion origen ya existente, entonces debo validar la Calificacion origen
	            if (!ConstantesCO.CALIFICACION_ORIGEN_EXISTENTE.equals(mercanciaObj.getSustentoCalifMercancia())) {

		    		if (objRow.getCell("CALIFICACIONUOID").getValue() != null){
		    			resVerifCalifCompleta = verificarCalificacionCompleta(new Long(objRow.getCell("CALIFICACIONUOID").getValue().toString()), new MessageList(), request, idAcuerdo);
		    	    	if ( ConstantesCO.OPCION_NO.equalsIgnoreCase( (String) resVerifCalifCompleta.get("califCompleta")) ){
		    	    		numDataProductorIncompleta++;
		    	    	}

		    	    	if (resVerifCalifCompleta.get("calificacionOrigen") != null) {
		    		    	calificacionOrigen = (CalificacionOrigen) resVerifCalifCompleta.get("calificacionOrigen");

		    		    	resVerifDJCompleta = verificarDJCompleta(calificacionOrigen, new MessageList(), request, idAcuerdo, ordenId);
		    		    	if ( ConstantesCO.OPCION_NO.equalsIgnoreCase( (String) resVerifDJCompleta.get("djCompleta")) ){
		    		    		numDataDJIncompleta++;
		        	    	}

		    		    	if ( resVerifDJCompleta.get("djObj")!=null ) {
		    		    		djObj = (DeclaracionJurada) resVerifDJCompleta.get("djObj");
		    			        if ( !((String) resVerifDJCompleta.get("djCompleta")).equals( djObj.getInformacionCompleta() ) ) {
		    			        	djObj.setInformacionCompleta((String) resVerifDJCompleta.get("djCompleta"));
		    			        	this.certificadoOrigenLogic.actualizarFlgDjCompleta(calificacionOrigen.getCalificacionUoId(), (String) resVerifDJCompleta.get("djCompleta"));
		    			        }

		    		    	}

		    	    	}
		    		}
	    	}
	    	}

	    	if (numDataMercanciaIncompleta > 0) {
	    		res = ConstantesCO.OPCION_NO;
	    		messageList.add(new Message("co.falta.registro.table.mercancia", new String[]{""+numDataMercanciaIncompleta}));
	    	}

	    	if ( numDataProductorIncompleta > 0) {
	    		res = ConstantesCO.OPCION_NO;
	    		messageList.add(new Message("co.falta.registro.table.calif.productores", new String[]{""+numDataProductorIncompleta}));
	    	}

	    	if ( numDataDJIncompleta > 0) {
	    		res = ConstantesCO.OPCION_NO;
	    		messageList.add(new Message("co.falta.registro.table.dj", new String[]{""+numDataDJIncompleta}));
	    	}

        }

    	return res;
    }

    protected String validaFacturaRegistrada (Long ordenId, Integer mto, String nroFactura){
    	String res="";
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("resultado", null);
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		filter.put("nroFactura", nroFactura);

    	try {
			ibatisService.executeSPWithObject("certificado.factura.fn.validaExiste",filter);
			if(filter.get("resultado")!=null) {
				res= filter.get("resultado").toString();
			}
		} catch (Exception e) {
			logger.error("Error en CertificadoOrigneFormatoController.validaFacturaRegistrada", e);
		}
    	return res;
    }
    
    //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
    protected String validaAcuerdoPais (Long ordenId){
    	String res="";
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("result", null);
		filter.put("ordenId", ordenId);
		
    	try {
			ibatisService.executeSPWithObject("valida_acuerdo_pais_vigente",filter);
			if(filter.get("result")!=null) {
				res= filter.get("result").toString();
			}
		} catch (Exception e) {
			logger.error("Error en CertificadoOrigneFormatoController.validaAcuerdoPais", e);
			
		}
       	return res;
    }

    
    /**
     * Verifica que los datos de la Calificaci�n y la DJ este completos
     *
     * @param djObj
     * @param puedeTransmitir
     * @param messageList
     */
    protected String verificarCalifDJCompleta(Long calificacionUoId, MessageList messageList, HttpServletRequest request, Integer idAcuerdo, Long ordenId){
    	String res;

    	Map resVerifCalifCompleta = verificarCalificacionCompleta(calificacionUoId, messageList, request, idAcuerdo);
    	res = (String) resVerifCalifCompleta.get("califCompleta");

    	if (resVerifCalifCompleta.get("calificacionOrigen") != null) {
	    	CalificacionOrigen calificacionOrigen = (CalificacionOrigen) resVerifCalifCompleta.get("calificacionOrigen");

	    	Map resVerifDJCompleta = verificarDJCompleta(calificacionOrigen, messageList, request, idAcuerdo, ordenId);
	    	String djCompleta = (String) resVerifDJCompleta.get("djCompleta");
	    	if ( ConstantesCO.OPCION_NO.equalsIgnoreCase( djCompleta )  ) {
	    		res = djCompleta;
	    	}

	    	if ( resVerifDJCompleta.get("djObj")!=null ) {
	    		DeclaracionJurada djObj = (DeclaracionJurada) resVerifDJCompleta.get("djObj");
		        if ( !res.equals( djObj.getInformacionCompleta() ) ) {
		        	djObj.setInformacionCompleta(djCompleta);
		        	this.certificadoOrigenLogic.actualizarFlgDjCompleta(calificacionOrigen.getCalificacionUoId(), res);
		        }

	    	}
    	}

    	return res;
    }

	private Map<String, Object> verificarCalificacionCompleta(Long calificacionUoId, MessageList messageList, HttpServletRequest request, Integer idAcuerdo){
		Map res = new HashMap<String, Object>();
	    String califCompleta = ConstantesCO.OPCION_SI;

    	// Obtenemos la calificacion TO DO ORIGEN: Poner validaciones de transmisi�n de campos de la calificaci�n aqu�
    	CalificacionOrigen calificacionOrigen = certificadoOrigenLogic.getCalificacionOrigenById(calificacionUoId);

    	if (calificacionOrigen.getTipoRolCalificacion() == null) {

    		califCompleta = ConstantesCO.OPCION_NO;
    		messageList.add(new Message("co.falta.registro.calificacion"));

    	} else {

    		Table tProductoresCalificacion = (Table) request.getAttribute("tProductoresCalificacion");

    		if (tProductoresCalificacion == null) {
                // Cargamos los productores asociados a la calificaci�n
                HashUtil<String, Object> filterDetalles = new HashUtil<String, Object>();
                filterDetalles.put("calificacionUoId", calificacionUoId);
                tProductoresCalificacion = ibatisService.loadGrid("calificacionOrigen.productor.grilla", filterDetalles);
    		}

    		if (!ConstantesCO.TIPO_ROL_PRODUCTOR_EXPORTADOR.equals(calificacionOrigen.getTipoRolCalificacion().toString()) &&
    			!ConstantesCO.TIPO_ROL_PRODUCTOR.equals(calificacionOrigen.getTipoRolCalificacion().toString())) {

    			if (tProductoresCalificacion == null || tProductoresCalificacion.size() == 0) {
    				califCompleta = ConstantesCO.OPCION_NO;
		        	messageList.add(new Message("co.falta.dj.productor"));
    			}

    			if (ConstantesCO.TIPO_ROL_EXPORTADOR_APODERADO.equals(calificacionOrigen.getTipoRolCalificacion().toString()) ||
    	    		ConstantesCO.TIPO_ROL_ACOPIADOR_APODERADO.equals(calificacionOrigen.getTipoRolCalificacion().toString())) {

	        		int numAdjProductorFaltante = 0;
	        		Row objRow;
	            	for (Object object : tProductoresCalificacion) {
	    	    		objRow = (Row) object;

	    				if (objRow.getCell("ADJUNTOID").getValue() == null){
	    					numAdjProductorFaltante++;
	    				}
	    	    	}

	    	    	if ( numAdjProductorFaltante > 0 ) {
	    	    		califCompleta = ConstantesCO.OPCION_NO;

	    				messageList.add(new Message("co.certificado.falta.calificacion.adjunto.productor", new String[]{""+numAdjProductorFaltante}));
	    	    	}
	    		}
    		}
	    }

    	res.put("califCompleta", califCompleta);
    	res.put("calificacionOrigen", calificacionOrigen);
    	return res;

	}

	private boolean verificarCriterioOrigenCompleto(CalificacionOrigen calificacionOrigen, Integer idAcuerdo){
		boolean res = true;

		if (calificacionOrigen.getCumpleCriterioCambioClasif() == null && calificacionOrigen.getCumpleOtroCriterio() == null && calificacionOrigen.getCumpleTotalmenteObtenido() == null){
			res = false;
		} else if ((calificacionOrigen.getSecuenciaNorma() == null || calificacionOrigen.getSecuenciaOrigen() == null) &&
				   (!idAcuerdo.equals(ConstantesCO.AC_SGP_NZ) && !idAcuerdo.equals(ConstantesCO.AC_SGP_AUS) && !idAcuerdo.equals(ConstantesCO.AC_EFTA) && !idAcuerdo.equals(ConstantesCO.AC_TLC_UE))) {
			res = false;
		} 
		
		/* 20140619_JMCA Comentado por TCKT 4563 - C.O, el campo partida_segun_acuerdo de la tabla calificacion_uo fue deprecado funcionalmente
		 * else if (calificacionOrigen.getPartidaSegunAcuerdo() == null &&
				   (!idAcuerdo.equals(ConstantesCO.AC_SGP_UE) && !idAcuerdo.equals(ConstantesCO.AC_EFTA) && !idAcuerdo.equals(ConstantesCO.AC_TLC_UE))) {
			res = false;
		}
		*/
		return res;
	}

	private Map<String, Object> verificarDJCompleta(CalificacionOrigen calificacionOrigen, MessageList messageList, HttpServletRequest request, Integer idAcuerdo, Long ordenId){
		Map<String, Object> res = new HashMap();
		String djCompleta = ConstantesCO.OPCION_SI;

		// Obtenemos la declaraci�n jurada asociada
        DeclaracionJurada djObj = certificadoOrigenLogic.getCODeclaracionJuradaById(calificacionOrigen.getDjId());

    	if ( djObj.getDjId() == null ){
    		djCompleta = ConstantesCO.OPCION_NO;
			messageList.add(new Message("co.certificado.falta.dj.completa", new String[]{"1"}));
    	} else if ("P".equalsIgnoreCase(djObj.getEstadoRegistro()) || "N".equalsIgnoreCase(djObj.getEstadoRegistro()) || "O".equalsIgnoreCase(djObj.getEstadoRegistro()) 
    			|| "Q".equalsIgnoreCase(djObj.getEstadoRegistro()) // 20140922JFR: Se agrega por observacion de correo "Fwd: RV: CASO DECLARACI�N JURADA SIN CRITERIO DE ORIGEN (CAN)" de Katty
    		) {
			int numDJAdjPendientes = this.formatoLogic.cuentaAdjuntosRequeridosDJ(djObj.getDjId());
			if (numDJAdjPendientes > 0) {
				djCompleta = ConstantesCO.OPCION_NO;
				messageList.add(new Message("co.falta.adjunto.dj.individual", new String[]{"" + numDJAdjPendientes}));
			}


			if (!verificarCriterioOrigenCompleto(calificacionOrigen, idAcuerdo)){
				djCompleta = ConstantesCO.OPCION_NO;
        		messageList.add(new Message("co.falta.registro.criterio.origen"));
			}

			
			System.out.println("*****************idAcuerdo: "+idAcuerdo);
			System.out.println("*****************DjId: "+djObj.getDjId());
			System.out.println("*************cuentaDJMateriales: "+this.certificadoOrigenLogic.cuentaDJMateriales(djObj.getDjId()));
			
    		String djConAlertas = validarDJCompleta(djObj, new MessageList(), idAcuerdo, this.certificadoOrigenLogic.cuentaDJMateriales(djObj.getDjId()));

    		if (ConstantesCO.OPCION_NO.equals(djConAlertas)) {
    			djCompleta = ConstantesCO.OPCION_NO;
    			messageList.add(new Message("co.certificado.falta.dj.completa", new String[]{"1"}));
    		}

    		//Verificamos si la DJ requiere ser validada y a�n no lo ha sido
    		HashMap<String, Object> filter = new HashMap<String, Object>();
    		filter.put("ordenId", ordenId);
    		filter.put("tipoRolCalificacion", calificacionOrigen.getTipoRolCalificacion());
    		if (ConstantesCO.OPCION_SI.equalsIgnoreCase(certificadoOrigenLogic.reqValidacionProductor(filter))){
    			filter.put("djId", djObj.getDjId());
    			System.out.println("*************validada: "+certificadoOrigenLogic.djValidada(filter));
    			// 20150707: Acta 35: Setear si DJ ha sido validada(Se usa para alertar al cambiar de Rol de una DJ validada)
    			String djValidada = certificadoOrigenLogic.djValidada(filter);
    			request.setAttribute("djValidada", djValidada);
    			if (ConstantesCO.OPCION_NO.equalsIgnoreCase(djValidada)) {
    				djCompleta = ConstantesCO.OPCION_NO;
        			messageList.add(new Message("co.certificado.falta.dj.completa", new String[]{"1"}));
        		}
    		}

		}

    	res.put("djCompleta", djCompleta);
    	res.put("djObj", djObj);
        return res;
    }

    /**
     * Verifica que los datos de la DJ esten completos
     *
     * @param djObj
     * @param messageList
     * @param puedeTransmitirDJValidada
     * @param idAcuerdo
     * @param numMateriales
     */
    protected String validarDJCompleta(DeclaracionJurada djObj, MessageList messageList, Integer idAcuerdo, int numMateriales){
        //Adjuntos requeridos
    	String puedeTransmitirDJValidada = ConstantesCO.OPCION_SI;

        if (djObj.getDenominacion() == null || "".equals(djObj.getDenominacion())) {
        	messageList.add(new Message("co.falta.dj.producto.data"));
        	puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
        }
        
        //Verifica el % valor FOB
        if (ComponenteOrigenUtil.validarFaltaValorFOBMateriales(idAcuerdo, djObj.getDjId(), ibatisService)){
        	messageList.add(new Message("co.falta.dj.datos.material.porcentajeFOB"));
    		puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
        }
        

    	int cuenta = 0;
        cuenta = formatoLogic.cuentaAdjuntosRequeridosDJ(djObj.getDjId());//, djObj.getTipoRolDj().toString());
        if (cuenta > 0) {
        	messageList.add(new Message("co.falta.adjunto.dj.individual", new String[]{""+cuenta}));
        	puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
        }

        if (numMateriales == 0 && !idAcuerdo.equals(ConstantesCO.AC_CUBA)) { //&& ConstantesCO.OPCION_NO.equals( djObj.getCumpleTotalmenteObtenido() ) ) {
        	messageList.add(new Message("co.falta.dj.material"));
        	puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
        }

        /*if (idAcuerdo.equals(ConstantesCO.AC_COSTARRICA)) {
        	HashUtil<String, Object> prmt = new HashUtil<String, Object>();
        	prmt.put("djId", djObj.getDjId());

        	if (ConstantesCO.OPCION_NO.equals(this.certificadoOrigenLogic.djMaterialTieneAdj(prmt))){
        		messageList.add(new Message("co.dj.materiales.validacion.falta.adjunto"));
            	puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
        	}
        }*/

        if ( !ComponenteOrigenUtil.consolidadoCorrectoSegunAcuerdo(idAcuerdo, djObj) ) {
        	messageList.add(new Message("co.falta.dj.informacion_consolidado"));
        	puedeTransmitirDJValidada = ConstantesCO.OPCION_NO;
        }

        return puedeTransmitirDJValidada;
    }
    
    /**
     * Carga en el request informaci�n correspondiente a la Calificaci�n de Origen, para ser invocada cuando corresponda
     * @param calificacionUoId Id de la Calificaci�n de Origen.
     * @param request Objeto request.
     * @return CalificacionOrigen
     */
    public CalificacionOrigen cargarInformacionCalificacionOrigen(Orden ordenObj, Long calificacionUoId, HttpServletRequest request, boolean requiereInformacionGeneral) {
    	UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
    	CalificacionOrigen calificacionOrigenObj = null;
    	
    	//NPCS 08/02/2018
    	String productorValidador = ConstantesCO.OPCION_NO;
    	String djValidada = ConstantesCO.OPCION_NO;
    	String esPropietarioDatos = ConstantesCO.OPCION_NO;
    	String reqValidacion = ConstantesCO.OPCION_NO;
    	String solicitoCalificacion = ConstantesCO.OPCION_NO;
    	
    	if ( calificacionUoId != null ) {
    		// Obtenemos las variables de request
            HashUtil<String, String> elements = RequestUtil.getParameter(request);

            // Cargamos la informaci�n de la Calificaci�n de Origen
            calificacionOrigenObj = certificadoOrigenLogic.getCalificacionOrigenById(calificacionUoId);
            RequestUtil.setAttributes(request, "CALIFICACION.", calificacionOrigenObj);
            
            DeclaracionJurada djObj = certificadoOrigenLogic.getCODeclaracionJuradaById(calificacionOrigenObj.getDjId());

            // Cargamos los productores asociados a la calificaci�n
            HashUtil<String, Object> filterDetalles = new HashUtil<String, Object>();
            filterDetalles.put("calificacionUoId", calificacionUoId);
            Table tProductoresCalificacion = ibatisService.loadGrid("calificacionOrigen.productor.grilla", filterDetalles);
            request.setAttribute("tProductoresCalificacion", tProductoresCalificacion);
            //NPCS 08/03/2018
            String tieneRolAsignado = calificacionOrigenObj.getTipoRolCalificacion()!=null?"S":"N";
            
            System.out.println("****tieneRolAsignado: ..."+tieneRolAsignado);
            
            //NPCS 08/02/2018
            // Verifica si el usuario logeado es el propietario de los datos de la Calificacion     
            esPropietarioDatos = certificadoOrigenLogic.esPropietarioDatos(calificacionUoId, usuario.getNumeroDocumento());
            
            System.out.println("****esPropietarioDatos: ..."+esPropietarioDatos);
            
            //NPCS 08/02/2018
            solicitoCalificacion = certificadoOrigenLogic.solicitoCalificacion(calificacionUoId, usuario.getNumeroDocumento());
            
            System.out.println("***"+solicitoCalificacion+solicitoCalificacion);
            
            //NPCS 08/02/2018
            //Verificamos si requiere validaci�n del PRODUCTOR
            if (calificacionOrigenObj != null && calificacionOrigenObj.getTipoRolCalificacion() != null) {
        		HashMap<String, Object> filter = new HashMap<String, Object>();
        		filter.put("ordenId", ordenObj.getOrden());
        		filter.put("tipoRolCalificacion", calificacionOrigenObj.getTipoRolCalificacion());
        		
        		reqValidacion = certificadoOrigenLogic.reqValidacionProductor(filter);
        		
        		if (ConstantesCO.OPCION_SI.equalsIgnoreCase(certificadoOrigenLogic.reqValidacionProductor(filter))){
        			//Vemos si el exportador envio a validar
        			productorValidador = certificadoOrigenLogic.existeProductorValidador(filterDetalles);
        			request.setAttribute("productorValidador",productorValidador);
        			//Vemos si el productor ya valido
        			filter.put("djId", djObj.getDjId());
        			djValidada = certificadoOrigenLogic.djValidada(filter);
        			request.setAttribute("djValidada", djValidada);
        			
        		}
            }


            HashUtil<String, Object> cmbFilter = new HashUtil<String, Object>();
            cmbFilter.put("acuerdoId", ordenObj.getIdAcuerdo());
            request.setAttribute("cmbFilter", cmbFilter);

            // Solo la primera vez esta como atributo de request, en los posteriores casos estar�a como parametro del submit
            request.setAttribute("esCalificacionOrigenStandAlone", request.getAttribute("esCalificacionOrigenStandAlone") != null ? (String) request.getAttribute("esCalificacionOrigenStandAlone") : elements.getString("esCalificacionOrigenStandAlone"));

            // Apoderamiento
            request.setAttribute("permiteApoderamientoXFrmt", formatoLogic.permiteApoderamientoXFrmt(calificacionOrigenObj.getAcuerdoInternacionalId()));

            // Validaci�n del productor para saber si mostrar o no un radio button en la pantalla "datosRolSolicitanteCalificacion.jsp"
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("idAcuerdo", calificacionOrigenObj.getAcuerdoInternacionalId());
            String reqValidacionProd = "N";
           	try {
				reqValidacionProd = (String) ibatisService.loadObject("tipo.validacion.productor.acuerdo", filter);
			} catch (Exception e) {
				e.printStackTrace();
			}
            request.setAttribute("reqValidacionProd", reqValidacionProd);


            HashUtil<String, Object> filterAutoriz = new HashUtil<String, Object>();
            filterAutoriz.put("calificacionUoId", calificacionUoId);
            Table tAutorizaciones = ibatisService.loadGrid("calificacionOrigen.exportador.autorizado.grilla", filterAutoriz);
            request.setAttribute("tAutorizaciones", tAutorizaciones);

            String mostrarBotonProductor = ConstantesCO.OPCION_NO;
            mostrarBotonProductor = formatoLogic.permiteCrearProductor(calificacionUoId);
	    	request.setAttribute("mostrarBotonProductor", mostrarBotonProductor);

	        String mostrarBtnValidacionProd = "0";
	        if ( "N".equals(request.getAttribute("bloqueado")!=null?request.getAttribute("bloqueado").toString():"N") && ordenObj != null && calificacionOrigenObj.getDjId() != null ) {
	        	mostrarBtnValidacionProd = this.certificadoOrigenLogic.djBtnValidacionProd(calificacionOrigenObj.getDjId(), ordenObj.getOrden(), ordenObj.getMto());
	        	HashUtil<String, Object> filterProcValidacion = new HashUtil<String, Object>();
	        	filterProcValidacion.put("djId", calificacionOrigenObj.getDjId());
	        	try {
	        		request.setAttribute("djEnProcesoValidacion", this.certificadoOrigenLogic.djEnProcesoValidacion(filterProcValidacion));
				} catch (Exception e) {
				}

	        }
	        
	        String numeroDocPropietarioDatos = calificacionOrigenObj.getNumDocPropietarioDatos()!=null?calificacionOrigenObj.getNumDocPropietarioDatos():"0";
	        if (usuario.getNumeroDocumento().toString().equals(numeroDocPropietarioDatos)) {
	        	request.setAttribute("modoProductorValidador", "S");
	        }else {
	        	request.setAttribute("modoProductorValidador", "N");
	        }
 
	        request.setAttribute("mostrarBtnValidacionProd", mostrarBtnValidacionProd);
	        request.setAttribute("iniVigencia", calificacionOrigenObj.getFechaInicioVigencia());
	        request.setAttribute("finVigencia", calificacionOrigenObj.getFechaFinVigencia());
	        request.setAttribute("estado", calificacionOrigenObj.getEstado());
	        //NPCS 08/02/2018
	        request.setAttribute("tipoRolCalificacion", calificacionOrigenObj.getTipoRolCalificacion());
	        request.setAttribute("productorValidador", productorValidador);
	        request.setAttribute("djValidada", djValidada);
	        request.setAttribute("esPropietarioDatos", esPropietarioDatos);
	        request.setAttribute("solicitoCalificacion", solicitoCalificacion);
	        request.setAttribute("tieneRolAsignado", tieneRolAsignado);

	        request.setAttribute("reqValidacion",reqValidacion);
	        request.setAttribute("estadoCalif", calificacionOrigenObj.getEstado());

            // S�lo si se requiere informaci�n general aplicamos la informaci�n
            // El caso contrario es cuando otros m�todos se encargan de eso
            if ( requiereInformacionGeneral ) {
                // Informaci�n general requerida
                request.setAttribute("orden", ordenObj.getOrden());
                request.setAttribute("mto", ordenObj.getMto());
                request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
                request.setAttribute("coId", ordenObj.getIdFormatoEntidad());
                request.setAttribute("idEntidadCertificadora", elements.getInt("idEntidadCertificadora"));
                request.setAttribute("idAcuerdo", elements.getInt("idAcuerdo"));
                request.setAttribute("idPais", elements.getInt("idPais"));
                request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());
                request.setAttribute("estadoDJ", elements.getString("estadoDJ"));
                request.setAttribute("secuencia", elements.getString("secuencia"));
                request.setAttribute("formato", elements.getString("formato"));

                request.setAttribute("tipoRolCalificacion",calificacionOrigenObj.getTipoRolCalificacion());
                request.setAttribute("estado",calificacionOrigenObj.getEstado());


                if ( ConstantesCO.AC_SGPC.equals(ordenObj.getIdAcuerdo())
                    	|| ConstantesCO.AC_SGP_UE.equals(ordenObj.getIdAcuerdo())
                    	|| ConstantesCO.AC_COREA.equals(ordenObj.getIdAcuerdo()) 
                    	|| ConstantesCO.AC_SGP_TURQUIA.equals(ordenObj.getIdAcuerdo())) {
                   	request.setAttribute("lblSufijo", "."+ordenObj.getIdAcuerdo());
                } else {
                	request.setAttribute("lblSufijo", "");
                }
            }

    	}
    	return calificacionOrigenObj;
    }

	/********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/


    /**
     * Carga los mensajes que coresponden a las validaciones de DJs requeridas para permitir la transmisi�n de la solicitud
     *
     * @param puedeTransmitir
     * @param estadoOrden
     * @param tMercancias
     * @param messageList
     */
    private String alertasDJsPendientes(String puedeTransmitir, String estadoOrden, boolean esReexportacion, Integer idAcuerdo, Table tMercancias, MessageList messageList) {
	    if ( !"N".equals( puedeTransmitir ) && ( ConstantesCO.CO_ESTADO_SUBSANACION.equals(estadoOrden) || ConstantesCO.CO_ESTADO_PENDIENTE_ENVIO_A_ENTIDAD.equals(estadoOrden))) {
	    	Row objRow;
	    	int numDJIncompleta = 0;
	    	int numDJXValidar = 0;
	    	int numEstadoInvalido = 0;
	    	int numMercIncompleta = 0;
	    	int numPendienteValidacion = 0;
	    	int numRechazadas = 0;

	    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    	filter.put("idAcuerdo", idAcuerdo);

            String tValidacion = "N";
            try {
            	tValidacion = (String) ibatisService.loadObject("tipo.validacion.productor.acuerdo", filter);
			} catch (Exception e) {
				// 
			}

            String estadoCalif = null;
	    	for (Object object : tMercancias) {
	    		objRow = (Row) object;
                
				if (objRow.getCell("DESCRIPCION").getValue() == null){
					numMercIncompleta++;
				}
                
				/*if ( ConstantesCO.OPCION_NO.equals(objRow.getCell("INFORMACIONCOMPLETA").getValue()) ) {
					numDJIncompleta++;
				}*/
				
				if (!esReexportacion) {
					estadoCalif = objRow.getCell("ESTADOREGISTROCALIF").getValue().toString().trim();
					if (!ConstantesCO.DJ_ESTADO_PENDIENTE_GENERACION_DR.equals(estadoCalif) &&
						!ConstantesCO.DJ_ESTADO_PENDIENTE_RESPUESTA_ENTIDAD.equals(estadoCalif) &&
						!ConstantesCO.DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO.equals(estadoCalif) &&
						!ConstantesCO.DJ_ESTADO_APROBADA.equals(estadoCalif) ) {
						if ( ConstantesCO.DJ_ESTADO_PENDIENTE_VALIDACION.equals(estadoCalif) ||
							 ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(estadoCalif) ) {
							numDJXValidar++;
						} else if (ConstantesCO.DJ_ESTADO_BORRADOR.equals(estadoCalif)){
							if (ConstantesCO.OPCION_SI.equals(tValidacion) && ConstantesCO.TIPO_ROL_EXPORTADOR.equals(objRow.getCell("TIPOROLCALIF").getValue())){
								numPendienteValidacion++;
							}
						} else if (ConstantesCO.OPCION_SI.equals(tValidacion) && ConstantesCO.DJ_ESTADO_RECHAZADA.equals(estadoCalif)){
							numRechazadas++;
						} else {
							numEstadoInvalido++;
						}
					}
				}
	    	}

	    	if ( numDJIncompleta > 0 || numDJXValidar > 0 || numEstadoInvalido > 0 || numMercIncompleta > 0 || numPendienteValidacion > 0 || numRechazadas > 0) {

				puedeTransmitir = "N";

				if ( numDJXValidar > 0 ){
					messageList.add(new Message("co.certificado.falta.dj.sin.aprobar", new String[]{""+numDJXValidar}));
				} else if (numPendienteValidacion > 0 || numRechazadas > 0){

					if ( numPendienteValidacion > 0 ){
	    				messageList.add(new Message("co.certificado.falta.dj.estado.pendiente.envio.validacion", new String[]{""+numPendienteValidacion}));
	    			}

					if ( numRechazadas > 0 ){
	    				messageList.add(new Message("co.certificado.falta.dj.estado.rechazado", new String[]{""+numRechazadas}));
	    			}
	    		} else {
	    			if ( numDJIncompleta > 0 ){
	    				messageList.add(new Message("co.certificado.falta.dj.completa", new String[]{""+numDJIncompleta}));
	    			}

				}

				if ( numEstadoInvalido > 0 ){
					messageList.add(new Message("co.certificado.falta.dj.estado.invalido", new String[]{""+numEstadoInvalido}));
				}

				if ( numMercIncompleta > 0 ){
					messageList.add(new Message("co.certificado.falta.mercancia.completa", new String[]{""+numMercIncompleta}));
				}

	    	}

	    }

	    return puedeTransmitir;
    }

    /**
     *  Carga la informaci�n espec�fica del Formato de Certificado de Origen
     */
    public void cargarInformacionEspecificaFormato(HttpServletRequest request, MessageList messageList) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        Orden ordenObj = (Orden) messageList.getObject();     
       
        // Carga los datos del detalle del formato
        if (ordenObj.getIdFormatoEntidad() != null){ // el idFormatoEntidad es en realidad el coId

        	String formato = request.getParameter("formato");

        	cargarDatosFormatoOrigen(ordenObj, formato, request, messageList); // Esta funci�n carga la informaci�n espec�fica de acuerdo al formato que se est� ejecutando, incluyendo validaciones de transmisi�n

        	
        	request.setAttribute("editable", ordenObj.getBloqueado().equals("S")?"no":"yes");
        	
            // Seteamos variables de request importantes para el flujo de todo el certificado
            request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("numeroSolicitud", ordenObj.getNumOrden());
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("formato", formato);
            request.setAttribute("transmitido", ordenObj.getTransmitido());
            request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());

            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
            	Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo()) &&
                !ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(formato.toLowerCase()) ) {
            	String resultadoConfirmarEvaluacion = formatoLogic.permiteConfirmarFinEval(ordenObj.getIdFormatoEntidad(), ordenObj.getOrden(), ordenObj.getMto());
                request.setAttribute("mostrarBotonConfirmarFinEval", resultadoConfirmarEvaluacion);
            }           
            // Colocamos la lista de si y no para los combos
            request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());
        }
        request.setAttribute("fechaActual", ibatisService.loadElement("comun.fechaActual", null).getString("FECHA"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

    }

    /**
     *  Carga la informaci�n general del Formato de Certificado de Origen
     */
    public void cargarInformacionGeneralFormato(HttpServletRequest request, MessageList messageList) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        
        if (!usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) { // Si no es un usuario de extranet
            request.setAttribute("activeSUCE", "active");
        }
        
        String puedeModificar = "S";
        
        Orden ordenObj = (Orden) messageList.getObject();
        
        if (ordenObj!=null && !"S".equals(ordenObj.getCertificadoReexportacion())) {
       	    System.out.println(ordenObj.getCertificadoReexportacion());
       	    filterADJ.put("certificadoReexportacion", "N");
        }
        
        Formato formato = (Formato) formatoLogic.getFormatoByTupa(ordenObj.getIdFormato(), ordenObj.getIdTupa());
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idTupa", ordenObj.getIdTupa());
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");
        
        String opcion = datos.getString("opcion");
        String historico = datos.getString("historico");
        
        // Creamos el objeto Auditoria
        Auditoria auditoria = new Auditoria();
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            if (usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
                auditoria.setRecursoOrigen(ConstantesCO.AUDITORIA_RECURSO_ORIGEN_EXTRANET_MESA_AYUDA);
            }
        } else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) ||
                   usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            auditoria.setRecursoOrigen(ConstantesCO.AUDITORIA_RECURSO_ORIGEN_USUARIO_IMPORTADOR_EXPORTADOR);
        }

        // Si es una SUCE
        boolean ventanaModificacionSUCE = false;
        Suce suceObj = null;
        Orden ordenMTOSUCE = null;

        if (ordenObj.getSuce() != null) {

            if (opcion.equals("")) opcion = ConstantesCO.OPCION_SUCE;
            
            // Verificamos el parametro "Utiliza Modificacion SUCE x MTO"
            request.setAttribute("utilizaModificacionSuceXMto", ordenObj.getModificacionSuceXMto());

            if ("S".equalsIgnoreCase(ordenObj.getModificacionSuceXMto())) {

	            // Si el usuario pulso el boton para ver el Borrador de SUCE
	            if ("N".equalsIgnoreCase(ordenObj.getVigente()) && !historico.equals("S")) {

            	//if (datos.get("mtoSUCE")!=null && !"".equals(datos.getString("mtoSUCE"))) {
            		suceObj = suceLogic.getSuceModificacionById(ordenObj.getSuce(), ordenObj.getMto());//datos.getInt("mtoSUCE"));

	            	ordenMTOSUCE = ordenLogic.getOrdenById(ordenObj.getOrden(), ordenObj.getMto());//datos.getInt("mtoSUCE"));

	            	request.setAttribute("mtoSUCE", ordenObj.getMto());//datos.get("mtoSUCE"));
	            	request.setAttribute("vigente", ordenMTOSUCE.getVigente());
	            	request.setAttribute("certificadoOrigen", ordenMTOSUCE.getCertificadoOrigen());
	            	request.setAttribute("certificadoReexportacion", ordenMTOSUCE.getCertificadoReexportacion());    
	            	request.setAttribute("modificacionSuceId", suceObj.getModificacionSuceId());
	            	request.setAttribute("mensajeId", suceObj.getMensajeId());

	            	//request.setAttribute("mto", suceObj.getMto()); // Reemplazamos el dato del MTO que se esta viendo
	            	ventanaModificacionSUCE = true;
	            } else {// Si se esta abriendo la version "vigente" de la SUCE
	            	suceObj = suceLogic.getSuceById(ordenObj.getSuce());
	            	//filter.clear();
	            	//filter.put("suceId", suceObj.getSuce());
	            	//int mtoSUCE = ibatisService.loadElement("suce.modificacionNoTransmitida", filter).getInt("MTO");
	            	//request.setAttribute("mtoSUCE", mtoSUCE);
	            }
            } else {
            	suceObj = suceLogic.getSuceById(ordenObj.getSuce());
            }

            //Cargar Datos de la SUCE
            request.setAttribute("numSuce", suceObj.getNumSuce());
            request.setAttribute("suceCerrada", suceObj.getCerrada());
            request.setAttribute("fechaRegistroSuce", suceObj.getFechaRegistro());
            request.setAttribute("expediente", suceObj.getExpediente());

            auditoria.setSuce(Integer.valueOf(suceObj.getNumSuce()));
            if (auditoria.getRecursoOrigen()!=null) {
                auditoriaLogic.grabarAccesoSuce(auditoria);
            }
        } else { // Si es una ORDEN
            if (opcion.equals("")) opcion = ConstantesCO.OPCION_ORDEN;

            auditoria.setOrden(Integer.valueOf(ordenObj.getNumOrden()));
            if (auditoria.getRecursoOrigen()!=null) {
                auditoriaLogic.grabarAccesoSolicitud(auditoria);
            }
        }

        //String controller = formato.getFormato().toLowerCase()+".htm"; /** comentado nuevo **/

        //int numAdjuntos = formatoLogic.cuentaAdjuntos(ordenObj.getIdFormato(), ordenObj.getOrden(), ordenObj.getMto()); /** comentado nuevo **/

        // Cargar datos del usuario creador del formato
        Solicitante usuarioFormato = formatoLogic.getUsuarioDetail(ordenObj.getOrden(),ordenObj.getMto());

        // Cargamos datos del usuario del formato como el declarante
        RequestUtil.setAttributes(request, "DECLARANTE.", usuarioFormato);

        //Cargar datos del solicitante
        Solicitante solicitante = formatoLogic.getSolicitanteDetail(ordenObj.getOrden(),ordenObj.getMto());
        RequestUtil.setAttributes(request, "SOLICITANTE.", solicitante);

        RequestUtil.setAttributes(request, "", ordenObj);

        if (ventanaModificacionSUCE) {

        	request.setAttribute("mto", ordenMTOSUCE.getMto());
        	request.setAttribute("vigente", ordenMTOSUCE.getVigente());
        	request.setAttribute("bloqueado", "N");
        	request.setAttribute("transmitido", ordenMTOSUCE.getTransmitido());

        	ModificacionSuce modifSuce = suceLogic.loadModificacionSuceByIdMensajeId(suceObj.getSuce(), suceObj.getModificacionSuceId(), suceObj.getMensajeId());
        	request.setAttribute("estadoRegistro", modifSuce.getEstadoRegistro());

        	/*if (modifSuce.getEstadoRegistro().equals("G")) { // G significa "pendiente de pago", se est� comentando pues por el momento Origen no tiene pagos.
        		request.setAttribute("bloqueado", "S");
        	}*/

        } else {
            if (ordenObj.getTransmitido().equals("S") /** || ordenObj.getModoTramite() == ConstantesCO.MODO_TRAMITE_WEB_SERVICE_EMPRESA_EXTERNA**/) {
            	request.setAttribute("bloqueado", "S");
            }
        }
        request.setAttribute("idEntidad", formato.getIdEntidad());
        request.setAttribute("formato", formato.getFormato());
        //request.setAttribute("numAdjuntos", numAdjuntos);
        request.setAttribute("nombreFormato", formato.getFormato()+" - "+formato.getNombre()+" (TUPA: "+tupa+")");
        //request.setAttribute("controller", controller);
        request.setAttribute("tipoPersona", solicitante.getPersonaTipo());
        request.setAttribute("idTupa", ordenObj.getIdTupa());

        request.setAttribute("opcion", opcion);

        //Cargar datos del representante
        HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
        filterRepresentante.put("usuarioId", solicitante.getUsuarioId());
        request.setAttribute("filterRepresentante", filterRepresentante);

        RepresentanteLegal representante = formatoLogic.getRepresentanteDetail(ordenObj.getOrden(),ordenObj.getMto());
        RequestUtil.setAttributes(request, "REPRESENTANTE.", representante);
        if ("2".equals(solicitante.getPersonaTipo()) && (representante == null || representante.getId() == null)) {
            messageList.add(new Message("falta.representanteLegal"));
            request.setAttribute("puedeTransmitir", ConstantesCO.OPCION_NO);
        }

        if (solicitante.getUbigeo() != null && !solicitante.getUbigeo().equals("")) {
            //Nombres del departamento, provincia y distrito
            HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
            request.setAttribute("departamento", element.get("DEPARTAMENTO"));
            request.setAttribute("provincia", element.get("PROVINCIA"));
            request.setAttribute("distrito", element.get("DISTRITO"));
            request.setAttribute("departamentoId", element.get("DEPARTAMENTOID"));
            request.setAttribute("provinciaId", element.get("PROVINCIAID"));
            request.setAttribute("distritoId", element.get("DISTRITOID"));
        }

        if (usuarioFormato.getUbigeo() != null && !usuarioFormato.getUbigeo().equals("")) {
            //Nombres del departamento, provincia y distrito
            HashMap<String, String> element = formatoLogic.getUbigeoDetail(usuarioFormato.getUbigeo());
            request.setAttribute("DECLARANTE.departamento", element.get("DEPARTAMENTO"));
            request.setAttribute("DECLARANTE.provincia", element.get("PROVINCIA"));
            request.setAttribute("DECLARANTE.distrito", element.get("DISTRITO"));
            request.setAttribute("DECLARANTE.departamentoId", element.get("DEPARTAMENTOID"));
            request.setAttribute("DECLARANTE.provinciaId", element.get("PROVINCIAID"));
            request.setAttribute("DECLARANTE.distritoId", element.get("DISTRITOID"));
        }

        //Cargar datos del Agente de Aduana o Laboratorio
        /*if (usuarioFormato.isAgente() || usuarioFormato.isLaboratorio()) {
            request.setAttribute("empresa", "S");
            Solicitante empresa = formatoLogic.getEmpresaExternaDetail(ordenObj.getOrden(),ordenObj.getMto());
            RequestUtil.setAttributes(request, "EMPRESA.", empresa);
            if (empresa.getUbigeo() != null) {
                // Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(empresa.getUbigeo());
                request.setAttribute("EMPRESA.departamento", element.get("DEPARTAMENTO"));
                request.setAttribute("EMPRESA.provincia", element.get("PROVINCIA"));
                request.setAttribute("EMPRESA.distrito", element.get("DISTRITO"));
            }
        }*/

        
        filterADJ.put("orden", ordenObj.getOrden());
        filterADJ.put("mto", ordenObj.getMto());
        filterADJ.put("formato", ordenObj.getIdFormato());

        request.setAttribute("filterADJ", filterADJ);

        //Cargar los datos de las Modificaciones de Suce
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            if (ordenObj.getSuce() != null) {
                request.setAttribute("activeSUCE", "active");
            } else {
                request.setAttribute("activeOrden", "active");
            }
        }

        //Cargar los datos de la orientaci�n luego de Transmitirse y estar Pendiente de Subsanaci�n
        // Todo este IF-ELSE debe corresponder SOLAMENTE para el caso de que NO exista SUCE
        /*if (suceObj==null) {

	        if (ordenObj.getTransmitido().equals("S") && (ordenObj.getEstadoRegistro().equals("S") /**|| ordenObj.getEstadoRegistro().equals("G")**//*)) {

	            messageList.setTitle("orden.title.orientacion");

	            //Verificar si ya se creo alguna Modificaci�n
	            int existe = ordenLogic.existeModificacion(ordenObj.getOrden(), ordenObj.getMto());

	            if (existe > 0 && !"S".equalsIgnoreCase(ordenObj.getDesistido())) {
	                 Message message = new Message("existe.modificacion.pendiente");
	                 messageList.add(message);
	                 //request.setAttribute("puedeModificar", "N");
	                 puedeModificar = "N";
	                 request.setAttribute("bloqueado", "S");
	            } else {
	                HashUtil<String, Object> filterORI = new HashUtil<String, Object>();
	                filterORI.put("orden", ordenObj.getOrden());
	                filterORI.put("mto", ordenObj.getMto());

	                Table tOrientacion = ibatisService.loadGrid("orden.orientacion.grilla", filterORI);
	                String descripcion = "";

	                for (int i = 0; i < tOrientacion.size(); i++ ){
	                    Row row = tOrientacion.getRow(i);
	                    descripcion = row.getCell("DESCRIPCION").getValue().toString();
	                    Message message = new ErrorMessage();
	                    message.setMessage(descripcion);
	                    messageList.add(message);
	                }
	            }

	        }else{
	            //Cargar los datos de la orientacion luego de crearse la modificacion
	            if((ordenObj.getMto()> 1) && (ordenObj.getTransmitido().equals("N"))  ){
	                HashUtil<String, Object> filterORI = new HashUtil<String, Object>();
	                filterORI.put("orden", ordenObj.getOrden());
	                filterORI.put("mto", ordenObj.getMto()-1);

	                Table tOrientacion = ibatisService.loadGrid("orden.orientacion.grilla", filterORI);
	                String descripcion;

	                for(int i = 0; i< tOrientacion.size(); i++ ){
	                    Row row = tOrientacion.getRow(i);
	                    descripcion = row.getCell("DESCRIPCION").getValue().toString();
	                    messageList.setTitle("orden.title.orientacion");
	                    Message message = new ErrorMessage();
	                    message.setMessage(descripcion);
	                    messageList.add(message);
	                }
	            }
	        }
        }
        else */
        if (suceObj != null) { //Esta linea deba acoplarse con el else de arriba, para CUANDO exista modificaci�n de ORDEN
            // AQUI validar para el caso de que la Orden tenga SUCE:
        	if (ordenMTOSUCE != null) {
                // - Si la MTO corresponde a una SUCE y esta Transmitida, bloquear el formato

	        	// Verificar si se est� en la SUCE vigente (MTO >= 2) y ya se creo alguna Modificacion
	        	if ("S".equalsIgnoreCase(ordenMTOSUCE.getVigente())) {

		        	HashUtil<String, Object> filterModif = new HashUtil<String, Object>();
		        	filterModif.put("suceId", suceObj.getSuce());
		            int existe = ibatisService.loadElement("suce.modificacionNoTransmitida.count", filterModif).getInt("CUENTAMODIFICACIONES");
		            //ordenLogic.existeModificacion(ordenObj.getOrden(), ordenObj.getMto());
		            if (existe > 0/** && !"S".equalsIgnoreCase(suceObj.getDesistido())**/) { // No hay desestimiento por el momento
		                 puedeModificar = "N";
		                 request.setAttribute("bloqueado", "S");
		            }
	        	}
	        	// Si se esta en la SUCE modificada, si ya fue transmitida pero aun no confirmada por la entidad, bloquear el formato
	        	else {
	        		if (ordenMTOSUCE.getTransmitido().equals("S")) {
		                 puedeModificar = "N";
		                 request.setAttribute("bloqueado", "S");
	        		}
	        	}

	        	// Cargamos las diferencias que existan entre la MTS actual y la anterior
                HashUtil<String, Object> filterModif = new HashUtil<String, Object>();      // No se est� colocando para Origen por el momento
                filterModif.put("ordenId", ordenMTOSUCE.getOrden());
                filterModif.put("mto", ordenMTOSUCE.getMto());
                Table tDiferenciasModificacionSUCE_cambios = ibatisService.loadGrid("formato.modificacion_suce.cambios.grilla", filterModif);
                request.setAttribute("tDiferenciasModificacionSUCE_cambios", tDiferenciasModificacionSUCE_cambios);

                Table tDiferenciasModificacionSUCE_nuevos_registros = ibatisService.loadGrid("formato.modificacion_suce.nuevos_registros.grilla", filterModif);
                request.setAttribute("tDiferenciasModificacionSUCE_nuevos_registros", tDiferenciasModificacionSUCE_nuevos_registros);

                Table tDiferenciasModificacionSUCE_registros_eliminados = ibatisService.loadGrid("formato.modificacion_suce.registros_eliminados.grilla", filterModif);
                request.setAttribute("tDiferenciasModificacionSUCE_registros_eliminados", tDiferenciasModificacionSUCE_registros_eliminados);
        	} else {
        		// Es una SUCE sin MTO
                puedeModificar = "N";
                request.setAttribute("bloqueado", "S");
        	}
            /*
            HashUtil<String, Object> filterNotif = new HashUtil<String, Object>();
            filterNotif.put("suceId", suceObj.getSuce());

            Table tOrientacion = ibatisService.loadGrid("suce.notificacionesAsociadas.grilla", filterNotif);
            String descripcion = "";

            for (int i = 0; i < tOrientacion.size(); i++ ){
                Row row = tOrientacion.getRow(i);
                descripcion = row.getCell("DESCRIPCION").getValue().toString();
                Message message = new ErrorMessage();
                message.setMessage(descripcion);
                messageList.add(message);
            }*/
        }
        
        // Cargamos documentos resolutivos y subsanaciones, las cuales solo aplican cuando existe suce
        if (ordenObj.getSuce() != null) {
        	// Documentos Resolutivos
            HashUtil<String, Object> filterResol = new HashUtil<String, Object>();
            filterResol.put("suceId", ordenObj.getSuce());
            Table tDocResolutivos = ibatisService.loadGrid("formato.doc_resolutivo.grilla", filterResol);
            request.setAttribute("tDocResolutivos", tDocResolutivos);
        	// Subsanaciones
            if( tDocResolutivos.size() > 0) request.setAttribute("tieneDr", "S");
            HashUtil<String, Object> filterSubsan = new HashUtil<String, Object>();
            filterSubsan.put("suceId", ordenObj.getSuce());
            Table tSubsanaciones = ibatisService.loadGrid("formato.subsanacion.grilla", filterSubsan);
            request.setAttribute("tSubsanaciones", tSubsanaciones);
        } else {
        	HashUtil<String, Object> filterSubsan = new HashUtil<String, Object>();
            filterSubsan.put("ordenId", ordenObj.getOrden());
            filterSubsan.put("mto", ordenObj.getMto());
            
            String key = "formato.subsanacion.solicitud.grilla";
            Table tSubsanaciones = ibatisService.loadGrid(key, filterSubsan);
            request.setAttribute("tSubsanaciones", tSubsanaciones);
        }

        // Cargar el historico de cambios de MTS
        /*if (ordenObj.getSuce() != null && !historico.equals("S")) { // No hay hist�ricos de MTS en Origen, por el momento
        	HashUtil<String, Object> filterHistoricoMTS = new HashUtil<String, Object>();
            filterHistoricoMTS.put("suceId", ordenObj.getSuce());
        	Table tHistoricoMTS = ibatisService.loadGrid("formato.suce.historicoMTS.grilla", filterHistoricoMTS);
        	request.setAttribute("tHistoricoMTS", tHistoricoMTS);
        }*/

        // Si el Usuario logueado no es el Usuario que hizo la SUCE (osea, el Usuario logueado es un Usuario Principal que puede VER la Solicitud/SUCE)
        if (usuarioFormato.getUsuarioId().intValue() != usuario.getIdUsuario().intValue()) {
            ordenObj.setBloqueado("S");
            request.setAttribute("bloqueado", "S");
        }

        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            request.setAttribute("bloqueado", "S");
        }

        /*if ("S".equalsIgnoreCase(ordenObj.getDesistido())) { // No hay desistimientos en Origen, por el momento
            request.setAttribute("bloqueado", "S");
            request.setAttribute("estadoRegistro", "D");
            request.setAttribute("puedeTransmitir", "N");
            //request.setAttribute("puedeModificar", "N");
            puedeModificar = "N";
        }*/

        request.setAttribute("puedeModificar", puedeModificar);
        request.setAttribute("historico", historico);
        
    	//JMC 23/06/2017 Alianza
    	String versionAcuerdo = getFormatoVersionSufijo(ordenObj);
    	request.setAttribute("idAcuerdoVersion", request.getAttribute("idAcuerdo").toString()+versionAcuerdo);
        
    }

    public ModelAndView cargarPartidas(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        MessageList messageList = new MessageList();
        Message message = new Message("co.partidas.info");
        messageList.add(message);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        return new ModelAndView(partidas);
    }

    public ModelAndView obtenerPartidas(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("opbusqueda", datos.getInt("opbusqueda"));
        if(datos.getInt("opbusqueda") == 0) {
            if(datos.containsKey("codigo") && datos.get("codigo") != null) {
                filter.put("partida_texto", datos.getString("codigo"));
            }
        } else if(datos.getInt("opbusqueda") == 1) {
            if(datos.containsKey("descripcion") && datos.get("descripcion") != null) {
                filter.put("descripcion", datos.getString("descripcion"));
                filter.put("aproximacion", datos.getString("opBusqDesc"));
            }
        }

        Table tPartidas = this.certificadoOrigenLogic.obtenerPartidas(filter);

        //tPartidas = ibatisService.loadGrid("comun.partida_arancelaria.select", filter);
        //tPartidas = ibatisService.loadGrid("co.comun.busqueda.arancelaria.select", new HashUtil<String, Object>());//ibatisService.loadGrid("comun.partida_arancelaria.select", filter);

        request.setAttribute("codigo", datos.get("codigo"));
        request.setAttribute("descripcion", datos.get("descripcion"));
        request.setAttribute("opBusqDesc", datos.get("opBusqDesc"));

        request.setAttribute("tPartidas", tPartidas);

        MessageList messageList = new MessageList();
        Message message = new Message("co.partidas.info");
        messageList.add(message);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        return new ModelAndView(partidas);
    }

    /**
     * Realiza la transmisi�n del certificado
     * @param request
     * @param response
     * @return
     */
    public ModelAndView transmitirOrdenFormato(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        ModelAndView cargarFormatoView = null;
        int orden = datos.getInt("orden");
        int mto = datos.getInt("mto");
        boolean primeraTransmision = datos.getString("transmitido").equals("N") && datos.getString("mto").equals("1");

        String validacionHash = "";
        String formato = request.getParameter("formato");
        /* JMC - 13/11/2013 - Ya no se debe validar que exista otras DJ calificadas con los mismo datos, el exportador debe poder transmitirlas 
        if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(formato)) {
        	HashMap<String, Object> filter = new HashMap<String, Object>();
        	filter.put("calificacionUoId", Long.valueOf(request.getParameter("CALIFICACION.calificacionUoId")));
        	validacionHash = this.certificadoOrigenLogic.validaHashCalificacion(filter);
        }
        */
        MessageList messageList = new MessageList();

        if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato) ||
        	ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase(formato) ||
        	ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(formato)) {

        	HashUtil<String, Object> filterValidacion = new HashUtil<String, Object>();
        	filterValidacion.put("idFormato", datos.getInt("idFormato"));
        	filterValidacion.put("idOrden", orden);
        	filterValidacion.put("mto", mto);
        	Message msgVal = this.formatoLogic.validacionPreTransmision(filterValidacion);

        	if (msgVal != null) {
        		messageList.add(msgVal);
        	}
        }


        if (messageList.size() == 0) {
        if ("".equals(validacionHash)) {
        	messageList = formatoLogic.transmiteOrden(orden, mto, primeraTransmision);
        } else {
        	Message msg = new Message();
        	msg.setMessage(validacionHash);
        	messageList.add(msg);
        }
        }

        //Repintar formato
        Orden ordenObj = ordenLogic.getOrdenById(new Long(orden), mto);
        messageList.setObject(ordenObj);

        cargarInformacionFormato(request, messageList);
        /*request.setAttribute("transmitido", "S");
        request.setAttribute("bloqueado", "S");*/
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        String nombreFormato = datos.getString("formato");

        return identificarPantallaFormato(nombreFormato.toLowerCase());


    }

	/**
	 * Descarga un Adjunto gen�ricamente
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView descargarAdjunto(HttpServletRequest request, HttpServletResponse response) {

		HashUtil<String, String> datos = RequestUtil.getParameter(request);
		int claveAdjunto = datos.getInt("idAdjunto");
		Adjunto adjunto = adjuntoLogic.descargarAdjuntoById(claveAdjunto);

	    byte [] bytes = adjunto.getArchivo();
	    try {
	    	if (bytes != null) {
	    		String nombreDocumento = adjunto.getNombre();
	            response.setContentType("application/force-download");
	            response.setHeader("Content-Disposition", "attachment;filename=\""+nombreDocumento+"\"");
	            response.setContentLength(bytes.length);
	            ServletOutputStream ouputStream = response.getOutputStream();
	            ouputStream.write(bytes, 0, bytes.length);
	            ouputStream.flush();
	            ouputStream.close();
	    	}
	    } catch (Exception e) {
	    	logger.error("Ocurrio un error al intentar descargar el archivo", e);
	    }

	    return null;
	}

	/**
	 * Descarga un Adjunto gen�ricamente
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView descargarPDFConsultaDR(HttpServletRequest request, HttpServletResponse response) {
		HashUtil<String, String> datos = RequestUtil.getParameter(request);
		int claveAdjunto = datos.getInt("idAdjunto");
		Adjunto adjunto = adjuntoLogic.descargarAdjuntoById(claveAdjunto);
        
	    byte [] bytes = adjunto.getArchivo();
	    try {
	    	if (bytes != null) {
	    		String nombreDocumento = adjunto.getNombre();
	            response.setContentType("application/pdf");
	            response.setHeader("Content-Disposition", "inline;filename=\""+nombreDocumento+"\"");
	            response.setContentLength(bytes.length);
	            ServletOutputStream ouputStream = response.getOutputStream();
	            ouputStream.write(bytes, 0, bytes.length);
	            ouputStream.flush();
	            ouputStream.close();
	    	}
	    } catch (Exception e) {
	    	logger.error("Ocurrio un error al intentar descargar el archivo", e);
	    }

	    return null;
	}
	
    /**
	 * Guardar los datos de la Firma de DR
	 * @param request
	 * @param response
	 * @return
	 */
    //20140611_JMCA PQT-08
    public ModelAndView guardarDatosFirmaCertificadoOrigenDRForm(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	long suceId = datos.getLong("DETALLE.suce_id");
    	long drId = new Long(request.getParameter("DETALLE.dr_id").toString());
    	int sdr = datos.getInt("DETALLE.sdr");    	
        
     	MessageList messageList = null;    
            
        try {
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("suceId", suceId);
        	filter.put("drId", drId);
        	filter.put("sdr", sdr);
        	filter.put("usuarioFirmante", datos.getString("usuarioFirmante"));
        	filter.put("secuenciaFuncionario", datos.getInt("secuenciaFuncionario"));
        	filter.put("fechaFirma", Util.getTimestampObject(datos.getString("fechaFirma")+" 00:00:00", "dd/MM/yyyy HH:mm:ss"));
        	filter.put("validaUsuario", "N");
        	filter.put("nroReferenciaCertificado", datos.getString("nroReferenciaCertificado"));
        	
        	filter.put("cargoEmpresaExportador", datos.getString("cargoEmpresaExportador"));
        	filter.put("telefonoEmpresaExportador", datos.getString("telefonoEmpresaExportador"));
        	
        	// 20150612_RPC: Acta.34:Firmas
        	try {
        		filter.put("fechaFirmaExportador",  datos.getString("fechaFirmaExportador") != null ? Util.getDateObject(datos.getString("fechaFirmaExportador")) : null);
        	} catch (Exception exc) {
        		System.err.println();
        	}
        	filter.put("emitidoPosteriori2", datos.getString("emitidoPosteriori2"));
        	
        	messageList = this.resolutorLogic.cargarDatosFirmaAdjuntoCODR(filter);
            
        } catch (Exception e) {
            e.printStackTrace();
        }        
        
        // Llenamos todo lo que vino para mantener la informaci�n en request
        RequestUtil.setAttributes(request);     
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        return this.cargarDatosDocumentoResolutivo(request, response);
	}

    public ModelAndView cargarDatosDocumentoResolutivo(HttpServletRequest request,HttpServletResponse response) {
    	HashUtil<String, String> elements = RequestUtil.getParameter(request);		
		UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
        request.setAttribute("usuarioCo", usuario.getIdUsuario());
        request.setAttribute("nombreCo", usuario.getNombreCompleto());
        
        //I GCHAVEZ - 28/02/2020 - Correcci�n firma digital. Error en Producci�n.
        UsuarioCO usuarioDatos = new UsuarioCO();
        usuarioDatos.setIdUsuario(usuario.getIdUsuario());
        Solicitante solicitanteDetail = formatoLogic.getUsuarioDetail(usuarioDatos);
        request.setAttribute("usuarioSol", solicitanteDetail.getUsuarioSOL());//20170803_JMC Alianza Pacifico: Para que el usuarioSOL se pase como parametro al agente firmador

        System.out.println("Usuario sol 1: "+usuario.getUsuarioSOL());
        System.out.println("Usuario sol 2: "+solicitanteDetail.getUsuarioSOL());
        //F GCHAVEZ - 28/02/2020 - Correcci�n firma digital. Error en Producci�n.
        
		int drId = elements.getInt("drId");
		int sdr = elements.get("sdrSelect")!=null?elements.getInt("sdrSelect"):elements.getInt("sdr");
		String formato = elements.getString("formato");
		//HashUtil<String, Object> coDR = certificadoOrigenLogic.getCertificadoOrigenDRByDrId(drId, sdr, formato.toLowerCase());
		Map<String, Object> params = new HashMap<String, Object>();
        //params.put("coDrId", mct001DrId);
        params.put("drId", drId);
        params.put("sdr", sdr);
        params.put("formato", formato.toLowerCase());
		CertificadoOrigenDR coDR = resolutorLogic.getCODrResolutorById(params);

		boolean tieneFirmaDigital = false;
		boolean produccionControlada = false;
		
        try {
			InteroperabilidadFirma etapa = iopLogic.obtenerToken(new Long(drId), sdr);
			produccionControlada = iopLogic.produccionControlada(new Long(drId));
			// Aqui solo interesa si ya genero el token o no
			
			if (etapa != null) {
				if(!produccionControlada && "2".equals(iopLogic.tipoFirma(new Long(drId)))) {
					tieneFirmaDigital = true;
				}    				
				
			} 

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        request.setAttribute("tieneFirmaDigital", tieneFirmaDigital);
        request.setAttribute("produccionControlada", produccionControlada);
        
		Integer idAcuerdo = coDR.getAcuerdoInternacionalId();

		//coDR.put("DETALLE.RECTIFICA_DR", (coDR.get("DETALLE.RECTIFICA_DR")!=null?coDR.get("DETALLE.RECTIFICA_DR").toString().trim():"N"));
		//RequestUtil.setAttributes(request, coDR);
		if (coDR.getRectificaDr()==null){
			coDR.setRectificaDr("N");
		}
		RequestUtil.setAttributes(request, "DR.", coDR);

		HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("coDrId", coDR.getCoDrId());
        filter.put("drId", coDR.getDrId());//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
        filter.put("sdr", coDR.getSdr());//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados

    	//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados      	
    	//Datos para la visualizacion de la Firma
    	if (coDR != null) {
    		// Colocamos aqu� la informaci�n que corresponde a los datos del firmante
            request.setAttribute("secuenciaFuncionario",coDR.getSecuenciaFuncionario());
            request.setAttribute("fechaFirma",coDR.getFechaFirmaEev());
            request.setAttribute("nroReferenciaCertificado", coDR.getNroReferenciaCertificado());
            request.setAttribute("cargoEmpresaExportador", coDR.getCargoEmpresaExportador());
            request.setAttribute("telefonoEmpresaExportador", coDR.getTelefonoEmpresaExportador());
            if (!tieneFirmaDigital && coDR.getFechaFirmaExportador() == null) { // Si no tiene firma y aun no grabamos nada, precargamos con el usuario
                request.setAttribute("usuarioFirmanteFirma", usuario.getNombreCompleto());
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                Date today = Calendar.getInstance().getTime();        
                String strToday = df.format(today);
                request.setAttribute("fechaFirmaExportadorFirma", strToday); 
                request.setAttribute("emitidoPosteriori2Firma", coDR.getEmitidoPosteriori2()); 
                request.setAttribute("usuarioFirmante",coDR.getUsuarioFirmanteExp());
                request.setAttribute("fechaFirmaExportador", coDR.getFechaFirmaExportador()); 
                request.setAttribute("emitidoPosteriori2", coDR.getEmitidoPosteriori2());
                
                request.setAttribute("faltaGrabarDatosPrimeraFirma", "S");
            } else { // caso contrario, jalamos de la base de datos
                request.setAttribute("usuarioFirmante",coDR.getUsuarioFirmanteExp());
                request.setAttribute("fechaFirmaExportador", coDR.getFechaFirmaExportador()); 
                request.setAttribute("emitidoPosteriori2", coDR.getEmitidoPosteriori2()); 
                request.setAttribute("usuarioFirmanteFirma",coDR.getUsuarioFirmanteExp());
                request.setAttribute("fechaFirmaExportadorFirma", coDR.getFechaFirmaExportador()); 
                request.setAttribute("emitidoPosteriori2Firma", coDR.getEmitidoPosteriori2());
                
                request.setAttribute("faltaGrabarDatosPrimeraFirma", "N");
            }
            
            String post = (String)request.getAttribute("emitidoPosteriori2Firma");
            if ("S".equals(post)) {
            	request.setAttribute("postEditable", false);
               // request.setAttribute("emitidoPosteriori2Firma", "S"); 
            } else {
            	request.setAttribute("postEditable", true);
                //request.setAttribute("emitidoPosteriori2Firma", "N"); 
            }
            
            Table tAdjuntosCODR = ibatisService.loadGrid("resolutor.co.dr.adjuntos.grilla", filter);
            request.setAttribute("tAdjuntosCODR", tAdjuntosCODR);
            request.setAttribute("numAdjuntosCODR", tAdjuntosCODR!=null?tAdjuntosCODR.size():0);
            
            HashUtil<String, Object> filterFirmate = new HashUtil<String, Object>();           
            filterFirmate.put("idEntidad", coDR.getEntidadId());//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
            request.setAttribute("filterFirmantesCertificador", filterFirmate);
            
            request.setAttribute("tipoDr", coDR.getTipoDr());
            request.setAttribute("estadoSDR", coDR.getEstadoSDR());
            request.setAttribute("esRectificacion", coDR.getEsRectificacion());
    		
    	}
    	
    	if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato)){

            //int idAcuerdo = elements.getInt("idAcuerdo");
        	Table tMercancias = ibatisService.loadGrid("certificado_origen.mercancia.dr.datos", filter);
        	request.setAttribute("tMercanciasDr", tMercancias);

        	Table tFacturas = ibatisService.loadGrid("certificado_origen.factura.dr.datos", filter);
        	request.setAttribute("tFacturasDr", tFacturas);
        	request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(coDR.getAcuerdoInternacionalId()));
        	
    	} else if (ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(formato) ){   		

    		Usuario empresa = usuariosLogic.getUsuarioEmpresaById(usuario.getIdUsuario());

    		/*
    		request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
            		new Object [] {""+usuario.getNombreCompleto(), ""+usuario.getNumeroDocumento(), ""+ coDR.getDrEntidad(), coDR.getCausalDuplicadoCo().toLowerCase()}));
    		*/
            if (empresa!=null) {
	    		request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
	            		new Object [] {""+empresa.getNombre(), ""+empresa.getNumeroDocumento(),
	    				               ""+ (coDR.getDrEntidad()!=null ? coDR.getDrEntidad():""),
	    				               ""+coDR.getCausalDuplicadoCo().toLowerCase()}));
            }

    	} else if ( ConstantesCO.CO_ANULACION.equalsIgnoreCase(formato) ) {

    		Usuario empresa = usuariosLogic.getUsuarioEmpresaById(usuario.getIdUsuario());
            
    		if (empresa!=null) {
	        	request.setAttribute("descTextoAnulacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.textoAnulacion",
	            		new Object [] {""+empresa.getNombre(), ""+empresa.getNumeroDocumento(),
	    				""+(usuario!=null?usuario.getNombreCompleto():""),
	    				""+ (coDR.getDrEntidad()!=null?coDR.getDrEntidad():"")}));
    		}

    		request.setAttribute("adjuntoIdFirma", elements.getString("adjuntoIdFirma"));
    	}

        filter = new HashUtil<String, Object>();
        filter.put("drId", new Long(drId));
        filter.put("sdr", sdr);


        Table tModificacionesDR = ibatisService.loadGrid("suse.resolutor.solicitud.grilla", filter);
        request.setAttribute("tModificacionesDR", tModificacionesDR);
        request.setAttribute("orden", elements.get("ordenId")!=null?elements.get("ordenId"):elements.get("orden"));
        request.setAttribute("mto", elements.get("mto"));
        request.setAttribute("formato", elements.getString("formato").toLowerCase());
        request.setAttribute("idAcuerdo", idAcuerdo);
        request.setAttribute("DETALLE.dr_id", coDR.getDrId());
        request.setAttribute("DETALLE.sdr", coDR.getSdr());
        request.setAttribute("DETALLE.suce_id", coDR.getSuceId());
        request.setAttribute("vigente", coDR.getVigente());

        request.setAttribute("permiteCrearSolicitudRectif", this.formatoLogic.permiteCrearSolicitudRectif(coDR.getSuceId()));

	    HashUtil<String, Object> filterVersionesDR = new HashUtil<String, Object>();
        filterVersionesDR.put("drId", coDR.getDrId());
        filterVersionesDR.put("transmitido", "1");
        request.setAttribute("filterVersionesDR", filterVersionesDR);
        request.setAttribute("version", sdr);

        try {
        	request.setAttribute("habilitarBtnFirma", this.resolutorLogic.habilitarBtnFirma(coDR.getSuceId(), new Long(drId), new Long(sdr)) );
		} catch (Exception e) {
			request.setAttribute("habilitarBtnFirma", ConstantesCO.OPCION_NO );
		}

        Orden ordenObj = ordenLogic.getOrdenById(new Long(elements.get("ordenId")!=null?elements.getString("ordenId"):elements.getString("orden")), new Integer(elements.getString("mto")));

        HashUtil<String, Object> filterTupa = new HashUtil<String, Object>();
        filter.put("idTupa", ordenObj.getIdTupa());
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");

        Formato objFormato = (Formato)formatoLogic.getFormatoByTupa(ordenObj.getIdFormato(), ordenObj.getIdTupa());

        request.setAttribute("nombreFormato", objFormato.getFormato()+" - "+objFormato.getNombre()+" (TUPA: "+tupa+")");

        String permiteRectificar = "N";
        //if ("S".equals(coDR.getString("DETALLE.RECTIFICA_DR")) && "A".equals(coDR.getString("DETALLE.ESTADO")) && "S".equals(coDR.getString("DETALLE.CERRADA"))){
        if ("S".equals(coDR.getRectificaDr()) && "A".equals(coDR.getEstadoRegistro())){ //&& "S".equals(coDR.getCerrada())){

            //if (ConstantesCO.OPCION_SI.equals(coDR.getEsRectificacion())){
                Table tAdjuntosCODR = ibatisService.loadGrid("resolutor.co.dr.adjuntos.grilla", filter);
                if (tAdjuntosCODR!=null && tAdjuntosCODR.size() > 0) {
                	permiteRectificar = ConstantesCO.OPCION_SI;
                }
            /*} else {
            	permiteRectificar = ConstantesCO.OPCION_SI;
            }*/

        }
        
        if(elements.get("drId") != null) {
        
		String permiteFirmaDigital = "N"; 
		
		try {
			permiteFirmaDigital = iopLogic.permiteFirmaDigital(ordenObj.getOrden());
		} catch (Exception e) {
			logger.error("ERROR: Al intentar cargar los datos de los documento resolutivos", e);
		}
		
		MessageList messageList = (MessageList)request.getAttribute(Constantes.MESSAGE_LIST);
        if (messageList==null) messageList = new MessageList();
        String habilitarFirmas = "S";
        
        if(formato.equals("mct001")||formato.equals("MCT001")||formato.equals("mct003")||formato.equals("MCT003")){
        	Message messageFD =  certificadoOrigenLogic.habilitaFirma(new Long(drId),sdr);
        	
        	if(messageFD != null && messageFD.getMessage() != null){
        		habilitarFirmas = "N";
        		messageList.add(messageFD);
        	}
        	request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        }
   
        request.setAttribute("habilitarFirmas", habilitarFirmas);
        
        request.setAttribute("permiteFirmaDigital", permiteFirmaDigital);
        }

        request.setAttribute("permiteRectificar", permiteRectificar);
        request.setAttribute("estadoRegistro", ordenObj.getEstadoRegistro());
        // Colocamos la lista de si y no para los combos
        request.setAttribute("listaSiNo", OptionListFactory.getListaSiNo());
        
    	return new ModelAndView(datosDocResolutivo);
	}
	
    
	public ModelAndView guardarDatosFirma(HttpServletRequest request,HttpServletResponse response) {
		InteroperabilidadFirmasLogic iopLogic = (InteroperabilidadFirmasLogic) SpringApplicationContext.getBean("interoperabilidadFirmasLogic");
		HashUtil<String, String> elements = RequestUtil.getParameter(request);
		
		MessageList messageList = new MessageList();
		
		Long drId = elements.getLong("DETALLE.dr_id");
		Integer sdr = elements.getInt("DETALLE.sdr");
		String usuarioFirmanteFirma = elements.getString("nombreUsuario");
		String emitidoPosteriori2Firma = elements.getString("posteriori");
		String formato = elements.getString("formato");
		
		try {

			
			System.out.println("DR ID "+drId);
			System.out.println("sdr "+sdr);
			System.out.println("usuarioFirmanteFirma "+usuarioFirmanteFirma);
			System.out.println("emitidoPosteriori2Firma "+emitidoPosteriori2Firma);
			System.out.println("formato "+formato);
			
			iopLogic.actualizaFirmaExportador(drId, sdr, usuarioFirmanteFirma, formato, emitidoPosteriori2Firma);
			
			Message message = new Message("insert.success");
			messageList.add(message);
			
		} catch (Exception e) {
			logger.error("ERROR: Al intentar grabar los datos de la Firma (CertificadoOrigenFormatoController->guardarDatosFirma", e);
			messageList.add(new ErrorMessage("insert.error", new Exception("ERROR: Al intentar grabar los datos de la Firma")));
		}
		
		// Llenamos todo lo que vino para mantener la informaci�n en request
        RequestUtil.setAttributes(request); 

        request.setAttribute("emitidoPosteriori2Firma", emitidoPosteriori2Firma); // 20190111 NPCS Produccion Completa
        
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        return this.cargarDatosDocumentoResolutivo(request, response);
	}
    
	public ModelAndView cargarDatosDocumentoResolutivoResolutor(HttpServletRequest request,HttpServletResponse response) {
		HashUtil<String, String> elements = RequestUtil.getParameter(request);
		UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
        request.setAttribute("usuarioCo", usuario.getIdUsuario());
        request.setAttribute("nombreCo", usuario.getNombreCompleto());
        
        //I GCHAVEZ - 28/02/2020 - Correcci�n firma digital. Error en Producci�n.
        UsuarioCO usuarioDatos = new UsuarioCO();
        usuarioDatos.setIdUsuario(usuario.getIdUsuario());
        Solicitante solicitanteDetail = formatoLogic.getUsuarioDetail(usuarioDatos);
        request.setAttribute("usuarioSol", solicitanteDetail.getUsuarioSOL());//20170803_JMC Alianza Pacifico: Para que el usuarioSOL se pase como parametro al agente firmador

        System.out.println("Usuario sol 1: "+usuario.getUsuarioSOL());
        System.out.println("Usuario sol 2: "+solicitanteDetail.getUsuarioSOL());
        //F GCHAVEZ - 28/02/2020 - Correcci�n firma digital. Error en Producci�n.
        
		int drId = elements.getInt("drId");
		int sdr = elements.get("sdrSelect")!=null?elements.getInt("sdrSelect"):elements.getInt("sdr");
		String formato = elements.getString("formato");
		//HashUtil<String, Object> coDR = certificadoOrigenLogic.getCertificadoOrigenDRByDrId(drId, sdr, formato.toLowerCase());
		Map<String, Object> params = new HashMap<String, Object>();
        //params.put("coDrId", mct001DrId);
        params.put("drId", drId);
        params.put("sdr", sdr);
        params.put("formato", formato.toLowerCase());
		CertificadoOrigenDR coDR = resolutorLogic.getCODrResolutorById(params);

		Integer idAcuerdo = coDR.getAcuerdoInternacionalId();

		//coDR.put("DETALLE.RECTIFICA_DR", (coDR.get("DETALLE.RECTIFICA_DR")!=null?coDR.get("DETALLE.RECTIFICA_DR").toString().trim():"N"));
		//RequestUtil.setAttributes(request, coDR);
		if (coDR.getRectificaDr()==null){
			coDR.setRectificaDr("N");
		}
		RequestUtil.setAttributes(request, "DR.", coDR);

		HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("coDrId", coDR.getCoDrId());
        filter.put("drId", coDR.getDrId());//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
        filter.put("sdr", coDR.getSdr());//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados

    	//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados      	
    	//Datos para la visualizacion de la Firma
    	if (coDR != null) {
    		// Colocamos aqu� la informaci�n que corresponde a los datos del firmante
            request.setAttribute("secuenciaFuncionario",coDR.getSecuenciaFuncionario());
            request.setAttribute("usuarioFirmante",coDR.getUsuarioFirmanteExp());
            request.setAttribute("fechaFirma",coDR.getFechaFirmaEev());
            request.setAttribute("nroReferenciaCertificado", coDR.getNroReferenciaCertificado());
            request.setAttribute("cargoEmpresaExportador", coDR.getCargoEmpresaExportador());
            request.setAttribute("telefonoEmpresaExportador", coDR.getTelefonoEmpresaExportador());
            request.setAttribute("fechaFirmaExportador", coDR.getFechaFirmaExportador()); // 20150612_RPC: Acta.34:Firmas
            request.setAttribute("emitidoPosteriori2", coDR.getEmitidoPosteriori2()); // 20150612_RPC: Acta.34:Firmas
          
            System.out.println("************ Emitido Posteriori 2"+ coDR.getEmitidoPosteriori2());
            
            Table tAdjuntosCODR = ibatisService.loadGrid("resolutor.co.dr.adjuntos.grilla", filter);
            request.setAttribute("tAdjuntosCODR", tAdjuntosCODR);
            request.setAttribute("numAdjuntosCODR", tAdjuntosCODR!=null?tAdjuntosCODR.size():0);
            
            HashUtil<String, Object> filterFirmate = new HashUtil<String, Object>();           
            filterFirmate.put("idEntidad", coDR.getEntidadId());//20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
            request.setAttribute("filterFirmantesCertificador", filterFirmate);
            
            request.setAttribute("tipoDr", coDR.getTipoDr());
            request.setAttribute("estadoSDR", coDR.getEstadoSDR());
            request.setAttribute("esRectificacion", coDR.getEsRectificacion());
    		
    	}
    	
    	if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato)){

            //int idAcuerdo = elements.getInt("idAcuerdo");
        	Table tMercancias = ibatisService.loadGrid("certificado_origen.mercancia.dr.datos", filter);
        	request.setAttribute("tMercanciasDr", tMercancias);

        	Table tFacturas = ibatisService.loadGrid("certificado_origen.factura.dr.datos", filter);
        	request.setAttribute("tFacturasDr", tFacturas);
        	request.setAttribute("enIngles", certificadoOrigenLogic.obtenerIdiomaLlenado(coDR.getAcuerdoInternacionalId()));
        	
    	} else if (ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(formato) ){   		

    		Usuario empresa = usuariosLogic.getUsuarioEmpresaById(usuario.getIdUsuario());

    		/*
    		request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
            		new Object [] {""+usuario.getNombreCompleto(), ""+usuario.getNumeroDocumento(), ""+ coDR.getDrEntidad(), coDR.getCausalDuplicadoCo().toLowerCase()}));
    		*/
            if (empresa!=null) {
	    		request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
	            		new Object [] {""+empresa.getNombre(), ""+empresa.getNumeroDocumento(),
	    				               ""+ (coDR.getDrEntidad()!=null ? coDR.getDrEntidad():""),
	    				               ""+coDR.getCausalDuplicadoCo().toLowerCase()}));
            }

    	} else if ( ConstantesCO.CO_ANULACION.equalsIgnoreCase(formato) ) {

    		Usuario empresa = usuariosLogic.getUsuarioEmpresaById(usuario.getIdUsuario());
            
    		if (empresa!=null) {
	        	request.setAttribute("descTextoAnulacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.textoAnulacion",
	            		new Object [] {""+empresa.getNombre(), ""+empresa.getNumeroDocumento(),
	    				""+(usuario!=null?usuario.getNombreCompleto():""),
	    				""+ (coDR.getDrEntidad()!=null?coDR.getDrEntidad():"")}));
    		}

    		request.setAttribute("adjuntoIdFirma", elements.getString("adjuntoIdFirma"));
    	}

        filter = new HashUtil<String, Object>();
        filter.put("drId", new Long(drId));
        filter.put("sdr", sdr);


        Table tModificacionesDR = ibatisService.loadGrid("suse.resolutor.solicitud.grilla", filter);
        request.setAttribute("tModificacionesDR", tModificacionesDR);
        request.setAttribute("orden", elements.get("ordenId")!=null?elements.get("ordenId"):elements.get("orden"));
        request.setAttribute("mto", elements.get("mto"));
        request.setAttribute("formato", elements.getString("formato").toLowerCase());
        request.setAttribute("idAcuerdo", idAcuerdo);
        request.setAttribute("DETALLE.dr_id", coDR.getDrId());
        request.setAttribute("DETALLE.sdr", coDR.getSdr());
        request.setAttribute("DETALLE.suce_id", coDR.getSuceId());
        request.setAttribute("vigente", coDR.getVigente());

        request.setAttribute("permiteCrearSolicitudRectif", this.formatoLogic.permiteCrearSolicitudRectif(coDR.getSuceId()));

	    HashUtil<String, Object> filterVersionesDR = new HashUtil<String, Object>();
        filterVersionesDR.put("drId", coDR.getDrId());
        filterVersionesDR.put("transmitido", "1");
        request.setAttribute("filterVersionesDR", filterVersionesDR);
        request.setAttribute("version", sdr);

        try {
        	request.setAttribute("habilitarBtnFirma", this.resolutorLogic.habilitarBtnFirma(coDR.getSuceId(), new Long(drId), new Long(sdr)) );
		} catch (Exception e) {
			request.setAttribute("habilitarBtnFirma", ConstantesCO.OPCION_NO );
		}

        Orden ordenObj = ordenLogic.getOrdenById(new Long(elements.get("ordenId")!=null?elements.getString("ordenId"):elements.getString("orden")), new Integer(elements.getString("mto")));

        HashUtil<String, Object> filterTupa = new HashUtil<String, Object>();
        filter.put("idTupa", ordenObj.getIdTupa());
        String tupa = ibatisService.loadElement("comun.tupa.element", filter).getString("TUPA");

        Formato objFormato = (Formato)formatoLogic.getFormatoByTupa(ordenObj.getIdFormato(), ordenObj.getIdTupa());

        request.setAttribute("nombreFormato", objFormato.getFormato()+" - "+objFormato.getNombre()+" (TUPA: "+tupa+")");

        String permiteRectificar = "N";
        //if ("S".equals(coDR.getString("DETALLE.RECTIFICA_DR")) && "A".equals(coDR.getString("DETALLE.ESTADO")) && "S".equals(coDR.getString("DETALLE.CERRADA"))){
        if ("S".equals(coDR.getRectificaDr()) && "A".equals(coDR.getEstadoRegistro())){ //&& "S".equals(coDR.getCerrada())){

            //if (ConstantesCO.OPCION_SI.equals(coDR.getEsRectificacion())){
                Table tAdjuntosCODR = ibatisService.loadGrid("resolutor.co.dr.adjuntos.grilla", filter);
                if (tAdjuntosCODR!=null && tAdjuntosCODR.size() > 0) {
                	permiteRectificar = ConstantesCO.OPCION_SI;
                }
            /*} else {
            	permiteRectificar = ConstantesCO.OPCION_SI;
            }*/

        }

        request.setAttribute("permiteRectificar", permiteRectificar);
        request.setAttribute("estadoRegistro", ordenObj.getEstadoRegistro());

    	return new ModelAndView(datosDocResolutivoResolutor);
	}
	
    public ModelAndView cargarAdjuntos(HttpServletRequest request, HttpServletResponse response) {
    	cargarInformacionAdjunto(request);
        return new ModelAndView(adjuntos);
    }

    public ModelAndView cargarArchivo(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	int orden = datos.getInt("orden");
    	int mto = datos.getInt("mto");
    	int formato = datos.getInt("formato");
    	int adjunto = datos.getInt("adjunto");
    	Integer mensajeId = (datos.get("mensajeId")!=null && !datos.get("mensajeId").equals("")) ? datos.getInt("mensajeId") : null;
    	String esDetalle = (datos.get("esDetalle")!=null && !datos.get("esDetalle").equals("")) ? datos.getString("esDetalle") : null;
    	//Integer notificacionId = (datos.get("notificacionId")!=null && datos.get("notificacionId").equals("")) ? datos.getInt("notificacionId") : null;
    	int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;

     	MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                byte [] bytes = multipartFile.getBytes();
                String nombre = multipartFile.getOriginalFilename();
                messageList = formatoLogic.uploadFile(orden,mto,formato,adjunto,adjuntoTipo,nombre,mensajeId,esDetalle,bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cargarInformacionAdjunto(request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return new ModelAndView(adjuntos);
    }

    public ModelAndView eliminarAdjunto(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        int id = 0;
        int mto = datos.getInt("mto");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            id = Integer.parseInt(row.getCell("ADJUNTO_ID").getValue().toString());

        	logger.debug("Se eliminara adjunto_id = "+id+", mto = "+mto);

        	adjuntoLogic.eliminarAdjuntoFormato(id, mto);
        }

        cargarInformacionAdjunto(request);
        return new ModelAndView(adjuntos);
    }

    private void cargarInformacionAdjunto(HttpServletRequest request) {
 	   HashUtil<String, String> datos = RequestUtil.getParameter(request);
 	   HashUtil<String, Object> filter = new HashUtil<String, Object>();

	   int idFormatoEntidad = datos.getInt("idFormatoEntidad");
	   int idFormato = datos.getInt("idFormato");

	   int idRequerido = datos.getInt("adjunto");
	   int idOrden = datos.getInt("orden");
	   int mto = datos.getInt("mto");
	   AdjuntoRequerido adjunto = formatoLogic.getRequeridoById(idFormato,idRequerido);

	   filter.put("idFormato", idFormato);
	   filter.put("idRequerido", idRequerido);
	   filter.put("idOrden", idOrden);
	   filter.put("mto", mto);

 	   Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.grilla", filter);

 	   HashUtil<String, Object> adjuntoDetalle = ibatisService.loadElement("formato.adjuntos.detalle.element", filter);
 	   if (adjuntoDetalle!=null && adjuntoDetalle.size() > 0) {
 		   request.setAttribute("idAdjuntoDetalle", adjuntoDetalle.get("ID"));
 		   request.setAttribute("nombreAdjuntoDetalle", adjuntoDetalle.get("NOMBRE"));
 	   }

 	   String hide = datos.getString("bloqueado").equals("S")?"yes":
 		             datos.getString("transmitido").equals("S")?"yes":
 		             adjunto.getPermiteAdicional().equals("N")?"yes":"no";

 	   request.setAttribute("tAdjuntos", tAdjuntos);
        request.setAttribute("idFormatoEntidad", idFormatoEntidad);
        request.setAttribute("formato", idFormato);
        request.setAttribute("idFormato", idFormato);
        request.setAttribute("adjunto", idRequerido);
        request.setAttribute("orden", idOrden);
        request.setAttribute("mto", mto);
        request.setAttribute("titulo", adjunto.getDescripcion());
        request.setAttribute("permiteAdicional", adjunto.getPermiteAdicional());
        request.setAttribute("bloqueado", datos.getString("bloqueado"));
        request.setAttribute("transmitido", datos.getString("transmitido"));
        request.setAttribute("controller", datos.getString("controller"));
        request.setAttribute("hide", hide);

    }

    public ModelAndView desisteOrdenFormato(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        long orden = datos.getLong("orden");
        int mto = datos.getInt("mto");

        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        // Se procede a desistir la ORDEN o la SUCE
        if (ordenObj.getSuce() == null || "".equals(ordenObj.getSuce())) {
            int idEntidad = datos.getInt("idEntidadCertificadora");
            ordenLogic.desisteOrden(idEntidad, orden, mto);
        } else {
            suceLogic.desisteSuce(ordenObj.getSuce());
        }

        return listarCertificadoOrigen(request,response);
    }

    /**
     * Carga la pantalla para realizar subsanaciones
     * @param request
     * @param response
     * @return
     */
    @SuppressWarnings("unchecked")
    public ModelAndView cargarSuce(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String controller = datos.getString("formato").toLowerCase()+".htm";
        int idSuce = datos.getInt("suce");
        int idModifSuce = datos.getInt("modificacionSuceId");

        Suce suceObj = suceLogic.getSuceById(idSuce);

        request.setAttribute("controller", controller);
        request.setAttribute("suce", idSuce);
        request.setAttribute("modifsuce", idModifSuce);
        request.setAttribute("orden", datos.getInt("orden"));
        
        //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
        String estadoAcuerdoPais = validaAcuerdoPais(datos.getLong("orden"));
        request.setAttribute("estadoAcuerdoPais",estadoAcuerdoPais);
        
        //20170113_GBT ACTA CO-004-16 Y ACTA CO-009-16
        request.setAttribute("idAcuerdo", datos.getInt("idAcuerdo"));
        
        request.setAttribute("mto", datos.getInt("mto"));
        request.setAttribute("idFormato", datos.getInt("idFormato"));
        request.setAttribute("formato", datos.getString("formato"));
        request.setAttribute("transmitido", "N");
        request.setAttribute("hide", "no");
        request.setAttribute("seleccionarNotificacion", "yes");
        request.setAttribute("tipo", datos.getString("tipo"));
        request.setAttribute("modificacionSuceXMto", suceObj.getModificacionSuceXMto());
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");
        request.setAttribute("estadoModif", (datos.getString("estadoModif") != null ? datos.getString("estadoModif") : ""));

        /*HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suceId", suceObj.getSuce());
        HashUtil<String, Object> datosMtoPendiente = ibatisService.loadElement("suce.modificacionNoTransmitida", filter);
        if (datosMtoPendiente.size() > 0) {
    	    int mtoSUCE = datosMtoPendiente.getInt("MTO");
    	    request.setAttribute("mtoSUCE", mtoSUCE);
        }
        */
        cargarInformacionAdjuntoSubsanacion(request, datos);

        return new ModelAndView(subsanacion);
    }

    /**
     * Carga la pantalla para de adjuntos de la notificaci�n
     * @param request
     * @param response
     * @return
     */
    @SuppressWarnings("unchecked")
    public ModelAndView cargarNotificacionAdjuntos(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);

    	// Cargamos los adjuntos asociados a la notificacion
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("notificacionId", datos.getInt("idNotificacion"));
        Table tAdjuntosNotificacion = ibatisService.loadGrid("resolutor.evaluador.suce.notificacionSubsanacionSuce.adjuntos", filter);
        request.setAttribute("tAdjuntos", tAdjuntosNotificacion);

        request.setAttribute("permiteAdicional", ConstantesCO.OPCION_NO);
        request.setAttribute("bloqueado", ConstantesCO.OPCION_SI);
        request.setAttribute("hide", "si");
        request.setAttribute("notifAdj", ConstantesCO.OPCION_SI);

        return new ModelAndView(adjuntos);
    }

    @SuppressWarnings("unchecked")
    private void cargarInformacionAdjuntoSubsanacion(HttpServletRequest request, HashUtil<String, String> datos) {
        //HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int suceId = datos.getInt("suce");
        int formato = datos.getInt("idFormato");
        int orden = datos.getInt("orden");
        int mto = datos.getInt("mto");
        int mensajeId = datos.getInt("mensajeId");
        int modifSuceId = datos.getInt("modificacionSuceId");

        ModificacionSuce modifSuceObj = null;
        Suce suceObj = suceLogic.getSuceById(suceId);
        request.setAttribute("estadoRegistroSUCE", suceObj.getEstadoRegistro());

        if (modifSuceId!=0) {
            modifSuceObj = suceLogic.loadModificacionSuceByIdMensajeId(suceId, modifSuceId, mensajeId);

        	request.setAttribute("mensaje", modifSuceObj.getMensaje());
            request.setAttribute("transmitido", modifSuceObj.getTransmitido());
            request.setAttribute("estadoRegistro", modifSuceObj.getEstadoRegistro());
            request.setAttribute("mtoSUCE", modifSuceObj.getMto());
            request.setAttribute("motivoRechazo", modifSuceObj.getMotivoRechazo());
            request.setAttribute("bloqueada", modifSuceObj.getBloqueada());

            String hide = modifSuceObj.getTransmitido().equals("N")?"no":"yes";
            String seleccionarNotificacion = modifSuceObj.getTransmitido().equals("N")?"yes":"no";
            request.setAttribute("hide", hide);
            request.setAttribute("seleccionarNotificacion", seleccionarNotificacion);

            request.setAttribute("mensajeId", modifSuceObj.getIdMensaje());
            request.setAttribute("modificacionSuceId", modifSuceId);

            // Si la Modificacion de SUCE fue creada seleccionando una Notificacion, entonces es de tipo "SUBSANACION"
            /*HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("suce", suceId);
            filter.put("modifsuce", modifSuceId);
            boolean tieneNotificacionesAtendidas = ibatisService.loadGrid("suce.notificacionesAtendidas", filter).size() > 0;
            if (tieneNotificacionesAtendidas) {
                request.setAttribute("tipo", "S");
            } else {
                request.setAttribute("tipo", "M");
            }
            */
            if (modifSuceObj.getTipoModificacionSuce() == 1) {
            	request.setAttribute("tipo", "M");
            } else if (modifSuceObj.getTipoModificacionSuce() == 2) {
            	request.setAttribute("tipo", "S");
            }
            // Verificamos el parametro "Utiliza Modificacion SUCE x MTO"
            request.setAttribute("utilizaModificacionSuceXMto", suceObj.getModificacionSuceXMto());
        }

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suceId", suceObj.getSuce());
        HashUtil<String, Object> datosMtoPendiente = ibatisService.loadElement("suce.modificacionNoTransmitida", filter);
        if (datosMtoPendiente.size() > 0) {
    	    int mtoSUCE = datosMtoPendiente.getInt("MTO");
    	    request.setAttribute("mtoSUCE", mtoSUCE);
        } else if ("S".equals(suceObj.getModificacionSuceXMto()) && modifSuceObj!=null) {
        	request.setAttribute("mtoSUCE", modifSuceObj.getMto());
        }

        //Pintar modificacion de suce
        filter.put("suce", suceId);
        filter.put("modifsuce", modifSuceId);
        Table tNotificaciones = ibatisService.loadGrid("suce.notificacion_subsanacion.grilla", filter);
        request.setAttribute("tNotificaciones", tNotificaciones);

        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
//        filterADJ.put("idFormato", formato);
//        filterADJ.put("idOrden", orden);
//        filterADJ.put("mto", modifSuceObj!=null && modifSuceObj.getMto()!=null ? modifSuceObj.getMto() : mto); // Si la Modificacion de SUCE trabaja con MTO o no
        filterADJ.put("idMensaje", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.suce.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);
    }

    public ModelAndView guardarSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> element = null;//RequestUtil.getParameter(request);

        RequestUtil.setAttributes(request);

        int suceId = datos.getInt("suce");
        //String formato = datos.getString("formato");
        List<String> notificaciones = obtenerCodigosElegidos(request);
        String mensaje = datos.getString("mensaje");
        String tipo = datos.getString("tipo");

        int modifSuceId = 0;
        int mensajeId = 0;
        String mensajeValidacion = validarGuardarModificacionSuce(/*formato, */datos.getString("estadoRegistro"), datos.getString("tipo"), notificaciones);
        if (mensajeValidacion==null) {
            element = suceLogic.creaSubsanacion(suceId, tipo, mensaje, notificaciones);
            modifSuceId = element.getInt("modificacionSuceId");

            if (modifSuceId > 0) {
            	mensajeId = element.getInt("mensajeId");

                request.setAttribute(Constantes.MESSAGE_LIST, (MessageList)element.get("messageList"));
                request.setAttribute("mensajeId", mensajeId);
                datos.put("mensajeId", mensajeId);
                //if (modifSuceId != 0) {
                    request.setAttribute("modificacionSuceId", modifSuceId);
                    datos.put("modificacionSuceId", modifSuceId);
                //}

                ModificacionSuce modifSuceObj = new ModificacionSuce();
                modifSuceObj = suceLogic.loadModificacionSuceByIdMensajeId(suceId, modifSuceId, mensajeId);
                if (modifSuceObj != null) {
                    String seleccionarNotificacion = modifSuceObj.getTransmitido().equals("N")?"yes":"no";
                    request.setAttribute("seleccionarNotificacion", seleccionarNotificacion);

                    String transmitido = modifSuceObj.getTransmitido();//suceLogic.loadTransmitidoModifSuce(suceId, element.getInt("modificacionSuceId"));
                    request.setAttribute("mtoSUCE", modifSuceObj.getMto());
                    request.setAttribute("transmitido", transmitido);
                }

                Suce suceObj = suceLogic.getSuceById(suceId);
                request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");
            } else {
                request.removeAttribute("modificacionSuceId");
                request.setAttribute(Constantes.MESSAGE_LIST, (MessageList)element.get("messageList"));

                request.removeAttribute("mensaje");

            }

        } else {
            request.removeAttribute("modificacionSuceId");
            MessageList messageList = new MessageList();
            messageList.add(new ErrorMessage(mensajeValidacion, new Exception("ERROR al intentar guardar la modificacion de la SUCE")));
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

            request.removeAttribute("mensaje");
        }

        /*//Pintar modificacion de suce
        HashUtil<String, Object> filterADJ = new HashUtil<String, Object>();
        filterADJ.put("idFormato", datos.getInt("idFormato"));
        filterADJ.put("idOrden", datos.getInt("orden"));
        filterADJ.put("mto", datos.getInt("mto"));
        filterADJ.put("idMensaje", mensajeId);
        Table tAdjuntos = ibatisService.loadGrid("formato.adjuntos.suce.grilla", filterADJ);
        request.setAttribute("tAdjuntos", tAdjuntos);

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", suceId);
        filter.put("modifsuce", modifSuceId);
        Table tNotificaciones = ibatisService.loadGrid("suce.notificacion.grilla", filter);
        request.setAttribute("tNotificaciones", tNotificaciones);
        */
        if (modifSuceId > 0){
            cargarInformacionAdjuntoSubsanacion(request, datos);
        }

        return new ModelAndView(subsanacion);
    }

    @SuppressWarnings("unused")
    private List<String> obtenerCodigosElegidos(HttpServletRequest request) {
        List<String> codPaises = new ArrayList<String>();
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));
        Table tabla = (Table)tables.get("notificaciones");
        String codNotificacion = "";
        if (tabla != null) {
            for(int contRow = 0; contRow < tabla.size(); contRow++){
                Row row = tabla.getRow(contRow);
                if ("S".equals(row.getCell("SELECCIONE").getValue().toString())){
                    codNotificacion = row.getCell("ID").getValue().toString();
                    codPaises.add(codNotificacion);
                }
            }
        }
        return codPaises;
    }

    private String validarGuardarModificacionSuce(/*String formato, */String estadoRegistro, String tipo, List<String> notificaciones) {
        String mensaje = null;
        //if (formato.equalsIgnoreCase("MTC003")) {
            if (estadoRegistro.equalsIgnoreCase("S") && tipo.equalsIgnoreCase("S") && notificaciones.size() == 0) {
                mensaje = "co.validacionGrabarSubsanacionSuce.error";
            }
        //}
        return mensaje;
        //return null;
    }


    public ModelAndView eliminarSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int suceId = datos.getInt("suce");
        int modificacionSuceId = datos.getInt("modificacionSuceId");
        int orden = datos.getInt("orden");
        int mto = datos.getInt("mto");
        String modificacionSuceXMto = datos.getString("modificacionSuceXMto");

        //Eliminar modificaci�n
        MessageList messageList = null;

    	messageList = suceLogic.eliminaSubsanacion(suceId, modificacionSuceId);

        //Repintar formato
        Orden ordenObj = ordenLogic.getOrdenById(new Long(orden), mto);
        messageList.setObject(ordenObj);
        cargarInformacionFormato(request, messageList);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        return new ModelAndView(this.formato);
    }

    @SuppressWarnings("unchecked")
    public ModelAndView actualizaSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        //String formato = datos.getString("formato");
        List<String> notificaciones = obtenerCodigosElegidos(request);

        RequestUtil.setAttributes(request);

        int suceId = datos.getInt("suce");
        int mensajeId = datos.getInt("mensajeId");
        int modificacionSuceId = datos.getInt("modificacionSuceId");
        String mensaje = datos.getString("mensaje");
        String tipo = datos.getString("tipo");

        String mensajeValidacion = validarGuardarModificacionSuce(/*formato, */datos.getString("estadoRegistro"), datos.getString("tipo"), notificaciones);
        if (mensajeValidacion==null) {
            MessageList messageList = suceLogic.actualizaSubsanacion(modificacionSuceId, mensajeId, tipo, mensaje, notificaciones);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

            Suce suceObj = suceLogic.getSuceById(suceId);
            request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");
        } else {
            MessageList messageList = new MessageList();
            messageList.add(new ErrorMessage(mensajeValidacion, new Exception("ERROR al intentar actualizar la modificacion de la SUCE")));
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);

            request.removeAttribute("mensaje");
        }

        cargarInformacionAdjuntoSubsanacion(request, datos);
        return new ModelAndView(subsanacion);
    }

    public ModelAndView cargarArchivoSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int orden = datos.getInt("orden");
        int mto = datos.get("mtoSUCE")!=null ? datos.getInt("mtoSUCE") : datos.getInt("mto");
        int formato = datos.getInt("idFormato");
        int idSuce = datos.getInt("suce");
        int adjuntoTipo = ConstantesCO.ADJUNTO_TIPO_PDF;

        MessageList messageList = null;
        if (request instanceof MultipartHttpServletRequest) {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = multipartRequest.getFile("archivo");
            try {
                byte [] bytes = multipartFile.getBytes();
                String nombre = multipartFile.getOriginalFilename();
                //long tamano = multipartFile.getSize();

                messageList = formatoLogic.cargarArchivoSubsanacion(datos.getInt("mensajeId"), nombre, bytes);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        RequestUtil.setAttributes(request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        Suce suceObj = suceLogic.getSuceById(idSuce);
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");

        cargarInformacionAdjuntoSubsanacion(request, datos);

        return new ModelAndView(subsanacion);
    }

    public ModelAndView eliminarAdjuntoSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));

        int idSuce = datos.getInt("suce");
        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        Table tablaAdjuntos = (Table)tables.get("ADJUNTOS");
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            int id = Integer.parseInt(row.getCell("ADJUNTO_ID").getValue().toString());

            adjuntoLogic.eliminarAdjuntoById(id);
        }
        RequestUtil.setAttributes(request);

        Suce suceObj = suceLogic.getSuceById(idSuce);
        request.setAttribute("desestimiento", "D".equalsIgnoreCase(suceObj.getEstadoRegistro()) || "S".equalsIgnoreCase(suceObj.getDesistido()) ? "S" : "N");
        cargarInformacionAdjuntoSubsanacion(request, datos);

        return new ModelAndView(subsanacion);
    }

    public ModelAndView cargarBusquedaCO(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);

    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	HashUtil<String, Object> filterPais = new HashUtil<String, Object>();

    	String key = "co.dr_aprobado_vigente.grilla";

    	UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        filter.put("usuarioId", usuario.getIdUsuario());
        filter.put("numeroDocumento", usuario.getNumeroDocumento());//T: 33728   28/09/2017  NPCS
        filter.put("documentoTipo", usuario.getTipoDocumento());//T: 33728   28/09/2017  NPCS
		filter.put("dr", datos.getString("dr"));
		filter.put("drEntidad", datos.getString("drEntidad"));
		filter.put("pais", datos.getString("pais"));
		filter.put("acuerdo", datos.getString("acuerdo"));
		//filter.put("desde", datos.getString("desde")); // JMC-18/05/2015 - Problema con anulaci�n de Certificado de origen
		filter.put("hasta", datos.getString("hasta"));
		filter.put("tipoCertificado", datos.getString("tipoCertificado"));
		
		if (ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(request.getParameter("formato"))) {
			filter.put("frmEvitado", ConstantesCO.CO_DUPLICADO);
		}
		if (!ConstantesCO.CO_ANULACION.equalsIgnoreCase(request.getParameter("formato"))) {//JMC-18/05/2015 - Problema con anulaci�n de Certificado de origen
			filter.put("desde", datos.getString("desde"));
		}		

		filterPais.put("pais", datos.getString("pais"));

    	Table tCertificadoOrigen = ibatisService.loadGrid(key, filter);
        request.setAttribute("tCertificadoOrigen", tCertificadoOrigen);

        RequestUtil.setAttributes(request);

    	return new ModelAndView(busquedaCO);
    }

    /*public ModelAndView crearDuplicadoFormato(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int idTupa = datos.getInt("idTupa");
        int idFormato = datos.getInt("idFormato");
        int acuerdo = datos.getInt("idAcuerdo");
        int pais = datos.getInt("idPais");
        int entidadCertificadora = datos.getInt("idEntidadCertificadora");
        int idSede = datos.getInt("idSede");
        Long drIdOrigen = datos.getLong("drIdOrigen");
        int sdrOrigen = datos.getInt("sdrOrigen");

        int solicitante = datos.getInt("SOLICITANTE.usuarioId");
        int empresa = !datos.getString("EMPRESA.id").equals("") ? datos.getInt("EMPRESA.id") : 0;
        int representante = !datos.getString("REPRESENTANTE.id").equals("") ? datos.getInt("REPRESENTANTE.id") : 0;

        MessageList messageList = ordenLogic.creaOrden(usuario, acuerdo, pais, entidadCertificadora, solicitante, representante, empresa, idTupa, idFormato, idSede);
        Orden orden = (Orden)messageList.getObject();
        certificadoOrigenLogic.insertCertificadoOrigenDuplicadoOrigen(orden.getIdFormatoEntidad(), drIdOrigen, sdrOrigen);
        cargarInformacionFormato(request, messageList);

        return new ModelAndView(duplicadoCertificadoOrigen);
    }*/

    /*public void cargarInformacionDuplicado(HttpServletRequest request, MessageList messageList) {
    	cargarInformacionGeneralFormato(request, messageList);
        cargarInformacionEspecificaDuplicado(request, messageList);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
    }


    public void cargarInformacionEspecificaDuplicado(HttpServletRequest request, MessageList messageList) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String puedeTransmitir = "N";

        Orden ordenObj = (Orden) messageList.getObject();

        // Carga los datos del detalle del formato
        if (ordenObj.getIdFormatoEntidad() != null){ // el idFormatoEntidad es en realidad el coId

        	String formato = datos.getString("formato");

            CertificadoOrigen certificadoOrigen = certificadoOrigenLogic.getCertificadoOrigenById(ordenObj.getIdFormatoEntidad(),formato);
            CODuplicado certificadoOrigenObj = (CODuplicado) certificadoOrigen;
            RequestUtil.setAttributes(request, "MCT002.", certificadoOrigenObj);

            request.setAttribute("nombreAcuerdo",certificadoOrigenObj.getNombreAcuerdo());
            request.setAttribute("drEntidad", certificadoOrigenObj.getDrEntidad());
            request.setAttribute("nombrePais",certificadoOrigenObj.getNombrePais());
            request.setAttribute("drOrigen",certificadoOrigenObj.getDrIdOrigen());
            request.setAttribute("nombreEntidad",certificadoOrigenObj.getNombreEntidad());
            request.setAttribute("fechaGeneracion",certificadoOrigenObj.getFechaDrEntidad());
            request.setAttribute("idAcuerdo",certificadoOrigenObj.getAcuerdoInternacionalId());
            request.setAttribute("idPais",certificadoOrigenObj.getPaisIsoIdAcuerdoInt());
            request.setAttribute("idEntidadCertificadora",certificadoOrigenObj.getEntidadId());
            request.setAttribute("idSede", certificadoOrigenObj.getSedeId());

            request.setAttribute("editable", ordenObj.getBloqueado().equals("S")?"no":"yes");


            String descCausal =certificadoOrigenObj.getCausalDuplicadoCo()!=null? certificadoOrigenLogic.getCausalById(certificadoOrigenObj.getCausalDuplicadoCo().toString()):"";
            request.setAttribute("descAceptacion", LabelConfig.getLabelConfig().getProperty("co.label.certificado.acepto_texto",
            		new Object [] {""+usuario.getNombreCompleto(), ""+usuario.getNumeroDocumento(), ""+certificadoOrigenObj.getDrEntidad(), "<span id='idCausal'>"+descCausal+"</span>"}));


            //Validaciones para la transmisi�n
            puedeTransmitir = "S";
            if((certificadoOrigenObj.getAceptacion()==null) || (certificadoOrigenObj.getAceptacion().equals("N"))){
            	puedeTransmitir="N";
            	messageList.add(new Message("co.falta.aceptacion"));
            }

            int cuenta = 0;

            // Verificar si hay adjuntos
            /*cuenta = formatoLogic.cuentaAdjuntosRequeridos(ordenObj.getIdFormato(), ordenObj.getOrden(), ordenObj.getMto());
            if(cuenta > 0){
                messageList.add(new Message("falta.adjuntos"));
                puedeTransmitir = "N";
            }*/

            // Seteamos variables de request importantes para el flujo de todo el certificado
            /*request.setAttribute("idFormatoEntidad", ordenObj.getIdFormatoEntidad());
            request.setAttribute("coId", ordenObj.getIdFormatoEntidad());
            request.setAttribute("numeroSolicitud", ordenObj.getNumOrden());
            request.setAttribute("orden", ordenObj.getOrden());
            request.setAttribute("mto", ordenObj.getMto());
            request.setAttribute("transmitido", ordenObj.getTransmitido());
            request.setAttribute("bloqueado", ordenObj.getBloqueado().equals("S")?ordenObj.getBloqueado():ordenObj.getTransmitido());


            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
                usuario.getRoles().containsKey(Rol.CO_ENTIDAD_EVALUADOR.getNombre())) {
            	String resultadoConfirmarEvaluacion = formatoLogic.permiteConfirmarFinEval(ordenObj.getIdFormatoEntidad());
                request.setAttribute("mostrarBotonConfirmarFinEval", resultadoConfirmarEvaluacion);

            }

        }
        request.setAttribute("puedeTransmitir", puedeTransmitir);
        request.setAttribute("fechaActual", ibatisService.loadElement("comun.fechaActual", null).getString("FECHA"));
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

    } */

    public ModelAndView transmiteSubsanacion(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int suceId = datos.getInt("suce");
        int modificacionSuceId = datos.getInt("modificacionSuceId");
        int mensajeId = datos.getInt("mensajeId");

        ModificacionSuce modifSuce = suceLogic.loadModificacionSuceByIdMensajeId(suceId, modificacionSuceId, mensajeId);
        MessageList messageList = suceLogic.transmiteSubsanacion(modifSuce);

        RequestUtil.setAttributes(request);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        cargarInformacionAdjuntoSubsanacion(request, datos);
        return new ModelAndView(subsanacion);
    }

    public ModelAndView transmiteSubsanacionSuceMTO(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        int suceId = datos.getInt("suce");
        int modificacionSuceId = datos.getInt("modificacionSuceId");
        long orden = datos.getLong("orden");
        int mto = datos.getInt("mto");
        int mensajeId = datos.getInt("mensajeId");
        String formato = datos.getString("formato");

        ModificacionSuce modifSuce = suceLogic.loadModificacionSuceByIdMensajeId(suceId, modificacionSuceId, mensajeId);
        
        MessageList messageList = new MessageList();
        
        if (ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato) ||
            ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase(formato) ||
            ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(formato)) {
	        HashUtil<String, Object> filterValidacion = new HashUtil<String, Object>();
	    	filterValidacion.put("idFormato", datos.getInt("idFormato"));
	    	filterValidacion.put("idOrden", (int)orden);
	    	filterValidacion.put("mto", mto);
	    	Message msgVal = this.formatoLogic.validacionPreTransmision(filterValidacion);
	        
	    	if (msgVal != null) {
	    		messageList.add(msgVal);
	    	}
    	}
        
    	if (messageList.size() == 0) {
    		messageList = suceLogic.transmiteSubsanacion(modifSuce);
    	}
        
        Orden ordenObj = ordenLogic.getOrdenById(orden, mto);

        messageList.setObject(ordenObj);
        cargarInformacionFormato(request,messageList);

       	return identificarPantallaFormato(formato.toLowerCase());

    }

    public ModelAndView cancelarSubsanacionSUCE(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        long numOrden = datos.getLong("numOrden");
        int suceId = datos.getInt("suce");
        int modificacionSuceId = datos.getInt("modificacionSuceId");
        String formato = datos.getString("formato");

        MessageList messageList = suceLogic.eliminaSubsanacion(suceId, modificacionSuceId);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);

        // Cargamos la version original de la SUCE
        Orden ordenObj = ordenLogic.loadOrdenByNumero(numOrden);
        messageList.setObject(ordenObj);

        cargarInformacionFormato(request, messageList);

        return identificarPantallaFormato(formato.toLowerCase());
    }

    public ModelAndView actualizarRepresentante(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request,Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        long orden = datos.getLong("orden");
        int mto = datos.getInt("mto");
        int solicitante = datos.getInt("SOLICITANTE.usuarioId");
        int representante = !datos.getString("REPRESENTANTE.id").equals("") ? datos.getInt("REPRESENTANTE.id") : 0;

        MessageList messageList = ordenLogic.updateOrdenRepresentante(solicitante, orden, mto, representante);

        cargarInformacionFormato(request, messageList);
        return new ModelAndView(this.formato);
    }

	/**
     * Muestra la pantalla del m�dulo de validaci�n de DJ's.
     * @param request
     * @param response
     * @return
     */
    public ModelAndView listarDeclaracionJurada(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        //20140624_JMC BUG 128
        String keyDeclaracionJurada="calificacionOrigen.registrados.grilla";
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
           		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
        		 usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
        	// VE TODOS LOS REGISTROS	
            filter.put("tipoDocumento", datos.getString("tipoDocumento"));
            filter.put("numeroDocumento", datos.getString("numeroDocumento"));
            filter.put("nombre", datos.getString("nombre"));
            
            request.setAttribute("tipoDocumento", datos.getString("tipoDocumento"));
            request.setAttribute("numeroDocumento", datos.getString("numeroDocumento"));
            request.setAttribute("nombre", datos.getString("nombre"));
            if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
                filter.put("filtroEntidad", datos.getString("filtroEntidad"));
            }
            request.setAttribute("hideDatosSoliciante", "no");
            
        } else {
            //20140624_JMC BUG 128
            if(usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
            		(usuario.getRoles().containsKey(Rol.CO_ENTIDAD_EVALUADOR.getNombre()) ||
            		 usuario.getRoles().containsKey(Rol.CO_ENTIDAD_SUPERVISOR.getNombre()))) {            	
            	keyDeclaracionJurada="calificacionOrigen.aprobados.grilla";
            	filter.put("filtroEntidad", usuario.getIdEntidad());
            	request.setAttribute("hideDatosSoliciante", "no");
            	
            } else { //20140624_JMC BUG 128           	
    	        filter.put("numeroDocumento", usuario.getNumeroDocumento());
    	        filter.put("tipoDocumento", usuario.getTipoDocumento());	        
                request.setAttribute("hideDatosSoliciante", "yes");
                
                if (datos.contains("filtroEntidad") && !"".equals(datos.getString("filtroEntidad"))){
                    filter.put("filtroEntidad", datos.getString("filtroEntidad"));
                }
            }
        }
        if (datos.contains("filtroAcuerdo") && !"".equals(datos.getString("filtroAcuerdo"))){
            filter.put("filtroAcuerdo", datos.getString("filtroAcuerdo"));
        }
        if (datos.contains("filtroEstadoEntidad") && !"".equals(datos.getString("filtroEstadoEntidad"))){        
            filter.put("filtroEstado", datos.getString("filtroEstadoEntidad"));
        }
        if (datos.contains("opcionFiltro") && !"".equals(datos.getString("opcionFiltro"))){
            if (ConstantesCO.DJ_FILTRO_CODIGO.equals(datos.getString("opcionFiltro")) && datos.contains("declaracion") && !"".equals(datos.getString("declaracion"))){
                filter.put("declaracion", datos.getString("declaracion"));
            } else if (ConstantesCO.DJ_FILTRO_DENOMINACION.equals(datos.getString("opcionFiltro")) && datos.contains("denominacion") && !"".equals(datos.getString("denominacion"))){
                filter.put("denominacion", datos.getString("denominacion"));
            } else if (ConstantesCO.DJ_FILTRO_SOLICITUD.equals(datos.getString("opcionFiltro")) && datos.contains("filtroSolicitud") && !"".equals(datos.getString("filtroSolicitud"))) {
                filter.put("filtroSolicitud", datos.getString("filtroSolicitud"));
            } else if(datos.contains("suce") && !"".equals(datos.getString("suce"))) {
                filter.put("filtroSuce", datos.getString("suce"));
            }
        }
        if (datos.contains("filtroNombreExportador") && !"".equals(datos.getString("filtroNombreExportador"))) {
            filter.put("filtroNombreExportador", datos.getString("filtroNombreExportador"));
        }
        
        RequestUtil.setAttributes(request);

        request.setAttribute("keyGrillaDeclaracionJurada", keyDeclaracionJurada);
        request.setAttribute("filterGrilla", filter);
        request.setAttribute("seleccionado", request.getParameter("seleccionado")!=null?request.getParameter("seleccionado"):"0");

        return new ModelAndView(listadoDeclaracionJurada);
    }

    public ModelAndView cargarOtraInformacionTramite(HttpServletRequest request, HttpServletResponse response) {
    	HashUtil<String, String> datos = RequestUtil.getParameter(request);
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	Integer orden = datos.getInt("orden");
    	Integer mto = datos.getInt("mto");
    	filter.put("ordenId", orden);

    	Table tablaJerarquia = formatoLogic.obtenerJerarquiaTramite(orden);
    	if (tablaJerarquia!=null || ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(datos.getString("formato"))) {
    		if (tablaJerarquia == null || tablaJerarquia.size() <= 1) {
    			request.setAttribute("noHayJerarquia", ConstantesCO.OPCION_SI);
    		} else {
    			request.setAttribute("noHayJerarquia", ConstantesCO.OPCION_NO);
    		}
    		request.setAttribute("mostrarJerarquia", "S");
    		request.setAttribute("tJerarquia", tablaJerarquia);
    	}

    	//Trazabilidad

    	UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);

        HashUtil<String, Object> filterTrazabilidad = new HashUtil<String, Object>();

        Orden ordenObj = ordenLogic.getOrdenById(Util.longValueOf(orden), mto);

        // Usuario CLAVE SOL
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
        	filterTrazabilidad.put("usuarioId", usuario.getIdUsuario());
        }
        // Si el usuario es de EXTRANET y NO tiene el rol HELP DESK
        else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
                 !usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
        	// VE TODA LA INFORMACION
        } else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
        	filterTrazabilidad.put("usuarioId", usuario.getIdUsuario());
        }

    	Table listadoTraza = null;
    	if (ordenObj.getSuce() == null){
        	filterTrazabilidad.put("orden", ordenObj.getNumOrden());
        	listadoTraza = ibatisService.loadGrid("trazabilidad.orden.grilla", filterTrazabilidad);
        	request.setAttribute("nombreColumna", "SOLICITUD");
        } else {
        	Suce suceObj = suceLogic.getSuceById(ordenObj.getSuce());
        	filterTrazabilidad.put("suce", suceObj.getNumSuce());
        	listadoTraza = ibatisService.loadGrid("trazabilidad.suce.grilla", filterTrazabilidad);
        	request.setAttribute("nombreColumna", "SUCE");
        }

        request.setAttribute("listadoTrazabilidad", listadoTraza);

    	RequestUtil.setAttributes(request);
    	return new ModelAndView(otraInformacionTramite);
    }

    protected String identificarNombrePantallaFormato(String nombreFormato){
    	String res = null;

       	if (ConstantesCO.CO_SOLICITUD.equals(nombreFormato.toLowerCase())) {
       		res = this.formato;
       	} else if (ConstantesCO.CO_DUPLICADO.equals(nombreFormato.toLowerCase())) {
       		res = duplicadoCertificadoOrigen;
       	} else if (ConstantesCO.CO_ANULACION.equals(nombreFormato.toLowerCase())) {
       		res = anulacionCertificadoOrigen;
       	} else if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(nombreFormato.toLowerCase())) {
       		res = this.calificacionAnticipadaCertificadoOrigen;
       	} else if (ConstantesCO.CO_REEMPLAZO.equals(nombreFormato.toLowerCase())) {
       		res = reemplazoCertificadoOrigen;
       	}

       	return res;
    }

    protected ModelAndView identificarPantallaFormato(String nombreFormato){

       	return new ModelAndView(identificarNombrePantallaFormato(nombreFormato));

    }
    //20140526 - JMC - BUG 1.PersonaNatural
    public Message validacionPreFormato(int usuarioId, Formato formato) {
        return formatoLogic.validacionPreFormato(usuarioId, formato);
    }
    

    /**
	 * Ejecuta la Busqueda CO Firma Digital
	 **/
	@SuppressWarnings("unchecked")
	public ModelAndView busquedaCOFD(HttpServletRequest request, HttpServletResponse response) {
		HashUtil<String, Object> filterAcuerdo = new HashUtil<String, Object>();
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		HashUtil<String, String> datos = RequestUtil.getParameter(request);
		
		UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);

		Message message = null;
		MessageList messageList = null;
		String codigoError = null;
		
		int acuerdo = (request.getParameter("acuerdo"))==null ? ConstantesCO.AC_ALIANZA_PACIFICO : Util.integerValueOf(request.getParameter("acuerdo"));
		String numCertificado = Util.stringValueOf(request.getParameter("numCertificado"));
		String idCertificado = Util.stringValueOf(request.getParameter("idCertificado"));
		String numRegistroFiscal = Util.stringValueOf(request.getParameter("numRegistroFiscal"));

		int paisExportador = (request.getParameter("paisExportador"))==null ? 0 : Util.integerValueOf(request.getParameter("paisExportador"));
		String mostrarNoVigentes = (request.getParameter("mostrarNoVigentes"))==null ? "N" : Util.stringValueOf(request.getParameter("mostrarNoVigentes"));
		
		Date fecha = (Date) ibatisService.loadElement("comun.fechaActual", null).get("FECHA");
		String fechaHoraActual = Util.getDate(fecha, FORMATO_FECHA_ES);

		filter.put("acuerdo", acuerdo);
		filterAcuerdo.put("idAcuerdo", acuerdo);

		if(numCertificado != null) filter.put("numCertificado", numCertificado);
		if(idCertificado != null) filter.put("idCertificado", idCertificado);
		if(datos.get("fechaDesde") != null) filter.put("fechaDesde", datos.getString("fechaDesde"));
		if(datos.get("fechaHasta") != null) filter.put("fechaHasta", datos.getString("fechaHasta"));
		if(paisExportador > 0) filter.put("paisExportador", paisExportador);
		if(mostrarNoVigentes != null) filter.put("mostrarNoVigentes", mostrarNoVigentes);
		
		if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
        		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())||usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre())
        	   ||usuario.getRoles().containsKey(Rol.VUCE_SUNAT_ESPECIALISTA.getNombre()))){
        			if(numRegistroFiscal != null) filter.put("numRegistroFiscal", numRegistroFiscal);
        		}else{
        			filter.put("numRegistroFiscal", usuario.getNumeroDocumento());
        		}
		
		Table tCertificados = ibatisService.loadGrid("certificadoOrigen.busqueda.COFD.grilla", filter);
		request.setAttribute("tCertificados", tCertificados);
		
		request.setAttribute("filterAcuerdo", filterAcuerdo);
		request.setAttribute("acuerdo", acuerdo);
		request.setAttribute("numCertificado", numCertificado);
		request.setAttribute("idCertificado", idCertificado);
		request.setAttribute("numRegistroFiscal", numRegistroFiscal);
		request.setAttribute("fechaDesde",datos.get("fechaDesde")!=null? datos.getString("fechaDesde"):"");
		request.setAttribute("fechaHasta",datos.get("fechaHasta")!=null? datos.getString("fechaHasta"):"");
		request.setAttribute("paisExportador", paisExportador);
		request.setAttribute("mostrarNoVigentes", mostrarNoVigentes);
		request.setAttribute("fechaHoraActual", fechaHoraActual);
		request.setAttribute("fechaActual", Util.getDate(fecha, "yyyyMMdd"));
		
		
		return new ModelAndView(busquedaCOFD);
	}

	//NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
	
	public ModelAndView descargarDocFD(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        long codId = datos.getLong("codId");
        //String codCertificado = datos.getString("codCertificado");
        String certificado = datos.getString("certificado");
        
        byte [] bytes = certificadoOrigenLogic.descargarDocFDById(codId);
        
        try {
            if (bytes != null && bytes.length > 0) {
                response.setContentType("application/force-download");
                response.setHeader("Content-Disposition", "attachment;filename="+certificado+".pdf");
                response.setContentLength(bytes.length);
                ServletOutputStream ouputStream = response.getOutputStream();
                ouputStream.write(bytes, 0, bytes.length);
                ouputStream.flush();
                ouputStream.close();
            }
        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar descargar el archivo", e);
        }
        return null;
    }
	
	
	public ModelAndView generarDocumento(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Integer codId = datos.getInt("codId");
        String certificado = datos.getString("certificado");
        
        byte[] pdfBytes;
        byte [] bytesEBXMLDR;
        
		try {
			
			//jalar XML segun el coid
			bytesEBXMLDR = certificadoOrigenLogic.descargarDocFDXMLById(codId);
			
			//eliminar registros de mercancias anteriores
			certificadoOrigenLogic.eliminarMercanciasXML(codId);
			
			//grabar en la BD los registros
			Acuerdo26IOPEBXML docResolutivoSecEBXML = (Acuerdo26IOPEBXML) procesoDR.procesarIOPRecepcionVUCEsUpdate(codId,bytesEBXMLDR); 
			
			
			pdfBytes = certificadoOrigenLogic.generarReporteAPBytes(codId);
    		certificadoOrigenLogic.insertArchivoCOFD(pdfBytes,codId);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Ocurrio un error al intentar generarDocumento ", e);
		}
        
		
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		Table tCertificados = ibatisService.loadGrid("certificadoOrigen.busqueda.COFD.grilla", filter);
		request.setAttribute("tCertificados", tCertificados);
		Date fecha = (Date) ibatisService.loadElement("comun.fechaActual", null).get("FECHA");
		request.setAttribute("acuerdo", ConstantesCO.AC_ALIANZA_PACIFICO);
		String fechaHoraActual = Util.getDate(fecha, FORMATO_FECHA_ES);
		request.setAttribute("fechaHoraActual", fechaHoraActual);
		request.setAttribute("fechaActual", Util.getDate(fecha, "yyyyMMdd"));
		
		return new ModelAndView(busquedaCOFD);
    }


    public void setIbatisService(IbatisService ibatisService) {
        this.ibatisService = ibatisService;
    }

    public void setUsuariosLogic(UsuariosLogic usuariosLogic) {
        this.usuariosLogic = usuariosLogic;
    }

    public void setFormatoLogic(FormatoLogic formatoLogic) {
        this.formatoLogic = formatoLogic;
    }

    public void setOrdenLogic(OrdenLogic ordenLogic) {
        this.ordenLogic = ordenLogic;
    }

    public void setAdjuntoLogic(AdjuntoLogic adjuntoLogic) {
        this.adjuntoLogic = adjuntoLogic;
    }

    public void setCertificadoOrigenLogic(
            CertificadoOrigenLogic certificadoOrigenLogic) {
        this.certificadoOrigenLogic = certificadoOrigenLogic;
    }

    public void setSuceLogic(SuceLogic suceLogic) {
		this.suceLogic = suceLogic;
	}

    public void setListadoCertificadoOrigen(String listadoCertificadoOrigen) {
        this.listadoCertificadoOrigen = listadoCertificadoOrigen;
    }

    public void setListadoTupas(String listadoTupas) {
        this.listadoTupas = listadoTupas;
    }

    public void setSeleccionarAcuerdo(String seleccionarAcuerdo) {
        this.seleccionarAcuerdo = seleccionarAcuerdo;
    }

    public String getFormato() {
		return formato;
	}

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public void setPartidas(String partidas) {
        this.partidas = partidas;
    }

	public void setDatosDocResolutivo(String datosDocResolutivo) {
		this.datosDocResolutivo = datosDocResolutivo;
	}

	public void setAdjuntos(String adjuntos) {
		this.adjuntos = adjuntos;
	}

	public void setSubsanacion(String subsanacion) {
		this.subsanacion = subsanacion;
	}

	public String getDuplicadoCertificadoOrigen() {
		return duplicadoCertificadoOrigen;
	}

	public void setDuplicadoCertificadoOrigen(String duplicadoCertificadoOrigen) {
		this.duplicadoCertificadoOrigen = duplicadoCertificadoOrigen;
	}

	public void setBusquedaCO(String busquedaCO) {
		this.busquedaCO = busquedaCO;
	}

	public void setListadoDeclaracionJurada(String listadoDeclaracionJurada) {
		this.listadoDeclaracionJurada = listadoDeclaracionJurada;
	}

	public String getAnulacionCertificadoOrigen() {
		return anulacionCertificadoOrigen;
	}

	public void setAnulacionCertificadoOrigen(String anulacionCertificadoOrigen) {
		this.anulacionCertificadoOrigen = anulacionCertificadoOrigen;
	}

	public String getCalificacionAnticipadaCertificadoOrigen() {
		return calificacionAnticipadaCertificadoOrigen;
	}

	public void setCalificacionAnticipadaCertificadoOrigen(
			String calificacionAnticipadaCertificadoOrigen) {
		this.calificacionAnticipadaCertificadoOrigen = calificacionAnticipadaCertificadoOrigen;
	}

	public String getReemplazoCertificadoOrigen() {
		return reemplazoCertificadoOrigen;
	}

	public void setReemplazoCertificadoOrigen(String reemplazoCertificadoOrigen) {
		this.reemplazoCertificadoOrigen = reemplazoCertificadoOrigen;
	}

 	public void setDocumentosResolutivos(String documentosResolutivos) {
		this.documentosResolutivos = documentosResolutivos;
	}

 	public void setDocumentosResolutivosFirma(String documentosResolutivosFirma) {
		this.documentosResolutivosFirma = documentosResolutivosFirma;
	}
 	
 	public void setDocumentosResolutivosFirmaResolutor(String documentosResolutivosFirmaResolutor) {
		this.documentosResolutivosFirmaResolutor = documentosResolutivosFirmaResolutor;
	}
 	
 	public void setDatosDocResolutivoResolutor(String datosDocResolutivoResolutor) {
		this.datosDocResolutivoResolutor = datosDocResolutivoResolutor;
	}
 	
	public ResolutorLogic getResolutorLogic() {
 		return resolutorLogic;
 	}

 	public void setResolutorLogic(ResolutorLogic resolutorLogic) {
 		this.resolutorLogic = resolutorLogic;
 	}

	public void setOtraInformacionTramite(String otraInformacionTramite) {
		this.otraInformacionTramite = otraInformacionTramite;
	}

	public void setAuditoriaLogic(AuditoriaLogic auditoriaLogic) {
		this.auditoriaLogic = auditoriaLogic;
	}

	public void setIopLogic(InteroperabilidadFirmasLogic iopLogic) {
		this.iopLogic = iopLogic;
	}

	public void setBusquedaCOFD(String busquedaCOFD) {
		this.busquedaCOFD = busquedaCOFD;
	}
	
	public void setProcesoDR(ProcesoDR procesoDR) {
		this.procesoDR = procesoDR;
	}
	
}
