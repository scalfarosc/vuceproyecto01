package pe.gob.mincetur.vuce.co.web.ajax.certificado;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class FacturaMercanciaElementLoader {

    static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);

    /**
     * Creates a new instance of DatosRUCElementLoader
     */
    public FacturaMercanciaElementLoader() {
    }

    public static HashUtil<String, String> obtenerFlgFacturaTercero(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();

        Long coId = new Long(filtros.getString("filter").split("[|]")[0].split("[=]")[1]);
        Integer secuencia = new Integer(filtros.getString("filter").split("[|]")[1].split("[=]")[1]);
        String formato = filtros.getString("filter").split("[|]")[2].split("[=]")[1];

        try {

        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("coId", coId);
        	filter.put("secuencia", secuencia);

        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	HashUtil<String, Object> datos = ibatisService.loadElement("certificadoOrigen.factura." + formato.toLowerCase() + ".tercer.pais", filter);

        	String flg = "N";
        	if (datos != null && datos.size()>0) {
        		flg = datos.getString("indicador_tercer_pais");
        	}

        	element.put("flgFacturaTercero", flg);

        } catch(Exception e) {
        	//element.put("CONSOLIDADO.mensaje", "ERROR al intentar actualizar el consolidado del Material");
        	logger.error("ERROR al intentar obtener los datos de la factura seleccionada", e);
        } finally {
        	return element;
        }
    }

}
