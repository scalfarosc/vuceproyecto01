package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class SubsanacionCellValidator implements CellValidator {

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue().toString().equals("T")) {
        	buttonCell.setImage("/co/imagenes/edit.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.subsanacion.editar"));
        } else {
        	buttonCell.setImage("/co/imagenes/ver.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.subsanacion.ver"));
        }
    }
}
