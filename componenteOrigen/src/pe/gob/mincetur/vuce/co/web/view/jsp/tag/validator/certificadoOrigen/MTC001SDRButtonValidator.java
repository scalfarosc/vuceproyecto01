package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class MTC001SDRButtonValidator {

    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
    	HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

    	//Table tModificacionesDR = (Table) request.getAttribute("tModificacionesDR");

    	String permiteCrearSolicitudRectif = (String) request.getAttribute("permiteCrearSolicitudRectif");
        String disponible = (String) (request.getAttribute("DR.disponible")!=null?request.getAttribute("DR.disponible"):"");
        String vigente = request.getAttribute("vigente")!=null?(String) request.getAttribute("vigente"):ConstantesCO.OPCION_NO;

    	//if (tModificacionesDR.size() > 0) {
    	if (!"S".equals(permiteCrearSolicitudRectif) || ConstantesCO.OPCION_NO.equals(disponible) || ConstantesCO.OPCION_NO.equals(vigente)){
    		//disabledButtons.put("nuevaSolicitudRectificacionDRButton", Constantes.DISABLING_ACTION_DISABLE);
    		disabledButtons.put("nuevaRectificacionDRButton", Constantes.DISABLING_ACTION_DISABLE);
    	}

    	buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> modificacionDR(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        String transmitido = (String) request.getAttribute("transmitido");

        if ("S".equals(transmitido)) {
        	disabledButtons.put("transmitirModifButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("actualizarModifButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("eliminarModifButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> modificacionEvalDR(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        String estadoRegistro = (String) request.getAttribute("estadoRegistro");

        if (!"T".equals(estadoRegistro)) {
        	disabledButtons.put("aprobarButton", Constantes.DISABLING_ACTION_HIDE);
        	disabledButtons.put("rechazarButton", Constantes.DISABLING_ACTION_HIDE);
        }

        int numSDRPendientes = Integer.parseInt(request.getAttribute("numSDRPendientes").toString());

        if(!"K".equals(estadoRegistro) || numSDRPendientes > 0) {
        	disabledButtons.put("nuevaRectificacionDRButton", Constantes.DISABLING_ACTION_HIDE);
        }

        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }



}
