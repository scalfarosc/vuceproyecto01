package pe.gob.mincetur.vuce.co.web.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.list.OptionList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.springframework.security.Authentication;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.context.HttpSessionContextIntegrationFilter;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.providers.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.userdetails.User;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.EntidadCertificadora;
import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Auditoria;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Usuario;
import pe.gob.mincetur.vuce.co.logic.AuditoriaLogic;
import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.UsuariosLogic;
import pe.gob.mincetur.vuce.co.remoting.ws.client.ConsultaRUCCWS;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;
import pe.gob.mincetur.vuce.co.logic.ContingenciaAuthLogic;
import pe.gob.mincetur.vuce.co.util.VerifyRecaptcha;
import pe.gob.sunat.tecnologia.menu.bean.UsuarioBean;
import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanT1144;

public class LoginController extends MultiActionController {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);
    
    private UsuariosLogic usuariosLogic;
    
    private IbatisService ibatisService;

    private FormatoLogic formatoLogic;
    
    private AuditoriaLogic auditoriaLogic;
    
	protected ContingenciaAuthLogic contingenciaAuthLogic;
    
    //private String terminos;
    
    private String registroUsuarioForm;
    
    private String mensajesPostLogueo;
    
    private String principal;
    
    private String principalVUCECentral;
    
    private String modificacionUsuarioForm;
    
    private String consultaDR;
    
	protected String contingenciaExito;

	protected String contingenciaErrorCaptura;

	protected String contingenciaErrorValidacion;
	
	protected String contingenciaIndex;	    
    
    public void setIbatisService(IbatisService ibatisService) {
        this.ibatisService = ibatisService;
    }
    
    public void setUsuariosLogic(UsuariosLogic usuariosLogic) {
        this.usuariosLogic = usuariosLogic;
    }
    
    public void setFormatoLogic(FormatoLogic formatoLogic) {
        this.formatoLogic = formatoLogic;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
    /*public void setTerminos(String terminos) {
        this.terminos = terminos;
    }*/
    
    public void setPrincipalVUCECentral(String principalVUCECentral) {
		this.principalVUCECentral = principalVUCECentral;
	}

	public void setRegistroUsuarioForm(String registroUsuarioForm) {
        this.registroUsuarioForm = registroUsuarioForm;
    }
    
    public void setModificacionUsuarioForm(String modificacionUsuarioForm) {
		this.modificacionUsuarioForm = modificacionUsuarioForm;
	}

	public void setMensajesPostLogueo(String mensajesPostLogueo) {
        this.mensajesPostLogueo = mensajesPostLogueo;
    }
    
    public void setConsultaDR(String consultaDR) {
		this.consultaDR = consultaDR;
	}

	public void setAuditoriaLogic(AuditoriaLogic auditoriaLogic) {
		this.auditoriaLogic = auditoriaLogic;
	}

    private ModelAndView inicioPrincipal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	HttpSession session = request.getSession();
    	
    	Enumeration headerNames = request.getHeaderNames();
    	String detalleConexionHeader = null;
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            System.out.println(headerName+" : "+request.getHeader(headerName));
            detalleConexionHeader = headerName + " : "+request.getHeader(headerName)+ "\n" + detalleConexionHeader;
        }    	
        
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        
        if (usuario.getMap().get("tipOrigen")==null) usuario.getMap().put("tipOrigen", "");
        if (usuario.getMap().get("tipUsuario")==null) usuario.getMap().put("tipUsuario", "");
        if (usuario.getMap().get("opeComex")==null) usuario.getMap().put("opeComex", "");
        
        boolean usuarioPrincipalRegistrado = false;
        int existe = 0;
        if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_RUC);
            usuario.setNumeroDocumento(usuario.getNumRUC());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL);
            usuario.setPrincipal(usuario.getMap().get("tipUsuario").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL));
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_SOL);
            request.getSession().setAttribute("logoutController", "http://www.vuce.gob.pe/vuce/index.jsp?l=&m=so");
            
            // Si el usuario es secundario se debe verificar que su usuario principal ya est� registrado
            if (!usuario.isPrincipal()) {
                HashUtil<String, Object> filter = new HashUtil<String, Object>();
                filter.put("tipoDocumento", usuario.getTipoDocumento());
                filter.put("numeroDocumento", usuario.getNumeroDocumento());
                usuarioPrincipalRegistrado = ibatisService.loadElement("co.usuarioPrincipalRegistrado", filter).size() > 0;
            }
            
            existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());
            
            // Si el usuario no existe en la VUCE obtenemos su Tipo de Persona desde la Consulta RUC
            if (existe==0) {
                // Consultamos a SUNAT para obtener el Tipo de Persona del usuario SOL logueado
                BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(usuario.getNumeroDocumento());
                if (datosRUC!=null) {
                    usuario.setTipoPersona(""+Integer.parseInt(datosRUC.getDdp_identi()));
                } else {
                    //throw new Exception("Error al invocar al web service de Consulta RUC");
                	Message message = new Message("co.logueo.sistemaNoDisponible");
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    request.getSession().invalidate();
                    return new ModelAndView(mensajesPostLogueo);
                }
            }
        } else if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_EXTRANET);
            usuario.setNumeroDocumento(usuario.getLogin());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT);
            usuario.setPrincipal(usuario.getMap().get("tipUsuario").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_EXT_PRINCIPAL));
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_ENTIDAD);
            request.getSession().setAttribute("logoutController", "http://www.vuce.gob.pe/vuce/index.jsp?l=&m=eo");
            
            usuarioPrincipalRegistrado = true;
            existe = usuariosLogic.existeUsuarioExtranet(usuario.getLogin());
        } else if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_DNI);
            usuario.setNumeroDocumento(usuario.getLogin());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI);
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_DNI);
            request.getSession().setAttribute("logoutController", "http://www.vuce.gob.pe/vuce/index.jsp?l=&m=do");
            
            usuarioPrincipalRegistrado = true;
            existe = usuariosLogic.existeUsuarioDNI(usuario.getLogin());
            
            usuario.setTipoPersona(ConstantesCO.TIPO_PERSONA_NATURAL);
        }
        
        // Cargamos los roles del usuario
        usuario.setRoles(new HashUtil((HashMap)usuario.getMap().get("roles")));
        
        // Si el usuario es SOL y Principal, ponerle siempre el Rol "CO.USUARIO.OPERACION"
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.isPrincipal()) {
            usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
        }
        
        if (existe!=0) {
        	usuario.setIdUsuario(existe);
        	
            //   LOG AUDITORIA - SESION   //
            Integer recursoOrigen = null;
        	if ((usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()))) {
        		recursoOrigen = ConstantesCO.AUDITORIA_RECURSO_ORIGEN_EXTRANET_MESA_AYUDA;
        	} else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) || usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
        		recursoOrigen = ConstantesCO.AUDITORIA_RECURSO_ORIGEN_USUARIO_IMPORTADOR_EXPORTADOR;
            }
        	if (recursoOrigen!=null) {
                Auditoria auditoria = new Auditoria();
                auditoria.setDetalleConexion(detalleConexionHeader);
        		auditoria.setRecursoOrigen(recursoOrigen);
        		auditoriaLogic.grabarInicioSesion(auditoria);
        	}
        }
        
        usuario.imprimir();
        
    	ModelAndView redirectPage = validarInformacionUsuarioPostLogueo(usuario, existe, usuarioPrincipalRegistrado, request);
        
    	// Asignamos el rol activo
    	asignarRolActivoUsuario(usuario, session, request);
    	
        return redirectPage;
    }
    
    private void asignarRolActivoUsuario(UsuarioCO usuario, HttpSession session, HttpServletRequest request) {
    	if (request.getSession(false)==null || !request.isRequestedSessionIdValid()) 
    		return;
    	
    	// Asignamos el rol activo
    	if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
        	OptionList listaRoles = usuario.getListaRolesSeleccionables();
        	
    		if (listaRoles.size() > 0) {
    	        usuario.setRolActivo(listaRoles.getOption(0).getCodigo());
    		}
    		
	        session.setAttribute(ConstantesCO.LISTA_ROLES_USUARIO, listaRoles);
    	}
    }
    
    private ModelAndView validarInformacionUsuarioPostLogueo(UsuarioCO usuario, int existe, boolean usuarioPrincipalRegistrado, HttpServletRequest request) {
    	ModelAndView redirectPage = null;
    	
    	// Si el usuario es EXTRANET
    	if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
        	boolean entidadCertificadoraOK = true;
        	
	    	// Si el usuario ya existe en BD, hay que buscar el estado de la entidad en BD
	    	if (existe!=0) {
	    		Usuario usuarioBD = usuariosLogic.getUsuarioById(usuario.getIdUsuario());
	    		
	    		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    		filter.put("entidadId", usuarioBD.getEntidadId());
	    		filter.put("usuarioId", usuarioBD.getUsuarioId());
	    		
	            // Obtener los datos de la Entidad 
	            //HashUtil<String, Object> datosEntidad = ibatisService.loadElement("comun.entidad.element", filter);
	    		HashUtil<String, Object> datosEntidad = ibatisService.loadElement("comun.entidad.tramite.pendiente.element", filter);
	            
	            // Si la entidad del usuario ha sido desactivada, no permitirle entrar
	    		if ("I".equals(datosEntidad.getString("ESTADO"))) {
	    			Message message = new Message("co.logueo.entidadDesactivada");
	                MessageList messageList = new MessageList();
	                messageList.add(message);
	                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	                request.getSession().invalidate();
	                redirectPage = new ModelAndView(mensajesPostLogueo);
	                
	                entidadCertificadoraOK = false;
	    		}
	    	}
	    	// Si el usuario aun no existe en BD, se debe validar contra el nombre del usuario que viene desde SUNAT
	    	else {
	    		EntidadCertificadora entidadCertificadora = usuariosLogic.obtenerEntidadCertificadora(usuario.getNombreCompleto());
	    		
	    		// Si el usuario es de alguna Entidad Certificadora (no es de MINCETUR ni administrado)
		    	if (entidadCertificadora!=null) {
		    		// Si la entidad del usuario no existe o ha sido desactivada, no permitirle entrar
			    	if (entidadCertificadora.getEstado().equals("I")) {
		    			Message message = new Message("co.logueo.entidadDesactivada");
		                MessageList messageList = new MessageList();
		                messageList.add(message);
		                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
		                request.getSession().invalidate();
		                redirectPage = new ModelAndView(mensajesPostLogueo);
		                
		                entidadCertificadoraOK = false;
			    	}
		    	}
	    	}
	    	
	    	// Si la entidad del usuario no paso la validacion, redirigir a la pantalla de mensajes post-logueo
	    	if (!entidadCertificadoraOK) {
	    		return redirectPage;
	    	}
    	}
    	
    	if (usuario.getIdUsuario()!=null && !validarRolesUsuario(usuario, request)) { //NPCS 28012020 Sunat ya no envia Roles
            request.getSession().invalidate();
            redirectPage = new ModelAndView(mensajesPostLogueo);
        } else {
        	Usuario usuarioBD = null;
        	// Si el usuario Existe entonces cargar sus datos desde la BD
        	if (usuario.getIdUsuario()!=null) {
        		usuarioBD = usuariosLogic.getUsuarioById(usuario.getIdUsuario());
        		usuario.setIngresoMR(usuarioBD.getIngresoMR());
        		usuario.setIngresoUO(usuarioBD.getIngresoUO());
            	usuario.setTipoDocumentoUS(usuarioBD.getTipoDocumentoUS());
                usuario.setNumeroDocumentoUS(usuarioBD.getNumeroDocumentoUS());
                usuario.setCargo(usuarioBD.getCargo());
                usuario.setNombreCompleto(usuarioBD.getNombre());
        	}
            if (existe!=0 && validarCambioRolUsuario(usuario)) {
                // Actualizamos los roles que vienen de SUNAT
                usuariosLogic.actualizarRolesUsuario(usuario);
                
                request.getSession().setAttribute(Constantes.USUARIO, usuario);
                cargarInformacionUsuarioEnFormularioDatos(request);
                request.setAttribute("noMostrarHeader", "S");
                redirectPage = new ModelAndView(registroUsuarioForm);
            } else {
            	// SI EL USUARIO ESTA EN BD Y HA ENTRADO ALGUNA VEZ A ORIGEN
                if (existe!=0 && "S".equals(usuarioBD.getIngresoUO())) {
                    // Validamos si es que el usuario tiene NOMBRE, CARGO, TIPO Y NRO. DE DOCUMENTO
                    if (usuarioBD.getNombre()==null || usuarioBD.getTipoDocumentoUS()==null || usuarioBD.getNumeroDocumentoUS()==null || usuarioBD.getCargo()==null) {
                        request.getSession().setAttribute(Constantes.USUARIO, usuario);
                        cargarInformacionUsuarioEnFormularioDatos(request);
                        request.setAttribute("noMostrarHeader", "S");
                    	return new ModelAndView(modificacionUsuarioForm);
                    } else {
	                    // Actualizamos los roles que vienen de SUNAT
	                    usuariosLogic.actualizarRolesUsuario(usuario);
	                    
	                    ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuario, ibatisService, request);
	                    
			    		// Actualizamos la informacion del RUC que viene de SUNAT
	                    if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.CENTRAL")) {
	                        redirectPage = new ModelAndView(principalVUCECentral);
	                    } else if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD.CONSULTA_DR") && 
	                    		   !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") && 
	                    		   !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.CENTRAL")) {
	                        redirectPage = new ModelAndView(new RedirectView("consultaDR.htm?method=inicioConsultaDR", true));
	                    } else {
		                    if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.isPrincipal()) {
		                        HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
		                        if (datosFichaRUC.size() > 0) {
		                            usuariosLogic.actualizarDatosRUCUsuario(usuario, datosFichaRUC);
		                        }
		                    }
		                    
		                    redirectPage = new ModelAndView(principal);
	                    }
                    }
                }
                // EL USUARIO ESTA EN BD PERO NUNCA ENTRO A ORIGEN
                else if (existe!=0 && !"S".equals(usuarioBD.getIngresoUO())) {
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(modificacionUsuarioForm);
                } else if (usuario.getRoles()==null || usuario.getRoles().isEmpty()) {
                	//NPCS 28012020 Sunat ya no envia Roles
                    //Message message = new Message("co.logueo.usuarioSinRoles");
                    //MessageList messageList = new MessageList();
                    //messageList.add(message);
                    //request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    //request.getSession().invalidate();
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(registroUsuarioForm); 
                /*} else if (existe==0 && usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
                    Message message = new Message("co.logueo.usuarioExtranetNoRegistrado");
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    request.getSession().invalidate();
                    redirectPage = mensajesPostLogueo;*/
                } else {
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(registroUsuarioForm);
                }
            }
        }
        
        return redirectPage;
    }
    
    private boolean validarCambioRolUsuario(UsuarioCO usuario) {
        boolean cambio = false;
        return cambio;
    }
    
    private ModelAndView validarInformacionUsuarioPostLogueoContingencia(UsuarioCO usuario, int existe, boolean usuarioPrincipalRegistrado, HttpServletRequest request) {
    	ModelAndView redirectPage = null;
    	
    	// Si el usuario es EXTRANET
    	if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
        	boolean entidadCertificadoraOK = true;
        	
	    	// Si el usuario ya existe en BD, hay que buscar el estado de la entidad en BD
	    	if (existe!=0) {
	    		Usuario usuarioBD = usuariosLogic.getUsuarioById(usuario.getIdUsuario());
	    		
	    		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    		filter.put("entidadId", usuarioBD.getEntidadId());
	    		filter.put("usuarioId", usuarioBD.getUsuarioId());
	    		
	            // Obtener los datos de la Entidad 
	            //HashUtil<String, Object> datosEntidad = ibatisService.loadElement("comun.entidad.element", filter);
	    		HashUtil<String, Object> datosEntidad = ibatisService.loadElement("comun.entidad.tramite.pendiente.element", filter);
	            
	            // Si la entidad del usuario ha sido desactivada, no permitirle entrar
	    		if ("I".equals(datosEntidad.getString("ESTADO"))) {
	    			Message message = new Message("co.logueo.entidadDesactivada");
	                MessageList messageList = new MessageList();
	                messageList.add(message);
	                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	                request.getSession().invalidate();
	                redirectPage = new ModelAndView(mensajesPostLogueo);
	                
	                entidadCertificadoraOK = false;
	    		}
	    	}
	    	// Si el usuario aun no existe en BD, se debe validar contra el nombre del usuario que viene desde SUNAT
	    	else {
	    		EntidadCertificadora entidadCertificadora = usuariosLogic.obtenerEntidadCertificadora(usuario.getNombreCompleto());
	    		
	    		// Si el usuario es de alguna Entidad Certificadora (no es de MINCETUR ni administrado)
		    	if (entidadCertificadora!=null) {
		    		// Si la entidad del usuario no existe o ha sido desactivada, no permitirle entrar
			    	if (entidadCertificadora.getEstado().equals("I")) {
		    			Message message = new Message("co.logueo.entidadDesactivada");
		                MessageList messageList = new MessageList();
		                messageList.add(message);
		                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
		                request.getSession().invalidate();
		                redirectPage = new ModelAndView(mensajesPostLogueo);
		                
		                entidadCertificadoraOK = false;
			    	}
		    	}
	    	}
	    	
	    	// Si la entidad del usuario no paso la validacion, redirigir a la pantalla de mensajes post-logueo
	    	if (!entidadCertificadoraOK) {
	    		return redirectPage;
	    	}
    	}
    	
    	if (!validarRolesUsuario(usuario, request)) {
            request.getSession().invalidate();
            redirectPage = new ModelAndView(mensajesPostLogueo);
        } else {
        	Usuario usuarioBD = null;
        	// Si el usuario Existe entonces cargar sus datos desde la BD
        	if (usuario.getIdUsuario()!=null) {
        		usuarioBD = usuariosLogic.getUsuarioById(usuario.getIdUsuario());
        		usuario.setIngresoMR(usuarioBD.getIngresoMR());
        		usuario.setIngresoUO(usuarioBD.getIngresoUO());
            	usuario.setTipoDocumentoUS(usuarioBD.getTipoDocumentoUS());
                usuario.setNumeroDocumentoUS(usuarioBD.getNumeroDocumentoUS());
                usuario.setCargo(usuarioBD.getCargo());
                usuario.setNombreCompleto(usuarioBD.getNombre());
        	}
            if (existe!=0) {
                // Actualizamos los roles que vienen de SUNAT
                usuariosLogic.actualizarRolesUsuario(usuario);
                
                request.getSession().setAttribute(Constantes.USUARIO, usuario);
                cargarInformacionUsuarioEnFormularioDatos(request);
                request.setAttribute("noMostrarHeader", "S");
                redirectPage = new ModelAndView(registroUsuarioForm);
            } else {
            	// SI EL USUARIO ESTA EN BD Y HA ENTRADO ALGUNA VEZ A ORIGEN
                if (existe!=0 && "S".equals(usuarioBD.getIngresoUO())) {
                    // Validamos si es que el usuario tiene NOMBRE, CARGO, TIPO Y NRO. DE DOCUMENTO
                    if (usuarioBD.getNombre()==null || usuarioBD.getTipoDocumentoUS()==null || usuarioBD.getNumeroDocumentoUS()==null || usuarioBD.getCargo()==null) {
                        request.getSession().setAttribute(Constantes.USUARIO, usuario);
                        cargarInformacionUsuarioEnFormularioDatos(request);
                        request.setAttribute("noMostrarHeader", "S");
                    	return new ModelAndView(modificacionUsuarioForm);
                    } else {
	                    // Actualizamos los roles que vienen de SUNAT
	                    usuariosLogic.actualizarRolesUsuario(usuario);
	                    
	                    ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuario, ibatisService, request);
	                    
			    		// Actualizamos la informacion del RUC que viene de SUNAT
	                    if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.CENTRAL")) {
	                        redirectPage = new ModelAndView(principalVUCECentral);
	                    } else if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD.CONSULTA_DR") && 
	                    		   !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") && 
	                    		   !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.CENTRAL")) {
	                        redirectPage = new ModelAndView(new RedirectView("consultaDR.htm?method=inicioConsultaDR", true));
	                    } else {
		                    if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.isPrincipal()) {
		                        HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
		                        if (datosFichaRUC.size() > 0) {
		                            usuariosLogic.actualizarDatosRUCUsuario(usuario, datosFichaRUC);
		                        }
		                    }
		                    
		                    redirectPage = new ModelAndView(principal);
	                    }
                    }
                }
                // EL USUARIO ESTA EN BD PERO NUNCA ENTRO A ORIGEN
                else if (existe!=0 && !"S".equals(usuarioBD.getIngresoUO())) {
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(modificacionUsuarioForm);
                } else if (usuario.getRoles()==null || usuario.getRoles().isEmpty()) {
                    Message message = new Message("co.logueo.usuarioSinRoles");
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    request.getSession().invalidate();
                    redirectPage = new ModelAndView(mensajesPostLogueo);
                /*} else if (existe==0 && usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
                    Message message = new Message("co.logueo.usuarioExtranetNoRegistrado");
                    MessageList messageList = new MessageList();
                    messageList.add(message);
                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
                    request.getSession().invalidate();
                    redirectPage = mensajesPostLogueo;*/
                } else {
                    request.getSession().setAttribute(Constantes.USUARIO, usuario);
                    cargarInformacionUsuarioEnFormularioDatos(request);
                    request.setAttribute("noMostrarHeader", "S");
                    redirectPage = new ModelAndView(registroUsuarioForm);
                }
            }
        }
        
        return redirectPage;
    }
    
    private boolean validarRolesUsuario(UsuarioCO usuario, HttpServletRequest request) {
        boolean ok = true;
        //HashUtil roles = usuario.getRoles();
        
        // 2017_07_16 JFR: Se agrego esta validacion para el caso en que SUNAT no este devolviendo los roles de un usuario en la autenticacion
        if (usuario.getIdUsuario()!=null && (usuario.getRoles()==null || usuario.getRoles().isEmpty())) {
    	    HashUtil roles = usuariosLogic.cargarRolesUsuario(usuario.getIdUsuario());
    	    usuario.setRoles(roles);
    	}
        
        // Si no tiene un rol que comience con CO entonces no dejarle entrar
        if (!ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO") && !ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "VUCE.SUNAT.ESPECIALISTA")) { //09.10.2017 JMC Alianza
        	ok = false;
            Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
            MessageList messageList = new MessageList();
            messageList.add(message);
            request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        } else {
	        // Validamos la correspondencia entre login de USUARIO y ROLES
	        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
	            // Rol que comienza con CO.ADMIN.* o CO.SUNAT.*, debe comenzar con EXTA
	            if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") || ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.SUNAT") 
	            		|| ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "VUCE.SUNAT")) { //09.10.2017 JMC Alianza
	                if (usuario.getNumeroDocumento()==null || !usuario.getNumeroDocumento().startsWith("EXTA")) {
	                    ok = false;
	                    Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
	                    MessageList messageList = new MessageList();
	                    messageList.add(message);
	                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	                }
	            }
	            
	            // Rol que comienza con CO.ENTIDAD.*, debe comenzar con EXT
	            if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD")) {
	                if (usuario.getNumeroDocumento()==null || !usuario.getNumeroDocumento().startsWith("EXT")) {
	                    ok = false;
	                    Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
	                    MessageList messageList = new MessageList();
	                    messageList.add(message);
	                    request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	                }
	            }
	        } else if (!usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
	            // No debe tener ningun rol que comience con CO.ENTIDAD.* o CO.ADMIN.* o CO.SUNAT
	            if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ENTIDAD") || ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.ADMIN") || ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "CO.SUNAT")
	            		|| ComponenteOrigenUtil.usuarioTieneRolSimilar(usuario, "VUCE.SUNAT")) { //09.10.2017 JMC Alianza
	                ok = false;
	                Message message = new Message("co.logueo.accesoRolNoPermitido", usuario.getNumeroDocumento(), usuario.getListaRoles());
	                MessageList messageList = new MessageList();
	                messageList.add(message);
	                request.setAttribute(Constantes.MESSAGE_LIST, messageList);
	            }
	        }
        }
        return ok;
    }
    
    /*private HashUtil<String, Object> obtenerDatosFichaRUC(UsuarioCO usuario) {
        HashUtil<String, Object> datosFichaRUC = new HashUtil<String, Object>();
        
        if (!ConsultaRUCCWS.servicioDisponible()) {
        	return datosFichaRUC;
        }
        
        // OJO: Obtener de la Consulta RUC para obtener los datos del RUC. Nos importan:
        // - Tipo Contribuyente: Persona Natural o Persona Juridica para saber si pedir "Datos de Empresa" y establecer "USUARIO.nombre" o "EMPRESA.nombre"
        // - Demas datos del RUC: Direccion, Telefono, Email, Fax
        String numeroDocumento = usuario.getNumeroDocumento();
        BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(numeroDocumento);
        String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(numeroDocumento);
        BeanDds datosSecundariosRUC = ConsultaRUCCWS.getDatosSecundarios(numeroDocumento);
        BeanT1144 datosT1144 = ConsultaRUCCWS.getDatosT1144(numeroDocumento);
        
        if (datosRUC!=null && domicilioLegal!=null && datosSecundariosRUC!=null && datosT1144!=null &&
            datosRUC.getDdp_numruc()!=null && datosRUC.getDdp_identi()!=null) {
        	String tipoPersona = Integer.valueOf(datosRUC.getDdp_identi()).toString();
        	usuario.setTipoPersona(tipoPersona);
            if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_NATURAL)) {
                datosFichaRUC.put("USUARIO.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("USUARIO.direccion", domicilioLegal.trim());
                datosFichaRUC.put("USUARIO.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("USUARIO.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("USUARIO.email", datosT1144.getCod_correo1().trim());
                
                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamento", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provincia", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distrito", datosUbigeo.get("DISTRITOID"));
                
                datosFichaRUC.put("ubigeoUsuarioEditable", "no");
                
            } else if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA)) {
                datosFichaRUC.put("EMPRESA.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("EMPRESA.direccion", domicilioLegal.trim());
                datosFichaRUC.put("EMPRESA.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("EMPRESA.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("EMPRESA.email", datosT1144.getCod_correo1().trim());
                
                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamentoE", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provinciaE", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distritoE", datosUbigeo.get("DISTRITOID"));
                
                // Obtenemos los Representantes Legales de la Empresa
                Table tablaRepresentantesLegales = usuariosLogic.obtenerTablaRepresentantesLegalesInicial(numeroDocumento);
                datosFichaRUC.put("tablaRepresentantesLegales", tablaRepresentantesLegales);
                
                datosFichaRUC.put("ubigeoUsuarioEditable", "yes");
            }
        }
        return datosFichaRUC;
    }
    */
    public void cargarInformacionUsuarioEnFormularioDatos(HttpServletRequest request) {
        //HashUtil<String, String> datos = RequestUtil.getParameter(request);
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        boolean pantallaRegistroDatosUsuario = usuario.getIdUsuario()==null;
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            request.setAttribute("USUARIO.usuariosol", usuario.getUsuarioSOL());
            request.setAttribute("ubigeoUsuarioEditable", "yes");
            
            // El usuario es Principal
            // 20130711: Se quito la validacion ya que tanto en usuario principal como secundario debe cargarse los datos de la empresa 
            //if (usuario.isPrincipal()) {
                HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
                Enumeration<String> keys = datosFichaRUC.keys();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    request.setAttribute(key, datosFichaRUC.get(key));
                }
            //}
            
            request.setAttribute("EMPRESA.numdoc", usuario.getNumRUC());
        } else {
            //request.setAttribute("USUARIO.nombre", usuario.getNombres());
            request.setAttribute("ubigeoUsuarioEditable", "yes");
        }
        
        if (!pantallaRegistroDatosUsuario) {
            HashUtil<String, Object> filterProvincia = new HashUtil<String, Object>();
            HashUtil<String, Object> filterDistrito = new HashUtil<String, Object>();
            Solicitante solicitante = formatoLogic.getUsuarioDetail(usuario);
            RequestUtil.setAttributes(request, "USUARIO.", solicitante);
            request.setAttribute("USUARIO.idUsuario", solicitante.getUsuarioId());
            if (solicitante.getUbigeo() != null) {
                // Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
                request.setAttribute("departamento", element.get("DEPARTAMENTOID"));
                request.setAttribute("provincia", element.get("PROVINCIAID"));
                request.setAttribute("USUARIO.distrito", element.get("DISTRITOID"));
                
                filterProvincia.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistrito.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistrito.put("provincia",element.get("PROVINCIAID"));
            } else {
                filterProvincia.put("departamento",0);
                filterDistrito.put("departamento",0);
                filterDistrito.put("provincia",0);
            }
            request.setAttribute("filterProvincia", filterProvincia);
            request.setAttribute("filterDistrito", filterDistrito);
        }
        request.setAttribute("USUARIO.tipodocumento", usuario.getTipoDocumento());
        request.setAttribute("USUARIO.numdoc", usuario.getNumeroDocumento());
        request.setAttribute("USUARIO.usuarioTipoId", usuario.getTipoUsuario());
        request.setAttribute("USUARIO.principal", usuario.isPrincipal() ? "S" : "N");
        request.setAttribute("USUARIO.tipoPersona", usuario.getTipoPersona());
        
        request.setAttribute("nombreDatosEmpresa", "EMPRESA");
        
        boolean mostrarDatosEmpresa = ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL.equalsIgnoreCase(usuario.getTipoOrigen()) && ConstantesCO.TIPO_PERSONA_JURIDICA.equalsIgnoreCase(usuario.getTipoPersona()) &&
                                      !usuariosLogic.existeEmpresa(usuario.getNumRUC());
        
        request.setAttribute("mostrarDatosEmpresa", mostrarDatosEmpresa);
        
        if (mostrarDatosEmpresa && !pantallaRegistroDatosUsuario) {
            HashUtil<String, Object> filterProvinciaE = new HashUtil<String, Object>();
            HashUtil<String, Object> filterDistritoE = new HashUtil<String, Object>();
            //Cargar datos del Agente de Aduana o Laboratorio
            Solicitante empresa = formatoLogic.getEmpresaDetail(usuario);
            RequestUtil.setAttributes(request, "EMPRESA.", empresa);
            if (empresa.getUbigeo() != null) {
                // Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(empresa.getUbigeo());
                request.setAttribute("departamentoE", element.get("DEPARTAMENTOID"));
                request.setAttribute("provinciaE", element.get("PROVINCIAID"));
                request.setAttribute("EMPRESA.distrito", element.get("DISTRITOID"));
                
                filterProvinciaE.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistritoE.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistritoE.put("provincia",element.get("PROVINCIAID"));
            } else {
                filterProvinciaE.put("departamento",0);
                filterDistritoE.put("departamento",0);
                filterDistritoE.put("provincia",0);
            }
            request.setAttribute("filterProvinciaE", filterProvinciaE);
            request.setAttribute("filterDistritoE", filterDistritoE);
        }
    }
    
    private void processLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        UsuarioBean userSession = (UsuarioBean)WebUtils.getSessionAttribute(request, "usuarioBean");
        UsuarioCO usuario = new UsuarioCO(userSession);
        
        if (userSession!=null) {
            Map roles = (HashMap)userSession.getMap().get("roles");
            WebUtils.setSessionAttribute(request, Constantes.USUARIO, usuario);
            
            GrantedAuthority [] grantedAuthorities = { new GrantedAuthorityImpl("ROLE_USER") };
            UserDetails userDetails = new User(usuario.getLogin(), "", true, true, true, true, grantedAuthorities);
            Authentication auth = new PreAuthenticatedAuthenticationToken(userDetails, "", grantedAuthorities);
            auth.setAuthenticated(true);
            SecurityContextHolder.getContext().setAuthentication(auth);
            WebUtils.setSessionAttribute(request, HttpSessionContextIntegrationFilter.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
        }
    }
    
    public ModelAndView s(HttpServletRequest request, HttpServletResponse response) throws Exception {
        processLogin(request, response);
        request.getSession().setAttribute("logoutController", "logins.html?t=s&logout");
        return inicioPrincipal(request, response);
    }
    
    public ModelAndView e(HttpServletRequest request, HttpServletResponse response) throws Exception {
        processLogin(request, response);
        request.getSession().setAttribute("logoutController", "logine.html?t=e&logout");
        return inicioPrincipal(request, response);
    }
    
    public ModelAndView d(HttpServletRequest request, HttpServletResponse response) throws Exception {
        processLogin(request, response);
        request.getSession().setAttribute("logoutController", "logind.html?t=d&logout");
        return inicioPrincipal(request, response);
    }
    
    public ModelAndView capturaDatosContingencia(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ModelAndView result = null;
    	logger.debug("Entrando a datos contingencia");
    	String ruc = request.getParameter("ruc");
    	String usuario = request.getParameter("usuario");
    	String tipoContingencia = request.getParameter("tipoContingencia");
		// Valor de recaptcha
		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
		
    	logger.debug("ruc " + ruc);
    	logger.debug("usuario " + usuario);
    	logger.debug("tipoContingencia " + tipoContingencia);
    	logger.debug("gRecaptchaResponse " + gRecaptchaResponse);
		
    	boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
    	Integer componente = 2; // 2 es origen
    	Integer tipoUsuario = null;
    	if ("s".equals(tipoContingencia)) {
    		tipoUsuario = 1; // Usuario SOL
    	} else if ("e".equals(tipoContingencia)) {
    		tipoUsuario = 4; // Usuario Extranet
    	}
    	logger.debug("tipoUsuario " + tipoUsuario);
    	logger.debug("verify " + verify);
    	
    	if (verify) {
        	// Mercancias restringidad, componente 1
        	// usuario sol: ruc y usuario sol 1
        	// extranet: usuario 4
        	String mensaje = contingenciaAuthLogic.solicitaContingencia(componente, tipoUsuario, ruc, usuario);
        	if (!"OK".equals(mensaje)) {
				String textoValidacionCaptura = contingenciaAuthLogic.obtenerMensajeValidacionCaptura();
				logger.debug("textoValidacionCaptura " + textoValidacionCaptura);
				request.setAttribute("textoValidacionCaptura", textoValidacionCaptura);        		
        		request.setAttribute("mensajeContingencia", mensaje);    		
        		result = new ModelAndView(contingenciaErrorCaptura);
        	} else {
				String textoCorreoEnviado = contingenciaAuthLogic.obtenerMensajeCorreoEnviado();
				logger.debug("textoCorreoEnviado " + textoCorreoEnviado);
				request.setAttribute("textoCorreoEnviado", textoCorreoEnviado);         		
				//request.setAttribute("mensajeContingencia", mensaje);
        		result = new ModelAndView(contingenciaExito);
        	}    		
    	} else {
    		request.getSession().setAttribute("mensajeCaptcha", "Por favor, confirme si no es un robot.");
    		// result = new ModelAndView(contingenciaIndex);  
    		response.sendRedirect("/co/index_contingencia.jsp?tipoContingencia="+tipoContingencia);
    	}
    	return result;
    }

    private void processLoginContingencia(HttpServletRequest request, HttpServletResponse response) throws Exception {
        UsuarioCO usuario = (UsuarioCO)WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        if (usuario!=null) {
            WebUtils.setSessionAttribute(request, Constantes.USUARIO, usuario);
            
            GrantedAuthority [] grantedAuthorities = { new GrantedAuthorityImpl("ROLE_USER") };
            UserDetails userDetails = new User(usuario.getLogin(), "", true, true, true, true, grantedAuthorities);
            Authentication auth = new PreAuthenticatedAuthenticationToken(userDetails, "", grantedAuthorities);
            auth.setAuthenticated(true);
            SecurityContextHolder.getContext().setAuthentication(auth);
            WebUtils.setSessionAttribute(request, HttpSessionContextIntegrationFilter.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
        }
    }
    
    public ModelAndView ingresoContingencia(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ModelAndView result = null;
    	logger.debug("Entrando a ingresoContingencia");
    	logger.debug("token " + request.getParameter("token"));
    	// TODO Esto tambien deberia estar en BD
    	//logger.debug("tipoContingencia " + request.getParameter("tipoContingencia"));
    	// Obtenemos el token
    	String token = request.getParameter("token");
    	// Obtenemos informacion de la persona en base al token
    	// Mercancias restringidad, componente 1
    	HashUtil<String, Object> informacion = contingenciaAuthLogic.validaToken(token, 2);
    	logger.debug("informacion " + informacion);
    	
    	// Creamos el usuario en sesion
        /*UsuarioBean userSession = new UsuarioBean();
        userSession.setNumRUC(informacion.getString("ruc"));
        userSession.setUsuarioSOL(informacion.getString("usuario"));
        UsuarioCO usuario = new UsuarioCO(userSession);*/
    	
    	String codigo = informacion.getString("codigo");
    	String mensaje = informacion.getString("mensaje");
    	String ruc = informacion.getString("ruc");
    	String user = informacion.getString("usuario");
    	logger.debug("codigo " + codigo);
    	logger.debug("mensaje " + mensaje);    	
    	logger.debug("ruc " + ruc);
    	logger.debug("user " + user);

    	// Si pasa toda validacion de token
    	if ("0".equals(codigo)) {

            /*************************************************************************/
        	UsuarioCO usuario = new UsuarioCO();
            
        	String tipoUsuario = informacion.getString("tipoUsuario");
            String tipoLogueo = (tipoUsuario.equals("1"))?"R":"E";;
        	
        	String rolUsuarioSOL = request.getParameter("rolUsuarioSOL");
        	String [] roles = request.getParameterValues("roles");
        	
        	if (tipoLogueo.equalsIgnoreCase("R")) {
    	        // SIMULANDO USUARIO SOL
        		usuario.setLogin(user);
    	        usuario.setUsuarioSOL(user);
    	    	usuario.setNumRUC(ruc);
        	} else if (tipoLogueo.equalsIgnoreCase("E")) {
    	        // SIMULANDO USUARIO EXTRANET
    	        usuario.setLogin(user);
    	        usuario.setNombreCompleto(request.getParameter("nombreCompleto"));
        	} else if (tipoLogueo.equalsIgnoreCase("D")) {
    	        // SIMULANDO USUARIO DNI
    	        usuario.setLogin(user);
        	}
        	
        	request.getSession().setAttribute("tipoLogueo", tipoLogueo);
        	request.getSession().setAttribute("tipoUsuario", tipoUsuario);
        	request.getSession().setAttribute("rolUsuarioSOL", rolUsuarioSOL);
        	request.getSession().setAttribute("roles", roles);        
                   
            /*************************************************************************/
            
            WebUtils.setSessionAttribute(request, Constantes.USUARIO, usuario);
            WebUtils.setSessionAttribute(request, "esContingencia", Boolean.TRUE);
            processLoginContingencia(request, response);
            result = procesamientoContingencia(request, response);
    	} else {
			String textoValidacionToken = mensaje;//contingenciaAuthLogic.obtenerMensajeValidacionToken();
			logger.debug("textoValidacionToken " + textoValidacionToken);
			request.setAttribute("textoValidacionToken", textoValidacionToken);      		
    		// request.setAttribute("mensajeContingencia", mensaje);
    		result = new ModelAndView(contingenciaErrorValidacion);    		
    	}
    	return result;
    }
    
    public ModelAndView procesamientoContingencia(HttpServletRequest request, HttpServletResponse response) {
    	HttpSession session = request.getSession();
        UsuarioCO usuario = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
        
        String detalleConexionHeader = null;
        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            detalleConexionHeader = headerName + " : "+request.getHeader(headerName)+ "\n" + detalleConexionHeader;
        }

        String tipoLogueo = (String)request.getSession().getAttribute("tipoLogueo");
        //String tipoUsuario = (String)request.getSession().getAttribute("tipoUsuario");
        String rolUsuarioSOL = (String)request.getSession().getAttribute("rolUsuarioSOL");
        if (rolUsuarioSOL==null) rolUsuarioSOL = "O";
        String [] roles = (String [])request.getSession().getAttribute("roles");
        
        boolean usuarioPrincipalRegistrado = false;
        if (tipoLogueo.equalsIgnoreCase("R")) {
            // SIMULANDO USUARIO SOL
            usuario.getMap().put("tipOrigen", ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL);
            //usuario.getMap().put("tipUsuario", tipoUsuario.equalsIgnoreCase("P") ? ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL : ConstantesCO.AUT_TIPO_USUARIO_SOL_SECUNDARIO);
            usuario.getMap().put("opeComex", "");

            // Consultamos a SUNAT para obtener el Tipo de Persona del usuario SOL logueado
            BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(usuario.getNumRUC());
            
            //////// OJO: La siguiente linea se comento porque como se ingresa por un simulador, no se puede tener a ciencia cierta un numero RUC correcto
            // Setear el tipo de persona aqui solo sirve si el usuario no existe en BD
            if (datosRUC!=null && datosRUC.getDdp_identi()!=null) {
                usuario.setTipoPersona(""+Integer.parseInt(datosRUC.getDdp_identi()));
            } else {
                usuario.setTipoPersona(ConstantesCO.TIPO_PERSONA_JURIDICA);
            }
        } else if (tipoLogueo.equalsIgnoreCase("E")) {
            // SIMULANDO USUARIO EXTRANET
            usuario.getMap().put("tipOrigen", ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT);
            //usuario.getMap().put("tipUsuario", tipoUsuario.equalsIgnoreCase("P") ? ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL : ConstantesCO.AUT_TIPO_USUARIO_SOL_SECUNDARIO);
            usuario.getMap().put("opeComex", "");
        } else if (tipoLogueo.equalsIgnoreCase("D")) {
            // SIMULANDO USUARIO DNI
            usuario.getMap().put("tipOrigen", ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI);
            usuario.getMap().put("opeComex", "");

            // Setear el tipo de persona aqui solo sirve si el usuario no existe en BD
            usuario.setTipoPersona(ConstantesCO.TIPO_PERSONA_NATURAL);
        }
        
        int existe = 0;
        if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_RUC);
            usuario.setNumeroDocumento(usuario.getNumRUC());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL);
            //usuario.setPrincipal(usuario.getMap().get("tipUsuario").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_SOL_PRINCIPAL));

            // Asignarle el rol SUPERVISOR si corresponde
            if (rolUsuarioSOL.equalsIgnoreCase("A") || rolUsuarioSOL.equalsIgnoreCase("L")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("O")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("S")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("F")) {
                usuario.getRoles().put(Rol.CO_USUARIO_FIRMA.getNombre(), Rol.CO_USUARIO_FIRMA.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("T")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
                usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
                usuario.getRoles().put(Rol.CO_USUARIO_FIRMA.getNombre(), Rol.CO_USUARIO_FIRMA.getNombre());
            }
            
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_SOL);
            
            if (!usuario.isPrincipal()) {
                HashUtil<String, Object> filter = new HashUtil<String, Object>();
                filter.put("tipoDocumento", usuario.getTipoDocumento());
                filter.put("numeroDocumento", usuario.getNumeroDocumento());
                usuarioPrincipalRegistrado = ibatisService.loadGrid("co.usuarioPrincipalRegistrado", filter).size() > 0;
                /*if (usuarioPrincipalRegistrado) {
                    existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());
                }*/
            /*} else {
                existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());*/
            }
            existe = usuariosLogic.existeUsuarioSOL(usuario.getUsuarioSOL(), usuario.getNumRUC());

        } else if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_EXTRANET);
            usuario.setNumeroDocumento(usuario.getLogin());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT);
            //usuario.setPrincipal(usuario.getMap().get("tipUsuario").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_EXT_PRINCIPAL));
            usuario.setNroRegistro(usuario.getLogin().substring(3,4));
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_ENTIDAD);

            // Asigno los roles seleccionados
            if (roles!=null) {
                for (int i=0; i < roles.length; i++) {
                    usuario.getRoles().put(roles[i], roles[i]);
                }
            }

            existe = usuariosLogic.existeUsuarioExtranet(usuario.getLogin());
        } else if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            //usuario = UsuarioFactory.getUsuarioDNI(usuario.getLogin());

            usuario.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_DNI);
            usuario.setNumeroDocumento(usuario.getLogin());
            usuario.setTipoOrigen(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI);
            usuario.setTipoUsuario(ConstantesCO.USUARIO_TIPO_DNI);
            existe = usuariosLogic.existeUsuarioDNI(usuario.getLogin());

            usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
        }
        
        if (existe!=0) {
            usuario.setIdUsuario(existe);

            //   LOG AUDITORIA - SESION   //
            Integer recursoOrigen = null;
            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) || usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
                recursoOrigen = ConstantesCO.AUDITORIA_RECURSO_ORIGEN_USUARIO_IMPORTADOR_EXPORTADOR;
            }
            if (recursoOrigen!=null) {
                Auditoria auditoria = new Auditoria();
                auditoria.setDetalleConexion(detalleConexionHeader);
                auditoria.setRecursoOrigen(recursoOrigen);
                auditoria.setTipoOrigen(2); // 2: contingencia
                auditoriaLogic.grabarInicioSesionContingencia(auditoria);
            }
        }

        usuario.imprimir();

    	
    	ModelAndView redirectPage = validarInformacionUsuarioPostLogueo(usuario, existe, usuarioPrincipalRegistrado, request);
        
        // Si el usuario es SOL y Principal, ponerle siempre el Rol "CO.USUARIO.OPERACION"
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.isPrincipal()) {
            usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            usuario.getRoles().put(Rol.CO_USUARIO_SUPERVISOR.getNombre(), Rol.CO_USUARIO_SUPERVISOR.getNombre());
        }

    	// Asignamos el rol activo
    	asignarRolActivoUsuario(usuario, session);
        
        // OJO: ESTO ES PARA PRUEBAS
        /*if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            // Asignarle el rol SUPERVISOR si corresponde
            if (rolUsuarioSOL.equalsIgnoreCase("A") || rolUsuarioSOL.equalsIgnoreCase("L")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            } else if (rolUsuarioSOL.equalsIgnoreCase("O")) {
                // Si el usuario es SOL, asignarle por defecto el Rol "CO.OPERACION" OJO: ESTO ES SOLO PARA PRUEBAS
                usuario.getRoles().put(Rol.CO_USUARIO_OPERACION.getNombre(), Rol.CO_USUARIO_OPERACION.getNombre());
            }
        }*/
        
        request.getSession().setAttribute("logoutController", "logout.htm?method=logoutTest");
        
        return redirectPage;
    } 
    
    private void asignarRolActivoUsuario(UsuarioCO usuario, HttpSession session) {
    	// Asignamos el rol activo
    	if (usuario.getMap().get("tipOrigen").toString().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
        	OptionList listaRoles = usuario.getListaRolesSeleccionables();
        	
    		if (listaRoles.size() > 0) {
    	        usuario.setRolActivo(listaRoles.getOption(0).getCodigo());
    		}
    		
    	    session.setAttribute(ConstantesCO.LISTA_ROLES_USUARIO, listaRoles);
    	}
    }    
	
    public ModelAndView logoutContingencia(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Performing logout contingencia....");
        HttpSession session = request.getSession();
        UsuarioCO usuario = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
        
        String detalleConexionHeader = null;
        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            detalleConexionHeader = headerName + " : "+request.getHeader(headerName)+ "\n" + detalleConexionHeader;
        }
        
        //   LOG AUDITORIA - SESION   //
        Integer recursoOrigen = null;
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) || usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            recursoOrigen = ConstantesCO.AUDITORIA_RECURSO_ORIGEN_USUARIO_IMPORTADOR_EXPORTADOR;
        }
        if (recursoOrigen!=null) {
            Auditoria auditoria = new Auditoria();
            auditoria.setDetalleConexion(detalleConexionHeader);
            auditoria.setRecursoOrigen(recursoOrigen);
            auditoria.setTipoOrigen(2); // 2: contingencia
            auditoriaLogic.grabarFinSesionContingencia(auditoria);
        }
        
        request.getSession().invalidate();
        try {
			response.sendRedirect("/vuce/index.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}
        return null;
     }	
    
    /** Este metodo se invoca en el esquema de autenticacion alternativo, una vez se haya validado al usuario **/
    public ModelAndView principal(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	return inicioPrincipal(request, response);
    }

	public void setContingenciaAuthLogic(ContingenciaAuthLogic contingenciaAuthLogic) {
		this.contingenciaAuthLogic = contingenciaAuthLogic;
	}

	public void setContingenciaExito(String contingenciaExito) {
		this.contingenciaExito = contingenciaExito;
	}

	public void setContingenciaErrorCaptura(String contingenciaErrorCaptura) {
		this.contingenciaErrorCaptura = contingenciaErrorCaptura;
	}

	public void setContingenciaErrorValidacion(String contingenciaErrorValidacion) {
		this.contingenciaErrorValidacion = contingenciaErrorValidacion;
	}

	public void setContingenciaIndex(String contingenciaIndex) {
		this.contingenciaIndex = contingenciaIndex;
	}
    
}
