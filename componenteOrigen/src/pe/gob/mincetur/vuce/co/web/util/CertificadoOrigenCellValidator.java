package pe.gob.mincetur.vuce.co.web.util;

import java.util.Hashtable;

import org.jlis.core.config.LabelConfig;
import org.jlis.core.util.HashUtil;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class CertificadoOrigenCellValidator implements CellValidator {

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && !cell.getValue().toString().equals("")) {
        	buttonCell.setOnClick("verCertificado");
        } else {
        	buttonCell.setOnClick(null);
        }

    }
}
