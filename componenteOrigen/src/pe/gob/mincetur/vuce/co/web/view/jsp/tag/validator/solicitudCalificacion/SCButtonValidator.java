package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.solicitudCalificacion;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;


public class SCButtonValidator  extends FormatoButtonValidator {
    
    
    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        
        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String puedeTransmitir = (String)request.getAttribute("puedeTransmitir");
        
        if (bloqueado==null) bloqueado = "";
        if (puedeTransmitir==null) puedeTransmitir = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("transButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("representanteButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("updateAdicionalButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("updateButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("buscarPartidasButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("agregarPaisAcuerdoButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("eliminarPaisAcuerdoButton", Constantes.DISABLING_ACTION_DISABLE);
            }else {
            	if(puedeTransmitir.equalsIgnoreCase("N")) disabledButtons.put("transButton", Constantes.DISABLING_ACTION_DISABLE);
            }
            
        } 
        
        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
    
    public HashUtil<String, HashUtil<String, String>> material(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        
        Object secuencia = request.getAttribute("MATERIAL.secuenciaCalifXDJ");
        String bloqueado = (String)request.getAttribute("bloqueado");
        
        if (bloqueado==null) bloqueado = "";
        
        if (secuencia == null) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_HIDE);
                
            }
        }
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
    
    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> adjuntosDocumentoResolutivo(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = (HashUtil<String, String>)buttonSet.get(Constantes.BUTTON_SET_DISABLE);
        if (disabledButtons==null) disabledButtons = new HashUtil<String, String>();
        
        String bloqueado = (String)request.getAttribute("bloqueado");
        
        if (bloqueado.equals("S")) {
            disabledButtons.put("cargarAdjuntoButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarAdjuntoButton", Constantes.DISABLING_ACTION_HIDE);
        }
        
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
   
    
}
