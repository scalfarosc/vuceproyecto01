package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class BusquedaCOFDCellValidator implements CellValidator {

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && cell.getValue().toString().equals("S")) {
        	buttonCell.setImage("/co/imagenes/pdf.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.cofd.valido"));
            buttonCell.setOnClick("descargarDocumento");
        } else {
        	buttonCell.setImage("/co/imagenes/tip2.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.cofd.novalido"));
        }
    }
}
