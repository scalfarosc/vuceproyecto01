package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificado;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;


public class FCO003ButtonValidator  extends FormatoButtonValidator {
    
    
    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        
        Object orden = request.getAttribute("ordenId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        Object paisImportadorId = request.getAttribute("CERTIFICADO.paisImportadorId");
        
        if (bloqueado==null) bloqueado = "";

        if (orden != null) {
            if (bloqueado.equalsIgnoreCase("S")) {
            	disabledButtons.put("updateButton", Constantes.DISABLING_ACTION_DISABLE);
            	disabledButtons.put("nuevoDetalleButton", Constantes.DISABLING_ACTION_DISABLE);
            }
            if (paisImportadorId==null || "".equals(paisImportadorId)) {
            	disabledButtons.put("nuevoDetalleButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        } 
        
        // Ahora se realizan validaciones adicionales, particulares de cada Entidad
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
    
    public HashUtil<String, HashUtil<String, String>> detalleCertificado(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        
        Object secuencia = request.getAttribute("FCO003DETALLE.secuencia");
        String bloqueado = (String)request.getAttribute("bloqueado");
        
        if (bloqueado==null) bloqueado = "";
        
        if (secuencia == null) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("buscarMercanciaButton", Constantes.DISABLING_ACTION_HIDE);
            }
        }
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
    
    public HashUtil<String, HashUtil<String, String>> detalleDRCertificado(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        
        Object secuencia = request.getAttribute("FCO003DETALLE.secuencia");
        String bloqueado = (String)request.getAttribute("bloqueado");
        
        if (bloqueado==null) bloqueado = "";
        
        if (secuencia == null) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
                disabledButtons.put("buscarMercanciaButton", Constantes.DISABLING_ACTION_HIDE);
            }
        }
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
    
    public HashUtil<String, HashUtil<String, String>> adjuntosDocumentoResolutivo(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = (HashUtil<String, String>)buttonSet.get(Constantes.BUTTON_SET_DISABLE);
        if (disabledButtons==null) disabledButtons = new HashUtil<String, String>();
        
        String bloqueado = (String)request.getAttribute("bloqueado");
        
        if (bloqueado.equals("S")) {
            disabledButtons.put("cargarAdjuntoButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarAdjuntoButton", Constantes.DISABLING_ACTION_HIDE);
        }
        
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
    
}
