package pe.gob.mincetur.vuce.co.web.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.jlis.web.util.TableUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Usuario;
import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.UsuariosLogic;
import pe.gob.mincetur.vuce.co.remoting.ws.client.ConsultaRUCCWS;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanT1144;

public class UsuariosController extends MultiActionController {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);
    
    private IbatisService ibatisService;
    
    private UsuariosLogic usuariosLogic;
    
    private FormatoLogic formatoLogic;
    
    private String terminos;
    
    //private String registroUsuarioForm;
    
    private String registroRepresentanteLegalForm;
    
    private String modificacionUsuarioForm;
    
    private String menuPrincipal;
    
    private String menuPrincipalVUCECentral;
    
    public void setMenuPrincipal(String menuPrincipal) {
        this.menuPrincipal = menuPrincipal;
    }

    public void setMenuPrincipalVUCECentral(String menuPrincipalVUCECentral) {
		this.menuPrincipalVUCECentral = menuPrincipalVUCECentral;
	}

	public void setRegistroRepresentanteLegalForm(
            String registroRepresentanteLegalForm) {
        this.registroRepresentanteLegalForm = registroRepresentanteLegalForm;
    }

    /*public void setRegistroUsuarioForm(String registroUsuarioForm) {
        this.registroUsuarioForm = registroUsuarioForm;
    }
    */
    public void setModificacionUsuarioForm(String modificacionUsuarioForm) {
		this.modificacionUsuarioForm = modificacionUsuarioForm;
	}

	public void setIbatisService(IbatisService ibatisService) {
        this.ibatisService = ibatisService;
    }

    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }
    
    public void setUsuariosLogic(UsuariosLogic usuariosLogic) {
        this.usuariosLogic = usuariosLogic;
    }

    public void setFormatoLogic(FormatoLogic formatoLogic) {
		this.formatoLogic = formatoLogic;
	}

	public ModelAndView terminosCondiciones(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        String nombreDatosEmpresa = datos.getString("nombreDatosEmpresa");
        Map<String, Object> datosUsuario = WebUtils.getParametersStartingWith(request, "USUARIO.");
        Map<String, Object> datosEmpresa = WebUtils.getParametersStartingWith(request, nombreDatosEmpresa+".");
        request.getSession().setAttribute("DATOS_NUEVO", datos);
        request.getSession().setAttribute("DATOS_USUARIO_NUEVO", datosUsuario);
        request.getSession().setAttribute("DATOS_EMPRESA_NUEVO", datosEmpresa);
        return new ModelAndView(terminos);
    }
    
    /*private HashUtil<String, Object> obtenerDatosFichaRUC(UsuarioCO usuario) {
    	HashUtil<String, Object> datosFichaRUC = new HashUtil<String, Object>();
        
        if (!ConsultaRUCCWS.servicioDisponible()) {
        	return datosFichaRUC;
        }
        
        // OJO: Obtener de la Consulta RUC para obtener los datos del RUC. Nos importan:
        // - Tipo Contribuyente: Persona Natural o Persona Juridica para saber si pedir "Datos de Empresa" y establecer "USUARIO.nombre" o "EMPRESA.nombre"
        // - Demas datos del RUC: Direccion, Telefono, Email, Fax
    	String numeroDocumento = usuario.getNumeroDocumento();
        BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(numeroDocumento);
        String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(numeroDocumento);
        BeanDds datosSecundariosRUC = ConsultaRUCCWS.getDatosSecundarios(numeroDocumento);
        BeanT1144 datosT1144 = ConsultaRUCCWS.getDatosT1144(numeroDocumento);
        
        if (datosRUC!=null && domicilioLegal!=null && datosSecundariosRUC!=null && datosT1144!=null &&
        	datosRUC.getDdp_numruc()!=null && datosRUC.getDdp_identi()!=null) {
        	String tipoPersona = Integer.valueOf(datosRUC.getDdp_identi()).toString();
        	usuario.setTipoPersona(tipoPersona);
            if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_NATURAL)) {
                datosFichaRUC.put("USUARIO.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("USUARIO.direccion", domicilioLegal.trim());
                datosFichaRUC.put("USUARIO.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("USUARIO.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("USUARIO.email", datosT1144.getCod_correo1().trim());
                
                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamento", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provincia", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distrito", datosUbigeo.get("DISTRITOID"));
                
                datosFichaRUC.put("ubigeoUsuarioEditable", "no");
                
            } else if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA)) {
                datosFichaRUC.put("EMPRESA.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("EMPRESA.direccion", domicilioLegal.trim());
                datosFichaRUC.put("EMPRESA.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("EMPRESA.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("EMPRESA.email", datosT1144.getCod_correo1().trim());
                
                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamentoE", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provinciaE", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distritoE", datosUbigeo.get("DISTRITOID"));
                
                // Obtenemos los Representantes Legales de la Empresa
                Table tablaRepresentantesLegales = usuariosLogic.obtenerTablaRepresentantesLegalesInicial(numeroDocumento);
                datosFichaRUC.put("tablaRepresentantesLegales", tablaRepresentantesLegales);
                
                datosFichaRUC.put("ubigeoUsuarioEditable", "yes");
            }
        }
        return datosFichaRUC;
    }
    */
    public void cargarInformacionUsuarioEnFormularioDatos(HttpServletRequest request) {
        //HashUtil<String, String> datos = RequestUtil.getParameter(request);
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        boolean pantallaRegistroDatosUsuario = usuario.getIdUsuario()==null;
        
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
            request.setAttribute("USUARIO.usuariosol", usuario.getUsuarioSOL());
            request.setAttribute("ubigeoUsuarioEditable", "yes");
            
            // El usuario es Principal o no es Agente de Aduana ni Laboratorio
            // 20130711: Se quito la validacion ya que tanto en usuario principal como secundario debe cargarse los datos de la empresa 
        	//if (usuario.isPrincipal()) {
        		HashUtil<String, Object> datosFichaRUC = usuariosLogic.obtenerDatosFichaRUC(usuario);
                Enumeration<String> keys = datosFichaRUC.keys();
                while (keys.hasMoreElements()) {
                	String key = keys.nextElement();
                	request.setAttribute(key, datosFichaRUC.get(key));
                }
        	//}
        	
            request.setAttribute("EMPRESA.numdoc", usuario.getNumRUC());
        } else {
            request.setAttribute("ubigeoUsuarioEditable", "yes");
        }
        
        if (!pantallaRegistroDatosUsuario) {
	    	HashUtil<String, Object> filterProvincia = new HashUtil<String, Object>();
	    	HashUtil<String, Object> filterDistrito = new HashUtil<String, Object>();
	        
	    	Solicitante solicitante = formatoLogic.getUsuarioDetail(usuario);
	    	
	    	RequestUtil.setAttributes(request, "USUARIO.", solicitante);
	    	
	    	request.setAttribute("USUARIO.idUsuario", solicitante.getUsuarioId());
	    	request.setAttribute("USUARIO.idEntidad", usuario.getIdEntidad());
	        if (solicitante.getUbigeo() != null) {
	            // Nombres del departamento, provincia y distrito
	            HashMap<String, String> element = formatoLogic.getUbigeoDetail(solicitante.getUbigeo());
	            request.setAttribute("departamento", element.get("DEPARTAMENTOID"));
	            request.setAttribute("provincia", element.get("PROVINCIAID"));
	            request.setAttribute("USUARIO.distrito", element.get("DISTRITOID"));
	            
		        filterProvincia.put("departamento",element.get("DEPARTAMENTOID"));
		        filterDistrito.put("departamento",element.get("DEPARTAMENTOID"));
		        filterDistrito.put("provincia",element.get("PROVINCIAID"));
	        } else {
	        	filterProvincia.put("departamento",0);
		        filterDistrito.put("departamento",0);
		        filterDistrito.put("provincia",0);
	        }
	        request.setAttribute("filterProvincia", filterProvincia);
	        request.setAttribute("filterDistrito", filterDistrito);
        }
        request.setAttribute("USUARIO.tipodocumento", usuario.getTipoDocumento());
        request.setAttribute("USUARIO.numdoc", usuario.getNumeroDocumento());
        request.setAttribute("USUARIO.usuarioTipoId", usuario.getTipoUsuario());
        request.setAttribute("USUARIO.principal", usuario.isPrincipal() ? "S" : "N");
        request.setAttribute("USUARIO.tipoPersona", usuario.getTipoPersona());
        
        UsuarioCO usu = usuariosLogic.getUsuarioCOById(usuario.getIdUsuario());
        request.setAttribute("USUARIO.area", usu.getArea());
        request.setAttribute("USUARIO.subArea", usu.getSubArea());
        request.setAttribute("USUARIO.tipodocumentoUS", usu.getTipoDocumentoUS());
        request.setAttribute("USUARIO.numdocUS", usu.getNumeroDocumentoUS());
        request.setAttribute("USUARIO.numdocUS", usu.getNumeroDocumentoUS());
        
        request.setAttribute("nombreDatosEmpresa", "EMPRESA");
        
        boolean mostrarDatosEmpresa = usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.getTipoPersona().equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA); 
        
        request.setAttribute("mostrarDatosEmpresa", mostrarDatosEmpresa);
        
        if (mostrarDatosEmpresa && !pantallaRegistroDatosUsuario) {
	    	HashUtil<String, Object> filterProvinciaE = new HashUtil<String, Object>();
	    	HashUtil<String, Object> filterDistritoE = new HashUtil<String, Object>();
	        //Cargar datos del Agente de Aduana o Laboratorio
	    	Solicitante empresa = formatoLogic.getEmpresaDetail(usuario);
            RequestUtil.setAttributes(request, "EMPRESA.", empresa);
            request.setAttribute("EMPRESA.idUsuario", empresa.getUsuarioId());
            if (empresa.getUbigeo() != null) {
                // Nombres del departamento, provincia y distrito
                HashMap<String, String> element = formatoLogic.getUbigeoDetail(empresa.getUbigeo());
                request.setAttribute("departamentoE", element.get("DEPARTAMENTOID"));
                request.setAttribute("provinciaE", element.get("PROVINCIAID"));
                request.setAttribute("EMPRESA.distrito", element.get("DISTRITOID"));
                
                filterProvinciaE.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistritoE.put("departamento",element.get("DEPARTAMENTOID"));
                filterDistritoE.put("provincia",element.get("PROVINCIAID"));
            } else {
            	filterProvinciaE.put("departamento",0);
            	filterDistritoE.put("departamento",0);
            	filterDistritoE.put("provincia",0);
            }
            request.setAttribute("filterProvinciaE", filterProvinciaE);
            request.setAttribute("filterDistritoE", filterDistritoE);
        }
    }
    
    public ModelAndView mostrarFormularioActualizarDatosUsuario(HttpServletRequest request, HttpServletResponse response) {
    	cargarInformacionUsuarioEnFormularioDatos(request);
    	return new ModelAndView(modificacionUsuarioForm);
    }
    
    /*public ModelAndView mostrarFormularioRegistroUsuario(HttpServletRequest request, HttpServletResponse response) {
    	cargarInformacionUsuarioEnFormularioDatos(request);
        return new ModelAndView(registroUsuarioForm);
    }
    */
    public ModelAndView mostrarNuevoRepresentanteLegalForm(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView(registroRepresentanteLegalForm);
    }
    
    public ModelAndView grabarUsuarioEmpresa(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuarioCO = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        
        HashUtil<String, String> datos = (HashUtil<String, String>)request.getSession().getAttribute("DATOS_NUEVO");//RequestUtil.getParameter(request);
        Map<String, Object> datosUsuario = (Map<String, Object>)request.getSession().getAttribute("DATOS_USUARIO_NUEVO");//WebUtils.getParametersStartingWith(request, "USUARIO.");
        Map<String, Object> datosEmpresa = (Map<String, Object>)request.getSession().getAttribute("DATOS_EMPRESA_NUEVO");//WebUtils.getParametersStartingWith(request, nombreDatosEmpresa+".");
        //Map<String, Object> datosRepLegal = WebUtils.getParametersStartingWith(request, "REPLEGAL.");
        Table representantesLegales = (Table)TableUtil.obtainTablesForGridInsertUpdate(datos).get("REPRESENTANTE_LEGAL");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        
        //insertar usuario y/o empresa
        Usuario usuario = usuariosLogic.convertMapToUsuario(datosUsuario);
        Usuario empresa = null;
        if (!datosEmpresa.isEmpty()) {
            empresa = usuariosLogic.convertMapToUsuario(datosEmpresa);
        }
        
        MessageList messageList = usuariosLogic.crearUsuario(usuario, empresa, representantesLegales, usuarioCO);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuarioCO, ibatisService, request);
        
        if (usuarioCO.getRoles()==null || usuarioCO.getRoles().isEmpty()) {//NPCS 28012020
        	request.setAttribute("noMostrarHeader", "S");
    	}
        
        if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuarioCO, "CO.CENTRAL")) {
        	return new ModelAndView(menuPrincipalVUCECentral);
        }

        return new ModelAndView(menuPrincipal);
    }
    
    public ModelAndView actualizarUsuarioEmpresa(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuarioCO = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        String nombreDatosEmpresa = request.getParameter("nombreDatosEmpresa");
        
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Map<String, Object> datosUsuario = WebUtils.getParametersStartingWith(request, "USUARIO.");
        Map<String, Object> datosEmpresa = WebUtils.getParametersStartingWith(request, nombreDatosEmpresa+".");
        //Map<String, Object> datosRepLegal = WebUtils.getParametersStartingWith(request, "REPLEGAL.");
        Table representantesLegales = (Table)TableUtil.obtainTablesForGridInsertUpdate(datos).get("REPRESENTANTE_LEGAL");
        HashUtil<String, String> elements = RequestUtil.getParameter(request);
        
        // Actualizar usuario y/o empresa
        Usuario usuario = usuariosLogic.convertMapToUsuario(datosUsuario);
        Usuario empresa = null;
        if (!datosEmpresa.isEmpty()) {
            empresa = usuariosLogic.convertMapToUsuario(datosEmpresa);
        }
        
        MessageList messageList = usuariosLogic.actualizarUsuario(usuario, empresa, representantesLegales, usuarioCO);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuarioCO, ibatisService, request);
        
        if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuarioCO, "CO.CENTRAL")) {
        	return new ModelAndView(menuPrincipalVUCECentral);
        }
        
        return new ModelAndView(menuPrincipal);
    }
    
    public ModelAndView actualizarRepresentantesLegales(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuarioCO = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        String nombreDatosEmpresa = request.getParameter("nombreDatosEmpresa");
        
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        Map<String, Object> datosEmpresa = WebUtils.getParametersStartingWith(request, nombreDatosEmpresa+".");
        Table representantesLegales = (Table)TableUtil.obtainTablesForGridInsertUpdate(datos).get("REPRESENTANTE_LEGAL");
        
        // Actualizar usuario y/o empresa
        Usuario empresa = null;
        if (!datosEmpresa.isEmpty()) {
            empresa = usuariosLogic.convertMapToUsuario(datosEmpresa);
        }
        
        MessageList messageList = usuariosLogic.actualizarRepresentantesLegalesEmpresa(empresa, representantesLegales);
        request.setAttribute(Constantes.MESSAGE_LIST, messageList);
        
        /*VuceUtil.cargarInformacionPantallaPrincipal(usuarioVUCE, ibatisService, request);
        
        if (VuceUtil.usuarioTieneRolSimilar(usuarioVUCE, "VUCE.CENTRAL")) {
        	return new ModelAndView(menuPrincipalVUCECentral);
        }
        
        return new ModelAndView(menuPrincipal);*/
        
        cargarInformacionUsuarioEnFormularioDatos(request);
        
    	return new ModelAndView(modificacionUsuarioForm);
    }
    
    public ModelAndView seleccionRolActivo(HttpServletRequest request, HttpServletResponse response) {
    	HttpSession session = request.getSession();
        UsuarioCO usuarioCO = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
    	String rolActivo = request.getParameter("rolSeleccionado");
    	if (rolActivo!=null && !"".equals(rolActivo)) {
	        usuarioCO.setRolActivo(rolActivo);
	        session.setAttribute(Constantes.USUARIO, usuarioCO);
    	}
    	
        ComponenteOrigenUtil.cargarInformacionPantallaPrincipal(usuarioCO, ibatisService, request);
        
        if (ComponenteOrigenUtil.usuarioTieneRolSimilar(usuarioCO, "CO.CENTRAL")) {
        	return new ModelAndView(menuPrincipalVUCECentral);
        }
        
        return new ModelAndView(menuPrincipal);
    }
    
}
