package pe.gob.mincetur.vuce.co.web.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.jlis.web.util.TableUtil;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.Mensaje;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.logic.BuzonLogic;
import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.SuceLogic;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;

/**
 * @author JCVB
 *
 */
public class BuzonController extends MultiActionController {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);
    private String mensajes;
    private String detalleMensaje;
    private IbatisService ibatisService;
    private BuzonLogic buzonLogic;
    private FormatoLogic formatoLogic;
    private SuceLogic suceLogic;

    public void setBuzonLogic(BuzonLogic buzonLogic) {
        this.buzonLogic = buzonLogic;
    }

    public void setFormatoLogic(FormatoLogic formatoLogic) {
        this.formatoLogic = formatoLogic;
    }

    public void setDetalleMensaje(String detalleMensaje) {
        this.detalleMensaje = detalleMensaje;
    }

    public void setMensajes(String mensajes) {
        this.mensajes = mensajes;
    }

    public void setIbatisService(IbatisService ibatisService) {
        this.ibatisService = ibatisService;
    }

    public ModelAndView listarMensajes(HttpServletRequest request, HttpServletResponse response) {
        cargarMensajes(request);
        return new ModelAndView(mensajes);
    }

    public void cargarMensajes(HttpServletRequest request){
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        logger.debug("Entrando al listar Mensajes del Buzon por Usuario");

        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> filterCombo = new HashUtil<String, Object>();

        //System.out.println("UsuarioId:" + datos.getString("usuarioId"));
        String usuarioId = datos.getString("usuarioId");

        String key = "buzon.listado_mensajes.grilla";

        // Usuario CLAVE SOL
        if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) ||
                (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
                 	   (usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre()) || 
                 		usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
                        usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre())))) {
            
        	// Si el usuario tiene el Rol SUPERVISOR y no ha seleccionado un Usuario en el combo de la pantalla del buzon
            if (usuario.getRoles().containsKey(Rol.CO_USUARIO_SUPERVISOR.getNombre()) && (usuarioId == null || usuarioId.equals(""))) {
                filter.put("tipoDocumento", usuario.getTipoDocumento());
                filter.put("numeroDocumento", usuario.getNumeroDocumento());
            }
            // Si el usuario es de EXTRANET y tiene el rol HELP DESK
            else if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
            		(usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())|| 
                     usuario.getRoles().containsKey(Rol.CO_CENTRAL_OPERADOR_FUNCIONAL.getNombre()) || 
                     usuario.getRoles().containsKey(Rol.CO_CENTRAL_SUPERVISOR_TECNICO.getNombre()))) {
                // VE TODOS LOS REGISTROS
            } else {
            	if (usuarioId.equals("")) usuarioId = String.valueOf(usuario.getIdUsuario());
                filter.put("usuarioId", usuarioId);
                request.setAttribute("combousuario", usuarioId);
            }
        	
            /*// Si el usuario es de EXTRANET y tiene el rol HELP DESK
            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
                usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
                // VE TODOS LOS REGISTROS
            } else {
                if (usuarioId.equals("")) usuarioId = String.valueOf(usuario.getIdUsuario());
                filter.put("usuarioId", usuarioId);
                request.setAttribute("combousuario", usuarioId);
            }*/

            filterCombo.put("tipoDocumento", usuario.getTipoDocumento());
            filterCombo.put("numeroDocumento", usuario.getNumeroDocumento());
            request.setAttribute("filtroComboUsuario", filterCombo);
        } else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            key = "buzon.listado_mensajes.extranet.grilla";
            
            if (Rol.CO_ENTIDAD_SUPERVISOR.getNombre().equals(usuario.getRolActivo())) {
                if (!usuarioId.equals("")) {
                    filter.put("usuarioId", usuarioId);
                    request.setAttribute("combousuario", usuarioId);
                }
                filter.put("idEntidad", usuario.getIdEntidad());
                filterCombo.put("idEntidad", usuario.getIdEntidad());
                request.setAttribute("filtroComboUsuario", filterCombo);
            } else {
                usuarioId = String.valueOf(usuario.getIdUsuario());
                filter.put("usuarioId", usuarioId);
            }
        } else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
            filter.put("usuarioId", usuario.getIdUsuario());
        }

        //filter.put("tipoTramite", "N");
        filter.put("filtroCadena", datos.getString("filtroCadena"));
        if (!datos.getString("fechaDesde").equals("")) {
            try {
                filter.put("fechaDesde", Util.getTimestampObject(datos.getString("fechaDesde")+" 00:00:00", "dd/MM/yyyy HH:mm:ss"));
                request.setAttribute("fechaDesde", datos.getString("fechaDesde"));
            } catch (ParseException e) {
            }
        }
        if (!datos.getString("fechaHasta").equals("")) {
            try {
                filter.put("fechaHasta", Util.getTimestampObject(datos.getString("fechaHasta")+" 23:59:59", "dd/MM/yyyy HH:mm:ss"));
                request.setAttribute("fechaHasta", datos.getString("fechaHasta"));
            } catch (ParseException e) {
            }
        }
        if (filter.get("idEntidad")==null && !datos.getString("entidad").equals("")) {
            filter.put("idEntidad", datos.getString("entidad"));
            request.setAttribute("entidad", datos.getString("entidad"));
        }
        if (!datos.getString("filtroEntidad").equals("")) {
            filter.put("idEntidad", datos.getInt("filtroEntidad"));
            request.setAttribute("entidad", datos.getInt("filtroEntidad"));
        }
        
        request.setAttribute("filtroCadena", datos.getString("filtroCadena"));
        request.setAttribute("keyGrilla", key);
        request.setAttribute("filter", filter);
    }

    public ModelAndView mostrarDetalleMensaje(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        int idMensaje = datos.getInt("idMensaje");
        int destinatario = datos.getInt("destinatario");
        filter.put("idMensaje",idMensaje);

        Table tAdjuntosMensaje = ibatisService.loadGrid("buzon.mensaje.adjuntos", filter);
        request.setAttribute("tAdjuntosMensaje", tAdjuntosMensaje);
        if(tAdjuntosMensaje.size()>0){
        	request.setAttribute("mostrarAdjuntosMensaje", "S");
        }

        if (idMensaje != 0) {
            MessageList messageListCarga = buzonLogic.getMessageById(usuario, idMensaje , destinatario);
            Mensaje mensajeObj = (Mensaje) messageListCarga.getObject();
            if (mensajeObj != null) {
                RequestUtil.setAttributes(request, "MENSAJE.", mensajeObj);

                //cargar notificaciones del mensaje
                if(mensajeObj.getNotificacionId() != null){
                    HashUtil<String, Object> filterNot = new HashUtil<String, Object>();
                    filterNot.put("idNotificacion",mensajeObj.getNotificacionId());
                    Table tNotificacionesMensaje = ibatisService.loadGrid("mensaje.notificaciones.elements", filterNot);
                    request.setAttribute("tNotificacionesMensaje", tNotificacionesMensaje);
                    if(tNotificacionesMensaje.size()>0){
                    	request.setAttribute("mostrarNotificacionesMensaje", "S");
                    }
                }

                if (datos.contains("suceId") && !"".equals(datos.getString("suceId"))){
                	Suce suceObj = suceLogic.getSuceById(datos.getInt("suceId"));
                    request.setAttribute("nombreLink", "Abrir SUCE " + suceObj.getNumSuce());//LabelConfig.getLabelConfig().getProperty("vuce.buzon.suce", new Object [] {""+suce.getNumSuce()}));
                    request.setAttribute("link", "javascript:verSuce()");//LabelConfig.getLabelConfig().getProperty("vuce.buzon.suce.link", new Object [] {"'"+formato.getFormato().toLowerCase()+"'", ""+suce.getIdOrden(), ""+orden.getMto()}));
                    request.setAttribute("target", "_self");
                }

            }
            request.setAttribute("idMensaje", idMensaje);
        }

        request.setAttribute("orden", request.getParameter("orden"));
        request.setAttribute("mto", request.getParameter("mto"));
        request.setAttribute("formato", request.getParameter("formato"));
        return new ModelAndView(detalleMensaje);
    }

    public ModelAndView inactivarMensajes(HttpServletRequest request, HttpServletResponse response) {
        HashUtil<String, Table> tables = TableUtil.obtainTablesForGridInsertUpdate(RequestUtil.getParameter(request));
        String [] deleteColumns = RequestUtil.getParameterValuesArray(request, "seleccione");
        int id;
        int destinatario;
        Table tablaAdjuntos = (Table)tables.get("mensajes");
        for(int i = 0; i< deleteColumns.length; i++ ){
            Row row = tablaAdjuntos.obtainRowByKeyFields(deleteColumns[i]);
            id = Integer.parseInt(row.getCell("MENSAJE_ID").getValue().toString());
            destinatario = Integer.parseInt(row.getCell("DESTINATARIO").getValue().toString());
            System.out.println("Se eliminara: "+id);

            buzonLogic.inactivaMensaje(id, destinatario);
        }

        cargarMensajes(request);
        return new ModelAndView(mensajes);
    }

     public ModelAndView descargarAdjunto(HttpServletRequest request, HttpServletResponse response) {

             HashUtil<String, String> datos = RequestUtil.getParameter(request);
             int claveAdjunto = datos.getInt("idAdjunto");

             Adjunto adjunto = buzonLogic.descargarAdjuntoById(claveAdjunto);
            byte [] bytes = adjunto.getArchivo();
            try {
                if (bytes != null && bytes.length > 0) {
                    String nombreDocumento = adjunto.getNombre();
                                    response.setContentType("application/force-download");
                    response.setHeader("Content-Disposition", "attachment;filename="+nombreDocumento);
                    response.setContentLength(bytes.length);
                    ServletOutputStream ouputStream = response.getOutputStream();
                    ouputStream.write(bytes, 0, bytes.length);
                    ouputStream.flush();
                    ouputStream.close();
                }
            } catch (Exception e) {
                logger.error("Ocurrio un error al intentar descargar el archivo", e);
            }
            return null;
    }

    public ModelAndView imprimirCPB(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        int idMensaje = datos.getInt("idMensaje");
        RequestUtil.setAttributes(request);

        Mensaje mensaje = buzonLogic.getMessageById(idMensaje);

        filter.put("buzonId", mensaje.getBuzonId());
        HashUtil<String, Object> element = ibatisService.loadElement("buzon.impresionCPB.element", filter);

        Map parameters = new HashMap();
        parameters.put("P_CPB", element.getString("CPB"));
        parameters.put("P_MONTO", element.getDouble("MONTO"));
        parameters.put("P_FECHA", element.get("FECHA_GENERACION"));
        parameters.put("P_FECHA_VIGENCIA", element.get("FECHA_VIGENCIA"));
        parameters.put("P_USUARIO", element.getString("NOMBRE_USUARIO"));
        parameters.put("P_NUMERO_DOCUMENTO", element.getString("NUMERO_DOCUMENTO"));
        parameters.put("P_FORMATO", element.getString("CODIGO_FORMATO")+" - "+element.getString("NOMBRE_FORMATO")+" / TUPA: "+element.getString("TUPA"));
        parameters.put("P_ORDEN", element.getString("SOLICITUD"));
        parameters.put("P_SUCE", element.getString("SUCE"));

        //parameters.put("FILE_IMAGEN", ctx.getResource("/images/logoReporte.jpg").getFile());

        try {
            Resource jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.cpb", "");
            //ctx.getResource("WEB-INF/resource/reportes/hojaResumen.jasper");
            JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());

            JasperPrint print = JasperFillManager.fillReport(jReport, parameters, new JREmptyDataSource());

            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline;filename=cpb.pdf");
            OutputStream os = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, os);
            os.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setSuceLogic(SuceLogic suceLogic) {
        this.suceLogic = suceLogic;
    }

}
