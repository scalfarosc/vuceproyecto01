package pe.gob.mincetur.vuce.co.web.ajax.certificado;

import org.apache.log4j.Logger;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;

public class MercanciaDisponibleElementLoader {

    static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
    
    /**
     * Creates a new instance of DatosRUCElementLoader
     */
    public MercanciaDisponibleElementLoader() {
    }
    
    public static HashUtil<String, String> cargarDatosMercanciaDisponible(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();
        
        String calificacionDrId = filtros.getString("filter").split("[|]")[0].split("[=]")[1];
        String secuenciaAcuerdo = filtros.getString("filter").split("[|]")[1].split("[=]")[1];
        
        if (calificacionDrId.equals("")) return element;
        if (secuenciaAcuerdo.equals("")) return element;
        
        try {
        	
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("calificacionDrId", calificacionDrId);
        	filter.put("secuenciaAcuerdo", secuenciaAcuerdo);
        	
        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	HashUtil<String, Object> datos = ibatisService.loadElement("certificado.detalle.mercanciaDisponible.element", filter);
        	
        	element.put("FCO003DETALLE.calificacionDrId", datos.getString("CALIFICACION_DR_ID"));
        	element.put("FCO003DETALLE.secuenciaAcuerdo", datos.getString("SECUENCIA_ACUERDO"));
        	element.put("FCO003DETALLE.descripcionComercial", datos.getString("DESCRIPCION_COMERCIAL"));
        	element.put("partidaArancelariaDesc", datos.getString("DESC_PARTIDA_ARANCELARIA"));
        	element.put("FCO003DETALLE.criterioOrigen", datos.getString("CRITERIO_ORIGEN"));
        	element.put("unidadMedidaFisicaDesc", datos.getString("UNIDAD_MEDIDA"));
        	
        	
        } catch(Exception e) {
        	logger.error("ERROR al intentar obtener los datos de la Mercancia Disponible", e);
        }
        return element;
    }
    
}
