package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;
import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;

public class MCT001ButtonValidator extends FormatoButtonValidator {

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        Object idFormatoEntidad = request.getAttribute("idFormatoEntidad");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String permiteFactura = (String)request.getAttribute("mostrarBotonFactura");

        if (bloqueado==null) bloqueado = "";

        if (idFormatoEntidad != null && bloqueado.equalsIgnoreCase("S")) {
        	disabledButtons.put("GrabarDetalleButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("NuevaFacturaButton", Constantes.DISABLING_ACTION_DISABLE);
        	disabledButtons.put("nuevaMercanciaButton", Constantes.DISABLING_ACTION_DISABLE);
        }
    	if ("N".equals(permiteFactura)) {
    		disabledButtons.put("NuevaFacturaButton", Constantes.DISABLING_ACTION_HIDE);
    	}

        if ( !( usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
        		Rol.CO_ENTIDAD_EVALUADOR.getNombre().equals(usuario.getRolActivo())) ) { // El bot�n s�lo se muestra a los evaluadores
        	disabledButtons.put("confirmarFinEvalButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
        	String permiteConfirmarFinEval = request.getAttribute("mostrarBotonConfirmarFinEval") != null ? (String) request.getAttribute("mostrarBotonConfirmarFinEval") : "N";

        	if ("N".equals(permiteConfirmarFinEval)) {
        		disabledButtons.put("confirmarFinEvalButton", Constantes.DISABLING_ACTION_HIDE);
        	}
        }
        //HashUtil<String, String> enabledButtons = new HashUtil<String, String>();
        //enabledButtons.put("nuevaSubsanacionButton", "nuevaSubsanacionButton");
        //buttonSet.put(Constantes.BUTTON_SET_ENABLE, enabledButtons);
        //disabledButtons.put("nuevaSubsanacionButton", Constantes.DISABLING_ACTION_HIDE);

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);

        return buttonSet;
    }

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formFactura(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        Object id = request.getAttribute("secuencia");
        String bloqueado = (String)request.getAttribute("bloqueado");
        if (bloqueado==null) bloqueado = "";

        if (id!= null && "0".equals(id.toString())) {
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarBButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarAdjButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarAdjBButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarAdjButton", Constantes.DISABLING_ACTION_DISABLE);

                disabledButtons.put("grabarBButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarBButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarAdjBButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        }

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formatoMercancia(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        Object id = request.getAttribute("secuencia");
        Object djId = request.getAttribute("CALIFICACION.djId");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String esCalificacionOrigenStandAlone = (String)request.getAttribute("esCalificacionOrigenStandAlone");
        String estadoDJ = (request.getAttribute("CO_MERCANCIA.estadoRegistroDJ")!= null?(String)request.getAttribute("CO_MERCANCIA.estadoRegistroDJ"):"");
        String certificadoReexportacion = (request.getAttribute("certificadoReexportacion")!= null?(String)request.getAttribute("certificadoReexportacion"):"");
        
        if (bloqueado == null) bloqueado = "";

        if (id!= null && "0".equals(id.toString())) {
        	boolean bloquearBtnAutoriz = false;
            disabledButtons.put("editarDJButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
            if(bloqueado.equalsIgnoreCase("S") && esCalificacionOrigenStandAlone.equalsIgnoreCase("S")){
            	 disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
            	 disabledButtons.put("grabarCalifButton", Constantes.DISABLING_ACTION_DISABLE);
            	 disabledButtons.put("grabarCOButton", Constantes.DISABLING_ACTION_DISABLE);

            	 if (request.getAttribute("CALIFICACION.fechaInicioVigencia") !=null && request.getAttribute("CALIFICACION.fechaFinVigencia") !=null ) {
            		 Date fIniVig = (Date) request.getAttribute("CALIFICACION.fechaInicioVigencia");
            		 Date fFinVig = (Date) request.getAttribute("CALIFICACION.fechaFinVigencia");
            		 Date fActual = new Date();

            		 fIniVig.setHours(0);
            		 fIniVig.setMinutes(0);
            		 fIniVig.setSeconds(0);

            		 if (fActual.before(fIniVig)) {
            			 bloquearBtnAutoriz = true;
            		 } else {
            			 fActual.setHours(0);
            			 fActual.setMinutes(0);
            			 fActual.setSeconds(0);

            			 if (fActual.after(fFinVig)){
            				 bloquearBtnAutoriz = true;
            			 }
            		 }

            		//disabledButtons.put("nuevaAutorizacionButton", Constantes.DISABLING_ACTION_DISABLE);
            	 } else {
            		 bloquearBtnAutoriz = true;
            	 }

            	 if (bloquearBtnAutoriz) {
            		 disabledButtons.put("nuevaAutorizacionButton", Constantes.DISABLING_ACTION_DISABLE);
            	 }

            }
        } else {
            if (bloqueado.equalsIgnoreCase("S")) {
                disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("grabarCalifButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("grabarCOButton", Constantes.DISABLING_ACTION_DISABLE);
                disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);
            }
        }

        if (djId == null) {
            disabledButtons.put("mostrarDJButton", Constantes.DISABLING_ACTION_HIDE);
            if(!certificadoReexportacion.equals("S")){
            	disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
            }
            disabledButtons.put("grabarCalifButton", Constantes.DISABLING_ACTION_DISABLE);
            disabledButtons.put("grabarCOButton", Constantes.DISABLING_ACTION_DISABLE);
        } else {
        	disabledButtons.put("nuevaDeclaracionButton", Constantes.DISABLING_ACTION_HIDE);
            disabledButtons.put("mercanciaExistenteButton", Constantes.DISABLING_ACTION_HIDE);
        }

        Object datDj = request.getAttribute("dj");
        if (datDj != null || "S".equalsIgnoreCase(bloqueado)) {
        	disabledButtons.put("editarDJButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
        	if (ConstantesCO.DJ_ESTADO_PENDIENTE_ACEPTACION_VALIDACION.equals(estadoDJ) || ConstantesCO.DJ_ESTADO_VALIDADO_PENDIENTE_ENVIO.equals(estadoDJ)){
        		disabledButtons.put("editarDJButton", Constantes.DISABLING_ACTION_HIDE);
        	} else {
        		disabledButtons.put("mostrarDJButton", Constantes.DISABLING_ACTION_HIDE);
        	}
        }

    	//Validar los botones del productor
    	ComponenteOrigenUtil.validarBotonesProductor(request, disabledButtons);

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> modificacionesSuce(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        Object idEntidad = request.getAttribute("idEntidad");
        String formato = (String)request.getAttribute("formato");
        String bloqueado = (String)request.getAttribute("bloqueado");
        String estadoRegistro = (String)request.getAttribute("estadoRegistro");
        String utilizaModificacionSuceXMto = (String)request.getAttribute("utilizaModificacionSuceXMto");
        String historico = (String)request.getAttribute("historico");

        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        int numNotifPendientes = Integer.parseInt( ( request.getAttribute("notifPendientes") != null ? request.getAttribute("notifPendientes").toString().trim() : "0" ) );

        if (disabledButtons.get("nuevaSubsabacionButton") == null && (numNotifPendientes == 0 || "B".equals(estadoRegistro))) { // "B": Desistido
        	//Table notifPendientes = request.getAttribute("notifPendientes") != null?(Table) request.getAttribute("notifPendientes"): new Table();

        	//if (numNotifPendientes == 0) {
        		disabledButtons.put("nuevaSubsanacionButton", Constantes.DISABLING_ACTION_HIDE);
        	//}
        }
        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }
}
