package pe.gob.mincetur.vuce.co.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


public class ServiciosController  extends MultiActionController {
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);
   
    private String servicios;

	public void setServicios(String servicios) {
		this.servicios = servicios;
	}

	
	public ModelAndView servicios(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(servicios);
	}
		
}
