package pe.gob.mincetur.vuce.co.web.util;

import java.util.Hashtable;

import org.jlis.core.util.HashUtil;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.util.TableUtil;
import org.jlis.web.view.jsp.tag.grid.validator.RowValidator;

public class SolicitudesPrioritariasRowValidator implements RowValidator {

	public void validate(Hashtable cellFormatterList, Row row, HashUtil arg2) {
    	Cell cell = row.getCell("PRIORIDAD");
    	if ("P".equals(cell.getValue())) {
    		TableUtil.setRowStyleSheet(cellFormatterList, row, "prioritarioClass");
    	} else {
    		TableUtil.restoreRowStyleSheet(cellFormatterList, row);
    	}
	}

}
