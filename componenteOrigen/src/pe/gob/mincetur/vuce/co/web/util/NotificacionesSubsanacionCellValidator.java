package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class NotificacionesSubsanacionCellValidator implements CellValidator {
	
	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && cell.getValue().toString().equals("P")) {
        	buttonCell.setImage("/co/imagenes/editar.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.notificacionSubsanacion.editar"));
        } else {
        	buttonCell.setImage("/co/imagenes/ver.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.notificacionSubsanacion.ver"));
        }
    }
}
