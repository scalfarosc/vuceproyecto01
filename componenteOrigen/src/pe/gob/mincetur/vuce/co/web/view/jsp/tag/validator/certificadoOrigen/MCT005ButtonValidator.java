package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;

public class MCT005ButtonValidator extends FormatoButtonValidator {

	@SuppressWarnings("unchecked")
    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
        HashUtil<String, String> disabledButtons = new HashUtil<String, String>();
        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);
        Object idFormatoEntidad = request.getAttribute("idFormatoEntidad");
        String bloqueado = (String)request.getAttribute("bloqueado");

        if (bloqueado==null) bloqueado = "";

        if (ConstantesCO.OPCION_SI.equalsIgnoreCase(bloqueado)) {
        	disabledButtons.put("grabarCalifButton", Constantes.DISABLING_ACTION_HIDE);
        	disabledButtons.put("grabarCOButton", Constantes.DISABLING_ACTION_HIDE);
        }

        ComponenteOrigenUtil.validarBotonesProductor(request, disabledButtons);

        buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

}
