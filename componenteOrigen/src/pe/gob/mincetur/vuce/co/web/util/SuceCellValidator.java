package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class SuceCellValidator implements CellValidator {

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
    }

	public void mostrarIconoDrSuce(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue().toString().equals("W")) {
        	buttonCell.setImage("/co/imagenes/edit.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.dr.editar"));
        } else {
        	buttonCell.setImage("/co/imagenes/ver.gif");
        	buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.dr.ver"));
        }
    }

	public void mostrarAlertaFirmaSuce(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue().toString().equals("0")) {
        	buttonCell.setImage("/co/imagenes/tip.gif");
            buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.firma.noAdjuntada"));
        } else {
        	buttonCell.setImage("");
        	buttonCell.setAlt("");
        }
    }


}
