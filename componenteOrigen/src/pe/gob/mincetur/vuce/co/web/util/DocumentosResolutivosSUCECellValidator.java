package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class DocumentosResolutivosSUCECellValidator implements CellValidator {

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && cell.getValue().toString().equalsIgnoreCase("NS")) {
        	buttonCell.setOnClick("descargarAdjuntoDR");
        	buttonCell.setImage("/co/imagenes/descargar.png");
        } else {
        	buttonCell.setOnClick(null);
        	buttonCell.setImage("");
        }

    }
}
