package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator;

import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;


public class DocumentosResolutivosCellValidator implements CellValidator {

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && cell.getValue().toString().equals(ConstantesCO.TIPO_DR_APROBACION)) {
        	buttonCell.setImage("/co/imagenes/ver.gif");
            buttonCell.setAlt("Ver Datos del Documento Resolutivo");
        } else {
            buttonCell.setImage(null);
            buttonCell.setAlt(null);
        }
    }

	public void validarDocumentosResolutivosCalificacion(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && cell.getValue().toString().equals("N")) {
        	buttonCell.setImage("/co/imagenes/ver.gif");
            buttonCell.setAlt("Ver Datos del Documento Resolutivo");
        } else {
        	buttonCell.setImage("/vuce/imagenes/editar.gif");
            buttonCell.setAlt("Ver Datos del Documento Resolutivo");
        }
    }

}
