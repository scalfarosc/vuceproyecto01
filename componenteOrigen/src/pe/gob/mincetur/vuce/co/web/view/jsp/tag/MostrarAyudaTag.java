package pe.gob.mincetur.vuce.co.web.view.jsp.tag;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

/**
 * @author Erick Osc�tegui
 */
public class MostrarAyudaTag extends TagSupport {

    private String etiqueta;
    private String material;
    private String codigoPais;
    private String acuerdoId = "";

	/**
     * M�todo inicial.
     *
     * @return SKIP_BODY
     */
    @SuppressWarnings("unchecked")
    public int doStartTag() throws JspException {
    	WebApplicationContext applicationContext = RequestContextUtils.getWebApplicationContext(pageContext.getRequest(),pageContext.getServletContext());
    	FormatoLogic formatoLogic = (FormatoLogic) applicationContext.getBean("formatoLogic");
    	String etiquetaFinal = null;

    	if (material != null) { // Estamos en el caso de materiales
        	if (ConstantesCO.MOSTRAR_AYUDA_ATRIBUTO_MATERIAL_SI.equals(material)) { // Debemos reemplazar de acuerdo al c�digo de pa�s que coloquemos
        		String cadenaComponente = ComponenteOrigenUtil.obtenerCadenaParaEtiquetaAyuda(acuerdoId, codigoPais); // Generar� la cadena a completar de acuerdo al codigo de pais utilizado
        		etiquetaFinal = etiqueta.replaceFirst("##", cadenaComponente);
        	}
    	} else {
    		etiquetaFinal = etiqueta;
    	}

    	Map<String, Object> filtros = new HashMap<String, Object>();
    	filtros.put("etiqueta", etiquetaFinal);

    	String ayuda = formatoLogic.obtenerAyuda(filtros);
		StringBuffer botonAyuda = new StringBuffer("");

    	if (!"".equals(ayuda)) {
    		botonAyuda.append("<img src='/co/imagenes/ayuda.jpg' width='20px' heigth='20px' title='")
    				  .append(ayuda)
    				  .append("' help='yes' />");
    	}

    	try {
			pageContext.getOut().write(botonAyuda.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return SKIP_BODY;
    }

    /**
     * M�todo final.
     *
     * @return EVAL_PAGE
     */
    public int doEndTag() {
    	etiqueta = null;
        return EVAL_PAGE;
    }

    public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getAcuerdoId() {
		return acuerdoId;
	}

	public void setAcuerdoId(String acuerdoId) {
		this.acuerdoId = acuerdoId;
	}

	public MostrarAyudaTag() {
		super();
    }

}
