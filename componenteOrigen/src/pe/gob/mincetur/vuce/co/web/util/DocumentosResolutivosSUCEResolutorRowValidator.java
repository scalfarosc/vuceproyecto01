package pe.gob.mincetur.vuce.co.web.util;

import java.util.Hashtable;

import org.jlis.core.config.LabelConfig;
import org.jlis.core.util.HashUtil;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.util.TableUtil;
import org.jlis.web.view.jsp.tag.grid.validator.RowValidator;

public class DocumentosResolutivosSUCEResolutorRowValidator implements RowValidator {

	public void validate(Hashtable cellFormatterList, Row row, HashUtil arg2) {
    	Cell cellAnulado = row.getCell("ANULADO");

    	if (cellAnulado.getValue()!=null && "S".equals(cellAnulado.getValue())) {
    		TableUtil.setRowStyleSheet(cellFormatterList, row, "codigoAnuladoClass");
    		TableUtil.disableRow(cellFormatterList, row);
    	} else {
    		TableUtil.restoreRowStyleSheet(cellFormatterList, row);
    		TableUtil.enableRow(cellFormatterList, row);
    		
    	}
	}

}
