package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class ResolutorAsignadosEstadosCellValidator implements CellValidator {

    public void mostrarAccionSuce(GenericCellFormatter cellFormatter, Cell cell, ButtonCell buttonCell) {
    	//String estado = null;
    	//String suce = null;
    	String accion = null;
    	//String [] arregloAcciones = null;
        if (cell.getValue() != null && !cell.getValue().toString().equals("")) {
        	accion = cell.getValue().toString();
        }

        /*if (accion != null) {
        	arregloAcciones = accion.split("_");
        	if (arregloAcciones.length == 1) {
        		estado = arregloAcciones[0];
        	} else if (arregloAcciones.length == 2) {
        		estado = arregloAcciones[0];
        		suce = arregloAcciones[1];
        	}
        }*/

        if (accion != null) {
        	if (!"".equals(accion)) { // Si tiene suce
        		buttonCell.setOnClick("editarSolicitudCalificada");
        		buttonCell.setImage("/co/imagenes/editar.gif");
        	} else { // Si no tiene suce no se puede hacer nada de esto
        		buttonCell.setOnClick("");
        		buttonCell.setImage("");
        	}
        }

    }

    public void mostrarEvaluar(GenericCellFormatter cellFormatter, Cell cell, ButtonCell buttonCell) {
    	String estado = null;
    	//String suce = null;
    	/*String accion = null;
    	String [] arregloAcciones = null;*/
        if (cell.getValue() != null && !cell.getValue().toString().equals("")) {
        	estado = cell.getValue().toString();
        } else {
        	estado = ConstantesCO.OPCION_NO;
        }

        /*if (accion != null) {
        	arregloAcciones = accion.split("_");
        	if (arregloAcciones.length == 1) {
        		estado = arregloAcciones[0];
        	} else if (arregloAcciones.length == 2) {
        		estado = arregloAcciones[0];
        		suce = arregloAcciones[1];
        	}
        }*/

        if (estado != null) {
        	if (estado != null && "S".equals(estado)) { // Si est� en estado por calificar
        		buttonCell.setOnClick("evaluarCertif");
        		buttonCell.setImage("/co/imagenes/flecha_asignar.gif");
        	} else /*if ("T".equals(estado))*/ { // Caso contrario
        		buttonCell.setOnClick("");
        		buttonCell.setImage("");
        	}
        }

    }

    public void mostrarRetifPendiente(GenericCellFormatter cellFormatter, Cell cell, ButtonCell buttonCell) {

    	if (cell.getValue() != null && !"0".equals(cell.getValue().toString())) {
    		buttonCell.setOnClick("evaluarCertif");
    		buttonCell.setImage("/co/imagenes/tip2.gif");
    		buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.solicitud.rectificacion.pendiente"));
    	} else {
    		buttonCell.setOnClick("");
    		buttonCell.setImage("");
    	}

    }

	public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
	}

}
