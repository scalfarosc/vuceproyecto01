package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;

public class MTC001DRButtonValidator {

    public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
    	HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

        String transmitido = (String) request.getAttribute("transmitido");
        String estadoRegistro = (String) request.getAttribute("estadoRegistro");
        String formato = (String) request.getAttribute("formato");
        int numAdjuntosCODR = request.getAttribute("numAdjuntosCODR") != null? Integer.parseInt(request.getAttribute("numAdjuntosCODR").toString()) : 0;
        String estadoSDR = request.getAttribute("estadoSDR") != null ? request.getAttribute("estadoSDR").toString() : "";
        String esRectificacion = request.getAttribute("esRectificacion") != null ? request.getAttribute("esRectificacion").toString(): "" ;
        String mostrarBtnFinalizacion = request.getAttribute("mostrarBtnFinalizacion")!=null?request.getAttribute("mostrarBtnFinalizacion").toString():ConstantesCO.OPCION_NO;
        String vigente = request.getAttribute("vigente")!=null ? (String) request.getAttribute("vigente"): ConstantesCO.OPCION_NO;

        System.out.println("***********************estadoSDR: "+estadoSDR);

		UsuarioCO usuario = (UsuarioCO) request.getSession().getAttribute("USUARIO");

        if (ConstantesCO.OPCION_SI.equals(transmitido)) {
        	disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_DISABLE);
    		disabledButtons.put("transmitirButton", Constantes.DISABLING_ACTION_DISABLE);
    		disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);

    		if (!ConstantesCO.OPCION_SI.equals(vigente) || ("I".equals(estadoSDR)) || !Rol.CO_ENTIDAD_FIRMA.getNombre().equals(usuario.getRolActivo())) {
    			disabledButtons.put("printResumeButton", Constantes.DISABLING_ACTION_HIDE);
    		}

        } else {
        	disabledButtons.put("printResumeButton", Constantes.DISABLING_ACTION_DISABLE);
        }

        if (formato.equalsIgnoreCase(ConstantesCO.CO_CALIFICACION_ANTICIPADA)) {
        	disabledButtons.put("printResumeButton", Constantes.DISABLING_ACTION_HIDE);
        }

        if (ConstantesCO.OPCION_SI.equals(esRectificacion) && "P".equals(estadoSDR)){
        	//disabledButtons.put("printResumeButton", Constantes.DISABLING_ACTION_HIDE);
        } else {
        	disabledButtons.put("eliminarRectificacionButton", Constantes.DISABLING_ACTION_HIDE);
    		disabledButtons.put("confirmarRectificacionButton", Constantes.DISABLING_ACTION_HIDE);
        }

        if (ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(formato) || ConstantesCO.CO_CALIFICACION_ANTICIPADA.equalsIgnoreCase(formato)
        	|| ConstantesCO.CO_ANULACION.equalsIgnoreCase(formato)) {
    		disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_HIDE);
        }

    	Table tAdjuntosCODR = (Table) request.getAttribute("tAdjuntosCODR");

    	if (tAdjuntosCODR ==null || tAdjuntosCODR.size() == 0) {
    		//disabledButtons.put("finalizarButton", Constantes.DISABLING_ACTION_DISABLE);
    		disabledButtons.put("eliminarAdjButton", Constantes.DISABLING_ACTION_DISABLE);
    	} else if ( tAdjuntosCODR.size() > 0) {
    		disabledButtons.put("cargarButton", Constantes.DISABLING_ACTION_DISABLE);
    	}

    	String suceCerrada = (request.getAttribute("suceCerrada")!=null? (String) request.getAttribute("suceCerrada") : "N");

        int numSDRPendientes = Integer.parseInt(request.getAttribute("numSDRPendientes").toString());

    	if ("S".equals(suceCerrada)){

    		if (esRectificacion.equals(ConstantesCO.OPCION_NO) || ("A".equals(estadoSDR) && numAdjuntosCODR > 0)) {
        		//disabledButtons.put("finalizarButton", Constantes.DISABLING_ACTION_DISABLE);
        		disabledButtons.put("cargarButton", Constantes.DISABLING_ACTION_DISABLE);
        		disabledButtons.put("eliminarAdjButton", Constantes.DISABLING_ACTION_DISABLE);
    		}
    	}

    	if (!"S".equals(suceCerrada) || numSDRPendientes == 0) {
    		disabledButtons.put("eliminarRectificacionButton", Constantes.DISABLING_ACTION_HIDE);
    		disabledButtons.put("confirmarRectificacionButton", Constantes.DISABLING_ACTION_HIDE);
    	} else {
    		disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
    		disabledButtons.put("transmitirButton", Constantes.DISABLING_ACTION_HIDE);
    		//disabledButtons.put("finalizarButton", Constantes.DISABLING_ACTION_HIDE);
    	}

    	if (numSDRPendientes > 0 && disabledButtons.containsKey("grabarButton") && !"mct002".equalsIgnoreCase(formato)){
    		disabledButtons.remove("grabarButton");
    	}

    	if (request.getAttribute("estadoSDR") != null && !"P".equals(request.getAttribute("estadoSDR").toString())) {
    		disabledButtons.put("grabarButton", Constantes.DISABLING_ACTION_DISABLE);
    	}

    	if ("S".equals(request.getAttribute("esRectificacion")) || (request.getAttribute("estadoSDR") != null && !"P".equals(request.getAttribute("estadoSDR").toString()) && "A".equals(estadoRegistro))) {
        	disabledButtons.put("eliminarButton", Constantes.DISABLING_ACTION_HIDE);
    		disabledButtons.put("transmitirButton", Constantes.DISABLING_ACTION_HIDE);
    		//disabledButtons.put("finalizarButton", Constantes.DISABLING_ACTION_HIDE);
    	}

    	if (ConstantesCO.OPCION_NO.equalsIgnoreCase(mostrarBtnFinalizacion) || !usuario.getRoles().containsKey(Rol.CO_ENTIDAD_FIRMA.getNombre())) {
    		disabledButtons.put("finalizarButton", Constantes.DISABLING_ACTION_HIDE);
    	}

    	buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

}
