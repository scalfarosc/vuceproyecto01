package pe.gob.mincetur.vuce.co.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Table;
import org.jlis.web.util.RequestUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;

public class TrazabilidadController extends MultiActionController {

	private IbatisService ibatisService;

	private String trazabilidad;

	public void setTrazabilidad(String trazabilidad) {
		this.trazabilidad = trazabilidad;
	}

    public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public ModelAndView listarTrazas(HttpServletRequest request, HttpServletResponse response) {
        UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        // Ocultar o mostrar la columna de Responsable
        if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT)) {
            request.setAttribute("hideResponsable", "no");
        } else {
            request.setAttribute("hideResponsable", "yes");
        }

        request.setAttribute("nombreColumna", "SOLICITUD");
        cargarTrazabilidad(request);
        return new ModelAndView(trazabilidad);
    }

    @SuppressWarnings("unchecked")
    private void cargarTrazabilidad(HttpServletRequest request) {
        HashUtil<String, String> datos = RequestUtil.getParameter(request);

        String rutaopcion = datos.getString("rutaopcion");
        String tipo = datos.getString("tipo");
        Integer intOrden = Integer.valueOf(0);
        Integer intSuce = Integer.valueOf(0);
        
        // Si existe la variable numero es porque estamos en el caso de la trazabilidad desde la opci�n de men�
        if ("1".equals(tipo)) {
            String numero = datos.getString("numero");
        	if (rutaopcion.equalsIgnoreCase("O") ) {
        		intOrden = (validarId(numero)) ? Integer.valueOf(numero).intValue() : Integer.valueOf(0);
        	} else {
        		intSuce = (validarId(numero)) ? Integer.valueOf(numero).intValue() : Integer.valueOf(0);
        	}
        } else {
            String orden = request.getParameter("numeroOrden");
            String suce = request.getParameter("numeroSuce");
            intOrden = (validarId(orden)) ? Integer.valueOf(orden).intValue() : Integer.valueOf(0);
            intSuce = (validarId(suce)) ? Integer.valueOf(suce).intValue() : Integer.valueOf(0);
        }
 
        if (intOrden > 0 || intSuce > 0) {
            UsuarioCO usuario = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();

            // Usuario CLAVE SOL
            if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL)) {
                filter.put("usuarioId", usuario.getIdUsuario());
            }
            // Si el usuario es de EXTRANET y NO tiene el rol HELP DESK
            else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) &&
                     !usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
            	// VE TODA LA INFORMACION
            } else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI)) {
                filter.put("usuarioId", usuario.getIdUsuario());
            }

            Table listadoTraza = null;
            if (rutaopcion.equalsIgnoreCase("O") ) {
                filter.put("orden", intOrden);
            	listadoTraza = ibatisService.loadGrid("trazabilidad.orden.grilla", filter);
            	request.setAttribute("nombreColumna", "SOLICITUD");
            	request.setAttribute("numero", intOrden!=0?intOrden:"");
            } else {
                filter.put("suce", intSuce);
            	listadoTraza = ibatisService.loadGrid("trazabilidad.suce.grilla", filter);
            	request.setAttribute("nombreColumna", "SUCE");
            	request.setAttribute("numero", intSuce!=0?intSuce:"");
            }

            request.setAttribute("listadoTrazabilidad", listadoTraza);
            request.setAttribute("numeroOrden", intOrden!=0?intOrden:"");
            request.setAttribute("numeroSuce", intSuce!=0?intSuce:"");
            request.setAttribute("rutaopcion", rutaopcion);
            //request.setAttribute("tipo", tipo);

            /*Orden orden = ordenLogic.loadOrdenByNumero(numeroOrden);
            if (orden!=null) {
                Formato formato = (Formato)formatoLogic.getFormatoByTupa(orden.getIdFormato(), orden.getIdTupa());
                request.setAttribute("nombreFormato", formato.getFormato()+" - "+formato.getNombre());
            }*/
        }
        request.setAttribute("tipo", tipo);
    }

    /**
     *
     * @param cadena Representa cualquier caracter
     * @return True si es un entero valido
     */
    private boolean validarId(String cadena) {
        try{
            Integer.parseInt(cadena);
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }

}
