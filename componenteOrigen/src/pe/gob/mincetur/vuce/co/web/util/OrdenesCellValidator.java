package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class OrdenesCellValidator implements CellValidator {

    public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue().toString().equals("N")) {
        	buttonCell.setImage("/co/imagenes/alert.gif");
            buttonCell.setAlt("SIN TRANSMITIR");
        } else {
            if (cell.getValue().toString().equals("A")) {
                buttonCell.setImage("/co/imagenes/bullgrn.gif");
                buttonCell.setAlt("ACEPTADA");
            } else if (cell.getValue().toString().equals("R")) {
                buttonCell.setImage("/co/imagenes/bullred.gif");
                buttonCell.setAlt("RECHAZADA");
            } else if (cell.getValue().toString().equals("P")) {
                buttonCell.setImage("/co/imagenes/bullyel.gif");
                buttonCell.setAlt("PENDIENTE");
            } else if (cell.getValue().toString().equals("CERRADA")) {
                buttonCell.setImage("/co/imagenes/bullgrn.gif");
                buttonCell.setAlt("CON RESPUESTA");
            } else if (cell.getValue().toString().equals("SR")) {
                buttonCell.setImage("/co/imagenes/bullyel.gif");
                buttonCell.setAlt("SIN RESPUESTA");
            } else {
            	buttonCell.setImage("/co/imagenes/alert.gif");
                buttonCell.setAlt("SIN TRANSMITIR");
            }
        }

    }

}
