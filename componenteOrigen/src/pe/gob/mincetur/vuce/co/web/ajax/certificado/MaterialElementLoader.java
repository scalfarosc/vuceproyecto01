package pe.gob.mincetur.vuce.co.web.ajax.certificado;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class MaterialElementLoader {

    static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);

    /**
     * Creates a new instance of DatosRUCElementLoader
     */
    public MaterialElementLoader() {
    }

    public static HashUtil<String, String> actualizarConsolidado(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();

        long djId = new Long(filtros.getString("filter").split("[|]")[0].split("[=]")[1]);
        BigDecimal demasGasto = new BigDecimal(filtros.getString("filter").split("[|]")[1].split("[=]")[1]);
        BigDecimal prctjSegunCriterio = null;
        String strPrctjSegunCriterio = filtros.getString("filter").split("[|]")[2].split("[=]")[1];
        if (!ConstantesCO.OPCION_NO.equals(strPrctjSegunCriterio)) {
        	prctjSegunCriterio = new BigDecimal(strPrctjSegunCriterio);
        }
        BigDecimal pesoNetoMercancia = null;
        String strPesoNetoMercancia = filtros.getString("filter").split("[|]")[3].split("[=]")[1];
        if (!ConstantesCO.OPCION_NO.equals(strPesoNetoMercancia)) {
        	pesoNetoMercancia = new BigDecimal(strPesoNetoMercancia);
        }
        BigDecimal valorUsFabrica = null;
        String strValorUsFabrica = filtros.getString("filter").split("[|]")[4].split("[=]")[1];
        if (!ConstantesCO.OPCION_NO.equals(strValorUsFabrica)) {
        	valorUsFabrica = new BigDecimal(strValorUsFabrica);
        }

        try {

        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("djId", djId);
        	filter.put("demasGasto", demasGasto);
        	filter.put("pesoNetoMercancia", pesoNetoMercancia);
        	filter.put("porcentajeSegunCriterio", prctjSegunCriterio);
        	filter.put("valorUsFabrica", valorUsFabrica);

        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	HashUtil<String, Object> datos = ibatisService.loadElement("certificadoOrigen.orden.dj.consolidado.update", filter);

        	element.put("CONSOLIDADO.mensaje", "El consolidado ha sido grabado con �xito");

        } catch(Exception e) {
        	element.put("CONSOLIDADO.mensaje", "ERROR al intentar actualizar el consolidado del Material");
        	logger.error("ERROR al intentar actualizar el consolidado del Material", e);
        } finally {
        	return element;
        }
    }
    
    
	public static HashUtil<String, String> validarEsProductor(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();
        
	    String[] filtro = filtros.getString("filter").split("[|]");
		int tipoDocumento = new Integer(filtro[0]);
		String numeroDocumento = filtro[1];
		long djId = new Long(filtro[2]);
		
		String respuesta = null;
	    try {
        	HashUtil<String, Object> filterProductor = new HashUtil<String, Object>();
        	filterProductor.put("estado", null);
        	filterProductor.put("tipoDocumento", tipoDocumento);
        	filterProductor.put("numeroDocumento", numeroDocumento);
        	filterProductor.put("djId", djId);
        	
        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	ibatisService.loadElement("dj.material.esProductorPorOrden", filterProductor);
        	respuesta = filterProductor.getString("estado");
        	
        	element.put("flagEsProductor", respuesta);
			
		} catch (Exception e) {
			element.put("flagEsProductor", "No es un codigo valido");
			logger.error(e);
		} finally {
			return element;
		}
	  
    }    
    
    

}
