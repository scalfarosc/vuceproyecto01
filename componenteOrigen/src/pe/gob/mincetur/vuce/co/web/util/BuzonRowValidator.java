package pe.gob.mincetur.vuce.co.web.util;

import java.util.Hashtable;

import org.jlis.core.util.HashUtil;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.RowValidator;

public class BuzonRowValidator implements RowValidator {

    public void validarEliminarMensaje(Hashtable cellFormatters, Row row, HashUtil arg2) {
    	Cell cell = row.getCell("INACTIVAR");
    	GenericCellFormatter cellFormatter = (GenericCellFormatter)cellFormatters.get(cell.getColumnName());
        if (row.getCell("ESTALEIDO").getValue().toString().equals("S")) {
        	cellFormatter.setEditable(true);
        } else {
        	cellFormatter.setEditable(false);
        }

    }

	public void validate(Hashtable arg0, Row arg1, HashUtil arg2) {
	}

}
