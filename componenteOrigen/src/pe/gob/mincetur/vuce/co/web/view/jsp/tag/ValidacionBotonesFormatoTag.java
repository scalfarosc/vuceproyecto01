/*
 * ValidacionBotonesFormatoTag.java
 *
 */

package pe.gob.mincetur.vuce.co.web.view.jsp.tag;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;

/**
 * @author Erick Osc�tegui
 */
public class ValidacionBotonesFormatoTag extends TagSupport {

    /** Holds value of property pagina. */
    private String pagina;

    /** Holds value of property formato. */
    private String formato;

    public ValidacionBotonesFormatoTag() {
        super();
    }

    /**
     * Method called at start of tag.
     *
     * @return SKIP_BODY
     */
    @SuppressWarnings("unchecked")
    public int doStartTag() throws JspException {
    	HashUtil<String, HashUtil<String, String>> buttonSet = (HashUtil<String, HashUtil<String, String>>)pageContext.getAttribute(Constantes.BUTTON_SET);
    	if (buttonSet==null) buttonSet = new HashUtil<String, HashUtil<String, String>>();

        Class parameterTypes [] = new Class[2];
        parameterTypes[0] = HttpServletRequest.class;
        parameterTypes[1] = HashUtil.class;

        Object args [] = new Object[2];
        args[0] = (HttpServletRequest)pageContext.getRequest();
        args[1] = buttonSet;

        try {
            Class clazz = Class.forName(getFormatoButtonValidator());
            Object obj = clazz.newInstance();
            String method = (pagina != null) ? pagina : "formato";
            buttonSet = (HashUtil<String, HashUtil<String, String>>)clazz.getDeclaredMethod(method, parameterTypes).invoke(obj, args);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        if (buttonSet.size() > 0) {
            if (buttonSet.containsKey(Constantes.BUTTON_SET_ENABLE)) {
                HashUtil<String, String> enabledButtons = (HashUtil<String, String>)buttonSet.get(Constantes.BUTTON_SET_ENABLE);
                HashUtil<String, String> enabledButtonsRequest;
                if (pageContext.getAttribute(Constantes.BUTTON_SET_ENABLE) != null) {
                	enabledButtonsRequest = (HashUtil<String, String>) pageContext.getAttribute(Constantes.BUTTON_SET_ENABLE);
                } else {
                	enabledButtonsRequest = new HashUtil<String, String>();
                }
                enabledButtonsRequest.add(enabledButtons);
                pageContext.setAttribute(Constantes.ENABLED_BUTTONS, enabledButtonsRequest);
                buttonSet.put(Constantes.BUTTON_SET_ENABLE, enabledButtonsRequest);
            }
            if (buttonSet.containsKey(Constantes.BUTTON_SET_DISABLE)) {
                HashUtil<String, String> disabledButtons = (HashUtil<String, String>)buttonSet.get(Constantes.BUTTON_SET_DISABLE);
                HashUtil<String, String> disabledButtonsRequest;
                if (pageContext.getAttribute(Constantes.DISABLED_BUTTONS) != null) {
                	disabledButtonsRequest = (HashUtil<String, String>) pageContext.getAttribute(Constantes.DISABLED_BUTTONS);
                } else {
                	disabledButtonsRequest = new HashUtil<String, String>();
                }
                disabledButtonsRequest.add(disabledButtons);
                pageContext.setAttribute(Constantes.DISABLED_BUTTONS, disabledButtonsRequest);
                buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtonsRequest);
            }
        }
        pageContext.setAttribute(Constantes.BUTTON_SET, buttonSet);
        return SKIP_BODY;
    }

    private String getFormatoButtonValidator() {
    	String nombrePackage = "pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator";
    	String formatoButtonValidator = "ButtonValidator";
    	if (this.formato != null) {
	    	if (this.formato.startsWith(ConstantesCO.PREF_DJ.toUpperCase())) {
	    		formatoButtonValidator = nombrePackage + ".certificadoOrigen." + this.formato + formatoButtonValidator;
	    	} else {
	    		formatoButtonValidator = nombrePackage + ".certificadoOrigen." + this.formato + formatoButtonValidator;
	    	}
    	} else {
    		formatoButtonValidator = nombrePackage + ".Formato" + formatoButtonValidator;
    	}
    	return formatoButtonValidator;
    }

    /**
     * Method called at end of tag.
     *
     * @return EVAL_PAGE
     */
    public int doEndTag() {
    	formato = null;
        pagina = null;
        return EVAL_PAGE;
    }

    public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

}
