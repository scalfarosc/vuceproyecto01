package pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.certificadoOrigen;

import javax.servlet.http.HttpServletRequest;

import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.FormatoButtonValidator;

public class SuceButtonValidator extends FormatoButtonValidator {

	@SuppressWarnings("unchecked")
	public HashUtil<String, HashUtil<String, String>> formato(HttpServletRequest request, HashUtil<String, HashUtil<String, String>> buttonSet) {
    	HashUtil<String, String> disabledButtons = new HashUtil<String, String>();

    	Table tDocResolutivos = (Table) request.getAttribute("tDocResolutivos");
    	String estadoRegistro = (String) request.getAttribute("estadoRegistro");
    	String pendienteCalificacion = request.getAttribute("pendienteCalificacion") != null ? (String) request.getAttribute("pendienteCalificacion") : "";
    	String notificacionPendiente = request.getAttribute("notificacionPendiente") != null ? (String) request.getAttribute("notificacionPendiente") : "";
    	////20170113_GBT ACTA CO-004-16 Y CO-009-16
        //Integer idAcuerdo = (Integer)request.getAttribute("idAcuerdo");

    	if ((tDocResolutivos.size() > 0) || ConstantesCO.OPCION_SI.equalsIgnoreCase(notificacionPendiente) ) {
    		disabledButtons.put("nuevaNotificacionSubsanacionButton", Constantes.DISABLING_ACTION_HIDE);
    	}

    	if (!"T".equals(estadoRegistro) || ConstantesCO.OPCION_SI.equalsIgnoreCase(pendienteCalificacion)){
    		disabledButtons.put("nuevoBorradorButton", Constantes.DISABLING_ACTION_HIDE);
    		disabledButtons.put("nuevoBorradorDenegButton", Constantes.DISABLING_ACTION_HIDE);
    	}

    	//20170113_GBT ACTA CO-004-16 Y CO-009-16
    	//if (idAcuerdo==24 || idAcuerdo==17) {
        //		disabledButtons.put("nuevoBorradorButton", Constantes.DISABLING_ACTION_DISABLE);
        //		disabledButtons.put("nuevoBorradorDenegButton", Constantes.DISABLING_ACTION_DISABLE);
        //}      
        //FIN DE CAMBIO GBT
    	
    	buttonSet.put(Constantes.BUTTON_SET_DISABLE, disabledButtons);
        return buttonSet;
    }

}
