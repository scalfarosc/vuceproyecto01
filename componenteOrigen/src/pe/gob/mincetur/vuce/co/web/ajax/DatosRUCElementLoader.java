package pe.gob.mincetur.vuce.co.web.ajax;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.bean.Option;
import org.jlis.core.list.OptionList;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pe.gob.mincetur.vuce.co.logic.FormatoLogic;
import pe.gob.mincetur.vuce.co.logic.UsuariosLogic;
import pe.gob.mincetur.vuce.co.remoting.ws.client.ConsultaRUCCWS;
import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanSpr;
import ConsultaRuc.BeanT1144;

public class DatosRUCElementLoader {

    static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);

    /**
     * Creates a new instance of DatosRUCElementLoader
     */
    public DatosRUCElementLoader() {
    }

    private static HashUtil<String, String> cargarDatosVacios(HashUtil<String, String> element) {
    	element.put("rucEE", "");
    	element.put("EMPRESA_EXTERNA.nombre", "");
    	element.put("EMPRESA_EXTERNA.direccion", "");
        element.put("EMPRESA_EXTERNA.telefono", "");
        element.put("EMPRESA_EXTERNA.fax", "");
        element.put("EMPRESA_EXTERNA.email", "");
        element.put("id_departamentoEE", "");
        element.put("id_provinciaEE", "");
        element.put("id_distritoEE", "");
    	return element;
    }
    
    private static HashUtil<String, Object> cargarDatosUsuarioxRUC(String ruc) {
    	HashUtil<String, Object> usuarioDetail = null;
    	ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        
    	UsuariosLogic usuariosLogic = (UsuariosLogic)ctx.getBean("usuariosLogic");
    	IbatisService ibatisService = (IbatisService)ctx.getBean("ibatisService");
    	
        if (usuariosLogic.existeEmpresa(ruc)) {
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("ruc", ruc);
            usuarioDetail = ibatisService.loadElement("usuario.empresaByRuc.element", filter);
        }
        return usuarioDetail;
    }
    
    public static HashUtil<String, String> cargarDatosRUC(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();

        String ruc = filtros.getString("filter").split("[=]")[1];
        if (ruc.equals("")) return element;

        try {
        	// Consultamos a la SUNAT por el RUC
        	BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(ruc);

        	// Si obtenemos datos, llenamos el nombre
        	if ( datosRUC != null && datosRUC.getDdp_identi() != null ) {
                element.put("EXPORTADOR.nombre", datosRUC.getDdp_nombre().trim());

	            /*String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(ruc);
	            BeanDds datosSecundariosRUC = ConsultaRUCCWS.getDatosSecundarios(ruc);
	            BeanT1144 datosT1144 = ConsultaRUCCWS.getDatosT1144(ruc);

	            if (domicilioLegal!=null && datosSecundariosRUC!=null && datosT1144!=null) {
	                element.put("EXPORTADOR.direccion", domicilioLegal.trim());
	                element.put("EXPORTADOR.telefono", datosSecundariosRUC.getDds_telef1().trim());
	                element.put("EXPORTADOR.fax", datosSecundariosRUC.getDds_numfax().trim());
	                element.put("EXPORTADOR.email", datosT1144.getCod_correo1().trim());
	            }*/
        	} else { // Si NO obtenemos datos, debemos limpiar el nombre ya que no se ha obtenido nada
            	HashUtil<String, Object> usuarioDetail = cargarDatosUsuarioxRUC(ruc);
            	if (usuarioDetail!=null) {
            		element.put("EXPORTADOR.nombre", usuarioDetail.getString("NOMBRE"));
            	} else {
        		    element.put("EXPORTADOR.nombre", "");
            	}
        	}

        } catch(Exception e) {
        	logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return element;
    }

    public static HashUtil<String, String> cargarDatosRUCEmpresaExterna(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();

        String [] split = filtros.getString("filter").split("[|]");
        String ruc = split[0].split("[=]")[1];
        String esAgenteAduanas = split[1].split("[=]")[1];
        String esLaboratorio = split[2].split("[=]")[1];
        if (ruc.equals("")) return element;

        try {
        	// Si el usuario es Agencia de Aduanas o Laboratorio, se debe validar que exista en la BD
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("numeroDocumento", ruc);

			// Si es Agente de Aduanas
			if (esAgenteAduanas.equalsIgnoreCase("S")) {
				IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
			    boolean existe = ibatisService.loadGrid("usuario.agenciaAduanas.existe", filter).size() > 0;
			    if (!existe) {
			    	logger.warn("El RUC "+ruc+" de la Agencia de Aduanas no existe en la BD de la VUCE");
			    	Message message = new Message("vuce.logueo.rucEmpresaExternaNoValido");
			    	element.put("idMensajeRUC_EmpresaExterna", message.getMessage());
			    	cargarDatosVacios(element);
			    	return element;
			    }
		    }
			// Si es Laboratorio
			else if (esLaboratorio.equalsIgnoreCase("S")) {
		    	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
		    	boolean existe = ibatisService.loadGrid("usuario.laboratorio.existe", filter).size() > 0;
			    if (!existe) {
			    	logger.warn("El RUC "+ruc+" del Laboratorio no existe en la BD de la VUCE");
			    	Message message = new Message("vuce.logueo.rucEmpresaExternaNoValido");
			    	element.put("idMensajeRUC_EmpresaExterna", message.getMessage());
			    	cargarDatosVacios(element);
			    	return element;
			    }
		    }

			element.put("idMensajeRUC_EmpresaExterna", "");
			element.put("rucEE", ruc);
			
            ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = sra.getRequest();
            WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
            FormatoLogic formatoLogic = (FormatoLogic)ctx.getBean("formatoLogic");
            
        	BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(ruc);
        	
            if (datosRUC==null) {
            	HashUtil<String, Object> usuarioDetail = cargarDatosUsuarioxRUC(ruc);
            	if (usuarioDetail!=null) {
            		element.put("EMPRESA_EXTERNA.nombre", usuarioDetail.getString("NOMBRE"));
            		element.put("EMPRESA_EXTERNA.direccion", usuarioDetail.getString("DIRECCION"));
            		element.put("EMPRESA_EXTERNA.telefono", usuarioDetail.getString("TELEFONO"));
                    element.put("EMPRESA_EXTERNA.fax", usuarioDetail.getString("FAX"));
                    element.put("EMPRESA_EXTERNA.email", usuarioDetail.getString("EMAIL"));
                    
                    HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetailByDistrito(usuarioDetail.getInt("DISTRITO_ID"));
                    element.put("id_departamentoEE", datosUbigeo.get("DEPARTAMENTOID"));
                    element.put("id_provinciaEE", datosUbigeo.get("PROVINCIAID"));
                    element.put("id_distritoEE", datosUbigeo.get("DISTRITOID"));
            	}
            } else if (datosRUC!=null && datosRUC.getDdp_numruc()!=null) {
                element.put("EMPRESA_EXTERNA.nombre", datosRUC.getDdp_nombre().trim());

	            String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(ruc);
	            BeanDds datosSecundariosRUC = ConsultaRUCCWS.getDatosSecundarios(ruc);
	            BeanT1144 datosT1144 = ConsultaRUCCWS.getDatosT1144(ruc);
                
	            if (domicilioLegal!=null && datosSecundariosRUC!=null && datosT1144!=null) {
	                element.put("EMPRESA_EXTERNA.direccion", domicilioLegal.trim());
	                element.put("EMPRESA_EXTERNA.telefono", datosSecundariosRUC.getDds_telef1().trim());
	                element.put("EMPRESA_EXTERNA.fax", datosSecundariosRUC.getDds_numfax().trim());
	                element.put("EMPRESA_EXTERNA.email", datosT1144.getCod_correo1().trim());
                    
	                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
	                element.put("id_departamentoEE", datosUbigeo.get("DEPARTAMENTOID"));
	                element.put("id_provinciaEE", datosUbigeo.get("PROVINCIAID"));
	                element.put("id_distritoEE", datosUbigeo.get("DISTRITOID"));
	            }
        	}

        } catch(Exception e) {
        	logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return element;
    }

    public static HashUtil<String, String> cargarDatosDJProductorRUC(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();

        String[] arrFiltros = filtros.getString("filter").split("[|]");
        String ruc = arrFiltros[0].split("[=]")[1];
        if (ruc.equals("")) return element;

        try {
        	// Consultamos a la SUNAT por el RUC
        	BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(ruc);
        	BeanDds ddsRUC = ConsultaRUCCWS.getDatosSecundarios(ruc);

        	// Si obtenemos datos, llenamos el nombre
        	if ( datosRUC != null && datosRUC.getDdp_identi() != null ) {
        		if (arrFiltros.length < 2 || !"1".equals(arrFiltros[1].split("[=]")[1])) {
                    element.put("USUARIO_DJ.nombre", datosRUC.getDdp_nombre().trim());
        		} else {
        			element.put("DJ_PRODUCTOR.numeroDocumento", ruc);
                    element.put("DJ_PRODUCTOR.nombre", datosRUC.getDdp_nombre().trim());
                    element.put("DJ_PRODUCTOR.direccion", ConsultaRUCCWS.getDomicilioLegal(ruc));
                    element.put("DJ_PRODUCTOR.direccionAdicional", ConsultaRUCCWS.getDomicilioLegal(ruc));
                    element.put("DJ_PRODUCTOR.telefono", ddsRUC.getDds_telef1());
                    element.put("DJ_PRODUCTOR.fax", ddsRUC.getDds_numfax());
        		}
        	} else { // Si la Consulta RUC no responde
            	HashUtil<String, Object> usuarioDetail = cargarDatosUsuarioxRUC(ruc);
            	if (usuarioDetail!=null) {
            		if (arrFiltros.length < 2 || !"1".equals(arrFiltros[1].split("[=]")[1])) {
                        element.put("USUARIO_DJ.nombre", usuarioDetail.getString("NOMBRE"));
            		} else {
            			element.put("DJ_PRODUCTOR.numeroDocumento", ruc);
                        element.put("DJ_PRODUCTOR.nombre", usuarioDetail.getString("NOMBRE"));
                        element.put("DJ_PRODUCTOR.direccion", usuarioDetail.getString("DIRECCION"));
                        element.put("DJ_PRODUCTOR.direccionAdicional", usuarioDetail.getString("DIRECCION"));
                        element.put("DJ_PRODUCTOR.telefono", usuarioDetail.getString("TELEFONO"));
                        element.put("DJ_PRODUCTOR.fax", usuarioDetail.getString("FAX"));
            		}
            	} else { // Si NO obtenemos datos, debemos limpiar el nombre ya que no se ha obtenido nada
	        		if (filtros.get("subPantalla") == null || !"1".equals(filtros.get("subPantalla").toString())) {
	                    element.put("USUARIO_DJ.nombre", "");
	                    element.put("USUARIO_DJ.direccion", "");
	        		} else {
	                    element.put("DJ_PRODUCTOR.nombre", "");
	                    element.put("DJ_PRODUCTOR.direccion", "");
	                    element.put("DJ_PRODUCTOR.direccionAdicional", "");
	                    element.put("DJ_PRODUCTOR.telefono", "");
	                    element.put("DJ_PRODUCTOR.fax", "");
	        		}
            	}
        	}

        } catch(Exception e) {
        	logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return element;
    }

    public static HashUtil<String, String> cargarDatosDJMaterialProdRUC(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();
        
        String[] arrFiltros = filtros.getString("filter").split("[|]");
        String ruc = arrFiltros[0].split("[=]")[1];
        if (ruc.equals("")) return element;
        
        try {
        	// Consultamos a la SUNAT por el RUC
        	BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(ruc);
            
        	// Si obtenemos datos, llenamos el nombre
        	if ( datosRUC != null && datosRUC.getDdp_identi() != null ) {
        		if (arrFiltros.length < 2 || !"1".equals(arrFiltros[1].split("[=]")[1])) {
                    element.put("USUARIO_DJ.nombre", datosRUC.getDdp_nombre().trim());
        		} else {
        			element.put("DJ_MATERIAL.fabricanteNumeroDocumento", ruc);
                    element.put("DJ_MATERIAL.fabricanteNombre", datosRUC.getDdp_nombre().trim());
                    //element.put("DJ_PRODUCTOR.direccion", ConsultaRUCCWS.getDomicilioLegal(ruc));
        		}
        	} else { // Si la Consulta RUC no responde
            	HashUtil<String, Object> usuarioDetail = cargarDatosUsuarioxRUC(ruc);
            	if (usuarioDetail!=null) {
            		if (arrFiltros.length < 2 || !"1".equals(arrFiltros[1].split("[=]")[1])) {
                        element.put("USUARIO_DJ.nombre", usuarioDetail.getString("NOMBRE"));
            		} else {
            			element.put("DJ_MATERIAL.fabricanteNumeroDocumento", ruc);
                        element.put("DJ_MATERIAL.fabricanteNombre", usuarioDetail.getString("NOMBRE"));
                        //element.put("DJ_PRODUCTOR.direccion", ConsultaRUCCWS.getDomicilioLegal(ruc));
            		}
            	} else { // Si NO obtenemos datos, debemos limpiar el nombre ya que no se ha obtenido nada
	        		if (filtros.get("subPantalla") == null || !"1".equals(filtros.get("subPantalla").toString())) {
	                    element.put("USUARIO_DJ.nombre", "");
	        		} else {
	                    element.put("DJ_MATERIAL.fabricanteNombre", "");
	        		}
            	}
        	}

        } catch(Exception e) {
        	logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return element;
    }

    public static HashUtil<String, String> cargarDatosDJExportadorRUC(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();

        String[] arrFiltros = filtros.getString("filter").split("[|]");
        String ruc = arrFiltros[0].split("[=]")[1];
        if (ruc.equals("")) return element;

        try {
        	// Consultamos a la SUNAT por el RUC
        	BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(ruc);
            
        	// Si obtenemos datos, llenamos el nombre
        	if (datosRUC != null && datosRUC.getDdp_identi() != null) {
        		if (arrFiltros.length < 2 || !"1".equals(arrFiltros[1].split("[=]")[1])) {
                    element.put("nombre", datosRUC.getDdp_nombre().trim());
        		} else {
        			element.put("numeroDocumento", ruc);
                    element.put("nombre", datosRUC.getDdp_nombre().trim());
                    element.put("direccion", ConsultaRUCCWS.getDomicilioLegal(ruc));
        		}
        	} else { // Si la Consulta RUC no devuelve nada
            	HashUtil<String, Object> usuarioDetail = cargarDatosUsuarioxRUC(ruc);
            	if (usuarioDetail!=null) {
            		if (arrFiltros.length < 2 || !"1".equals(arrFiltros[1].split("[=]")[1])) {
                        element.put("nombre", usuarioDetail.getString("NOMBRE"));
            		} else {
            			element.put("numeroDocumento", ruc);
                        element.put("nombre", usuarioDetail.getString("NOMBRE"));
                        element.put("direccion", usuarioDetail.getString("DIRECCION"));
            		}
            	} else { // Si NO obtenemos datos, debemos limpiar el nombre ya que no se ha obtenido nada
	        		if (filtros.get("subPantalla") == null || !"1".equals(filtros.get("subPantalla").toString())) {
	                    element.put("nombre", "");
	                    element.put("direccion", "");
	        		} else {
	                    element.put("nombre", "");
	                    element.put("direccion", "");
	        		}
                }
        	}

        } catch(Exception e) {
        	logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return element;
    }

    @SuppressWarnings("unchecked")
    public static OptionList cargarListaEstablecimientosAnexosYDomicilioLegal(HashUtil filtros) {
        String [] split = filtros.getString("filter").split("[|]");
        String ruc = split[0].split("[=]")[1];
        
        if (ruc.equals("")) return null;
        logger.debug("cargarListaEstablecimientosAnexosYDomicilioLegal");
        try {
        	String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(ruc);
            BeanSpr [] establecimientosAnexos = ConsultaRUCCWS.getEstablecimientosAnexos(ruc);
            OptionList element = new OptionList();
            
            if (domicilioLegal!=null) {
            	Option op = new Option("-1", domicilioLegal);
                element.add(op);
            }
            
            if (establecimientosAnexos!=null) {
                for (BeanSpr ea : establecimientosAnexos) {
                    if (ea!=null && ea.getDireccion()!=null) {
                        Option op = new Option(""+ea.getSpr_correl(), ea.getDireccion().trim());
                        element.add(op);
                    }
                }
                
                ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
                HttpServletRequest request = sra.getRequest();
                
                request.getSession().setAttribute("listaEstablecimientosAnexosYDomicilioLegalConsultaRUC", establecimientosAnexos);
                
                return element;
            }
            
        } catch(Exception e) {
            logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return null;
    }

    /*
    @SuppressWarnings("unchecked")
    public static OptionList cargarListaRepresentantesLegales(HashUtil filtros) {
        String [] split = filtros.getString("filter").split("[|]");
        String ruc = split[0].split("[=]")[1];
        
        if (ruc.equals("")) return null;
        
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        IbatisService ibatisService = (IbatisService)ctx.getBean("ibatisService");
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        
        try {
            BeanRso [] representantesLegales = ConsultaRUCCWS.getRepLegales(ruc);
            OptionList element = new OptionList();
            
            HashUtil<String, String> rlColocados = new HashUtil<String, String>();
            if (representantesLegales!=null) {
                for (BeanRso rl : representantesLegales) {
                    if (rl!=null) {
                        String nroDoc = rl.getRso_nrodoc();
                        if (nroDoc!=null) nroDoc = nroDoc.trim();
                        
                        String td = ""+rl.getRso_docide()+"|"+nroDoc;
                        if (!rlColocados.containsKey(td)) {
                            rlColocados.put(td, td);
                            
                            String nombreRepresentanteLegal = rl.getRso_nombre();
                            if (nombreRepresentanteLegal!=null) nombreRepresentanteLegal = nombreRepresentanteLegal.trim();
                            
                            // Obtener el tipo de documento VUCE en base al tipo de documento de SUNAT
                            filter.clear();
                            filter.put("tipoDocumentoSUNAT", rl.getRso_docide());
                            Integer tipoDoc = (Integer)ibatisService.loadObject("comun.devuelveTipoDocumentoVUCE", filter);
                            
                            // Si no existe un tipo de documento VUCE equivalente entonces no considerar este Representante Legal
                            if (tipoDoc==null) continue;
                            
                            Option op = new Option(""+tipoDoc+"|"+nroDoc, nombreRepresentanteLegal);
                            element.add(op);
                        }
                    }
                }
                
                return element;
            }
            
        } catch(Exception e) {
            logger.error("ERROR al intentar acceder a la Consulta RUC de SUNAT", e);
        }
        return null;
    }
    */

}
