package pe.gob.mincetur.vuce.co.web.util;

import org.jlis.core.config.LabelConfig;
import org.jlis.core.util.Util;
import org.jlis.web.bean.ButtonCell;
import org.jlis.web.bean.Cell;
import org.jlis.web.view.jsp.tag.grid.util.GenericCellFormatter;
import org.jlis.web.view.jsp.tag.grid.validator.CellValidator;

public class NotificacionCertificadoCellValidator implements CellValidator {

    public void validate(GenericCellFormatter cellControl, Cell cell, ButtonCell buttonCell) {
        if (cell.getValue()!=null && !cell.getValue().toString().equals("")) {
            String nroContador = cell.getValue().toString();
            
            String tipo = nroContador.split("[=]")[0];
            int contador = Util.integerValueOf(nroContador.split("[=]")[1]);
            if (tipo.equals("NS")) {
                buttonCell.setImage("/co/imagenes/tip.gif");
                buttonCell.setAlt(LabelConfig.getLabelConfig().getProperty("co.label.certificados.informacionNotificacionesPendientes", new Object [] {""+contador}));
            }
        } else {
            buttonCell.setImage(null);
            buttonCell.setAlt("");
        }
    }
}
