package pe.gob.mincetur.vuce.co.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class LogoutController extends MultiActionController {

    Logger logger = Logger.getLogger(Constantes.LOGGER_CONTROLLER);

    private String logout;

    public ModelAndView logoutTest(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Performing logout....");
        //String eval = request.getParameter("eval");
        request.getSession().invalidate();
        request.setAttribute("eval", "1");
        return new ModelAndView(logout);
    }
    
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Performing logout....");

        UsuarioCO usuario = (UsuarioCO) WebUtils.getSessionAttribute(request, Constantes.USUARIO);

        String logoutURL = null;
        if (ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL.equals(usuario.getTipoOrigen())) {
            logoutURL = ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLSOL");
        } else if (ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT.equals(usuario.getTipoOrigen())) {
            logoutURL = ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLEXT");
        } else if (ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_DNI.equals(usuario.getTipoOrigen())) {
            logoutURL = ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLDNI");
        }

        // Si no existe esta variable en Sesion entonces la autenticacion vino de VUCE

        request.getSession().invalidate();
        try {
            response.sendRedirect(logoutURL);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

}
