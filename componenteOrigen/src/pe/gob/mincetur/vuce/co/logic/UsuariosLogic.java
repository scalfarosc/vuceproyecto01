package pe.gob.mincetur.vuce.co.logic;

import java.util.Map;

import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.EntidadCertificadora;
import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Usuario;

public interface UsuariosLogic {
	
    public void testGrabacionUsuarioContextIdentifier(UsuarioCO usuario);
	
    public Usuario convertMapToUsuario(Map<String, Object> map);
	
	public RepresentanteLegal convertMapToRepresentanteLegal(Map<String, Object> map);
	
	public Usuario getUsuarioById(Integer usuarioId);
	
	public MessageList crearUsuario(Usuario usuario , Usuario empresa , Table representantesLegales, UsuarioCO usuarioVUCE);
	
	public MessageList actualizarUsuario(Usuario usuario, Usuario empresa , Table representantesLegales, UsuarioCO usuarioCO);
	
	public int existeUsuarioSOL(String usuarioSol , String numDoc);
	
	public int existeUsuarioExtranet(String login);
	
	public int existeUsuarioDNI(String login);
	
	public boolean existeEmpresa(String numDoc);
	
	public Usuario getUsuarioEmpresaById(int idUsuario);
	
	public boolean usuarioTieneTramites(UsuarioCO usuario);
	
	public Table obtenerTablaRepresentantesLegalesInicial(String numeroDocumento);
	
	public void actualizarRolesUsuario(UsuarioCO usuario);
	
	public void actualizarDatosRUCUsuario(UsuarioCO usuarioVUCE, HashUtil<String, Object> datosFichaRUC);
	
	public MessageList actualizarRepresentantesLegalesEmpresa(Usuario empresa, Table representantesLegales);
	
	public EntidadCertificadora obtenerEntidadCertificadora(String cadena);
	
	public UsuarioCO getUsuarioCOById(int idUsuario);

	public HashUtil<String, Object> obtenerDatosFichaRUC(UsuarioCO usuario);
	
	public HashUtil cargarRolesUsuario(int idUsuario);
	
}
