package pe.gob.mincetur.vuce.co.logic.solicitudCalificacion;

import java.util.Map;

import org.jlis.core.list.MessageList;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.AdjuntoRequeridoSC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCAcuerdo;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCEA;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCEARepresentante;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCMaterial;

public interface SCELogic {
    
    public MessageList creaSCE(Long ordenId, Integer mto, Integer entidadId, Long usuario, Long solicitante, Long representante, String cargo);
    
    public MessageList transmiteSC(Long ordenId, int mto);
    
    public SCEA convertMapToSCE(Map<String, Object> map);
    
    public MessageList updateSCEA(SCEA scea);
    
    public SCEA getSCEAById(Long ordenId, Integer mto);
    
    public UsuarioFormato getUsuarioFormatoById(long ordenId, Integer mto);
    
    public Solicitud getSolicitudById(long ordenId, Integer mto);
    
    public Solicitante getSolicitanteDetail(long ordenId, int mto, int usuarioFormatoTipo);
    
    public SCEARepresentante convertMapToSCMaterial(Map<String, Object> map);
    
    public SCEARepresentante getRepresentanteById(Long expautId, Integer secuenciaRepresentante);
    
    public MessageList updateCargoDeclarante(long solicitante,long ordenId, int mto, String cargo);
    
    public AdjuntoRequeridoSC getAdjuntoRequeridoById(Integer adjuntoRequeridoSC);
    
    public MessageList uploadFile(Long ordenId, Integer mto, Integer adjuntoRequerido, String nombre, byte [] bytes);
     
    public Integer getAdjuntoRequeridoCount(long ordenId,int mto);
     
    public SC registrarBorradorDr(Long suceId) throws Exception;
     
    public MessageList deleteBorradorDr(Long borradorDrId, Long numSuce, Long formatoDrId, Long suceId);

    public Solicitante getSolicitanteDRDetail(long drId, int sdr, int usuarioFormatoTipo);
    
    public Solicitante getSolicitanteDRBorradorDetail(long calificacionDrId, int usuarioFormatoTipo);
    
    public SC getSCDRById(long calificacionDrId);
    
    public SCMaterial getSCMaterialDRById(long calificacionDrId, Integer secuenciaCalifXDJ);
    
    public SC convertMapToSCDR(Map<String, Object> map);
    
    public MessageList updateSCDR(SC sc) throws Exception;
    
    public SCAcuerdo getSCAcuerdoDRById(long calificacionDrId, Integer secuenciaAcuerdo);
    
    public SCAcuerdo convertMapToSCAcuerdoDR(Map<String, Object> map);
    
    public MessageList updateSCAcuerdoDR (SCAcuerdo obj);
    
    public MessageList transmiteResolutor(long calificacionDrBorradorId);
    
}
