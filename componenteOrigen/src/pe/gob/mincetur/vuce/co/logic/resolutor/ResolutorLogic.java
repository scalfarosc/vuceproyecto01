package pe.gob.mincetur.vuce.co.logic.resolutor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;
import org.springframework.web.servlet.ModelAndView;

import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.AdjuntoCertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;


/**
 * Interfaz de l�gica de negocio de Certificados de Origen
 * @author Ricardo Montes
 */

public interface ResolutorLogic {

	public MessageList designaEvaluador(Map<String,Object> parametros);

	public MessageList aceptaDesignacionTCE(CertificadoOrigen co);

    public MessageList rechazaDesignacionTCE(CertificadoOrigen co);

	public MessageList evaluadorDJCalifica(DeclaracionJurada dj);
	
	public MessageList evaluadorDJCalificaApruebaTransmite(DeclaracionJurada dj);

	public MessageList evaluadorDJNoCalifica(DeclaracionJurada dj);
	
	public MessageList evaluadorDJNoCalificaDeniegaTransmite(DeclaracionJurada dj);

	public MessageList registrarBorradorDr(CertificadoOrigenDR coDR);
	
	public HashUtil<String, Object> confirmarBorradorDr(Long coDrId, Long suceId);

	public MessageList registrarNotificacionSubsanacionSuce(HashUtil<String, String> datos);

	public MessageList enviarNotificacionSubsanacionSuce(HashUtil<String, String> datos);

	public MessageList eliminarNotificacionSubsanacionSuce(HashUtil<String, String> datos);

	public MessageList cargarArchivoNotificacion(int idOrden, int idMto, String nombre, Integer notificacionId, byte[] bytes);

	public MessageList generarBorradorDr(Long suceId, String tipoDr);

	public MessageList eliminarBorradorDr(Long coDrId, Long suceId);

	public MessageList actualizarObsBorradorDr(Long mct001DrId, String observacionEvaluador, String formato, Integer idAcuerdo);

	public CertificadoOrigenDR getCODrResolutorById(Map filter);

	public MessageList cargarAdjuntoCODR(AdjuntoCertificadoOrigenDR param);

    public DR obtenerDRById(Long drId, Integer sdr);

	public MessageList aprobarModificacionSubsanacion(HashUtil<String, String> datos);

	public MessageList rechazarModificacionSubsanacion(HashUtil<String, String> datos, byte [] bytes, String nombreArchivo);

	public MessageList cerrarSuce(Long suceId, Long drId, Long sdr, String tipoDr, String formato);

    public MessageList denegarSolicitud(Map<String, Object> filtros);

    public MessageList actualizarDjXEvaluador(Long djId, String partidaSegunAcuerdo, Integer secuenciaNorma, Integer secuenciaOrigen);

    public MessageList confirmarFinDJEval(Long coId, Long suceId);

    public String obtenerTipoDr(Long drId, Integer sdr);

    public String obtenerTipoDrBorrador(Long borradorDrId);

    public boolean drEsRectificacion(Long drId, Integer sdr);

    public DR getDRBorradorById(Long drBorradorId);

	public CertificadoOrigenDR getCODrRectificacionById(Long drId, String formato);

    public MessageList registrarNotificacionSubsanacionOrden(HashUtil<String, String> datos);

    public MessageList enviarNotificacionSubsanacionOrden(HashUtil<String, String> datos);

    public MessageList eliminarNotificacionSubsanacionOrden(HashUtil<String, String> datos);

	public MessageList cargarDatosFirmaAdjuntoCODR(HashUtil<String, Object> filter);

	public String habilitarBtnFirma(Long suceId, Long drId, Long sdr);

    public int numDJsRechazadas(Map<String,Object> parametros) throws Exception;

    public String mostrarBtnFinalizacion(Map<String,Object> parametros);

}