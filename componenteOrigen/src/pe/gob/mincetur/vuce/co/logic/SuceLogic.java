package pe.gob.mincetur.vuce.co.logic;

import java.util.List;

import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;

public interface SuceLogic {

    public Suce getSuceById(int suce);

    public Suce getSuceByIdAndUsuarioId(long suce, long usuarioId, long rol);

    public ModificacionSuce loadModificacionSuceByIdMensajeId(int suce, int modificacionSuce, int mensajeId);

    public ModificacionDR loadModificacionDrById(long drId, int sdr, int modificacionDrId, int mensajeId);

    public HashUtil<String, Object> creaSubsanacion(int idSuce, String tipo, String mensaje, List<String> notificaciones);

    public HashUtil<String, Object> crearModificacionDr(long drId, int sdr, String mensaje, String esSubsanacion);

    public MessageList transmiteModificacionDr(ModificacionDR modificacionDr);

    public MessageList actualizaModificacionDr(int mensajeId, String mensaje);

    public void eliminaModificacionDr(long drId, int sdr, int modificacionDrId, int mensajeId);

    public MessageList transmiteSubsanacion(ModificacionSuce modificacionSuce);

    public MessageList actualizaSubsanacion(int modifSuceId, int mensajeId, String tipo, String mensaje, List<String> notificaciones);

    public MessageList eliminaSubsanacion(int suce, int modifsuce);

    public MessageList aprobarModificacionDr(long drId, int sdr, int modificacionDrId);

    public MessageList rechazarModificacionDr(long drId, int sdr, int modificacionDrId, String mensaje);

    public HashUtil<String, Object> registrarRectificacionDrEval(long drId, int sdr, String mensaje);

    public HashUtil<String, Object> eliminarRectificacionDrEval(long drId, int sdr, int mensajeId);

    public HashUtil<String, Object> modificarRectificacionDrEval(int mensajeId, String mensaje);

    public HashUtil<String, Object> confirmarRectificacionDrEval(long drId, int sdr);

    public Suce getSuceModificacionById(int suceId, int mto);

    public Suce getSuceByNumeroAndUsuarioId(int numSuce, int usuarioId, int rol);

    public Suce loadSuceByNumero(int numSuce);

    public void desisteSuce(int idSuce);

    public MessageList aceptarDesestimientoSUCE(HashUtil<String, String> datos);

    public MessageList rechazarDesestimientoSUCE(HashUtil<String, String> datos);

    public String suceFlgPendienteCalif(Integer suceId);

}