package pe.gob.mincetur.vuce.co.logic;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.AdjuntoRequerido;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;

public interface FormatoLogic {

    public MessageList transmiteOrden(int orden, int mto, boolean primeraTransmision);

	public HashMap<String, String> getUbigeoDetail(String ubigeo);

	public HashMap<String, String> getUbigeoDetailByDistrito(int distritoId);

	public Solicitante getUsuarioDetail(UsuarioCO usuario);

    public Solicitante getUsuarioDetail(long orden, int mto);

	public Solicitante getEmpresaDetail(UsuarioCO usuario);

	public Solicitante getSolicitanteDetail(UsuarioCO usuario);

    public Solicitante getSolicitanteDetail(long orden, int mto);

	public String loadNombreUsuario(String numeroDocumento);

    public String getPartidaById(Long partidaArancelaria);

    //public String getDescripcionUnidadMedidaFisicaById(Integer unidadMedidaFisicaId); 20140617_JMCA BUG 119

    public Formato getFormatoByTupa(int idFormato, int idTupa);

    public RepresentanteLegal getRepresentanteDetail(long orden, int mto);

    public JasperPrint ejecutarReporteConConexionBD(JasperReport report, Map parameters);

	public AdjuntoRequerido getRequeridoById(int idFormato, int idRequerido);

	public MessageList uploadFile(int idOrden, int idMto, int formato, int adjunto, int adjuntoTipo, String nombre, Integer mensajeId, String esDetalle, byte [] bytes);

	public String obtenerAyuda(Map<String, Object> filtros);

	public MessageList cargarArchivoDR(Integer mensajeId, String nombre, byte[] bytes) throws Exception;

	public MessageList cargarArchivoSubsanacion(Integer mensajeId, String nombre, byte[] bytes);

	public String permiteCrearFactura(Long ordenId, Long codigoCertificado);

	public String permiteCrearProductor(Long calificacionUoId);

	public String permiteConfirmarFinEval(Long coId, Long ordenId, Integer mto);

    public Long getFormatoDrId(int drId, int sdr);

    public int cuentaAdjuntosRequeridos(Orden orden);

    public int cuentaNotificacionesPendientes(Long suceId);

    public int cuentaAdjuntosRequeridosDJ(long djId); //, String tipoRolDj);

    public int cuentaNotificacionesSolicitudPendientes(Long ordenId, int mto);

    public MessageList insertAdjuntoFactura(HashUtil<String, Object> filter) throws Exception;

    public MessageList eliminarAdjuntoFactura(Long adjuntoId, Long coId, String formato) throws Exception;

    public Table obtenerJerarquiaTramite(Integer ordenId);

	public String permiteCrearSolicitudRectif(Long suceId);

	public String permiteApoderamientoXFrmt(Integer acuerdoInternacionalId);

    public int cuentaAdjuntosFacturaRequeridos(long coId, String formato);

	public String acuerdoConValidacionProductor(Integer codigoAcuerdo);

    public MessageList insertAdjuntoProductor(HashUtil<String, Object> filter) throws Exception;

    public MessageList eliminarAdjuntoProductor(HashUtil<String, Object> filter) throws Exception;

	public Message validacionPreTransmision(HashUtil<String, Object> filter);
	
	public Message validacionPreFormato(int usuarioId, Formato formato); //20140526 - JMC - BUG 1.PersonaNatural
	
	public String es_version_antigua(Long dr, Integer sdr);
	
	public String es_dj_version_diferente(Long dr, Integer sdr);
	
	public String es_reemplazo_version_diferente(Long dr, Integer sdr);

}
