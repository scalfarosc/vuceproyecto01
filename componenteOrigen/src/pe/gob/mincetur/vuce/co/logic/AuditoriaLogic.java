package pe.gob.mincetur.vuce.co.logic;

import pe.gob.mincetur.vuce.co.domain.Auditoria;

public interface AuditoriaLogic {
	
	public void grabarInicioSesion(Auditoria auditoria);
	
	public void grabarInicioSesionContingencia(Auditoria auditoria);
	
	public void grabarFinSesionContingencia(Auditoria auditoria);
	
	public void grabarAccesoSolicitud(Auditoria auditoria);
	
	public void grabarAccesoSuce(Auditoria auditoria);
	
	public void grabarAccesoDr(Auditoria auditoria);	

}
