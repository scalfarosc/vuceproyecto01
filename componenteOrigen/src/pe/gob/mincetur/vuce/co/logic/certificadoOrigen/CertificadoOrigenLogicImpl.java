package pe.gob.mincetur.vuce.co.logic.certificadoOrigen;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.list.OptionList;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Table;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.dao.DBSessionContext;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.AdjuntoRequeridoDJMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;
import pe.gob.mincetur.vuce.co.domain.ebxml.MCT001EBXML;
import pe.gob.mincetur.vuce.co.domain.ebxml.MCT005EBXML;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.logic.FormatoLogicImpl;
import pe.gob.mincetur.vuce.co.logic.OrdenLogic;
import pe.gob.mincetur.vuce.co.logic.SuceLogic;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.service.certificadoOrigen.CertificadoOrigenService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.stream.StreamUtil;
import pe.gob.mincetur.vuce.co.util.xml.SchemaValidationErrorHandler;

/**
*Objeto :	CertificadoOrigenDAOImpl
*Descripcion :	Clase implementadora de l�gica de negocio de Certificados de Origen.
*Fecha de Creacion :	
*Autor :	E. Osc�tegui - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		05/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ
*/
public class CertificadoOrigenLogicImpl extends FormatoLogicImpl implements CertificadoOrigenLogic {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

    private CertificadoOrigenService certificadoOrigenService;
    
    private OrdenLogic ordenLogic;
    
    private SuceLogic suceLogic;

    public void setOrdenLogic(OrdenLogic ordenLogic) {
		this.ordenLogic = ordenLogic;
	}
    
    public void setSuceLogic(SuceLogic suceLogic) {
		this.suceLogic = suceLogic;
	}
    

    /**
     * Obtiene un Certificado de Origen por su Id
     */
    @SuppressWarnings("unchecked")
    public CertificadoOrigen getCertificadoOrigenById(long coId, String formato) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("coId", coId);
        filter.put("formato", formato);
        CertificadoOrigen certificadoOrigen = null;
        try {
            certificadoOrigen = certificadoOrigenService.getCertificadoOrigenById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCertificadoOrigenById", e);
        }
        return certificadoOrigen;
    }

    /**
     * Obtiene una factura espec�fica de certificado de origen
     */
    @SuppressWarnings("unchecked")
    public COFactura getCOFacturaById(long coId, int secuencia, String formato) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("coId", coId);
        filter.put("secuencia", secuencia);
        filter.put("formato", formato);
        COFactura factura = null;
        try {
            factura = certificadoOrigenService.getCOFacturaById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCOFacturaById", e);
        }
        return factura;
    }

    /**
     * Obtiene una mercanc�a espec�fica de certificado de origen
     */
    @SuppressWarnings("unchecked")
    public CertificadoOrigenMercancia getCOMercanciaById(long coId, int secuencia, String formato) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("coId", coId);
        filter.put("secuencia", secuencia);
        filter.put("formato", formato);
        CertificadoOrigenMercancia mercancia = null;
        try {
            mercancia = certificadoOrigenService.getCOMercanciaById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCOMercanciaById", e);
        }
        return mercancia;
    }

    /**
     * Conversor de datos de request a objeto Certificado Origen
     */
    public CertificadoOrigen convertMapToCertificadoOrigen(Map<String, Object> map) {
        CertificadoOrigen certificadoOrigen = new CertificadoOrigen();

        certificadoOrigen.setDireccionAdicional(map.get("direccionAdicional")!=null?map.get("direccionAdicional").toString():null);
        certificadoOrigen.setImportadorNombre(map.get("importadorNombre")!=null?map.get("importadorNombre").toString():null);
        certificadoOrigen.setImportadorDireccion(map.get("importadorDireccion")!=null?map.get("importadorDireccion").toString():null);
        certificadoOrigen.setImportadorPais(!"".equals(map.get("importadorPais")) ? Integer.valueOf(map.get("importadorPais").toString()) : null);
        certificadoOrigen.setImportadorRegistroFiscal(map.get("importadorRegistroFiscal")!=null?map.get("importadorRegistroFiscal").toString():null);
        certificadoOrigen.setImportadorTelefono(map.get("importadorTelefono")!=null?map.get("importadorTelefono").toString():null);
        certificadoOrigen.setImportadorFax(map.get("importadorFax")!=null?map.get("importadorFax").toString():null);        
        certificadoOrigen.setImportadorEmail(map.get("importadorEmail")!=null?map.get("importadorEmail").toString():null);        
        certificadoOrigen.setImportadorCiudad(map.get("importadorCiudad")!=null?map.get("importadorCiudad").toString():null);// JMC 06/07/2017 Version 2 Alianza Pacifico

        try {
            certificadoOrigen.setFechaPartidaTransporte(!"".equals(map.get("fechaPartidaTransporte")) ? Util.getDateObject(map.get("fechaPartidaTransporte").toString()) : null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        certificadoOrigen.setNumeroTransporte(map.get("numeroTransporte").toString());
        certificadoOrigen.setModoTransCargaId(!"".equals(map.get("modoTransCargaId"))?Integer.valueOf(map.get("modoTransCargaId").toString()):null);
        certificadoOrigen.setPuertoCargaId(!"".equals(map.get("puertoCargaId")) ? Long.valueOf(map.get("puertoCargaId").toString()) : null);
        certificadoOrigen.setModoTransDescargaId(!"".equals(map.get("modoTransDescargaId"))?Integer.valueOf(map.get("modoTransDescargaId").toString()):null);
        certificadoOrigen.setPuertoDescargaId(!"".equals(map.get("puertoDescargaId")) ? Long.valueOf(map.get("puertoDescargaId").toString()) : null);
        certificadoOrigen.setIncluyeProductor(map.get("incluyeProductor") != null ? map.get("incluyeProductor").toString() : "N");
        certificadoOrigen.setEmitidoPosteriori(map.get("emitidoPosteriori") != null ? map.get("emitidoPosteriori").toString() : "N");
        certificadoOrigen.setEsConfidencial(map.get("esConfidencial") != null ? map.get("esConfidencial").toString() : "N");
        certificadoOrigen.setObservacion(map.get("observacion").toString());
        certificadoOrigen.setPaisDescargaId(!"".equals(map.get("paisDescargaId"))?Integer.valueOf(map.get("paisDescargaId").toString()):null);
        certificadoOrigen.setSecuenciaPaisImportador(!"".equals(map.get("secuenciaPaisImportador"))?Integer.valueOf(map.get("secuenciaPaisImportador").toString()):null);
        certificadoOrigen.setSecuenciaPaisDescarga(!"".equals(map.get("secuenciaPaisDescarga"))?Integer.valueOf(map.get("secuenciaPaisDescarga").toString()):null);
        
        return certificadoOrigen;
    }

    public DeclaracionJurada convertMapToDeclaracionJurada(Map<String, Object> map){
        DeclaracionJurada dj = new DeclaracionJurada();

        dj.setDenominacion(map.get("denominacion").toString());
        dj.setCaracteristica(map.get("caracteristica").toString());
        dj.setPartidaArancelaria(new Long(map.get("partidaArancelaria").toString()));
        dj.setUmFisicaId(new Integer(map.get("umFisicaId").toString()));
        dj.setCantidadUmRefer(map.get("cantidadUmRefer").toString());
        
        if(map.get("cantidad")!=null){
        	dj.setCantidad(new BigDecimal(map.get("cantidad").toString()));	
        }
        
        if(!"".equals(map.get("valorUs"))){
        	BigDecimal valorUs = new BigDecimal(map.get("valorUs").toString());        
            dj.setValorUs(valorUs);	
        }
                
        
        BigDecimal pesoNetoMercancia = null;
        String strPesoNetoMercancia = map.get("pesoNetoMercancia")!=null?map.get("pesoNetoMercancia").toString():"";        
        if (!ConstantesCO.OPCION_NO.equals(strPesoNetoMercancia) && !"".equals(strPesoNetoMercancia) && strPesoNetoMercancia!=null) {
        	pesoNetoMercancia = new BigDecimal(strPesoNetoMercancia);        	
        }
        dj.setPesoNetoMercancia(pesoNetoMercancia);
        
        BigDecimal valorUsFabrica = null;
        String strValorUsFabrica = map.get("valorUsFabrica").toString();        
        if (!ConstantesCO.OPCION_NO.equals(strValorUsFabrica) && !"".equals(strValorUsFabrica)) {
        	valorUsFabrica = new BigDecimal(strValorUsFabrica);        	
        }
        dj.setValorUsFabrica(valorUsFabrica);

        /*BigDecimal prctjSegunCriterio = null;
        String strPrctjSegunCriterio = datos.getString("DJ.porcentajeSegunCriterio");
        if (!ConstantesCO.OPCION_NO.equals(strPrctjSegunCriterio) && !"".equals(strPrctjSegunCriterio)) {
        	prctjSegunCriterio = new BigDecimal(strPrctjSegunCriterio);
        }*/
        
        /*if (map.get("cumpleTotalmenteObtenido") == null) {
            dj.setCumpleTotalmenteObtenido("N");
        } else {
            dj.setCumpleTotalmenteObtenido(map.get("cumpleTotalmenteObtenido").toString());
        }

        if (map.get("cumpleCriterioCambioClasif") == null) {
            dj.setCumpleCriterioCambioClasif("N");
        } else {
            dj.setCumpleCriterioCambioClasif(map.get("cumpleCriterioCambioClasif").toString());
        }

        if (map.get("cumpleOtroCriterio") == null) {
            dj.setCumpleOtroCriterio("N");
        } else {
            dj.setCumpleOtroCriterio(map.get("cumpleOtroCriterio").toString());
        }

        if (map.get("partidaSegunAcuerdo") != null){
            dj.setPartidaSegunAcuerdo(map.get("partidaSegunAcuerdo").toString());
        }

        if (map.get("secuenciaNorma") != null && !"".equals(map.get("secuenciaNorma").toString().trim())){
            dj.setSecuenciaNorma(new Integer(map.get("secuenciaNorma").toString()));
        }

        if (map.get("secuenciaOrigen") != null && !"".equals(map.get("secuenciaOrigen").toString().trim())){
            dj.setSecuenciaOrigen(new Integer(map.get("secuenciaOrigen").toString()));
        }

        if (map.get("criterioOrigenCertificado") != null && !"".equals(map.get("criterioOrigenCertificado").toString().trim())){
            dj.setCriterioOrigenCertificado(map.get("criterioOrigenCertificado").toString());
        }

        dj.setTipoRolDj(Util.integerValueOf(map.get("tipoRolDj").toString()));
        */

        if (map.get("aceptacion") == null) {
            dj.setAceptacion("N");
        } else {
            dj.setAceptacion(map.get("aceptacion").toString());
        }

        /*if (map.get("valorUsFabrica") != null && !"".equals(map.get("valorUsFabrica").toString().trim())) {
            dj.setValorUsFabrica(new BigDecimal(map.get("valorUsFabrica").toString()));
        }*/

        return dj;
    }

    /**
     * Conversor de datos de request a objeto Mercancia del Certificado Origen
     */
    public CertificadoOrigenMercancia convertMapToCOMercancia(Map<String, Object> map) {
        CertificadoOrigenMercancia coMercancia = new CertificadoOrigenMercancia();

        //coMercancia.setCoId(new Long(map.get("coId").toString()));

        if (map.get("secuencia") != null) {
            coMercancia.setSecuenciaMercancia(new Integer(map.get("secuencia").toString()));
        }

        if (map.get("itemCoMercancia") != null && !"".equals(map.get("itemCoMercancia").toString())) {
            coMercancia.setItemCoMercancia(new Integer(map.get("itemCoMercancia").toString()));
        }

        if (map.get("djId") != null) {
            coMercancia.setDjId(Util.longValueOf((map.get("djId").toString())));
        }

        coMercancia.setDescripcion(map.get("descripcion").toString());
        coMercancia.setUmFisicaId(map.get("umFisicaId")!=null && !map.get("umFisicaId").equals("") ? new Integer(map.get("umFisicaId").toString()) : null);

        coMercancia.setPeso(map.get("peso")!=null && !map.get("peso").equals("") ? new BigDecimal(map.get("peso").toString()) : null);
        coMercancia.setCantidad(map.get("cantidad")!=null && !map.get("cantidad").equals("") ? new BigDecimal(map.get("cantidad").toString()) : null);
        coMercancia.setTipoEnvase(map.get("tipoEnvase")!=null && !map.get("tipoEnvase").equals("") ? new Integer(map.get("tipoEnvase").toString()) : null);
        coMercancia.setDescripcionTipoEnvase(map.get("descripcionTipoEnvase")!=null && !map.get("descripcionTipoEnvase").equals("") ? map.get("descripcionTipoEnvase").toString() : null);
        coMercancia.setSecuenciaFactura(map.get("secuenciaFactura")!=null && !map.get("secuenciaFactura").equals("")  ? Integer.valueOf(map.get("secuenciaFactura").toString()) : null);

        if (coMercancia.getEstadoRegistro() == null) {
            coMercancia.setEstadoRegistro("P");
        }

        if (map.get("valorFacturadoUs") != null && !"".equals(map.get("valorFacturadoUs").toString())) {
            coMercancia.setValorFacturadoUs(new BigDecimal(map.get("valorFacturadoUs").toString()));
        }

        coMercancia.setMarcasNumeros(map.get("marcasNumeros").toString());
        coMercancia.setTipoBulto(map.get("tipoBulto").toString());
        if (map.get("cantidadBulto") != null && !"".equals(map.get("cantidadBulto").toString())) {
            coMercancia.setCantidadBulto(new Integer(map.get("cantidadBulto").toString()));
        }
        
        if (map.get("umFisicaSegunFactura") != null && !"".equals(map.get("umFisicaSegunFactura").toString())) {            
            coMercancia.setUmFisicaSegunFactura(map.get("umFisicaSegunFactura").toString());
        }
        
        coMercancia.setEsMercanciaGranel(map.get("esMercanciaGranel") != null ? map.get("esMercanciaGranel").toString() : "N");
        
        coMercancia.setPartidaSegunAcuerdo(map.get("partidaSegunAcuerdo")!=null ? map.get("partidaSegunAcuerdo").toString() : null);
        
        coMercancia.setSecuenciaNorma(Util.integerValueOf(map.get("secuenciaNorma")));
        
        coMercancia.setSecuenciaOrigen(Util.integerValueOf(map.get("secuenciaOrigen")));

        return coMercancia;
    }

    /**
     * Actualiza la tabla principal de Certificado de Origen
     */
    public MessageList updateCertificadoOrigen(CertificadoOrigen certificadoOrigen) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateCertificadoOrigen(certificadoOrigen);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCertificadoOrigen", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Carga adjuntos de Certificado de Origen
     */
    public MessageList cargarAdjuntoCertificadoOrigen(AdjuntoCertificadoOrigen adjunto) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.cargarAdjuntoCertificadoOrigen(adjunto);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.cargarArchivoCertificadoOrigen", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    @SuppressWarnings("unchecked")
    /**
     * Obtiene informaci�n de una Mercanc�a
     */
    public CertificadoOrigenMercancia getCertificadoOrigenMercanciaById(long id, int secuencia, String formato) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("coId", id);
        filter.put("secuencia", secuencia);
        filter.put("formato", formato);
        CertificadoOrigenMercancia certificadoOrigenMercancia = null;
        try {
            certificadoOrigenMercancia = certificadoOrigenService.getCertificadoOrigenMercanciaById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCertificadoOrigenMercanciaById", e);
        }
        return certificadoOrigenMercancia;
    }

    /**
     * Carga los datos de una Mercanc�a en base al mapa del request
     */
    public CertificadoOrigenMercancia convertMapToCertificadoOrigenMercancia(Map<String, Object> map) {
        CertificadoOrigenMercancia certificadoOrigenMercancia = new CertificadoOrigenMercancia();
        certificadoOrigenMercancia.setCoId(Util.longValueOf(map.get("coId")));
        certificadoOrigenMercancia.setSecuenciaMercancia(Util.integerValueOf(map.get("secuenciaMercancia")));
        certificadoOrigenMercancia.setItemCoMercancia(Util.integerValueOf(map.get("itemCoMercancia")));
        certificadoOrigenMercancia.setDjId(Util.longValueOf(map.get("djId")));
        certificadoOrigenMercancia.setDescripcion(map.get("descripcion").toString());
        certificadoOrigenMercancia.setUmFisicaId(Util.integerValueOf(map.get("umFisicaId")));
        certificadoOrigenMercancia.setPeso(map.get("peso")!=null && !map.get("peso").equals("")?new BigDecimal((String) map.get("peso")) : null);
        certificadoOrigenMercancia.setCantidad(map.get("cantidad")!=null && !map.get("cantidad").equals("")?new BigDecimal((String) map.get("cantidad")) : null);
        certificadoOrigenMercancia.setTipoEnvase(Util.integerValueOf(map.get("tipoEnvase")));
        certificadoOrigenMercancia.setDescripcionTipoEnvase(map.get("descripcionTipoEnvase").toString());
        certificadoOrigenMercancia.setEstadoRegistro(map.get("estadoRegistro").toString());
        certificadoOrigenMercancia.setSecuenciaFactura(Util.integerValueOf(map.get("secuenciaFactura")));
        return certificadoOrigenMercancia;
    }

    /**
     * Registra una Mercanc�a con Dj Existente
     */
    public MessageList insertCertificadoOrigenMercanciaDjExistente(CertificadoOrigenMercancia certificadoOrigenMercancia, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("certificadoOrigenMercancia", certificadoOrigenMercancia);
            filter.put("formato", formato);

            certificadoOrigenService.insertCertificadoOrigenMercanciaDjExistente(filter);

            certificadoOrigenService.updateCertificadoOrigenMercancia(filter);

            messageList.setObject(certificadoOrigenMercancia);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCertificadoOrigenMercanciaDjExistente", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Registra una Mercanc�a con Dj Nueva
     */
    /*public MessageList insertCertificadoOrigenMercanciaDjNueva(CertificadoOrigenMercancia certificadoOrigenMercancia) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertCertificadoOrigenMercanciaDjNueva(certificadoOrigenMercancia);
            messageList.setObject(certificadoOrigenMercancia);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCertificadoOrigenMercanciaDjNueva", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }*/

    /**
     * Actualiza una Mercanc�a
     */
    public MessageList updateCertificadoOrigenMercancia(CertificadoOrigenMercancia certificadoOrigenMercancia, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("certificadoOrigenMercancia", certificadoOrigenMercancia);
            filter.put("formato", formato);

            certificadoOrigenService.updateCertificadoOrigenMercancia(filter);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCertificadoOrigenMercancia", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Actualiza una DJ
     */
    public MessageList updateMercanciaDeclaracionJurada(DeclaracionJurada dj) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateMercanciaDeclaracionJurada(dj);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateMercanciaDeclaracionJurada", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Elimina una Mercanc�a
     */
    public MessageList deleteCertificadoOrigenMercancia(CertificadoOrigenMercancia certificadoOrigenMercancia, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("certificadoOrigenMercancia", certificadoOrigenMercancia);
            filter.put("formato", formato);

            certificadoOrigenService.deleteCertificadoOrigenMercancia(filter);
            message = new Message("delete.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteCertificadoOrigenMercancia", e);
            message = new ErrorMessage("delete.error", e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Elimina una DJ
     */
    public MessageList deleteMercanciaDeclaracionJurada(DeclaracionJurada dj) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.deleteMercanciaDeclaracionJurada(dj);
            message = new Message("delete.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteMercanciaDeclaracionJurada", e);
            message = new ErrorMessage("delete.error", e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Conversor de datos de request a objeto Factura del Certificado Origen
     */
    public COFactura convertMapToCOFactura(Map<String, Object> map) {
        COFactura coFactura = new COFactura();

        coFactura.setCoId(new Long(map.get("coId").toString()));

        if (map.get("secuencia") != null) {
            coFactura.setSecuencia(new Integer(map.get("secuencia").toString()));
        }
        coFactura.setNumero(map.get("numero").toString());
        try{
            coFactura.setFecha(!"".equals(map.get("fecha")) ? Util.getDateObject(map.get("fecha").toString()) : null);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        coFactura.setIndicadorTercerPais(map.get("indicadorTercerPais") != null ? map.get("indicadorTercerPais").toString() : "N");
        coFactura.setNombreOperadorTercerPais(map.get("nombreOperadorTercerPais") != null?("".equals(map.get("nombreOperadorTercerPais").toString().trim())?null:map.get("nombreOperadorTercerPais").toString()):null);
        coFactura.setDireccionOperadorTercerPais(map.get("direccionOperadorTercerPais") != null?("".equals(map.get("nombreOperadorTercerPais").toString().trim())?null:map.get("direccionOperadorTercerPais").toString()):null);
        coFactura.setTieneFacturaTercerPais(map.get("tieneFacturaTercerPais") != null ? map.get("tieneFacturaTercerPais").toString() : "N");
        // 20150610_RPC: Acta.33: Mostrar Factura Acuerdos AELC/UE (NULL para otros acuerdos)
        coFactura.setMostrarFactura(Util.stringValueOf(map.get("mostrarFactura")));
        coFactura.setPaisOperadorTercerPais(map.get("paisOperadorTercerPais").toString());//JMC 06/07/2017 Version 2 Alianza Pacifico
        return coFactura;
    }

    /**
     * Inserta la tabla Factura de Certificado de Origen
     */
    public MessageList insertCOFactura(COFactura factura, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("factura", factura);
            filter.put("formato", formato);

            certificadoOrigenService.insertCOFactura(filter);
            messageList.setObject(factura);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCOFactura", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Actualiza la tabla Factura de Certificado de Origen
     */
    public MessageList updateCOFactura(COFactura factura, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("factura", factura);
            filter.put("formato", formato);

            certificadoOrigenService.updateCOFactura(filter);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCOFactura", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Elimina la tabla Factura de Certificado de Origen
     */
    public MessageList deleteCOFactura(COFactura factura, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("factura", factura);
            filter.put("formato", formato);

            certificadoOrigenService.deleteCOFactura(filter);
            message = new Message("delete.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteCOFactura", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Registra una Declaraci�n Jurada
     */
    public MessageList insertDeclaracionJurada(DeclaracionJurada declaracionJurada) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertDeclaracionJurada(declaracionJurada);
            messageList.setObject(declaracionJurada);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJurada", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Registra una Declaraci�n Jurada Sin Registro de Mercanc�a
     */
    public MessageList insertDeclaracionJuradaSinMercancia(DeclaracionJurada declaracionJurada) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertDeclaracionJuradaSinMercancia(declaracionJurada);
            messageList.setObject(declaracionJurada);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaSinMercancia", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Actualiza una Declaraci�n Jurada
     */
    public MessageList updateDeclaracionJurada(DeclaracionJurada declaracionJurada) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateDeclaracionJurada(declaracionJurada);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateDeclaracionJurada", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Actualiza el consolidado de una Declaraci�n Jurada
     */
    public MessageList updateDeclaracionJuradaConsolidado(DeclaracionJurada declaracionJurada) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateDeclaracionJuradaConsolidado(declaracionJurada);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateDeclaracionJuradaConsolidado", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Carga adjuntos de Declaraci�n Jurada, y de material de declaraci�n jurada
     */
    public MessageList cargarAdjuntoDeclaracionJurada(AdjuntoCertificadoOrigen adjunto) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.cargarAdjuntoDeclaracionjurada(adjunto);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.cargarArchivoDeclaracionJurada", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    @SuppressWarnings("unchecked")
    /**
     * Obtiene informaci�n de un Material de Declaraci�n Jurada
     */
    public DeclaracionJuradaMaterial getDeclaracionJuradaMaterialById(int id, int secuencia) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("id", id);
        filter.put("secuenciaMaterial", secuencia);
        DeclaracionJuradaMaterial declaracionJuradaMaterial = null;
        try {
            declaracionJuradaMaterial = certificadoOrigenService.getDeclaracionJuradaMaterialById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getDeclaracionJuradaMaterialById", e);
        }
        return declaracionJuradaMaterial;
    }

    /**
     * Carga los datos de un Material de Declaraci�n Jurada en base al mapa del request
     */
    public DeclaracionJuradaMaterial convertMapToDeclaracionJuradaMaterial(Map<String, Object> map) {
        DeclaracionJuradaMaterial declaracionJuradaMaterial = new DeclaracionJuradaMaterial();
        declaracionJuradaMaterial.setDjId(Util.longValueOf(map.get("djId")));
        declaracionJuradaMaterial.setSecuenciaMaterial(Util.integerValueOf(map.get("secuenciaMaterial")));
        declaracionJuradaMaterial.setItemMaterial(Util.integerValueOf(map.get("itemMaterial")));
        declaracionJuradaMaterial.setDescripcion(map.get("descripcion").toString());
        declaracionJuradaMaterial.setPartidaArancelaria(Util.longValueOf(map.get("partidaArancelaria")));
        declaracionJuradaMaterial.setPartidaArancelariaDesc(map.get("partidaArancelariaDesc").toString());
        declaracionJuradaMaterial.setPaisProcedencia(Util.integerValueOf(map.get("paisProcedencia")));
        declaracionJuradaMaterial.setPaisOrigen(map.get("paisOrigen")!=null?Util.integerValueOf(map.get("paisOrigen")):null);
        declaracionJuradaMaterial.setFabricanteNombre(map.get("fabricanteNombre")!=null?map.get("fabricanteNombre").toString():null);
        declaracionJuradaMaterial.setFabricanteDocumentoTipo(map.get("fabricanteDocumentoTipo")!=null?Util.integerValueOf(map.get("fabricanteDocumentoTipo")):null);
        declaracionJuradaMaterial.setFabricanteNumeroDocumento(map.get("fabricanteNumeroDocumento")!=null?map.get("fabricanteNumeroDocumento").toString():null);
        declaracionJuradaMaterial.setUmFisicaId(map.get("umFisicaId")!=null?Util.integerValueOf(map.get("umFisicaId")):null);
        declaracionJuradaMaterial.setCantidad(map.get("cantidad")!=null && !map.get("cantidad").equals("")?new BigDecimal((String) map.get("cantidad")) : null);
        declaracionJuradaMaterial.setValorUs(map.get("valorUs")!=null && !map.get("valorUs").equals("")?new BigDecimal((String) map.get("valorUs")) : null);
        declaracionJuradaMaterial.setPorcentajeValor(map.get("porcentajeValor")!=null && !map.get("porcentajeValor").equals("")?new BigDecimal((String) map.get("porcentajeValor")) : null);
        declaracionJuradaMaterial.setPartidaSegunAcuerdo(map.get("partidaSegunAcuerdo")!=null?map.get("partidaSegunAcuerdo").toString():null);
        declaracionJuradaMaterial.setPesoNeto(( map.get("pesoNeto")!=null && !"".equals(map.get("pesoNeto").toString().trim()) )?new BigDecimal(map.get("pesoNeto").toString()):null);
        //declaracionJuradaMaterial.setIndOrigen(Util.integerValueOf(map.get("indOrigen")));
        declaracionJuradaMaterial.setSecuenciaMaterial(Util.integerValueOf(map.get("secuenciaMaterial")));
        return declaracionJuradaMaterial;
    }

    /**
     * Registra un Material de Declaraci�n Jurada
     */
    public MessageList insertDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertDeclaracionJuradaMaterial(declaracionJuradaMaterial);
            messageList.setObject(declaracionJuradaMaterial);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaMaterialDjNueva", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Actualiza un Material de Declaraci�n Jurada
     */
    public MessageList updateDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateDeclaracionJuradaMaterial(declaracionJuradaMaterial);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateDeclaracionJuradaMaterial", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Elimina un Material de Declaraci�n Jurada
     */
    public MessageList deleteDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.deleteDeclaracionJuradaMaterial(declaracionJuradaMaterial);
            message = new Message("delete.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteDeclaracionJuradaMaterial", e);
            message = new ErrorMessage("delete.error", e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    @SuppressWarnings("unchecked")
    /**
     * Obtiene informaci�n de un Productor de Declaraci�n Jurada
     */
    public DeclaracionJuradaProductor getDeclaracionJuradaProductorById(long id, int secuencia) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("id", id);
        filter.put("secuenciaProductor", secuencia);
        DeclaracionJuradaProductor declaracionJuradaProductor = null;
        try {
            declaracionJuradaProductor = certificadoOrigenService.getDeclaracionJuradaProductorById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getDeclaracionJuradaProductorById", e);
        }
        return declaracionJuradaProductor;
    }

    /**
     * Carga los datos de un Productor de Declaraci�n Jurada en base al mapa del request
     */
    public DeclaracionJuradaProductor convertMapToDeclaracionJuradaProductor(Map<String, Object> map) {
        DeclaracionJuradaProductor declaracionJuradaProductor = new DeclaracionJuradaProductor();
        //declaracionJuradaProductor.setCoId(Util.longValueOf(map.get("coId")));
        if ( map.get("secuenciaProductor") != null && "".equals(map.get("secuenciaProductor").toString()) && "0".equals(map.get("secuenciaProductor").toString())){
        declaracionJuradaProductor.setSecuenciaProductor(Util.integerValueOf(map.get("secuenciaProductor")));
        }

        declaracionJuradaProductor.setDocumentoTipo(Util.integerValueOf(map.get("documentoTipo")));

        if (map.get("numeroDocumento") != null) {
            declaracionJuradaProductor.setNumeroDocumento(map.get("numeroDocumento").toString());
        }

        if (map.get("nombre") != null) {
            declaracionJuradaProductor.setNombre(map.get("nombre").toString());
        }

        if(map.get("direccion") != null) {
            declaracionJuradaProductor.setDireccion(map.get("direccion").toString());
        }
        
        if(map.get("direccionAdicional") != null) {
            declaracionJuradaProductor.setDireccionAdicional(map.get("direccionAdicional").toString());
        }
        

        if ( map.get("telefono") != null ) {
            declaracionJuradaProductor.setTelefono(map.get("telefono").toString());
        }

        if ( map.get("email") != null ) {
            declaracionJuradaProductor.setEmail(map.get("email").toString());
        }
        //20140626_JMC BUG Fax CostaRica
        if ( map.get("fax") != null ) {
            declaracionJuradaProductor.setFax(map.get("fax").toString());
        }

        if(map.get("esValidador") != null) {
            declaracionJuradaProductor.setEsValidador(map.get("esValidador").toString());
        } else {
            declaracionJuradaProductor.setEsValidador(ConstantesCO.OPCION_NO);
        }

        if (map.get("tipoRol") != null) {
            declaracionJuradaProductor.setTipoRolCalificacionNew(Util.integerValueOf(map.get("tipoRol")));
        }
        //JMC 06/07/2017 Version 2 Alianza Pacifico
        if ( map.get("pais") != null ) {
            declaracionJuradaProductor.setPais(map.get("pais").toString());
        }
        
        if ( map.get("ciudad") != null ) {
            declaracionJuradaProductor.setCiudad(map.get("ciudad").toString());
        }



        return declaracionJuradaProductor;
    }

    /**
     * Registra un Productor de Declaraci�n Jurada
     */
    public MessageList insertDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertDeclaracionJuradaProductor(declaracionJuradaProductor);
            messageList.setObject(declaracionJuradaProductor);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaProductorDjNueva", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Actualiza un Productor de Declaraci�n Jurada
     */
    public MessageList updateDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateDeclaracionJuradaProductor(declaracionJuradaProductor);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateDeclaracionJuradaProductor", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * Elimina un Productor de Declaraci�n Jurada
     */
    public MessageList deleteDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.deleteDeclaracionJuradaProductor(declaracionJuradaProductor);
            message = new Message("delete.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteDeclaracionJuradaProductor", e);
            message = new ErrorMessage("delete.error", e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }


    /**
     * Inserta la tabla Mercancia de Certificado de Origen
     */
    /*public MessageList insertCOMercancia(CertificadoOrigenMercancia mercancia) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {

            //if ( mercancia.getDjId() == null ) {
                certificadoOrigenService.insertCertificadoOrigenMercanciaDjNueva(mercancia);
            /*} else {
                certificadoOrigenService.insertCertificadoOrigenMercanciaDjExistente(mercancia);
            }*/

            /*messageList.setObject(mercancia);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCOMercancia", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }*/
    
    /**
     * Registra la tabla Mercancia de Certificado de Origen
     */
    public MessageList insertCOMercancia(CertificadoOrigenMercancia mercancia, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("certificadoOrigenMercancia", mercancia);
            filter.put("formato", formato);

            certificadoOrigenService.insertCertificadoOrigenMercancia(filter);
            message = new Message("insert.success");
            messageList.setObject(mercancia);
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCOMercancia", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Actualiza la tabla Mercancia de Certificado de Origen
     */
    public MessageList updateCOMercancia(CertificadoOrigenMercancia mercancia, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("certificadoOrigenMercancia", mercancia);
            filter.put("formato", formato);

            certificadoOrigenService.updateCertificadoOrigenMercancia(filter);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCOMercancia", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Elimina la tabla Mercancia de Certificado de Origen
     */
    public MessageList deleteCOMercancia(CertificadoOrigenMercancia mercancia, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("certificadoOrigenMercancia", mercancia);
            filter.put("formato", formato);

            certificadoOrigenService.deleteCertificadoOrigenMercancia(filter);
            message = new Message("delete.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteCOMercancia", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }



    @SuppressWarnings("unchecked")
    /**
     * Obtiene informaci�n de una Declaracion Jurada
     */
    public DeclaracionJurada getCODeclaracionJuradaById(long id) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("djId", id);
        DeclaracionJurada dj = null;
        try {
            dj = certificadoOrigenService.getCODeclaracionJuradaById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCODeclaracionJuradaById", e);
        }
        return dj;
    }

    @SuppressWarnings("unchecked")
    /**
     * Obtiene informaci�n de una Declaracion Jurada
     */
    public List<DeclaracionJuradaProductor> getDJProductorByDJId(long djId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("djId", djId);
        List<DeclaracionJuradaProductor> lstProd = null;
        try {
            lstProd = certificadoOrigenService.getDJProductorByDJId(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCODeclaracionJuradaById", e);
        }
        return lstProd;
    }

    /**
     * M�todo para obtener los pa�ses de un determinado acuerdo internacional
     */
    public OptionList obtenerPaisesAcuerdo(HashUtil filtros) {
        String acuerdo = filtros.getString("filter");
        HashUtil<String, Object> filterAcuerdo = new HashUtil<String, Object>();
        filterAcuerdo.put("acuerdo", acuerdo);

           IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
           OptionList listaPuertosEmbarque = ibatisService.loadList("certificado_origen.paises_acuerdo.select", filterAcuerdo);
        return listaPuertosEmbarque;
    }

    /**
     * M�todo para obtener los acuerdos internacionales listados por pais
     */
    public OptionList obtenerListadoAcuerdos(HashUtil filtros) {
        String pais = filtros.getString("filter");
        HashUtil<String, Object> filterAcuerdo = new HashUtil<String, Object>();
        filterAcuerdo.put("pais", new Integer(pais));

           IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
           OptionList listaPuertosEmbarque = ibatisService.loadList("certificado_origen.acuerdos_pais.select", filterAcuerdo);
        return listaPuertosEmbarque;
    }

    /**
     * M�todo para obtener los acuerdos internacionales de Reexportacion
     */
    public OptionList obtenerListadoAcuerdosReexportacion(HashUtil filtros) {
        String pais = filtros.getString("filter");
        HashUtil<String, Object> filterAcuerdo = new HashUtil<String, Object>();
        filterAcuerdo.put("pais", new Integer(pais));
        
        IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        OptionList listaPuertosEmbarque = ibatisService.loadList("certificado_origen.acuerdos_pais.reexportacion.select", filterAcuerdo);
        return listaPuertosEmbarque;
    }

    @SuppressWarnings("unchecked")
    public HashUtil<String, Object> getCertificadoOrigenDRByDrId(int drId, int sdr, String formato) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("formato", formato);
        HashUtil<String, Object> coDR = new HashUtil<String, Object>();
        try {
            HashMap<String, Object> dr = certificadoOrigenService.getCertificadoOrigenDRById(filter);
            coDR.putAll(dr);
            coDR.addPrefix("DETALLE");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCertificadoOrigenDRByDrId", e);
        }
        return coDR;
    }

    public AdjuntoRequeridoDJMaterial getRequeridoDJById(int adjuntoRequeridoDj) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("adjuntoRequeridoDj", adjuntoRequeridoDj);
        String titulo = null;

        AdjuntoRequeridoDJMaterial adjunto = new AdjuntoRequeridoDJMaterial();
        try {
            adjunto = this.certificadoOrigenService.loadRequerido(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return adjunto;
    }

    public MessageList uploadFileDJ(long djId, int secuenciaMaterial, int adjuntoRequeridoDj, int adjuntoTipo, String nombre, byte[] bytes) {
        MessageList messageList = new MessageList();

        AdjuntoCertificadoOrigen archivoAdjunto = new AdjuntoCertificadoOrigen();
        archivoAdjunto.setDjId(djId);
        if (secuenciaMaterial > -1){
            archivoAdjunto.setSecuenciaMaterial(secuenciaMaterial);
        }
        archivoAdjunto.setAdjuntoRequeridoUo(adjuntoRequeridoDj);
        archivoAdjunto.setAdjuntoTipo(adjuntoTipo);
        archivoAdjunto.setNombreArchivo(ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);


        Message message = null;
        try {
            certificadoOrigenService.insertAdjuntoDJ(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.uploadFileDJ", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

    public void eliminarAdjuntoDJ(long adjuntoId, int djId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("adjuntoId", adjuntoId);
        filter.put("djId", djId);
        try {
            certificadoOrigenService.eliminarAdjuntoDJ(filter);
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.eliminarAdjuntoFormato", e);
        }
    }


    /*
     * Duplicado
     * */

    public CODuplicado getCertificadoOrigenMTC002ById(long coId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("coId", coId);
        CODuplicado coDuplicado = null;
        try {
            coDuplicado = certificadoOrigenService.getCertificadoOrigenMTC002ById(filter);
        } catch (Exception e) {
            logger.error("Error en SNS026LogicImpl.getCertificadoOrigenMTC002ById", e);
        }
        return coDuplicado;
    }

    public CODuplicado convertMapToCertificadoOrigenMCT002(Map<String, Object> map) {
        CODuplicado obj = new CODuplicado();
        obj.setDrIdOrigen(Util.longValueOf(map.get("drIdOrigen")));
        obj.setSdrOrigen(Util.integerValueOf(map.get("sdrOrigen")));
        obj.setCausalDuplicadoCo(Util.integerValueOf(map.get("causalDuplicadoCo")));
        obj.setSustentoAdicional(map.get("sustentoAdicional").toString());
        obj.setAceptacion(map.get("aceptacion")!=null?map.get("aceptacion").toString():"N");
        return obj;
    }

    public MessageList updateCertificadoOrigenDuplicado(CODuplicado coDuplicado) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateCertificadoOrigenDuplicado(coDuplicado);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCertificadoOrigenDuplicado", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }
    
    public MessageList crearSolicitudCertificado(UsuarioCO usuario, HashUtil<String, String> datos) throws Exception {
    	MessageList messageList = new MessageList();
		int idTupa = datos.getInt("idTupa");
        int idFormato = datos.getInt("idFormato");
        int acuerdo = datos.getInt("idAcuerdo");
        int pais = datos.getInt("idPais");
        int entidadCertificadora = datos.getInt("idEntidadCertificadora");
        
        String nombreFormato = datos.getString("formato");
        String certificadoOrigen = datos.getString("certificadoOrigen");
        String certificadoReexportacion = datos.getString("certificadoReexportacion");
        
        int solicitante = datos.getInt("SOLICITANTE.usuarioId");
        int empresa = !datos.getString("EMPRESA.id").equals("") ? datos.getInt("EMPRESA.id") : 0;
        int representante = !datos.getString("REPRESENTANTE.id").equals("") ? datos.getInt("REPRESENTANTE.id") : 0;
        
		messageList = ordenLogic.creaOrden(usuario, acuerdo, pais, entidadCertificadora, solicitante, representante, empresa, idTupa, idFormato, certificadoOrigen, certificadoReexportacion);
		messageList.remove(0);
		
        Orden orden = (Orden)messageList.getObject();
		
        if (ConstantesCO.CO_SOLICITUD.equals(nombreFormato.toLowerCase())) {
        	// Nada
        } else if (ConstantesCO.CO_DUPLICADO.equals(nombreFormato.toLowerCase())) {
    		Long drIdOrigen = datos.getLong("drIdOrigen");
            int sdrOrigen = datos.getInt("sdrOrigen");
            
			insertCertificadoOrigenDuplicadoOrigen(orden.getIdFormatoEntidad(), orden.getOrden(), drIdOrigen, sdrOrigen);
			
		} else if (ConstantesCO.CO_ANULACION.equals(nombreFormato.toLowerCase())) {
    		Long drIdOrigen = datos.getLong("drIdOrigen");
            int sdrOrigen = datos.getInt("sdrOrigen");
            
            insertCertificadoOrigenAnulacionOrigen(orden.getIdFormatoEntidad(), drIdOrigen, sdrOrigen);
            
		} else if (ConstantesCO.CO_CALIFICACION_ANTICIPADA.equals(nombreFormato.toLowerCase())) {
    		if ("".equals(datos.getString("calificacionUoId"))) {
            	CalificacionOrigen calificacionOrigen = new CalificacionOrigen();
           		calificacionOrigen.setCoId(orden.getIdFormatoEntidad());
           		calificacionOrigen.setOrdenIdInicial(orden.getOrden());
           		calificacionOrigen.setMtoInicial(orden.getMto());
           		calificacionOrigen.setEntidadId(entidadCertificadora);
           		calificacionOrigen.setAcuerdoInternacionalId(acuerdo);
           		calificacionOrigen.setPaisIsoIdxAcuerdoInt(pais);
           		
           		insertCalificacionOrigenConDeclaracionJurada(calificacionOrigen);
            } else {
            	HashUtil<String, Object> filter = new HashUtil<String, Object>();
                
            	filter.put("coId", orden.getIdFormatoEntidad());
            	filter.put("calificacionUoId", datos.getLong("calificacionUoId"));
            	filter.put("ordenId", orden.getOrden());
            	filter.put("mto", orden.getMto());
                
            	copiarDJCalificacionXCalificacion(filter);
            }
		} else if (ConstantesCO.CO_REEMPLAZO.equals(nombreFormato.toLowerCase())) {
    		Long drIdOrigen = datos.getLong("drIdOrigen");
            int sdrOrigen = datos.getInt("sdrOrigen");
            
            insertCertificadoOrigenReemplazoOrigen(orden.getOrden(), orden.getIdFormatoEntidad(), drIdOrigen, sdrOrigen);
		}
        
        Message message = new Message("insert.success");
        messageList.add(message);
        
    	return messageList;
    }
    
    private void insertCertificadoOrigenDuplicadoOrigen(long coId, Long orden, Long drIdOrigen, int sdrOrigen) throws Exception {
        CODuplicado coDuplicado = new CODuplicado();
        coDuplicado.setCoId(coId);
        coDuplicado.setOrden(orden);
        coDuplicado.setDrIdOrigen(drIdOrigen);
        coDuplicado.setSdrOrigen(sdrOrigen);
        
        certificadoOrigenService.insertCertificadoOrigenDuplicadoOrigen(coDuplicado);
    }

    public String getCausalById(String causal) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("causal", causal);
        String descCausal = null;
        try {
            descCausal = certificadoOrigenService.getCausalById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCausalById", e);
        }
        return descCausal;
    }


    public void setCertificadoOrigenService(
            CertificadoOrigenService certificadoOrigenService) {
        this.certificadoOrigenService = certificadoOrigenService;
    }

    /*
     * Anulacion
     * */

	public COAnulacion getCertificadoOrigenMTC004ById(long coId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("coId", coId);
        COAnulacion coAnulacion = null;
        try {
            coAnulacion = certificadoOrigenService.getCertificadoOrigenMTC004ById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCertificadoOrigenMTC004ById", e);
        }
        return coAnulacion;
    }

    public COAnulacion convertMapToCertificadoOrigenMCT004(Map<String, Object> map) {
        COAnulacion obj = new COAnulacion();
        obj.setDrIdOrigen(Util.longValueOf(map.get("drIdOrigen")));
        obj.setSdrOrigen(Util.integerValueOf(map.get("sdrOrigen")));
        obj.setSustentoAdicional(map.get("sustentoAdicional").toString());
        obj.setAceptacion(map.get("aceptacion")!=null?map.get("aceptacion").toString():"N");
        return obj;
    }

    public MessageList updateCertificadoOrigenAnulacion(COAnulacion coAnulacion) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateCertificadoOrigenAnulacion(coAnulacion);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCertificadoOrigenAnulacion", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }
    
    private void insertCertificadoOrigenAnulacionOrigen(long coId,Long drIdOrigen, int sdrOrigen) throws Exception {
        COAnulacion coAnulacion = new COAnulacion();
        coAnulacion.setCoId(coId);
        coAnulacion.setDrIdOrigen(drIdOrigen);
        coAnulacion.setSdrOrigen(sdrOrigen);
        
        certificadoOrigenService.insertCertificadoOrigenAnulacionOrigen(coAnulacion);
    }

    /**
     * M�todo para solicitar la validaci�n de una DJ.
     * Este m�todo es invocado por un exportador
     */
    public MessageList solicitaValidacionProductor(long djId) {
        MessageList messageList = new MessageList();
        Map<String,Object> parametros = new HashMap<String,Object>();
        parametros.put("djId", djId);

        Message message = null;
        try {
            certificadoOrigenService.solicitaValidacionProductor(parametros);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.solicitaValidacionProductor", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * M�todo para aceptar la solicitud de validaci�n de una DJ.
     * Este m�todo es invocado por un productor
     */
    public MessageList aceptaSolicitudValidacion(long djId) {
        MessageList messageList = new MessageList();
        Map<String,Object> parametros = new HashMap<String,Object>();
        parametros.put("djId", djId);

        Message message = null;
        try {
            certificadoOrigenService.aceptaSolicitudValidacion(parametros);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.aceptaSolicitudaValidacion", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * M�todo para rechazar la solicitud de validaci�n de una DJ.
     * Este m�todo es invocado por un productor
     */
    public MessageList rechazaSolicitudValidacion(long djId) {
        MessageList messageList = new MessageList();
        Map<String,Object> parametros = new HashMap<String,Object>();
        parametros.put("djId", djId);

        Message message = null;
        try {
            certificadoOrigenService.rechazaSolicitudValidacion(parametros);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.rechazaSolicitudaValidacion", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * M�todo para transmitir la validaci�n de una DJ.
     * Este m�todo es invocado por un productor
     */
    public MessageList transmiteDjValidada(long djId) {
        MessageList messageList = new MessageList();
        Map<String,Object> parametros = new HashMap<String,Object>();
        parametros.put("djId", djId);

        Message message = null;
        try {
            certificadoOrigenService.transmiteDjValidada(parametros);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.transmiteDjValidada", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }
    
    private void insertCertificadoOrigenReemplazoOrigen(long ordenId, long coId,Long drIdOrigen, int sdrOrigen) throws Exception {
        COReemplazo coReemplazo = new COReemplazo();
        coReemplazo.setOrden(ordenId);
        coReemplazo.setCoId(coId);
        coReemplazo.setDrIdOrigen(drIdOrigen);
        coReemplazo.setSdrOrigen(sdrOrigen);
        
        certificadoOrigenService.insertCertificadoOrigenReemplazoOrigen(coReemplazo);
    }

    public MessageList updateCertificadoOrigenReemplazo(COReemplazo coReemplazo) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.updateCertificadoOrigenReemplazo(coReemplazo);
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCertificadoOrigenAnulacion", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    public COReemplazo convertMapToCertificadoOrigenMCT003(Map<String, Object> map) {
        COReemplazo obj = new COReemplazo();
        
        obj.setDireccionAdicional(map.get("direccionAdicional")!=null?map.get("direccionAdicional").toString():null);
        
        obj.setDrIdOrigen(Util.longValueOf(map.get("drIdOrigen")));
        obj.setSdrOrigen(Util.integerValueOf(map.get("sdrOrigen")));
        obj.setCausalReemplazoCo(Util.integerValueOf(map.get("causalReemplazoCo").toString()));
        obj.setSustentoAdicional(map.get("sustentoAdicional").toString());
        obj.setObservacion(map.get("observacion").toString());

        // ACE Chile
        obj.setImportadorNombre(map.get("importadorNombre").toString());
        obj.setImportadorDireccion(map.get("importadorDireccion").toString());
        obj.setImportadorPais(!"".equals(map.get("importadorPais")) ? Integer.valueOf(map.get("importadorPais").toString()) : null);
        try {
            obj.setFechaPartidaTransporte(!"".equals(map.get("fechaPartidaTransporte")) ? Util.getDateObject(map.get("fechaPartidaTransporte").toString()) : null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        obj.setNumeroTransporte(map.get("numeroTransporte").toString());
        obj.setModoTransCargaId(map.get("modoTransCargaId")!=null && !"".equals(map.get("modoTransCargaId"))?Integer.valueOf(map.get("modoTransCargaId").toString()):null);
        obj.setPuertoCargaId(map.get("puertoCargaId")!=null && !"".equals(map.get("puertoCargaId")) ? Long.valueOf(map.get("puertoCargaId").toString()) : null);
        obj.setModoTransDescargaId(map.get("modoTransDescargaId")!=null && !"".equals(map.get("modoTransDescargaId"))?Integer.valueOf(map.get("modoTransDescargaId").toString()):null);
        obj.setPuertoDescargaId(map.get("puertoDescargaId")!=null && !"".equals(map.get("puertoDescargaId")) ? Long.valueOf(map.get("puertoDescargaId").toString()) : null);
        obj.setIncluyeProductor(map.get("incluyeProductor") != null ? map.get("incluyeProductor").toString() : "N");
        // UE
        obj.setEmitidoPosteriori(map.get("emitidoPosteriori") != null ? map.get("emitidoPosteriori").toString() : "N");
        
        //Centro America
        obj.setImportadorRegistroFiscal(map.get("importadorRegistroFiscal").toString());
        obj.setImportadorTelefono(map.get("importadorTelefono").toString());
        obj.setImportadorFax(map.get("importadorFax").toString());
        obj.setImportadorEmail(map.get("importadorEmail").toString());
        obj.setEsConfidencial(map.get("esConfidencial") != null ? map.get("esConfidencial").toString() : "N");
        obj.setPaisDescargaId(map.get("paisDescargaId")!=null && !"".equals(map.get("paisDescargaId"))?Integer.valueOf(map.get("paisDescargaId").toString()):null);
        
        obj.setImportadorCiudad(map.get("importadorCiudad") !=null ? map.get("importadorCiudad").toString() : null); //JMC 19/06/2017 Version 2 Alianza Pacifico
        
        return obj;
    }

    public HashUtil<String, Object> insertSubsanacionSolicitud(long ordenId,int mto) {
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("mtoNew", null);

        Message message = null;
        try {
            certificadoOrigenService.insertSubsanacionSolicitud(filter);
            element.put("ordenId", filter.getLong("ordenId"));
            element.put("mto", filter.getInt("mtoNew"));
            message = new Message("update.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertSubsanacionSolicitud", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
            element.put(Constantes.MESSAGE_LIST, messageList);
        }

        return element;

    }

    public MessageList deleteSubsanacionSolicitud(long ordenId,int mto) {
        MessageList messageList = new MessageList();
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);

        Message message = null;
        try {
            certificadoOrigenService.deleteSubsanacionSolicitud(filter);
            message = new Message("co.resolutor.exportador.eliminarSubsanacionOrden.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.deleteSubsanacionSolicitud", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    public int mtoCertificadoVigente(Long ordenId) throws Exception {
        return certificadoOrigenService.mtoCertificadoVigente(ordenId);
    }

    public Long getAdjunFirmaId(Long suceId, Long drId) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suceId", suceId);
        filter.put("drId", drId);

        return certificadoOrigenService.getAdjunFirmaId(filter);
    }

    public MessageList registraExportadorAutorizadoDj(Long calificacionUoId, String tipoDoc, String numerodoc, Date fInicio, Date fFin){
        MessageList messageList = new MessageList();
        HashUtil<String, Object> param = new HashUtil<String, Object>();

        param.put("calificacionUoId", calificacionUoId);
        param.put("inicioVigencia", fInicio);
        param.put("finVigencia", fFin);       

        
        Message message = null;
        try {
        	param.put("usuarioEmpId", this.certificadoOrigenService.devuelveEmpresaIdOUsuPrincipalIdByUsuId(tipoDoc, numerodoc));
        	
            if(param.get("usuarioEmpId").toString().equalsIgnoreCase("0")) {
            	message = new Message("co.exportador.autorizado.usuarioPrincipalNoRegistrado",numerodoc);            	
            } else {
	            certificadoOrigenService.registraExportadorAutorizadoDj(param);
	            message = new Message("co.dj.registro.autorizacion.success");
            }
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.registraExportadorAutorizadoDj", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    public MessageList revocaExportadorAutorizadoDj(Long calificacionUoId, String usuarioEmpId){
        MessageList messageList = new MessageList();
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("calificacionUoId", calificacionUoId);
        param.put("usuarioEmpId", usuarioEmpId);

        Message message = null;
        try {
            certificadoOrigenService.revocaExportadorAutorizadoDj(param);
            message = new Message("co.dj.revoca.autorizacion.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.revocaExportadorAutorizadoDj", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;
    }

    @SuppressWarnings("unchecked")
    /**
     * Obtiene informaci�n de una Declaracion Jurada
     */
    public void actualizarFlgDjCompleta(long calificacionUoId, String flg) {
        CalificacionOrigen filter = new CalificacionOrigen();
        filter.setCalificacionUoId(calificacionUoId);
        filter.setInformacionCompleta(flg);
        DeclaracionJurada dj = null;
        try {
            certificadoOrigenService.actualizarFlgDjCompleta(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCODeclaracionJuradaById", e);
        }
    }

    public Table obtenerPartidas(HashUtil<String, Object> filter){
        Table res = null;
        try {
            this.certificadoOrigenService.obtenerPartidas(filter);
            res = ibatisService.loadGrid("co.comun.busqueda.arancelaria.select", new HashUtil<String, Object>());
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.obtenerPartidas", e);
        }
        return res;
    }

    /**
     * Invoca la funcion que determina si se debe mostrar el boton para requerir la validacion del productor para la dj
     */
    public String djBtnValidacionProd(Long dj_id, Long orden_id, Integer mto) {
        String res = "0";

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("dj_id", dj_id);
        filter.put("orden_id", orden_id);
        filter.put("mto", mto);

        try {
            res = this.certificadoOrigenService.djBtnValidacionProd(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.djBtnValidacionProd", e);
        }

        return res;
    }

    /********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/
   
    /**
     * Registra la Direccion Adicional de una Calificacion
     */

    public MessageList updateDireccionAdicionalCalificacionOrigen(Long calificacionId, String direccionAdicional) {
    	MessageList messageList = new MessageList();
    	Message message = null;
        CalificacionOrigen calificacionOrigen = null;
        try {
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("coId", calificacionId);
            filter.put("direccionAdicional", direccionAdicional);
            
            certificadoOrigenService.updateDireccionAdicionalCalificacionOrigen(filter);
            messageList.setObject(calificacionOrigen);
            message = new Message("insert.success");            
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateDireccionAdicionalCalificacionOrigen", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Obtiene una Calificaci�n de Origen por su Id
     */

    public CalificacionOrigen getCalificacionOrigenById(Long calificacionId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("calificacionId", calificacionId);
        CalificacionOrigen calificacionOrigen = null;
        try {
            calificacionOrigen = certificadoOrigenService.getCalificacionOrigenById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCalificacionOrigenById", e);
        }
        return calificacionOrigen;
    }
    @SuppressWarnings("unchecked")
    public DeclaracionJurada getDjByCalificacionUoId(Long calificacionUoId, String enIngles) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("calificacionUoId", calificacionUoId);
        DeclaracionJurada dj = null;
        try {
            dj = certificadoOrigenService.getDjByCalificacionUoId(filter, enIngles);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getDjByCalificacionUoId", e);
        }
        return dj;
    }

    /**
     * Registra una Calificaci�n de Origen
     */
    public MessageList insertCalificacionOrigen(CalificacionOrigen calificacionOrigen) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("calificacionOrigen", calificacionOrigen);

            certificadoOrigenService.insertCalificacionOrigen(filter);
            messageList.setObject(calificacionOrigen);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCalificacionOrigen", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Actualiza informaci�n de Rol de una Calificaci�n de Origen
     */
    public MessageList updateRolCalificacionOrigen(CalificacionOrigen calificacionOrigen) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("calificacionOrigen", calificacionOrigen);

            certificadoOrigenService.updateRolCalificacionOrigen(filter);
            messageList.setObject(calificacionOrigen);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateRolCalificacionOrigen", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Actualiza informaci�n de Criterio de una Calificaci�n de Origen
     */
    public MessageList updateCriterioCalificacionOrigen(CalificacionOrigen calificacionOrigen) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("calificacionOrigen", calificacionOrigen);

            certificadoOrigenService.updateCriterioCalificacionOrigen(filter);
            messageList.setObject(calificacionOrigen);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.updateCriterioCalificacionOrigen", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Conversor de datos de request a objeto Calificaci�n Origen
     */
    public CalificacionOrigen convertMapToCalificacionOrigen(Map<String, Object> map) {
        CalificacionOrigen calificacionOrigen = new CalificacionOrigen();
        calificacionOrigen.setTipoRolCalificacion(( map.get("tipoRolCalificacion") != null  && !"".equals(map.get("tipoRolCalificacion")) ) ? Integer.valueOf(map.get("tipoRolCalificacion").toString()) : null);
        calificacionOrigen.setCumpleTotalmenteObtenido(map.get("cumpleTotalmenteObtenido") == null ? "N" : map.get("cumpleTotalmenteObtenido").toString());
        calificacionOrigen.setCumpleCriterioCambioClasif(map.get("cumpleCriterioCambioClasif") == null ? "N" : map.get("cumpleCriterioCambioClasif").toString());
        calificacionOrigen.setCumpleOtroCriterio(map.get("cumpleOtroCriterio") == null ? "N" : map.get("cumpleOtroCriterio").toString());
        calificacionOrigen.setSecuenciaNorma( (map.get("secuenciaNorma") != null && !"".equals(map.get("secuenciaNorma").toString().trim()) ) ? new Integer(map.get("secuenciaNorma").toString()) : null);
        calificacionOrigen.setSecuenciaOrigen( (map.get("secuenciaOrigen") != null && !"".equals(map.get("secuenciaOrigen").toString().trim())) ? new Integer(map.get("secuenciaOrigen").toString()) :null);
        calificacionOrigen.setPartidaSegunAcuerdo(map.get("partidaSegunAcuerdo") != null ? map.get("partidaSegunAcuerdo").toString() : null);
        calificacionOrigen.setDisposicionId(map.get("disposicionId") != null ? map.get("disposicionId").toString() : null);
        calificacionOrigen.setPorcentajeSegunCriterio( (map.get("porcentajeSegunCriterio") != null && !"".equals(map.get("porcentajeSegunCriterio").toString().trim())) ? new BigDecimal(map.get("porcentajeSegunCriterio").toString()) : null);
        return calificacionOrigen;
    }

    /**
     * Registra una Declaraci�n Jurada que tiene asociada una Calificaci�n de Origen
     */
    public MessageList insertDeclaracionJuradaCalificacion(DeclaracionJurada declaracionJurada) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("declaracionJurada", declaracionJurada);

            certificadoOrigenService.insertDeclaracionJuradaCalificacion(filter);
            messageList.setObject(declaracionJurada);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaCalificacion", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Registra una Calificaci�n de Origen junto a una Declaraci�n Jurada
     */
    private void insertCalificacionOrigenConDeclaracionJurada(CalificacionOrigen calificacionOrigen) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("calificacionOrigen", calificacionOrigen);
        
        certificadoOrigenService.insertCalificacionOrigen(filter);
        
        DeclaracionJurada djNueva = new DeclaracionJurada();
        djNueva.setCalificacionUoId(calificacionOrigen.getCalificacionUoId());
        filter.clear();
        filter.put("declaracionJurada", djNueva);
        
        certificadoOrigenService.insertDeclaracionJuradaCalificacion(filter);
    }

    /**
     * Devuelve 1 si la dj esta en proceso de validacion
     */
    public String djEnProcesoValidacion(HashUtil<String, Object> filter) {
        String res = "0";
        try {
            res = certificadoOrigenService.djEnProcesoValidacion(filter);
         } catch (Exception e) {
            logger.error("Error al obtener estado del proceso de validacion ddjj", e);
         }
         return res;
    }

    /**
     * Registra una Calificaci�n de Origen junto a una Mercancia y una Declaraci�n Jurada
     */
    public MessageList insertCalificacionOrigenConMercanciaDeclaracionJurada(CalificacionOrigen calificacionOrigen) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("calificacionOrigen", calificacionOrigen);

            certificadoOrigenService.insertCalificacionOrigenConMercanciaDeclaracionJurada(filter);

            messageList.setObject(calificacionOrigen);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCalificacionOrigenConMercanciaDeclaracionJurada", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }

    /**
     * Registra un mercanc�a a partir de una calificacion existente
     */
    public MessageList enlazaDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception {
        MessageList messageList = new MessageList();
        HashUtil<String, Object> res = certificadoOrigenService.enlazaDJCalificacionMercancia(filter);
        
        messageList.setObject(res);
        Message message = new Message("insert.success");
        messageList.add(message);
        return messageList;
    }
    
    /**
     * Registra un mercanc�a nueva a partir de una calificacion existente
     */
    public MessageList copiarDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception {
        MessageList messageList = new MessageList();
        HashUtil<String, Object> res = certificadoOrigenService.copiarDJCalificacionMercancia(filter);
        
        messageList.setObject(res);
        Message message = new Message("insert.success");
        messageList.add(message);
        return messageList;
    }

    /*
     * Devuelve el n�mero de materiales registrados para una DJ determinada
     */
    public int cuentaDJMateriales(Long djId){
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("djId", djId);
        try{
            return this.certificadoOrigenService.cuentaDJMateriales(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.cuentaDJMateriales", e);
            return 0;
        }
    }

    /**
     * Registra un calificaci�n nueva a partir de una calificacion existente (para MCT005)
     */
    private void copiarDJCalificacionXCalificacion(HashUtil<String, Object> filter) throws Exception {
        HashUtil<String, Object> res = certificadoOrigenService.copiarDJCalificacionXCalificacion(filter);
    }

    /**
     * Devuelve S si todos los materiales originarios de la dj tienen archivos adjuntos
     */
    public String djMaterialTieneAdj(HashUtil<String, Object> filter) {
        String res = "0";
        try {
            res = certificadoOrigenService.djEnProcesoValidacion(filter);
         } catch (Exception e) {
            logger.error("Error al verificar que todos los materiales origienarios de la DJ tengan adjuntos", e);
         }
         return res;
    }

    /********************************************* CAMBIO PARA CALIFICACI�N *******************************************************/

    /**
     * Indica si una dj requiere ser validada por el productor
     */
    @SuppressWarnings("finally")
    public String reqValidacionProductor(HashMap<String, Object> filter){
    	String res = ConstantesCO.OPCION_NO;
    	try{
    		res = certificadoOrigenService.reqValidacionProductor(filter);
    	} catch (Exception e) {
    		logger.error("Error al verificar si la DJ debe ser validada por el productor", e);
    	} finally {
    		return res;
    	}
    }

    /**
     * Indica si una dj ha sido validada
     */
    @SuppressWarnings("finally")
	public String djValidada(HashMap<String, Object> filter){
    	String res = ConstantesCO.OPCION_NO;
    	try{
    		res = certificadoOrigenService.djValidada(filter);
    	} catch (Exception e) {
    		logger.error("Error al verificar si la DJ ha sido validada por el productor", e);
    	} finally {
    		return res;
    	}
    }


 /**
     * Indica si una se ha pedido a un productor validar la DJ
     * NPCS: 08/02/2018
     */
    @SuppressWarnings("finally")
	public String existeProductorValidador(HashMap<String, Object> filter){
    	String res = ConstantesCO.OPCION_NO;
    	try{
    		res = certificadoOrigenService.existeProductorValidador(filter);
    	} catch (Exception e) {
    		logger.error("Error al verificar si se ha pedido que se valide la DJ", e);
    	} finally {
    		return res;
    	}
    }


    
	/**
	 * Devuelve un mensaje de error si el hash de la calificaci�n es igual al hash de la calificacion original
	 */
	public String validaHashCalificacion(HashMap<String, Object> filter){
    	String res = "";

    	try{
    		certificadoOrigenService.validaHashCalificacion(filter);
    	} catch (Exception e) {
    		if (e.getCause().getMessage().contains(ConstantesCO.ERROR_HASH_DUPLICADO)) {
    			res = "En el sistema existe una declaraci�n con exactamente los mismos datos de la calificaci�n a transmitir, modifique los datos de la declaraci�n jurada para poder continuar con el proceso.";
    		} else {
    			res = e.getMessage();
        		logger.error("Error al validar el Hash de la calificaci�n", e);
    		}
    	}

		return res;
	}

	/*******************************************************************************************************************/
	@SuppressWarnings("finally")
	public String obtenerIdiomaLlenado(Integer idAcuerdo) {
	    String res = "N";
    	try{
    		res = certificadoOrigenService.obtenerIdiomaLlenado(idAcuerdo);
    	} catch (Exception e) {
    		logger.error("Error al obtener el idioma del llenado del Acuerdo", e);
    	} finally {
    		return res;
    	}
    }

	/*
	 * Ejecuta la validaci�n que verifica que no se exceda el numero de lineas para la impresion de las mercancias
	 */
	public Message validarCertificadoEspacioDet(HashMap<String, Object> filter){
		Message msg = null;
		filter.put("seccion", 3);
		try {
			certificadoOrigenService.validarCertificadoEspacioDet(filter);
		} catch (Exception e) {
			msg = ComponenteOrigenUtil.getErrorMessage(e);
		}

    	return msg;
	}

    /**
     * Obtiene la suma de los valores US$ de los materiales de una dj
     */
	public double totalValorUSMateriales(HashMap<String, Object> filter){
    	double res = 0;
    	try{
    		res = certificadoOrigenService.totalValorUSMateriales(filter);
    	} catch (Exception e) {
    		logger.error("Error al intentar obtener la sumatoria de los valores US$ de los materiales de la declaraci�n jurada", e);
    	} 

    	return res;
    }
	
	/**
	 * Verifica si el usuario logeado sea el propietario de la DJ Calificada
	 */
	public String esPropietarioDatos(long calificacionUoId, String numDocUsuario){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		String esPropietario = "N";
		filter.put("calificacionUoId", calificacionUoId);

		try{             
			HashUtil<String, Object> res = certificadoOrigenService.obtenerDatosPropietarioDJ(filter);
			//Ticket 149343 GCHAVEZ-05/07/2019
			if( res!=null && numDocUsuario.equalsIgnoreCase(res.get("numDocPropietarioDJ").toString())){
			//Ticket 149343 GCHAVEZ-05/07/2019
				esPropietario="S";
			}
		} catch (Exception e) {
			logger.error("No se pudo validar si el usuario logeado es el propietario de los datos", e);
		}
		
		return esPropietario;
	}


	/**
	 * Verifica si el usuario logeado es quien solicito la Calificacion
	 * NPCS: 12/02/2018
	 */
	public String solicitoCalificacion(long calificacionUoId, String numDocUsuario){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		String solicitoCalificacion = "N";
		filter.put("calificacionUoId", calificacionUoId);

		try{
			HashUtil<String, Object> res = certificadoOrigenService.solicitoCalificacion(filter);
			if( res.get("numDocSolicitanteCalif").toString().equalsIgnoreCase(numDocUsuario)){
				solicitoCalificacion="S";
			}
		} catch (Exception e) {
			logger.error("No se pudo validar si el usuario logeado es quien solicito la Calificacion", e);
		}
		
		return solicitoCalificacion;
	}
	
	
	
	//20140825_JMC_Intercambio_Empresa
	
	/**
	 * Se parsea el EBXML de la Empresa a objeto MCT001EBXML
	 */
	public MCT001EBXML getCertificadoObjectFromEbXML(File ebXml){//InputStream isEbXml) 		
		
		MCT001EBXML mct001EbXml = null;
		
		try{
	        //Variables genericas
	        String numero;
	        String fecha;
	        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Node drNodo;
			
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = dbf.newDocumentBuilder();
	        builder.setErrorHandler(new SchemaValidationErrorHandler());          
	        Document documento = builder.parse(ebXml);
	        XPath xPath = XPathFactory.newInstance().newXPath();
	        
	        mct001EbXml =  new MCT001EBXML();
	        
	        CertificadoOrigen certificadoOrigen = new CertificadoOrigen();
	        
	        //*******Parseamos los datos del Solicitante******
	        Solicitante solicitante = new Solicitante();
	        Node dr = (Node)xPath.evaluate("/coo/Header/PetitionerTradeParty", documento, XPathConstants.NODE);
	        
	        certificadoOrigen.setNumeroDocumentoSolicitante(xPath.evaluate("Identification", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setDireccionAdicional(xPath.evaluate("AdditionalTradeAddress/StreetName", dr, XPathConstants.STRING).toString());	        
	        
	        solicitante.setNombre(xPath.evaluate("Name", dr, XPathConstants.STRING).toString());	        
	        numero = xPath.evaluate("IdentificationType", dr, XPathConstants.STRING).toString().trim();
	        if (!numero.equals("")) solicitante.setDocumentoTipo(Integer.parseInt(numero));	        
	        solicitante.setNumeroDocumento(xPath.evaluate("Identification", dr, XPathConstants.STRING).toString());
	        solicitante.setDireccion(xPath.evaluate("TradeAddress/StreetName", dr, XPathConstants.STRING).toString());
	        solicitante.setUbigeo(xPath.evaluate("Ubigeo", dr, XPathConstants.STRING).toString());
	        solicitante.setTelefono(xPath.evaluate("TelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        solicitante.setFax(xPath.evaluate("FaxUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        solicitante.setEmail(xPath.evaluate("EmailUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        solicitante.setCelular(xPath.evaluate("MobileTelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        solicitante.setTipoPersona(xPath.evaluate("Type", dr, XPathConstants.STRING).toString());
	        
	        
	        //*******Parseamos los datos de la persona que hace el formato******
	        dr = (Node)xPath.evaluate("/coo/Header/PersonTradeParty", documento, XPathConstants.NODE);
	        Solicitante usuario = new Solicitante();
	        usuario.setNombre(xPath.evaluate("Name", dr, XPathConstants.STRING).toString());
	        /*numero = xPath.evaluate("IdentificationType", dr, XPathConstants.STRING).toString().trim();
	        if (!numero.equals("")) usuario.setDocumentoTipo(Integer.parseInt(numero));
	        */
	        usuario.setNumeroDocumento(xPath.evaluate("Identification", dr, XPathConstants.STRING).toString());
	        usuario.setDireccion(xPath.evaluate("TradeAddress/StreetName", dr, XPathConstants.STRING).toString());
	        usuario.setUbigeo(xPath.evaluate("Ubigeo", dr, XPathConstants.STRING).toString());
	        usuario.setTelefono(xPath.evaluate("TelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        usuario.setFax(xPath.evaluate("FaxUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        usuario.setEmail(xPath.evaluate("EmailUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        usuario.setCelular(xPath.evaluate("MobileTelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        usuario.setTipoPersona(xPath.evaluate("Type", dr, XPathConstants.STRING).toString());

	        mct001EbXml.setUsuario(usuario);
	        mct001EbXml.setSolicitante(solicitante);
	        
	        //mct001EbXml.setEmpresaExterna(empresaExterna);
	        

	        
	        //*******Parseamos los datos del Certificado******
	        
	        dr = (Node)xPath.evaluate("/coo/CertificateOfOrigin", documento, XPathConstants.NODE);
	        	                
	        //certificadoOrigen.setSecuenciaCertificado(Util.integerValueOf(xPath.evaluate("CertifyingAgencyCode", dr, XPathConstants.STRING).toString()));
	        certificadoOrigen.setPaisIsoCodigo(xPath.evaluate("Agreement/TradeCountry/Code", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setAcuerdoInternacionalCodigo(xPath.evaluate("Agreement/Code", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setEntidadCodigo(Util.integerValueOf(xPath.evaluate("CertifyingAgencyCode", dr, XPathConstants.STRING).toString()));
	        certificadoOrigen.setSedeCodigo(Util.integerValueOf(xPath.evaluate("CertifyingAgencyHeadquartersCode", dr, XPathConstants.STRING).toString()));
	        certificadoOrigen.setEmitidoPosteriori(xPath.evaluate("APosterioriIssuedFlag", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setIncluyeProductor(xPath.evaluate("IncludedProducerFlag", dr, XPathConstants.STRING).toString());
	        //Datos del Importador
	        certificadoOrigen.setImportadorNombre(xPath.evaluate("ImporterTradeParty/Name", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setImportadorDireccion(xPath.evaluate("ImporterTradeParty/TradeAddress/StreetName", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setImportadorPaisIsoCodigo(xPath.evaluate("ImporterTradeCountry/Code", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setImportadorRegistroFiscal(xPath.evaluate("ImporterTradeParty/TaxIdentification", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setImportadorEmail(xPath.evaluate("ImporterTradeParty/EmailUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setImportadorTelefono(xPath.evaluate("ImporterTradeParty/TelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        certificadoOrigen.setImportadorFax(xPath.evaluate("ImporterTradeParty/FaxUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	        
			if(!"".equals(xPath.evaluate("TransportMeans/DepartureDate", dr, XPathConstants.STRING).toString())){
				certificadoOrigen.setFechaPartidaTransporte(sdf.parse(xPath.evaluate("TransportMeans/DepartureDate", dr, XPathConstants.STRING).toString()));
			}			
	        
	        certificadoOrigen.setNumeroTransporte(xPath.evaluate("TransportMeans/TransportNumber", dr, XPathConstants.STRING).toString()) ;
	    	certificadoOrigen.setModoTransCargaCodigo(Util.integerValueOf(xPath.evaluate("TransportMeans/LoadingPortType", dr, XPathConstants.STRING).toString()));
	    	certificadoOrigen.setModoTransDescargaCodigo(Util.integerValueOf(xPath.evaluate("TransportMeans/LandingPortType", dr, XPathConstants.STRING).toString()));	    	
	    	certificadoOrigen.setPuertoCargaCodigo(xPath.evaluate("TransportMeans/LoadingPort", dr, XPathConstants.STRING).toString());
	    	certificadoOrigen.setPuertoDescargaCodigo(xPath.evaluate("TransportMeans/LandingPort", dr, XPathConstants.STRING).toString());	    	
	    	certificadoOrigen.setPaisCargaCodigo(xPath.evaluate("TransportMeans/LoadingPortTradeCountry/Code", dr, XPathConstants.STRING).toString());
	    	certificadoOrigen.setPaisDescargaCodigo(xPath.evaluate("TransportMeans/LandingPortTradeCountry/Code", dr, XPathConstants.STRING).toString());
	    	certificadoOrigen.setObservacion(xPath.evaluate("Comment", dr, XPathConstants.STRING).toString());
	    	
	    	mct001EbXml.setCertificadoOrigen(certificadoOrigen);
	    	
			NodeList drAdjuntos;
			drAdjuntos = (NodeList)xPath.evaluate("/coo/AttachedDocument", documento, XPathConstants.NODESET);
			List<AdjuntoFormato> adjuntos = new ArrayList<AdjuntoFormato>();
			if(drAdjuntos != null) {
				for (int i=0; i < drAdjuntos.getLength(); i++) {
		        	Node drAdjunto = drAdjuntos.item(i);
		        	AdjuntoFormato adjunto = new AdjuntoFormato();
		        	adjunto.setSecuencia(Util.integerValueOf(xPath.evaluate("DocumentItem", drAdjunto, XPathConstants.STRING).toString()));
		        	adjunto.setNombre(xPath.evaluate("AttachedName", drAdjunto, XPathConstants.STRING).toString());
		        	adjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
		        	adjuntos.add(adjunto);
		        }
				mct001EbXml.setAdjuntos(adjuntos);
			}

	        NodeList drInvoices = (NodeList)xPath.evaluate("/coo/CertificateOfOrigin/Invoice", documento, XPathConstants.NODESET);
	        for (int contProd=0; contProd < drInvoices.getLength(); contProd++) {
	        	Node drInvoice = drInvoices.item(contProd); 	        	
		    	COFactura coFactura = new COFactura();
		    	
		    	coFactura.setSecuencia(Util.integerValueOf(xPath.evaluate("Item", drInvoice, XPathConstants.STRING).toString()));
		    	coFactura.setIndicadorTercerPais(xPath.evaluate("ThirdOpFlag", drInvoice, XPathConstants.STRING).toString());	    	
		    	coFactura.setNombreOperadorTercerPais(xPath.evaluate("ThirdOpTradeParty/Name", drInvoice, XPathConstants.STRING).toString());
		    	coFactura.setDireccionOperadorTercerPais(xPath.evaluate("ThirdOpTradeParty/TradeAddress/StreetName", drInvoice, XPathConstants.STRING).toString());
		    	coFactura.setTieneFacturaTercerPais(xPath.evaluate("InvoicedThirdOpFlag", drInvoice, XPathConstants.STRING).toString());
		    	coFactura.setNumero(xPath.evaluate("Number", drInvoice, XPathConstants.STRING).toString());		    	
		    		        
				if(!"".equals(xPath.evaluate("Date", drInvoice, XPathConstants.STRING).toString())){
					coFactura.setFecha(sdf.parse(xPath.evaluate("Date", drInvoice, XPathConstants.STRING).toString()));
				}				
				
		    	coFactura.setAdjuntoId(Util.longValueOf(xPath.evaluate("AttachedDocument/DocumentItem", drInvoice, XPathConstants.STRING).toString()));
		    	
		    	for(AdjuntoFormato adjunto: mct001EbXml.getAdjuntos()){
		    		if(adjunto.getSecuencia().toString().equalsIgnoreCase(coFactura.getAdjuntoId().toString())){
		    			coFactura.setNombreAdjunto(adjunto.getNombre());
		    		}		    		
		    	}
		    	
		    	//String nombreAdjunto = mct001EbXml.getAdjuntos().get(Util.integerValueOf(coFactura.getAdjuntoId())).getNombre();
		    	//coFactura.setNombreAdjunto(nombreAdjunto);
		    	
		    	mct001EbXml.addCOFactura(coFactura);

	        }
	        

	        NodeList drMercancias = (NodeList)xPath.evaluate("/coo/CertificateOfOrigin/Declaration/GoodsCharacteristic", documento, XPathConstants.NODESET);
	        for (int cont=0; cont < drMercancias.getLength(); cont++) {
	        	Node drMercancia = drMercancias.item(cont); 	        	
		        
		    	CertificadoOrigenMercancia certificadoOrigenMercancia = new CertificadoOrigenMercancia();
		    	certificadoOrigenMercancia.setSecuenciaMercancia(Util.integerValueOf(xPath.evaluate("ItemVUCE", drMercancia, XPathConstants.STRING).toString()));
		    	certificadoOrigenMercancia.setDescripcion(xPath.evaluate("Description", drMercancia, XPathConstants.STRING).toString());
		    	certificadoOrigenMercancia.setMarcasNumeros(xPath.evaluate("LogisticsPackage/LogisticsShippingMarks/Marking", drMercancia, XPathConstants.STRING).toString());
		    	certificadoOrigenMercancia.setEsMercanciaGranel(xPath.evaluate("BulkFlag", drMercancia, XPathConstants.STRING).toString());
		    	certificadoOrigenMercancia.setTipoBulto(xPath.evaluate("BulkKind", drMercancia, XPathConstants.STRING).toString());
		    	certificadoOrigenMercancia.setCantidadBulto(Util.integerValueOf(xPath.evaluate("BulkQuantity", drMercancia, XPathConstants.STRING).toString()));
		    	certificadoOrigenMercancia.setCantidad(Util.bigDecimalValueOf(xPath.evaluate("Quantity", drMercancia, XPathConstants.STRING).toString()));
		    	certificadoOrigenMercancia.setUmFisicaCodigo(xPath.evaluate("MeasureUnit", drMercancia, XPathConstants.STRING).toString());
		    	certificadoOrigenMercancia.setValorFacturadoUs(Util.bigDecimalValueOf(xPath.evaluate("InvoicedValue", drMercancia, XPathConstants.STRING).toString()));
		    	certificadoOrigenMercancia.setUmFisicaSegunFactura(xPath.evaluate("MeasureUnitByInvoice", drMercancia, XPathConstants.STRING).toString());		    	
		    	certificadoOrigenMercancia.setNumeroDj(xPath.evaluate("DeclarationCode", drMercancia, XPathConstants.STRING).toString());
		    	certificadoOrigenMercancia.setNumeroFactura(xPath.evaluate("Invoice/Number", drMercancia, XPathConstants.STRING).toString());		    	
		    	certificadoOrigenMercancia.setPartidaSegunAcuerdo(xPath.evaluate("TariffCode", drMercancia, XPathConstants.STRING).toString());		    	
		    	mct001EbXml.addCertificadoOrigenMercancia(certificadoOrigenMercancia);

	        } 
	        			
	    	
	    } 	catch (FileNotFoundException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (ParserConfigurationException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (SAXException e) {
	      logger.error("Error al validar el archivo XML de Transaccion. Documento no valido", e);
	    } catch (IOException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (XPathExpressionException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (Exception e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } 
		return mct001EbXml;
		//return null;	
		
	}
	
	public MCT005EBXML getDJObjectFromEbXML(File ebXml){//InputStream isEbXml)

		MCT005EBXML mct005EbXml = null;
		
		try{
	        //Variables genericas
	        String numero;
	        String fecha;
	        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Node drNodo;
			
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = dbf.newDocumentBuilder();
	        builder.setErrorHandler(new SchemaValidationErrorHandler());          
	        Document documento = builder.parse(ebXml);
	        XPath xPath = XPathFactory.newInstance().newXPath();
	        
	        mct005EbXml =  new MCT005EBXML();	        
	        DeclaracionJurada declaracionJurada = new DeclaracionJurada();
	        CertificadoOrigen certificadoOrigen = new CertificadoOrigen();	        
	        //CalificacionOrigen calificacion = new CalificacionOrigen();	        
	        
	        //*******Parseamos los datos del Solicitante******
	        Solicitante solicitante = new Solicitante();
	        Node dr = (Node)xPath.evaluate("/coo/Header/PetitionerTradeParty", documento, XPathConstants.NODE);
	        
	        declaracionJurada.setNumeroDocumentoSolicitante(xPath.evaluate("Identification", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setDireccionAdicional(xPath.evaluate("AdditionalTradeAddress/StreetName", dr, XPathConstants.STRING).toString());	        
	        
	        solicitante.setNombre(xPath.evaluate("Name", dr, XPathConstants.STRING).toString());	        
	        numero = xPath.evaluate("IdentificationType", dr, XPathConstants.STRING).toString().trim();
	        if (!numero.equals("")) solicitante.setDocumentoTipo(Integer.parseInt(numero));	        
	        solicitante.setNumeroDocumento(xPath.evaluate("Identification", dr, XPathConstants.STRING).toString());
	        solicitante.setDireccion(xPath.evaluate("TradeAddress/StreetName", dr, XPathConstants.STRING).toString());
	        solicitante.setUbigeo(xPath.evaluate("Ubigeo", dr, XPathConstants.STRING).toString());
	        solicitante.setTelefono(xPath.evaluate("TelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        solicitante.setFax(xPath.evaluate("FaxUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        solicitante.setEmail(xPath.evaluate("EmailUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        solicitante.setCelular(xPath.evaluate("MobileTelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        solicitante.setTipoPersona(xPath.evaluate("Type", dr, XPathConstants.STRING).toString());	        
	        
	        //*******Parseamos los datos de la persona que hace el formato******
	        dr = (Node)xPath.evaluate("/coo/Header/PersonTradeParty", documento, XPathConstants.NODE);
	        Solicitante usuario = new Solicitante();
	        usuario.setNombre(xPath.evaluate("Name", dr, XPathConstants.STRING).toString());
	        /*numero = xPath.evaluate("IdentificationType", dr, XPathConstants.STRING).toString().trim();
	        if (!numero.equals("")) usuario.setDocumentoTipo(Integer.parseInt(numero));
	        */
	        usuario.setNumeroDocumento(xPath.evaluate("Identification", dr, XPathConstants.STRING).toString());
	        usuario.setDireccion(xPath.evaluate("TradeAddress/StreetName", dr, XPathConstants.STRING).toString());
	        usuario.setUbigeo(xPath.evaluate("Ubigeo", dr, XPathConstants.STRING).toString());
	        usuario.setTelefono(xPath.evaluate("TelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        usuario.setFax(xPath.evaluate("FaxUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        usuario.setEmail(xPath.evaluate("EmailUniversalCommunication/UniformResourceIdentifier", dr, XPathConstants.STRING).toString());
	        usuario.setCelular(xPath.evaluate("MobileTelephoneUniversalCommunication/CompleteNumber", dr, XPathConstants.STRING).toString());
	        usuario.setTipoPersona(xPath.evaluate("Type", dr, XPathConstants.STRING).toString());

	        mct005EbXml.setUsuario(usuario);
	        mct005EbXml.setSolicitante(solicitante);	        
	        	        
	        //*******Parseamos los datos del Certificado******
	        
	        dr = (Node)xPath.evaluate("/coo/CertificateOfOrigin", documento, XPathConstants.NODE);
	        	                
	        //certificadoOrigen.setSecuenciaCertificado(Util.integerValueOf(xPath.evaluate("CertifyingAgencyCode", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setPaisIsoIdPorAcuerdoIntCodigo(xPath.evaluate("Agreement/TradeCountry/Code", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setAcuerdoInternacionalCodigo(xPath.evaluate("Agreement/Code", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setEntidadCodigo(Util.integerValueOf(xPath.evaluate("CertifyingAgencyCode", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setSedeCodigo(Util.integerValueOf(xPath.evaluate("CertifyingAgencyHeadquartersCode", dr, XPathConstants.STRING).toString()));

	        declaracionJurada.setTipoRolDj(Util.integerValueOf(xPath.evaluate("Qualification/Rol", dr, XPathConstants.STRING).toString()));
	        
	    	//Adjuntos
			NodeList drAdjuntos;
			drAdjuntos = (NodeList)xPath.evaluate("/coo/AttachedDocument", documento, XPathConstants.NODESET);
			List<AdjuntoFormato> adjuntos = new ArrayList<AdjuntoFormato>();
			if(drAdjuntos != null) {
				for (int i=0; i < drAdjuntos.getLength(); i++) {
		        	Node drAdjunto = drAdjuntos.item(i);
		        	AdjuntoFormato adjunto = new AdjuntoFormato();
		        	adjunto.setSecuencia(Util.integerValueOf(xPath.evaluate("DocumentItem", drAdjunto, XPathConstants.STRING).toString()));
		        	adjunto.setNombre(xPath.evaluate("AttachedName", drAdjunto, XPathConstants.STRING).toString());
		        	adjunto.setAdjuntoRequerido(Util.integerValueOf(xPath.evaluate("DocumentCode", drAdjunto, XPathConstants.STRING).toString()));
		        	adjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
		        	adjuntos.add(adjunto);
		        }
				mct005EbXml.setAdjuntos(adjuntos);
			}
			
	        //Detalle de Productores
			NodeList drProductores = (NodeList)xPath.evaluate("/coo/CertificateOfOrigin/ProducerTradeParty", dr, XPathConstants.NODESET);
	        for (int contProd=0; contProd < drProductores.getLength(); contProd++) {
	        	Node drProductor = drProductores.item(contProd); 	        	
	        	DeclaracionJuradaProductor productor = new DeclaracionJuradaProductor();
			
		        productor.setDocumentoTipo(Util.integerValueOf(xPath.evaluate("IdentificationType", drProductor, XPathConstants.STRING).toString()));
		        productor.setSecuenciaProductor(Util.integerValueOf(xPath.evaluate("Item", drProductor, XPathConstants.STRING).toString()));
		        productor.setNumeroDocumento(xPath.evaluate("Identification", drProductor, XPathConstants.STRING).toString());
		        productor.setNombre(xPath.evaluate("Name", drProductor, XPathConstants.STRING).toString());
		        productor.setDireccion(xPath.evaluate("AdditionalTradeAddress/StreetName", drProductor, XPathConstants.STRING).toString());
		        productor.setDireccionAdicional(xPath.evaluate("AdditionalTradeAddress/StreetName", drProductor, XPathConstants.STRING).toString());
		        productor.setTelefono(xPath.evaluate("TelephoneUniversalCommunication/CompleteNumber", drProductor, XPathConstants.STRING).toString());
		        productor.setFax(xPath.evaluate("FaxUniversalCommunication/CompleteNumber", drProductor, XPathConstants.STRING).toString());
		        productor.setEmail(xPath.evaluate("EmailUniversalCommunication/UniformResourceIdentifier", drProductor, XPathConstants.STRING).toString());
		        productor.setEsValidador(xPath.evaluate("IsValidatorFlag", drProductor, XPathConstants.STRING).toString());
		        
		        productor.setAdjuntoId(Util.longValueOf(xPath.evaluate("AttachedDocument/DocumentItem", drProductor, XPathConstants.STRING).toString()));
		    	
		    	for(AdjuntoFormato adjunto: mct005EbXml.getAdjuntos()){
		    		if(adjunto.getSecuencia().toString().equalsIgnoreCase(productor.getAdjuntoId().toString())){
		    			productor.setNombreAdjunto(adjunto.getNombre());
		    		}		    		
		    	}
		    	
		        mct005EbXml.addDeclaracionJuradaProductor(productor);
	        }
	        
	        //Criterio de Origen
	        declaracionJurada.setCumpleTotalmenteObtenido(xPath.evaluate("Qualification/ObtainedCompletelyFlag", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setCumpleCriterioCambioClasif(xPath.evaluate("Qualification/TariffCodeChangeFlag", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setCumpleOtroCriterio(xPath.evaluate("Qualification/CriterionOtherFlag", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setSecuenciaNorma(Util.integerValueOf(xPath.evaluate("Qualification/RuleItem", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setSecuenciaOrigen(Util.integerValueOf(xPath.evaluate("Qualification/CriterionItem", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setPartidaSegunAcuerdo(xPath.evaluate("Qualification/Agreement/TariffCode", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setPorcentajeSegunCriterio(Util.bigDecimalValueOf(xPath.evaluate("Qualification/CriterionAboutPercent", dr, XPathConstants.STRING).toString()));
	        //mct005EbXml.setCalificacion(calificacion);
	        
	        //Declaraci�n Jurada - Datos de la DJ
	        declaracionJurada.setDenominacion(xPath.evaluate("Declaration/GoodsCharacteristic/BrandName", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setCaracteristica(xPath.evaluate("Declaration/GoodsCharacteristic/Features", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setPartidaArancelaria(Util.longValueOf(xPath.evaluate("Declaration/GoodsCharacteristic/TariffCode", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setUnidadMedidaCodigo(xPath.evaluate("Declaration/GoodsCharacteristic/MeasureUnit", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setCantidadUmRefer(xPath.evaluate("Declaration/GoodsCharacteristic/MeasureUnitAndQuantity", dr, XPathConstants.STRING).toString());
	        declaracionJurada.setAceptacion(xPath.evaluate("Declaration/GoodsCharacteristic/AcceptanceFlag", dr, XPathConstants.STRING).toString());
	        
	        //Declaraci�n Jurada - Materiales
	        declaracionJurada.setDemasGasto(Util.bigDecimalValueOf(xPath.evaluate("Declaration/GoodsCharacteristic/ExpensesOther", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setPorcentajeDemasGastos(Util.bigDecimalValueOf(xPath.evaluate("Declaration/GoodsCharacteristic/ExpensesOtherPercent", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setValorUs(Util.bigDecimalValueOf(xPath.evaluate("Declaration/GoodsCharacteristic/UnitPrice", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setValorUsFabrica(Util.bigDecimalValueOf(xPath.evaluate("Declaration/GoodsCharacteristic/FactoryPrice", dr, XPathConstants.STRING).toString()));
	        declaracionJurada.setPesoNetoMercancia(Util.bigDecimalValueOf(xPath.evaluate("Declaration/GoodsCharacteristic/Weight", dr, XPathConstants.STRING).toString()));
	        //declaracion.setPorcentajeSegunCriterio(xPath.evaluate("Declaration/GoodsCharacteristic/ExpensesOther", dr, XPathConstants.STRING).toString());
	        mct005EbXml.setDeclaracionJurada(declaracionJurada);
	        
	        //Detalle de Materiales
			NodeList drMateriales = (NodeList)xPath.evaluate("/coo/CertificateOfOrigin/Declaration/GoodsCharacteristic/Material", dr, XPathConstants.NODESET);
	        for (int contProd=0; contProd < drMateriales.getLength(); contProd++) {
	        	Node drMaterial = drMateriales.item(contProd); 	        	
	        	DeclaracionJuradaMaterial material = new DeclaracionJuradaMaterial();
	        
		        material.setSecuenciaMaterial(Util.integerValueOf(xPath.evaluate("ItemVUCE", drMaterial, XPathConstants.STRING).toString()));
		        material.setDescripcion(xPath.evaluate("Name", drMaterial, XPathConstants.STRING).toString());
		        material.setPartidaArancelaria(Util.longValueOf(xPath.evaluate("TariffCode", drMaterial, XPathConstants.STRING).toString()));
		        material.setPaisProcedenciaCodigo(xPath.evaluate("ProcedenceTradeCountry/Code", drMaterial, XPathConstants.STRING).toString());
		        material.setPaisOrigenCodigo(xPath.evaluate("OriginTradeCountry/Code", drMaterial, XPathConstants.STRING).toString());
		        material.setFabricanteDocumentoTipo(Util.integerValueOf(xPath.evaluate("ManufacturerEstablishment/IdentificationType", drMaterial, XPathConstants.STRING).toString()));
		        material.setFabricanteNumeroDocumento(xPath.evaluate("ManufacturerEstablishment/Identification", drMaterial, XPathConstants.STRING).toString());
		        material.setFabricanteNombre(xPath.evaluate("ManufacturerEstablishment/Name", drMaterial, XPathConstants.STRING).toString());
		        
		        material.setUmFisicaCodigo(Util.integerValueOf(xPath.evaluate("MeasureUnit", drMaterial, XPathConstants.STRING).toString()));
		        material.setCantidad(Util.bigDecimalValueOf(xPath.evaluate("Quantity", drMaterial, XPathConstants.STRING).toString()));
		        material.setPesoNeto(Util.bigDecimalValueOf(xPath.evaluate("NetWeight", drMaterial, XPathConstants.STRING).toString()));
		        material.setValorUs(Util.bigDecimalValueOf(xPath.evaluate("Value", drMaterial, XPathConstants.STRING).toString()));
		        material.setPorcentajeValor(Util.bigDecimalValueOf(xPath.evaluate("ValuePercent", drMaterial, XPathConstants.STRING).toString()));
		        material.setPartidaSegunAcuerdo(xPath.evaluate("Agreement/TariffCode", drMaterial, XPathConstants.STRING).toString());
		        
		        NodeList drMaterialAdjuntos = (NodeList)xPath.evaluate("AttachedJustification", drMaterial, XPathConstants.NODESET);
		        for (int contAdj=0; contAdj < drMaterialAdjuntos.getLength(); contAdj++) {
		        	Node drAdjunto = drMaterialAdjuntos.item(contAdj);
		        	Adjunto adjunto = new Adjunto();
		        	if(!"".equalsIgnoreCase(xPath.evaluate("DocumentItem", drAdjunto, XPathConstants.STRING).toString())){
		        		adjunto.setId(Util.integerValueOf(xPath.evaluate("DocumentItem", drAdjunto, XPathConstants.STRING).toString()));
		        	}
		        		
		        	//adjunto.setId(Util.integerValueOf(xPath.evaluate("DocumentItem", drAdjunto, XPathConstants.STRING).toString()));
		        	adjunto.setNombre(xPath.evaluate("AttachedName", drAdjunto, XPathConstants.STRING).toString());
		        	material.addAdjunto(adjunto);
		        }
		        //material.setAdjuntoId(Util.longValueOf(xPath.evaluate("AttachedJustification/DocumentItem", drMaterial, XPathConstants.STRING).toString()));
		    	/*
		    	for(AdjuntoFormato adjunto: mct005EbXml.getAdjuntos()){
		    		if(adjunto.getSecuencia().toString().equalsIgnoreCase(material.getAdjuntoId().toString())){
		    			material.setNombreAdjunto(adjunto.getNombre());
		    		}		    		
		    	}
		    	*/
		        mct005EbXml.addDeclaracionJuradaMaterial(material);
	    	
	        }	        			
	    	
	    } 	catch (FileNotFoundException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (ParserConfigurationException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (SAXException e) {
	      logger.error("Error al validar el archivo XML de Transaccion. Documento no valido", e);
	    } catch (IOException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (XPathExpressionException e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } catch (Exception e) {
	      logger.error("Error al validar el archivo XML de Transaccion", e);
	    } 
		return mct005EbXml;
		//return null;	
	}

	//Parseamos el ebxml a objeto
    public SolicitudEBXML parserEbXmlToBean(byte[] xmlNotificacion, byte[] ebXml, byte[] adjuntosZip, int idNTX, String tipoTransaccion) {
    	SolicitudEBXML formatoEbXml=null;
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		try{
			
			if(tipoTransaccion.equalsIgnoreCase(ConstantesCO.CO_SOLICITUD)){//MCT001
				
				formatoEbXml = getCertificadoObjectFromEbXML(StreamUtil.getBytesAsFile(ebXml));
				
			} else if(tipoTransaccion.equalsIgnoreCase(ConstantesCO.CO_CALIFICACION_ANTICIPADA)){//MCT005
				
				formatoEbXml = getDJObjectFromEbXML(StreamUtil.getBytesAsFile(ebXml));
			}

			if (formatoEbXml==null) {
				return formatoEbXml;
			}

			
		} catch(Exception ex) {
			logger.error("ERROR: Al parsear el formato en FormatoLogicImpl.parserEbXmlToBean", ex);
		}
		return formatoEbXml;
    }
    
    public int insertCertificadoOrigenEmpresa(CertificadoOrigen certificadoOrigen) {
    	int secuencia=0;

        try {
        	secuencia = certificadoOrigenService.insertCertificadoOrigenEmpresa(certificadoOrigen);

        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCertificadoOrigenEmpresa", e);
            //message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        
        return secuencia;

    }
    
    public MessageList insertCOFacturaEmpresa(COFactura factura) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertCOFacturaEmpresa(factura);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCOFacturaEmpresa", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }   
    public MessageList insertCertificadoOrigenMercanciaEmpresa(CertificadoOrigenMercancia certificadoOrigenMercancia) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertCertificadoOrigenMercanciaEmpresa(certificadoOrigenMercancia);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertCertificadoOrigenMercanciaEmpresa", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    }
    
    public int insertDeclaracionJuradaEmpresa(DeclaracionJurada declaracionJurada) {
    	int secuencia=0;

        try {
        	secuencia = certificadoOrigenService.insertDeclaracionJuradaEmpresa(declaracionJurada);

        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaEmpresa", e);
            //message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        
        return secuencia;
    }

    public MessageList insertDeclaracionJuradaProductorEmpresa(DeclaracionJuradaProductor declaracionJuradaProductor) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertDeclaracionJuradaProductorEmpresa(declaracionJuradaProductor);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaProductorEmpresa", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    } 
    
    public MessageList insertDeclaracionJuradaMaterialEmpresa(DeclaracionJuradaMaterial declaracionJuradaMaterial) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            certificadoOrigenService.insertDeclaracionJuradaMaterialEmpresa(declaracionJuradaMaterial);
            message = new Message("insert.success");
        }
        catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.insertDeclaracionJuradaMaterialEmpresa", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        finally {
            messageList.add(message);
        }
        return messageList;

    } 
    
    
    public static HashUtil<String, String> aceptaReexportacion(HashUtil filtro) {
        HashUtil<String, String> element = new HashUtil<String, String>();
        
        Integer acuerdoId = Util.integerValueOf(filtro.getString("filter"));

        if (acuerdoId==null) return element;
        
        try {
        	
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("aceptaReexportacion", null);
        	filter.put("acuerdoId", acuerdoId);
        	
        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	ibatisService.loadElement("comun.acepta_reexportacion.element", filter);
        
        	element.put("aceptaReexportacion", filter.getString("aceptaReexportacion"));        	
        	
        } catch(Exception e) {
        	logger.error("ERROR al intentar obtener los datos del Acuerdo", e);
        }
        return element;
    }

    
    public MCT001EBXML convertirMCT001(DR dr) throws Exception {
    	MCT001EBXML mct001ebxml = new MCT001EBXML("MCT001_DR.vm");
    	
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", dr.getDrId());
        filter.put("sdr", dr.getSdr());
        
        mct001ebxml.setDr(dr);
        
    	//Obtener Certificado
    	CertificadoOrigen certificadoOrigen = (CertificadoOrigen)ibatisService.loadObject("certificado_origen.mct001.dr.datos.empresa", filter);    	
    	mct001ebxml.setCertificadoOrigen(certificadoOrigen);
    	//Obtener Factura
    	filter.put("coDrId", certificadoOrigen.getCoId());
    	List<COFactura> listCoFactura = (List<COFactura>)ibatisService.loadGenericList("certificado_origen.factura.dr.datos.empresa", filter);
    	mct001ebxml.setListCoFactura(listCoFactura);
    	//Obtener Mercancia
    	List<CertificadoOrigenMercancia> listCertificadoOrigenMercancia = (List<CertificadoOrigenMercancia>)ibatisService.loadGenericList("certificado_origen.factura.dr.datos.empresa", filter);    	
    	mct001ebxml.setListCertificadoOrigenMercancia(listCertificadoOrigenMercancia);
    	
    	return mct001ebxml;
    }
    public MCT005EBXML convertirMCT005(DR dr) throws Exception {
    	
    	MCT005EBXML mct005ebxml = new MCT005EBXML("MCT005_DR.vm"); 

    	mct005ebxml.setTemplate("MCT005_DR.vm");

        HashUtil<String, Object> filter = new HashUtil<String, Object>();

    	HashUtil<String, Object> filterUsr = new HashUtil<String, Object>();

        

        filter.put("drId", dr.getDrId());

        filter.put("sdr", dr.getSdr());

        

        Suce suceObj = suceLogic.getSuceById(dr.getSuceId().intValue());

        Orden ordenObj = ordenLogic.loadOrdenById(Long.valueOf(suceObj.getIdOrden()));

        

        filterUsr.put("orden", suceObj.getIdOrden());

        filterUsr.put("mto", ordenObj.getMto());

        

		Solicitante solicitante = formatoService.loadSolicitanteFormato(filterUsr);

		Solicitante usuario = formatoService.loadUsuarioFormato(filterUsr);




        mct005ebxml.setDr(dr);

        mct005ebxml.setSolicitante(solicitante);

        mct005ebxml.setUsuario(usuario);

        mct005ebxml.setFechaEmision(suceObj.getFechaRegistro());

    	

    	//Obtener las Declaraciones

    	DeclaracionJurada declaracionJurada=(DeclaracionJurada)ibatisService.loadObject("declaracion_jurada.mct005.dr.datos.empresa", filter);

    	mct005ebxml.setDeclaracionJurada(declaracionJurada);

    	//Obtener los productores

    	List<DeclaracionJuradaProductor> listProductores = (List<DeclaracionJuradaProductor>)ibatisService.loadGenericList("declaracion_jurada.mct005.dr.productor.empresa", filter);;

    	mct005ebxml.setListProductores(listProductores);

    	//Obtener los materiales

    	List<DeclaracionJuradaMaterial> listMateriales = (List<DeclaracionJuradaMaterial>)ibatisService.loadGenericList("declaracion_jurada.mct005.dr.material.empresa", filter);;

    	mct005ebxml.setListMateriales(listMateriales);

    	//Obtener Calificacion

    	CalificacionOrigen calificacion = null;

    	mct005ebxml.setCalificacion(calificacion);

    	//Obtener el Certificado

    	CertificadoOrigen certificadoOrigen = null;

    	mct005ebxml.setCertificadoOrigen(certificadoOrigen);

    	

    	return mct005ebxml;
    }
    
    public void setearResumenReporte(Long drId, Long sdr, Integer idAcuerdo, HashUtil<String, String> datos, HttpServletResponse response) throws Exception {
 	   try {
        	
             Map parameters = new HashMap();
	           	parameters.put("DRID", drId);
	           	parameters.put("SDR", sdr);
	           	
	           	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	           	filter.put("drId", drId);
	           	filter.put("sdr", sdr);
	           	filter.put("ordenId", null);
	           	filter.put("mto", null);
				
				JasperPrint print = new JasperPrint();
				
				String archivo = "reportes.hojaResumen.acuerdo";
	
		        if(ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo) ||ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)||ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)||
	           			ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)||ConstantesCO.AC_SGP_UE.equals(idAcuerdo)||ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)||
	           			ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)||ConstantesCO.AC_SGP_AUS.equals(idAcuerdo)){
	           		archivo="reportes.hojaResumen.sgp";
	           	}else{
	           		archivo+=idAcuerdo;
	           	}
	
	            Resource jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource(archivo, "");
	   			JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());
	   	        DBSessionContext db = (DBSessionContext)SpringContext.getApplicationContext().getBean("dbSessionContext");
	
	           	this.setearSubReportes(ConstantesCO.TIPO_REPORTE_SUCE,idAcuerdo, parameters, datos);
				
				filter.put("table", datos.getString("formato"));
	   		    filter.put("table_acuerdo", datos.getString("formato")+"_"+idAcuerdo);
	           	
				if(ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)){
		           	//Poblar tabla de detalle
		           	ibatisService.executeSPWithObject("reportes.maxItemsReports", filter);
	            }
				
				if(ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){
	            	int inicio = 1;
	   		    	int fin = 1;
	   		    	int paginas = 0;
	   		    	int lineasTotales = 0;
	   		    	int lineasDescripcion = 0;
	   		    	int lineasFactura = 0;
	   		    	int lineasCantidad = 0;
	   		    	int lineas = 0;
	   		    	boolean faltaImprimir=false;
	   		    	String descripcion;
	   		    	String factura;
	   		    	String cantidad;
	   		    	String observacion1;
	   		    	String observacion2;
	   		    	String observacion3;
	   		    	JasperPrint printDet;
	   		    	
	   		    	String observacionCab = Util.stringValueOf(ibatisService.loadObject("certificadoOrigen.observacionCAB.26.dr.element", filter));
	   		    	String cadenaObservaciones = (observacionCab!=null)?observacionCab:"";
	   		    	String concatObs ="";
	   		    	int numMercancias = Util.integerValueOf(ibatisService.loadObject("certificadoOrigen.numDetalle.26.dr.element", filter));
					
					System.out.println("****numMercancias: "+numMercancias);
					while (fin <= numMercancias){
	   		    		
	   		    	    filter.put("item", fin);
	   		    		descripcion = ibatisService.loadElement("certificadoOrigen.descripcion.26.dr.element", filter).getString("DESCRIPCION");
	   		    		factura = ibatisService.loadElement("certificadoOrigen.descripcion.26.dr.element", filter).getString("FACTURA");
	   		    		cantidad = ibatisService.loadElement("certificadoOrigen.descripcion.26.dr.element", filter).getString("CANTIDAD");
	   		    		observacion1 = ibatisService.loadElement("certificadoOrigen.descripcion.26.dr.element", filter).getString("OBSERVACION1").trim();
	   		    		observacion2 = ibatisService.loadElement("certificadoOrigen.descripcion.26.dr.element", filter).getString("OBSERVACION2").trim();
	   		    		observacion3 = ibatisService.loadElement("certificadoOrigen.descripcion.26.dr.element", filter).getString("OBSERVACION3").trim();
	   		    		concatObs = observacion1+observacion2+observacion3;
	   		    		concatObs = concatObs.trim();
	   		    		
	   		    		lineasDescripcion = (descripcion.length() / 47) + ((descripcion.length() % 47) > 0 ? 1:0);
	   		    		lineasFactura = (factura.length() / 18) + ((factura.length() % 18) > 0 ? 1:0);
	   		    		lineasCantidad = (cantidad.length() / 14) + ((cantidad.length() % 14) > 0 ? 1:0);
	   		    		
	   		    		lineas = (lineasDescripcion > lineasFactura) ? lineasDescripcion : lineasFactura;
	   		    		lineas = (lineas > lineasCantidad) ? lineas : lineasCantidad;
	   		    		
	   		    		if(lineasTotales + lineas < 30){
	   		    			lineasTotales += lineas;
	   		    			faltaImprimir = true;
	   		    			if(concatObs.length() > 0) cadenaObservaciones+=" "+fin+"."+observacion1+" "+observacion2+" "+observacion3;
	   		    			fin ++;
	   		    			
	   		    		}else{
	   		    			
	   		    			fin --;
	   		    			System.out.println("inicio: "+inicio);
	   		    			System.out.println("fin: "+fin);
	   		    			parameters.put("P_INICIO", inicio);
	   		    			parameters.put("P_FIN", fin );
	   		    			parameters.put("P_OBSERVACION", cadenaObservaciones );
	   		    			faltaImprimir = false;
	   		    			
	   		    			//Para los siguientes
	   		    			inicio = ++fin;
	   		    			//fin ++;
	   		    			lineasTotales = 0;
	   		    		
	   		    			printDet = this.ejecutarReporteConConexionBD(jReport, parameters);

	   		    		    if(paginas > 0){
	   		    		       List pages = printDet.getPages();
		   		               for (int k = 0; k < pages.size(); k++) {
		   		                    JRPrintPage object = (JRPrintPage)pages.get(k);
		   		                    print.addPage(object);
		   		                  }
	   		    		    } else {
	   		    		        //En el caso de la primera p�gina;
	   		    		    	print = printDet; 
	   		   		    		//print = this.ejecutarReporteConConexionBD(jReport, parameters);
	   		    		    }
	
	   		    			paginas ++;
	   		    			cadenaObservaciones = "";
	   		    		}

	   		    	}
					
					if(faltaImprimir){
						System.out.println("****falta Imprimir");
						System.out.println("inicio: "+inicio);
   		    			System.out.println("fin: "+fin);
		    			parameters.put("P_INICIO", inicio);
		    			parameters.put("P_FIN", fin );
		    			parameters.put("P_OBSERVACION", cadenaObservaciones );
		    			printDet = this.ejecutarReporteConConexionBD(jReport, parameters);

		    		    if(paginas > 0){
		    		       List pages = printDet.getPages();
	   		               for (int k = 0; k < pages.size(); k++) {
	   		                    JRPrintPage object = (JRPrintPage)pages.get(k);
	   		                    print.addPage(object);
	   		                  }
		    		    } else {
		    		        //En el caso de la primera p�gina;
		    		    	print = printDet; 
		   		    		//print = this.ejecutarReporteConConexionBD(jReport, parameters);
		    		    }
		    		}
	   		    	
	   		    }else{
		   		    print = this.ejecutarReporteConConexionBD(jReport, parameters);	   		    	
	   		    }
				
				if(ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)||ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){ 
	   			    	String anexoProductor = Util.stringValueOf(ibatisService.loadObject("certificadoOrigen.anexo_productor.dr.element", filter));
	   			    	if("S".equals(anexoProductor)){
		   		            Resource productores = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.productores.dr.acuerdo"+idAcuerdo, "");
		   	
		   		            JasperReport jReportProd = (JasperReport)JRLoader.loadObject(productores.getInputStream());
		   	
		   		            try {
		   	
		   		            	JasperPrint printProd = this.ejecutarReporteConConexionBD(jReportProd, parameters);
		   	
		   		            	List pages = printProd .getPages();
		   		            	
		   		            	for (int j = 0; j < pages.size(); j++) {
		   		                    JRPrintPage object = (JRPrintPage)pages.get(j);
		   		                    print.addPage(object);
		   	
		   		                  }
		   	
		   		            } catch (Exception e) {
		   		                logger.error("Ocurrio un error al intentar crear el archivo", e);
		   		            }
	   	            
	   	            } 
	   			    }
					
					 if(ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)||ConstantesCO.AC_MEXICO.equals(idAcuerdo)||ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)
	   		    		||ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)
	   		    		||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)||ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){            
	   	            Resource instructivo = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.instructivo.acuerdo"+idAcuerdo, "");
	
	   	            JasperReport jReportIns = (JasperReport)JRLoader.loadObject(instructivo.getInputStream());
	
	   	            try {
	
	   	            	JasperPrint printInst = JasperFillManager.fillReport(jReportIns, null);
	
	   	            	List pages = printInst .getPages();
	   	            	
	   	            	for (int j = 0; j < pages.size(); j++) {
	   	                    JRPrintPage object = (JRPrintPage)pages.get(j);
	   	                    print.addPage(object);
	
	   	                  }
	
	   	            } catch (Exception e) {
	   	                logger.error("Ocurrio un error al intentar crear el archivo", e);
	   	            }
	               
	               } 
	
	               response.setContentType("application/pdf");
	               response.setHeader("Content-Disposition", "inline;filename=HojaResumen.pdf");
	               OutputStream os = response.getOutputStream();
	               JasperExportManager.exportReportToPdfStream(print, os);
	               os.close();
	               
	           } catch (JRException e) {
	               e.printStackTrace();
	           } catch (IOException e) {
	               e.printStackTrace();
	           } catch (IllegalArgumentException e) {
	   			e.printStackTrace();
	   		} catch (SecurityException e) {
	   			e.printStackTrace();
	   		} catch (Exception e) {
	   			e.printStackTrace();
	   		}
			

 }
    /**
     * Genera el PDF del Certificado de Origen recibido por el Pais de Alianza
     */
    public byte[] generarReporteAPBytes(Integer codId) throws Exception {
    	logger.debug("------ GENERA y REGISTRA REPORTE PDF CO ALIANZA ----CO_ID: "+codId);
    	int acuerdoId = ConstantesCO.AC_ALIANZA_PACIFICO;
    	byte [] result = null;
    	
    	 try {
         	
             Map parameters = new HashMap();
	           	parameters.put("COID", codId);
	           	
	           	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	           	filter.put("COID", codId);
	           	
	           	Resource jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen.iop.acuerdo" + acuerdoId, "");
	           	
	           	Resource jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen.iop.acuerdo" + acuerdoId + ".subreporte1", "");
	           	
	        	if (jasperFileSubReport1!=null && jasperFileSubReport1.exists()) {
	        		JasperReport jReportSubReport1 = (JasperReport)JRLoader.loadObject(jasperFileSubReport1.getInputStream());
	        		parameters.put("P_SUB_REPORTE_1", jReportSubReport1);
	        	}
    	
	   			JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());
	   			DBSessionContext db = (DBSessionContext)SpringContext.getApplicationContext().getBean("dbSessionContext");
	        	
	        	JasperPrint print = new JasperPrint();

	            int inicio = 1;
	   		    int fin = 1;
	   		    int paginas = 0;
	   		    int lineasTotales = 0;
	   		    int lineasDescripcion = 0;
	   		    int lineasFactura = 0;
	   		    int lineasCantidad = 0;
	   		    int lineas = 0;
	   		    boolean faltaImprimir=false;
	   		    String descripcion;
	   		    String factura;
	   		    String cantidad;
	   		    String observacion1;
	   		    String observacion2;
	   		    String observacion3;
	   		    
	   		    JasperPrint printDet;
	   		    
	   		    String observacionCab = Util.stringValueOf(ibatisService.loadObject("certificadoOrigen.iop.observacionCAB.26.element", filter)); 	
	   		    String cadenaObservaciones = (observacionCab!=null)?observacionCab:"";
	   		    String concatObs ="";
	   		    
	   		    int numMercancias = Util.integerValueOf(ibatisService.loadObject("certificadoOrigen.iop.numDetalle.26.element", filter));
	   		    
	   		    System.out.println("N�mero Mercancias: "+numMercancias);
	   		    
	   		    while (fin <= numMercancias){
	   		    		
	   		    	    filter.put("item", fin);
	   		    		descripcion = ibatisService.loadElement("certificadoOrigen.iop.descripcion.26.element", filter).getString("DESCRIPCION");
	   		    		factura = ibatisService.loadElement("certificadoOrigen.iop.descripcion.26.element", filter).getString("FACTURA");
	   		    		cantidad = ibatisService.loadElement("certificadoOrigen.iop.descripcion.26.element", filter).getString("CANTIDAD");
	   		    		observacion1 = ibatisService.loadElement("certificadoOrigen.iop.descripcion.26.element", filter).getString("OBSERVACION1").trim();
	   		    		observacion2 = ibatisService.loadElement("certificadoOrigen.iop.descripcion.26.element", filter).getString("OBSERVACION2").trim();
	   		    		observacion3 = ibatisService.loadElement("certificadoOrigen.iop.descripcion.26.element", filter).getString("OBSERVACION3").trim();
	   		    		concatObs = observacion1+observacion2+observacion3;
	   		    		concatObs = concatObs.trim();

	   		    		lineasDescripcion = (descripcion.length() / 47) + ((descripcion.length() % 47) > 0 ? 1:0);
	   		    		lineasFactura = (factura.length() / 18) + ((factura.length() % 18) > 0 ? 1:0);
	   		    		lineasCantidad = (cantidad.length() / 14) + ((cantidad.length() % 14) > 0 ? 1:0);
	   		    		
	   		    		lineas = (lineasDescripcion > lineasFactura) ? lineasDescripcion : lineasFactura;
	   		    		lineas = (lineas > lineasCantidad) ? lineas : lineasCantidad;
	   		    		
	   		    		if(lineasTotales + lineas < 30){
	   		    			lineasTotales += lineas;
	   		    			faltaImprimir = true;
	   		    			if(concatObs.length() > 0) cadenaObservaciones+=" "+fin+"."+observacion1+" "+observacion2+" "+observacion3;
	   		    			fin ++;
	   		    			
	   		    		}else{
	   		    			
	   		    			System.out.println("Inicio: "+inicio);
	   		    			System.out.println("Fin: "+fin);
	   		    			
	   		    			parameters.put("P_INICIO", inicio);
	   		    			parameters.put("P_FIN", fin );
	   		    			parameters.put("P_OBSERVACION", cadenaObservaciones );
	   		    			faltaImprimir = false;
	   		    			
	   		    			//Para los siguientes
	   		    			inicio = ++fin;
	   		    			//fin ++;
	   		    			lineasTotales = 0;
	   		    		
	   		    			printDet = this.ejecutarReporteConConexionBD(jReport, parameters);

	   		    		    if(paginas > 0){
	   		    		       List pages = printDet.getPages();
		   		               for (int k = 0; k < pages.size(); k++) {
		   		                    JRPrintPage object = (JRPrintPage)pages.get(k);
		   		                    print.addPage(object);
		   		                  }
	   		    		    } else {
	   		    		        //En el caso de la primera p�gina;
	   		    		    	print = printDet; 
	   		   		    		//print = this.ejecutarReporteConConexionBD(jReport, parameters);
	   		    		    }
	
	   		    			paginas ++;
	   		    			cadenaObservaciones = "";
	   		    		}

	   		    	}
	   		    	
   		    		if(faltaImprimir){
   		    			System.out.println("Falta imprimir: ");
   		    			System.out.println("Inicio: "+inicio);
   		    			System.out.println("Fin: "+fin);
   		    			
   		    			parameters.put("P_INICIO", inicio);
   		    			parameters.put("P_FIN", fin );
   		    			parameters.put("P_OBSERVACION", cadenaObservaciones );
   		    			printDet = this.ejecutarReporteConConexionBD(jReport, parameters);

   		    		    if(paginas > 0){
   		    		       List pages = printDet.getPages();
	   		               for (int k = 0; k < pages.size(); k++) {
	   		                    JRPrintPage object = (JRPrintPage)pages.get(k);
	   		                    print.addPage(object);
	   		                  }
   		    		    } else {
   		    		        //En el caso de la primera p�gina;
   		    		    	print = printDet; 
   		   		    		//print = this.ejecutarReporteConConexionBD(jReport, parameters);
   		    		}
   		    	}


	   		    
	   		    String imprimeAnexo = Util.stringValueOf(ibatisService.loadObject("firma.imprimeAnexo.element", filter));
	   		    
	   		    
		    	if("S".equals(imprimeAnexo)){	
	   		    
			   		Resource productores = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.productores.iop.acuerdo"+acuerdoId, "");
			   		JasperReport jReportProd = (JasperReport)JRLoader.loadObject(productores.getInputStream());
			   		     try {
			   		        JasperPrint printProd = this.ejecutarReporteConConexionBD(jReportProd, parameters);
			   	            List pages = printProd .getPages();
			   		        for (int j = 0; j < pages.size(); j++) {
			   		             JRPrintPage object = (JRPrintPage)pages.get(j);
			   		             print.addPage(object);
			   	                 }
			   		      } catch (Exception e) {
			   		                logger.error("Ocurrio un error al intentar crear el archivo", e);
			   		      }
		    	} 
		   		     
          
	   	       Resource instructivo = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.instructivo.acuerdo"+acuerdoId, "");
	           JasperReport jReportIns = (JasperReport)JRLoader.loadObject(instructivo.getInputStream());
	               try {
	                    JasperPrint printInst = JasperFillManager.fillReport(jReportIns, null);
	   	            	List pages = printInst .getPages();
	   	            	for (int j = 0; j < pages.size(); j++) {
	   	                    JRPrintPage object = (JRPrintPage)pages.get(j);
	   	                    print.addPage(object);
	   	                  }
	   	            } catch (Exception e) {
	   	                logger.error("Ocurrio un error al intentar crear el archivo", e);
	   	            }
	               
	               ByteArrayOutputStream baos = new ByteArrayOutputStream();
	               JasperExportManager.exportReportToPdfStream(print, baos);
	               baos.close();
	               result = baos.toByteArray();
	           } catch (JRException e) {
	               e.printStackTrace();
	           } catch (IOException e) {
	               e.printStackTrace();
	           } catch (IllegalArgumentException e) {
	   			e.printStackTrace();
	   		} catch (SecurityException e) {
	   			e.printStackTrace();
	   		} catch (Exception e) {
	   			e.printStackTrace();
	   		}
       return result;
    }
    
    public byte[] generarReporteBytes(Long drId, Long sdr, Integer idAcuerdo, String formato) throws Exception {
       byte [] result = null;
 	   try {
        	
             Map parameters = new HashMap();
	           	parameters.put("DRID", drId);
	           	parameters.put("SDR", sdr);
	           	
	           	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	           	filter.put("drId", drId);
	           	filter.put("sdr", sdr);
	           	filter.put("ordenId", null);
	           	filter.put("mto", null);
	
	           	this.setearSubReportesParaGeneracionPorBytes(ConstantesCO.TIPO_REPORTE_SUCE, idAcuerdo, formato, parameters);
	           	
	           	Resource jasperFile;
	           	
	           	if(ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo) ||ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)||ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)||
	           			ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)||ConstantesCO.AC_SGP_UE.equals(idAcuerdo)||ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)||
	           			ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)){
	           				jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen.sgp", "");
	           	} else {
	           			jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen.acuerdo" + idAcuerdo, "");

	           	}
	
	            if(ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)){
		           	//Poblar tabla de detalle
		           	ibatisService.executeSPWithObject("reportes.maxItemsReports", filter);
	            }
	           	
	           	//ctx.getResource("WEB-INF/resource/reportes/hojaResumen.jasper");
	           	JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());
	
	   	        DBSessionContext db = (DBSessionContext)SpringContext.getApplicationContext().getBean("dbSessionContext");
	   		    JasperPrint print = this.ejecutarReporteConConexionBD(jReport, parameters);
	   		    
	   		    filter.put("table", formato);
	   		    filter.put("table_acuerdo", formato+"_"+idAcuerdo);
	   	            	
	   	        //Integer numProductores = Util.integerValueOf(ibatisService.loadObject("certificadoOrigen.cantidadProductores.dr.count", filter));     	
	   	                		
	   			if(ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)||ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){ 
	   			    	String anexoProductor = Util.stringValueOf(ibatisService.loadObject("certificadoOrigen.anexo_productor.dr.element", filter));
	   			    	if("S".equals(anexoProductor)){
		   		            Resource productores = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.productores.dr.acuerdo"+idAcuerdo, "");
		   	
		   		            JasperReport jReportProd = (JasperReport)JRLoader.loadObject(productores.getInputStream());
		   	
		   		            try {
		   	
		   		            	JasperPrint printProd = this.ejecutarReporteConConexionBD(jReportProd, parameters);
		   	
		   		            	List pages = printProd .getPages();
		   		            	
		   		            	for (int j = 0; j < pages.size(); j++) {
		   		                    JRPrintPage object = (JRPrintPage)pages.get(j);
		   		                    print.addPage(object);
		   	
		   		                  }
		   	
		   		            } catch (Exception e) {
		   		                logger.error("Ocurrio un error al intentar crear el archivo", e);
		   		            }
	   	            
	   	            } 
	   			    }
	   		    
	   		    
	   		    if(ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)||ConstantesCO.AC_MEXICO.equals(idAcuerdo)||ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)
	   		    		||ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)
	   		    		||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)||ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){            
	   	            Resource instructivo = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.instructivo.acuerdo"+idAcuerdo, "");
	
	   	            JasperReport jReportIns = (JasperReport)JRLoader.loadObject(instructivo.getInputStream());
	
	   	            try {
	
	   	            	JasperPrint printInst = JasperFillManager.fillReport(jReportIns, null);
	
	   	            	List pages = printInst .getPages();
	   	            	
	   	            	for (int j = 0; j < pages.size(); j++) {
	   	                    JRPrintPage object = (JRPrintPage)pages.get(j);
	   	                    print.addPage(object);
	
	   	                  }
	
	   	            } catch (Exception e) {
	   	                logger.error("Ocurrio un error al intentar crear el archivo", e);
	   	            }
	               
	               } 
	
	               //response.setContentType("application/pdf");
	               //response.setHeader("Content-Disposition", "inline;filename=HojaResumen.pdf");
	               ByteArrayOutputStream baos = new ByteArrayOutputStream();
	               JasperExportManager.exportReportToPdfStream(print, baos);
	               baos.close();
	               result = baos.toByteArray();
	           } catch (JRException e) {
	               e.printStackTrace();
	           } catch (IOException e) {
	               e.printStackTrace();
	           } catch (IllegalArgumentException e) {
	   			e.printStackTrace();
	   		} catch (SecurityException e) {
	   			e.printStackTrace();
	   		} catch (Exception e) {
	   			e.printStackTrace();
	   		}
       return result;

    }
    
    public void setearAvanceReporte(Long ordenId, Long mto, Integer idAcuerdo, HashUtil<String, String> datos, HttpServletResponse response) throws Exception {
 	   try {
        	
             Map parameters = new HashMap();
	           	parameters.put("ORDENID", ordenId);
	           	parameters.put("MTO", mto);
	           	
	           	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	           	filter.put("ordenId", ordenId);
	           	filter.put("mto", mto);
	           	filter.put("drId", null);
	           	filter.put("sdr", null);
	           	
	           	JasperPrint print = new JasperPrint();
	           	
	           	String archivo = "reportes.hojaResumen.avance.acuerdo";
	           	
	           	if(ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo) ||ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)||ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)||
	           			ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)||ConstantesCO.AC_SGP_UE.equals(idAcuerdo)||ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)||
	           			ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)||ConstantesCO.AC_SGP_AUS.equals(idAcuerdo)){
	           		archivo="reportes.hojaResumen.avance.sgp";
	           	}else{
	           		archivo+=idAcuerdo;
	           	}
	           	
	           	Resource jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource(archivo, "");
	   			JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());
	   	        DBSessionContext db = (DBSessionContext)SpringContext.getApplicationContext().getBean("dbSessionContext");
	
	           	this.setearSubReportes(ConstantesCO.TIPO_REPORTE_AVANCE,idAcuerdo, parameters, datos);

	   		    filter.put("table", datos.getString("formato"));
	   		    filter.put("table_acuerdo", datos.getString("formato")+"_"+idAcuerdo);
	           	
	            //Poblar tabla de detalle
	            if(ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)){
		           	ibatisService.executeSPWithObject("reportes.maxItemsReports", filter);
	            }
	   		    

	            if(ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){
	            	int inicio = 1;
	   		    	int fin = 1;
	   		    	int paginas = 0;
	   		    	int lineasTotales = 0;
	   		    	int lineasDescripcion = 0;
	   		    	int lineasFactura = 0;
	   		    	int lineasCantidad = 0;
	   		    	int lineas = 0;
	   		    	boolean faltaImprimir=false;
	   		    	String descripcion;
	   		    	String factura;
	   		    	String cantidad;
	   		    	String observacion1;
	   		    	String observacion2;
	   		    	String observacion3;
	   		    	JasperPrint printDet;
	   		    	
	   		    	String observacionCab = Util.stringValueOf(ibatisService.loadObject("certificadoOrigen.observacionCAB.26.element", filter));
	   		    	String cadenaObservaciones = (observacionCab!=null)?observacionCab:"";
	   		    	String concatObs ="";
	   		    	int numMercancias = Util.integerValueOf(ibatisService.loadObject("certificadoOrigen.numDetalle.26.element", filter));
	   		    	
	   		    	while (fin <= numMercancias){
	   		    		
	   		    	    filter.put("item", fin);
	   		    		descripcion = ibatisService.loadElement("certificadoOrigen.descripcion.26.element", filter).getString("DESCRIPCION");
	   		    		factura = ibatisService.loadElement("certificadoOrigen.descripcion.26.element", filter).getString("FACTURA");
	   		    		cantidad = ibatisService.loadElement("certificadoOrigen.descripcion.26.element", filter).getString("CANTIDAD");
	   		    		observacion1 = ibatisService.loadElement("certificadoOrigen.descripcion.26.element", filter).getString("OBSERVACION1").trim();
	   		    		observacion2 = ibatisService.loadElement("certificadoOrigen.descripcion.26.element", filter).getString("OBSERVACION2").trim();
	   		    		observacion3 = ibatisService.loadElement("certificadoOrigen.descripcion.26.element", filter).getString("OBSERVACION3").trim();
	   		    		concatObs = observacion1+observacion2+observacion3;
	   		    		concatObs = concatObs.trim();

	   		    		lineasDescripcion = (descripcion.length() / 47) + ((descripcion.length() % 47) > 0 ? 1:0);
	   		    		lineasFactura = (factura.length() / 18) + ((factura.length() % 18) > 0 ? 1:0);
	   		    		lineasCantidad = (cantidad.length() / 14) + ((cantidad.length() % 14) > 0 ? 1:0);
	   		    		
	   		    		lineas = (lineasDescripcion > lineasFactura) ? lineasDescripcion : lineasFactura;
	   		    		lineas = (lineas > lineasCantidad) ? lineas : lineasCantidad;
	   		    		
	   		    		if(lineasTotales + lineas < 30){
	   		    			lineasTotales += lineas;
	   		    			faltaImprimir = true;
	   		    			if(concatObs.length() > 0) cadenaObservaciones+=" "+fin+"."+observacion1+" "+observacion2+" "+observacion3;
	   		    			fin ++;
	   		    			
	   		    		}else{
	   		    			
	   		    			fin --;
	   		    			parameters.put("P_INICIO", inicio);
	   		    			parameters.put("P_FIN", fin );
	   		    			parameters.put("P_OBSERVACION", cadenaObservaciones );
	   		    			faltaImprimir = false;
	   		    			
	   		    			//Para los siguientes
	   		    			inicio = ++fin;
	   		    			//fin ++;
	   		    			lineasTotales = 0;
	   		    		
	   		    			printDet = this.ejecutarReporteConConexionBD(jReport, parameters);

	   		    		    if(paginas > 0){
	   		    		       List pages = printDet.getPages();
		   		               for (int k = 0; k < pages.size(); k++) {
		   		                    JRPrintPage object = (JRPrintPage)pages.get(k);
		   		                    print.addPage(object);
		   		                  }
	   		    		    } else {
	   		    		        //En el caso de la primera p�gina;
	   		    		    	print = printDet; 
	   		   		    		//print = this.ejecutarReporteConConexionBD(jReport, parameters);
	   		    		    }
	
	   		    			paginas ++;
	   		    			cadenaObservaciones = "";
	   		    		}

	   		    	}
	   		    	
   		    		if(faltaImprimir){
   		    			
   		    			parameters.put("P_INICIO", inicio);
   		    			parameters.put("P_FIN", fin );
   		    			parameters.put("P_OBSERVACION", cadenaObservaciones );
   		    			printDet = this.ejecutarReporteConConexionBD(jReport, parameters);

   		    		    if(paginas > 0){
   		    		       List pages = printDet.getPages();
	   		               for (int k = 0; k < pages.size(); k++) {
	   		                    JRPrintPage object = (JRPrintPage)pages.get(k);
	   		                    print.addPage(object);
	   		                  }
   		    		    } else {
   		    		        //En el caso de la primera p�gina;
   		    		    	print = printDet; 
   		   		    		//print = this.ejecutarReporteConConexionBD(jReport, parameters);
   		    		    }
   		    		}
	   		    	
	   		    }else{
		   		    print = this.ejecutarReporteConConexionBD(jReport, parameters);	   		    	
	   		    }
	            	   	       
	   			if(ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)
	   			    		||ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){ 
	   			    	
	   			    	String anexoProductor = Util.stringValueOf(ibatisService.loadObject("certificadoOrigen.anexo_productor.element", filter));
	   			    	if("S".equals(anexoProductor)){	
		   		            Resource productores = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.productores.acuerdo"+idAcuerdo, "");
		   	
		   		            JasperReport jReportProd = (JasperReport)JRLoader.loadObject(productores.getInputStream());
		   	
		   		            try {
		   	
		   		            	JasperPrint printProd = this.ejecutarReporteConConexionBD(jReportProd, parameters);
		   	
		   		            	List pages = printProd.getPages();
		   		            	
		   		            	for (int j = 0; j < pages.size(); j++) {
		   		                    JRPrintPage object = (JRPrintPage)pages.get(j);
		   		                    print.addPage(object);
		   	
		   		                  }
		   	
		   		            } catch (Exception e) {
		   		                logger.error("Ocurrio un error al intentar crear el archivo", e);
		   		            }
		   	            
		   	            } 
	   	        }
	   		    
	   		    
	   		    if(ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)||ConstantesCO.AC_MEXICO.equals(idAcuerdo)||ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)
	   		    		||ConstantesCO.AC_PANAMA.equals(idAcuerdo)||ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)||ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)
	   		    		||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)||ConstantesCO.AC_ALIANZA_PACIFICO.equals(idAcuerdo)){            
	   	            Resource instructivo = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.instructivo.acuerdo"+idAcuerdo, "");
	
	   	            JasperReport jReportIns = (JasperReport)JRLoader.loadObject(instructivo.getInputStream());
	
	   	            try {
	
	   	            	JasperPrint printInst = JasperFillManager.fillReport(jReportIns, null);
	
	   	            	List pages = printInst .getPages();
	   	            	
	   	            	for (int j = 0; j < pages.size(); j++) {
	   	                    JRPrintPage object = (JRPrintPage)pages.get(j);
	   	                    print.addPage(object);
	
	   	                  }
	
	   	            } catch (Exception e) {
	   	                logger.error("Ocurrio un error al intentar crear el archivo", e);
	   	            }
	               
	               } 
	
	               response.setContentType("application/pdf");
	               response.setHeader("Content-Disposition", "inline;filename=HojaResumenAvance.pdf");
	               OutputStream os = response.getOutputStream();
	               JasperExportManager.exportReportToPdfStream(print, os);
	               os.close();
	               
	           } catch (JRException e) {
	               e.printStackTrace();
	           } catch (IOException e) {
	               e.printStackTrace();
	           } catch (IllegalArgumentException e) {
	   			e.printStackTrace();
	   		} catch (SecurityException e) {
	   			e.printStackTrace();
	   		} catch (Exception e) {
	   			e.printStackTrace();
	   		}

 }
 
    
    private void setearSubReportes(String tipoReporte, Integer idAcuerdo, Map parameters, HashUtil<String, String> datos) throws JRException, IOException{

    	parameters.put("P_VISTA_CAB_CLAUSE", datos.getString("formato").toUpperCase());
    	parameters.put("P_VISTA_DET_CLAUSE", datos.getString("formato").toUpperCase());
    	
    	Resource jasperFileSubReport1;
    	
    	if((ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo) ||ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)||ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)||
    			ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)||ConstantesCO.AC_SGP_UE.equals(idAcuerdo)||ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)||
    			ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)||ConstantesCO.AC_SGP_AUS.equals(idAcuerdo))){
    		    parameters.put("P_ACUERDO", idAcuerdo.toString());
    			jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+ tipoReporte +"sgp.subreporte1", "");
    	} else{
	    	jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+ tipoReporte +"acuerdo" + idAcuerdo + ".subreporte1", "");
	    	System.out.println("reportes.hojaResumen."+ tipoReporte +"acuerdo" + idAcuerdo + ".subreporte1");
    	}
    	
    	if (jasperFileSubReport1!=null && jasperFileSubReport1.exists()) {
    		JasperReport jReportSubReport1 = (JasperReport)JRLoader.loadObject(jasperFileSubReport1.getInputStream());
    		parameters.put("P_SUB_REPORTE_1", jReportSubReport1);
    	}

    	if(ConstantesCO.AC_CHILE.equals(idAcuerdo) || ConstantesCO.AC_CUBA.equals(idAcuerdo) || ConstantesCO.AC_MEXICO.equals(idAcuerdo)|| ConstantesCO.AC_MERCOSUR.equals(idAcuerdo)||
    			ConstantesCO.AC_CAN.equals(idAcuerdo)|| ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)|| ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)|| ConstantesCO.AC_PANAMA.equals(idAcuerdo)||
    			ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)|| ConstantesCO.AC_COREA.equals(idAcuerdo)|| ConstantesCO.AC_CHINA.equals(idAcuerdo)|| ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)){
        	Resource jasperFileSubReport2 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte2", "");
        	if (jasperFileSubReport2!=null && jasperFileSubReport2.exists()) {
        		JasperReport jReportSubReport2 = (JasperReport)JRLoader.loadObject(jasperFileSubReport2.getInputStream());
        		parameters.put("P_SUB_REPORTE_2", jReportSubReport2);
        	}
    	}

    	if(ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)){
        	Resource jasperFileSubReport3 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte3", "");
        	if (jasperFileSubReport3!=null && jasperFileSubReport3.exists()) {
        		JasperReport jReportSubReport3 = (JasperReport)JRLoader.loadObject(jasperFileSubReport3.getInputStream());
        		parameters.put("P_SUB_REPORTE_3", jReportSubReport3);
        	}
    	}
    }
    
    private void setearSubReportesParaGeneracionPorBytes(String tipoReporte, Integer idAcuerdo, String formato, Map parameters) throws JRException, IOException{

    	parameters.put("P_VISTA_CAB_CLAUSE", formato);
    	parameters.put("P_VISTA_DET_CLAUSE", formato);
    	
    	Resource jasperFileSubReport1;
    	
    	if((ConstantesCO.AC_SGP_RUSIA.equals(idAcuerdo) ||ConstantesCO.AC_SGP_JAPON.equals(idAcuerdo)||ConstantesCO.AC_SGP_NORUEGA.equals(idAcuerdo)||
    			ConstantesCO.AC_SGP_NZ.equals(idAcuerdo)||ConstantesCO.AC_SGP_UE.equals(idAcuerdo)||ConstantesCO.AC_SGP_CANADA.equals(idAcuerdo)||
    			ConstantesCO.AC_SGP_TURQUIA.equals(idAcuerdo)||ConstantesCO.AC_SGP_AUS.equals(idAcuerdo))){
    			jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+ tipoReporte +"sgp.subreporte1", "");
    	} else{
       			jasperFileSubReport1 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+ tipoReporte +"acuerdo" + idAcuerdo + ".subreporte1", "");
   
       	}
    	
    	if (jasperFileSubReport1!=null && jasperFileSubReport1.exists()) {
    		JasperReport jReportSubReport1 = (JasperReport)JRLoader.loadObject(jasperFileSubReport1.getInputStream());
    		parameters.put("P_SUB_REPORTE_1", jReportSubReport1);
    	}

    	if(ConstantesCO.AC_CHILE.equals(idAcuerdo) || ConstantesCO.AC_CUBA.equals(idAcuerdo) || ConstantesCO.AC_MEXICO.equals(idAcuerdo)|| ConstantesCO.AC_MERCOSUR.equals(idAcuerdo)||
    			ConstantesCO.AC_CAN.equals(idAcuerdo)|| ConstantesCO.AC_TLC_JAPON.equals(idAcuerdo)|| ConstantesCO.AC_COSTARRICA.equals(idAcuerdo)|| ConstantesCO.AC_PANAMA.equals(idAcuerdo)||
    			ConstantesCO.AC_GUATEMALA.equals(idAcuerdo)||ConstantesCO.AC_HONDURAS.equals(idAcuerdo)|| ConstantesCO.AC_COREA.equals(idAcuerdo)|| ConstantesCO.AC_CHINA.equals(idAcuerdo)|| ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)){
        	Resource jasperFileSubReport2 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte2", "");
        	if (jasperFileSubReport2!=null && jasperFileSubReport2.exists()) {
        		JasperReport jReportSubReport2 = (JasperReport)JRLoader.loadObject(jasperFileSubReport2.getInputStream());
        		parameters.put("P_SUB_REPORTE_2", jReportSubReport2);
        	}
    	}

    	if(ConstantesCO.AC_TLC_VENEZUELA.equals(idAcuerdo)){
        	Resource jasperFileSubReport3 = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.hojaResumen."+tipoReporte+"acuerdo" + idAcuerdo + ".subreporte3", "");
        	if (jasperFileSubReport3!=null && jasperFileSubReport3.exists()) {
        		JasperReport jReportSubReport3 = (JasperReport)JRLoader.loadObject(jasperFileSubReport3.getInputStream());
        		parameters.put("P_SUB_REPORTE_3", jReportSubReport3);
        	}
    	}
    }
    

    public byte[] descargarDocFDById(long codId){
    	MessageList messageList = new MessageList();
        Message message = null;
        byte [] bytes = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        try {
        	bytes = certificadoOrigenService.loadCertificadoFD(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoOrigenLogicImpl.descargarDocFDById", e);
		}
        return bytes;
    }
    //NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
    public byte[] descargarDocFDXMLById(long codId){
    	MessageList messageList = new MessageList();
        Message message = null;
        byte [] bytes = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        try {
        	bytes = certificadoOrigenService.loadCertificadoFDXML(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoOrigenLogicImpl.descargarDocFDById", e);
		}
        return bytes;
    }
    
    public void eliminarMercanciasXML(int codId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        filter.put("secuencia", null);
        try {
        	logger.debug("Eliminando Registro de Productor Mercancia *********************** codId:"+codId);
        	System.out.println("Eliminando Registro de Productor Mercancia *********************** codId:"+codId);
        	certificadoOrigenService.eliminarProductorMercanciasXML(filter);
        	logger.debug("Eliminando Registro de Mercancia *********************** codId:"+codId);
        	System.out.println("Eliminando Registro de Mercancia *********************** codId:"+codId);
            certificadoOrigenService.eliminarMercanciasXML(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.eliminarMercanciasXMLcodId:", e);
        }
    }
    

    public String validaCOFD(Integer idCertificado) {
        String resultado = "";
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("idCertificado", idCertificado);
            resultado = certificadoOrigenService.validaCOFD(filter);

        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.validaCOFD", e);
        }
      
        return resultado;
    }

	public void insertArchivoCOFD(byte[] bytes, Integer idCertificado)  {
		 HashUtil<String, Object> filter = new HashUtil<String, Object>();
		 try {
	            filter.put("idCertificado", idCertificado);
	            filter.put("archivo", bytes);
	            certificadoOrigenService.insertArchivoCOFD(filter);

	        } catch (Exception e) {
	            logger.error("Error en CertificadoOrigenLogicImpl.insertArchivoCOFD", e);
	        }
	}
	
    public Message habilitaFirma(Long drId, Integer sdr) {
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("drId", drId);
            filter.put("sdr", sdr);            
            certificadoOrigenService.habilitaFirma(filter);
            } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.habilitaFirma", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        return message;
    }
    
    //NPCS 26/02/2019 CO-2019-001 IO Alianza
    public static HashUtil<String, String> mostrarMensajeFD(HashUtil filtros) {
        HashUtil<String, String> element = new HashUtil<String, String>();
        
        String[] filtro = filtros.getString("filter").split("[|]");
       //Ticket 149343 GCHAVEZ-05/07/2019
        Integer formato = filtro[0]==null?0:Util.integerValueOf(filtro[0]);
		Integer pais = filtro[1]==null?0:Util.integerValueOf(filtro[1]);
		Integer acuerdo = filtro[2]==null?0:Util.integerValueOf(filtro[2]);
		Integer entidad = filtro[3]==null?0:Util.integerValueOf(filtro[3]);
		//Ticket 149343 GCHAVEZ-05/07/2019
        
        try {
        	
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        	filter.put("formato", formato);
        	filter.put("pais", pais);
        	filter.put("acuerdo", acuerdo);
        	filter.put("entidad", entidad);
        	
        	IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
        	ibatisService.loadElement("comun.mostrar_mensaje_FD.element", filter);
        
        	element.put("mostrarMensajeFD", filter.getString("mostrarMensajeFD"));        	
        	
        } catch(Exception e) {
        	logger.error("ERROR al intentar obtener los datos para mostrarMensajeFD", e);
        }
        return element;
    }
    
}



	