package pe.gob.mincetur.vuce.co.logic;

import java.util.List;

import org.jlis.core.list.MessageList;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoFormatoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;

public interface AdjuntoLogic {
    
	public Adjunto descargarAdjuntoById(int idAdjunto);
	
    public void eliminarAdjuntoFormato(int id, int mto);
    
    public void eliminarAdjuntoById(int id);
    
    public List<AdjuntoFormatoCertificado> loadModifSuceList(int mensajeId) throws Exception;
    
    public MessageList uploadFileDR(Long calificacionDrBorradorId, Long certificadoDrBorradorId, String nombre, byte [] bytes);
    
    public Adjunto descargarAdjuntoDRById(int idAdjunto);
    
    public void eliminarAdjuntoCalificacionById(Long calificacionDrBorradorId, int id);
    
    public void eliminarAdjuntoCertificadoById(Long certificadoDrBorradorId, int id);
    
    //20140825_JMC_Intercambio_Empresa
    public byte[] getXMLFromList(List<Adjunto> adjuntos) ;
    
    public byte[] getEBXMLFromList(List<Adjunto> adjuntos);

    public byte[] getZIPFromList(List<Adjunto> adjuntos) ;
    
    public List<Adjunto> getAdjuntosFromList(List<Adjunto> adjuntos);
    
    public List<Adjunto> cargarArchivosTransmision(Transmision transmision) ;    

	public void grabarArchivosPDFTransmision(int idNTX, byte[] adjuntos);
    
	public void registrarNombreAdjuntos(int mct001TmpId, List<AdjuntoFormato> adjuntos);
	
	public void grabarArchivosTransmision(Transmision transmision, byte[] bytesXML, byte[] bytesEBXML, byte[] bytesZipAdjuntos);
	
	public void registrarNombreAdjuntosDJ(int mct005TmpId, List<AdjuntoFormato> adjuntos);
	
	public void registrarNombreAdjuntosDJMaterial(int mct005TmpId, List<DeclaracionJuradaMaterial> listDeclaracionJuradaMaterial);
	
	public List<Adjunto> cargarArchivosTransmisionIOP(Transmision transmision);
}
