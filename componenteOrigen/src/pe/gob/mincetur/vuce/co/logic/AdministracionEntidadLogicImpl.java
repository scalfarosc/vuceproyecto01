package pe.gob.mincetur.vuce.co.logic;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.service.FormatoService;
import pe.gob.mincetur.vuce.co.service.UsuariosService;
import pe.gob.mincetur.vuce.co.service.certificado.CertificadoService;
import pe.gob.mincetur.vuce.co.service.solicitudCalificacion.SCService;
import pe.gob.mincetur.vuce.co.util.Rol;

public class AdministracionEntidadLogicImpl implements AdministracionEntidadLogic {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);
    
    private IbatisService ibatisService;
    
    private FormatoService formatoService;
    
    private UsuariosService usuariosService;
    
    private AdjuntoService adjuntoService;
    
    private SCService scService;
    
    private CertificadoService certificadoService;
    
    public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public void setFormatoService(FormatoService formatoService) {
		this.formatoService = formatoService;
	}

	public void setUsuariosService(UsuariosService usuariosService) {
		this.usuariosService = usuariosService;
	}

	public void setAdjuntoService(AdjuntoService adjuntoService) {
		this.adjuntoService = adjuntoService;
	}

    public void setScService(SCService scService) {
		this.scService = scService;
	}
    
	public void setCertificadoService(CertificadoService certificadoService) {
		this.certificadoService = certificadoService;
	}

	public MessageList asignarEvaluadorSolicitudCalificacion(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        
        try {
            Long ordenId = datos.getLong("ordenId");
            Integer usuarioId = datos.getInt("usuarioId");
            
            scService.asignarEvaluador(ordenId, usuarioId, Rol.CO_ENTIDAD_EVALUADOR.getCodigo());
            
            message = new Message("co.administracionEntidad.asignarEvaluador.success");
            
        } catch (Exception e) {
            logger.error("ERROR: Al asignar el evaluador a la Solicitud de Calificacion", e);
            message = new ErrorMessage("co.administracionEntidad.asignarEvaluador.error", e);
        }
        messageList.add(message);
        return messageList;
    }
    
	public MessageList asignarEvaluadorCertificado(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        
        try {
            Long ordenId = datos.getLong("ordenId");
            Integer usuarioId = datos.getInt("usuarioId");
            
            certificadoService.asignarEvaluador(ordenId, usuarioId, Rol.CO_ENTIDAD_EVALUADOR.getCodigo());
            
            message = new Message("co.administracionEntidad.asignarEvaluador.success");
            
        } catch (Exception e) {
            logger.error("ERROR: Al asignar el evaluador al Certificado", e);
            message = new ErrorMessage("co.administracionEntidad.asignarEvaluador.error", e);
        }
        messageList.add(message);
        return messageList;
    }
	
    public Solicitante getSolicitanteDetail(long ordenId, int mto, int usuarioFormatoTipo) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
        Solicitante solicitante = null;
		try {
			solicitante = scService.getSolicitanteDetail(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return solicitante;
    }
    
	public MessageList iniciarEvaluacionCalificacion(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        
        try {
            Long ordenId = datos.getLong("ordenId");
            
            scService.iniciarEvaluacionCalificacion(ordenId);
            
            message = new Message("co.administracionEntidad.iniciarEvaluacionCalificacion.success");
            
        } catch (Exception e) {
            logger.error("ERROR: Al asignar el evaluador al Certificado", e);
            message = new ErrorMessage("co.administracionEntidad.iniciarEvaluacionCalificacion.error", e);
        }
        messageList.add(message);
        return messageList;
    }
	
	public MessageList iniciarEvaluacionCertificado(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        
        try {
            Long ordenId = datos.getLong("ordenId");
            
            certificadoService.iniciarEvaluacionCertificado(ordenId);
            
            message = new Message("co.administracionEntidad.iniciarEvaluacionCertificado.success");
            
        } catch (Exception e) {
            logger.error("ERROR: Al asignar el evaluador al Certificado", e);
            message = new ErrorMessage("co.administracionEntidad.iniciarEvaluacionCertificado.error", e);
        }
        messageList.add(message);
        return messageList;
    }
	
}
