package pe.gob.mincetur.vuce.co.logic;

import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;
import pe.gob.mincetur.vuce.co.service.SuceService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;

public class SuceLogicImpl implements SuceLogic {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

    private SuceService suceService;

    public Suce getSuceById(int suce){
        Suce suceObj = null;
         try {
             suceObj = suceService.loadSuceById(suce);
         } catch (Exception e) {
             logger.error("Ocurrio un error al cargar la Suce", e);
         }
        return suceObj;
    }

	public Suce getSuceByIdAndUsuarioId(long suce, long usuarioId, long rol){
        Suce suceObj = null;
         try {
             suceObj = suceService.loadSuceByIdAndUsuarioId(suce, usuarioId, rol);
         } catch (Exception e) {
             logger.error("Ocurrio un error al cargar la Suce", e);
         }
        return suceObj;
    }

    public ModificacionSuce loadModificacionSuceByIdMensajeId(int suce, int modificacionSuce, int mensajeId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", suce);
        filter.put("modifsuce", modificacionSuce);
        filter.put("mensajeId", mensajeId);
        ModificacionSuce modifSuce = null;
        try {
            modifSuce = suceService.loadModificacionSuceByIdMensajeId(filter);
        } catch (Exception e) {
            logger.error(e);
        }
        return modifSuce;
    }

    public ModificacionDR loadModificacionDrById(long drId, int sdr, int modificacionDrId, int mensajeId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("modificacionDrId", modificacionDrId);
        filter.put("mensajeId",mensajeId);

        ModificacionDR modificacionDR = null;

        try {
            modificacionDR = suceService.loadModificacionDrById(filter);
        } catch (Exception e) {
            logger.error(e);
        }
        return modificacionDR;
    }

    public HashUtil<String, Object> creaSubsanacion(int idSuce, String tipo, String mensaje, List<String> notificaciones) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("suce", idSuce);
        filter.put("mensaje", mensaje);
        filter.put("mensajeId", null);
        filter.put("modificacionSuceId", null);
        filter.put("esSubsanacion", tipo.equalsIgnoreCase("S") ? "S" : "N");
        try {
            element = suceService.insertSubsanacion(filter, notificaciones);
            if (tipo.equalsIgnoreCase("S")) {
                message = new Message("co.subsanacion_suce.success");
            }/* else {
                message = new Message("vuce.modificacion_suce.success");
            }*/
        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.creaSubsanacion", e);
            if (tipo.equalsIgnoreCase("S")) {
                //message = new ErrorMessage("co.subsanacion_suce.error", e);
                message = ComponenteOrigenUtil.getErrorMessage(e);
            }/* else {
                message = new ErrorMessage("vuce.modificacion_suce.error", e);
            }*/
            element.put("mensajeId", 0);
            element.put("modificacionSuceId", 0);
        }
        messageList.add(message);
        element.put("messageList", messageList);
        return element;
    }

    public HashUtil<String, Object> crearModificacionDr(long drId, int sdr, String mensaje, String esSubsanacion) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("mensaje", mensaje);
        filter.put("modificacionDrId", null);
        filter.put("mensajeId", null);

        try {
            element = suceService.insertModificacionDr(filter);
            message = new Message("co.modificacion_dr.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.crearModificacionDr", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);

            element.put("mensajeId", 0);
            element.put("modificacionDrId", 0);
        }
        messageList.add(message);
        element.put("messageList", messageList);
        return element;
    }

    public MessageList transmiteModificacionDr(ModificacionDR modificacionDr) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", modificacionDr.getDrId());
        filter.put("sdr", modificacionDr.getSdr());
        filter.put("modificacionDrId", modificacionDr.getModificacionDr());
        filter.put("vdId", null);

        try {
            suceService.transmiteModificacionDr(filter);
            message = new Message("co.transmision_dr.success");

            /*if (filter.get("idVC")!=null) {
                Integer idVC = filter.getInt("idVC");
                Transmision transmision = transmisionService.loadTransmision(idVC);

                // Creamos la notificacion para la Entidad
                TransaccionVUCE mensaje = new TransaccionVUCE();
                mensaje.setIdTransaccion(idVC);
                mensaje.setTransaccion(TransaccionType.N24.getCodigo());
                mensaje.setTipoDocumento(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_MODIFICACION_DR);
                mensaje.setNumeroDocumento(String.valueOf(modificacionDr.getModificacionDr()));
                mensaje.setTipoDocumentoReferencia(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_DR);
                mensaje.setNumeroDocumentoReferencia(String.valueOf(modificacionDr.getDr()));

                List<AdjuntoFormato> adjuntos = new ArrayList<AdjuntoFormato>();

                ModificacionDREBXML modificacionDREBXML = new ModificacionDREBXML();
                modificacionDREBXML.setMensaje(modificacionDr.getMensaje());
                modificacionDREBXML.setNumeroDR(String.valueOf(modificacionDr.getDr()));

                HashUtil<String, Object> objects = new HashUtil<String, Object>();
                objects.put("modificacionDREBXML", modificacionDREBXML);
                adjuntos.add(XMLUtil.generateEBXML(modificacionDREBXML, objects));
                mensaje.setAdjuntos(adjuntos);
                gestorProcesos.procesarMensaje(transmision, mensaje);
            }*/
            modificacionDr.setTransmitido("S");
            messageList.setObject(modificacionDr);

        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.transmiteModificacionDr", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        messageList.add(message);
        return messageList;
    }

    public MessageList actualizaModificacionDr(int mensajeId, String mensaje){
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("mensajeId", mensajeId);
        filter.put("mensaje", mensaje);

        try {

            suceService.actualizaModificacionDr(filter);
            message = new Message("co.modificacion_suce.actualiza.success");

        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.actualizaModifSuce", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

    public void eliminaModificacionDr(long drId, int sdr, int modificacionDrId, int mensajeId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("modificacionDrId", modificacionDrId);
        filter.put("mensajeId", mensajeId);

        try {
            suceService.eliminaModificacionDr(filter);

        } catch (Exception e) {
            logger.error(e);
        }
    }

    public MessageList transmiteSubsanacion(ModificacionSuce modificacionSuce) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> filterNotif = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("suce", modificacionSuce.getSuce());
        filter.put("modifsuce", modificacionSuce.getModificacionSuce());
        filter.put("idVC", null);
        filter.put("notificacion", null);
        filterNotif.put("suce", modificacionSuce.getSuce());
        filterNotif.put("modifsuce", modificacionSuce.getModificacionSuce());

        try {
            suceService.transmiteSubsanacion(filter);
            message = new Message("co.transmision_suce.success");

        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.transmiteModifSuce", e);
            //message = new ErrorMessage("co.transmision_suce.error", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        messageList.add(message);
        return messageList;
    }

    public MessageList actualizaSubsanacion(int modifSuceId, int mensajeId, String tipo, String mensaje, List<String> notificaciones) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("mensajeId", mensajeId);
        filter.put("mensaje", mensaje);

        try {
            suceService.actualizaSubsanacion(modifSuceId, filter, notificaciones);
            /*if (tipo.equalsIgnoreCase("S")) {
                message = new Message("vuce.modificacion_suce.actualiza.success");
            } else {*/
                message = new Message("co.subsanacion_suce.actualiza.success");
            //}
        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.actualizaModifSuce", e);
            /*if (tipo.equalsIgnoreCase("S")) {
                message = new ErrorMessage("vuce.modificacion_suce.actualiza.error", e);
            } else {*/
                message = new ErrorMessage("co.subsanacion_suce.actualiza.error", e);
            //}
        }

        messageList.add(message);
        return messageList;
    }

    public MessageList eliminaSubsanacion(int suce, int modifsuce) {
    	MessageList messageList = new MessageList();
        Message message = null;

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", suce);
        filter.put("modificacionSUCE", modifsuce);
        try {
            suceService.eliminaSubsanacion(filter);
            message = new Message("co.modificacion_suce.eliminar.success");
        } catch (Exception e) {
            logger.error(e);
            message = new ErrorMessage("co.modificacion_suce.eliminar.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    public MessageList aprobarModificacionDr(long drId, int sdr, int modificacionDrId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("modificacionDrId", modificacionDrId);

        try {
            suceService.aprobarModificacionDr(filter);
            message = new Message("co.modificacion_dr.aprobar.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.aprobarModificacionDr", e);
            message = new ErrorMessage("co.modificacion_dr.aprobar.error", e);
        }
        messageList.add(message);
        return messageList;
    }

    public MessageList rechazarModificacionDr(long drId, int sdr, int modificacionDrId, String mensaje) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("modificacionDrId", modificacionDrId);
        filter.put("mensaje", mensaje);
        filter.put("mensajeId", null);

        try {
            suceService.rechazarModificacionDr(filter);
            message = new Message("co.modificacion_dr.rechazar.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.rechazarModificacionDr", e);
            message = new ErrorMessage("co.modificacion_dr.rechazar.error", e);
        }
        messageList.add(message);
        return messageList;
    }

    public HashUtil<String, Object> registrarRectificacionDrEval(long drId, int sdr, String mensaje) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("mensaje", mensaje);
        filter.put("sdrNew", null);
        filter.put("mensajeId", null);

        try {
            element = suceService.registrarRectificacionDrEval(filter);
            message = new Message("co.dr.eval.registrar.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.registrarDrEval", e);
            message = new ErrorMessage("co.dr.eval.registrar.error", e);
        }
        messageList.add(message);
        element.put("messageList", messageList);
        return element;
    }

    public HashUtil<String, Object> eliminarRectificacionDrEval(long drId, int sdr, int mensajeId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("mensajeId", mensajeId);

        try {
            element = suceService.eliminarRectificacionDrEval(filter);
            message = new Message("co.dr.eval.eliminar.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.eliminarDrEval", e);
            message = new ErrorMessage("co.dr.eval.eliminar.error", e);
        }
        messageList.add(message);
        element.put("messageList", messageList);
        return element;
    }

    public HashUtil<String, Object> modificarRectificacionDrEval(int mensajeId, String mensaje) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("mensajeId", mensajeId);
        filter.put("mensaje", mensaje);

        try {
            suceService.modificarRectificacionDrEval(filter);
            message = new Message("co.dr.eval.modificar.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.modificarDrEval", e);
            message = new ErrorMessage("co.dr.eval.modificar.error", e);
        }
        messageList.add(message);
        element.put("messageList", messageList);
        return element;
    }

    public HashUtil<String, Object> confirmarRectificacionDrEval(long drId, int sdr) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> element = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("drId", drId);
        filter.put("sdr", sdr);

        try {
            suceService.confirmarRectificacionDrEval(filter);
            message = new Message("co.dr.eval.confirmar.success");

        } catch (Exception e) {
            logger.error("Error en SUCELogicImpl.confirmarDrEval", e);
            message = new ErrorMessage("co.dr.eval.confirmar.error", e);
        }
        messageList.add(message);
        element.put("messageList", messageList);
        return element;
    }

    public Suce getSuceModificacionById(int suceId, int mto){
        Suce suceObj = null;
         try {
             suceObj = suceService.loadSuceModificacionById(suceId, mto);
         } catch (Exception e) {
             logger.error("Ocurrio un error al cargar la Suce", e);
         }
        return suceObj;
    }

    public Suce getSuceByNumeroAndUsuarioId(int numSuce, int usuarioId, int rol){
        Suce suceObj = null;
         try {
             suceObj = suceService.loadSuceByNumeroAndUsuarioId(numSuce, usuarioId, rol);
         } catch (Exception e) {
             logger.error("Ocurrio un error al cargar la Suce", e);
         }
        return suceObj;
    }

    public Suce loadSuceByNumero(int numSuce){
    	Suce res = null;
        try {
        	res = suceService.loadSuceByNumero(numSuce);
        } catch (Exception e) {
            logger.error("Ocurrio un error al cargar la Suce", e);
        }
    	return res;
    }

    public void desisteSuce(int idSuce) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        try {
            filter.put("suceId", idSuce);
            filter.put("idVC", null);
            suceService.desistirSuce(filter);

            /*if (filter.get("idVC")!=null && filter.getInt("idVC") > 0) {
                Integer idVC = filter.getInt("idVC");
                Transmision transmision = transmisionService.loadTransmision(idVC);
                //int idEntidad = suceService.obtenerEntidadIdxSuceId(idSuce);

                Suce suce = suceService.loadSuceById(idSuce);

                // Creamos el mensaje para la Entidad
                TransaccionVUCE mensaje = new TransaccionVUCE();
                mensaje.setIdTransaccion(idVC);
                mensaje.setTransaccion(TransaccionType.N32.getCodigo());
                mensaje.setTipoDocumento(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_SUCE);
                mensaje.setNumeroDocumento(String.valueOf(suce.getNumSuce()));
                //Transmision transmision = transmisionService.registrarTransmisionSaliente(idEntidad, mensaje);

                // Invocar al gestor de procesos
                gestorProcesos.procesarMensaje(transmision, mensaje);

                //transmisionService.vincularTCEaTransmision(transmision.getId(), null, Util.longValueOf(suce.getNumSuce()), null);

            }
        } catch (TransmisionException e) {
            logger.error("Ocurrio un error al envio del mensaje de Desistir Suce", e);*/
        } catch (Exception e) {
            logger.error("Error al desistir la Suce", e);
        }
    }

    public MessageList aceptarDesestimientoSUCE(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suceObj = null;
        try {
            int suceId = datos.getInt("suceId");
            suceObj = suceService.loadSuceById(suceId);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("suce", suceObj.getSuce());
            /*Table CDAs = ibatisService.loadGrid("suce.obtenerCDAsParaAnular", filter);

            for (int i=0; i < CDAs.size(); i++) {
            	Row row = CDAs.getRow(i);
            	String cdaAntiguo = row.getCell("CDA").getValue().toString();

	            // Invocar al WS de SUNAT para pedir que anule el CDA antiguo
	            // Creamos la notificacion para SUNAT
	            NotificacionSUNAT mensajeSUNAT = new NotificacionSUNAT();
	            mensajeSUNAT.setTransaccion(TransaccionType.N12.getCodigo());
	            mensajeSUNAT.setTipoDocumento(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_CDA);
	            mensajeSUNAT.setNumeroDocumento(cdaAntiguo);
	            mensajeSUNAT.setTipoDocumentoReferencia(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_SUCE);
	            mensajeSUNAT.setNumeroDocumentoReferencia(String.valueOf(suceObj.getNumSuce()));

            	Transmision transmisionSUNAT = transmisionService.registrarTransmisionSalienteSUNAT(ConstantesVUCE.ENTIDAD_SUNAT, mensajeSUNAT);

                // Encolamos el mensaje
                transmisionService.encolarMensajeSunat(transmisionSUNAT, mensajeSUNAT);

                String resp = null;
            	try {
                    resp = (String)gestorProcesos.procesarMensaje(transmisionSUNAT, mensajeSUNAT);
            	} catch (Exception e) {
            		// Si ocurre algun error al comunicarse con SUNAT, el proceso sigue su curso y se acepta el desistimiento
            	}
                // Actualizo el estado de la transmision
	            transmisionSUNAT.setEstadoVC(ConstantesVUCE.TRANSMISION_ESTADO_VC_ENVIADO_A_SUNAT);
                transmisionService.updateEstadoTransmision(transmisionSUNAT);

            	System.out.println("************************************************** Respuesta de \"AnularPago\": "+resp);
            }*/
            // Se pasa transmision.getId() para que el STORED PROCEDURE pueda asociar los archivos al VC_ID
	        suceService.aceptacionDesestimientoSuce(null, suceObj.getNumSuce());

            message = new Message("vuce.administracionEntidad.aceptarDesestimientoSUCE.success", ""+suceObj.getNumSuce());

        } catch (Exception e) {
            logger.error("ERROR: Al cerrar la SUCE", e);
            message = new ErrorMessage("vuce.administracionEntidad.aceptarDesestimientoSUCE.error", e, ""+suceObj.getNumSuce());
        }
        messageList.add(message);
        return messageList;
    }

    public MessageList rechazarDesestimientoSUCE(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suceObj = null;
        try {
            int suceId = datos.getInt("suceId");
            suceObj = suceService.loadSuceById(suceId);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("suce", suceObj.getSuce());

	        suceService.rechazoDesestimientoSuce(suceObj.getNumSuce());

            message = new Message("vuce.administracionEntidad.rechazarDesestimientoSUCE.success", ""+suceObj.getNumSuce());

        } catch (Exception e) {
            logger.error("ERROR: Al cerrar la SUCE", e);
            message = new ErrorMessage("vuce.administracionEntidad.rechazarDesestimientoSUCE.error", e, ""+suceObj.getNumSuce());
        }
        messageList.add(message);
        return messageList;
    }

    public String suceFlgPendienteCalif(Integer suceId){
    	return this.suceService.suceFlgPendienteCalif(suceId);
    }

    public SuceService getSuceService() {
		return suceService;
	}

	public void setSuceService(SuceService suceService) {
		this.suceService = suceService;
	}

}
