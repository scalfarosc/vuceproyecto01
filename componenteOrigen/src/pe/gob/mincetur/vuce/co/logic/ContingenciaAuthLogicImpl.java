package pe.gob.mincetur.vuce.co.logic;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.service.ContingenciaAuthService;

/**
 * LogicImpl para Contingencia VUCE
 * @author 
 *
 */
public class ContingenciaAuthLogicImpl implements ContingenciaAuthLogic {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

    protected ContingenciaAuthService service;

	@Override
	public Integer obtenerModoOperacion() throws Exception {
		logger.debug("obtenerModoOperacion");
		return service.obtenerModoOperacion();
	}

	@Override
	public String registraConsulta(Integer componente, String ruc, String usuario, Integer claveSolDisponible, Integer indOperacion) throws Exception {
		return service.registraConsulta(componente, ruc, usuario, claveSolDisponible, indOperacion);
	}

	@Override
	public String solicitaContingencia(Integer componente, Integer tipoUsuario, String ruc, String usuario) throws Exception {
		return service.solicitaContingencia(componente, tipoUsuario, ruc, usuario);
	}

	@Override
	public HashUtil<String, Object> validaToken(String token, Integer componente) throws Exception {
		return service.validaToken(token, componente);
	}
	
	public void setService(ContingenciaAuthService service) {
		this.service = service;
	}

	@Override
	public String obtenerMensajeIndexContingencia() throws Exception {
		return service.obtenerMensajeIndexContingencia();
	}

	@Override
	public String obtenerMensajeCorreoEnviado() throws Exception {
		return service.obtenerMensajeCorreoEnviado();
	}

	@Override
	public String obtenerMensajeValidacionToken() throws Exception {
		return service.obtenerMensajeValidacionToken();
	}

	@Override
	public String obtenerMensajeValidacionCaptura() throws Exception {
		return service.obtenerMensajeValidacionCaptura();
	}

}
