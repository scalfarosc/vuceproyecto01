package pe.gob.mincetur.vuce.co.logic;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.service.OrdenService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class OrdenLogicImpl implements OrdenLogic {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

    private OrdenService ordenService;

    private FormatoLogic formatoLogic;

	public void setOrdenService(OrdenService ordenService) {
		this.ordenService = ordenService;
	}

	public void setFormatoLogic(FormatoLogic formatoLogic) {
		this.formatoLogic = formatoLogic;
	}

	public MessageList getOrdenDetailById(Long idOrden, Integer idMto) {
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("orden", idOrden);
            filter.put("mto", idMto);
            Orden orden = ordenService.loadOrdenDetail(filter);
            messageList.setObject(orden);

        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.loadOrdenDet", e);
            message = new ErrorMessage("error.carga", e);
        }
        messageList.add(message);
        return messageList;
    }

    public Orden getOrdenById(Long idOrden, Integer idMto) {
        Orden orden = new Orden();
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("orden", idOrden);
            filter.put("mto", idMto);
            orden = ordenService.loadOrdenDetail(filter);

        } catch (Exception e) {
            logger.error("Error en OrdenLogicImpl.getOrdenById", e);
        }
        return orden;
    }

    /**
     * Creación de la Orden para Certificados de Origen
     */
    public MessageList creaOrden(UsuarioCO usuario, Integer acuerdo, Integer pais, Integer entidadCertificadora, Integer solicitante, Integer representante, 
    		                     Integer empresa, Integer idTupa, Integer idFormato, String certificadoOrigen, String certificadoReexportacion) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashUtil<String, Object> ordenDet = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();

        filter.put("idUsuario", usuario.getIdUsuario());
        filter.put("idAcuerdo", acuerdo);
        filter.put("idPais", pais);
        filter.put("idEntidadCertificadora", entidadCertificadora);
        //filter.put("idSede", idSede);
        filter.put("idFormato", idFormato);
        filter.put("idTupa", idTupa);
        filter.put("certificadoOrigen", certificadoOrigen);
        filter.put("certificadoReexportacion", certificadoReexportacion);        
        filter.put("orden", null);
        filter.put("mto", null);
        filter.put("idFormatoEntidad", null);

        Message message = null;
        try {
            // inserto datos de la cabecera
            ordenDet = ordenService.insertOrdenCab(filter);

            Long orden = ordenDet.getLong("orden");
            Integer mto = ordenDet.getInt("mto");

            // inserto usuario
            filter.clear();
            filter.put("orden", orden);
            filter.put("mto", mto);
            filter.put("idUsuario", usuario.getIdUsuario());
            filter.put("idUsuarioTipo", ConstantesCO.USUARIO_USUARIO);
            filter.put("idRepresentante",null);
            filter.put("cargo",usuario.getCargo());
            ordenService.insertOrdenUsuario(filter);
            
            // inserto solicitante
            // 20150803_JFR: Obtengo el Solicitante
            int idSolicitante = formatoLogic.getSolicitanteDetail(usuario).getUsuarioId().intValue();
            
            filter.clear();
            filter.put("orden", orden);
            filter.put("mto", mto);
            filter.put("idSolicitante", idSolicitante);
            filter.put("idSolicitanteTipo", ConstantesCO.USUARIO_SOLICITANTE);
            filter.put("idRepresentante",null);
            filter.put("cargo",usuario.getCargo());
            ordenService.insertOrdenSol(filter);
            
            if (representante != 0){
                // inserto representante
                filter.clear();
                filter.put("orden", orden);
                filter.put("mto", mto);
                filter.put("idSolicitante", idSolicitante);
                filter.put("idRepresentanteTipo", ConstantesCO.USUARIO_REPRESENTANTE);
                filter.put("idRepresentante",representante);
                filter.put("cargo",usuario.getCargo());
                ordenService.insertOrdenRep(filter);
            }
            
            Orden ordenObj = getOrdenById(orden, mto);
            //System.out.println("Num Orden: "+ordenObj.getNumOrden());
            messageList.setObject(ordenObj);
            message = new Message("insert.success");

        } catch (Exception e) {
            logger.error("Error al crear la Orden", e);
            message = new ErrorMessage("insert.error", e);
        }

        messageList.add(message);

        return messageList;
    }

    public MessageList creaModifOrden(UsuarioCO usuario, Long orden, Integer mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();

        filter.put("orden", orden);
        filter.put("mto", mto);
        filter.put("mtoNew", null);

        Message message = null;
        try {
            filter = ordenService.creaModifOrden(filter);

            mto = filter.getInt("mtoNew");
            Orden ordenObj = getOrdenById(orden, mto);

            messageList.setObject(ordenObj);
            message = new Message("vuce.modificacion_orden.success");

        } catch (Exception e) {
            logger.error("Error al crear la modificación de la Orden", e);
            message = new ErrorMessage("vuce.modificacion_orden.error", e);
        }

        messageList.add(message);
        return messageList;

    }

    public void desisteOrden(Integer idEntidad, Long orden, Integer mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();

        try {
            filter.put("orden", orden);
            filter.put("mto", mto);

            ordenService.desistirOrden(filter);

        } catch (Exception e) {
            logger.error("Error al desistir la Orden", e);
        }
    }

    public MessageList updateOrdenRepresentante(Integer idSolicitante, Long orden, Integer mto, Integer idRepresentante) {
        if (idRepresentante== 0) return null;

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        filter.put("orden", orden);
        filter.put("mto", mto);
        filter.put("idSolicitante", idSolicitante);
        filter.put("idRepresentante", idRepresentante);
        //filter.put("cargo",usuario.getCargo());

        Message message = null;
        try {
            ordenService.updateOrdenRepresentante(filter);

            //RepresentanteLegal representante = null;//formatoLogic.getRepresentanteDetail(orden, mto);

            /*if (representante == null) {
                // Inserto representante
                filter.put("idRepresentanteTipo", ConstantesCO.USUARIO_REPRESENTANTE);
                ordenService.insertOrdenRep(filter);
            }*/

            Orden ordenObj = getOrdenById(orden, mto);

            messageList.setObject(ordenObj);
            message = new Message("update.representante.success");

        } catch (Exception e) {
            logger.error("Error al actualizar al representante legal", e);
            message = new ErrorMessage("update.representante.error", e);

        }

        messageList.add(message);
        return messageList;
    }

    /*public int existeModificacion(Integer orden, Integer mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("orden", orden);
        filter.put("mto", mto);
        int cuenta = 0;
        try {
             cuenta = ordenService.existeModificaciones(filter);
        } catch (Exception e){
             logger.error("Error verificar si existe modificaciones", e);
        }

        return cuenta;
    }
    */
    public Orden loadOrdenByNumero(Long numero) {
        Orden orden = null;
        try {
            orden = ordenService.loadOrdenByNumero(numero);
        } catch (Exception e) {
            logger.error("Error en OrdenServiceImpl.loadOrdenByNumero", e);
        }
        return orden;
    }

    public Orden loadOrdenById(Long idOrden) {
        Orden orden = null;
        try {
            orden = ordenService.loadOrdenById(idOrden);
        } catch (Exception e) {
            logger.error("Error en OrdenServiceImpl.loadOrdenById", e);
        }
        return orden;
    }

}
