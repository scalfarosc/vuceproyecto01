package pe.gob.mincetur.vuce.co.logic;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.Mensaje;
import pe.gob.mincetur.vuce.co.service.BuzonService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.Rol;

public class BuzonLogicImpl implements BuzonLogic {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);
	
	private BuzonService buzonService;
	
    public void setBuzonService(BuzonService buzonService) {
		this.buzonService = buzonService;
	}

	public Mensaje getMessageById(int idMensaje) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idMensaje", idMensaje);
        Mensaje mensaje = null;
		try {
			mensaje = buzonService.loadMensaje(filter);
		} catch (Exception e) {
			logger.error("ERROR: Al intentar obtener el mensaje a partir del ID "+idMensaje);
		}
        return mensaje;
	}

	public MessageList getMessageById(UsuarioCO usuario, int idMensaje , int destinatario) {
		MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("idMensaje", idMensaje);
            filter.put("destinatario", destinatario);
            
            Mensaje mensaje = null;
            
            if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
         		!usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
            	mensaje = buzonService.loadMensajeExtranet(filter);
            } else {
            	mensaje = buzonService.loadMensaje(filter);
            }
            
            messageList.setObject(mensaje);
            
            // Si el usuario que esta leyendo el mensaje no es un usuario EXTRANET y es el destinatario del mensaje, entonces se marca el mensaje como leido
            if (!usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && mensaje.getEstado().equalsIgnoreCase("N") && 
                destinatario==usuario.getIdUsuario()) {
	            //Actualiza el mensaje a Leido
	            buzonService.updateMensajeLeido(filter);
	            message = new Message("vuce.test.insert.success");            
	            
            } else if (usuario.getTipoOrigen().equalsIgnoreCase(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) && 
            		   !usuario.getRoles().containsKey(Rol.CO_ADMIN_HELPDESK.getNombre())) {
            	//Actualiza el mensaje a Leido
	            buzonService.updateMensajeLeido(filter);
	            message = new Message("vuce.test.insert.success");
            }
        } catch (Exception e) {
            logger.error("Error en BuzonLogicImpl.getMessageById", e);
            message = new ErrorMessage("vuce.test.insert.error", e);
        }
        messageList.add(message);
        return messageList;
	}
	
	public MessageList inactivaMensaje(int idMensaje , int destinatario) {
		MessageList messageList = new MessageList();
        Message message = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("idMensaje", idMensaje);
            filter.put("destinatario", destinatario);
            
            //Inactivar el mensaje
            buzonService.inactivaMensaje(filter);
            message = new Message("vuce.test.insert.success");
            
        } catch (Exception e) {
            logger.error("Error en BuzonLogicImpl.inactivaMensaje", e);
            message = new ErrorMessage("vuce.test.insert.error", e);
        }
        messageList.add(message);
        return messageList;
	}
	
	public Adjunto descargarAdjuntoById(int idAdjunto) {
		MessageList messageList = new MessageList();
        Message message = null;
        Adjunto adjunto = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idAdjunto", idAdjunto);
        try {
        	adjunto = buzonService.loadAdjunto(filter);
		} catch (Exception e) {
			logger.error("Error en BuzonLogicImpl.descargarAdjuntoById", e);
		}
        return adjunto;
	
	}
}
