package pe.gob.mincetur.vuce.co.logic;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.service.InteroperabilidadFirmaService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;


public class InteroperabilidadFirmasLogicImpl implements InteroperabilidadFirmasLogic {

    private InteroperabilidadFirmaService iopService;
    
    private AdjuntoService adjuntoService;
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
    
    public void setIopService(InteroperabilidadFirmaService iopService) {
		this.iopService = iopService;
	}

    public InteroperabilidadFirma obtenerToken(Long drId, Integer sdr) throws Exception {
    	return iopService.obtenerToken(drId, sdr);
    }
    
    public Integer registrarInteroperabilidad(InteroperabilidadFirma iop) throws Exception {
    	return iopService.registrarInteroperabilidad(iop);
    }
    
    public InteroperabilidadFirma obtenerEtapa(String token, Integer secuencia)  throws Exception {
    	return iopService.obtenerEtapa(token, secuencia);
    }
    
	public byte[] generarXml(String token)  throws Exception {
    	return iopService.generarXml(token);
    }
    
    public byte[] generarPdf(String token)  throws Exception {
    	return iopService.generarPdf(token);
    }

    public Integer enviarXmlFirmado(String token, byte[] xml) throws Exception {
    	return iopService.enviarXmlFirmado(token, xml);
    }
    
    public Integer enviarPdfFirmado(String token, byte[] pdf) throws Exception {
    	return iopService.enviarPdfFirmado(token, pdf);
    }

    public void actualizaFirmaExportador(Long drId, Integer sdr, String nombreExp, String formato, String posteriori) throws Exception {
    	iopService.actualizaFirmaExportador(drId, sdr, nombreExp, formato, posteriori);
    }

    public InteroperabilidadFirma obtenerPdf(String token, Integer secuencia) throws Exception {
    	return iopService.obtenerPdf(token, secuencia);
    }    
    
    //JMC_20180518_PRODUCCION CONTROLADA : Genera automaticamente el certificado origen electronico para Alianza - 
    public void generarCertificadoOrigenElectronicoPC(Long drId, Integer sdr) {
    	//, String nombreExp, String formato, String posteriori
    	String token = null;
    	try {
			//Ya no es necesario, el dato se actualizara cuando el Evaluador Firme(adjunte el documento firmado). Consultar a TAPIA
    		//Actualizar fecha de firma: mct001_dr
			//actualizaFirmaExportador(drId, sdr, nombreExp, formato, posteriori);//Verificar: para fisico y electronico lo deben llamar
			
			//Generar token (secuencia 1)
			token = generarTokenFA(drId, sdr);
			
			//Generar XML sin Firma(secuencia 2)
			byte [] xmlGeneradoAdministrado = generarXml(token);
			enviarXmlFirmado(token, xmlGeneradoAdministrado);
			byte [] pdfGeneradoAdministrado = generarPdf(token);
			enviarPdfFirmado(token, pdfGeneradoAdministrado);
			
			//Generar XML sin Firma(secuencia 3)
			byte [] xmlGeneradoEval = generarXml(token);
			enviarXmlFirmado(token, xmlGeneradoEval);
			byte [] pdfGeneradoEval = generarPdf(token);
			enviarPdfFirmado(token, pdfGeneradoEval);   
			//Genera N85
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("Ocurrio un error al generarCertificadoOrigenElectronico", e);
		}
    	
    	    	
    }
    
    //JMC_20180518 : Generar Token para Firma automatica (artificio para soportar mismo modelo de Firmado)
    public String generarTokenFA (Long drId, Integer sdr) {
    	String token = null;
		try {
			InteroperabilidadFirma iop = new InteroperabilidadFirma();
			iop.setDrId(drId);
			iop.setSdr(sdr);
			iop.setAcuerdoInternacionalId(ConstantesCO.AC_ALIANZA_PACIFICO);
			UUID unique = UUID.randomUUID();
			token = unique.toString();
			iop.setToken(token);
			registrarInteroperabilidad(iop);
			
		} catch (Exception e) {				
			logger.error("Ocurrio un error al generarTokenFA", e);
		}
		return token;    	
    }
    
    public boolean produccionControlada(Long drId) throws Exception{
    	return "S".equals(iopService.produccionControlada(drId));
    }
    
    public RespuestaCert obtenerCertificado(String certificadoOrigen, String codigoPais) {
        MessageList messageList = new MessageList();
        Message message = null;
        RespuestaCert adjunto = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("certificadoOrigen", certificadoOrigen);
        filter.put("codigoPais", codigoPais);
        try {
            adjunto = iopService.obtenerCertificado(filter);
            
            
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.obtenerCertificado", e);

        }
        return adjunto;
    }
    
    public String tipoFirma(Long drId) throws Exception{
    	return iopService.tipoFirma(drId);
    }

    public String permiteFirmaDigital(Long ordenId) throws Exception{
    	return iopService.permiteFirmaDigital(ordenId);
    }
}
