package pe.gob.mincetur.vuce.co.logic.resolutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.web.servlet.ModelAndView;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.logic.FormatoLogicImpl;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.AdjuntoCertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.service.SuceService;
import pe.gob.mincetur.vuce.co.service.resolutor.ResolutorService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

/**
 * Clase implementadora de lógica de negocio de Certificados de Origen
 * @author Ricardo Montes
 */
public class ResolutorLogicImpl extends FormatoLogicImpl implements ResolutorLogic {

	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

	private ResolutorService resolutorService;
	private SuceService suceService;

	/**
	 * Registra la designación de un Evaluador por parte de un Administrador
	 */
	public MessageList designaEvaluador(Map<String,Object> parametros) {
		MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.designaEvaluador(parametros);
	        message = new Message("update.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.designaEvaluador", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
	}

	/**
	 * Registra la aceptación de la designación TCE
	 */
    public MessageList aceptaDesignacionTCE(CertificadoOrigen co) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.aceptaDesignacionTCE(co);
			messageList.setObject(co);
	        message = new Message("update.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.aceptaDesignacionTCE", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }

    /**
	 * Registra el rechazo de la designación TCE
	 */
    public MessageList rechazaDesignacionTCE(CertificadoOrigen co) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.rechazaDesignacionTCE(co);
			messageList.setObject(co);
	        message = new Message("update.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.rechazaDesignacionTCE", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }

    /**
     * Registra la calificación del evaluador
     */
	public MessageList evaluadorDJCalifica(DeclaracionJurada dj) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.evaluadorDJCalifica(dj);
			messageList.setObject(dj);
	        message = new Message("dj.califica.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.evaluadorDJCalifica", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }

		
	
    /**
     * Registra la calificación del evaluador la Aprueba y la Transmite
     */
	public MessageList evaluadorDJCalificaApruebaTransmite(DeclaracionJurada dj) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.evaluadorDJCalificaApruebaTransmite(dj);
			messageList.setObject(dj);
	        message = new Message("dj.califica.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.evaluadorDJCalificaApruebaTransmite", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }

	
	/**
     * Registra la no calificación del evaluador
     */
	public MessageList evaluadorDJNoCalifica(DeclaracionJurada dj) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
        	resolutorService.evaluadorDJNoCalifica(dj);
			messageList.setObject(dj);
	        message = new Message("dj.rechaza.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.evaluadorDJNoCalifica", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }
	
	/**
     * Registra la no calificación del evaluador la Deniega y Transmite
     */
	public MessageList evaluadorDJNoCalificaDeniegaTransmite(DeclaracionJurada dj) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
        	resolutorService.evaluadorDJNoCalificaDeniegaTransmite(dj);
			messageList.setObject(dj);
	        message = new Message("dj.rechaza.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.evaluadorDJNoCalificaDeniegaTransmite", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }	

	/**
	 * Inserta el registro de un dr en borrador
	 */
	public MessageList registrarBorradorDr(CertificadoOrigenDR coDR) {
    	MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.registrarBorradorDr(coDR);
			messageList.setObject(coDR);
	        message = new Message("insert.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.registrarBorradorDr", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }

	/**
	 * Confirma el registro de un dr en borrador
	 */
	/*
	public MessageList confirmarBorradorDr(Long coDrId, Long suceId) {
		HashUtil params = new HashUtil();
		params.put("coDrId", coDrId);
		params.put("suceId", suceId);
		
		//2018.06.06 JMC Produccion Controlada Alianza Pacifico
		params.put("drId", null);
		params.put("sdr", null);

    	MessageList messageList = new MessageList();
		Message message = null;
        try {

        	if ("1".equals(this.resolutorService.subsanacionNoAtendida(suceId))){
        		Message msgAlert = new Message("co.resolutor.dr.subsanaciones.pendientes");
        		messageList.add(msgAlert);
        	}

			resolutorService.confirmarBorradorDr(params);
			message = new Message("transmite.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.confirmarBorradorDr", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
    }*/

	//2018.06.06 JMC Produccion Controlada Alianza Pacifico
	public HashUtil<String, Object> confirmarBorradorDr(Long coDrId, Long suceId) {
		HashUtil params = new HashUtil();
		HashUtil<String, Object> element = new HashUtil<String, Object>();
		
		params.put("coDrId", coDrId);
		params.put("suceId", suceId);		
		//2018.06.06 JMC Produccion Controlada Alianza Pacifico
		params.put("drId", null);
		params.put("sdr", null);

    	MessageList messageList = new MessageList();
		Message message = null;
        try {

        	if ("1".equals(this.resolutorService.subsanacionNoAtendida(suceId))){
        		Message msgAlert = new Message("co.resolutor.dr.subsanaciones.pendientes");
        		messageList.add(msgAlert);
        	}

        	element = resolutorService.confirmarBorradorDr(params);
			message = new Message("transmite.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.confirmarBorradorDr", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
		
		element.put("messageList", messageList);		
		return element;	    
    }	
	
	/**
	 * Registra una Notificación de Subsanación
	 */
    public MessageList registrarNotificacionSubsanacionSuce(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	/*int ordenId = datos.getInt("ordenId");
        	int mto = datos.getInt("mto");*/
            int suceId = datos.getInt("suceId");
            /*HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("orden", ordenId);
            filter.put("mto", mto);
            Orden orden = ordenService.loadOrdenDetail(filter);*/
        	//suce = suceService.loadSuceById(suceId);

            //filter.clear();
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
            if (datos.getString("notificacionId").equals("")) {
	            /*filter.put("orden", orden.getNumOrden());
	            filter.put("mto", mto);*/
	            //filter.put("suce", suce.getNumSuce());
            	filter.put("suceId", suceId);
	            filter.put("mensaje", datos.getString("mensaje"));
	            filter.put("notificacionId", null);
	            Integer notificacionId = suceService.registrarBorradorNotificacionSubsanacion(filter);
	            messageList.setObject(notificacionId);
            } else {
            	int notificacionId = datos.getInt("notificacionId");
	            filter.put("notificacionId", notificacionId);
	            filter.put("mensaje", datos.getString("mensaje"));
            	suceService.actualizarBorradorNotificacionSubsanacion(filter);
            	messageList.setObject(notificacionId);
            }

            //message = new Message("vuce.administracionEntidad.registrarNotificacionSubsanacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.registrarNotificacionSubsanacionSuce.success");

        } catch (Exception e) {
            logger.error("ERROR: Al registrar la Notificacion de Subsanacion de la SUCE", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);//new ErrorMessage("vuce.administracionEntidad.registrarNotificacionSubsanacionSuce.error", e, ""+suce.getNumSuce());
        }

        messageList.add(message);
        return messageList;
    }

    /**
     * Envia una Notificación de Subsanación
     * @param datos
     * @return
     */
    public MessageList enviarNotificacionSubsanacionSuce(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	int suceId = datos.getInt("suceId");
            int notificacionId = datos.getInt("notificacionId");
            //suce = suceService.loadSuceById(suceId);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("notificacionId", notificacionId);
	        suceService.enviarBorradorNotificacionSubsanacion(filter);

            //message = new Message("vuce.administracionEntidad.enviarBorradorNotificacionSubsanacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.enviarBorradorNotificacionSubsanacionSuce.success");

        } catch (Exception e) {
            logger.error("ERROR: Al enviar la Notificacion de Subsanacion de la SUCE", e);
            //message = new ErrorMessage("vuce.administracionEntidad.enviarBorradorNotificacionSubsanacionSuce.error", e, ""+suce.getNumSuce());
            message = new ErrorMessage("co.resolutor.evaluador.enviarBorradorNotificacionSubsanacionSuce.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    /**
     * Elimina una Notificación de Subsanación
     */
    public MessageList eliminarNotificacionSubsanacionSuce(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	int suceId = datos.getInt("suceId");
            int notificacionId = datos.getInt("notificacionId");
            //suce = suceService.loadSuceById(suceId);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("notificacionId", notificacionId);
	        suceService.eliminarBorradorNotificacionSubsanacion(filter);

            //message = new Message("vuce.administracionEntidad.eliminarNotificacionSubsanacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.eliminarNotificacionSubsanacionSuce.success");

        } catch (Exception e) {
            logger.error("ERROR: Al eliminar la Notificacion de Subsanacion de la SUCE", e);
            //message = new ErrorMessage("vuce.administracionEntidad.eliminarNotificacionSubsanacionSuce.error", e, ""+suce.getNumSuce());
            message = new ErrorMessage("co.resolutor.evaluador.eliminarNotificacionSubsanacionSuce.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    /**
     * Carga un archivo de Notificación de Subsanación
     */
    public MessageList cargarArchivoNotificacion(int idOrden, int idMto, String nombre, Integer notificacionId, byte[] bytes) {
        MessageList messageList = new MessageList();

        AdjuntoFormato archivoAdjunto = new AdjuntoFormato();
        archivoAdjunto.setOrdenId(idOrden);
        archivoAdjunto.setMto(idMto);
        archivoAdjunto.setNombre(ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setNotificacionId(notificacionId);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
        	adjuntoService.insertAdjuntoNotificacion(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en AdministracionEntidadLogicImpl.uploadFile", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

	/**
	 * Genera el registro de un dr en borrador
	 */
	public MessageList generarBorradorDr(Long suceId, String tipoDr) {
		HashUtil params = new HashUtil();
		params.put("suceId", suceId);
    	params.put("tipoDr", tipoDr);
    	params.put("coDrId", new Long("0"));

    	MessageList messageList = new MessageList();
		Message message = null;
        try {
        	if ("1".equals(this.resolutorService.subsanacionNoAtendida(suceId))){
        		Message msgAlert = new Message("co.resolutor.dr.subsanaciones.pendientes");
        		messageList.add(msgAlert);
        	}

			resolutorService.generarBorradorDr(params);
			messageList.setObject(params);
			message = new Message("insert.success");

	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.confirmarBorradorDr", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }

        return messageList;

    }

	/**
	 * Elimina el registro de un dr en borrador
	 */
	public MessageList eliminarBorradorDr(Long coDrId, Long suceId) {
		HashUtil params = new HashUtil();
		params.put("coDrId", coDrId);
		params.put("suceId", suceId);
    	MessageList messageList = new MessageList();
		Message message = null;
		try {
			resolutorService.eliminarBorradorDr(params);
			message = new Message("delete.success");

	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.confirmarBorradorDr", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }

        return messageList;
    }

	/**
	 * Elimina el registro de un dr en borrador
	 */
	public MessageList actualizarObsBorradorDr(Long mct001DrId, String observacionEvaluador, String formato, Integer idAcuerdo) {
		HashUtil params = new HashUtil();
		params.put("coDrId", mct001DrId);
		params.put("observacionEvaluador", observacionEvaluador);
		params.put("acuerdoInternacionalId", idAcuerdo);
		params.put("formato", formato);

    	MessageList messageList = new MessageList();
		Message message = null;
		try {
			resolutorService.actualizarObsBorradorDr(params);
			message = new Message("update.success");

	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.actualizarObsBorradorDr", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }

        return messageList;
    }

    /**
	 * Obtiene un borrador de dr
	 */
	@SuppressWarnings("unchecked")
	public CertificadoOrigenDR getCODrResolutorById(Map filter) {
		//HashMap<String, Object> filter = new HashMap<String, Object>();
        //filter.put("mct001DrId", mct001DrId);
        CertificadoOrigenDR coDr = null;
        try {
        	coDr = resolutorService.getCODrResolutorById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getCODrResolutorById", e);
        }
		return coDr;
	}

	/**
	 *  Registra un adjunto de la DR
	 */
	public MessageList cargarAdjuntoCODR(AdjuntoCertificadoOrigenDR param) {

    	MessageList messageList = new MessageList();
		Message message = null;
		try {
			resolutorService.cargarAdjuntoCODR(param);
			message = new Message("insert.success");

	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.cargarAdjuntoCODR", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }

        return messageList;
    }

    public DR obtenerDRById(Long drId, Integer sdr) {
        DR dr = null;
        try {
            dr = resolutorService.getDRById(drId, sdr);
        } catch (Exception e) {
            logger.error("Error en ResolutorLogicImpl.obtenerDRById", e);
        }
        return dr;
    }

	/**
	 * Aprueba la Modificación de la Subsanación
	 */
    public MessageList aprobarModificacionSubsanacion(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
            int suceId = datos.getInt("suceId");
            int modificacionSuceId = datos.getInt("modificacionSuceId");
            //double montoTasa = datos.getString("tasa").equals("") ? 0 : datos.getDouble("tasa");
            //String ampliacionPlazo = datos.getString("ampliacionPlazo");

            //suce = suceService.loadSuceById(suceId);
            suceService.aprobarModificacion(suceId, modificacionSuceId);
            //suceService.aprobarModificacion(suce.getNumSuce(), modificacionSuceId);

            //if (!ampliacionPlazo.equalsIgnoreCase("S")) {
/*	            HashUtil<String, Object> filter = new HashUtil<String, Object>();
	            filter.put("numSuce", suce.getNumSuce());
	            filter.put("mts", modificacionSuceId);
	            filter.put("monto", montoTasa);
	            filter.put("idTransaccion", TransaccionType.N22.getId());
	            filter.put("idTasa", null);
	            filter.put("vcIdEntidad", null);
	            filter.put("vcIdSunat", null);

	            filter = suceService.registrarTasaSuce(filter);

	            Integer vcIdSunat = null;
	            if(filter.get("vcIdSunat")!=null){
	                vcIdSunat = filter.getInt("vcIdSunat");
	            }  else {
	                vcIdSunat = 0;
	            }

	            // Se verifica si el mensaje viene CON o SIN TASA
	            if (montoTasa!=0.0) {
	                int idTasa = filter.getInt("idTasa");
	                Tasa tasa = cdaService.loadTasaById(idTasa);
	                String cda = tasa.getCda();

	                // Creamos la notificacion para SUNAT
	                Transmision transmisionSunat = transmisionService.loadTransmision(vcIdSunat);
	                NotificacionSUNAT mensajeSUNAT = new NotificacionSUNAT();
	                mensajeSUNAT.setIdTransaccion(vcIdSunat);
	                mensajeSUNAT.setTransaccion(TransaccionType.N3.getCodigo());
	                mensajeSUNAT.setFecha(Util.getDate(tasa.getFechaCaducidad(), "dd/MM/yyyy"));
	                mensajeSUNAT.setMontoPago(tasa.getMonto().intValue());
	                mensajeSUNAT.setTipoDocumento(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_CDA);
	                mensajeSUNAT.setNumeroDocumento(String.valueOf(cda));
	                mensajeSUNAT.setTipoDocumentoReferencia(ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_SUCE+","+ConstantesVUCE.TIPO_DOCUMENTO_TRANSMISION_MODIFICACION_SUCE);
	                mensajeSUNAT.setNumeroDocumentoReferencia(String.valueOf(suce.getNumSuce())+","+modificacionSuceId);

	                Orden orden = ordenService.loadOrdenById(suce.getIdOrden());
	                UsuarioVUCE usuario = usuariosService.loadUsuarioVUCEById(orden.getIdUsuario());
	                mensajeSUNAT.setTipoDocumentoUsuario(usuario.getTipoDocumentoSUNAT());
	                mensajeSUNAT.setNumeroDocumentoUsuario(usuario.getNumeroDocumento());

	                // Invocar al proceso de registro de Transmision saliente
	                gestorProcesos.procesarMensaje(transmisionSunat, mensajeSUNAT);
	            }*/
            //}

            //message = new Message("vuce.administracionEntidad.aprobarModificacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.aprobarModificacionSuce.success");

        } catch (Exception e) {
            logger.error("ERROR: Al aprobar la Modificacion de SUCE", e);
            //message = new ErrorMessage("vuce.administracionEntidad.aprobarModificacionSuce.error", e, ""+suce.getNumSuce());
            //message = new ErrorMessage("co.resolutor.evaluador.aprobarModificacionSuce.error", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

    /**
     * Rechaza una Modificación de Subsanación
     */
    public MessageList rechazarModificacionSubsanacion(HashUtil<String, String> datos, byte [] bytes, String nombreArchivo) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	int suceId = datos.getInt("suceId");
            int modificacionSuceId = datos.getInt("modificacionSuceId");
            String mensaje = datos.getString("mensaje");

            suce = suceService.loadSuceById(suceId);
            long ordenId = Long.valueOf(suce.getIdOrden());
            Orden orden = ordenService.loadOrdenById(ordenId);

            List<Adjunto> adjuntos = new ArrayList<Adjunto>();
            
            if (bytes!=null && bytes.length > 0 && nombreArchivo!=null && !nombreArchivo.equals("")) {
	            AdjuntoFormato adjunto = new AdjuntoFormato();
	            adjunto.setOrdenId(suce.getIdOrden());
	            adjunto.setMto(orden.getMto());
	            adjunto.setFormatoId(suce.getIdFormato());
	            adjunto.setArchivo(bytes);
	            adjunto.setNombre(nombreArchivo);
	            adjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
	            adjuntos.add(adjunto);
            }
            suceService.rechazarModificacion(orden, suce, modificacionSuceId, mensaje, adjuntos);

            //message = new Message("vuce.administracionEntidad.rechazarModificacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.rechazarModificacionSuce.success");

        } catch (Exception e) {
            logger.error("ERROR: Al aceptar la Modificacion de SUCE", e);
            //message = new ErrorMessage("vuce.administracionEntidad.rechazarModificacionSuce.error", e, ""+suce.getNumSuce());
            message = new ErrorMessage("co.resolutor.evaluador.rechazarModificacionSuce.error", e);
        }

        messageList.add(message);
        return messageList;
    }

	/**
	 * Cierra la suce
	 */
    public MessageList cerrarSuce(Long suceId, Long drId, Long sdr, String tipoDr, String formato) {
        MessageList messageList = new MessageList();
        Message message = null;

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("suceId", suceId);
        try {

        	if ("A".equals(tipoDr) &&
        		(ConstantesCO.CO_SOLICITUD.equalsIgnoreCase(formato) ||
        		ConstantesCO.CO_DUPLICADO.equalsIgnoreCase(formato) ||
        		ConstantesCO.CO_REEMPLAZO.equalsIgnoreCase(formato)) ) {

        		resolutorService.confirmarDRFinFirma(drId, sdr);

        	}

        	if (!this.drEsRectificacion(drId, sdr.intValue())) {
        		resolutorService.cerrarSuce(params);
        	}

            message = new Message("co.resolutor.suce.cierre.success");

        } catch (Exception e) {
            logger.error("ERROR: Al cerrar la Modificacion de SUCE", e);
            //message = new ErrorMessage("co.resolutor.suce.cierre.error", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

	/**
	 * Denegación de la Solicitud por un Supervisor
	 */
	public MessageList denegarSolicitud(Map<String,Object> filtros) {
		MessageList messageList = new MessageList();
		Message message = null;
        try {
			resolutorService.denegarSolicitud(filtros);
	        message = new Message("update.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.denegarSolicitud", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
	}

	/**
	 * Metodo para que el evaluador actualice los datos de la dj
	 */
    public MessageList actualizarDjXEvaluador(Long djId, String partidaSegunAcuerdo, Integer secuenciaNorma, Integer secuenciaOrigen) {
        MessageList messageList = new MessageList();
        Message message = null;

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("djId", djId);
        params.put("partidaSegunAcuerdo", partidaSegunAcuerdo);
        params.put("secuenciaNorma", secuenciaNorma);
        params.put("secuenciaOrigen", secuenciaOrigen);
        try {

            resolutorService.actualizarDjXEvaluador(params);

            message = new Message("co.dj.evaluador.modifica.success");

        } catch (Exception e) {
            logger.error("ERROR: Al actualizar la DJ por parte del evaluador", e);
            message = new ErrorMessage("co.dj.evaluador.modifica.error", e);
        }

        messageList.add(message);
        return messageList;

    }

	/**
	 * Finaliza la evaluacion de DJs
	 */
    public MessageList confirmarFinDJEval(Long coId, Long suceId) {
        MessageList messageList = new MessageList();
        Message message = null;

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("coId", coId);
        params.put("suceId", suceId);
        try {

            resolutorService.confirmarFinDJEval(params);

            message = new Message("dj.resolutor.fin.evaluacion.success");

        } catch (Exception e) {
            logger.error("ERROR: Al confirmar la evaluacion de las DJs", e);
            //message = new ErrorMessage("vuce.administracionEntidad.aprobarModificacionSuce.error", e, ""+suce.getNumSuce());
            message = new ErrorMessage("dj.resolutor.fin.evaluacion.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    public String obtenerTipoDr(Long drId, Integer sdr) {
    	String res = "";
    	try {
    		res = this.resolutorService.obtenerTipoDr(drId, sdr);
    	} catch (Exception e) {
            logger.error("ERROR: Al confirmar la evaluacion de las DJs", e);
            //message = new ErrorMessage("vuce.administracionEntidad.aprobarModificacionSuce.error", e, ""+suce.getNumSuce());
            //message = new ErrorMessage("dj.resolutor.fin.evaluacion.error", e);
        }
    	return res;
    }

    public String obtenerTipoDrBorrador(Long borradorDrId) {
    	String res = "";
    	try{
    		res = this.resolutorService.obtenerTipoDrBorrador(borradorDrId);
    	} catch (Exception e) {
            logger.error("ERROR: Al confirmar la evaluacion de las DJs", e);
            //message = new ErrorMessage("vuce.administracionEntidad.aprobarModificacionSuce.error", e, ""+suce.getNumSuce());
            //message = new ErrorMessage("dj.resolutor.fin.evaluacion.error", e);
        }
    	return res;
    }

    public boolean drEsRectificacion(Long drId, Integer sdr) {
        boolean esRectificacion = false;
        try {

            esRectificacion = resolutorService.drEsRectificacion(drId, sdr);

        } catch (Exception e) {
            logger.error("Error en DocumentoAutorizanteLogicImpl.esRectificacion", e);
        }
        return esRectificacion;
    }

    public DR getDRBorradorById(Long drBorradorId) {
        DR dr = null;
        try {

            dr = resolutorService.getDRBorradorById(drBorradorId);

        } catch (Exception e) {
            logger.error("Error en DocumentoAutorizanteLogicImpl.eliminarRectificacionDR", e);
        }
        return dr;
    }

    /**
	 * Obtiene un borrador de rectificacion
	 */
	@SuppressWarnings("unchecked")
	public CertificadoOrigenDR getCODrRectificacionById(Long drId, String formato) {
		HashMap<String, Object> filter = new HashMap<String, Object>();
        filter.put("drId", drId);
        filter.put("formato", formato);
        CertificadoOrigenDR coDr = null;
        try {
        	coDr = resolutorService.getCODrRectificacionById(filter);
        } catch (Exception e) {
            logger.error("Error en CertificadoOrigenLogicImpl.getMtc001DrById", e);
        }
		return coDr;
	}

	/**
	 * Registra una Notificación de Subsanación
	 */
    public MessageList registrarNotificacionSubsanacionOrden(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	/*int ordenId = datos.getInt("ordenId");
        	int mto = datos.getInt("mto");*/
            //int suceId = datos.getInt("suceId");
            /*HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("orden", ordenId);
            filter.put("mto", mto);
            Orden orden = ordenService.loadOrdenDetail(filter);*/
        	//suce = suceService.loadSuceById(suceId);

            //filter.clear();
        	HashUtil<String, Object> filter = new HashUtil<String, Object>();
            if (datos.getString("notificacionId").equals("")) {
	            /*filter.put("suceId", suceId);*/
	            //filter.put("suce", suce.getNumSuce());
        		filter.put("ordenId", datos.getLong("ordenId"));
        		filter.put("mto", datos.getInt("mto"));
        		filter.put("mensaje", datos.getString("mensaje"));
	            filter.put("notificacionId", null);
	            Integer notificacionId = resolutorService.registrarNotificacionSubsanacionOrden(filter);
	            messageList.setObject(notificacionId);
            } else {
            	int notificacionId = datos.getInt("notificacionId");
	            filter.put("notificacionId", notificacionId);
	            filter.put("mensaje", datos.getString("mensaje"));
	            resolutorService.actualizarNotificacionSubsanacionOrden(filter);
            	messageList.setObject(notificacionId);
            }

            //message = new Message("vuce.administracionEntidad.registrarNotificacionSubsanacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.registrarNotificacionSubsanacionOrden.success");

        } catch (Exception e) {
            logger.error("ERROR: Al registrar la Notificacion de Subsanacion de la Solicitud", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);//new ErrorMessage("vuce.administracionEntidad.registrarNotificacionSubsanacionSuce.error", e, ""+suce.getNumSuce());
        }

        messageList.add(message);
        return messageList;
    }

    /**
     * Envia una Notificación de Subsanación
     * @param datos
     * @return
     */
    public MessageList enviarNotificacionSubsanacionOrden(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	//int suceId = datos.getInt("suceId");
            int notificacionId = datos.getInt("notificacionId");
            //suce = suceService.loadSuceById(suceId);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
    		filter.put("ordenId", datos.getLong("ordenId"));
    		filter.put("mto", datos.getInt("mto"));
            filter.put("notificacionId", notificacionId);
            resolutorService.enviarNotificacionSubsanacionOrden(filter);

            //message = new Message("vuce.administracionEntidad.enviarBorradorNotificacionSubsanacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.enviarBorradorNotificacionSubsanacionOrden.success");

        } catch (Exception e) {
            logger.error("ERROR: Al enviar la Notificacion de Subsanacion de la Solicitud", e);
            //message = new ErrorMessage("vuce.administracionEntidad.enviarBorradorNotificacionSubsanacionSuce.error", e, ""+suce.getNumSuce());
            message = new ErrorMessage("co.resolutor.evaluador.enviarBorradorNotificacionSubsanacionOrden.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    /**
     * Elimina una Notificación de Subsanación
     */
    public MessageList eliminarNotificacionSubsanacionOrden(HashUtil<String, String> datos) {
        MessageList messageList = new MessageList();
        Message message = null;
        Suce suce = null;
        try {
        	//int suceId = datos.getInt("suceId");
            int notificacionId = datos.getInt("notificacionId");
            //suce = suceService.loadSuceById(suceId);

            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("notificacionId", notificacionId);
	        resolutorService.eliminarNotificacionSubsanacionOrden(filter);

            //message = new Message("vuce.administracionEntidad.eliminarNotificacionSubsanacionSuce.success", ""+suce.getNumSuce());
            message = new Message("co.resolutor.evaluador.eliminarNotificacionSubsanacionOrden.success");

        } catch (Exception e) {
            logger.error("ERROR: Al eliminar la Notificacion de Subsanacion de la Solicitud", e);
            //message = new ErrorMessage("vuce.administracionEntidad.eliminarNotificacionSubsanacionSuce.error", e, ""+suce.getNumSuce());
            message = new ErrorMessage("co.resolutor.evaluador.eliminarNotificacionSubsanacionOrden.error", e);
        }

        messageList.add(message);
        return messageList;
    }

	/**
	 *  Registra un adjunto de la DR
	 */
	public MessageList cargarDatosFirmaAdjuntoCODR(HashUtil<String, Object> filter) {

    	MessageList messageList = new MessageList();
		Message message = null;
		try {
			resolutorService.cargarDatosFirmaAdjuntoCODR(filter);
			message = new Message("insert.success");

	    }
	    catch (Exception e) {
	        logger.error("Error en ResolutorLogicImpl.cargarDatosFirmaAdjuntoCODR", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    }
	    finally {
	    	messageList.add(message);
	    }

        return messageList;
    }

    /**
     * Devuelve N si no se debe mostrar el botón de impresión y S si se debe mostrar dicho botón
     *
     * @param suceId
     * @param drId
     * @param sdr
     * @return
     * @throws Exception
     */
	public String habilitarBtnFirma(Long suceId, Long drId, Long sdr) {
		try {
			return this.resolutorService.habilitarBtnFirma(suceId, drId, sdr);
		} catch (Exception e) {
			Log.debug(e.getStackTrace());
			return ConstantesCO.OPCION_NO;
		}
	}

	public void setResolutorService(ResolutorService resolutorService) {
		this.resolutorService = resolutorService;
	}

	public void setSuceService(SuceService suceService) {
		this.suceService = suceService;
	}

	/**
	 * Devuelve el numero de DJs que el evaluador haya indicado que no califican
	 *
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
    public int numDJsRechazadas(Map<String,Object> parametros) throws Exception{
    	return this.resolutorService.numDJsRechazadas(parametros);
    }

	/**
	 * Devuelve un indicador para mostrar u ocultar el boton de finalización de la SUCE
	 *
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
    public String mostrarBtnFinalizacion(Map<String,Object> parametros){
    	try {
    		return this.resolutorService.mostrarBtnFinalizacion(parametros);
		} catch (Exception e) {
			Log.debug(e.getStackTrace());
			return ConstantesCO.OPCION_NO;
		}

    }

}