package pe.gob.mincetur.vuce.co.logic.solicitudCalificacion;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.AdjuntoFormatoSC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.AdjuntoRequeridoSC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCAcuerdo;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCDetalleDj;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCMaterial;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.service.solicitudCalificacion.SCService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class SCLogicImpl implements SCLogic{
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

	private SCService scService = null;
	
	private AdjuntoService adjuntoService;

	public static Logger getLogger() {
		return logger;
	}

	public AdjuntoService getAdjuntoService() {
		return adjuntoService;
	}

	public void setAdjuntoService(AdjuntoService adjuntoService) {
		this.adjuntoService = adjuntoService;
	}

	public static void setLogger(Logger logger) {
		SCLogicImpl.logger = logger;
	}

	public SCService getScService() {
		return scService;
	}

	public void setScService(SCService scService) {
		this.scService = scService;
	}

	public MessageList creaSC(long ordenIdDJ, int mtoDJ, int entidadId, long usuario, long solicitante, long representante, String cargo) {
        HashUtil<String, Object> filterSC = new HashUtil<String, Object>();
        HashUtil<String, Object> filterUsuario = new HashUtil<String, Object>();
        HashUtil<String, Object> filterSolicitante = new HashUtil<String, Object>();
		HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
        HashUtil<String, Object> scDet = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        
        filterSC.put("usuarioId", usuario);
        filterSC.put("entidadId", entidadId);
        filterSC.put("ordenIdDJ", ordenIdDJ);
        filterSC.put("mtoDJ", mtoDJ);
        filterSC.put("ordenId", null);
        filterSC.put("mto", null);
        filterSC.put("calificacionId", null);
          
        try{
        
	    scDet = scService.insertSC(filterSC);

	    scDet.imprimir();
	    
        long ordenId = scDet.getLong("ordenId");
        int mto = scDet.getInt("mto");
        long calificacionId =  scDet.getLong("calificacionId");
        
        // inserto usuario
        filterUsuario.put("ordenId", ordenId);
        filterUsuario.put("mto", mto);
        filterUsuario.put("usuarioId", usuario);
        filterUsuario.put("usuarioFormatoTipo", ConstantesCO.USUARIO_USUARIO);
        filterUsuario.put("representanteId",null);
        filterUsuario.put("cargo", cargo);
        scService.insertUsuarioSC(filterUsuario);
        
        // inserto solicitante
        filterSolicitante.put("ordenId", ordenId);
        filterSolicitante.put("mto", mto);
        filterSolicitante.put("usuarioId", solicitante);
        filterSolicitante.put("usuarioFormatoTipo", ConstantesCO.USUARIO_SOLICITANTE);
        filterSolicitante.put("representanteId",null);
        filterUsuario.put("cargo", null);
        scService.insertUsuarioSC(filterSolicitante);
        
        if (representante != 0){
            // inserto representante
            filterRepresentante.put("ordenId", ordenId);
            filterRepresentante.put("mto", mto);
            filterRepresentante.put("usuarioId", solicitante);
            filterRepresentante.put("usuarioFormatoTipo", ConstantesCO.USUARIO_REPRESENTANTE);
            filterRepresentante.put("representanteId",representante);
            filterUsuario.put("cargo", null);
            scService.insertUsuarioSC(filterRepresentante);
        }
       
            SC scObj = getSCById(ordenId, mto);
            messageList.setObject(scObj);
            message = new Message("insert.success");
         
        } catch (Exception e) {
            logger.error("Error al crear la Orden", e);
            message = new ErrorMessage("insert.error", e);
        }

        messageList.add(message);

        return messageList;
    }
	
	public MessageList transmiteSC(long ordenId, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);

        try {
			scService.transmiteSC(filter);
			message = new Message("transmite.success");
		} catch (Exception e) {
			logger.error("Error al transmitir la Solicitud", e);
            message = new ErrorMessage("transmite.error", e);
		}
		
		SC scObj = getSCById(ordenId, mto);
        messageList.setObject(scObj);
        
        messageList.add(message);

        return messageList;
    }

	
	public SC convertMapToSC(Map<String, Object> map) {
		SC obj = new SC();
		obj.setCalificacionId(Util.longValueOf(map.get("calificacionId")));
		obj.setOrdenId(Util.longValueOf(map.get("ordenId")));
		obj.setMto(Util.integerValueOf(map.get("mto")));
		obj.setDescripcionComercial(map.get("descripcionComercial").toString());
		obj.setUmFisicaId(Util.integerValueOf(map.get("umFisicaId")));
		return obj;
	}
	
	public MessageList updateSC(SC sc) throws Exception {
		MessageList messageList = new MessageList();
		Message message = null;
		try {
			scService.updateSC(sc);
			message = new Message("update.success");
		} catch (Exception e) {
			logger.error("Error en updateSC", e);
			message = new ErrorMessage("update.error", e);
			//message = VuceUtil.getErrorMessage(e);
		} finally {
			messageList.add(message);
		}
		return messageList;
	}
	
	public SC getSCById(long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		SC obj = null;
		try {
			obj = scService.getSCById(filter);
		} catch (Exception e) {
			logger.error("Error en SCLogicImpl.getSCById", e);
		}
		return obj;
	}
	
	public SCMaterial convertMapToSCMaterial(Map<String, Object> map){
		SCMaterial obj = new SCMaterial();
		obj.setCalificacionId(Util.longValueOf(map.get("calificacionId")));
		obj.setSecuenciaCalifXDJ(Util.integerValueOf(map.get("secuenciaCalifXDJ")));
		//obj.setTipoOrigenMaterial(Util.integerValueOf(map.get("tipoOrigenMaterial")));		
        return obj;
	}
	
	public SCMaterial getSCMaterialById(long calificacionId, Integer secuenciaCalifXDJ) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("calificacionId", calificacionId);
		filter.put("secuenciaCalifXDJ", secuenciaCalifXDJ);
		SCMaterial obj = null;
		try {
			obj = scService.getSCMaterialById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getSCMaterialById", e);
		}
		return obj;
	}
	
	public UsuarioFormato getUsuarioFormatoById(long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		UsuarioFormato obj = null;
		try {
			obj = scService.getUsuarioFormatoById(filter);
		} catch (Exception e) {
			logger.error("Error en SCLogicImpl.getUsuarioFormatoById", e);
		}
		return obj;
	}
	
	public Solicitud getSolicitudById(long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		Solicitud obj = null;
		try {
			obj = scService.getSolicitudById(filter);
		} catch (Exception e) {
			logger.error("Error en SCLogicImpl.getSolicitudById", e);
		}
		return obj;
	}
	
    public Solicitante getSolicitanteDetail(long ordenId, int mto, int usuarioFormatoTipo) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
        Solicitante solicitante = null;
		try {
			solicitante = scService.getSolicitanteDetail(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return solicitante;
    }
    
	/*public MessageList updateSCMaterial (SCMaterial obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        try {
        	scService.updateMaterial(obj);
            message = new Message("update.success");
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.updateMaterial", e);
            message = new ErrorMessage("update.error",e);
            //message = VuceUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
	}
	*/
	 public MessageList updateRepresentante(long solicitante,long ordenId, int mto, long representante) {
	        if (representante== 0) return null;
	        
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        MessageList messageList = new MessageList();
	        filter.put("solicitante", solicitante);
	        filter.put("ordenId", ordenId);
	        filter.put("mto", mto);
	        filter.put("representante", representante);

	        Message message = null;
	        try {
	            scService.updateRepresentante(filter);
	            
	            SC scObj = getSCById(ordenId, mto);

	            messageList.setObject(scObj);
	            message = new Message("update.representante.success");

	        } catch (Exception e) {
	            logger.error("Error al actualizar al representante legal", e);
	            message = new ErrorMessage("update.representante.error", e);

	        }

	        messageList.add(message);
	        return messageList;
	    }
	 
	    public MessageList insertAcuerdoPais(long ordenId, int mto, int acuerdoInternacionalId, int paisIsoId ) {
	    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    	SC scObj = getSCById(ordenId, mto);
	        MessageList messageList = new MessageList();
	        messageList.setObject(scObj);
	        filter.put("calificacionId", scObj.getCalificacionId());
	        filter.put("acuerdoInternacionalId", acuerdoInternacionalId);
	        filter.put("paisIsoId", paisIsoId);
	        filter.put("secuenciaAcuerdo", null);
	        Message message = null;
	        try {
	            scService.insertAcuerdoPais(filter);
	            
	            message = new Message("insert.success");

	        } catch (Exception e) {
	            logger.error("Error al insertar el Acuerdo Pa�s", e);
	            message = new ErrorMessage("insert.error", e);

	        }
	        messageList.add(message);
	        return messageList;

	    }
	    
	    public MessageList deleteAcuerdoPais(long ordenId, int mto, int secuenciaAcuerdo){
	    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    	SC scObj = getSCById(ordenId, mto);
	        filter.put("calificacionId", scObj.getCalificacionId());
	        filter.put("secuenciaAcuerdo", secuenciaAcuerdo);
	
			MessageList messageList = new MessageList();
			messageList.setObject(scObj);
			Message message = null;
			
	        try {
	        	scService.deleteAcuerdoPais(filter);
		        message = new Message("delete.success");

		    } 
		    catch (Exception e) {
		        logger.error("Error en SCLogicImpl.deleteAcuerdoPais", e);
		        message = new ErrorMessage("delete.error", e);
		    }	
		    finally {
		    	messageList.add(message);
		    }        
		    return messageList;
	    }
	    
		public List<SCMaterial> getMaterialList(Long calificacionId) {
			List<SCMaterial> lista = null;
			try {
			    HashUtil<String, Object> filter = new HashUtil<String, Object>();
			    filter.put("calificacionId", calificacionId);
			    lista = scService.getMaterialList(filter);
			} catch (Exception e) {
				logger.error("Error en SCLogicImpl.getMaterialList", e);			
			}
			return lista;
		}
	
		 public MessageList updateCargoDeclarante(long solicitante,long ordenId, int mto, String cargo) {
	         HashUtil<String, Object> filter = new HashUtil<String, Object>();
	         MessageList messageList = new MessageList();
	         filter.put("solicitante", solicitante);
	         filter.put("ordenId", ordenId);
	         filter.put("mto", mto);
	         filter.put("cargo", cargo);

	         Message message = null;
	         try {
	            scService.updateCargoDeclarante(filter);
	            
	            SC scObj = getSCById(ordenId, mto);

	            messageList.setObject(scObj);
	            message = new Message("update.cargoDeclarante.success");

	         } catch (Exception e) {
	            logger.error("Error al actualizar al Cargo del Declarante", e);
	            message = new ErrorMessage("update.cargoDeclarante.error", e);
	         }

	         messageList.add(message);
	         return messageList;
	    }
		 
		 public AdjuntoRequeridoSC getAdjuntoRequeridoById(Integer adjuntoRequeridoSC) {
		        HashUtil<String, Object> filter = new HashUtil<String, Object>();
		        filter.put("adjuntoRequeridoSC", adjuntoRequeridoSC);
		        String titulo = null;
		        AdjuntoRequeridoSC adjunto = null;
		        try {
		        	adjunto = scService.getAdjuntoRequeridoById(filter);
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        return adjunto;
		    }

		    public MessageList uploadFile(Long ordenId, Integer mto, Integer adjuntoRequerido, String nombre, byte [] bytes) {
		        MessageList messageList = new MessageList();
		        
		        AdjuntoFormatoSC archivoAdjunto = new AdjuntoFormatoSC();
		        //archivoAdjunto.setOrdenId(ordenId);
		        archivoAdjunto.setMto(mto);
		        archivoAdjunto.setAdjuntoRequerido(adjuntoRequerido);
		        archivoAdjunto.setNombre("(Req-"+adjuntoRequerido+") "+ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
		        archivoAdjunto.setArchivo(bytes);
		        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
		        
		        Message message = null;
		        try {
		            adjuntoService.insertAdjunto(archivoAdjunto);
		            message = new Message("insert.success");
		        } catch (Exception e) {
		            logger.error("Error en SCLogicImpl.uploadFile", e);
		            message = ComponenteOrigenUtil.getErrorMessage(e);
		        }

		        messageList.add(message);
		        return messageList;
		    }
		    
			 public Integer getAdjuntoRequeridoCount(long ordenId,int mto){
				 HashUtil<String, Object> filter = new HashUtil<String, Object>();
		         MessageList messageList = new MessageList();
		         filter.put("ordenId", ordenId);
		         filter.put("mto", mto);
		         Integer cuenta = 0;
		         
		         try {
		        	 cuenta = scService.getAdjuntoRequeridoCount(filter);
				} catch (Exception e) {
					logger.error("Error al consultar el numero de adjuntos requeridos obligatorios", e);
				}
				 return cuenta;
			 }
			 
			 public SC registrarBorradorDr(Long suceId) throws Exception {
				    SC sc = new SC();
				    sc.setSuceId(suceId);
			    	try{
			    		sc = scService.insertBorradorResolutivoDr(sc);
			    	}catch(Exception e){
			    		logger.error("Error en SCServiceImpl.registrarBorrador", e);
			    	}
			    	return sc;
			    }
			    
			 public MessageList deleteBorradorDr(Long borradorDrId, Long numSuce, Long formatoDrId, Long suceId) {
			    	MessageList messageList = new MessageList();
			    	Message message = null;
			    	SC sc = new SC();
				    sc.setSuceId(suceId);
			        try {
			        	scService.deleteBorradorResolutivoDr(sc);
			            message = new Message("delete.success");
			        } catch (Exception e) {
			            logger.error("Error en FormatoServiceImpl.deleteBorradorResolutivoDr", e);
			            message = new ErrorMessage("delete.error", e);
			        }

			        messageList.add(message);
			        return messageList;
			}
			 
				
		public SC getSCDRById(long calificacionDrId) {
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("calificacionDrId", calificacionDrId);
			SC obj = null;
			try {
				obj = scService.getSCDRById(filter);
			} catch (Exception e) {
				logger.error("Error en SCLogicImpl.getSCById", e);
			}
				return obj;
		}
		
	    public Solicitante getSolicitanteDRDetail(long drId, int sdr, int usuarioFormatoTipo) {
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("drId", drId);
	        filter.put("sdr", sdr);
	        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
	        Solicitante solicitante = null;
			try {
				solicitante = scService.getSolicitanteDRDetail(filter);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        return solicitante;
	    }
	    
	    public Solicitante getSolicitanteDRBorradorDetail(long calificacionDrId, int usuarioFormatoTipo) {
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("calificacionDrId", calificacionDrId);
	        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
	        Solicitante solicitante = null;
			try {
				solicitante = scService.getSolicitanteDRBorradorDetail(filter);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        return solicitante;
	    }
	    
		public SCMaterial getSCMaterialDRById(long calificacionDrId, Integer secuenciaCalifXDJ) {
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("calificacionDrId", calificacionDrId);
			filter.put("secuenciaCalifXDJ", secuenciaCalifXDJ);
			SCMaterial obj = null;
			try {
				obj = scService.getSCMaterialDRById(filter);
			} catch (Exception e) {
				logger.error("Error en SCLogicImpl.getSCMaterialDRById", e);
			}
			return obj;
		}
		
		public SC convertMapToSCDR(Map<String, Object> map) {
			SC obj = new SC();
			obj.setCalificacionDrId(Util.longValueOf(map.get("calificacionDrId")));
			obj.setDescripcionComercial(map.get("descripcionComercial").toString());
			obj.setUmFisicaId(Util.integerValueOf(map.get("umFisicaId")));
			obj.setPartidaArancelaria(Util.longValueOf(map.get("partidaArancelaria")));
			return obj;
		}
		
		public MessageList updateSCDR(SC sc) throws Exception {
			MessageList messageList = new MessageList();
			Message message = null;
			try {
				scService.updateSCDR(sc);
				message = new Message("update.success");
			} catch (Exception e) {
				logger.error("Error en updateSC", e);
				message = new ErrorMessage("update.error", e);
				//message = VuceUtil.getErrorMessage(e);
			} finally {
				messageList.add(message);
			}
			return messageList;
		}
		
		public SCAcuerdo getSCAcuerdoDRById(long calificacionDrId, Integer secuenciaAcuerdo) {
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("calificacionDrId", calificacionDrId);
			filter.put("secuenciaAcuerdo", secuenciaAcuerdo);
			SCAcuerdo obj = null;
			try {
				obj = scService.getSCAcuerdoDRById(filter);
			} catch (Exception e) {
				logger.error("Error en SCLogicImpl.getSCAcuerdoDRById", e);
			}
			return obj;
		}
		
		public SCAcuerdo convertMapToSCAcuerdoDR(Map<String, Object> map){
			SCAcuerdo obj = new SCAcuerdo();
			obj.setCalificacionDrId(Util.longValueOf(map.get("calificacionDrId")));
			obj.setSecuenciaAcuerdo(Util.integerValueOf(map.get("secuenciaAcuerdo")));
			obj.setPaisIsoId(Util.integerValueOf(map.get("paisIsoId")));
			obj.setAcuerdoInternacionalId(Util.integerValueOf(map.get("acuerdoInternacionalId")));
			obj.setCalificacion(map.get("calificacion").toString());
			obj.setNorma(map.get("norma").toString());
			obj.setCriterioOrigen(map.get("criterioOrigen").toString());
			obj.setMotivoCalificacionNegativa(map.get("motivoCalificacionNegativa").toString());	
	        return obj;
		}
		
		public MessageList updateSCAcuerdoDR (SCAcuerdo obj){
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        MessageList messageList = new MessageList();
	        Message message = null;
	        try {
	        	scService.updateAcuerdoDR(obj);
	            message = new Message("update.success");
	        } catch (Exception e) {
	            logger.error("Error en DJLogicImpl.updateSCAcuerdoDR", e);
	            message = new ErrorMessage("update.error",e);
	            //message = VuceUtil.getErrorMessage(e);
	        }
	
	        messageList.add(message);
        return messageList;
	}

	public MessageList transmiteResolutor(long calificacionDrBorradorId) {
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        MessageList messageList = new MessageList();
	        Message message = null;
	        filter.put("calificacionDrBorradorId", calificacionDrBorradorId);
	        
	        System.out.println("Transmite Logic");
	        try {
				scService.transmiteResolutor(filter);
				message = new Message("transmite.success");
			} catch (Exception e) {
				logger.error("Error al transmitir el Resolutor", e);
	            message = new ErrorMessage("transmite.error", e);
			}
	        
	        messageList.add(message);

	        return messageList;
	    }
	
	public List<SCAcuerdo> getAcuerdoList(Long calificacionDrId) {
		List<SCAcuerdo> lista = null;
		try {
		    HashUtil<String, Object> filter = new HashUtil<String, Object>();
		    filter.put("calificacionDrId", calificacionDrId);
		    lista = scService.getAcuerdoList(filter);
		} catch (Exception e) {
			logger.error("Error en SCLogicImpl.getAcuerdoList", e);			
		}
		return lista;
	}
	
	 public SCDetalleDj getDatosDJParaCalificacion(long ordenId, Integer mto) {
		 SCDetalleDj resultado = null;
		 try {
			 HashUtil<String, Object> filter = new HashUtil<String, Object>();
			 filter.put("ordenId", ordenId);
			 filter.put("mto", mto);
			 resultado = scService.getDatosDJParaCalificacion(filter);
		 } catch (Exception e) {
			logger.error("Error en SCLogicImpl.getDatosDJParaCalificacion", e);			
		 }
		return resultado;
	 }
	
		   
}
