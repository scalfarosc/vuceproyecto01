package pe.gob.mincetur.vuce.co.logic.certificado;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.list.MessageList;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DataSourceUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoFormatoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoRequeridoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificado.Certificado;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.service.certificado.CertificadoService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;


public class CertificadoLogicImpl implements CertificadoLogic {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

	private IbatisService ibatisService;
	
	private CertificadoService certificadoService = null;
	
	private AdjuntoService adjuntoService;
	
	public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public void setCertificadoService(CertificadoService certificadoService) {
		this.certificadoService = certificadoService;
	}
	
	public void setAdjuntoService(AdjuntoService adjuntoService) {
		this.adjuntoService = adjuntoService;
	}

	public MessageList creaCertificado(Integer usuarioId, Integer entidadCertificadoraId, Integer formatoId, Integer solicitante, Integer representante, String cargo) {
        HashUtil<String, Object> filterCertificado = new HashUtil<String, Object>();
        
        HashUtil<String, Object> certificadoDet = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        
        filterCertificado.put("usuarioId", usuarioId);
        filterCertificado.put("entidadCertificadoraId", entidadCertificadoraId);
        filterCertificado.put("certificadoFormatoId", formatoId);
        filterCertificado.put("ordenId", null);
        filterCertificado.put("mto", null);
        filterCertificado.put("certificadoId", null);
        
        try {
	        certificadoDet = certificadoService.insertCertificado(filterCertificado);
	        
		    System.out.println("desde LOGIC");
		    certificadoDet.imprimir();
		    
	        Long ordenId = certificadoDet.getLong("ordenId");
	        Integer mto = certificadoDet.getInt("mto");
	        Long certificadoId =  certificadoDet.getLong("certificadoId");
	        
	        // inserto usuario
	        HashUtil<String, Object> filterUsuario = new HashUtil<String, Object>();
	        filterUsuario.put("ordenId", ordenId);
	        filterUsuario.put("mto", mto);
	        filterUsuario.put("usuarioId", usuarioId);
	        filterUsuario.put("usuarioFormatoTipo", ConstantesCO.USUARIO_USUARIO);
	        filterUsuario.put("representanteId",null);
	        filterUsuario.put("cargo", cargo);
	        certificadoService.insertUsuarioCertificado(filterUsuario);
	        
	        // inserto solicitante
	        HashUtil<String, Object> filterSolicitante = new HashUtil<String, Object>();
	        filterSolicitante.put("ordenId", ordenId);
	        filterSolicitante.put("mto", mto);
	        filterSolicitante.put("usuarioId", solicitante);
	        filterSolicitante.put("usuarioFormatoTipo", ConstantesCO.USUARIO_SOLICITANTE);
	        filterSolicitante.put("representanteId", null);
	        filterUsuario.put("cargo", null);
	        certificadoService.insertUsuarioCertificado(filterSolicitante);
	        
	        if (representante != null && representante != 0) {
	            // inserto representante
				HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
	            filterRepresentante.put("ordenId", ordenId);
	            filterRepresentante.put("mto", mto);
	            filterRepresentante.put("usuarioId", solicitante);
	            filterRepresentante.put("usuarioFormatoTipo", ConstantesCO.USUARIO_REPRESENTANTE);
	            filterRepresentante.put("representanteId",representante);
	            filterUsuario.put("cargo", null);
	            certificadoService.insertUsuarioCertificado(filterRepresentante);
	        }
            
            //Solicitud solicitudObj = getSolicitudById(ordenId, mto);
	        Certificado certificadoObj = getCertificadoById(ordenId, mto);
            messageList.setObject(certificadoObj);
            message = new Message("insert.success");
            
        } catch (Exception e) {
            logger.error("Error al crear el Certificado", e);
            //message = new ErrorMessage("insert.error", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);

        return messageList;
    }
	
	public MessageList transmiteCertificado(Long ordenId, Integer mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);

        try {
        	certificadoService.transmiteCertificado(filter);
			message = new Message("transmite.success");
		} catch (Exception e) {
			logger.error("Error al transmitir la Solicitud", e);
            message = new ErrorMessage("transmite.error", e);
		}
		
		Certificado certificadoObj = getCertificadoById(ordenId, mto);
        messageList.setObject(certificadoObj);
        
        messageList.add(message);

        return messageList;
    }

	
	public Certificado getCertificadoById(Long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		
		Certificado obj = null;
		try {
			obj = certificadoService.getCertificadoById(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoLogicImpl.getCertificadoById", e);
		}
		return obj;
	}
	
	public UsuarioFormato getUsuarioFormatoById(Long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		UsuarioFormato obj = null;
		try {
			obj = certificadoService.getUsuarioFormatoById(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoLogicImpl.getUsuarioFormatoById", e);
		}
		return obj;
	}
	
	public Solicitud getSolicitudById(Long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		Solicitud obj = null;
		try {
			obj = certificadoService.getSolicitudById(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoLogicImpl.getSolicitudById", e);
		}
		return obj;
	}
	
    public Solicitante getSolicitanteDetail(Long ordenId, Integer mto, Integer usuarioFormatoTipo) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
        Solicitante solicitante = null;
		try {
			solicitante = certificadoService.getSolicitanteDetail(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoLogicImpl.getSolicitanteDetail", e);
		}
        return solicitante;
    }
    
    public AdjuntoRequeridoCertificado getAdjuntoRequeridoById(Integer certificadoFormatoId, Integer adjuntoRequeridoCertificado) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("certificadoFormatoId", certificadoFormatoId);
        filter.put("adjuntoRequeridoCertificado", adjuntoRequeridoCertificado);
        String titulo = null;
        AdjuntoRequeridoCertificado adjunto = null;
        try {
        	adjunto = certificadoService.getAdjuntoRequeridoById(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return adjunto;
    }

    public MessageList uploadFile(Long ordenId, Integer mto, Integer adjuntoRequerido, String nombre, byte [] bytes) {
        MessageList messageList = new MessageList();
        
        AdjuntoFormatoCertificado archivoAdjunto = new AdjuntoFormatoCertificado();
        //archivoAdjunto.setOrdenId(ordenId);
        archivoAdjunto.setMto(mto);
        archivoAdjunto.setAdjuntoRequerido(adjuntoRequerido);
        archivoAdjunto.setNombre("(Req-"+adjuntoRequerido+") "+ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
        
        Message message = null;
        try {
            adjuntoService.insertAdjunto(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en CertificadoLogicImpl.uploadFile", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

    public MessageList updateRepresentante(Long solicitante, Long ordenId, Integer mto, Integer representante) {
        if (representante==0 || representante==null) return null;
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        filter.put("solicitante", solicitante);
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("representante", representante);
        
        Message message = null;
        try {
            certificadoService.updateRepresentante(filter);
            
            Certificado certificadoObj = getCertificadoById(ordenId, mto);
            messageList.setObject(certificadoObj);
            
            message = new Message("update.representante.success");
            
        } catch (Exception e) {
            logger.error("Error al actualizar al representante legal", e);
            message = new ErrorMessage("update.representante.error", e);
        }

        messageList.add(message);
        return messageList;
    }
    
    public MessageList updateCargoDeclarante(Long declarante, Long ordenId, Integer mto, String cargo) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        filter.put("solicitante", declarante);
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("cargo", cargo);
        
        Message message = null;
        try {
            certificadoService.updateCargoDeclarante(filter);
            
            Certificado certificadoObj = getCertificadoById(ordenId, mto);
            messageList.setObject(certificadoObj);
            
            message = new Message("update.cargoDeclarante.success");
            
        } catch (Exception e) {
            logger.error("Error al actualizar al Cargo del Declarante", e);
            message = new ErrorMessage("update.cargoDeclarante.error", e);
        }

        messageList.add(message);
        return messageList;
    }
    
    public Integer getAdjuntoRequeridoCount(long ordenId,int mto){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        Integer cuenta = 0;
        
        try {
       	    cuenta = certificadoService.getAdjuntoRequeridoCount(filter);
		} catch (Exception e) {
			logger.error("Error al consultar el numero de adjuntos requeridos obligatorios", e);
		}
		return cuenta;
	}
    
    public byte [] imprimirCertificado(UsuarioCO usuario , HashUtil<String, String> datos) {
    	byte [] bytes = null;
        
        try {
        	Long ordenId = datos.getLong("ordenId");
            Integer mto = datos.getInt("mto");
            
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("ordenId", ordenId);
            filter.put("mto", mto);
            
            //HashUtil<String, Object> cabecera = ibatisService.loadElement("certificado.vistaPrevia.cabecera", filter);
            HashUtil<String, Object> cabecera = null;//ibatisService.loadElement("certificado.vistaPrevia.cabecera", filter);
            
            Map parameters = new HashMap();
            parameters.put("P_DATOS_EXPORTADOR", cabecera.getString("DATOS_EXPORTADOR"));
            parameters.put("P_DATOS_IMPORTADOR", cabecera.getString("DATOS_IMPORTADOR"));
            //parameters.put("P_MEDIO_TRANSPORTE", cabecera.getString("MEDIO_TRANSPORTE"));
            parameters.put("P_DETALLE_EVALUADOR", cabecera.getString("DETALLE_EVALUADOR"));
            parameters.put("P_FECHA_TRANSMISION", cabecera.get("FECHA_TRANSMISION"));
            parameters.put("P_PAIS_IMPORTACION", cabecera.getString("PAIS_IMPORTACION"));
            parameters.put("P_ORDEN_ID", cabecera.getLong("ORDEN_ID"));
            parameters.put("P_MTO", cabecera.getInt("MTO"));
            
        	String formato = datos.getString("formato");
			Resource jasperFile = ApplicationConfig.getApplicationConfig().getApplicationResource("reportes.certificado."+formato, "");
			
			JasperReport jReport = (JasperReport)JRLoader.loadObject(jasperFile.getInputStream());
			
			Connection conn = DataSourceUtils.getConnection((DataSource)SpringContext.getApplicationContext().getBean("dataSource"));
			JasperPrint print = JasperFillManager.fillReport(jReport, parameters, conn);
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            JasperExportManager.exportReportToPdfStream(print, baos);
            baos.flush();
            baos.close();
            
            bytes = baos.toByteArray();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }
    
    public HashUtil<String, Object> registrarBorradorDr(Long suceId) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>(); 
    	HashUtil<String, Object> element = new HashUtil<String, Object>();
    	filter.put("suceId", suceId);
    	filter.put("certificadoDrBorradorId", null);
    	filter.put("certificadoDrId", null);
		    
	    try{
	    	element = certificadoService.insertBorradorResolutivoDr(filter);
	    }catch(Exception e){
	    	logger.error("Error en CertificadoServiceImpl.registrarBorradorDr", e);
	    }
	    
	    return element;
	}
	    
	public MessageList deleteBorradorDr(Long certificadoDrBorradorId) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>(); 	
		filter.put("certificadoDrBorradorId", certificadoDrBorradorId);
		MessageList messageList = new MessageList();
	    Message message = null;
	    try {
	        	certificadoService.deleteBorradorResolutivoDr(filter);
	            message = new Message("delete.success");
	    } catch (Exception e) {
	            logger.error("Error en FormatoServiceImpl.deleteBorradorResolutivoDr", e);
	            message = new ErrorMessage("delete.error", e);
	    }
	
	    messageList.add(message);
	    return messageList;
	}
	
	public MessageList transmiteResolutor(long certificadoDrBorradorId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        filter.put("certificadoDrBorradorId", certificadoDrBorradorId);
        
        try {
        	certificadoService.transmiteResolutor(filter);
			message = new Message("transmite.success");
		} catch (Exception e) {
			logger.error("Error al transmitir el Resolutor", e);
            message = new ErrorMessage("transmite.error", e);
		}
        
        messageList.add(message);
        return messageList;
    }
	
	 public Solicitante getSolicitanteDRDetail(long drId, int sdr, int usuarioFormatoTipo) {
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("drId", drId);
	        filter.put("sdr", sdr);
	        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
	        Solicitante solicitante = null;
			try {
				solicitante = certificadoService.getSolicitanteDRDetail(filter);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        return solicitante;
	    }
	    
	public Solicitante getSolicitanteDRBorradorDetail(long certificadoDrId, int usuarioFormatoTipo) {
	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        filter.put("certificadoDrId", certificadoDrId);
	        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
	        Solicitante solicitante = null;
			try {
				solicitante = certificadoService.getSolicitanteDRBorradorDetail(filter);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        return solicitante;
	    }
    
	public Certificado getCertificadoDRById(Long certificadoDrId) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("certificadoDrId", certificadoDrId);
		
		Certificado obj = null;
		try {
			obj = certificadoService.getCertificadoDRById(filter);
		} catch (Exception e) {
			logger.error("Error en CertificadoLogicImpl.getCertificadoDRById", e);
		}
		return obj;
	}
}
