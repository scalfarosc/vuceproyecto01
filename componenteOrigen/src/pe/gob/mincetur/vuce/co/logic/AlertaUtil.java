package pe.gob.mincetur.vuce.co.logic;

import org.apache.log4j.Logger;
import org.jlis.core.springframework.context.SpringContext;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;

import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;

public class AlertaUtil {
    
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);
    
	public static void registrarErrorProceso(Long orden, Long suce, Long dr, Long vcId, Exception e) {
		try {
			IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
			HashUtil <String, Object> filter = new HashUtil <String, Object>();
			filter.put("orden", orden);
			filter.put("suce", suce);
			filter.put("dr", dr);
			filter.put("vcId", vcId);
			filter.put("mensaje", ComponenteOrigenUtil.getStackTrace(e));
			//ibatisService.executeSPWithObject("alerta.errorProceso", filter);
		} catch (Exception ex) {
			logger.error("ERROR: Al registrar la alerta", ex);
		}
	}
	
	public static void registrarErrorProcesoSUNAT(String cda, Integer idLiquidacion, Exception e) {
		try {
			IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
			HashUtil <String, Object> filter = new HashUtil <String, Object>();
			filter.put("cda", cda);
			filter.put("idLiquidacion", idLiquidacion);
			filter.put("mensaje", ComponenteOrigenUtil.getStackTrace(e));
			//ibatisService.executeSPWithObject("alerta.errorProcesoSUNAT", filter);
		} catch (Exception ex) {
			logger.error("ERROR: Al registrar la alerta", ex);
		}
	}
	
	public static void registrarErrorConexionWS(Integer entidadId, String nombreWS, Exception e) {
		try {
			IbatisService ibatisService = (IbatisService)SpringContext.getApplicationContext().getBean("ibatisService");
			HashUtil <String, Object> filter = new HashUtil <String, Object>();
			filter.put("entidadId", entidadId);
			filter.put("nombreWS", nombreWS);
			filter.put("mensaje", ComponenteOrigenUtil.getStackTrace(e));
			//ibatisService.executeSPWithObject("alerta.errorConexionWS", filter);
		} catch (Exception ex) {
			logger.error("ERROR: Al registrar la alerta", ex);
		}
	}
	
}
