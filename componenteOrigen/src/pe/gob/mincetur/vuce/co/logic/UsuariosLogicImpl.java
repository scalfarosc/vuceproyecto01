package pe.gob.mincetur.vuce.co.logic;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.service.ibatis.IbatisServiceImpl;
import org.jlis.web.bean.Cell;
import org.jlis.web.list.Row;
import org.jlis.web.list.Table;

import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanRso;
import ConsultaRuc.BeanT1144;
import pe.gob.mincetur.vuce.co.bean.EntidadCertificadora;
import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Usuario;
import pe.gob.mincetur.vuce.co.remoting.ws.client.ConsultaRUCCWS;
import pe.gob.mincetur.vuce.co.service.UsuariosService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.TipoDocumentoType;

public class UsuariosLogicImpl implements UsuariosLogic {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);
    
    private IbatisService ibatisService;
    
    private UsuariosService usuariosService;
    
    private FormatoLogic formatoLogic;
    
    public void setIbatisService(IbatisService ibatisService) {
        this.ibatisService = ibatisService;
    }

    public void setUsuariosService(UsuariosService usuariosService) {
        this.usuariosService = usuariosService;
    }
    
    public void setFormatoLogic(FormatoLogic formatoLogic) {
		this.formatoLogic = formatoLogic;
	}
    
    public void testGrabacionUsuarioContextIdentifier(UsuarioCO usuario) {
        Random r = new Random();
        r.setSeed(12987);
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("c1", usuario.getLogin());
        filter.put("c2", Util.getDate());
        filter.put("c3", ""+r.nextLong());
        usuariosService.testGrabacionUsuarioContextIdentifier(filter);
        
        filter.clear();
        filter.put("usuario", usuario.getLogin());
        String resp = ((HashMap<String, String>)IbatisServiceImpl.getIbatisService().loadElement("vuce.authentication.usuario.element", filter)).get("CLAVE");
        logger.debug("Dato: "+resp);
        
        filter.clear();
        filter.put("c1", usuario.getLogin());
        filter.put("c2", Util.getDate());
        filter.put("c3", ""+r.nextLong());
        usuariosService.testGrabacionUsuarioContextIdentifier(filter);
    }
    
    public Usuario convertMapToUsuario(Map<String, Object> map) {
        Usuario usuario = new Usuario();
        usuario.setUsuarioSol(map.get("usuariosol")!=null?map.get("usuariosol").toString():null);
        usuario.setUsuarioTipoId(Util.integerValueOf(map.get("usuarioTipoId")));
        usuario.setDistritoId(map.get("distrito")!=null ? Util.integerValueOf(map.get("distrito")) : null);
        usuario.setDireccion(map.get("direccion")!=null ? map.get("direccion").toString() : null);
        usuario.setReferencia(map.get("referencia")!=null ? map.get("referencia").toString() : null);
        usuario.setArea(map.get("area")!=null ? map.get("area").toString() : null);
        usuario.setSubArea(map.get("subArea")!=null ? map.get("subArea").toString() : null);
        usuario.setCargo(map.get("cargo")!=null ? map.get("cargo").toString() : null);
        usuario.setEmail(map.get("email")!=null ? map.get("email").toString() : null);
        usuario.setEmpresa(map.get("esEmpresa").toString());
        usuario.setEstado("1");
        usuario.setFax(map.get("fax")!=null ? map.get("fax").toString() : null);
        usuario.setNombre(map.get("nombre")!=null ? map.get("nombre").toString() : null);
        usuario.setTipoDocumento(Util.integerValueOf(map.get("tipodocumento")));
        usuario.setNumeroDocumento(map.get("numdoc")!=null?map.get("numdoc").toString():null);
        usuario.setTipoDocumentoUS(Util.integerValueOf(map.get("tipodocumentoUS")));
        usuario.setNumeroDocumentoUS(map.get("numdocUS")!=null?map.get("numdocUS").toString():null);
        usuario.setPrincipal(map.get("principal")!=null ? map.get("principal").toString() :"N");
        usuario.setTelefono(map.get("telefono")!=null ? map.get("telefono").toString() : null);
        usuario.setCelular(map.get("celular")!=null ? map.get("celular").toString() : null);
        usuario.setRecibeNotificacion((map.get("recibeNotificacion") == null || map.get("recibeNotificacion").equals("")) ? "N" : map.get("recibeNotificacion").toString());
        usuario.setTipoPersona(Util.integerValueOf(map.get("tipoPersona")));
        //usuario.setEsAgenteAduanas(map.get("esAgenteAduanas")!=null ? map.get("esAgenteAduanas").toString():"N");
        //usuario.setEsLaboratorio(map.get("esLaboratorio")!=null ? map.get("esLaboratorio").toString():"N");
        usuario.setCodigoEntidadSunat(map.get("nroRegistro")!=null ? map.get("nroRegistro").toString() : null);
        usuario.setUsuarioId(Util.integerValueOf(map.get("idUsuario")));
        usuario.setEntidadId(Util.integerValueOf(map.get("idEntidad")));
        return usuario;
    }
    
    public RepresentanteLegal convertMapToRepresentanteLegal(Map<String, Object> map) {
        RepresentanteLegal repLegal = new RepresentanteLegal();
        repLegal.setNombre(map.get("nombre").toString());
        repLegal.setNumdoc(map.get("nrodoc").toString());
        
        return repLegal;
    }
    
    public int existeUsuarioSOL(String usuarioSol , String numDoc) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Message message = null;
        MessageList messageList = new MessageList();
        filter.put("usuario_sol", usuarioSol);
        filter.put("numero_documento", numDoc);
        filter.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_RUC);
        
        int existe = 0;
        try {
            existe = usuariosService.existeUsuarioSol(filter);
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.existeUsuario", e);
            message = new ErrorMessage("error.crear.usuario", e);
        }
        messageList.add(message);
        return existe;
    }
    
    public int existeUsuarioExtranet(String login) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Message message = null;
        MessageList messageList = new MessageList();
        filter.put("numero_documento", login);
        filter.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_EXTRANET);
        
        int existe = 0;
        try {
            existe = usuariosService.existeUsuarioExtranet(filter);
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.existeUsuario", e);
            message = new ErrorMessage("error.crear.usuario", e);
        }
        messageList.add(message);
        return existe;
    }
    
    public int existeUsuarioDNI(String login) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Message message = null;
        MessageList messageList = new MessageList();
        filter.put("numero_documento", login);
        filter.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_DNI);
        
        int existe = 0;
        try {
            existe = usuariosService.existeUsuarioDNI(filter);
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.existeUsuario", e);
            message = new ErrorMessage("error.crear.usuario", e);
        }
        messageList.add(message);
        return existe;
    }
    
    public boolean existeEmpresa(String numDoc) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Message message = null;
        MessageList messageList = new MessageList();
        filter.put("numero_documento", numDoc);
        filter.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_RUC);
        
        boolean existe = false;
        try {
            existe = usuariosService.existeEmpresa(filter);
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.existeEmpresa", e);
            message = new ErrorMessage("error.crear.usuario", e);
        }
        messageList.add(message);
        return existe;
    }
    
    public boolean usuarioTieneTramites(UsuarioCO usuario) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        filter.put("idUsuario", usuario.getIdUsuario());
        
        boolean tiene = false;
        try {
        	tiene = usuariosService.usuarioTieneTramites(filter);
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.usuarioTieneTramites", e);
        }
        return tiene;
    }
    
    public void actualizarRolesUsuario(UsuarioCO usuario) {
        try {
        	if (usuario.getRoles()!=null && !usuario.getRoles().isEmpty()) {
        	    HashUtil roles = usuariosService.actualizarRolesUsuario(usuario.getIdUsuario(), usuario.getRoles());
        	    usuario.setRoles(roles);
        	}
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.actualizarRolesUsuario", e);
        }
    }
    
    public HashUtil cargarRolesUsuario(int idUsuario) {
    	return usuariosService.cargarRolesUsuario(idUsuario);
    }
    
    public EntidadCertificadora obtenerEntidadCertificadora(String cadena) {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("cadena", cadena);
    	
    	EntidadCertificadora ec = null;
    	
    	try {
    		ec = (EntidadCertificadora)ibatisService.loadObject("usuario.entidadDesdeCodigoSUNAT", filter);
    	} catch(Exception e) {
    		logger.error("ERROR: Al cargar la entidad certificadora", e);
    	}
    	return ec;
    }
    
    private String obtenerCodigoUnidadOrigenEntidadCertificadora(UsuarioCO usuarioCO) {
    	EntidadCertificadora ec = obtenerEntidadCertificadora(usuarioCO.getNombreCompleto());
    	
    	if (ec!=null) {
    	    return ec.getCodigo();
    	}
    	
    	return null;
    }
    
    public MessageList crearUsuario(Usuario usuario, Usuario empresa, Table representantesLegales, UsuarioCO usuarioCO) {
        HashUtil<String, Object> filterUsuario = new HashUtil<String, Object>();
        HashUtil<String, Object> filterEmpresa = new HashUtil<String, Object>();
        
        MessageList messageList = new MessageList();
        
        // Datos del Usuario
        filterUsuario.put("documento_tipo", usuario.getTipoDocumento());
        filterUsuario.put("numero_documento", usuario.getNumeroDocumento());
        filterUsuario.put("usuario_sol", usuario.getUsuarioSol());
        filterUsuario.put("nombre", usuario.getNombre());
        filterUsuario.put("email", usuario.getEmail());
        filterUsuario.put("recibeNotificacion", usuario.getRecibeNotificacion());
        filterUsuario.put("telefono", usuario.getTelefono());
        filterUsuario.put("celular", usuario.getCelular());
        filterUsuario.put("fax", usuario.getFax());
        filterUsuario.put("direccion", usuario.getDireccion());
        filterUsuario.put("referencia", usuario.getReferencia());
        filterUsuario.put("distrito_id", usuario.getDistritoId());
        filterUsuario.put("usuario_tipo_id", usuario.getUsuarioTipoId());
        filterUsuario.put("principal", usuario.getPrincipal());
        filterUsuario.put("persona_tipo", usuario.getTipoPersona());
        filterUsuario.put("cargo", usuario.getCargo());
        filterUsuario.put("documento_tipo_us", usuario.getTipoDocumentoUS());
        filterUsuario.put("numero_documento_us", usuario.getNumeroDocumentoUS());
        filterUsuario.put("ingreso_a_mr", usuarioCO.getIngresoMR());
        filterUsuario.put("ingreso_a_uo", "S"); // Es "S" porque estamos en Componente Origen
        
        Message message = null;
        try {
            // Si es un usuario EXTRANET, tiene un id de Entidad
	        if (usuario.getTipoDocumento().equals(ConstantesCO.TIPO_DOCUMENTO_EXTRANET) && usuario.getCodigoEntidadSunat()!=null) {
	            HashUtil<String, Object> filter = new HashUtil<String, Object>();
	            filter.put("codigoEntidad", usuario.getCodigoEntidadSunat());
	            
	        	// Obtener el prefijo que identifica a que Entidad Certificadora pertenece el usuario Extranet
	        	String codigoUnidadOrigen = obtenerCodigoUnidadOrigenEntidadCertificadora(usuarioCO);
	        	
	        	// Si el usuario es de alguna Entidad Certificadora, buscar el "entidad_id" correspondiente
	        	if (codigoUnidadOrigen!=null) {
	        		filter.put("codigoUnidadOrigen", codigoUnidadOrigen);
	        		
		            // Obtener el ID de la Entidad en base al CODIGO DE ENTIDAD capturado desde el logueo y al CODIGO DE UNIDAD DE ORIGEN obtenido 
		            Integer idEntidad = ibatisService.loadElement("usuario.claveEntidadDesdeCodigoSUNAT", filter).getInt("entidad_id");
		            filterUsuario.put("entidad_id", idEntidad);
	        	}
	        	// Si el usuario no esta asociado a ninguna Entidad Certificadora entonces pertenece a MINCETUR
	        	else {
		            // Obtener el ID de la Entidad en base al CODIGO DE ENTIDAD capturado desde el logueo
		            Integer idEntidad = ibatisService.loadElement("usuario.claveEntidadDesdeCodigoSUNAT.MINCETUR", filter).getInt("entidad_id");
		            filterUsuario.put("entidad_id", idEntidad);
	        	}
	            filterUsuario.put("area", usuario.getArea());
	            filterUsuario.put("subArea", usuario.getSubArea());
	        	//throw new Exception("ERROR: No se pueden registrar Usuarios Extranet");
	        }
	        
	        // Datos de la Empresa
	        if (empresa!= null) {
	            filterEmpresa.put("numero_documento", empresa.getNumeroDocumento());
	            filterEmpresa.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_RUC);
	            filterEmpresa.put("nombre", empresa.getNombre());
	            filterEmpresa.put("email", empresa.getEmail());
	            filterEmpresa.put("telefono", empresa.getTelefono());
	            filterEmpresa.put("fax", empresa.getFax());
	            filterEmpresa.put("direccion", empresa.getDireccion());
	            filterEmpresa.put("distrito_id", empresa.getDistritoId());
	        }
	        
	        int idUsuario = -1;
	        
            // Si se consignaron datos de la Empresa, se graban
            if (empresa != null){
                int idEmpresa = usuariosService.insertEmpresa(filterEmpresa, representantesLegales);
                filterUsuario.put("empresa_id", idEmpresa);
            } else {
                filterUsuario.put("empresa_id", null);
            }
            idUsuario = usuariosService.insertUsuario(filterUsuario);
            
            // Ahora registramos los roles del usuario
            HashUtil nuevosRoles = usuariosService.actualizarRolesUsuario(idUsuario, usuarioCO.getRoles());
            usuarioCO.setRoles(nuevosRoles);
            
            message = new Message("success.crear.usuario");
            
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.crearUsuario", e);
            message = new ErrorMessage("error.crear.usuario", e);
        }

        messageList.add(message);
        
        return messageList;
    }
    
    public MessageList actualizarUsuario(Usuario usuario, Usuario empresa , Table representantesLegales, UsuarioCO usuarioCO) {
        HashUtil<String, Object> filterUsuario = new HashUtil<String, Object>();
        HashUtil<String, Object> filterEmpresa = new HashUtil<String, Object>();
        
        MessageList messageList = new MessageList();
        // Datos del Usuario
        filterUsuario.put("documento_tipo", usuario.getTipoDocumento());
        filterUsuario.put("numero_documento", usuario.getNumeroDocumento());
        filterUsuario.put("usuario_sol", usuario.getUsuarioSol());
        filterUsuario.put("nombre", usuario.getNombre());
        filterUsuario.put("email", usuario.getEmail());
        filterUsuario.put("recibeNotificacion", usuario.getRecibeNotificacion());
        filterUsuario.put("telefono", usuario.getTelefono());
        filterUsuario.put("celular", usuario.getCelular());
        filterUsuario.put("fax", usuario.getFax());
        filterUsuario.put("direccion", usuario.getDireccion());
        filterUsuario.put("referencia", usuario.getReferencia());
        filterUsuario.put("distrito_id", usuario.getDistritoId());
        filterUsuario.put("usuario_tipo_id", usuario.getUsuarioTipoId());
        filterUsuario.put("principal", usuario.getPrincipal());
        filterUsuario.put("persona_tipo", usuario.getTipoPersona());
        filterUsuario.put("usuario_id", usuario.getUsuarioId());
        filterUsuario.put("cargo", usuario.getCargo());
        filterUsuario.put("documento_tipo_us", usuario.getTipoDocumentoUS());
        filterUsuario.put("numero_documento_us", usuario.getNumeroDocumentoUS());
        filterUsuario.put("ingreso_a_mr", usuarioCO.getIngresoMR());
        filterUsuario.put("ingreso_a_uo", "S"); // Es "S" porque estamos en Componente Origen
        
        // Si es un usuario EXTRANET, tiene un id de Entidad
        if (usuario.getTipoDocumento().equals(ConstantesCO.TIPO_DOCUMENTO_EXTRANET) && usuario.getCodigoEntidadSunat()!=null) {
            // Obtener el ID de la Entidad en base al ID capturado desde el logueo
            /*HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("codigoEntidad", usuario.getCodigoEntidadSunat());
            Integer idEntidad = ibatisService.loadElement("usuario.claveEntidadDesdeCodigoSUNAT", filter).getInt("entidad_id");
            filterUsuario.put("entidad_id", idEntidad);*/
            filterUsuario.put("area", usuario.getArea());
            filterUsuario.put("subArea", usuario.getSubArea());
        }
        
        // Datos de la Empresa
        if (empresa!= null) {
            filterEmpresa.put("numero_documento", empresa.getNumeroDocumento());
            filterEmpresa.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_RUC);
            filterEmpresa.put("nombre", empresa.getNombre());
            filterEmpresa.put("email", empresa.getEmail());
            filterEmpresa.put("telefono", empresa.getTelefono());
            filterEmpresa.put("fax", empresa.getFax());
            filterEmpresa.put("direccion", empresa.getDireccion());
            filterEmpresa.put("distrito_id", empresa.getDistritoId());
            filterEmpresa.put("empresa_id", empresa.getUsuarioId());
        }
        
        Message message = null;
        try {
            if (empresa != null){
                usuariosService.updateEmpresa(filterEmpresa, representantesLegales);
            }
            usuariosService.updateUsuario(filterUsuario);
            
            message = new Message("success.actualizar.usuario");
            
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.actualizarUsuario", e);
            message = new ErrorMessage("error.actualizar.usuario", e);
        }
        messageList.add(message);
        
        return messageList;
    }
    
    public Table obtenerTablaRepresentantesLegalesInicial(String numeroDocumento) {
        Table tablaRepresentantesLegales = new Table();
        tablaRepresentantesLegales.setName("REPRESENTANTE_LEGAL");
        
        BeanRso [] representantesLegales = ConsultaRUCCWS.getRepLegales(numeroDocumento);
        HashUtil<String, Object> keysTabla = new HashUtil<String, Object>();
        int rowIndex = 1;
        for (int i=0; representantesLegales!=null && i < representantesLegales.length; i++) {
            BeanRso representanteLegal = representantesLegales[i];
            if (representanteLegal!=null) {
                String nroDoc = representanteLegal.getRso_nrodoc();
                String nombreRepresentanteLegal = representanteLegal.getRso_nombre();
                if (nroDoc!=null) nroDoc = nroDoc.trim();
                if (nombreRepresentanteLegal!=null) nombreRepresentanteLegal = nombreRepresentanteLegal.trim();
                
                String keyTabla = "";
                keyTabla = representanteLegal.getRso_docide()+"|"+nroDoc;
                if (!keysTabla.containsKey(keyTabla)) {
                	keysTabla.put(keyTabla, keyTabla);
                    
                    Row row = new Row();
                    row.setTable(tablaRepresentantesLegales);
                    row.setRowNumber(rowIndex++);
                    
                    Cell cell = new Cell();
                    cell.setColumnName("SELECCIONE");
                    cell.setColumnNumber(1);
                    cell.setTableName(tablaRepresentantesLegales.getName());
                    cell.setValue("S");
                    row.addCell(cell);
                    
                    cell = new Cell();
                    cell.setColumnName("DOCUMENTO_TIPO");
                    cell.setColumnNumber(2);
                    cell.setTableName(tablaRepresentantesLegales.getName());
                    cell.setValue(representanteLegal.getRso_docide());
                    row.addCell(cell);
                    
                    cell = new Cell();
                    cell.setColumnName("DESCRIPCION");
                    cell.setColumnNumber(3);
                    cell.setTableName(tablaRepresentantesLegales.getName());
                    TipoDocumentoType tipoDocumento = TipoDocumentoType.getTipoDocumentoTypeByCodigoSunat(representanteLegal.getRso_docide());
                    if (tipoDocumento!=null) {
                        cell.setValue(tipoDocumento.getDescripcion());
                    } else {
                        cell.setValue("--");
                    }
                    row.addCell(cell);
                    
                    cell = new Cell();
                    cell.setColumnName("NUMERO_DOCUMENTO");
                    cell.setColumnNumber(4);
                    cell.setTableName(tablaRepresentantesLegales.getName());
                    cell.setValue(nroDoc);
                    row.addCell(cell);
                    
                    cell = new Cell();
                    cell.setColumnName("NOMBRE");
                    cell.setColumnNumber(5);
                    cell.setTableName(tablaRepresentantesLegales.getName());
                    cell.setValue(nombreRepresentanteLegal);
                    row.addCell(cell);
                    
                    tablaRepresentantesLegales.addRow(row);
                    /*System.out.print("::::: Tipo Doc: "+representanteLegal.getRso_docide()+", ");
                    System.out.print("Nro. Doc: "+nroDoc+", ");
                    System.out.println("Nombre: "+representanteLegal.getRso_nombre());*/
                }
            }
        }
        tablaRepresentantesLegales.setOriginalSize(tablaRepresentantesLegales.size());
        return tablaRepresentantesLegales;
    }
    
    public Usuario getUsuarioById(Integer usuarioId) {
    	Usuario usuario = null;
    	try {
    	    usuario = usuariosService.loadUsuarioById(usuarioId);
    	} catch(Exception e) {
    		logger.error("ERROR en UsuariosLogicImpl.getUsuarioById", e);
    	}
    	return usuario;
    }
    
    public void actualizarDatosRUCUsuario(UsuarioCO usuarioCO, HashUtil<String, Object> datosFichaRUC) {
        try {
	    	if (usuarioCO.getTipoPersona().equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_NATURAL)) {
	        	Usuario usuario = usuariosService.loadUsuarioById(usuarioCO.getIdUsuario());
	        	
	            usuario.setNombre(datosFichaRUC.getString("USUARIO.nombre"));
	            usuario.setDireccion(datosFichaRUC.getString("USUARIO.direccion"));
	            usuario.setTelefono(datosFichaRUC.getString("USUARIO.telefono"));
	            usuario.setFax(datosFichaRUC.getString("USUARIO.fax"));
	            usuario.setEmail(datosFichaRUC.getString("USUARIO.email"));
	            usuario.setDistritoId(Integer.parseInt(datosFichaRUC.getString("id_distrito")));
	            
		    	HashUtil<String, Object> filterUsuario = new HashUtil<String, Object>();
		        
		        // Datos del Usuario
		        filterUsuario.put("documento_tipo", usuario.getTipoDocumento());
		        filterUsuario.put("numero_documento", usuario.getNumeroDocumento());
		        filterUsuario.put("usuario_sol", usuario.getUsuarioSol());
		        filterUsuario.put("nombre", usuario.getNombre());
		        filterUsuario.put("email", usuario.getEmail());
		        filterUsuario.put("recibeNotificacion", usuario.getRecibeNotificacion());
		        filterUsuario.put("telefono", usuario.getTelefono());
		        filterUsuario.put("celular", usuario.getCelular());
		        filterUsuario.put("fax", usuario.getFax());
		        filterUsuario.put("direccion", usuario.getDireccion());
		        filterUsuario.put("referencia", usuario.getReferencia());
		        filterUsuario.put("distrito_id", usuario.getDistritoId());
		        filterUsuario.put("usuario_tipo_id", usuario.getUsuarioTipoId());
		        filterUsuario.put("principal", usuario.getPrincipal());
		        filterUsuario.put("persona_tipo", usuario.getTipoPersona());
		        filterUsuario.put("usuario_id", usuario.getUsuarioId());
	            filterUsuario.put("empresa_externa_id", null);
	            filterUsuario.put("cargo", usuario.getCargo());
	            filterUsuario.put("documento_tipo_us", usuario.getTipoDocumentoUS());
	            filterUsuario.put("numero_documento_us", usuario.getNumeroDocumentoUS());
	            filterUsuario.put("documento_tipo_us", usuario.getTipoDocumentoUS());
	            filterUsuario.put("numero_documento_us", usuario.getNumeroDocumentoUS());
	            filterUsuario.put("ingreso_a_mr", usuarioCO.getIngresoMR());
	            filterUsuario.put("ingreso_a_uo", "S"); // Es "S" porque estamos en Componente Origen
	            
	            usuariosService.updateUsuario(filterUsuario);
	    	} else if (usuarioCO.getTipoPersona().equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA)) {
	        	Usuario empresa = usuariosService.loadEmpresaById(usuarioCO.getIdUsuario());
	        	
	        	empresa.setNombre(datosFichaRUC.getString("EMPRESA.nombre"));
	        	empresa.setDireccion(datosFichaRUC.getString("EMPRESA.direccion"));
	            empresa.setTelefono(datosFichaRUC.getString("EMPRESA.telefono"));
	            empresa.setFax(datosFichaRUC.getString("EMPRESA.fax"));
	            empresa.setEmail(datosFichaRUC.getString("EMPRESA.email"));
	            empresa.setDistritoId(Integer.parseInt(datosFichaRUC.getString("id_distritoE")));
	    		
	            HashUtil<String, Object> filterEmpresa = new HashUtil<String, Object>();
		        
	            filterEmpresa.put("numero_documento", empresa.getNumeroDocumento());
	            filterEmpresa.put("documento_tipo", ConstantesCO.TIPO_DOCUMENTO_RUC);
	            filterEmpresa.put("nombre", empresa.getNombre());
	            filterEmpresa.put("email", empresa.getEmail());
	            filterEmpresa.put("telefono", empresa.getTelefono());
	            filterEmpresa.put("fax", empresa.getFax());
	            filterEmpresa.put("direccion", empresa.getDireccion());
	            filterEmpresa.put("distrito_id", empresa.getDistritoId());
	            filterEmpresa.put("empresa_id", empresa.getUsuarioId());
	            
	            usuariosService.updateEmpresa(filterEmpresa);
	    	}
	    	
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.actualizarUsuario", e);
        }
    }
    
    public MessageList actualizarRepresentantesLegalesEmpresa(Usuario empresa, Table representantesLegales) {
    	MessageList messageList = new MessageList();
        Message message = null;
        try {
        	
        	usuariosService.updateRepresentantesLegalesEmpresa(empresa.getUsuarioId(), representantesLegales);
        	
        	message = new Message("success.actualizar.representantesLegales");
        } catch (Exception e) {
            logger.error("Error en UsuariosLogicImpl.actualizarRepresentantesLegalesEmpresa", e);
            message = new ErrorMessage("error.actualizar.representantesLegales", e);
        }
        messageList.add(message);
        
        return messageList;
    }
    
    public Usuario getUsuarioEmpresaById(int idUsuario) {
    	Usuario usuario = null;
		try {
			usuario = usuariosService.loadEmpresaById(idUsuario);
		} catch (Exception e) {
			logger.error("ERROR: Al intentar cargar el usuario", e);
		}
    	return usuario;
    }
    
    public UsuarioCO getUsuarioCOById(int idUsuario) {
    	UsuarioCO usuario = null;
		try {
			usuario = usuariosService.loadUsuarioCOById(idUsuario);
		} catch (Exception e) {
			logger.error("ERROR: Al intentar cargar el usuario", e);
		}
    	return usuario;
    }
    
    public HashUtil<String, Object> obtenerDatosFichaRUC(UsuarioCO usuario) {
        HashUtil<String, Object> datosFichaRUC = new HashUtil<String, Object>();
        
        /*
        if (!ConsultaRUCCWS.servicioDisponible()) {
        	return datosFichaRUC;
        }
        */
        
        // OJO: Obtener de la Consulta RUC para obtener los datos del RUC. Nos importan:
        // - Tipo Contribuyente: Persona Natural o Persona Juridica para saber si pedir "Datos de Empresa" y establecer "USUARIO.nombre" o "EMPRESA.nombre"
        // - Demas datos del RUC: Direccion, Telefono, Email, Fax
        String numeroDocumento = usuario.getNumeroDocumento();
        BeanDdp datosRUC = ConsultaRUCCWS.getDatosPrincipales(numeroDocumento);
        String domicilioLegal = ConsultaRUCCWS.getDomicilioLegal(numeroDocumento);
        BeanDds datosSecundariosRUC = ConsultaRUCCWS.getDatosSecundarios(numeroDocumento);
        BeanT1144 datosT1144 = ConsultaRUCCWS.getDatosT1144(numeroDocumento);
        
        if (datosRUC!=null && domicilioLegal!=null && datosSecundariosRUC!=null && datosT1144!=null &&
            datosRUC.getDdp_numruc()!=null && datosRUC.getDdp_identi()!=null) {
            String tipoPersona = Integer.valueOf(datosRUC.getDdp_identi()).toString();
            usuario.setTipoPersona(tipoPersona);
            if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_NATURAL)) {
                datosFichaRUC.put("USUARIO.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("USUARIO.direccion", domicilioLegal.trim());
                datosFichaRUC.put("USUARIO.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("USUARIO.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("USUARIO.email", datosT1144.getCod_correo1().trim());
                
                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamento", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provincia", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distrito", datosUbigeo.get("DISTRITOID"));
                
                datosFichaRUC.put("ubigeoUsuarioEditable", "no");
                
            } else if (tipoPersona.equalsIgnoreCase(ConstantesCO.TIPO_PERSONA_JURIDICA)) {
                datosFichaRUC.put("EMPRESA.nombre", datosRUC.getDdp_nombre().trim());
                datosFichaRUC.put("EMPRESA.direccion", domicilioLegal.trim());
                datosFichaRUC.put("EMPRESA.telefono", datosSecundariosRUC.getDds_telef1().trim());
                datosFichaRUC.put("EMPRESA.fax", datosSecundariosRUC.getDds_numfax().trim());
                datosFichaRUC.put("EMPRESA.email", datosT1144.getCod_correo1().trim());
                
                HashMap<String, String> datosUbigeo = formatoLogic.getUbigeoDetail(datosRUC.getDdp_ubigeo());
                datosFichaRUC.put("id_departamentoE", datosUbigeo.get("DEPARTAMENTOID"));
                datosFichaRUC.put("id_provinciaE", datosUbigeo.get("PROVINCIAID"));
                datosFichaRUC.put("id_distritoE", datosUbigeo.get("DISTRITOID"));
                
                // Obtenemos los Representantes Legales de la Empresa
                Table tablaRepresentantesLegales = this.obtenerTablaRepresentantesLegalesInicial(numeroDocumento);
                datosFichaRUC.put("tablaRepresentantesLegales", tablaRepresentantesLegales);
                
                datosFichaRUC.put("ubigeoUsuarioEditable", "yes");
            }
        }
        return datosFichaRUC;
    }
    
}
