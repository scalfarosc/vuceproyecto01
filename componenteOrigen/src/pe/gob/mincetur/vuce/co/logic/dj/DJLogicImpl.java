package pe.gob.mincetur.vuce.co.logic.dj;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.dj.AdjuntoFormatoDJ;
import pe.gob.mincetur.vuce.co.domain.dj.AdjuntoRequeridoDJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJExportador;
import pe.gob.mincetur.vuce.co.domain.dj.DJMaterial;
import pe.gob.mincetur.vuce.co.domain.dj.DJProductor;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.service.dj.DJService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;



public class DJLogicImpl implements DJLogic{

	private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

	private DJService djService = null;

	private AdjuntoService adjuntoService;

	public DJService getDjService() {
		return djService;
	}

	public void setAdjuntoService(AdjuntoService adjuntoService) {
		this.adjuntoService = adjuntoService;
	}

	public void setDjService(DJService djService) {
		this.djService = djService;
	}

	public MessageList creaDJ(Long usuario, Long solicitante, Long representante, String cargo) {
        HashUtil<String, Object> filterDJ = new HashUtil<String, Object>();
        HashUtil<String, Object> filterUsuario = new HashUtil<String, Object>();
        HashUtil<String, Object> filterSolicitante = new HashUtil<String, Object>();
		HashUtil<String, Object> filterRepresentante = new HashUtil<String, Object>();
        HashUtil<String, Object> djDet = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;

        filterDJ.put("usuarioId", usuario);
        filterDJ.put("ordenId", null);
        filterDJ.put("mto", null);
        filterDJ.put("djId", null);

        try {
            djDet = djService.insertDJ(filterDJ);
		    djDet.imprimir();

	        long ordenId = djDet.getLong("ordenId");
	        int mto = djDet.getInt("mto");
	        long djId =  djDet.getLong("djId");

	        // inserto usuario
	        filterUsuario.put("ordenId", ordenId);
	        filterUsuario.put("mto", mto);
	        filterUsuario.put("usuarioId", usuario);
	        filterUsuario.put("usuarioFormatoTipo", ConstantesCO.USUARIO_USUARIO);
	        filterUsuario.put("representanteId",null);
	        filterUsuario.put("cargo", cargo);
	        djService.insertUsuarioDJ(filterUsuario);

	        // inserto solicitante
	        filterSolicitante.put("ordenId", ordenId);
	        filterSolicitante.put("mto", mto);
	        filterSolicitante.put("usuarioId", solicitante);
	        filterSolicitante.put("usuarioFormatoTipo", ConstantesCO.USUARIO_SOLICITANTE);
	        filterSolicitante.put("representanteId",null);
	        filterUsuario.put("cargo", null);
	        djService.insertUsuarioDJ(filterSolicitante);

	        if (representante != 0){
	            // inserto representante
	            filterRepresentante.put("ordenId", ordenId);
	            filterRepresentante.put("mto", mto);
	            filterRepresentante.put("usuarioId", solicitante);
	            filterRepresentante.put("usuarioFormatoTipo", ConstantesCO.USUARIO_REPRESENTANTE);
	            filterRepresentante.put("representanteId",representante);
	            filterUsuario.put("cargo", null);
	            djService.insertUsuarioDJ(filterRepresentante);
	        }

            DJ djObj = getDJById(ordenId, mto);
            messageList.setObject(djObj);
            message = new Message("insert.success");

        } catch (Exception e) {
            logger.error("Error al crear la Orden", e);
            message = new ErrorMessage("insert.error", e);
        }

        messageList.add(message);

        return messageList;
    }

	public MessageList transmiteDJ(long ordenId, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;

        filter.put("ordenId", ordenId);
        filter.put("mto", mto);

        try {
			djService.transmiteDJ(filter);
			message = new Message("transmite.success");
		} catch (Exception e) {
			logger.error("Error al transmitir la Solicitud", e);
            message = new ErrorMessage("transmite.error", e);
		}

		DJ djObj = getDJById(ordenId, mto);
        messageList.setObject(djObj);

        messageList.add(message);

        return messageList;
    }

	public DJ convertMapToDJ(Map<String, Object> map) {
		DJ obj = new DJ();
		obj.setDjId(Util.longValueOf(map.get("djId")));
		obj.setOrdenId(Util.longValueOf(map.get("ordenId")));
		obj.setMto(Util.integerValueOf(map.get("mto")));
		obj.setDenominacion(map.get("denominacion")!=null ? map.get("denominacion").toString() : null);
		obj.setCaracteristica(map.get("caracteristica")!=null ? map.get("caracteristica").toString() : null);
		obj.setPartidaArancelaria(Util.longValueOf(map.get("partidaArancelaria")));
		obj.setUmFisicaId(Util.integerValueOf(map.get("umFisicaId")));
		obj.setPlantaDireccion(map.get("plantaDireccion")!=null ? map.get("plantaDireccion").toString() : null);
		obj.setPlantaDistritoId(Util.integerValueOf(map.get("plantaDistritoId")));
		obj.setDemasGasto(Util.bigDecimalValueOf(map.get("demasGasto")));
		obj.setTipoCambio(Util.bigDecimalValueOf(map.get("tipoCambio")));
		obj.setEsAcopiador(map.get("esAcopiador")!=null ? map.get("esAcopiador").toString() : null);
		obj.setEsExportador(map.get("esExportador")!=null ? map.get("esExportador").toString() : "N");
		obj.setTipoUsuarioDj(Util.integerValueOf(map.get("tipoUsuarioDj")));
		obj.setValorFOBTotalMaterial(Util.bigDecimalValueOf(map.get("valorFOBTotalMaterial")));
		obj.setValorExworkTotalMaterial(Util.bigDecimalValueOf(map.get("valorExworkTotalMaterial")));
		obj.setValorFOBUnitarioTotal(Util.bigDecimalValueOf(map.get("valorFOBUnitarioTotal")));
		obj.setValorExworkUnitarioTotal(Util.bigDecimalValueOf(map.get("valorExworkUnitarioTotal")));
		return obj;
	}

	public MessageList updateDJ(DJ dj) throws Exception {
		MessageList messageList = new MessageList();
		Message message = null;
		try {
			djService.updateDJ(dj);
			dj = actualizarTotalesValoresFobExWork(dj);
			message = new Message("update.success");
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.updateDGM001", e);
			message = new ErrorMessage("update.error", e);
			//message = VuceUtil.getErrorMessage(e);
		} finally {
			messageList.add(message);
		}
		return messageList;
	}

	private DJ actualizarTotalesValoresFobExWork(DJ dj) {
		BigDecimal totalMaterial = null;
		BigDecimal demasGasto = null;
		BigDecimal totalUnitario = null;
		// Tenemos que verificar la consistencia de los valores ingresados, valortotalmaterial + demasgasto = valorunitariototal, sea fob o exwork
		// Primero el fob
		totalMaterial = dj.getValorFOBTotalMaterial();
		if (totalMaterial != null) {
			demasGasto = dj.getDemasGasto();
			if (demasGasto != null) {
				totalUnitario = totalMaterial.add(demasGasto);
			} else {
				totalUnitario = totalMaterial;
			}
		} else { // Si es nulo el totalMaterial, el demasGasto deberia ser la suma
			demasGasto = dj.getDemasGasto();
			if (demasGasto != null) {
				totalUnitario = demasGasto;
			} else {
				totalUnitario = new BigDecimal(0.0);
			}
		}
		dj.setValorFOBUnitarioTotal(totalUnitario);

		// Ahora el ExWork
		totalMaterial = dj.getValorExworkTotalMaterial();
		if (totalMaterial != null) {
			demasGasto = dj.getDemasGasto();
			if (demasGasto != null) {
				totalUnitario = totalMaterial.add(demasGasto);
			} else {
				totalUnitario = totalMaterial;
			}
		} else { // Si es nulo el totalMaterial, el demasGasto deberia ser la suma
			demasGasto = dj.getDemasGasto();
			if (demasGasto != null) {
				totalUnitario = demasGasto;
			} else {
				totalUnitario = new BigDecimal(0.0);
			}
		}
		dj.setValorExworkUnitarioTotal(totalUnitario);

		return dj;
	}

	public DJ getDJById(long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		DJ obj = null;
		try {
			obj = djService.getDJById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getDJById", e);
		}
		return obj;
	}

	public DJMaterial getDJMaterialById(long djId, Integer secuenciaMaterial) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("djId", djId);
		filter.put("secuenciaMaterial", secuenciaMaterial);
		DJMaterial obj = null;
		try {
			obj = djService.getDJMaterialById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getDJMaterialById", e);
		}
		return obj;
	}

	public DJProductor getDJProductorById(long djId, Integer secuenciaProductor) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("djId", djId);
		filter.put("secuenciaProductor", secuenciaProductor);
		DJProductor obj = null;
		try {
			obj = djService.getDJProductorById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getDJProductorById", e);
		}
		return obj;
	}

	public DJExportador getDJExportadorById(long djId, Integer secuenciaExportador) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("djId", djId);
		filter.put("secuenciaExportador", secuenciaExportador);
		DJExportador obj = null;
		try {
			obj = djService.getDJExportadorById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getDJExportadorById", e);
		}
		return obj;
	}

	public UsuarioFormato getUsuarioFormatoById(long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		UsuarioFormato obj = null;
		try {
			obj = djService.getUsuarioFormatoById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getUsuarioFormatoById", e);
		}
		return obj;
	}

	public Solicitud getSolicitudById(long ordenId, Integer mto) {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("ordenId", ordenId);
		filter.put("mto", mto);
		Solicitud obj = null;
		try {
			obj = djService.getSolicitudById(filter);
		} catch (Exception e) {
			logger.error("Error en DJLogicImpl.getSolicitudById", e);
		}
		return obj;
	}

    public Solicitante getSolicitanteDetail(long ordenId, int mto, int usuarioFormatoTipo) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        filter.put("usuarioFormatoTipo", usuarioFormatoTipo);
        Solicitante solicitante = null;
		try {
			solicitante = djService.getSolicitanteDetail(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return solicitante;
    }

	public DJMaterial convertMapToDJMaterial(Map<String, Object> map){
		DJMaterial obj = new DJMaterial();
		obj.setDjId(Util.longValueOf(map.get("djId")));
		obj.setSecuenciaMaterial(Util.integerValueOf(map.get("secuenciaMaterial")));
		obj.setItem(Util.integerValueOf(map.get("item")));
		obj.setDescripcion(map.get("descripcion").toString());
		obj.setFabricanteNombre(map.get("fabricanteNombre").toString());
		obj.setFabricanteTipoDocumento(Util.integerValueOf(map.get("fabricanteTipoDocumento")));
		obj.setFabricanteNumeroDocumento(map.get("fabricanteNumeroDocumento").toString());
		obj.setCantidad(Util.bigDecimalValueOf(map.get("cantidad")));
		obj.setValorFobUS(Util.bigDecimalValueOf(map.get("valorFobUS")));
		obj.setValorExWorkUS(Util.bigDecimalValueOf(map.get("valorExWorkUS")));
		obj.setPartidaArancelaria(Util.longValueOf(map.get("partidaArancelaria")));
		obj.setPaisProcedencia(Util.integerValueOf(map.get("paisProcedencia")));
		obj.setPaisOrigen(Util.integerValueOf(map.get("paisOrigen")));
		obj.setUmFisicaId(Util.integerValueOf(map.get("umFisicaId")));
        return obj;
	}

	public MessageList insertDJMaterial (DJMaterial obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Integer secuencia;
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            secuencia = djService.insertMaterial(obj);
            obj = getDJMaterialById(obj.getDjId(), obj.getSecuenciaMaterial());
            message = new Message("insert.success");
            messageList.setObject(obj);
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.insertMaterial", e);
            message = new ErrorMessage("insert.error",e);
			//message = VuceUtil.getErrorMessage(e);
        }
        messageList.add(message);
        return messageList;
	}
	public MessageList updateDJMaterial (DJMaterial obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        try {
        	djService.updateMaterial(obj);
            message = new Message("update.success");
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.updateMaterial", e);
            message = new ErrorMessage("update.error",e);
            //message = VuceUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
	}
	public MessageList deleteMaterial(DJMaterial obj){
		MessageList messageList = new MessageList();
		Message message = null;
        try {
        	djService.deleteMaterial(obj);
	        message = new Message("delete.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en DJLogicImpl.deleteMaterial", e);
	        message = new ErrorMessage("delete.error", e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
	}

	public DJProductor convertMapToDJProductor(Map<String, Object> map){
		DJProductor obj = new DJProductor();
		obj.setDjId(Util.longValueOf(map.get("djId")));
		obj.setSecuenciaProductor(Util.integerValueOf(map.get("secuenciaProductor")));
		obj.setNumeroDocumento(map.get("numeroDocumento").toString());
		obj.setNombre(map.get("nombre").toString());
		obj.setLugarProduccion(map.get("lugarProduccion").toString());

        return obj;
	}

	public MessageList insertDJProductor (DJProductor obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Integer secuencia;
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            secuencia = djService.insertProductor(obj);
            obj.setSecuenciaProductor(secuencia);
            message = new Message("insert.success");
            messageList.setObject(obj);
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.insertProductor", e);
            message = new ErrorMessage("insert.error",e);
			//message = VuceUtil.getErrorMessage(e);
        }
        messageList.add(message);
        return messageList;
	}
	public MessageList updateDJProductor (DJProductor obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        try {
        	djService.updateProductor(obj);
            message = new Message("update.success");
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.updateProductor", e);
			message = new ErrorMessage("update.error",e);
            //message = VuceUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
	}
	public MessageList deleteProductor(DJProductor obj){
		MessageList messageList = new MessageList();
		Message message = null;
        try {
        	djService.deleteProductor(obj);
	        message = new Message("delete.success");
	    }
	    catch (Exception e) {
	        logger.error("Error en DJLogicImpl.deleteProductor", e);
	        message = new ErrorMessage("delete.error", e);
	    }
	    finally {
	    	messageList.add(message);
	    }
	    return messageList;
	}

	public DJExportador convertMapToDJExportador(Map<String, Object> map){
		DJExportador obj = new DJExportador();
		obj.setDjId(Util.longValueOf(map.get("djId")));
		obj.setSecuenciaExportador(Util.integerValueOf(map.get("secuenciaExportador")));
		obj.setDocumentoTipo(Util.integerValueOf(map.get("documentoTipo")));
		obj.setNombre(map.get("nombre").toString());
		obj.setNumeroDocumento(map.get("numeroDocumento").toString());

        return obj;
	}

	public MessageList insertDJExportador (DJExportador obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Integer secuencia;
        MessageList messageList = new MessageList();
        Message message = null;
        try {
            secuencia = djService.insertExportador(obj);
            obj.setSecuenciaExportador(secuencia);

            message = new Message("insert.success");

        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.insertExportador", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
            obj.setDocumentoTipo(null);
            obj.setNumeroDocumento(null);
            obj.setNombre(null);
        } finally {
        	messageList.setObject(obj);
        }

        messageList.add(message);
        return messageList;
	}
	public MessageList updateDJExportador (DJExportador obj){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        MessageList messageList = new MessageList();
        Message message = null;
        try {
        	djService.updateExportador(obj);
            message = new Message("update.success");
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.updateExportador", e);
			message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
	}
	public MessageList deleteExportador(DJExportador obj){
		MessageList messageList = new MessageList();
		Message message = null;
        try {
        	djService.deleteExportador(obj);
	        message = new Message("delete.success");
	    } catch (Exception e) {
	        logger.error("Error en DJLogicImpl.deleteExportador", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    } finally {
	    	messageList.add(message);
	    }
	    return messageList;
	}

	public MessageList actualizarSeleccionEsExportador(DJ obj) {
		MessageList messageList = new MessageList();
		Message message = null;
        try {
    		HashUtil<String, Object> filter = new HashUtil<String, Object>();
    		filter.put("djId", obj.getDjId());
    		filter.put("esExportador", obj.getEsExportador());

        	djService.actualizarSeleccionEsExportador(filter);

	        message = new Message("update.success");
	    } catch (Exception e) {
	        logger.error("Error en DJLogicImpl.actualizarSeleccionEsExportador", e);
	        message = ComponenteOrigenUtil.getErrorMessage(e);
	    } finally {
	    	messageList.add(message);
	    }
	    return messageList;
	}

    public AdjuntoRequeridoDJ getAdjuntoRequeridoById(Integer adjuntoRequeridoDJ) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("adjuntoRequeridoDJ", adjuntoRequeridoDJ);
        String titulo = null;
        AdjuntoRequeridoDJ adjunto = null;
        try {
        	adjunto = djService.getAdjuntoRequeridoById(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return adjunto;
    }

    public MessageList uploadFile(Long ordenId, Integer mto, Integer adjuntoRequerido, String nombre, byte [] bytes) {
        MessageList messageList = new MessageList();

        AdjuntoFormatoDJ archivoAdjunto = new AdjuntoFormatoDJ();
        //archivoAdjunto.setOrdenId(ordenId);
        archivoAdjunto.setMto(mto);
        archivoAdjunto.setAdjuntoRequerido(adjuntoRequerido);
        archivoAdjunto.setNombre("(Req-"+adjuntoRequerido+") "+ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
            adjuntoService.insertAdjunto(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en DJLogicImpl.uploadFile", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

	 public MessageList updateRepresentante(long solicitante,long ordenId, int mto, long representante) {
	        if (representante== 0) return null;

	        HashUtil<String, Object> filter = new HashUtil<String, Object>();
	        MessageList messageList = new MessageList();
	        filter.put("solicitante", solicitante);
	        filter.put("ordenId", ordenId);
	        filter.put("mto", mto);
	        filter.put("representante", representante);

	        Message message = null;
	        try {
	            djService.updateRepresentante(filter);

	            DJ djObj = getDJById(ordenId, mto);

	            messageList.setObject(djObj);
	            message = new Message("update.representante.success");

	        } catch (Exception e) {
	            logger.error("Error al actualizar al representante legal", e);
	            message = new ErrorMessage("update.representante.error", e);

	        }

	        messageList.add(message);
	        return messageList;
	    }


	 public MessageList updateCargoDeclarante(long solicitante,long ordenId, int mto, String cargo) {
         HashUtil<String, Object> filter = new HashUtil<String, Object>();
         MessageList messageList = new MessageList();
         filter.put("solicitante", solicitante);
         filter.put("ordenId", ordenId);
         filter.put("mto", mto);
         filter.put("cargo", cargo);

         Message message = null;
         try {
            djService.updateCargoDeclarante(filter);

            DJ djObj = getDJById(ordenId, mto);

            messageList.setObject(djObj);
            message = new Message("update.cargoDeclarante.success");

         } catch (Exception e) {
            logger.error("Error al actualizar al Cargo del Declarante", e);
            message = new ErrorMessage("update.cargoDeclarante.error", e);
         }

         messageList.add(message);
         return messageList;
    }

	 public Integer getAdjuntoRequeridoCount(long ordenId,int mto){
		 HashUtil<String, Object> filter = new HashUtil<String, Object>();
         MessageList messageList = new MessageList();
         filter.put("ordenId", ordenId);
         filter.put("mto", mto);
         Integer cuenta = 0;

         try {
        	 cuenta = djService.getAdjuntoRequeridoCount(filter);
		} catch (Exception e) {
			logger.error("Error al consultar el numero de adjuntos requeridos obligatorios", e);
		}
		 return cuenta;
	 }

	 public void modificarRol(long djId, int tipoUsuarioDj) {
		 try {
        	 djService.modificarRol(djId, tipoUsuarioDj);
		 } catch (Exception e) {
			logger.error("Error al modificar el rol del usuario en la ddjj", e);
		 }
		 return;
	 }

}
