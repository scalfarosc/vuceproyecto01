package pe.gob.mincetur.vuce.co.logic;

import org.jlis.core.list.MessageList;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.Mensaje;

public interface BuzonLogic {
	
	public Mensaje getMessageById(int idMensaje);
	
	public MessageList getMessageById(UsuarioCO usuario, int idMensaje ,int  destinatario);
	
	public Adjunto descargarAdjuntoById(int idAdjunto);
	
	public MessageList inactivaMensaje(int idMensaje , int destinatario);

}
