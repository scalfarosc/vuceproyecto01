package pe.gob.mincetur.vuce.co.logic;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;
import org.jlis.core.bean.ErrorMessage;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.service.ibatis.IbatisService;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.dao.DBSessionContext;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.AdjuntoRequerido;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.procesodr.domain.AdjuntoMensaje;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.service.FormatoService;
import pe.gob.mincetur.vuce.co.service.OrdenService;
import pe.gob.mincetur.vuce.co.service.UsuariosService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;


public class FormatoLogicImpl implements FormatoLogic {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);

    protected OrdenService ordenService;

	protected FormatoService formatoService;

    protected AdjuntoService adjuntoService;

    protected IbatisService ibatisService;

    private AdjuntoLogic adjuntoLogic;

    private UsuariosService usuariosService;

    private DBSessionContext dbSessionContext;
    
    

    public void setOrdenService(OrdenService ordenService) {
		this.ordenService = ordenService;
	}

	public void setFormatoService(FormatoService formatoService) {
		this.formatoService = formatoService;
	}

	public void setAdjuntoService(AdjuntoService adjuntoService) {
        this.adjuntoService = adjuntoService;
    }

    public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}

	public void setAdjuntoLogic(AdjuntoLogic adjuntoLogic) {
		this.adjuntoLogic = adjuntoLogic;
	}

	public void setUsuariosService(UsuariosService usuariosService) {
		this.usuariosService = usuariosService;
	}

	public void setDbSessionContext(DBSessionContext dbSessionContext) {
		this.dbSessionContext = dbSessionContext;
	}

	public MessageList transmiteOrden(int orden, int mto, boolean primeraTransmision) {
        MessageList messageList = new MessageList();

        Orden ordenObj = null;
        Message message = null;
        try {
            ordenObj = new Orden();

            HashUtil<String, Object> filter = new HashUtil<String, Object>();

            filter.put("orden", orden);
            filter.put("mto", mto);
            // Cargamos la orden para luego poder volver a la p�gina
            ordenObj = ordenService.loadOrdenDetail(filter);
            messageList.setObject(ordenObj);

            filter.clear();
            filter.put("orden", orden);
            filter.put("mto", mto);
            // Transmitimos la orden
            filter = ordenService.transmiteOrden(filter);

            message = new Message("transmite.success");

        } catch (Exception e) {
            logger.error("Error al transmitir la orden", e);
            try {
                message = ComponenteOrigenUtil.getErrorMessage(e);
            } catch (Exception e2) {
            	message = new ErrorMessage("transmite.error", e);
            }
        }
        messageList.add(message);
        return messageList;
    }

    public HashMap<String, String> getUbigeoDetail(String ubigeo) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashMap<String, String> element = new HashUtil<String, Object>();
        filter.put("ubigeo", ubigeo);
        try {
            element = formatoService.loadUbigeo(filter);
        } catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.loadUbigeo", e);
        }
        return element;
    }

    public HashMap<String, String> getUbigeoDetailByDistrito(int distritoId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        HashMap<String, String> element = new HashUtil<String, Object>();
        filter.put("distrito", distritoId);
        try {
            element = formatoService.loadUbigeoByDistrito(filter);
        } catch (Exception e) {
            logger.error("Error en formatoServiceImpl.loadUbigeo", e);
        }
        return element;
    }

    public Solicitante getUsuarioDetail(UsuarioCO usuario) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("usuario", usuario.getIdUsuario());
        Solicitante solicitante = new Solicitante();
        try {
            solicitante = formatoService.loadSolicitanteSinEmpresa(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return solicitante;
    }

    public Solicitante getUsuarioDetail(long orden, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("orden", orden);
        filter.put("mto", mto);
        Solicitante solicitante = new Solicitante();
        try {
            solicitante = formatoService.loadUsuarioFormato(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return solicitante;
    }

    public Solicitante getEmpresaDetail(UsuarioCO usuario) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("usuario", usuario.getIdUsuario());
        Solicitante solicitante = new Solicitante();
        try {
            solicitante = formatoService.loadSolicitanteConEmpresa(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return solicitante;
    }

    public Solicitante getSolicitanteDetail(UsuarioCO usuario) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("usuario", usuario.getIdUsuario());
        Solicitante solicitante = new Solicitante();
        try {
            if (usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_SOL) && usuario.getTipoPersona().equals(ConstantesCO.TIPO_PERSONA_JURIDICA)) {
                solicitante = formatoService.loadSolicitanteConEmpresa(filter);
            } else {
                solicitante = formatoService.loadSolicitanteSinEmpresa(filter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return solicitante;
    }

    public Solicitante getSolicitanteDetail(long orden, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("orden", orden);
        filter.put("mto", mto);
        Solicitante solicitante = new Solicitante();
        try {
            solicitante = formatoService.loadSolicitanteFormato(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return solicitante;
    }

    public String loadNombreUsuario(String numeroDocumento) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("numeroDocumento", numeroDocumento);
        String nombre = "";
		try {
			nombre = formatoService.loadNombreUsuario(filter);
		} catch (Exception e) {
			e.printStackTrace();
		}

        return nombre;
    }

    public String getPartidaById(Long partidaArancelaria) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("partida", partidaArancelaria);
        String titulo = null;
        try {
            titulo = formatoService.loadPartidaArancelaria(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return titulo;
    }

    /* Deprecado 20140617_JMCA BUG 119
     * public String getDescripcionUnidadMedidaFisicaById(Integer unidadMedidaFisicaId) { 
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("unidadMedidaFisicaId", unidadMedidaFisicaId);
        String descripcionUnidadMedidaFisica = null;
        try {
            descripcionUnidadMedidaFisica = ibatisService.loadElement("formato.unidad_medida_fisica.element", filter).getString("NOMBRE_COMERCIAL");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return descripcionUnidadMedidaFisica;
    }
	*/
    public Formato getFormatoByTupa(int idFormato, int idTupa) {
        Formato formato = null;
        try {
            HashUtil<String, Object> filter = new HashUtil<String, Object>();
            filter.put("idFormato", idFormato);
            filter.put("idTupa", idTupa);
            formato = formatoService.loadFormatoByTupa(filter);

        } catch (Exception e) {
            logger.error("Error en TestServiceImpl.cargarFormato", e);
        }
        return formato;
    }

    public RepresentanteLegal getRepresentanteDetail(long orden, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("orden", orden);
        filter.put("mto", mto);
        RepresentanteLegal representante = new RepresentanteLegal();
        try {
            representante = formatoService.loadRepresentanteFormato(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return representante;
    }

    public JasperPrint ejecutarReporteConConexionBD(JasperReport report, Map parameters) {
    	JasperPrint print = null;
    	try {
    		print = formatoService.ejecutarReporteConConexionBD(report, parameters);
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.ejecutarReporteConConexionBD", e);
        }
    	return print;
    }

    public AdjuntoRequerido getRequeridoById(int idFormato, int idRequerido) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idFormato", idFormato);
        filter.put("idRequerido", idRequerido);
        String titulo = null;
        AdjuntoRequerido adjunto = new AdjuntoRequerido();
        try {
        	adjunto = formatoService.loadRequerido(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return adjunto;
    }

    public MessageList uploadFile(int idOrden, int idMto, int formato, int adjunto, int adjuntoTipo, String nombre, Integer mensajeId, String esDetalle, byte[] bytes) {
        MessageList messageList = new MessageList();

        AdjuntoFormato archivoAdjunto = new AdjuntoFormato();
        archivoAdjunto.setOrdenId(idOrden);
        archivoAdjunto.setMto(idMto);
        archivoAdjunto.setFormatoId(formato);
        archivoAdjunto.setAdjuntoRequerido(adjunto);
        if (esDetalle==null || esDetalle.equalsIgnoreCase("N")) {
            archivoAdjunto.setNombre("(Req-"+adjunto+") " + ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        } else {
        	archivoAdjunto.setNombre(ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        }
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setMensajeId(mensajeId);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
        archivoAdjunto.setEsDetalle(esDetalle);

        Message message = null;
        try {
            adjuntoService.insertAdjunto(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.fileUpload", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

	public String obtenerAyuda(Map<String, Object> filtros) {
		String result = null;
		try {
			result =  formatoService.obtenerAyuda(filtros);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == null) {
			result = "";
		}
		return result;
	}

    public MessageList cargarArchivoDR(Integer mensajeId, String nombre, byte[] bytes) throws Exception {
        MessageList messageList = new MessageList();

        AdjuntoMensaje archivoAdjunto = new AdjuntoMensaje();
        archivoAdjunto.setMensajeId(mensajeId);
        archivoAdjunto.setNombre(ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
            adjuntoService.insertAdjuntoResolutorDR(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.cargarArchivoDR", e);
            message = new ErrorMessage("insert.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    public MessageList cargarArchivoSubsanacion(Integer mensajeId, String nombre, byte[] bytes) {
        MessageList messageList = new MessageList();

        AdjuntoMensaje archivoAdjunto = new AdjuntoMensaje();
        archivoAdjunto.setMensajeId(mensajeId);
        archivoAdjunto.setNombre(ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
            adjuntoService.insertAdjuntoSubsanacion(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.cargarArchivoSubsanacion", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

	public String permiteCrearFactura(Long ordenId, Long codigoCertificado) {
		String result = null;
		try {
			result = formatoService.permiteCrearFactura(ordenId, codigoCertificado);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.permiteCrearFactura", e);
		}
		return result;
	}

	public String permiteCrearProductor(Long calificacionUoId) {
		String result = null;
		try {
			result = formatoService.permiteCrearProductor(calificacionUoId);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.permiteCrearProductor", e);
		}
		return result;
	}

	public String permiteConfirmarFinEval(Long coId, Long ordenId, Integer mto) {
		String result = null;
		try {
			result = formatoService.permiteConfirmarFinEval(coId, ordenId, mto);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.permiteConfirmarFinEval", e);
		}
		return result;
	}

    public Long getFormatoDrId(int drId, int sdr){

        Long formatoDrId = null;

        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);

        try {
        	formatoDrId = formatoService.getFormatoDrId(filter);
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.getFormatoDrId", e);
        }

        return formatoDrId;
    }
    
    public int cuentaAdjuntosRequeridos(Orden orden) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("formato", orden.getIdFormato());
        filter.put("orden", orden.getOrden());
        filter.put("mto", orden.getMto());
        filter.put("certificadoReexportacion", orden.getCertificadoReexportacion());
        int cuenta = 1;
        try {
            cuenta = formatoService.cuentaAdjuntosRequeridos(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cuenta;
    }

    public int cuentaNotificacionesPendientes(Long suceId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", suceId);
        int cuenta = 0;
        try {
            cuenta = formatoService.cuentaNotificacionesPendientes(filter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	return cuenta;
        }
    }

    //public int cuentaAdjuntosRequeridosDJ(long djId, String tipoRolDj) {
    public int cuentaAdjuntosRequeridosDJ(long djId) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("djId", djId);
        //filter.put("tipoRolDj", tipoRolDj);
        int cuenta = 1;
        try {
            cuenta = formatoService.cuentaAdjuntosRequeridosDJ(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cuenta;
    }

    public int cuentaNotificacionesSolicitudPendientes(Long ordenId, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("mto", mto);
        int cuenta = 0;
        try {
            cuenta = formatoService.cuentaNotificacionesSolicitudPendientes(filter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	return cuenta;
        }
    }

    public MessageList insertAdjuntoFactura(HashUtil<String, Object> filter) throws Exception {
        MessageList messageList = new MessageList();

        filter.put("adjuntoTipo", ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
            adjuntoService.insertAdjuntoFactura(filter);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.insertAdjuntoFactura", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

    public MessageList eliminarAdjuntoFactura(Long adjuntoId, Long coId, String formato) throws Exception {
        MessageList messageList = new MessageList();
        
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("id", adjuntoId);
        filter.put("coId", coId);
        filter.put("formato", formato);
        //filter.put("adjuntoTipo", ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
            adjuntoService.eliminarAdjuntoFactura(filter);
            message = new Message("delete.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.eliminarAdjuntoFactura", e);
            message = new ErrorMessage("delete.error", e);
        }

        messageList.add(message);
        return messageList;
    }

    public Table obtenerJerarquiaTramite(Integer ordenId) {
    	Table tablaJerarquia = null;
    	try {

    		HashUtil<String, Object> filter = new HashUtil<String, Object>();
    		filter.put("tiene", null);
    		filter.put("ordenId", ordenId);

    		ibatisService.executeSPWithObject("formato.jerarquia.tieneJerarquia", filter);

    		boolean tieneJerarquia = filter.get("tiene").toString().equalsIgnoreCase("S");

    		if (tieneJerarquia) {
	    		filter.clear();
	    		filter.put("ordenId", ordenId);

	    		ibatisService.executeSPWithObject("formato.jerarquia.calcula", filter);

	    		tablaJerarquia = ibatisService.loadGrid("formato.jerarquia.listado", null);
    		}
        } catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.obtenerJerarquiaTramite", e);
        }
        return tablaJerarquia;
    }

	public String permiteCrearSolicitudRectif(Long suceId){
		String result = null;
		try {
			result = formatoService.permiteCrearSolicitudRectif(suceId);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.permiteCrearSolicitudRectif", e);
		}
		return result;
	}

	public String permiteApoderamientoXFrmt(Integer acuerdoInternacionalId){
		String result = "N";
		try {
			result = formatoService.permiteApoderamientoXFrmt(acuerdoInternacionalId);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.permiteApoderamientoXFrmt", e);
		}
		return result;
	}

    public int cuentaAdjuntosFacturaRequeridos(long coId, String formato) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("formato", formato);
        filter.put("id", coId);
        int cuenta = 0;
        try {
            cuenta = formatoService.cuentaAdjuntosFacturaRequeridos(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cuenta;
    }

	public String acuerdoConValidacionProductor(Integer codigoAcuerdo) {
		String result = null;
		try {
			result = formatoService.acuerdoConValidacionProductor(codigoAcuerdo);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.acuerdoConValidacionProductor", e);
		}
		return result;
	}

    public MessageList insertAdjuntoProductor(HashUtil<String, Object> filter) throws Exception {
        MessageList messageList = new MessageList();

        filter.put("adjuntoTipo", ConstantesCO.ADJUNTO_TIPO_PDF);

        Message message = null;
        try {
            adjuntoService.insertAdjuntoProductor(filter);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.insertAdjuntoProductor", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }

    public MessageList eliminarAdjuntoProductor(HashUtil<String, Object> filter) throws Exception {
        MessageList messageList = new MessageList();

        //filter.put("id", adjuntoId);

        Message message = null;
        try {
            adjuntoService.eliminarAdjuntoProductor(filter);
            message = new Message("delete.success");
        } catch (Exception e) {
            logger.error("Error en FormatoServiceImpl.eliminarAdjuntoProductor", e);
            message = new ErrorMessage("delete.error", e);
        }

        messageList.add(message);
        return messageList;
    }

	public Message validacionPreTransmision(HashUtil<String, Object> filter){
		Message res = null;

		try{
			this.formatoService.validacionPreTransmision(filter);
		} catch (Exception e) {
            logger.error("Error al transmitir la orden", e);
            try {
            	res = ComponenteOrigenUtil.getErrorMessage(e);
            } catch (Exception e2) {
            	res = new ErrorMessage("transmite.error", e);
            }
        }

		return res;
	}
	
		
    public Message validacionPreFormato(int usuarioId, Formato formato) { //20140526 - JMC - BUG 1.PersonaNatural
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        Message message = null;
        
        filter.put("idFormato", formato.getId());
        filter.put("idUsuario", usuarioId);
        filter.put("mensaje", null);
        
        try {
            formatoService.validacionPreFormato(filter);
            String mensaje = filter.getString("mensaje");
            
            if (!mensaje.equals("")) {
                message = new Message("message", mensaje);
            }
        } catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.validacionPreFormato", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }
        return message;
    }
    
	public String es_version_antigua(Long dr, Integer sdr) {
		String result = null;
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("dr", null);
		filter.put("sdr", null);
		
		try {
			result = formatoService.es_version_antigua(filter);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.acuerdoConValidacionProductor", e);
		}
		return result;
	}
	
	public String es_dj_version_diferente(Long dr, Integer sdr) {
		String result = null;
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("dr", null);
		filter.put("sdr", null);
		
		try {
			result = formatoService.es_dj_version_diferente(filter);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.acuerdoConValidacionProductor", e);
		}
		return result;
	}
	
	public String es_reemplazo_version_diferente(Long dr, Integer sdr) {
		String result = null;
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("dr", null);
		filter.put("sdr", null);
		
		try {
			result = formatoService.es_reemplazo_version_diferente(filter);
		} catch (Exception e) {
            logger.error("Error en FormatoLogicImpl.acuerdoConValidacionProductor", e);
		}
		return result;
	}


}
