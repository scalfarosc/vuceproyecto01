package pe.gob.mincetur.vuce.co.logic;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.domain.Auditoria;
import pe.gob.mincetur.vuce.co.service.AuditoriaService;

public class AuditoriaLogicImpl implements AuditoriaLogic{
	
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);
    
    private AuditoriaService auditoriaService;

	public void setAuditoriaService(AuditoriaService auditoriaService) {
		this.auditoriaService = auditoriaService;
	}

	public void grabarInicioSesion(Auditoria auditoria) {
		try {
			auditoriaService.insertInicioSesion(auditoria);
		} catch (Exception e) {
			logger.error("ERROR en AuditoriaLogicImpl.grabarInicioSesion", e);
		}
	}

	public void grabarInicioSesionContingencia(Auditoria auditoria) {
		try {
			auditoriaService.insertInicioSesionContingencia(auditoria);
		} catch (Exception e) {
			logger.error("ERROR en AuditoriaLogicImpl.grabarInicioSesionContingencia", e);
		}
	}
	
	public void grabarFinSesionContingencia(Auditoria auditoria) {
		try {
			auditoriaService.insertFinSesionContingencia(auditoria);
		} catch (Exception e) {
			logger.error("ERROR en AuditoriaLogicImpl.grabarFinSesionContingencia", e);
		}
	}
	
	public void grabarAccesoSolicitud(Auditoria auditoria) {
		try {
			auditoriaService.insertAccesoSolicitud(auditoria);
		} catch (Exception e) {
			logger.error("ERROR en AuditoriaLogicImpl.grabarAccesoSolicitud", e);
		}
	}

	public void grabarAccesoSuce(Auditoria auditoria) {
		try {
			auditoriaService.insertAccesoSuce(auditoria);
		} catch (Exception e) {
			logger.error("ERROR en AuditoriaLogicImpl.grabarAccesoSuce", e);
		}
	}

	public void grabarAccesoDr(Auditoria auditoria) {
		try {
			auditoriaService.insertAccesoDr(auditoria);
		} catch (Exception e) {
			logger.error("ERROR en AuditoriaLogicImpl.grabarAccesoDr", e);
		}
	}

}
