package pe.gob.mincetur.vuce.co.logic.certificado;

import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoRequeridoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificado.Certificado;

public interface CertificadoLogic {
	
	public MessageList creaCertificado(Integer usuario, Integer entidadCertificadora, Integer formatoId, Integer solicitante, Integer representante, String cargo);
	
	public MessageList transmiteCertificado(Long ordenId, Integer mto);
	
	public Certificado getCertificadoById(Long ordenId, Integer mto);
	
	public UsuarioFormato getUsuarioFormatoById(Long ordenId, Integer mto);
	
	public Solicitud getSolicitudById(Long ordenId, Integer mto);
	
	public Solicitante getSolicitanteDetail(Long ordenId, Integer mto, Integer usuarioFormatoTipo);
	
	public AdjuntoRequeridoCertificado getAdjuntoRequeridoById(Integer certificadoFormatoId, Integer adjuntoRequeridoCertificado);
	
	public MessageList uploadFile(Long idOrden, Integer idMto, Integer adjunto, String nombre, byte [] bytes);
	
	public MessageList updateRepresentante(Long solicitante, Long ordenId, Integer mto, Integer representante);
	
	public MessageList updateCargoDeclarante(Long declarante, Long ordenId, Integer mto, String cargo);
	
	public Integer getAdjuntoRequeridoCount(long ordenId,int mto);
	
	public byte [] imprimirCertificado(UsuarioCO usuario , HashUtil<String, String> datos);
	
    public HashUtil<String, Object> registrarBorradorDr(Long suceId) throws Exception;
	    
	public MessageList deleteBorradorDr(Long certificadoDrBorradorId);
	
	public MessageList transmiteResolutor(long certificadoDrBorradorId);
	
	 public Solicitante getSolicitanteDRDetail(long drId, int sdr, int usuarioFormatoTipo);
	    
	public Solicitante getSolicitanteDRBorradorDetail(long certificadoDrId, int usuarioFormatoTipo);
	
	public Certificado getCertificadoDRById(Long certificadoDrId);
	
}
