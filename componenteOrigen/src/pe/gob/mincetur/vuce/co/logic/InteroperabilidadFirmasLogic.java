package pe.gob.mincetur.vuce.co.logic;

import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;

public interface InteroperabilidadFirmasLogic {

    public InteroperabilidadFirma obtenerToken(Long drId, Integer sdr) throws Exception;
	
    public Integer registrarInteroperabilidad(InteroperabilidadFirma iop) throws Exception;
	
    public InteroperabilidadFirma obtenerEtapa(String token, Integer secuencia)  throws Exception;

    public byte[] generarXml(String token)  throws Exception;
    
    public byte[]  generarPdf(String token)  throws Exception;

    public Integer enviarXmlFirmado(String token, byte[] xml) throws Exception;
    
    public Integer enviarPdfFirmado(String token, byte[] pdf) throws Exception;

    public void actualizaFirmaExportador(Long drId, Integer sdr, String nombreExp, String formato, String posteriori) throws Exception;
    
    public InteroperabilidadFirma obtenerPdf(String token, Integer secuencia) throws Exception;

    public void generarCertificadoOrigenElectronicoPC(Long drId, Integer sdr);
    
    public String generarTokenFA (Long drId, Integer sdr);
    
    public boolean produccionControlada(Long drId) throws Exception;
    
    public RespuestaCert obtenerCertificado(String certificadoOrigen , String codigoPais);
    
    public String tipoFirma(Long drId) throws Exception;
    
    public String permiteFirmaDigital(Long ordenId) throws Exception;
}
