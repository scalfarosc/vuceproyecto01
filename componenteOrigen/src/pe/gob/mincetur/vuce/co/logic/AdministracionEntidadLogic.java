package pe.gob.mincetur.vuce.co.logic;

import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;

public interface AdministracionEntidadLogic {
	
	public MessageList asignarEvaluadorSolicitudCalificacion(HashUtil<String, String> datos);
	
	public MessageList asignarEvaluadorCertificado(HashUtil<String, String> datos);
	
	public Solicitante getSolicitanteDetail(long ordenId, int mto, int usuarioFormatoTipo);
	
	public MessageList iniciarEvaluacionCalificacion(HashUtil<String, String> datos);
	
	public MessageList iniciarEvaluacionCertificado(HashUtil<String, String> datos);
	
}
