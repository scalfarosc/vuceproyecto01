package pe.gob.mincetur.vuce.co.logic;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoFormatoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.service.AdjuntoService;
import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.file.ZipUtil;

public class AdjuntoLogicImpl implements AdjuntoLogic {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_LOGIC);
    
    private AdjuntoService adjuntoService;
    
    public void setAdjuntoService(AdjuntoService adjuntoService) {
        this.adjuntoService = adjuntoService;
    }
    
    public Adjunto descargarAdjuntoById(int idAdjunto) {
        MessageList messageList = new MessageList();
        Message message = null;
        Adjunto adjunto = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idAdjunto", idAdjunto);
        try {
            adjunto = adjuntoService.loadAdjunto(filter);
        } catch (Exception e) {
            logger.error("Error en BuzonLogicImpl.descargarAdjuntoById", e);

        }
        return adjunto;
    }

    public void eliminarAdjuntoFormato(int id, int mto) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("id", id);
        //filter.put("mto", mto);
        try {
            adjuntoService.eliminarAdjuntoFormato(filter);
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.eliminarAdjuntoFormato", e);
        }
    }
    
    public void eliminarAdjuntoById(int id) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("id", id);
        try {
            adjuntoService.eliminarAdjuntoById(filter);
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.eliminarAdjuntoById", e);
        }
    }

    public List<AdjuntoFormatoCertificado> loadModifSuceList(int mensajeId) throws Exception{
    	return adjuntoService.loadModifSuceList(mensajeId);
    }
    
    public MessageList uploadFileDR(Long calificacionDrBorradorId, Long certificadoDrBorradorId, String nombre, byte [] bytes) {
        MessageList messageList = new MessageList();
        
        Adjunto archivoAdjunto = new Adjunto();
        archivoAdjunto.setCalificacionDrBorradorId(calificacionDrBorradorId);
        archivoAdjunto.setCertificadoDrBorradorId(certificadoDrBorradorId);
        
        archivoAdjunto.setNombre("(Req-"+ComponenteOrigenUtil.replaceSpecialCharacters(nombre));
        archivoAdjunto.setArchivo(bytes);
        archivoAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
        
        Message message = null;
        try {
            adjuntoService.insertAdjuntoResolutorBorradorDR(archivoAdjunto);
            message = new Message("insert.success");
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.uploadFileDR", e);
            message = ComponenteOrigenUtil.getErrorMessage(e);
        }

        messageList.add(message);
        return messageList;
    }
    
    public Adjunto descargarAdjuntoDRById(int idAdjunto) {
        MessageList messageList = new MessageList();
        Message message = null;
        Adjunto adjunto = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idAdjunto", idAdjunto);
        try {
            adjunto = adjuntoService.loadAdjuntoDR(filter);
        } catch (Exception e) {
            logger.error("Error en BuzonLogicImpl.descargarAdjuntoDRById", e);

        }
        return adjunto;
    }
    
    public void eliminarAdjuntoCalificacionById(Long calificacionDrBorradorId, int id) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("borradorDrId", calificacionDrBorradorId);
        filter.put("id", id);
        try {
            adjuntoService.eliminarAdjuntoCalificacionById(filter);
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.eliminarAdjuntoCalificacionById", e);
        }
    }
    
    public void eliminarAdjuntoCertificadoById(Long certificadoDrBorradorId, int id) {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("borradorDrId", certificadoDrBorradorId);
        filter.put("id", id);
        try {
            adjuntoService.eliminarAdjuntoCertificadoById(filter);
        } catch (Exception e) {
            logger.error("Error en AdjuntoLogicImpl.eliminarAdjuntoCertificadoById", e);
        }
    }
    
    //======= Intercambio Electronico ======//
    //20140825_JMC_Intercambio_Empresa
    
    private byte[] getXMLebXMLFromList(List<Adjunto> adjuntos, int tipo) {
        byte[] bytesFile = null;
        if (adjuntos!=null) {
        for (Adjunto adjunto : adjuntos) {
            if (adjunto.getTipo().equals(tipo)) {
                bytesFile = adjunto.getArchivo();
                break;
            }
        }
        }
        return bytesFile;
    }

    public byte[] getXMLFromList(List<Adjunto> adjuntos) {
        return getXMLebXMLFromList(adjuntos, ConstantesCO.ADJUNTO_TIPO_XML);
    }

    public byte[] getEBXMLFromList(List<Adjunto> adjuntos) {
        return getXMLebXMLFromList(adjuntos, ConstantesCO.ADJUNTO_TIPO_EBXML);
    }

    public byte[] getZIPFromList(List<Adjunto> adjuntos) {
        return getXMLebXMLFromList(adjuntos, ConstantesCO.ADJUNTO_TIPO_ZIP);
    }

    public List<Adjunto> getAdjuntosFromList(List<Adjunto> adjuntos) {
    	List<Adjunto> listaAdjuntos = new ArrayList<Adjunto>();
        for (Adjunto adjunto : adjuntos) {
            if (adjunto.getTipo().equals(ConstantesCO.ADJUNTO_TIPO_PDF)) {
                listaAdjuntos.add(adjunto);
            }
        }
        return listaAdjuntos;
    }
    
    public List<Adjunto> cargarArchivosTransmision(Transmision transmision) {
        List<Adjunto> adjuntos = null;
        try {
            String transaccion = transmision.getTransaccion();            
            // Cargo los archivos de la transmision (XML y ebXML)
            adjuntos = adjuntoService.loadListForTransmision("adjunto.transmision.list", transmision.getId());

        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar cargar los archivos adjuntos de la transmision: "+transmision, e);
        }
        return adjuntos;
    }
    
    public List<Adjunto> cargarArchivosTransmisionIOP(Transmision transmision) {
        List<Adjunto> adjuntos = null;
        try {
            String transaccion = transmision.getTransaccion();
            //adjuntos = adjuntoService.loadListForTransmision("adjunto.transmision.iop.list", transmision.getId());
            adjuntos = adjuntoService.loadListForTransmision("adjunto.transmision.iop.cabecera.list", transmision.getId());

        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar cargar los archivos adjuntos de la transmision: "+transmision, e);
        }
        return adjuntos;
    }    

    public void grabarArchivosTransmision(Transmision transmision, byte[] bytesXML, byte[] bytesEBXML, byte[] bytesZipAdjuntos) {
        try {
            // Inserto el XML
            if (bytesXML!=null) {
            	Adjunto xml = new AdjuntoFormato();
                xml.setNombre(ConstantesCO.ADJUNTO_NOMBRE_XML);
                xml.setArchivo(bytesXML);
                xml.setTipo(ConstantesCO.ADJUNTO_TIPO_XML);
                xml.setVcId(transmision.getId());
                
                adjuntoService.insertAdjuntoForTransmision(xml);
            }
            
            // Inserto el ebXML
            if (bytesEBXML!=null) {
            	Adjunto ebXML = new AdjuntoFormato();
                ebXML.setNombre(ConstantesCO.ADJUNTO_NOMBRE_EBXML);
                ebXML.setArchivo(bytesEBXML);
                ebXML.setTipo(ConstantesCO.ADJUNTO_TIPO_EBXML);
                ebXML.setVcId(transmision.getId());                
                adjuntoService.insertAdjuntoForTransmision(ebXML);
            }
            
            // Inserto los adjuntos
            if (bytesZipAdjuntos!=null) {
                Adjunto zipAdjuntos = new AdjuntoFormato();
                zipAdjuntos.setNombre(ConstantesCO.ADJUNTO_NOMBRE_ADJUNTOS);
                zipAdjuntos.setArchivo(bytesZipAdjuntos);
                zipAdjuntos.setTipo(ConstantesCO.ADJUNTO_TIPO_ZIP);
                zipAdjuntos.setVcId(transmision.getId());
                adjuntoService.insertAdjuntoForTransmision(zipAdjuntos);
            }
        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar grabar los archivos de la transmision en la Base de Datos", e);
        }
    }
    
	public void grabarArchivosPDFTransmision(int idNTX, byte[] adjuntosZip) {		
		try {			
			if (adjuntosZip != null) {
				HashMap<String,byte[]> adjuntos = ZipUtil.decompressFile(new ByteArrayInputStream(adjuntosZip));		
				Iterator<String> it = adjuntos.keySet().iterator();
				while (it.hasNext()) {
					String nombre = it.next();
					Adjunto adjunto = new AdjuntoFormato();
					adjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);
					adjunto.setNombre(nombre);
					adjunto.setArchivo(adjuntos.get(nombre));
				    //files.add(adjunto);
					int idAdjunto = adjuntoService.insertAdjuntoForTransmisionEmpresa(adjunto);
				    adjuntoService.insertTransmisionForAdjunto(idNTX, idAdjunto);	
				}
			}
			
        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar grabar los archivos de la transmision en la Base de Datos", e);
        }
		
		//return adjuntosRegistrados;
	}

	public void registrarNombreAdjuntos(int mct001TmpId, List<AdjuntoFormato> adjuntos){
		
		try {
			for (AdjuntoFormato adjunto : adjuntos){				
				AdjuntoFormato pdfAdjunto = new AdjuntoFormato();
				pdfAdjunto.setNombre(adjunto.getNombre());
				pdfAdjunto.setSecuencia(adjunto.getSecuencia());
				//pdfAdjunto.setArchivo(adjunto.getArchivo());
				//pdfAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);                
                adjuntoService.insertNombreAdjunto(mct001TmpId, pdfAdjunto);                
			}
            
        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar grabar los archivos de la transmision en la Base de Datos", e);
        }
		
	}
	
	
    

	public void registrarNombreAdjuntosDJ(int mct005TmpId, List<AdjuntoFormato> adjuntos){
		
		try {
			for (AdjuntoFormato adjunto : adjuntos){				
				AdjuntoFormato pdfAdjunto = new AdjuntoFormato();
				pdfAdjunto.setNombre(adjunto.getNombre());
				pdfAdjunto.setSecuencia(adjunto.getSecuencia());
				pdfAdjunto.setAdjuntoRequerido(adjunto.getAdjuntoRequerido());
                adjuntoService.insertNombreAdjuntoDJForTransmision(mct005TmpId, pdfAdjunto);                
			}
            
        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar grabar los archivos de la transmision en la Base de Datos", e);
        }
		
	}
	
	public void registrarNombreAdjuntosDJMaterial(int mct005TmpId, List<DeclaracionJuradaMaterial> listDeclaracionJuradaMaterial){		
		try {
			
			for(DeclaracionJuradaMaterial material : listDeclaracionJuradaMaterial){
				
				for (Adjunto adjunto : material.getListAdjunto()){				
					Adjunto pdfAdjunto = new Adjunto();
					pdfAdjunto.setId(adjunto.getId());
					pdfAdjunto.setNombre(adjunto.getNombre());
					
	                adjuntoService.insertNombreAdjuntoDJMaterialForTransmision(mct005TmpId, (int)material.getSecuenciaMaterial(), pdfAdjunto);                
				}
			}
			/*
			for (AdjuntoFormato adjunto : adjuntos){				
				AdjuntoFormato pdfAdjunto = new AdjuntoFormato();
				pdfAdjunto.setNombre(adjunto.getNombre());
				pdfAdjunto.setSecuencia(adjunto.getSecuencia());
				//pdfAdjunto.setArchivo(adjunto.getArchivo());
				//pdfAdjunto.setTipo(ConstantesCO.ADJUNTO_TIPO_PDF);                
                adjuntoService.insertNombreAdjuntoDJForTransmision(mct005TmpId, pdfAdjunto);                
			}
            */
        } catch (Exception e) {
            logger.error("Ocurrio un error al intentar grabar los archivos de la transmision en la Base de Datos", e);
        }
		
	}		
	
}
