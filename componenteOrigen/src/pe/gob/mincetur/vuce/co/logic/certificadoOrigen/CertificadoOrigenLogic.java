package pe.gob.mincetur.vuce.co.logic.certificadoOrigen;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.jlis.core.bean.Message;
import org.jlis.core.list.MessageList;
import org.jlis.core.list.OptionList;
import org.jlis.core.util.HashUtil;
import org.jlis.web.list.Table;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.AdjuntoCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.AdjuntoRequeridoDJMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;
import pe.gob.mincetur.vuce.co.domain.ebxml.MCT001EBXML;
import pe.gob.mincetur.vuce.co.domain.ebxml.MCT005EBXML;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;


/**
 * Interfaz de lógica de negocio de Certificados de Origen
 * @author Erick Oscátegui
 */

public interface CertificadoOrigenLogic {

	public CertificadoOrigen getCertificadoOrigenById(long coId, String formato);

	public CertificadoOrigenMercancia getCOMercanciaById(long coId, int secuencia, String formato);

	public CertificadoOrigen convertMapToCertificadoOrigen(Map<String, Object> map);

	public MessageList updateCertificadoOrigen(CertificadoOrigen certificadoOrigen);

	public MessageList cargarAdjuntoCertificadoOrigen(AdjuntoCertificadoOrigen adjunto);

	public CertificadoOrigenMercancia getCertificadoOrigenMercanciaById(long id, int secuencia, String formato);

	public CertificadoOrigenMercancia convertMapToCOMercancia(Map<String, Object> map);

	public MessageList insertCertificadoOrigenMercanciaDjExistente(CertificadoOrigenMercancia certificadoOrigenMercancia, String formato);

	//public MessageList insertCertificadoOrigenMercanciaDjNueva(CertificadoOrigenMercancia certificadoOrigenMercancia);

	public MessageList updateCertificadoOrigenMercancia(CertificadoOrigenMercancia certificadoOrigenMercancia, String formato);

	public MessageList updateMercanciaDeclaracionJurada(DeclaracionJurada dj);

	public MessageList deleteCertificadoOrigenMercancia(CertificadoOrigenMercancia certificadoOrigenMercancia, String formato);

	public MessageList deleteMercanciaDeclaracionJurada(DeclaracionJurada dj);

	public COFactura getCOFacturaById(long coId, int secuencia, String formato);

	public COFactura convertMapToCOFactura(Map<String, Object> map);

	public MessageList insertCOFactura(COFactura factura, String formato);

	public MessageList updateCOFactura(COFactura factura, String formato);

	public MessageList deleteCOFactura(COFactura factura, String formato);

	public DeclaracionJurada getCODeclaracionJuradaById(long id);

	public DeclaracionJurada convertMapToDeclaracionJurada(Map<String, Object> map);

	public MessageList insertDeclaracionJurada(DeclaracionJurada declaracionJurada);

	public MessageList insertDeclaracionJuradaSinMercancia(DeclaracionJurada declaracionJurada);

	public MessageList updateDeclaracionJurada(DeclaracionJurada declaracionJurada);

	public MessageList updateDeclaracionJuradaConsolidado(DeclaracionJurada declaracionJurada);

	public MessageList cargarAdjuntoDeclaracionJurada(AdjuntoCertificadoOrigen adjunto);

	public DeclaracionJuradaMaterial getDeclaracionJuradaMaterialById(int id, int secuencia);

	//public MessageList insertCOMercancia(CertificadoOrigenMercancia mercancia);

	public MessageList updateCOMercancia(CertificadoOrigenMercancia mercancia, String formato);

	public MessageList deleteCOMercancia(CertificadoOrigenMercancia mercancia, String formato);

	public List<DeclaracionJuradaProductor> getDJProductorByDJId(long djId);

    public DeclaracionJuradaMaterial convertMapToDeclaracionJuradaMaterial(Map<String, Object> map);

	public MessageList insertDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial);

	public MessageList updateDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial);

	public MessageList deleteDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial);

	public DeclaracionJuradaProductor getDeclaracionJuradaProductorById(long id, int secuencia);

    public DeclaracionJuradaProductor convertMapToDeclaracionJuradaProductor(Map<String, Object> map);

	public MessageList insertDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor);

	public MessageList updateDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor);

	public MessageList deleteDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor);

	public OptionList obtenerPaisesAcuerdo(HashUtil filtros);

	public HashUtil<String, Object> getCertificadoOrigenDRByDrId(int drId, int sdr, String formato);

    public AdjuntoRequeridoDJMaterial getRequeridoDJById(int adjuntoRequeridoDj);

    public MessageList uploadFileDJ(long djId, int secuenciaMaterial, int adjuntoRequeridoDj, int adjuntoTipo, String nombre, byte[] bytes);

    public void eliminarAdjuntoDJ(long adjuntoId, int djId);

    public CODuplicado getCertificadoOrigenMTC002ById(long coId);

    public CODuplicado convertMapToCertificadoOrigenMCT002(Map<String, Object> map);

    public MessageList updateCertificadoOrigenDuplicado(CODuplicado coDuplicado);

    public MessageList crearSolicitudCertificado(UsuarioCO usuario, HashUtil<String, String> datos) throws Exception;
    
    //public MessageList insertCertificadoOrigenDuplicadoOrigen(MessageList messageList, long coId, Long orden, Long drIdOrigen, int sdrOrigen);

    public String getCausalById(String causal);

    public COAnulacion getCertificadoOrigenMTC004ById(long coId);

    public COAnulacion convertMapToCertificadoOrigenMCT004(Map<String, Object> map);

    public MessageList updateCertificadoOrigenAnulacion(COAnulacion coAnulacion);

    //public MessageList insertCertificadoOrigenAnulacionOrigen(MessageList messageList, long coId,Long drIdOrigen, int sdrOrigen);

    public MessageList solicitaValidacionProductor(long djId);

    public MessageList aceptaSolicitudValidacion(long djId);

    public MessageList rechazaSolicitudValidacion(long djId);

    public MessageList transmiteDjValidada(long djId);
    
    //NPCS: 09/02/2018
    public String existeProductorValidador(HashMap<String, Object> filter);

    //public MessageList insertCertificadoOrigenReemplazoOrigen(MessageList messageList, long ordenId, long coId,Long drIdOrigen, int sdrOrigen);

    public MessageList updateCertificadoOrigenReemplazo(COReemplazo coReemplazo);

    public COReemplazo convertMapToCertificadoOrigenMCT003(Map<String, Object> map);

    public HashUtil<String, Object> insertSubsanacionSolicitud(long ordenId,int mto);

    public MessageList deleteSubsanacionSolicitud(long ordenId,int mto);

    public int mtoCertificadoVigente(Long ordenId) throws Exception;

	public Long getAdjunFirmaId(Long suceId, Long drId) throws Exception;

    public MessageList registraExportadorAutorizadoDj(Long djId, String tipoDoc, String numerodoc, Date fInicio, Date fFin);

    public MessageList revocaExportadorAutorizadoDj(Long djId, String usuarioEmpId);

	public void actualizarFlgDjCompleta(long calificacionUoId, String flg);

	public Table obtenerPartidas(HashUtil<String, Object> filter);

	public String djBtnValidacionProd(Long dj_id, Long orden_id, Integer mto);

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	public CalificacionOrigen getCalificacionOrigenById(Long calificacionId);
	public MessageList updateDireccionAdicionalCalificacionOrigen(Long calificacionId, String direccionAdicional);
	public MessageList insertCalificacionOrigen(CalificacionOrigen calificacionOrigen);
	public MessageList updateRolCalificacionOrigen(CalificacionOrigen calificacionOrigen);
	public MessageList updateCriterioCalificacionOrigen(CalificacionOrigen calificacionOrigen);
	public CalificacionOrigen convertMapToCalificacionOrigen(Map<String, Object> map);
	public MessageList insertDeclaracionJuradaCalificacion(DeclaracionJurada declaracionJurada);
	//public MessageList insertCalificacionOrigenConDeclaracionJurada(MessageList messageList, CalificacionOrigen calificacionOrigen);
	public MessageList insertCalificacionOrigenConMercanciaDeclaracionJurada(CalificacionOrigen calificacionOrigen);
	public String djEnProcesoValidacion(HashUtil<String, Object> filter) throws Exception;
    public MessageList enlazaDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception;
    public MessageList copiarDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception;
    public int cuentaDJMateriales(Long djId);
    public DeclaracionJurada getDjByCalificacionUoId(Long calificacionUoId, String enIngles);
    //public MessageList copiarDJCalificacionXCalificacion(MessageList messageList, HashUtil<String, Object> filter);
    public String djMaterialTieneAdj(HashUtil<String, Object> filter);

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	public String reqValidacionProductor(HashMap<String, Object> filter);
	public String djValidada(HashMap<String, Object> filter);
	public String validaHashCalificacion(HashMap<String, Object> filter);

	/*******************************************************************************************************************************/

	public String obtenerIdiomaLlenado(Integer idAcuerdo);
	public Message validarCertificadoEspacioDet(HashMap<String, Object> filter);
	public double totalValorUSMateriales(HashMap<String, Object> filter);
	
	public String esPropietarioDatos(long calificacionUoId, String nroDocUsuario);
	
	//20140825_JMC_Intercambio_Empresa
	public MCT001EBXML getCertificadoObjectFromEbXML(File ebXml);	
	public MCT005EBXML getDJObjectFromEbXML(File ebXml);		
	public SolicitudEBXML parserEbXmlToBean(byte[] xmlNotificacion, byte[] ebXml, byte[] bytesZipAdjuntos, int idNTX, String tipoTransaccion);	
	public int insertCertificadoOrigenEmpresa(CertificadoOrigen certificadoOrigen);    
    public MessageList insertCOFacturaEmpresa(COFactura factura); 
    public MessageList insertCertificadoOrigenMercanciaEmpresa(CertificadoOrigenMercancia certificadoOrigenMercancia) ;
    public int insertDeclaracionJuradaEmpresa(DeclaracionJurada declaracionJurada);    
    public MessageList insertDeclaracionJuradaProductorEmpresa(DeclaracionJuradaProductor declaracionJuradaProductor);
    public MessageList insertDeclaracionJuradaMaterialEmpresa(DeclaracionJuradaMaterial declaracionJuradaMaterial);
    public MCT001EBXML convertirMCT001(DR dr) throws Exception;
    public MCT005EBXML convertirMCT005(DR dr) throws Exception;
    //20140912 Reexportacion
    public MessageList insertCOMercancia(CertificadoOrigenMercancia mercancia, String formato);
    
    public void setearResumenReporte(Long drId, Long sdr, Integer idAcuerdo, HashUtil<String, String> datos, HttpServletResponse response) throws Exception;
    public void setearAvanceReporte(Long ordenId, Long mto, Integer idAcuerdo, HashUtil<String, String> datos, HttpServletResponse response) throws Exception;
    public byte[] generarReporteBytes(Long drId, Long sdr, Integer idAcuerdo, String formato) throws Exception;    
    public byte[] generarReporteAPBytes(Integer codId) throws Exception;    
    public byte[] descargarDocFDById(long codId);
    
    public String validaCOFD(Integer idCertificado);
    public void insertArchivoCOFD(byte[] bytes, Integer idCertificado);
    
    public Message habilitaFirma(Long drId, Integer sdr);

    /*NPCS 12/02/2018*/
	public String solicitoCalificacion(long calificacionUoId, String numDocUsuario);
	
    //NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
    public byte[] descargarDocFDXMLById(long codId);
    public void eliminarMercanciasXML(int codId);
        
}