package pe.gob.mincetur.vuce.co.logic;

import org.jlis.core.list.MessageList;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.domain.Orden;

public interface OrdenLogic {

    public MessageList getOrdenDetailById(Long idOrden, Integer idMto);

    public Orden getOrdenById(Long idOrden, Integer idMto);

    public Orden loadOrdenById(Long idOrden);

    public Orden loadOrdenByNumero(Long numero);

    public MessageList creaOrden(UsuarioCO usuario, Integer acuerdo, Integer pais, Integer entidadCertificadora, Integer solicitante, Integer representante, 
    		                     Integer empresa, Integer idTupa, Integer idFormato, String certificadoOrigen, String certificadoReexportacion); 

    public MessageList creaModifOrden(UsuarioCO usuario, Long orden, Integer mto);

    public void desisteOrden(Integer idEntidad, Long orden, Integer mto);

    public MessageList updateOrdenRepresentante(Integer idSolicitante, Long orden, Integer mto, Integer idRepresentante);

    //public Integer existeModificacion(Integer idOrden, Integer idMto);
}
