package pe.gob.mincetur.vuce.co.logic.dj;

import java.util.Map;

import org.jlis.core.list.MessageList;

import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.dj.AdjuntoRequeridoDJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJExportador;
import pe.gob.mincetur.vuce.co.domain.dj.DJMaterial;
import pe.gob.mincetur.vuce.co.domain.dj.DJProductor;

public interface DJLogic {
	
	public MessageList creaDJ(Long usuario, Long solicitante, Long representante, String cargo);
	public MessageList transmiteDJ(long ordenId, int mto);
	public DJ convertMapToDJ(Map<String, Object> map);
	public MessageList updateDJ(DJ dj) throws Exception;
	public DJ getDJById(long ordenId, Integer mto);
	public DJMaterial getDJMaterialById(long djId, Integer secuenciaMaterial);
	public DJProductor getDJProductorById(long djId, Integer secuenciaProductor);
	public DJExportador getDJExportadorById(long djId, Integer secuenciaExportador);
	public UsuarioFormato getUsuarioFormatoById(long ordenId, Integer mto);
	public Solicitud getSolicitudById(long ordenId, Integer mto);
	public Solicitante getSolicitanteDetail(long ordenId, int mto, int usuarioFormatoTipo);
	
	public DJMaterial convertMapToDJMaterial(Map<String, Object> map);
	public MessageList insertDJMaterial (DJMaterial obj);
	public MessageList updateDJMaterial (DJMaterial obj);
	public MessageList deleteMaterial(DJMaterial obj);
	public DJProductor convertMapToDJProductor(Map<String, Object> map);
	public MessageList insertDJProductor (DJProductor obj);
	public MessageList updateDJProductor (DJProductor obj);
	public MessageList deleteProductor(DJProductor obj);
	public DJExportador convertMapToDJExportador(Map<String, Object> map);
	public MessageList insertDJExportador (DJExportador obj);
	public MessageList updateDJExportador (DJExportador obj);
	public MessageList deleteExportador(DJExportador obj);
	
	public MessageList actualizarSeleccionEsExportador(DJ obj);
	
	public AdjuntoRequeridoDJ getAdjuntoRequeridoById(Integer adjuntoRequeridoDJ);
	
	public MessageList uploadFile(Long ordenId, Integer mto, Integer adjuntoRequerido, String nombre, byte [] bytes);
	
	public MessageList updateRepresentante(long solicitante,long ordenId, int mto, long representante);
	
	public MessageList updateCargoDeclarante(long solicitante,long ordenId, int mto, String cargo);
	
	public Integer getAdjuntoRequeridoCount(long ordenId,int mto);
	
	public void modificarRol(long djId, int tipoUsuarioDj);
	
}
