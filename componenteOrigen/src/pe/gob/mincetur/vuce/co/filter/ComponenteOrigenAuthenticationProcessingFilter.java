/*
 * VuceAuthenticationProcessingFilter.java
 *
 */

package pe.gob.mincetur.vuce.co.filter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.jlis.core.filter.JLisAuthenticationProcessingFilter;
import org.jlis.core.util.Constantes;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.context.HttpSessionContextIntegrationFilter;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.providers.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.util.WebUtils;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.sunat.tecnologia.menu.bean.UsuarioBean;

public class ComponenteOrigenAuthenticationProcessingFilter extends JLisAuthenticationProcessingFilter {
    
	/** Read the object from Base64 string. */
    private static Object fromString(String s) throws IOException, ClassNotFoundException {
        byte [] data = Base64.decodeBase64(s.getBytes());
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o  = ois.readObject();
        ois.close();
        return o;
    }
	
	public Authentication attemptAuthentication(HttpServletRequest request) throws AuthenticationException {
		Cookie cookies[] = request.getCookies();
        boolean loginEnabled = false;
        UsuarioCO usuario = null;
        
        if (cookies != null) {
            for(int i = 0; i < cookies.length; i++) {
                System.out.println("cookie:" + cookies[i].getName() + " domain " + cookies[i].getDomain() + " path " + cookies[i].getPath());
                //if (!"SOLID".equals(cookies[i].getName())) {
                if (cookies[i].getName()!=null && !cookies[i].getName().contains("SOLID")) {
                    continue;
                }
                
                loginEnabled = true;
                
                try {
                	UsuarioBean usuarioBean = (UsuarioBean)fromString(cookies[i].getValue());
                    usuario = new UsuarioCO(usuarioBean);
                    WebUtils.setSessionAttribute(request, "usuarioBean", usuarioBean);
                } catch (IOException e) {
                	e.printStackTrace();
                } catch (ClassNotFoundException e) {
                	e.printStackTrace();
                }
                break;
            }
        }
        
        if (loginEnabled) {
        	GrantedAuthority [] grantedAuthorities = { new GrantedAuthorityImpl("ROLE_USER") };
        	Authentication aut = new PreAuthenticatedAuthenticationToken(usuario, null, grantedAuthorities);
        	aut.setAuthenticated(true);
        	return aut;
        }
    	return super.attemptAuthentication(request);
	}
	
    protected void onSuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException {
        UsuarioBean usuarioBean = (UsuarioBean)WebUtils.getSessionAttribute(request, "usuarioBean");
        
        // Si la autenticacion es de PRODUCCION
        if (usuarioBean!=null) {
            //usuario = new UsuarioCO(usuarioBean);
        	//Map roles = (HashMap)usuarioBean.getMap().get("roles");
            
            UsuarioCO usuario = (UsuarioCO)authResult.getPrincipal();
            WebUtils.setSessionAttribute(request, Constantes.USUARIO, usuario);
            
            //GrantedAuthority [] grantedAuthorities = { new GrantedAuthorityImpl("ROLE_USER") };
            //UserDetails userDetails = new User(usuario.getLogin(), "", true, true, true, true, grantedAuthorities);
            //Authentication auth = new PreAuthenticatedAuthenticationToken(userDetails, "", grantedAuthorities);
            //authResult.setAuthenticated(true);
            SecurityContextHolder.getContext().setAuthentication(authResult);
            WebUtils.setSessionAttribute(request, HttpSessionContextIntegrationFilter.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
            
            super.onSuccessfulAuthentication(request, response, authResult);
        }
        // Si la autenticacion es del Simulador
        else {
        	UsuarioCO usuario = new UsuarioCO();
            
            String tipoLogueo = request.getParameter("tipoLogueo");
        	String tipoUsuario = request.getParameter("tipoUsuario");
        	String rolUsuarioSOL = request.getParameter("rolUsuarioSOL");
        	String [] roles = request.getParameterValues("roles");
        	
        	if (tipoLogueo.equalsIgnoreCase("R")) {
    	        // SIMULANDO USUARIO SOL
    	        usuario.setUsuarioSOL(request.getParameter("usuario"));
    	    	usuario.setNumRUC(request.getParameter("ruc"));
        	} else if (tipoLogueo.equalsIgnoreCase("E")) {
    	        // SIMULANDO USUARIO EXTRANET
    	        usuario.setLogin(request.getParameter("usuario"));
    	        usuario.setNombreCompleto(request.getParameter("nombreCompleto"));
        	} else if (tipoLogueo.equalsIgnoreCase("D")) {
    	        // SIMULANDO USUARIO DNI
    	        usuario.setLogin(request.getParameter("usuario"));
        	}
        	
        	request.getSession().setAttribute("tipoLogueo", tipoLogueo);
        	request.getSession().setAttribute("tipoUsuario", tipoUsuario);
        	request.getSession().setAttribute("rolUsuarioSOL", rolUsuarioSOL);
        	request.getSession().setAttribute("roles", roles);
        	
            super.onSuccessfulAuthentication(usuario, request, response, authResult);
        }
    }
    
    protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
    	super.onUnsuccessfulAuthentication(request, response, failed);
    }
    
}