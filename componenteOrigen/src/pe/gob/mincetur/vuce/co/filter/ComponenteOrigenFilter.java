/*
 * ComponenteOrigenFilter.java
 *
 */

package pe.gob.mincetur.vuce.co.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.NDC;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;

public class ComponenteOrigenFilter implements Filter {
    
    public void init(FilterConfig arg0) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	boolean pop = false;
    	if (request instanceof HttpServletRequest) {
        	/*String modulo = request.getParameter("modulo");
        	if (modulo!=null) {
        		HttpSession session = ((HttpServletRequest)request).getSession();
        		session.setAttribute(ConstantesCO.MODULO_SELECCIONADO, modulo);
        	}
        	*/
        	HttpSession session = ((HttpServletRequest)request).getSession(false);
    		if (session!=null) {
    		    UsuarioCO usuario = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
    		    if (usuario!=null) {
    		    	pop = true;
    		    	NDC.push("USR "+usuario.getNumeroDocumento()+"-"+usuario.getUsuarioSOL());
    		    }
    		}
        }
    	
        chain.doFilter(request, response);
        
        if (pop) {
        	NDC.pop();
        	NDC.remove();
        }
    }

    public void destroy() {
    }
    
}