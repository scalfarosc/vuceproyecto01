package pe.gob.mincetur.vuce.co.filter;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import pe.gob.sunat.framework.security.Coder;
import pe.gob.sunat.tecnologia.menu.bean.UsuarioBean;

public class ComponenteOrigenLoginInterceptor extends HandlerInterceptorAdapter {
    
	// Reescritura de la "authURL" que utiliza SUNAT al momento de validar el ingreso de un usuario a la aplicación destino
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	    UsuarioBean userSession = (UsuarioBean)WebUtils.getSessionAttribute((HttpServletRequest)request, "usuarioBean");
	    
	    if (userSession==null) {
	    	String solid = (String)WebUtils.getSessionAttribute(request, "sunat.solid");
	    	if (solid!=null) {
	    	    userSession = (UsuarioBean)(new Coder()).decodeBase64(solid);
	    	}
	    }
	    
	    if (userSession!=null && userSession.getMap()!=null) {
	    	boolean reemplazarUserSession = false;
    		Map mapa = userSession.getMap();
        	Iterator it = mapa.keySet().iterator();
        	while (!reemplazarUserSession && it.hasNext()) {
        		Object key = it.next();
        		if (key instanceof String && "authURL".equalsIgnoreCase((String)key)) {
        			System.out.println("******************************** URL en Filter: "+((HttpServletRequest)request).getRequestURL());
            		
        			List listAuthURL = (List)userSession.getMap().get(key);
        			for (int i=0; i < listAuthURL.size(); i++) {
        				String value = (String)listAuthURL.get(i);
        				if (value!=null && value.startsWith("https://www.vuce.gob.pe")) {
	        				value = "http"+value.substring(5, value.length());
	        				System.out.println("URL reemplazado a: "+value);
	        				listAuthURL.set(i, value);
	        				reemplazarUserSession = true;
        				}
        			}
 				    userSession.getMap().put(key, listAuthURL);
        		}
        	}
        	
        	if (reemplazarUserSession) {
                WebUtils.setSessionAttribute(request, "usuarioBean", userSession);
                WebUtils.setSessionAttribute(request, "sunat.solid", (new Coder()).encodeBase64(userSession));
        	}
    	}
		return true;
	}
	
}
