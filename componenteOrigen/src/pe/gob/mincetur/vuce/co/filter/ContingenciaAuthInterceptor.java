package pe.gob.mincetur.vuce.co.filter;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import pe.gob.mincetur.vuce.co.logic.ContingenciaAuthLogic;

/**
 * Interceptor para las url de SUNAT
 * @author 
 *
 */
public class ContingenciaAuthInterceptor extends HandlerInterceptorAdapter {
	
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);	
	private final static Integer TIMEOUT = 20000;	
	private ContingenciaAuthLogic logic = null;	
    
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		boolean result = true;
		logger.debug("Contingencia Interceptor ");
    	
		logger.debug("Obteniendo ConsultaRucLogic " + logic);
    	Integer modoConsulta = null;
    	
		// s:SOL, e:Extranet
		String tipoContingencia = request.getParameter("t");
		String logout = request.getParameter("logout");
		
		logger.debug("Url " + ((HttpServletRequest)request).getRequestURL());
		logger.debug("Url " + tipoContingencia);
		logger.debug("Url " + logout);
		System.out.println("******************************** URL en Filter: "+((HttpServletRequest)request).getRequestURL());

		// Si estamos en el caso de un logout, no hacemos absolutamente nada y dejamos salir
		if (logout != null) {
			logger.debug("Estamos en logout " + logout);
			request.getSession().invalidate();
			result = true;
		} else { // Estamos entrando
			logger.debug("Estamos en login " + logout);
	    	try {
				modoConsulta = logic.obtenerModoOperacion();
				logger.debug("Modo Consulta = " + modoConsulta);
			} catch (Exception e1) {
				e1.printStackTrace();
				logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			}

	    	// Limpiamos la variable del captcha
			request.getSession().removeAttribute("mensajeCaptcha");
	    	Integer componente = 2; // Origen
	    	boolean solDisponible = true;
	    	if (modoConsulta != null) {
	    		// Modo Automatico, solo en este caso tenemos que ir a hacer un ping
	    		if (modoConsulta.intValue() == 1) {
	    			solDisponible = solDisponible(tipoContingencia);
	    			logger.debug("SOL Disponible " + solDisponible);
	    			logic.registraConsulta(componente, "", "", (solDisponible?1:0), modoConsulta);
	    			if (solDisponible) {
	    				result = true;
	    			} else {
	    				String textoIndexContingencia = logic.obtenerMensajeIndexContingencia();
	    				logger.debug("textoIndexContingencia " + textoIndexContingencia);
	    				request.getSession().setAttribute("textoIndexContingencia", textoIndexContingencia);
	        			response.sendRedirect("/co/index_contingencia.jsp?tipoContingencia="+tipoContingencia);
	        			result = false;
	    			}
	    		} else if (modoConsulta.intValue() == 2) { // Modo VUCE
	    			logic.registraConsulta(componente, "", "", 0, modoConsulta);
					String textoIndexContingencia = logic.obtenerMensajeIndexContingencia();
					logger.debug("textoIndexContingencia " + textoIndexContingencia);
					request.getSession().setAttribute("textoIndexContingencia", textoIndexContingencia);    			
	    			response.sendRedirect("/co/index_contingencia.jsp?tipoContingencia="+tipoContingencia);
	    			result = false; 
	    		} else if (modoConsulta.intValue() == 3) { // Modo SUNAT
	    			logic.registraConsulta(componente, "", "", 1, modoConsulta);
	    			result = true; // Puede parecer redundante, pero se mantiene para claridad del codigo
	    		}
	    	}
		}

		return result;
	}

	private boolean solDisponible(String tipoContingencia) {
		try {
			String baseUrl = "https://www.sunat.gob.pe/xTIPO_CONTINGENCIAsecurity/SignOnVerification.htm?signonForwardAction=https%3A%2F%2Fwww.vuce.gob.pe%2Fvuce%2FloginTIPO_CONTINGENCIA.html%3Ft%3Ds";
			String url = baseUrl.replaceAll("TIPO_CONTINGENCIA", tipoContingencia);
			//String url = "http://cualquiercochinadamariano.com";
			logger.debug("TESTEANDO URL: " + url);
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setConnectTimeout(TIMEOUT);
			connection.setReadTimeout(TIMEOUT);
			connection.setRequestMethod("HEAD");
			
			int responseCode = connection.getResponseCode();
			connection.disconnect();
			logger.debug("TESTEANDO URL SUNAT: " + responseCode);
			
			return (200 <= responseCode && responseCode <= 399);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void setLogic(ContingenciaAuthLogic logic) {
		this.logic = logic;
	}
	
}
