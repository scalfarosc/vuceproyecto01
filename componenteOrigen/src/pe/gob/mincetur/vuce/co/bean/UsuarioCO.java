package pe.gob.mincetur.vuce.co.bean;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jlis.core.bean.Option;
import org.jlis.core.bean.Usuario;
import org.jlis.core.list.OptionList;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;

import pe.gob.sunat.tecnologia.menu.bean.UsuarioBean;

public class UsuarioCO extends Usuario {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
    
    private Integer idUsuario;
    
    private String numeroDocumento; // Guarda el numero de documento ya sea RUC (en el caso SOL), DNI (en el caso DNI) y Login (en el caso Extranet)
    
    private int tipoDocumento; // Guarda el tipo de documento
    
    private String tipoOrigen; // Si viene de Clave SOL, Extranet o DNI
    
    private Integer tipoDocumentoUS;
    
    private String numeroDocumentoUS;
    
    private String cargo;
    
    private boolean principal;
    
    private int tipoUsuario;
    
    private String area;
    
    private String subArea;

    private Integer idEntidad;
    
    private String tipoPersona; // Cargado desde el web service de Consulta RUC. 1=Natural, 2=Juridica
    
    private String tipoDocumentoSUNAT;
    
    private boolean cambioRoles;
    
    private String ingresoMR;
    
    private String ingresoUO;
    
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public int getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(int tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Integer getTipoDocumentoUS() {
		return tipoDocumentoUS;
	}

	public void setTipoDocumentoUS(Integer tipoDocumentoUS) {
		this.tipoDocumentoUS = tipoDocumentoUS;
	}

	public String getNumeroDocumentoUS() {
		return numeroDocumentoUS;
	}

	public void setNumeroDocumentoUS(String numeroDocumentoUS) {
		this.numeroDocumentoUS = numeroDocumentoUS;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getTipoOrigen() {
		return tipoOrigen;
	}

	public void setTipoOrigen(String tipoOrigen) {
		this.tipoOrigen = tipoOrigen;
	}

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	public int getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(int tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSubArea() {
        return subArea;
    }

    public void setSubArea(String subArea) {
        this.subArea = subArea;
    }

    public Integer getIdEntidad() {
		return idEntidad;
	}

	public void setIdEntidad(Integer idEntidad) {
		this.idEntidad = idEntidad;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTipoDocumentoSUNAT() {
		return tipoDocumentoSUNAT;
	}

	public void setTipoDocumentoSUNAT(String tipoDocumentoSUNAT) {
		this.tipoDocumentoSUNAT = tipoDocumentoSUNAT;
	}

	public boolean isCambioRoles() {
		return cambioRoles;
	}

	public void setCambioRoles(boolean cambioRoles) {
		this.cambioRoles = cambioRoles;
	}

	public String getIngresoMR() {
		return ingresoMR;
	}

	public void setIngresoMR(String ingresoMR) {
		this.ingresoMR = ingresoMR;
	}

	public String getIngresoUO() {
		return ingresoUO;
	}

	public void setIngresoUO(String ingresoUO) {
		this.ingresoUO = ingresoUO;
	}

	public UsuarioCO() {
    }
    
    public UsuarioCO(UsuarioBean usuarioBean) {
    	if (usuarioBean!=null) {
	    	this.setApePaterno(usuarioBean.getApePaterno());
	    	this.setApeMaterno(usuarioBean.getApeMaterno());
	    	this.setCodCate(usuarioBean.getCodCate());
	    	this.setCodDepend(usuarioBean.getCodDepend());
	    	this.setCodTOpeComer(usuarioBean.getCodTOpeComer());
	    	this.setCodUO(usuarioBean.getCodUO());
	    	this.setCorreo(usuarioBean.getCorreo());
	    	this.setDesCate(usuarioBean.getDesCate());
	    	this.setDesUO(usuarioBean.getDesUO());
	    	this.setId(usuarioBean.getId());
	    	this.setIdCelular(usuarioBean.getIdCelular());
	    	this.setLogin(usuarioBean.getLogin());
	    	this.setNivelUO(usuarioBean.getNivelUO());
	    	this.setNombreCompleto(usuarioBean.getNombreCompleto());
	    	this.setNombres(usuarioBean.getNombres());
	    	this.setNroRegistro(usuarioBean.getNroRegistro());
	    	this.setNumRUC(usuarioBean.getNumRUC());
	    	this.setTicket(usuarioBean.getTicket());
	    	this.setUsuarioSOL(usuarioBean.getUsuarioSOL());
	    	this.setMap(usuarioBean.getMap());
    	}
    }
    
    public void imprimir() {
    	System.out.println("************************************");
    	System.out.println("Apellido Paterno: "+this.getApePaterno());
    	System.out.println("Apellido Materno: "+this.getApeMaterno());
    	System.out.println("Clave: "+this.getClave());
    	System.out.println("Cod Cate: "+this.getCodCate());
    	System.out.println("Cod Depend: "+this.getCodDepend());
    	System.out.println("Cod TOpeComer: "+this.getCodTOpeComer());
    	System.out.println("Cod UO: "+this.getCodUO());
    	System.out.println("Correo: "+this.getCorreo());
    	System.out.println("Des Cate: "+this.getDesCate());
    	System.out.println("Descripcion: "+this.getDescripcion());
    	System.out.println("Des UO: "+this.getDesUO());
    	System.out.println("Id (DEPRECADO): "+this.getId());
    	System.out.println("Id Celular: "+this.getIdCelular());
    	System.out.println("Id Usuario: "+this.getIdUsuario());
    	System.out.println("Login: "+this.getLogin());
    	System.out.println("Nivel UO: "+this.getNivelUO());
    	System.out.println("Nombre Completo: "+this.getNombreCompleto());
    	System.out.println("Nombres: "+this.getNombres());
    	System.out.println("Nro Registro: "+this.getNroRegistro());
    	System.out.println("Num RUC: "+this.getNumRUC());
    	System.out.println("Numero Documento: "+this.getNumeroDocumento());
    	System.out.println("Ticket: "+this.getTicket());
    	System.out.println("Usuario SOL: "+this.getUsuarioSOL());
    	System.out.println("Visibilidad: "+this.getVisibilidad());
    	System.out.println("Cambio Roles?: "+this.isCambioRoles());
    	System.out.println("--------------- MAPA ---------------");
    	Map mapa = this.getMap();
    	Iterator it = mapa.keySet().iterator();
    	while (it.hasNext()) {
    		Object key = it.next();
    		System.out.println(key+" : "+this.getMap().get(key));
    	}
    	System.out.println("-------------- ROLES ---------------");
    	this.getRoles().imprimir();
    	System.out.println("************************************");
    }
    
    public OptionList getListaRolesSeleccionables() {
    	OptionList lista = new OptionList();
        HashUtil roles = this.getRoles();
        Enumeration en = roles.keys();
        while (en.hasMoreElements()) {
            String rol = (String)en.nextElement();
            Option op = new Option(rol, rol);
            lista.add(op);
        }
        return lista;
    }
    
    public String getListaRoles() {
        String lista = "";
        HashUtil roles = this.getRoles();
        Enumeration en = roles.keys();
        while (en.hasMoreElements()) {
            String rol = (String)en.nextElement();
            lista += rol;
            if (en.hasMoreElements()) {
                lista += ", ";
            }
        }
        return lista;
    }
    
}
