package pe.gob.mincetur.vuce.co.bean;

import java.util.ArrayList;
import java.util.List;

public class ErroresEmpresa {

	private String template;
	
	private List<ErroresEstructura> estructuras = new ArrayList<ErroresEstructura>();	
	
	private List<ErroresDatos> datos = new ArrayList<ErroresDatos>();;
	
    public ErroresEmpresa(){
    	this.setTemplate("mensajesTransmision/ErroresEmpresa.vm");
    }

    public ErroresEmpresa(String template){
    	this.template = template;
    }
    
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}

	public List<ErroresEstructura> getEstructuras() {
		return estructuras;
	}

	public void setEstructuras(List<ErroresEstructura> estructuras) {
		this.estructuras = estructuras;
	}
	
	public void addEstructuras (ErroresEstructura estructuras) {
		this.estructuras.add(estructuras);
	}
	
	public List<ErroresDatos> getDatos() {
		return datos;
	}

	public void setDatos(List<ErroresDatos> datos) {
		this.datos = datos;
	}
	
	public void addDatos (ErroresDatos Datos) {
		this.datos.add(Datos);
	}
    
	
}
