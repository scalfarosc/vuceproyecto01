package pe.gob.mincetur.vuce.co.bean;

public class ErroresEstructura {
	
	private String descripcion;

	public ErroresEstructura(){}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
