package pe.gob.mincetur.vuce.co.bean;


public class EntidadCertificadora {
    
	/*CCLI(14, "CCLI", "A"), // CAMARA DE COMERCIO DE LIMA
	ADEX(15, "ADEX", "A"), // ASOCIACION DE EXPORTADORES
	SNIN(16, "SNIN", "A"), // SOCIEDAD NACIONAL DE INDUSTRIAS
	CLIB(17, "CLIB", "A"), // CAMARA DE COMERCIO Y PRODUCCION DE LA LIBERTAD
	CTAC(18, "CTAC", "A"), // CAMARA DE COMERCIO, INDUSTRIA Y PRODUCCION DE TACNA
	CICA(19, "CICA", "A"), // CAMARA DE COMERCIO, INDUSTRIA Y TURISMO DE ICA
	CPUN(20, "CPUN", "A"), // CAMARA DE COMERCIO Y PRODUCCION DE PUNO
	CLAM(21, "CLAM", "A"), // CAMARA DE COMERCIO Y PRODUCCION DE LAMBAYEQUE
	CLOR(22, "CLOR", "A"), // CAMARA DE COMERCIO, INDUSTRIA Y TURISMO DE LORETO
	CCUZ(23, "CCUZ", "I"), // CAMARA DE COMERCIO, INDUSTRIA, SERVICIOS, TURISMO Y DE LA PRODUCCION DEL CUZCO
	CARE(24, "CARE", "A"), // CAMARA DE COMERCIO E INDUSTRIA DE AREQUIPA
	CAGV(25, "CAGV", "A"), // CAMARA DE COMERCIO Y PRODUCCION DE AGUAS VERDES
	CSUL(26, "CSUL", "A"), // CAMARA DE COMERCIO, PRODUCCION Y TURISMO DE SULLANA
	CPIU(27, "CPIU", "A"), // CAMARA DE COMERCIO Y PRODUCCION DE PIURA
	CILO(28, "CILO", "A"), // CAMARA DE COMERCIO E INDUSTRIA DE ILO
	CSAN(29, "CSAN", "I"), // CAMARA DE COMERCIO Y PRODUCCION DEL SANTA
	CCHI(30, "CCHI", "A"), // CAMARA CHINCHANA DE COMERCIO, INDUSTRIA, TURISMO, SERVICIO Y AGRICULTURA
	CMOQ(31, "CMOQ", "I"), // CAMARA DE COMERCIO E INDUSTRIA DE MOQUEGUA
	CCSM(32, "CCSM", "I"), // CAMARA DE COMERCIO, PRODUCCION Y TURISMO DE SAN MARTIN
	CUCA(33, "CUCA", "I"), // CAMARA DE COMERCIO, INDUSTRIA Y TURISMO DE UCAYALI
	CPAI(34, "CPAI", "A"), // CAMARA DE PRODUCCION, COMERCIO TURISMO Y SERVICIOS DE PAITA
	CHYO(39, "CHYO", "A"), // CAMARA DE COMERCIO DE HUANCAYO
	CCAJ(40, "CCAJ", "A"), // CAMARA DE COMERCIO DE CAJAMARCA
	CBAG(54, "CBAG", "A"), // CAMARA DE COMERCIO, AGRICULTURA E INDUSTRIA DE BAGUA - AMAZONAS
	CAYO(88, "CAYO", "A"), // C�MARA DE COMERCIO, INDUSTRIA Y TURISMO DE AYACUCHO
	COMX(89, "COMX", "A"); // COMEXPER� - SOCIEDAD DE COMERCIO EXTERIOR DEL PER�
	*/
	private Integer id;

    private String codigo;
    
    private String estado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

    /*private EntidadCertificadora(int id, String codigo, String estado) {
        this.id=id;
        this.codigo = codigo;
        this.estado = estado;
    }
    */
    
    
    /*public static EntidadCertificadora validarExisteCodigoEntidadCertificadora(String cadena) {
    	EntidadCertificadora [] entidades = values();
        for (EntidadCertificadora entidad : entidades) {
            if (cadena.startsWith(entidad.getCodigo())) {
                return entidad;
            }
        }
        return null;
    }
    
    public static EntidadCertificadora getEntidadByCodigo(String codigo) {
    	EntidadCertificadora [] entidades = values();
        for (EntidadCertificadora entidad : entidades) {
            if (entidad.getCodigo().equals(codigo)) {
                return entidad;
            }
        }
        return null;
    }
    */
}
