package pe.gob.mincetur.vuce.co.bean;

public class ErroresDatos {
	
	private String codigo;
	
	private String campo;
	
	private String descripcion;
	
	public ErroresDatos(){}	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
