package pe.gob.mincetur.vuce.co.domain.firma;

public class BienOrigen {

	private String orden;
	private String ordenFactura;
	private String codigoItem;
	private String nombreItem;
	private String pesoItem;
	private String medidaItem;
	//private String rvcItem;
	private String reglasOrigenItem;
	private String otraInstanciaItem;
	private String fechaDeclaracion;
	private String cantidadMedida;//se depreca
	private String unidadMedida;
	private String valorContenidoRegional;
	private String ordenProductor;
	
	/*
	public String getCantidadMedida() {
		return cantidadMedida;
	}
	public void setCantidadMedida(String cantidadMedida) {
		this.cantidadMedida = cantidadMedida;
		String [] pieces = cantidadMedida.split(" ");
		if (pieces != null && pieces.length == 2) {
			this.pesoItem = pieces[0];
			this.medidaItem = pieces[1];
		}
	}
	*/
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public String getOrdenFactura() {
		return ordenFactura;
	}
	public void setOrdenFactura(String ordenFactura) {
		this.ordenFactura = ordenFactura;
	}
	public String getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}
	public String getNombreItem() {
		return nombreItem;
	}
	public void setNombreItem(String nombreItem) {
		this.nombreItem = nombreItem;
	}
	public String getPesoItem() {
		return pesoItem;
	}
	public void setPesoItem(String pesoItem) {
		this.pesoItem = pesoItem;
	}
	public String getMedidaItem() {
		return medidaItem;
	}
	public void setMedidaItem(String medidaItem) {
		this.medidaItem = medidaItem;
	}
	public String getReglasOrigenItem() {
		return reglasOrigenItem;
	}
	public void setReglasOrigenItem(String reglasOrigenItem) {
		this.reglasOrigenItem = reglasOrigenItem;
	}
	public String getOtraInstanciaItem() {
		return otraInstanciaItem;
	}
	public void setOtraInstanciaItem(String otraInstanciaItem) {
		this.otraInstanciaItem = otraInstanciaItem;
	}
	public String getFechaDeclaracion() {
		return fechaDeclaracion;
	}
	public void setFechaDeclaracion(String fechaDeclaracion) {
		this.fechaDeclaracion = fechaDeclaracion;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getValorContenidoRegional() {
		return valorContenidoRegional;
	}
	public void setValorContenidoRegional(String valorContenidoRegional) {
		this.valorContenidoRegional = valorContenidoRegional;
	}
	public String getOrdenProductor() {
		return ordenProductor;
	}
	public void setOrdenProductor(String ordenProductor) {
		this.ordenProductor = ordenProductor;
	}
	public String getCantidadMedida() {
		return cantidadMedida;
	}
	public void setCantidadMedida(String cantidadMedida) {
		this.cantidadMedida = cantidadMedida;
	}
	
}
