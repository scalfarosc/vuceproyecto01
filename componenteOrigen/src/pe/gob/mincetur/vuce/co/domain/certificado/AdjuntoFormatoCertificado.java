package pe.gob.mincetur.vuce.co.domain.certificado;

import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;

public class AdjuntoFormatoCertificado extends AdjuntoFormato {

    private Integer certificadoFormatoId;
    
	public Integer getCertificadoFormatoId() {
		return certificadoFormatoId;
	}

	public void setCertificadoFormatoId(Integer certificadoFormatoId) {
		this.certificadoFormatoId = certificadoFormatoId;
	}

}
