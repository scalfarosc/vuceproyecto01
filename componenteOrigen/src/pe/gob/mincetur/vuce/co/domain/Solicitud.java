package pe.gob.mincetur.vuce.co.domain;

import java.util.Date;

public class Solicitud {
	private Long            ordenId;
	private Long            orden;
	private Integer         mto;
	private Long            suceId;
	private Long            suce;
	private Date            fechaRegistro;
	private Date            fechaActualizacion;
	private Integer         ano;
	private String          vigente;
	private String          bloqueado;
	private String          transmitido;
	private String          estadoRegistro;
	private Date            fechaCaducidad;
	private String          anulado;
	private Integer         entidadId;
	private String          declaracionJurada;
	private String          estadoRegistroSUCE;
	
	public Long getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}
	public Long getOrden() {
		return orden;
	}
	public void setOrden(Long orden) {
		this.orden = orden;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public Long getSuceId() {
		return suceId;
	}
	public void setSuceId(Long suceId) {
		this.suceId = suceId;
	}
	public Long getSuce() {
		return suce;
	}
	public void setSuce(Long suce) {
		this.suce = suce;
	}
	public String getVigente() {
		return vigente;
	}
	public void setVigente(String vigente) {
		this.vigente = vigente;
	}
	public String getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}
	public String getTransmitido() {
		return transmitido;
	}
	public void setTransmitido(String transmitido) {
		this.transmitido = transmitido;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public Date getFechaCaducidad() {
		return fechaCaducidad;
	}
	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
	public String getAnulado() {
		return anulado;
	}
	public void setAnulado(String anulado) {
		this.anulado = anulado;
	}
	public Integer getEntidadId() {
		return entidadId;
	}
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}
	public String getDeclaracionJurada() {
		return declaracionJurada;
	}
	public void setDeclaracionJurada(String declaracionJurada) {
		this.declaracionJurada = declaracionJurada;
	}
	public String getEstadoRegistroSUCE() {
		return estadoRegistroSUCE;
	}
	public void setEstadoRegistroSUCE(String estadoRegistroSUCE) {
		this.estadoRegistroSUCE = estadoRegistroSUCE;
	}

}
