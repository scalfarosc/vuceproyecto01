package pe.gob.mincetur.vuce.co.domain.dj;

import java.math.BigDecimal;

public class DJMaterial {
	
	private Long            djId;
	private Integer         secuenciaMaterial;
	private Integer         item;
	private String          descripcion;
	private String          fabricanteNombre;
	private Integer         fabricanteTipoDocumento;
	private String          fabricanteNumeroDocumento;
	private BigDecimal      cantidad;
	private Long            partidaArancelaria;
	private Integer         paisProcedencia;
	private Integer         paisOrigen;
	private Integer         umFisicaId;
	private BigDecimal      porcentajeValorFob;
	private BigDecimal      porcentajeValorExwork;
	private BigDecimal      valorFobUS;
	private BigDecimal      valorExWorkUS;
	
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public Integer getSecuenciaMaterial() {
		return secuenciaMaterial;
	}
	public void setSecuenciaMaterial(Integer secuenciaMaterial) {
		this.secuenciaMaterial = secuenciaMaterial;
	}
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFabricanteNombre() {
		return fabricanteNombre;
	}
	public void setFabricanteNombre(String fabricanteNombre) {
		this.fabricanteNombre = fabricanteNombre;
	}
	public Integer getFabricanteTipoDocumento() {
		return fabricanteTipoDocumento;
	}
	public void setFabricanteTipoDocumento(Integer fabricanteTipoDocumento) {
		this.fabricanteTipoDocumento = fabricanteTipoDocumento;
	}
	public String getFabricanteNumeroDocumento() {
		return fabricanteNumeroDocumento;
	}
	public void setFabricanteNumeroDocumento(String fabricanteNumeroDocumento) {
		this.fabricanteNumeroDocumento = fabricanteNumeroDocumento;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public Long getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Long partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public Integer getPaisProcedencia() {
		return paisProcedencia;
	}
	public void setPaisProcedencia(Integer paisProcedencia) {
		this.paisProcedencia = paisProcedencia;
	}
	
	public Integer getPaisOrigen() {
		return paisOrigen;
	}
	public void setPaisOrigen(Integer paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public BigDecimal getPorcentajeValorFob() {
		return porcentajeValorFob;
	}
	public void setPorcentajeValorFob(BigDecimal porcentajeValorFob) {
		this.porcentajeValorFob = porcentajeValorFob;
	}
	public BigDecimal getPorcentajeValorExwork() {
		return porcentajeValorExwork;
	}
	public void setPorcentajeValorExwork(BigDecimal porcentajeValorExwork) {
		this.porcentajeValorExwork = porcentajeValorExwork;
	}
	public BigDecimal getValorFobUS() {
		return valorFobUS;
	}
	public void setValorFobUS(BigDecimal valorFobUS) {
		this.valorFobUS = valorFobUS;
	}
	public BigDecimal getValorExWorkUS() {
		return valorExWorkUS;
	}
	public void setValorExWorkUS(BigDecimal valorExWorkUS) {
		this.valorExWorkUS = valorExWorkUS;
	}
	
}
