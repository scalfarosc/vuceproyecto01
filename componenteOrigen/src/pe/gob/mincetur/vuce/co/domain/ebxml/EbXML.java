package pe.gob.mincetur.vuce.co.domain.ebxml;

import java.util.List;

import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;

public abstract class EbXML {

	private String template;
	
	private List<AdjuntoFormato> adjuntos;

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public List<AdjuntoFormato> getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(List<AdjuntoFormato> adjuntos) {
		this.adjuntos = adjuntos;
	}
	
}
