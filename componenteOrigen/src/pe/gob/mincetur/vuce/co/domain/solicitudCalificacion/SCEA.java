package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

import java.math.BigDecimal;
import java.util.Date;


public class SCEA {
	
	private Long expautId;
	private Long ordenId;
	private Integer mto;
	private String indNuevo;
	private String indRenovacion;
	private String personaTipo;
	private String nombreExpo;
	private String domicilioExpo;
	private String telefonoExpo;
	private String faxExpo;
	private String emailExpo;
	private String rucExpo;
	private String nombreRep;
	private String dniRep;
	private String ceRep;
	private String indDomicilioRep; ////////////////////////
	private String domicilioRepLegal;
	private String domicilioRepFiscal;
	private String indNotificaCorreo;
	private String indOtroEstablecimiento;
	private String domicilioOtroEstablecimiento;
	private String indRepOtrosPaises;
	private String domicilioOtroPais;
	private Integer anoFacturaFiscal;
	private BigDecimal montoMercadoNacional;
	private BigDecimal montoMercadoInternacional;
	private BigDecimal montoTotalAnual;
	private String nombreAcuerdoComercial;
	private String indVerificacionAbiertaExpo;
	private String verificacionAbiertaExpo;
	private String indSancionExpo;
	private String sancionExpo;
	private String indAuditadaExpo;
	private String autoridadInforme;
	private Date fechaComunicacionInforme;
	private String mercanciaVerficadaInforme;
	private String clasifArancelariaInforme;
	private String mercadoDestinoInforme;
	private String resultadoVerificacion;
	private String indRegAdquisicionExpprod;
	private String indAcreditaOrigenExpprod;
	private String indProcesoProduccionExpprod;
	private String indDocComercialesExpprod;
	private String indPlazoMinimoExpprod;
	private String indRegAdquisicionExp;
	private String indAcreditaOrigenExp;
	private String indPlazoMinimoExp;
	private String indSonAlmacenados;
	private String indDisposicionTransporte;
	private String disposicionTransporte;
	
	public Long getExpautId() {
		return expautId;
	}
	public void setExpautId(Long expautId) {
		this.expautId = expautId;
	}
	public Long getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public String getIndNuevo() {
		return indNuevo;
	}
	public void setIndNuevo(String indNuevo) {
		this.indNuevo = indNuevo;
	}
	public String getIndRenovacion() {
		return indRenovacion;
	}
	public void setIndRenovacion(String indRenovacion) {
		this.indRenovacion = indRenovacion;
	}
	public String getPersonaTipo() {
		return personaTipo;
	}
	public void setPersonaTipo(String personaTipo) {
		this.personaTipo = personaTipo;
	}
	public String getNombreExpo() {
		return nombreExpo;
	}
	public void setNombreExpo(String nombreExpo) {
		this.nombreExpo = nombreExpo;
	}
	public String getDomicilioExpo() {
		return domicilioExpo;
	}
	public void setDomicilioExpo(String domicilioExpo) {
		this.domicilioExpo = domicilioExpo;
	}
	public String getTelefonoExpo() {
		return telefonoExpo;
	}
	public void setTelefonoExpo(String telefonoExpo) {
		this.telefonoExpo = telefonoExpo;
	}
	public String getFaxExpo() {
		return faxExpo;
	}
	public void setFaxExpo(String faxExpo) {
		this.faxExpo = faxExpo;
	}
	public String getEmailExpo() {
		return emailExpo;
	}
	public void setEmailExpo(String emailExpo) {
		this.emailExpo = emailExpo;
	}
	public String getRucExpo() {
		return rucExpo;
	}
	public void setRucExpo(String rucExpo) {
		this.rucExpo = rucExpo;
	}
	public String getNombreRep() {
		return nombreRep;
	}
	public void setNombreRep(String nombreRep) {
		this.nombreRep = nombreRep;
	}
	public String getDniRep() {
		return dniRep;
	}
	public void setDniRep(String dniRep) {
		this.dniRep = dniRep;
	}
	public String getCeRep() {
		return ceRep;
	}
	public void setCeRep(String ceRep) {
		this.ceRep = ceRep;
	}
	public String getIndDomicilioRep() {
		return indDomicilioRep;
	}
	public void setIndDomicilioRep(String indDomicilioRep) {
		this.indDomicilioRep = indDomicilioRep;
	}
	public String getDomicilioRepLegal() {
		return domicilioRepLegal;
	}
	public void setDomicilioRepLegal(String domicilioRepLegal) {
		this.domicilioRepLegal = domicilioRepLegal;
	}
	public String getDomicilioRepFiscal() {
		return domicilioRepFiscal;
	}
	public void setDomicilioRepFiscal(String domicilioRepFiscal) {
		this.domicilioRepFiscal = domicilioRepFiscal;
	}
	public String getIndNotificaCorreo() {
		return indNotificaCorreo;
	}
	public void setIndNotificaCorreo(String indNotificaCorreo) {
		this.indNotificaCorreo = indNotificaCorreo;
	}
	public String getIndOtroEstablecimiento() {
		return indOtroEstablecimiento;
	}
	public void setIndOtroEstablecimiento(String indOtroEstablecimiento) {
		this.indOtroEstablecimiento = indOtroEstablecimiento;
	}
	public String getDomicilioOtroEstablecimiento() {
		return domicilioOtroEstablecimiento;
	}
	public void setDomicilioOtroEstablecimiento(String domicilioOtroEstablecimiento) {
		this.domicilioOtroEstablecimiento = domicilioOtroEstablecimiento;
	}
	public String getIndRepOtrosPaises() {
		return indRepOtrosPaises;
	}
	public void setIndRepOtrosPaises(String indRepOtrosPaises) {
		this.indRepOtrosPaises = indRepOtrosPaises;
	}
	public String getDomicilioOtroPais() {
		return domicilioOtroPais;
	}
	public void setDomicilioOtroPais(String domicilioOtroPais) {
		this.domicilioOtroPais = domicilioOtroPais;
	}
	public Integer getAnoFacturaFiscal() {
		return anoFacturaFiscal;
	}
	public void setAnoFacturaFiscal(Integer anoFacturaFiscal) {
		this.anoFacturaFiscal = anoFacturaFiscal;
	}
	public BigDecimal getMontoMercadoNacional() {
		return montoMercadoNacional;
	}
	public void setMontoMercadoNacional(BigDecimal montoMercadoNacional) {
		this.montoMercadoNacional = montoMercadoNacional;
	}
	public BigDecimal getMontoMercadoInternacional() {
		return montoMercadoInternacional;
	}
	public void setMontoMercadoInternacional(BigDecimal montoMercadoInternacional) {
		this.montoMercadoInternacional = montoMercadoInternacional;
	}
	public BigDecimal getMontoTotalAnual() {
		return montoTotalAnual;
	}
	public void setMontoTotalAnual(BigDecimal montoTotalAnual) {
		this.montoTotalAnual = montoTotalAnual;
	}
	public String getNombreAcuerdoComercial() {
		return nombreAcuerdoComercial;
	}
	public void setNombreAcuerdoComercial(String nombreAcuerdoComercial) {
		this.nombreAcuerdoComercial = nombreAcuerdoComercial;
	}
	public String getIndVerificacionAbiertaExpo() {
		return indVerificacionAbiertaExpo;
	}
	public void setIndVerificacionAbiertaExpo(String indVerificacionAbiertaExpo) {
		this.indVerificacionAbiertaExpo = indVerificacionAbiertaExpo;
	}
	public String getVerificacionAbiertaExpo() {
		return verificacionAbiertaExpo;
	}
	public void setVerificacionAbiertaExpo(String verificacionAbiertaExpo) {
		this.verificacionAbiertaExpo = verificacionAbiertaExpo;
	}
	public String getIndSancionExpo() {
		return indSancionExpo;
	}
	public void setIndSancionExpo(String indSancionExpo) {
		this.indSancionExpo = indSancionExpo;
	}
	public String getSancionExpo() {
		return sancionExpo;
	}
	public void setSancionExpo(String sancionExpo) {
		this.sancionExpo = sancionExpo;
	}
	public String getIndAuditadaExpo() {
		return indAuditadaExpo;
	}
	public void setIndAuditadaExpo(String indAuditadaExpo) {
		this.indAuditadaExpo = indAuditadaExpo;
	}
	public String getAutoridadInforme() {
		return autoridadInforme;
	}
	public void setAutoridadInforme(String autoridadInforme) {
		this.autoridadInforme = autoridadInforme;
	}
	public Date getFechaComunicacionInforme() {
		return fechaComunicacionInforme;
	}
	public void setFechaComunicacionInforme(Date fechaComunicacionInforme) {
		this.fechaComunicacionInforme = fechaComunicacionInforme;
	}
	public String getMercanciaVerficadaInforme() {
		return mercanciaVerficadaInforme;
	}
	public void setMercanciaVerficadaInforme(String mercanciaVerficadaInforme) {
		this.mercanciaVerficadaInforme = mercanciaVerficadaInforme;
	}
	public String getClasifArancelariaInforme() {
		return clasifArancelariaInforme;
	}
	public void setClasifArancelariaInforme(String clasifArancelariaInforme) {
		this.clasifArancelariaInforme = clasifArancelariaInforme;
	}
	public String getMercadoDestinoInforme() {
		return mercadoDestinoInforme;
	}
	public void setMercadoDestinoInforme(String mercadoDestinoInforme) {
		this.mercadoDestinoInforme = mercadoDestinoInforme;
	}
	public String getResultadoVerificacion() {
		return resultadoVerificacion;
	}
	public void setResultadoVerificacion(String resultadoVerificacion) {
		this.resultadoVerificacion = resultadoVerificacion;
	}
	public String getIndRegAdquisicionExpprod() {
		return indRegAdquisicionExpprod;
	}
	public void setIndRegAdquisicionExpprod(String indRegAdquisicionExpprod) {
		this.indRegAdquisicionExpprod = indRegAdquisicionExpprod;
	}
	public String getIndAcreditaOrigenExpprod() {
		return indAcreditaOrigenExpprod;
	}
	public void setIndAcreditaOrigenExpprod(String indAcreditaOrigenExpprod) {
		this.indAcreditaOrigenExpprod = indAcreditaOrigenExpprod;
	}
	public String getIndProcesoProduccionExpprod() {
		return indProcesoProduccionExpprod;
	}
	public void setIndProcesoProduccionExpprod(String indProcesoProduccionExpprod) {
		this.indProcesoProduccionExpprod = indProcesoProduccionExpprod;
	}
	public String getIndDocComercialesExpprod() {
		return indDocComercialesExpprod;
	}
	public void setIndDocComercialesExpprod(String indDocComercialesExpprod) {
		this.indDocComercialesExpprod = indDocComercialesExpprod;
	}
	public String getIndPlazoMinimoExpprod() {
		return indPlazoMinimoExpprod;
	}
	public void setIndPlazoMinimoExpprod(String indPlazoMinimoExpprod) {
		this.indPlazoMinimoExpprod = indPlazoMinimoExpprod;
	}
	public String getIndRegAdquisicionExp() {
		return indRegAdquisicionExp;
	}
	public void setIndRegAdquisicionExp(String indRegAdquisicionExp) {
		this.indRegAdquisicionExp = indRegAdquisicionExp;
	}
	public String getIndAcreditaOrigenExp() {
		return indAcreditaOrigenExp;
	}
	public void setIndAcreditaOrigenExp(String indAcreditaOrigenExp) {
		this.indAcreditaOrigenExp = indAcreditaOrigenExp;
	}
	public String getIndPlazoMinimoExp() {
		return indPlazoMinimoExp;
	}
	public void setIndPlazoMinimoExp(String indPlazoMinimoExp) {
		this.indPlazoMinimoExp = indPlazoMinimoExp;
	}
	public String getIndSonAlmacenados() {
		return indSonAlmacenados;
	}
	public void setIndSonAlmacenados(String indSonAlmacenados) {
		this.indSonAlmacenados = indSonAlmacenados;
	}
	public String getIndDisposicionTransporte() {
		return indDisposicionTransporte;
	}
	public void setIndDisposicionTransporte(String indDisposicionTransporte) {
		this.indDisposicionTransporte = indDisposicionTransporte;
	}
	public String getDisposicionTransporte() {
		return disposicionTransporte;
	}
	public void setDisposicionTransporte(String disposicionTransporte) {
		this.disposicionTransporte = disposicionTransporte;
	}
	
}
