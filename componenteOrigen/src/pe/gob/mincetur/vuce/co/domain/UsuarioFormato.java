package pe.gob.mincetur.vuce.co.domain;

public class UsuarioFormato {
	private Integer         usuarioFormatoTipo;
	private Long            usuarioId;
	private Long            ordenId;
	private Integer         mto;
	private Integer         documentoTipo;
	private String          numeroDocumento;
	private String          nombre;
	private String          email;
	private String          telefono;
	private String          fax;
	private String          direccion;
	private Integer         distritoId;
	private String          referencia;
	private String          actividad;
	private String          celular;
	private Integer         representanteLegal;
	private Integer         personaTipo;
	private Long            reasignacionId;
	private String          estado;
	private String          tipoPersona;
	
	public Integer getUsuarioFormatoTipo() {
		return usuarioFormatoTipo;
	}
	public void setUsuarioFormatoTipo(Integer usuarioFormatoTipo) {
		this.usuarioFormatoTipo = usuarioFormatoTipo;
	}
	public Long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public Long getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public Integer getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(Integer documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getDistritoId() {
		return distritoId;
	}
	public void setDistritoId(Integer distritoId) {
		this.distritoId = distritoId;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public Integer getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(Integer representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	public Integer getPersonaTipo() {
		return personaTipo;
	}
	public void setPersonaTipo(Integer personaTipo) {
		this.personaTipo = personaTipo;
	}
	public Long getReasignacionId() {
		return reasignacionId;
	}
	public void setReasignacionId(Long reasignacionId) {
		this.reasignacionId = reasignacionId;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
}
