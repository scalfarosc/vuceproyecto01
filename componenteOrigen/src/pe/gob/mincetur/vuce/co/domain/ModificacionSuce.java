
package pe.gob.mincetur.vuce.co.domain;

import java.util.Date;

public class ModificacionSuce {
    private Integer suce;
    private Integer ano;
    private Integer numSuce;
    private Integer modificacionSuce;
    private Date fechaRegistro;
    private Date fechaActualizacion;
    private String bloqueada;
    private String cerrada;
    private String transmitido;
    private Integer idMensaje;
    private String mensaje;
    private String estadoRegistro;
	private Integer ordenId;
	private Integer mto;
	private String motivoRechazo;
	private Integer tipoModificacionSuce;
    private Integer mensajeIdRechazo;
    
	public Integer getSuce() {
		return suce;
	}
	public void setSuce(Integer suce) {
		this.suce = suce;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Integer getNumSuce() {
		return numSuce;
	}
	public void setNumSuce(Integer numSuce) {
		this.numSuce = numSuce;
	}
	public Integer getModificacionSuce() {
		return modificacionSuce;
	}
	public void setModificacionSuce(Integer modificacionSuce) {
		this.modificacionSuce = modificacionSuce;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public String getBloqueada() {
		return bloqueada;
	}
	public void setBloqueada(String bloqueada) {
		this.bloqueada = bloqueada;
	}
	public String getCerrada() {
		return cerrada;
	}
	public void setCerrada(String cerrada) {
		this.cerrada = cerrada;
	}
	public String getTransmitido() {
		return transmitido;
	}
	public void setTransmitido(String transmitido) {
		this.transmitido = transmitido;
	}
	public Integer getIdMensaje() {
		return idMensaje;
	}
	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public Integer getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Integer ordenId) {
		this.ordenId = ordenId;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public String getMotivoRechazo() {
		return motivoRechazo;
	}
	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	public Integer getTipoModificacionSuce() {
		return tipoModificacionSuce;
	}
	public void setTipoModificacionSuce(Integer tipoModificacionSuce) {
		this.tipoModificacionSuce = tipoModificacionSuce;
	}
    public Integer getMensajeIdRechazo() {
		return mensajeIdRechazo;
	}
	public void setMensajeIdRechazo(Integer mensajeIdRechazo) {
		this.mensajeIdRechazo = mensajeIdRechazo;
	}
    
}
