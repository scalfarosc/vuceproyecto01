package pe.gob.mincetur.vuce.co.domain.ebxml;

import java.util.ArrayList;
import java.util.List;

import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;

public class MCT005EBXML extends SolicitudEBXML {
	
	private DR dr;
	
	private DeclaracionJurada declaracionJurada;
	
	private List<DeclaracionJuradaProductor> listProductores = new ArrayList<DeclaracionJuradaProductor>();

    private CertificadoOrigen certificadoOrigen;

    private CalificacionOrigen calificacion;
    
    private List<DeclaracionJuradaMaterial> listMateriales = new ArrayList<DeclaracionJuradaMaterial>();;
	
	public MCT005EBXML(){
	    super();
	    setTemplate("itp/MCT005.vm");		
	}

	public MCT005EBXML(String template){
	    super();
	    setTemplate(template);		
	}

	public DeclaracionJurada getDeclaracionJurada() {
		return declaracionJurada;
	}

	public void setDeclaracionJurada(DeclaracionJurada declaracionJurada) {
		this.declaracionJurada = declaracionJurada;
	}

	
	public List<DeclaracionJuradaProductor> getListProductores() {
		return listProductores;
	}

	public void setListProductores(List<DeclaracionJuradaProductor> listProductores) {
		this.listProductores = listProductores;
	}

	public CertificadoOrigen getCertificadoOrigen() {
		return certificadoOrigen;
	}

	public void setCertificadoOrigen(CertificadoOrigen certificadoOrigen) {
		this.certificadoOrigen = certificadoOrigen;
	}

	public CalificacionOrigen getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(CalificacionOrigen calificacion) {
		this.calificacion = calificacion;
	}

	public List<DeclaracionJuradaMaterial> getListMateriales() {
		return listMateriales;
	}

	public void setListMateriales(List<DeclaracionJuradaMaterial> listMateriales) {
		this.listMateriales = listMateriales;
	}

	public boolean addDeclaracionJuradaProductor(DeclaracionJuradaProductor o){
		return listProductores.add(o);
	}	
	
	public boolean addDeclaracionJuradaMaterial(DeclaracionJuradaMaterial o){
		return listMateriales.add(o);
	}

	public DR getDr() {
		return dr;
	}

	public void setDr(DR dr) {
		this.dr = dr;
	}

}
