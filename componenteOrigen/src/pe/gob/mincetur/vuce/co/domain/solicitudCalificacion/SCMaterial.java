package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

import java.math.BigDecimal;

public class SCMaterial {
	
	private Long            calificacionId;
	private Integer         secuenciaCalifXDJ;
	private Integer         item;
	private String          descripcion;
	private String          fabricanteNombre;
	private BigDecimal      cantidad;
	private BigDecimal      valorUs;
	private Long            partidaArancelaria;
	private String          descripcionPartida;
	private Integer         umFisicaId;
	private String          unidadMedida;
	private Integer         tipoOrigenMaterial;
	private String          fabricanteNumeroDocumento;
	private String          paisProcedenciaDescripcion;
	private String          paisOrigenDescripcion;
	private BigDecimal      porcentajeValorFob;
	private BigDecimal      porcentajeValorExwork;
	private BigDecimal      valorFob;
	private BigDecimal      valorExWork;
	
	private Long            calificacionDrId;
	
	public Long getCalificacionDrId() {
		return calificacionDrId;
	}
	public void setCalificacionDrId(Long calificacionDrId) {
		this.calificacionDrId = calificacionDrId;
	}
	public Long getCalificacionId() {
		return calificacionId;
	}
	public void setCalificacionId(Long calificacionId) {
		this.calificacionId = calificacionId;
	}
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFabricanteNombre() {
		return fabricanteNombre;
	}
	public void setFabricanteNombre(String fabricanteNombre) {
		this.fabricanteNombre = fabricanteNombre;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getValorUs() {
		return valorUs;
	}
	public void setValorUs(BigDecimal valorUs) {
		this.valorUs = valorUs;
	}
	public Long getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Long partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public Integer getTipoOrigenMaterial() {
		return tipoOrigenMaterial;
	}
	public void setTipoOrigenMaterial(Integer tipoOrigenMaterial) {
		this.tipoOrigenMaterial = tipoOrigenMaterial;
	}
	public Integer getSecuenciaCalifXDJ() {
		return secuenciaCalifXDJ;
	}
	public void setSecuenciaCalifXDJ(Integer secuenciaCalifXDJ) {
		this.secuenciaCalifXDJ = secuenciaCalifXDJ;
	}
	public String getDescripcionPartida() {
		return descripcionPartida;
	}
	public void setDescripcionPartida(String descripcionPartida) {
		this.descripcionPartida = descripcionPartida;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getFabricanteNumeroDocumento() {
		return fabricanteNumeroDocumento;
	}
	public void setFabricanteNumeroDocumento(String fabricanteNumeroDocumento) {
		this.fabricanteNumeroDocumento = fabricanteNumeroDocumento;
	}
	public String getPaisProcedenciaDescripcion() {
		return paisProcedenciaDescripcion;
	}
	public void setPaisProcedenciaDescripcion(String paisProcedenciaDescripcion) {
		this.paisProcedenciaDescripcion = paisProcedenciaDescripcion;
	}
	
	public String getPaisOrigenDescripcion() {
		return paisOrigenDescripcion;
	}
	public void setPaisOrigenDescripcion(String paisOrigenDescripcion) {
		this.paisOrigenDescripcion = paisOrigenDescripcion;
	}
	public BigDecimal getPorcentajeValorFob() {
		return porcentajeValorFob;
	}
	public void setPorcentajeValorFob(BigDecimal porcentajeValorFob) {
		this.porcentajeValorFob = porcentajeValorFob;
	}
	public BigDecimal getPorcentajeValorExwork() {
		return porcentajeValorExwork;
	}
	public void setPorcentajeValorExwork(BigDecimal porcentajeValorExwork) {
		this.porcentajeValorExwork = porcentajeValorExwork;
	}
	public BigDecimal getValorFob() {
		return valorFob;
	}
	public void setValorFob(BigDecimal valorFob) {
		this.valorFob = valorFob;
	}
	public BigDecimal getValorExWork() {
		return valorExWork;
	}
	public void setValorExWork(BigDecimal valorExWork) {
		this.valorExWork = valorExWork;
	}

}
