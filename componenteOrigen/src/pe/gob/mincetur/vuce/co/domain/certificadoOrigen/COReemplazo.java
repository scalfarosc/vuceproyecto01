package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.util.Date;

import pe.gob.mincetur.vuce.co.domain.Orden;

public class COReemplazo extends CertificadoOrigen{

	private Long            drIdOrigen;
	private Integer         sdrOrigen;

	private Integer         causalReemplazoCo;
	private String          sustentoAdicional;
	private Date            fechaDrEntidad;
	private String          drEntidad;
	private Integer         sedeId;
	private Long         	drOrigen;
	private String          nroReferenciaCertificado; // 20140520_JRF: Se agrega por el paquete PQT-06
	
	//20140527_JMC: Se elimina por el BUG-MCT003-TLC-Venezuela 
	/*private String 			importadorRegistroFiscal; // Acuerdo Costa Rica, Panama, Guatemala
	private String 			importadorTelefono; // Acuerdo Costa Rica, Panama, Guatemala
	private String 			importadorFax; // Acuerdo Costa Rica, Panama, Guatemala
	private String 			importadorEmail; // Acuerdo Costa Rica, Panama, Guatemala
	*/

	private String 			esConfidencial;

	private String			observacionEvalOrigen;

	private Long            ordenIdOrigen;
	private Integer 		mtoOrigen;
    private String			formatoOrigen;    
    

	public Long getDrIdOrigen() {
		return drIdOrigen;
	}
	public void setDrIdOrigen(Long drIdOrigen) {
		this.drIdOrigen = drIdOrigen;
	}
	public Integer getSdrOrigen() {
		return sdrOrigen;
	}
	public void setSdrOrigen(Integer sdrOrigen) {
		this.sdrOrigen = sdrOrigen;
	}
	public Integer getCausalReemplazoCo() {
		return causalReemplazoCo;
	}
	public void setCausalReemplazoCo(Integer causalReemplazoCo) {
		this.causalReemplazoCo = causalReemplazoCo;
	}
	public String getSustentoAdicional() {
		return sustentoAdicional;
	}
	public void setSustentoAdicional(String sustentoAdicional) {
		this.sustentoAdicional = sustentoAdicional;
	}
	public Date getFechaDrEntidad() {
		return fechaDrEntidad;
	}
	public void setFechaDrEntidad(Date fechaDrEntidad) {
		this.fechaDrEntidad = fechaDrEntidad;
	}
	public String getDrEntidad() {
		return drEntidad;
	}
	public void setDrEntidad(String drEntidad) {
		this.drEntidad = drEntidad;
	}
	public Integer getSedeId() {
		return sedeId;
	}
	public void setSedeId(Integer sedeId) {
		this.sedeId = sedeId;
	}
	public Long getDrOrigen() {
		return drOrigen;
	}
	public void setDrOrigen(Long drOrigen) {
		this.drOrigen = drOrigen;
	}
	public String getObservacionEvalOrigen() {
		return observacionEvalOrigen;
	}
	public void setObservacionEvalOrigen(String observacionEvalOrigen) {
		this.observacionEvalOrigen = observacionEvalOrigen;
	}
	public Long getOrdenIdOrigen() {
		return ordenIdOrigen;
	}
	public void setOrdenIdOrigen(Long ordenIdOrigen) {
		this.ordenIdOrigen = ordenIdOrigen;
	}
	public Integer getMtoOrigen() {
		return mtoOrigen;
	}
	public void setMtoOrigen(Integer mtoOrigen) {
		this.mtoOrigen = mtoOrigen;
	}
	public String getFormatoOrigen() {
		return formatoOrigen;
	}
	public void setFormatoOrigen(String formatoOrigen) {
		this.formatoOrigen = formatoOrigen;
	}
	public String getNroReferenciaCertificado() {
		return nroReferenciaCertificado;
	}
	public void setNroReferenciaCertificado(String nroReferenciaCertificado) {
		this.nroReferenciaCertificado = nroReferenciaCertificado;
	}
	public String getEsConfidencial() {
		return esConfidencial;
	}
	public void setEsConfidencial(String esConfidencial) {
		this.esConfidencial = esConfidencial;
	}
}
