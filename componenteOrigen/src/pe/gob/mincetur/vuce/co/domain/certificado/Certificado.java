package pe.gob.mincetur.vuce.co.domain.certificado;

public class Certificado {
    
	private String formato;
	
	private Integer entidadCertificadoraId;
	
	private String entidadCertificadora;

	private Integer certificadoFormatoId;

	private String nombreVisible;

	private Integer tceId;

	private Long ordenId;

	private Integer mto;
	
	private Long certificadoId;

	private Integer cantidadItem;
	
	private Long certificadoDrId;
	
	private Long certificadoDrBorradorId;
	
	private Long drId;
	
	private Integer sdr;

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public Integer getEntidadCertificadoraId() {
		return entidadCertificadoraId;
	}

	public void setEntidadCertificadoraId(Integer entidadCertificadoraId) {
		this.entidadCertificadoraId = entidadCertificadoraId;
	}

	public String getEntidadCertificadora() {
		return entidadCertificadora;
	}

	public void setEntidadCertificadora(String entidadCertificadora) {
		this.entidadCertificadora = entidadCertificadora;
	}

	public Integer getCertificadoFormatoId() {
		return certificadoFormatoId;
	}

	public void setCertificadoFormatoId(Integer certificadoFormatoId) {
		this.certificadoFormatoId = certificadoFormatoId;
	}

	public String getNombreVisible() {
		return nombreVisible;
	}

	public void setNombreVisible(String nombreVisible) {
		this.nombreVisible = nombreVisible;
	}

	public Integer getTceId() {
		return tceId;
	}

	public void setTceId(Integer tceId) {
		this.tceId = tceId;
	}

	public Long getOrdenId() {
		return ordenId;
	}

	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}

	public Integer getMto() {
		return mto;
	}

	public void setMto(Integer mto) {
		this.mto = mto;
	}

	public Long getCertificadoId() {
		return certificadoId;
	}

	public void setCertificadoId(Long certificadoId) {
		this.certificadoId = certificadoId;
	}

	public Integer getCantidadItem() {
		return cantidadItem;
	}

	public void setCantidadItem(Integer cantidadItem) {
		this.cantidadItem = cantidadItem;
	}

	public Long getCertificadoDrId() {
		return certificadoDrId;
	}

	public void setCertificadoDrId(Long certificadoDrId) {
		this.certificadoDrId = certificadoDrId;
	}

	public Long getCertificadoDrBorradorId() {
		return certificadoDrBorradorId;
	}

	public void setCertificadoDrBorradorId(Long certificadoDrBorradorId) {
		this.certificadoDrBorradorId = certificadoDrBorradorId;
	}

	public Long getDrId() {
		return drId;
	}

	public void setDrId(Long drId) {
		this.drId = drId;
	}

	public Integer getSdr() {
		return sdr;
	}

	public void setSdr(Integer sdr) {
		this.sdr = sdr;
	}
	


}
