package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

import java.util.Date;


public class SCEARepresentante {
	
	private Long expautId;
	private Integer secuenciaRepresentante;
	private String nombre;
	private String cargo;
	private String telefono;
	private String fax;
	private String email;
	private String indConoceExigencia;
	private String indConoceReglas;
	private String indConEntrenamiento;
	private String lugarCapacitacion;
	private Date fechaPoder;
	
	public Long getExpautId() {
		return expautId;
	}
	public void setExpautId(Long expautId) {
		this.expautId = expautId;
	}
	public Integer getSecuenciaRepresentante() {
		return secuenciaRepresentante;
	}
	public void setSecuenciaRepresentante(Integer secuenciaRepresentante) {
		this.secuenciaRepresentante = secuenciaRepresentante;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIndConoceExigencia() {
		return indConoceExigencia;
	}
	public void setIndConoceExigencia(String indConoceExigencia) {
		this.indConoceExigencia = indConoceExigencia;
	}
	public String getIndConoceReglas() {
		return indConoceReglas;
	}
	public void setIndConoceReglas(String indConoceReglas) {
		this.indConoceReglas = indConoceReglas;
	}
	public String getIndConEntrenamiento() {
		return indConEntrenamiento;
	}
	public void setIndConEntrenamiento(String indConEntrenamiento) {
		this.indConEntrenamiento = indConEntrenamiento;
	}
	public String getLugarCapacitacion() {
		return lugarCapacitacion;
	}
	public void setLugarCapacitacion(String lugarCapacitacion) {
		this.lugarCapacitacion = lugarCapacitacion;
	}
	public Date getFechaPoder() {
		return fechaPoder;
	}
	public void setFechaPoder(Date fechaPoder) {
		this.fechaPoder = fechaPoder;
	}
	
}
