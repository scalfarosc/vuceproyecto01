package pe.gob.mincetur.vuce.co.domain.ebxml;

import java.util.ArrayList;
import java.util.List;

import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;

public class MCT001EBXML extends SolicitudEBXML  {
	
	private DR dr;
	
	private CertificadoOrigen certificadoOrigen;
	
	private List<COFactura> listCoFactura = new ArrayList<COFactura>();

	private List<CertificadoOrigenMercancia> listCertificadoOrigenMercancia = new ArrayList<CertificadoOrigenMercancia>();

	
	public MCT001EBXML(){		
	    super();
	    setTemplate("MCT001.vm");		
	}
	
	public MCT001EBXML(String template){		
	    super();
	    setTemplate(template);		
	}

	public CertificadoOrigen getCertificadoOrigen() {
		return certificadoOrigen;
	}

	public void setCertificadoOrigen(CertificadoOrigen certificadoOrigen) {
		this.certificadoOrigen = certificadoOrigen;
	}

	public List<CertificadoOrigenMercancia> getListCertificadoOrigenMercancia() {
		return listCertificadoOrigenMercancia;
	}

	public void setListCertificadoOrigenMercancia(
			List<CertificadoOrigenMercancia> listCertificadoOrigenMercancia) {
		this.listCertificadoOrigenMercancia = listCertificadoOrigenMercancia;
	}
	
	public boolean addCertificadoOrigenMercancia(CertificadoOrigenMercancia o){
		return listCertificadoOrigenMercancia.add(o);
	}

	public List<COFactura> getListCoFactura() {
		return listCoFactura;
	}

	public void setListCoFactura(List<COFactura> listCoFactura) {
		this.listCoFactura = listCoFactura;
	}
	
	public boolean addCOFactura(COFactura o){
		return listCoFactura.add(o);
	}

	public DR getDr() {
		return dr;
	}

	public void setDr(DR dr) {
		this.dr = dr;
	}	

}
