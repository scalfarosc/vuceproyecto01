package pe.gob.mincetur.vuce.co.domain.firma;

import java.util.List;

public class CodExporter {
	
	private Long codigoId;
	private String version;
	private String remitente;
	private String nombreAcuerdo;
	private String acronimoAcuerdo;
	private EnteComercio exportador;
	private EnteComercio importador;
	private List<EnteComercio> productores;
	private List<FacturaOrigen> facturas;
	private List<BienOrigen> bienes;
	private String comentarioGeneral;//Observaciones
	//private List<ComentarioOrigen> comentarios;	
		
	private List<FacturaTercerOpOrigen> tercerOperadores;
	private String cantidadProductores;
	private String cantidadFacturas;
	private String cantidadMercancias;
	private String productor;
	private String InformacionProductorConfidencial;
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	public String getNombreAcuerdo() {
		return nombreAcuerdo;
	}
	public void setNombreAcuerdo(String nombreAcuerdo) {
		this.nombreAcuerdo = nombreAcuerdo;
	}
	public String getAcronimoAcuerdo() {
		return acronimoAcuerdo;
	}
	public void setAcronimoAcuerdo(String acronimoAcuerdo) {
		this.acronimoAcuerdo = acronimoAcuerdo;
	}
	public EnteComercio getExportador() {
		return exportador;
	}
	public void setExportador(EnteComercio exportador) {
		this.exportador = exportador;
	}
	public EnteComercio getImportador() {
		return importador;
	}
	public void setImportador(EnteComercio importador) {
		this.importador = importador;
	}
	public List<EnteComercio> getProductores() {
		return productores;
	}
	public void setProductores(List<EnteComercio> productores) {
		this.productores = productores;
	}
	public List<FacturaOrigen> getFacturas() {
		return facturas;
	}
	public void setFacturas(List<FacturaOrigen> facturas) {
		this.facturas = facturas;
	}
	public List<BienOrigen> getBienes() {
		return bienes;
	}
	public void setBienes(List<BienOrigen> bienes) {
		this.bienes = bienes;
	}
	public String getComentarioGeneral() {
		return comentarioGeneral;
	}
	public void setComentarioGeneral(String comentarioGeneral) {
		this.comentarioGeneral = comentarioGeneral;
	}
	public List<FacturaTercerOpOrigen> getTercerOperadores() {
		return tercerOperadores;
	}
	public void setTercerOperadores(List<FacturaTercerOpOrigen> tercerOperadores) {
		this.tercerOperadores = tercerOperadores;
	}
	public String getCantidadProductores() {
		return cantidadProductores;
	}
	public void setCantidadProductores(String cantidadProductores) {
		this.cantidadProductores = cantidadProductores;
	}
	public String getCantidadFacturas() {
		return cantidadFacturas;
	}
	public void setCantidadFacturas(String cantidadFacturas) {
		this.cantidadFacturas = cantidadFacturas;
	}
	public String getCantidadMercancias() {
		return cantidadMercancias;
	}
	public void setCantidadMercancias(String cantidadMercancias) {
		this.cantidadMercancias = cantidadMercancias;
	}
	public String getProductor() {
		return productor;
	}
	public void setProductor(String productor) {
		this.productor = productor;
	}
	public String getInformacionProductorConfidencial() {
		return InformacionProductorConfidencial;
	}
	public void setInformacionProductorConfidencial(
			String informacionProductorConfidencial) {
		InformacionProductorConfidencial = informacionProductorConfidencial;
	}
	public Long getCodigoId() {
		return codigoId;
	}
	public void setCodigoId(Long codigoId) {
		this.codigoId = codigoId;
	}
}
