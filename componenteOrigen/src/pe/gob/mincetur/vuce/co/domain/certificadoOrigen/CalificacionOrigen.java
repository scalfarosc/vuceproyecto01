package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.math.BigDecimal;
import java.util.Date;

/**
*Objeto :	CalificacionOrigen
*Descripcion :	Clase Entidad
*Fecha de Creacion :	
*Autor :	vuce - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		05/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ. 
*/
public class CalificacionOrigen {
	
	private Long coId;
	private Long calificacionUoId;
	private Integer secuenciaMercancia;
	private Long ordenIdInicial;
	private Integer mtoInicial;
	private Long djId;
	private String calificacionUo;
	private Integer tipoRolCalificacion;
	private Integer entidadId;
	private Integer acuerdoInternacionalId;
	private Integer paisIsoIdxAcuerdoInt;
	private String cumpleTotalmenteObtenido;
	private String cumpleCriterioCambioClasif;
	private Integer secuenciaOrigen;
	private Integer secuenciaNorma;
	private String cumpleOtroCriterio;
	private String partidaSegunAcuerdo;
	private BigDecimal porcentajeSegunCriterio;
	private String calificacion;
	private Date fechaRegistro;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Date fechaAprobacion;
	private Date fechaDenegacion;
	private String detalleDenegacion;
	private String informacionCompleta;
	private String anulado;
	private Integer tipoAnulacionCalificacion;
	private Date fechaAnulacion;
	private String estadoRegistro;
	private String estado;
	private String codigoProductoxUsuario;
	private String copiaxModificacion;
	private Long calificacionUoIdCopia;
	private Long calificacionUoSec;
	private Long usuidRegAud;
	private Long usuidModAud;
	private Date fechaRegAud;
	private Date fechaModAud;
	private String usubdRegAud;
	private String usubdModAud;
	private Long docTipoPropietarioDatos;
	private String numDocPropietarioDatos;
	private Integer tipoUsuarioPropietario;
	private Integer tipoCalificacion; // 20150116_RPC: Juegos y Surtidos
	private String disposicionId;//05/04/2016 NC: Alianza Pacifico
	
	public Long getCoId() {
		return coId;
	}
	public void setCoId(Long coId) {
		this.coId = coId;
	}
	public Long getCalificacionUoId() {
		return calificacionUoId;
	}
	public void setCalificacionUoId(Long calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	public Integer getSecuenciaMercancia() {
		return secuenciaMercancia;
	}
	public void setSecuenciaMercancia(Integer secuenciaMercancia) {
		this.secuenciaMercancia = secuenciaMercancia;
	}
	public Long getOrdenIdInicial() {
		return ordenIdInicial;
	}
	public void setOrdenIdInicial(Long ordenIdInicial) {
		this.ordenIdInicial = ordenIdInicial;
	}
	public Integer getMtoInicial() {
		return mtoInicial;
	}
	public void setMtoInicial(Integer mtoInicial) {
		this.mtoInicial = mtoInicial;
	}
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public String getCalificacionUo() {
		return calificacionUo;
	}
	public void setCalificacionUo(String calificacionUo) {
		this.calificacionUo = calificacionUo;
	}
	public Integer getTipoRolCalificacion() {
		return tipoRolCalificacion;
	}
	public void setTipoRolCalificacion(Integer tipoRolCalificacion) {
		this.tipoRolCalificacion = tipoRolCalificacion;
	}
	public Integer getEntidadId() {
		return entidadId;
	}
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public Integer getPaisIsoIdxAcuerdoInt() {
		return paisIsoIdxAcuerdoInt;
	}
	public void setPaisIsoIdxAcuerdoInt(Integer paisIsoIdxAcuerdoInt) {
		this.paisIsoIdxAcuerdoInt = paisIsoIdxAcuerdoInt;
	}
	public String getCumpleTotalmenteObtenido() {
		return cumpleTotalmenteObtenido;
	}
	public void setCumpleTotalmenteObtenido(String cumpleTotalmenteObtenido) {
		this.cumpleTotalmenteObtenido = cumpleTotalmenteObtenido;
	}
	public String getCumpleCriterioCambioClasif() {
		return cumpleCriterioCambioClasif;
	}
	public void setCumpleCriterioCambioClasif(String cumpleCriterioCambioClasif) {
		this.cumpleCriterioCambioClasif = cumpleCriterioCambioClasif;
	}
	public Integer getSecuenciaOrigen() {
		return secuenciaOrigen;
	}
	public void setSecuenciaOrigen(Integer secuenciaOrigen) {
		this.secuenciaOrigen = secuenciaOrigen;
	}
	public Integer getSecuenciaNorma() {
		return secuenciaNorma;
	}
	public void setSecuenciaNorma(Integer secuenciaNorma) {
		this.secuenciaNorma = secuenciaNorma;
	}
	public String getCumpleOtroCriterio() {
		return cumpleOtroCriterio;
	}
	public void setCumpleOtroCriterio(String cumpleOtroCriterio) {
		this.cumpleOtroCriterio = cumpleOtroCriterio;
	}
	public String getPartidaSegunAcuerdo() {
		return partidaSegunAcuerdo;
	}
	public void setPartidaSegunAcuerdo(String partidaSegunAcuerdo) {
		this.partidaSegunAcuerdo = partidaSegunAcuerdo;
	}
	public BigDecimal getPorcentajeSegunCriterio() {
		return porcentajeSegunCriterio;
	}
	public void setPorcentajeSegunCriterio(BigDecimal porcentajeSegunCriterio) {
		this.porcentajeSegunCriterio = porcentajeSegunCriterio;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public Date getFechaDenegacion() {
		return fechaDenegacion;
	}
	public void setFechaDenegacion(Date fechaDenegacion) {
		this.fechaDenegacion = fechaDenegacion;
	}
	public String getDetalleDenegacion() {
		return detalleDenegacion;
	}
	public void setDetalleDenegacion(String detalleDenegacion) {
		this.detalleDenegacion = detalleDenegacion;
	}
	public String getInformacionCompleta() {
		return informacionCompleta;
	}
	public void setInformacionCompleta(String informacionCompleta) {
		this.informacionCompleta = informacionCompleta;
	}
	public String getAnulado() {
		return anulado;
	}
	public void setAnulado(String anulado) {
		this.anulado = anulado;
	}
	public Integer getTipoAnulacionCalificacion() {
		return tipoAnulacionCalificacion;
	}
	public void setTipoAnulacionCalificacion(Integer tipoAnulacionCalificacion) {
		this.tipoAnulacionCalificacion = tipoAnulacionCalificacion;
	}
	public Date getFechaAnulacion() {
		return fechaAnulacion;
	}
	public void setFechaAnulacion(Date fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodigoProductoxUsuario() {
		return codigoProductoxUsuario;
	}
	public void setCodigoProductoxUsuario(String codigoProductoxUsuario) {
		this.codigoProductoxUsuario = codigoProductoxUsuario;
	}
	public String getCopiaxModificacion() {
		return copiaxModificacion;
	}
	public void setCopiaxModificacion(String copiaxModificacion) {
		this.copiaxModificacion = copiaxModificacion;
	}
	public Long getCalificacionUoIdCopia() {
		return calificacionUoIdCopia;
	}
	public void setCalificacionUoIdCopia(Long calificacionUoIdCopia) {
		this.calificacionUoIdCopia = calificacionUoIdCopia;
	}
	public Long getCalificacionUoSec() {
		return calificacionUoSec;
	}
	public void setCalificacionUoSec(Long calificacionUoSec) {
		this.calificacionUoSec = calificacionUoSec;
	}
	public Long getUsuidRegAud() {
		return usuidRegAud;
	}
	public void setUsuidRegAud(Long usuidRegAud) {
		this.usuidRegAud = usuidRegAud;
	}
	public Long getUsuidModAud() {
		return usuidModAud;
	}
	public void setUsuidModAud(Long usuidModAud) {
		this.usuidModAud = usuidModAud;
	}
	public Date getFechaRegAud() {
		return fechaRegAud;
	}
	public void setFechaRegAud(Date fechaRegAud) {
		this.fechaRegAud = fechaRegAud;
	}
	public Date getFechaModAud() {
		return fechaModAud;
	}
	public void setFechaModAud(Date fechaModAud) {
		this.fechaModAud = fechaModAud;
	}
	public String getUsubdRegAud() {
		return usubdRegAud;
	}
	public void setUsubdRegAud(String usubdRegAud) {
		this.usubdRegAud = usubdRegAud;
	}
	public String getUsubdModAud() {
		return usubdModAud;
	}
	public void setUsubdModAud(String usubdModAud) {
		this.usubdModAud = usubdModAud;
	}
	public Long getDocTipoPropietarioDatos() {
		return docTipoPropietarioDatos;
	}
	public void setDocTipoPropietarioDatos(Long docTipoPropietarioDatos) {
		this.docTipoPropietarioDatos = docTipoPropietarioDatos;
	}
	public String getNumDocPropietarioDatos() {
		return numDocPropietarioDatos;
	}
	public void setNumDocPropietarioDatos(String numDocPropietarioDatos) {
		this.numDocPropietarioDatos = numDocPropietarioDatos;
	}
	public Integer getTipoUsuarioPropietario() {
		return tipoUsuarioPropietario;
	}
	public void setTipoUsuarioPropietario(Integer tipoUsuarioPropietario) {
		this.tipoUsuarioPropietario = tipoUsuarioPropietario;
	}

	public Integer getTipoCalificacion() {
		return tipoCalificacion;
	}
	public void setTipoCalificacion(Integer tipoCalificacion) {
		this.tipoCalificacion = tipoCalificacion;
	}
	public String getDisposicionId() {
		return disposicionId;
	}
	public void setDisposicionId(String disposicionId) {
		this.disposicionId = disposicionId;
	}
	//Ticket 149343 GCHAVEZ-05/07/2019
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CalificacionOrigen [coId=");
		builder.append(coId);
		builder.append(", calificacionUoId=");
		builder.append(calificacionUoId);
		builder.append(", secuenciaMercancia=");
		builder.append(secuenciaMercancia);
		builder.append(", ordenIdInicial=");
		builder.append(ordenIdInicial);
		builder.append(", mtoInicial=");
		builder.append(mtoInicial);
		builder.append(", djId=");
		builder.append(djId);
		builder.append(", calificacionUo=");
		builder.append(calificacionUo);
		builder.append(", tipoRolCalificacion=");
		builder.append(tipoRolCalificacion);
		builder.append(", entidadId=");
		builder.append(entidadId);
		builder.append(", acuerdoInternacionalId=");
		builder.append(acuerdoInternacionalId);
		builder.append(", paisIsoIdxAcuerdoInt=");
		builder.append(paisIsoIdxAcuerdoInt);
		builder.append(", cumpleTotalmenteObtenido=");
		builder.append(cumpleTotalmenteObtenido);
		builder.append(", cumpleCriterioCambioClasif=");
		builder.append(cumpleCriterioCambioClasif);
		builder.append(", secuenciaOrigen=");
		builder.append(secuenciaOrigen);
		builder.append(", secuenciaNorma=");
		builder.append(secuenciaNorma);
		builder.append(", cumpleOtroCriterio=");
		builder.append(cumpleOtroCriterio);
		builder.append(", partidaSegunAcuerdo=");
		builder.append(partidaSegunAcuerdo);
		builder.append(", porcentajeSegunCriterio=");
		builder.append(porcentajeSegunCriterio);
		builder.append(", calificacion=");
		builder.append(calificacion);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", fechaInicioVigencia=");
		builder.append(fechaInicioVigencia);
		builder.append(", fechaFinVigencia=");
		builder.append(fechaFinVigencia);
		builder.append(", fechaAprobacion=");
		builder.append(fechaAprobacion);
		builder.append(", fechaDenegacion=");
		builder.append(fechaDenegacion);
		builder.append(", detalleDenegacion=");
		builder.append(detalleDenegacion);
		builder.append(", informacionCompleta=");
		builder.append(informacionCompleta);
		builder.append(", anulado=");
		builder.append(anulado);
		builder.append(", tipoAnulacionCalificacion=");
		builder.append(tipoAnulacionCalificacion);
		builder.append(", fechaAnulacion=");
		builder.append(fechaAnulacion);
		builder.append(", estadoRegistro=");
		builder.append(estadoRegistro);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", codigoProductoxUsuario=");
		builder.append(codigoProductoxUsuario);
		builder.append(", copiaxModificacion=");
		builder.append(copiaxModificacion);
		builder.append(", calificacionUoIdCopia=");
		builder.append(calificacionUoIdCopia);
		builder.append(", calificacionUoSec=");
		builder.append(calificacionUoSec);
		builder.append(", usuidRegAud=");
		builder.append(usuidRegAud);
		builder.append(", usuidModAud=");
		builder.append(usuidModAud);
		builder.append(", fechaRegAud=");
		builder.append(fechaRegAud);
		builder.append(", fechaModAud=");
		builder.append(fechaModAud);
		builder.append(", usubdRegAud=");
		builder.append(usubdRegAud);
		builder.append(", usubdModAud=");
		builder.append(usubdModAud);
		builder.append(", docTipoPropietarioDatos=");
		builder.append(docTipoPropietarioDatos);
		builder.append(", numDocPropietarioDatos=");
		builder.append(numDocPropietarioDatos);
		builder.append(", tipoUsuarioPropietario=");
		builder.append(tipoUsuarioPropietario);
		builder.append(", tipoCalificacion=");
		builder.append(tipoCalificacion);
		builder.append(", disposicionId=");
		builder.append(disposicionId);
		builder.append("]");
		return builder.toString();
	}
	//Ticket 149343 GCHAVEZ-05/07/2019

}


