package pe.gob.mincetur.vuce.co.domain.firma;

public class EnteComercio {
	
	private String pais;
	private String nombreNegocio;
	private String direccion;
	private String ciudad;
	private String identificacionImpuesto;
	private String telefono;
	private String fax;
	private String email;
	private String url;
	private String orden; // campo opcional usado para productores
	private String exportador; // campo opcional usado para productores
	private String confidencialidad; // campo opcional usado para productores
	private String telfFaxEmail;
	private String declaracion;
	private String nombreProductor; //campo para determinar si es confidencial o no "DISPONIBLE A SOLICITUD DE LA AUTORIDAD COMPETENTE"
	private String productorExportador;//mismo o diferente
		
	public String getTelfFaxEmail() {
		return telfFaxEmail;
	}
	public void setTelfFaxEmail(String telfFaxEmail) {
		this.telfFaxEmail = telfFaxEmail;
		String [] piezas = telfFaxEmail.split("/");
		if (piezas != null && piezas.length == 4) {
			this.telefono = piezas[1].trim();
			this.fax = piezas[2].trim();
			this.email = piezas[3].trim();
		}
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getIdentificacionImpuesto() {
		return identificacionImpuesto;
	}
	public void setIdentificacionImpuesto(String identificacionImpuesto) {
		this.identificacionImpuesto = identificacionImpuesto;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public String getExportador() {
		return exportador;
	}
	public void setExportador(String exportador) {
		this.exportador = exportador;
	}
	public String getConfidencialidad() {
		return confidencialidad;
	}
	public void setConfidencialidad(String confidencialidad) {
		this.confidencialidad = confidencialidad;
	}
	public String getDeclaracion() {
		return declaracion;
	}
	public void setDeclaracion(String declaracion) {
		this.declaracion = declaracion;
	}
	public String getNombreProductor() {
		return nombreProductor;
	}
	public void setNombreProductor(String nombreProductor) {
		this.nombreProductor = nombreProductor;
	}
	public String getProductorExportador() {
		return productorExportador;
	}
	public void setProductorExportador(String productorExportador) {
		this.productorExportador = productorExportador;
	}


}
