package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.util.Date;

public class CODuplicado extends CertificadoOrigen{

	private Long            drIdOrigen;
	private Integer         sdrOrigen;
	private Long            drOrigen;
	private String          nroReferenciaCertificado; // 20140520_JRF: Se agrega por el paquete PQT-06

	private Integer         causalDuplicadoCo;
	private String          sustentoAdicional;
	private String          aceptacion;
	private Date            fechaDrEntidad;
	private String          drEntidad;
	private Integer         sedeId;
	private Long            ordenIdOrigen;
	private Integer 		mtoOrigen;
    private String			formatoOrigen;

	public Long getDrIdOrigen() {
		return drIdOrigen;
	}
	public void setDrIdOrigen(Long drIdOrigen) {
		this.drIdOrigen = drIdOrigen;
	}
	public Integer getSdrOrigen() {
		return sdrOrigen;
	}
	public void setSdrOrigen(Integer sdrOrigen) {
		this.sdrOrigen = sdrOrigen;
	}
	public Integer getCausalDuplicadoCo() {
		return causalDuplicadoCo;
	}
	public void setCausalDuplicadoCo(Integer causalDuplicadoCo) {
		this.causalDuplicadoCo = causalDuplicadoCo;
	}
	public String getSustentoAdicional() {
		return sustentoAdicional;
	}
	public void setSustentoAdicional(String sustentoAdicional) {
		this.sustentoAdicional = sustentoAdicional;
	}
	public String getAceptacion() {
		return aceptacion;
	}
	public void setAceptacion(String aceptacion) {
		this.aceptacion = aceptacion;
	}
	public Date getFechaDrEntidad() {
		return fechaDrEntidad;
	}
	public void setFechaDrEntidad(Date fechaDrEntidad) {
		this.fechaDrEntidad = fechaDrEntidad;
	}
	public String getDrEntidad() {
		return drEntidad;
	}
	public void setDrEntidad(String drEntidad) {
		this.drEntidad = drEntidad;
	}
	public Integer getSedeId() {
		return sedeId;
	}
	public void setSedeId(Integer sedeId) {
		this.sedeId = sedeId;
	}
	public Long getOrdenIdOrigen() {
		return ordenIdOrigen;
	}
	public void setOrdenIdOrigen(Long ordenIdOrigen) {
		this.ordenIdOrigen = ordenIdOrigen;
	}
	public Integer getMtoOrigen() {
		return mtoOrigen;
	}
	public void setMtoOrigen(Integer mtoOrigen) {
		this.mtoOrigen = mtoOrigen;
	}
	public String getFormatoOrigen() {
		return formatoOrigen;
	}
	public void setFormatoOrigen(String formatoOrigen) {
		this.formatoOrigen = formatoOrigen;
	}
	public Long getDrOrigen() {
		return drOrigen;
	}
	public void setDrOrigen(Long drOrigen) {
		this.drOrigen = drOrigen;
	}
	public String getNroReferenciaCertificado() {
		return nroReferenciaCertificado;
	}
	public void setNroReferenciaCertificado(String nroReferenciaCertificado) {
		this.nroReferenciaCertificado = nroReferenciaCertificado;
	}

}
