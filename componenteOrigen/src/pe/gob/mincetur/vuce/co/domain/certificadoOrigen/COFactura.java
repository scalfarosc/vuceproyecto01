package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.util.Date;

/**
 * Clase de datos de la Factura del Certificado de Origen
 * @author Ricardo Montse
 */

public class COFactura {

	private Long coId;
	private Integer secuencia;
	private String numero;
	private Date fecha;
	private String indicadorTercerPais;
	private String nombreOperadorTercerPais;
	private String direccionOperadorTercerPais;
	private Long adjuntoId;
	private String nombreAdjunto;//20140825_JMC_Intercambio_Empresa
	private String tieneFacturaTercerPais;
	private Integer acuerdoInternacionalId;
	private String mostrarFactura; // 20150610_RPC: Acta.33: Mostrar Factura Acuerdos AELC/UE
	private String paisOperadorTercerPais;//JMC 28/06/2017 Alianza

	public Long getCoId() {
		return coId;
	}
	public void setCoId(Long coId) {
		this.coId = coId;
	}
	public Integer getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getIndicadorTercerPais() {
		return indicadorTercerPais;
	}
	public void setIndicadorTercerPais(String indicadorTercerPais) {
		this.indicadorTercerPais = indicadorTercerPais;
	}
	public String getNombreOperadorTercerPais() {
		return nombreOperadorTercerPais;
	}
	public void setNombreOperadorTercerPais(String nombreOperadorTercerPais) {
		this.nombreOperadorTercerPais = nombreOperadorTercerPais;
	}
	public Long getAdjuntoId() {
		return adjuntoId;
	}
	public void setAdjuntoId(Long adjuntoId) {
		this.adjuntoId = adjuntoId;
	}
	public String getDireccionOperadorTercerPais() {
		return direccionOperadorTercerPais;
	}
	public void setDireccionOperadorTercerPais(String direccionOperadorTercerPais) {
		this.direccionOperadorTercerPais = direccionOperadorTercerPais;
	}
	public String getTieneFacturaTercerPais() {
		return tieneFacturaTercerPais;
	}
	public void setTieneFacturaTercerPais(String tieneFacturaTercerPais) {
		this.tieneFacturaTercerPais = tieneFacturaTercerPais;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public String getNombreAdjunto() {
		return nombreAdjunto;
	}
	public void setNombreAdjunto(String nombreAdjunto) {
		this.nombreAdjunto = nombreAdjunto;
	}
	
	public String getMostrarFactura() {
		return mostrarFactura;
	}
	public void setMostrarFactura(String mostrarFactura) {
		this.mostrarFactura = mostrarFactura;
	}
	public String getPaisOperadorTercerPais() {
		return paisOperadorTercerPais;
	}
	public void setPaisOperadorTercerPais(String paisOperadorTercerPais) {
		this.paisOperadorTercerPais = paisOperadorTercerPais;
	}
	
}
