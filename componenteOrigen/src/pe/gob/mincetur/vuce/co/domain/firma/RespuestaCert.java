package pe.gob.mincetur.vuce.co.domain.firma;

import java.util.List;

public class RespuestaCert {
	
	private String codId;
	private String idRespuesta;
	private String codigoEstado;
	private String razon;
	private String fechaRespuesta;
	private String codigoCertificado;	
	private String fechaCertificado;
	private String certificadoId;
	private String remitente;
	private byte [] archivo;
	private List<ErrorDetalle> errorDetalle;
	
	public String getCodId() {
		return codId;
	}
	public void setCodId(String codId) {
		this.codId = codId;
	}
	public String getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(String idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public String getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	public String getRazon() {
		return razon;
	}
	public void setRazon(String razon) {
		this.razon = razon;
	}
	public String getFechaRespuesta() {
		return fechaRespuesta;
	}
	public void setFechaRespuesta(String fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}
	public String getCodigoCertificado() {
		return codigoCertificado;
	}
	public void setCodigoCertificado(String codigoCertificado) {
		this.codigoCertificado = codigoCertificado;
	}
	public String getFechaCertificado() {
		return fechaCertificado;
	}
	public void setFechaCertificado(String fechaCertificado) {
		this.fechaCertificado = fechaCertificado;
	}
	public String getCertificadoId() {
		return certificadoId;
	}
	public void setCertificadoId(String certificadoId) {
		this.certificadoId = certificadoId;
	}
	public List<ErrorDetalle> getErrorDetalle() {
		return errorDetalle;
	}
	public void setErrorDetalle(List<ErrorDetalle> errorDetalle) {
		this.errorDetalle = errorDetalle;
	}
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	public byte[] getArchivo() {
		return archivo;
	}
	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	
}
