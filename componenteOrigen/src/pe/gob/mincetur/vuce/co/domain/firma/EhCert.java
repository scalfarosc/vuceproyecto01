package pe.gob.mincetur.vuce.co.domain.firma;

public class EhCert {
	
	private String idEH;
	private String pais;
	private String nombre;
	private String direccion;
	private String codigoCertificado;	
	private String fechaCertificado;
	private String certificadoId;
	private String codigoCertificadoReemplazado;
	private String tipoEmision;
	
	public String getIdEH() {
		return idEH;
	}
	public void setIdEH(String idEH) {
		this.idEH = idEH;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCodigoCertificado() {
		return codigoCertificado;
	}
	public void setCodigoCertificado(String codigoCertificado) {
		this.codigoCertificado = codigoCertificado;
	}
	public String getFechaCertificado() {
		return fechaCertificado;
	}
	public void setFechaCertificado(String fechaCertificado) {
		this.fechaCertificado = fechaCertificado;
	}
	public String getCertificadoId() {
		return certificadoId;
	}
	public void setCertificadoId(String certificadoId) {
		this.certificadoId = certificadoId;
	}
	public String getCodigoCertificadoReemplazado() {
		return codigoCertificadoReemplazado;
	}
	public void setCodigoCertificadoReemplazado(String codigoCertificadoReemplazado) {
		this.codigoCertificadoReemplazado = codigoCertificadoReemplazado;
	}
	public String getTipoEmision() {
		return tipoEmision;
	}
	public void setTipoEmision(String tipoEmision) {
		this.tipoEmision = tipoEmision;
	}
}
