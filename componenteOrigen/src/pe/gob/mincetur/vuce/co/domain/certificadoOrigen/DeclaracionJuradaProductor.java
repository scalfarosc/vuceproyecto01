package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

/**
 * Clase de datos de Productor de la Calificación
 * @author Erick Oscátegui
 */

public class DeclaracionJuradaProductor {

	private Long calificacionUoId;
	private Integer secuenciaProductor;
	private Integer documentoTipo;
	private String numeroDocumento;
	private String nombre;
	private String documentoTipoDesc;
	private String direccion;
	private String direccionAdicional;
	private String esValidador;
	private String telefono;
	private String email;
	private String fax;
	private Integer tipoRolCalificacionNew;
	private String pais; // JMC 23/07/2017 Alianza -->
	private String ciudad; // JMC 23/07/2017 Alianza -->

	//20140825_JMC_Intercambio_Empresa
	private Long adjuntoId;
	private String nombreAdjunto;
	private Long mct005TmpId;
	
	public Long getCalificacionUoId() {
		return calificacionUoId;
	}
	public void setCalificacionUoId(Long calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	public Integer getSecuenciaProductor() {
		return secuenciaProductor;
	}
	public void setSecuenciaProductor(Integer secuenciaProductor) {
		this.secuenciaProductor = secuenciaProductor;
	}
	public Integer getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(Integer documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDocumentoTipoDesc() {
		return documentoTipoDesc;
	}
	public void setDocumentoTipoDesc(String documentoTipoDesc) {
		this.documentoTipoDesc = documentoTipoDesc;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEsValidador() {
		return esValidador;
	}
	public void setEsValidador(String esValidador) {
		this.esValidador = esValidador;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getTipoRolCalificacionNew() {
		return tipoRolCalificacionNew;
	}
	public void setTipoRolCalificacionNew(Integer tipoRolCalificacionNew) {
		this.tipoRolCalificacionNew = tipoRolCalificacionNew;
	}
	public String getDireccionAdicional() {
		return direccionAdicional;
	}
	public void setDireccionAdicional(String direccionAdicional) {
		this.direccionAdicional = direccionAdicional;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public Long getAdjuntoId() {
		return adjuntoId;
	}
	public void setAdjuntoId(Long adjuntoId) {
		this.adjuntoId = adjuntoId;
	}
	public String getNombreAdjunto() {
		return nombreAdjunto;
	}
	public void setNombreAdjunto(String nombreAdjunto) {
		this.nombreAdjunto = nombreAdjunto;
	}
	public Long getMct005TmpId() {
		return mct005TmpId;
	}
	public void setMct005TmpId(Long mct005TmpId) {
		this.mct005TmpId = mct005TmpId;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

}
