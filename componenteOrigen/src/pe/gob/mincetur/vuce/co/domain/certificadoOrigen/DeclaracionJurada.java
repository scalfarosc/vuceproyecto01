package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Clase de datos de Declaraci�n Jurada
 * @author Erick Osc�tegui
 */

public class DeclaracionJurada {

	private Long calificacionUoId;
	private Long coId; // Para la inserci�n relacionada con mercanc�a, no es parte de la tabla de dj
	private Integer secuenciaMercancia; // Para la inserci�n relacionada con mercanc�a, no es parte de la tabla de dj
	private Long djId;
	private Integer tipoRolDj;
	private String denominacion;
	private String caracteristica;
	private Long partidaArancelaria;
	private Integer umFisicaId;
	private String cumpleTotalmenteObtenido;
	private String cumpleCriterioCambioClasif;
	private String dj;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String estadoRegistro;
	private String anulado;
	private Integer tipoAnulacionDj;
	private Date fechaRegistro;
	private Date fechaAprobacion;
	private Date fechaAnulacion;
	private Date fechaDenegacion;
	private String calificacion;
	private String partidaSegunAcuerdo;
	private Integer entidadId;
	private Integer acuerdoInternacionalId;
	private Integer secuenciaNorma;
	private Integer secuenciaOrigen;
	private BigDecimal demasGasto;
	private BigDecimal porcentajeDemasGastos;
	private BigDecimal valorUs;
	private Date fechaAceptacionValidacion;
	private Date fechaFinValidacion;
	private Integer productorTipoDocumento;
	private String productorNumeroDocumento;
	private String denominacionArancelaria;
	private String criterioOrigenCertificado;
	private String detalleDenegacion;
    private BigDecimal pesoNetoMercancia;
    private Long usuarioIdProductorValidador;
    private Integer paisIsoIdPorAcuerdoInt;
	private String tienePoderPorProductor;
	private Long orden;
	private Integer mto;
	private String cumpleOtroCriterio;
	private String aceptacion;
    private BigDecimal porcentajeSegunCriterio;
    private BigDecimal valorUsFabrica;
    private String informacionCompleta;
    private String copiaXModificacion;
    private String unidadMedida;
	private String cantidadUmRefer;
	
	//20140825_JMC_Intercambio_Empresa
	private String paisIsoIdPorAcuerdoIntCodigo;
	private String acuerdoInternacionalCodigo;
	private Integer entidadCodigo;
	private Integer sedeCodigo;
	private String unidadMedidaCodigo;
	private String numeroDocumentoSolicitante;
	private Integer secuenciaDeclaracionJurada;	
	private String direccionAdicional;
		
	private BigDecimal cantidad;
	
	public Long getCalificacionUoId() {
		return calificacionUoId;
	}
	public void setCalificacionUoId(Long calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	public Long getCoId() {
		return coId;
	}
	public void setCoId(Long coId) {
		this.coId = coId;
	}
	public Integer getSecuenciaMercancia() {
		return secuenciaMercancia;
	}
	public void setSecuenciaMercancia(Integer secuenciaMercancia) {
		this.secuenciaMercancia = secuenciaMercancia;
	}
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public Integer getTipoRolDj() {
		return tipoRolDj;
	}
	public void setTipoRolDj(Integer tipoRolDj) {
		this.tipoRolDj = tipoRolDj;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public Long getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Long partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public String getCumpleTotalmenteObtenido() {
		return cumpleTotalmenteObtenido;
	}
	public void setCumpleTotalmenteObtenido(String cumpleTotalmenteObtenido) {
		this.cumpleTotalmenteObtenido = cumpleTotalmenteObtenido;
	}
	public String getCumpleCriterioCambioClasif() {
		return cumpleCriterioCambioClasif;
	}
	public void setCumpleCriterioCambioClasif(String cumpleCriterioCambioClasif) {
		this.cumpleCriterioCambioClasif = cumpleCriterioCambioClasif;
	}
	public String getDj() {
		return dj;
	}
	public void setDj(String dj) {
		this.dj = dj;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public String getAnulado() {
		return anulado;
	}
	public void setAnulado(String anulado) {
		this.anulado = anulado;
	}
	public Integer getTipoAnulacionDj() {
		return tipoAnulacionDj;
	}
	public void setTipoAnulacionDj(Integer tipoAnulacionDj) {
		this.tipoAnulacionDj = tipoAnulacionDj;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public Date getFechaAnulacion() {
		return fechaAnulacion;
	}
	public void setFechaAnulacion(Date fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}
	public Date getFechaDenegacion() {
		return fechaDenegacion;
	}
	public void setFechaDenegacion(Date fechaDenegacion) {
		this.fechaDenegacion = fechaDenegacion;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	public String getPartidaSegunAcuerdo() {
		return partidaSegunAcuerdo;
	}
	public void setPartidaSegunAcuerdo(String partidaSegunAcuerdo) {
		this.partidaSegunAcuerdo = partidaSegunAcuerdo;
	}
	public Integer getEntidadId() {
		return entidadId;
	}
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public Integer getSecuenciaNorma() {
		return secuenciaNorma;
	}
	public void setSecuenciaNorma(Integer secuenciaNorma) {
		this.secuenciaNorma = secuenciaNorma;
	}
	public Integer getSecuenciaOrigen() {
		return secuenciaOrigen;
	}
	public void setSecuenciaOrigen(Integer secuenciaOrigen) {
		this.secuenciaOrigen = secuenciaOrigen;
	}
	public String getDetalleDenegacion() {
		return detalleDenegacion;
	}
	public void setDetalleDenegacion(String detalleDenegacion) {
		this.detalleDenegacion = detalleDenegacion;
	}
	public BigDecimal getDemasGasto() {
		return demasGasto;
	}
	public void setDemasGasto(BigDecimal demasGasto) {
		this.demasGasto = demasGasto;
	}
	public Integer getProductorTipoDocumento() {
		return productorTipoDocumento;
	}
	public void setProductorTipoDocumento(Integer productorTipoDocumento) {
		this.productorTipoDocumento = productorTipoDocumento;
	}
	public String getProductorNumeroDocumento() {
		return productorNumeroDocumento;
	}
	public void setProductorNumeroDocumento(String productorNumeroDocumento) {
		this.productorNumeroDocumento = productorNumeroDocumento;
	}
	public String getDenominacionArancelaria() {
		return denominacionArancelaria;
	}
	public void setDenominacionArancelaria(String denominacionArancelaria) {
		this.denominacionArancelaria = denominacionArancelaria;
	}
	public String getCriterioOrigenCertificado() {
		return criterioOrigenCertificado;
	}
	public void setCriterioOrigenCertificado(String criterioOrigenCertificado) {
		this.criterioOrigenCertificado = criterioOrigenCertificado;
	}
	public String getTienePoderPorProductor() {
		return tienePoderPorProductor;
	}
	public void setTienePoderPorProductor(String tienePoderPorProductor) {
		this.tienePoderPorProductor = tienePoderPorProductor;
	}
	public Date getFechaAceptacionValidacion() {
		return fechaAceptacionValidacion;
	}
	public void setFechaAceptacionValidacion(Date fechaAceptacionValidacion) {
		this.fechaAceptacionValidacion = fechaAceptacionValidacion;
	}
	public Date getFechaFinValidacion() {
		return fechaFinValidacion;
	}
	public void setFechaFinValidacion(Date fechaFinValidacion) {
		this.fechaFinValidacion = fechaFinValidacion;
	}
	public BigDecimal getPesoNetoMercancia() {
		return pesoNetoMercancia;
	}
	public void setPesoNetoMercancia(BigDecimal pesoNetoMercancia) {
		this.pesoNetoMercancia = pesoNetoMercancia;
	}
	public Long getUsuarioIdProductorValidador() {
		return usuarioIdProductorValidador;
	}
	public void setUsuarioIdProductorValidador(Long usuarioIdProductorValidador) {
		this.usuarioIdProductorValidador = usuarioIdProductorValidador;
	}
	public Integer getPaisIsoIdPorAcuerdoInt() {
		return paisIsoIdPorAcuerdoInt;
	}
	public void setPaisIsoIdPorAcuerdoInt(Integer paisIsoIdPorAcuerdoInt) {
		this.paisIsoIdPorAcuerdoInt = paisIsoIdPorAcuerdoInt;
	}
	public Long getOrden() {
		return orden;
	}
	public void setOrden(Long orden) {
		this.orden = orden;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public BigDecimal getPorcentajeDemasGastos() {
		return porcentajeDemasGastos;
	}
	public void setPorcentajeDemasGastos(BigDecimal porcentajeDemasGastos) {
		this.porcentajeDemasGastos = porcentajeDemasGastos;
	}
	public BigDecimal getValorUs() {
		return valorUs;
	}
	public void setValorUs(BigDecimal valorUs) {
		this.valorUs = valorUs;
	}
	public String getCumpleOtroCriterio() {
		return cumpleOtroCriterio;
	}
	public void setCumpleOtroCriterio(String cumpleOtroCriterio) {
		this.cumpleOtroCriterio = cumpleOtroCriterio;
	}
	public String getAceptacion() {
		return aceptacion;
	}
	public void setAceptacion(String aceptacion) {
		this.aceptacion = aceptacion;
	}
	public BigDecimal getPorcentajeSegunCriterio() {
		return porcentajeSegunCriterio;
	}
	public void setPorcentajeSegunCriterio(BigDecimal porcentajeSegunCriterio) {
		this.porcentajeSegunCriterio = porcentajeSegunCriterio;
	}
	public BigDecimal getValorUsFabrica() {
		return valorUsFabrica;
	}
	public void setValorUsFabrica(BigDecimal valorUsFabrica) {
		this.valorUsFabrica = valorUsFabrica;
	}
	public String getInformacionCompleta() {
		return informacionCompleta;
	}
	public void setInformacionCompleta(String informacionCompleta) {
		this.informacionCompleta = informacionCompleta;
	}
	public String getCopiaXModificacion() {
		return copiaXModificacion;
	}
	public void setCopiaXModificacion(String copiaXModificacion) {
		this.copiaXModificacion = copiaXModificacion;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getCantidadUmRefer() {
		return cantidadUmRefer;
	}
	public void setCantidadUmRefer(String cantidadUmRefer) {
		this.cantidadUmRefer = cantidadUmRefer;
	}
	public String getPaisIsoIdPorAcuerdoIntCodigo() {
		return paisIsoIdPorAcuerdoIntCodigo;
	}
	public void setPaisIsoIdPorAcuerdoIntCodigo(String paisIsoIdPorAcuerdoIntCodigo) {
		this.paisIsoIdPorAcuerdoIntCodigo = paisIsoIdPorAcuerdoIntCodigo;
	}
	public String getAcuerdoInternacionalCodigo() {
		return acuerdoInternacionalCodigo;
	}
	public void setAcuerdoInternacionalCodigo(String acuerdoInternacionalCodigo) {
		this.acuerdoInternacionalCodigo = acuerdoInternacionalCodigo;
	}
	public Integer getEntidadCodigo() {
		return entidadCodigo;
	}
	public void setEntidadCodigo(Integer entidadCodigo) {
		this.entidadCodigo = entidadCodigo;
	}
	public Integer getSedeCodigo() {
		return sedeCodigo;
	}
	public void setSedeCodigo(Integer sedeCodigo) {
		this.sedeCodigo = sedeCodigo;
	}
	public String getUnidadMedidaCodigo() {
		return unidadMedidaCodigo;
	}
	public void setUnidadMedidaCodigo(String unidadMedidaCodigo) {
		this.unidadMedidaCodigo = unidadMedidaCodigo;
	}
	public String getNumeroDocumentoSolicitante() {
		return numeroDocumentoSolicitante;
	}
	public void setNumeroDocumentoSolicitante(String numeroDocumentoSolicitante) {
		this.numeroDocumentoSolicitante = numeroDocumentoSolicitante;
	}
	public Integer getSecuenciaDeclaracionJurada() {
		return secuenciaDeclaracionJurada;
	}
	public void setSecuenciaDeclaracionJurada(Integer secuenciaDeclaracionJurada) {
		this.secuenciaDeclaracionJurada = secuenciaDeclaracionJurada;
	}
	public String getDireccionAdicional() {
		return direccionAdicional;
	}
	public void setDireccionAdicional(String direccionAdicional) {
		this.direccionAdicional = direccionAdicional;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

}
