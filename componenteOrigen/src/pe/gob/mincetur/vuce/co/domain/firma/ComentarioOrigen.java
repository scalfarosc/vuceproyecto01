package pe.gob.mincetur.vuce.co.domain.firma;

public class ComentarioOrigen {

	private Boolean esTercero;
	private String pais;
	private String nombreNegocio;
	private String direccion;
	private String numeroFactura;
	private String fechaFactura;

	public Boolean getEsTercero() {
		return esTercero;
	}
	public void setEsTercero(Boolean esTercero) {
		this.esTercero = esTercero;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public String getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(String fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	
}
