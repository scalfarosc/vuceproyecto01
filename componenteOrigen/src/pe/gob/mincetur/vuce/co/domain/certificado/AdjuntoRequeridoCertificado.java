package pe.gob.mincetur.vuce.co.domain.certificado;

public class AdjuntoRequeridoCertificado {

	private Integer certificadoFormatoId;
	
	private Integer adjuntoRequeridoCertificado;
	
	private String descripcion;
	
	private String obligatorio;
	
	private String estado;

	public Integer getCertificadoFormatoId() {
		return certificadoFormatoId;
	}

	public void setCertificadoFormatoId(Integer certificadoFormatoId) {
		this.certificadoFormatoId = certificadoFormatoId;
	}

	public Integer getAdjuntoRequeridoCertificado() {
		return adjuntoRequeridoCertificado;
	}

	public void setAdjuntoRequeridoCertificado(Integer adjuntoRequeridoCertificado) {
		this.adjuntoRequeridoCertificado = adjuntoRequeridoCertificado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObligatorio() {
		return obligatorio;
	}

	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
