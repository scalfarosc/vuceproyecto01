package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pe.gob.mincetur.vuce.co.domain.Adjunto;

/**
 * Clase de datos de Material de Declaración Jurada
 * @author Erick Oscátegui
 */

public class DeclaracionJuradaMaterial {

	private Long djId;
	private Integer secuenciaMaterial;
	private Integer itemMaterial;
	private String descripcion;
	private Long partidaArancelaria;
	private String partidaArancelariaDesc;
	private Integer paisProcedencia;
	private Integer paisOrigen;
	private String fabricanteNombre;
	private Integer fabricanteDocumentoTipo;
	private String fabricanteNumeroDocumento;
	private Integer umFisicaId;
	private BigDecimal cantidad;
	private BigDecimal 	valorUs;
	private BigDecimal porcentajeValor;
	private BigDecimal porcentajeSegunAcuerdo;
	private Integer indOrigen;
	private BigDecimal pesoNeto;
	private String partidaSegunAcuerdo;
	
	//20140825_JMC_Intercambio_Empresa
	private Integer umFisicaCodigo;	
	private Long adjuntoId;
	private String nombreAdjunto;	
	private Long mct005TmpId;
	private String paisProcedenciaCodigo;
	private String paisOrigenCodigo;
	private List<Adjunto> listAdjunto = new ArrayList<Adjunto>();

	
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public Integer getSecuenciaMaterial() {
		return secuenciaMaterial;
	}
	public void setSecuenciaMaterial(Integer secuenciaMaterial) {
		this.secuenciaMaterial = secuenciaMaterial;
	}
	public Integer getItemMaterial() {
		return itemMaterial;
	}
	public void setItemMaterial(Integer itemMaterial) {
		this.itemMaterial = itemMaterial;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Long partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public String getPartidaArancelariaDesc() {
		return partidaArancelariaDesc;
	}
	public void setPartidaArancelariaDesc(String partidaArancelariaDesc) {
		this.partidaArancelariaDesc = partidaArancelariaDesc;
	}
	public Integer getPaisProcedencia() {
		return paisProcedencia;
	}
	public void setPaisProcedencia(Integer paisProcedencia) {
		this.paisProcedencia = paisProcedencia;
	}
	public Integer getPaisOrigen() {
		return paisOrigen;
	}
	public void setPaisOrigen(Integer paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	public String getFabricanteNombre() {
		return fabricanteNombre;
	}
	public void setFabricanteNombre(String fabricanteNombre) {
		this.fabricanteNombre = fabricanteNombre;
	}
	public Integer getFabricanteDocumentoTipo() {
		return fabricanteDocumentoTipo;
	}
	public void setFabricanteDocumentoTipo(Integer fabricanteDocumentoTipo) {
		this.fabricanteDocumentoTipo = fabricanteDocumentoTipo;
	}
	public String getFabricanteNumeroDocumento() {
		return fabricanteNumeroDocumento;
	}
	public void setFabricanteNumeroDocumento(String fabricanteNumeroDocumento) {
		this.fabricanteNumeroDocumento = fabricanteNumeroDocumento;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getIndOrigen() {
		return indOrigen;
	}
	public void setIndOrigen(Integer indOrigen) {
		this.indOrigen = indOrigen;
	}
	public BigDecimal getPesoNeto() {
		return pesoNeto;
	}
	public void setPesoNeto(BigDecimal pesoNeto) {
		this.pesoNeto = pesoNeto;
	}
	public BigDecimal getValorUs() {
		return valorUs;
	}
	public void setValorUs(BigDecimal valorUs) {
		this.valorUs = valorUs;
	}
	public BigDecimal getPorcentajeValor() {
		return porcentajeValor;
	}
	public void setPorcentajeValor(BigDecimal porcentajeValor) {
		this.porcentajeValor = porcentajeValor;
	}
	public BigDecimal getPorcentajeSegunAcuerdo() {
		return porcentajeSegunAcuerdo;
	}
	public void setPorcentajeSegunAcuerdo(BigDecimal porcentajeSegunAcuerdo) {
		this.porcentajeSegunAcuerdo = porcentajeSegunAcuerdo;
	}
	public String getPartidaSegunAcuerdo() {
		return partidaSegunAcuerdo;
	}
	public void setPartidaSegunAcuerdo(String partidaSegunAcuerdo) {
		this.partidaSegunAcuerdo = partidaSegunAcuerdo;
	}
	public Integer getUmFisicaCodigo() {
		return umFisicaCodigo;
	}
	public void setUmFisicaCodigo(Integer umFisicaCodigo) {
		this.umFisicaCodigo = umFisicaCodigo;
	}
	public Long getAdjuntoId() {
		return adjuntoId;
	}
	public void setAdjuntoId(Long adjuntoId) {
		this.adjuntoId = adjuntoId;
	}
	public String getNombreAdjunto() {
		return nombreAdjunto;
	}
	public void setNombreAdjunto(String nombreAdjunto) {
		this.nombreAdjunto = nombreAdjunto;
	}
	public Long getMct005TmpId() {
		return mct005TmpId;
	}
	public void setMct005TmpId(Long mct005TmpId) {
		this.mct005TmpId = mct005TmpId;
	}
	public String getPaisProcedenciaCodigo() {
		return paisProcedenciaCodigo;
	}
	public void setPaisProcedenciaCodigo(String paisProcedenciaCodigo) {
		this.paisProcedenciaCodigo = paisProcedenciaCodigo;
	}
	public String getPaisOrigenCodigo() {
		return paisOrigenCodigo;
	}
	public void setPaisOrigenCodigo(String paisOrigenCodigo) {
		this.paisOrigenCodigo = paisOrigenCodigo;
	}
	public List<Adjunto> getListAdjunto() {
		return listAdjunto;
	}
	public void setListAdjunto(List<Adjunto> listAdjunto) {
		this.listAdjunto = listAdjunto;
	}
	public boolean addAdjunto(Adjunto o){
		return listAdjunto.add(o);
	}	

}
