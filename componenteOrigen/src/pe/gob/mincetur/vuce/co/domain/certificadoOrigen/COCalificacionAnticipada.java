package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

public class COCalificacionAnticipada extends CertificadoOrigen{

	private Long            djId;
	private Long 			calificacionUoId;

	public Long getDjId() {
		return djId;
	}

	public void setDjId(Long djId) {
		this.djId = djId;
	}

	public Long getCalificacionUoId() {
		return calificacionUoId;
	}

	public void setCalificacionUoId(Long calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	
}
