package pe.gob.mincetur.vuce.co.domain;

public class AdjuntoRequerido {
	
	private Integer       formatoId;
	private Integer       adjuntoRequerido;
	private String        descripcion;
	private String        obligatorio;
	private String        estado;
	private String        permiteAdicional;
	public Integer getFormatoId() {
		return formatoId;
	}
	public void setFormatoId(Integer formatoId) {
		this.formatoId = formatoId;
	}
	public Integer getAdjuntoRequerido() {
		return adjuntoRequerido;
	}
	public void setAdjuntoRequerido(Integer adjuntoRequerido) {
		this.adjuntoRequerido = adjuntoRequerido;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getObligatorio() {
		return obligatorio;
	}
	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPermiteAdicional() {
		return permiteAdicional;
	}
	public void setPermiteAdicional(String permiteAdicional) {
		this.permiteAdicional = permiteAdicional;
	}
	
	

}
