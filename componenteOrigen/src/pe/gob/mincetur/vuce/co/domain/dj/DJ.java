package pe.gob.mincetur.vuce.co.domain.dj;

import java.math.BigDecimal;

public class DJ {
    
	private Long            djId;
	private Long            ordenId;
	private Integer         mto;
	private String          denominacion;
	private String          caracteristica;
	private Integer         umFisicaId;
	private String          plantaDireccion;
	private Integer         plantaDistritoId;
	private BigDecimal      demasGasto;
	private BigDecimal      valorFOBUnitarioTotal;
	private BigDecimal      valorExworkUnitarioTotal;
	private BigDecimal      tipoCambio;
	private String          esAcopiador;
	private BigDecimal      valorFOBTotalMaterial;
	private BigDecimal      valorExworkTotalMaterial;
	private Long            partidaArancelaria;
	private String          esExportador;
	private Integer			tipoUsuarioDj;
	private String          unidadMedida;
	private String          partidaArancelariaDesc;
	
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public Long getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public String getPlantaDireccion() {
		return plantaDireccion;
	}
	public void setPlantaDireccion(String plantaDireccion) {
		this.plantaDireccion = plantaDireccion;
	}
	public Integer getPlantaDistritoId() {
		return plantaDistritoId;
	}
	public void setPlantaDistritoId(Integer plantaDistritoId) {
		this.plantaDistritoId = plantaDistritoId;
	}
	public BigDecimal getDemasGasto() {
		return demasGasto;
	}
	public void setDemasGasto(BigDecimal demasGasto) {
		this.demasGasto = demasGasto;
	}
	public BigDecimal getValorFOBUnitarioTotal() {
		return valorFOBUnitarioTotal;
	}
	public void setValorFOBUnitarioTotal(BigDecimal valorFOBUnitarioTotal) {
		this.valorFOBUnitarioTotal = valorFOBUnitarioTotal;
	}
	public BigDecimal getValorExworkUnitarioTotal() {
		return valorExworkUnitarioTotal;
	}
	public void setValorExworkUnitarioTotal(BigDecimal valorExworkUnitarioTotal) {
		this.valorExworkUnitarioTotal = valorExworkUnitarioTotal;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getEsAcopiador() {
		return esAcopiador;
	}
	public void setEsAcopiador(String esAcopiador) {
		this.esAcopiador = esAcopiador;
	}
	public BigDecimal getValorFOBTotalMaterial() {
		return valorFOBTotalMaterial;
	}
	public void setValorFOBTotalMaterial(BigDecimal valorFOBTotalMaterial) {
		this.valorFOBTotalMaterial = valorFOBTotalMaterial;
	}
	public BigDecimal getValorExworkTotalMaterial() {
		return valorExworkTotalMaterial;
	}
	public void setValorExworkTotalMaterial(BigDecimal valorExworkTotalMaterial) {
		this.valorExworkTotalMaterial = valorExworkTotalMaterial;
	}
	public Long getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Long partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public String getEsExportador() {
		return esExportador;
	}
	public void setEsExportador(String esExportador) {
		this.esExportador = esExportador;
	}
	public Integer getTipoUsuarioDj() {
		return tipoUsuarioDj;
	}
	public void setTipoUsuarioDj(Integer tipoUsuarioDj) {
		this.tipoUsuarioDj = tipoUsuarioDj;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getPartidaArancelariaDesc() {
		return partidaArancelariaDesc;
	}
	public void setPartidaArancelariaDesc(String partidaArancelariaDesc) {
		this.partidaArancelariaDesc = partidaArancelariaDesc;
	}

}
