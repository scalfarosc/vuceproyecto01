package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.math.BigDecimal;

/**
 * Clase de datos de Mercanc�a
 * @author Erick Osc�tegui
 */

public class CertificadoOrigenMercancia {

	private Long calificacionUoId;
	private Long coId;
	private Integer secuenciaMercancia;
	private Integer itemCoMercancia;
	private Long djId;
	private String descripcion;
	private Integer umFisicaId;
	private BigDecimal peso;
	private BigDecimal cantidad;
	private Integer tipoEnvase;
	private String descripcionTipoEnvase;
	private String estadoRegistro;
	private Integer secuenciaFactura;
	private String naladisa;
	private BigDecimal valorFacturadoUs;
	private String marcasNumeros;
	private String tipoBulto;
	private Integer cantidadBulto;
	private String estadoRegistroDJ;
	private Integer acuerdoInternacionalId;
	private Long ordenId;
	private Integer mto;
	private String esMercanciaGranel;
	private String sustentoCalifMercancia;
	private String umFisicaSegunFactura;
	private String partidaSegunAcuerdo; // PQT-04
	
	//20140825_JMC_Intercambio_Empresa
	private String numeroFactura;
	private String numeroDj;
	private String umFisicaCodigo;
	
	//20140911 Reexportacion
	private Integer secuenciaNorma;
	private Integer secuenciaOrigen;

	public Long getCalificacionUoId() {
		return calificacionUoId;
	}
	public void setCalificacionUoId(Long calificacionUoId) {
		this.calificacionUoId = calificacionUoId;
	}
	public Long getCoId() {
		return coId;
	}
	public void setCoId(Long coId) {
		this.coId = coId;
	}
	public Integer getSecuenciaMercancia() {
		return secuenciaMercancia;
	}
	public void setSecuenciaMercancia(Integer secuenciaMercancia) {
		this.secuenciaMercancia = secuenciaMercancia;
	}
	public Integer getItemCoMercancia() {
		return itemCoMercancia;
	}
	public void setItemCoMercancia(Integer itemCoMercancia) {
		this.itemCoMercancia = itemCoMercancia;
	}
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public BigDecimal getPeso() {
		return peso;
	}
	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getTipoEnvase() {
		return tipoEnvase;
	}
	public void setTipoEnvase(Integer tipoEnvase) {
		this.tipoEnvase = tipoEnvase;
	}
	public String getDescripcionTipoEnvase() {
		return descripcionTipoEnvase;
	}
	public void setDescripcionTipoEnvase(String descripcionTipoEnvase) {
		this.descripcionTipoEnvase = descripcionTipoEnvase;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public Integer getSecuenciaFactura() {
		return secuenciaFactura;
	}
	public void setSecuenciaFactura(Integer secuenciaFactura) {
		this.secuenciaFactura = secuenciaFactura;
	}
	public String getNaladisa() {
		return naladisa;
	}
	public void setNaladisa(String naladisa) {
		this.naladisa = naladisa;
	}
	public BigDecimal getValorFacturadoUs() {
		return valorFacturadoUs;
	}
	public void setValorFacturadoUs(BigDecimal valorFacturadoUs) {
		this.valorFacturadoUs = valorFacturadoUs;
	}
	public String getMarcasNumeros() {
		return marcasNumeros;
	}
	public void setMarcasNumeros(String marcasNumeros) {
		this.marcasNumeros = marcasNumeros;
	}
	public String getTipoBulto() {
		return tipoBulto;
	}
	public void setTipoBulto(String tipoBulto) {
		this.tipoBulto = tipoBulto;
	}
	public Integer getCantidadBulto() {
		return cantidadBulto;
	}
	public void setCantidadBulto(Integer cantidadBulto) {
		this.cantidadBulto = cantidadBulto;
	}
	public String getEstadoRegistroDJ() {
		return estadoRegistroDJ;
	}
	public void setEstadoRegistroDJ(String estadoRegistroDJ) {
		this.estadoRegistroDJ = estadoRegistroDJ;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public Long getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public String getEsMercanciaGranel() {
		return esMercanciaGranel;
	}
	public void setEsMercanciaGranel(String esMercanciaGranel) {
		this.esMercanciaGranel = esMercanciaGranel;
	}
	public String getSustentoCalifMercancia() {
		return sustentoCalifMercancia;
	}
	public void setSustentoCalifMercancia(String sustentoCalifMercancia) {
		this.sustentoCalifMercancia = sustentoCalifMercancia;
	}
	public String getUmFisicaSegunFactura() {
		return umFisicaSegunFactura;
	}
	public void setUmFisicaSegunFactura(String umFisicaSegunFactura) {
		this.umFisicaSegunFactura = umFisicaSegunFactura;
	}
	public String getPartidaSegunAcuerdo() {
		return partidaSegunAcuerdo;
	}
	public void setPartidaSegunAcuerdo(String partidaSegunAcuerdo) {
		this.partidaSegunAcuerdo = partidaSegunAcuerdo;
	}
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public String getNumeroDj() {
		return numeroDj;
	}
	public void setNumeroDj(String numeroDj) {
		this.numeroDj = numeroDj;
	}
	public String getUmFisicaCodigo() {
		return umFisicaCodigo;
	}
	public void setUmFisicaCodigo(String umFisicaCodigo) {
		this.umFisicaCodigo = umFisicaCodigo;
	}
	public Integer getSecuenciaNorma() {
		return secuenciaNorma;
	}
	public void setSecuenciaNorma(Integer secuenciaNorma) {
		this.secuenciaNorma = secuenciaNorma;
	}
	public Integer getSecuenciaOrigen() {
		return secuenciaOrigen;
	}
	public void setSecuenciaOrigen(Integer secuenciaOrigen) {
		this.secuenciaOrigen = secuenciaOrigen;
	}  
	
}
