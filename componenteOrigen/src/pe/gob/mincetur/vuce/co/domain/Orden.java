package pe.gob.mincetur.vuce.co.domain;

import java.util.Date;

/**
*Objeto :	Orden
*Descripcion :	Clase Entidad
*Fecha de Creacion :	
*Autor :	vuce - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		05/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ. 
*/

public class Orden {

    private Integer tce;

    private Integer idFormato;

    private Integer idTupa;

    private Long orden;

    private Integer mto;

    private Integer suce;

    private Integer numOrden;

    private Date fechaRegistro;

    private Date fechaRegistroOrden;

    private Date fechaActualizacion;

    private String estadoRegistro;

    private String bloqueado;

    private String transmitido;

    private String cerrada;

    private Integer idUsuario;

    private Long idFormatoEntidad; // Es el COId

    private String desistido;

    private Integer modoTramite;

    private String tipoOperacion;

    private String modificacionSuceXMto;

    private String vigente;

    private Integer idTasa;

    private Integer idAcuerdo;

    private Integer idPais;

    private Integer idEntidadCertificadora;

    private String nombreEntidad;

    private String nombreFormato;

    private String nombreAcuerdo;

    private String nombreSede;

    private String nombrePais;
    
    private String certificadoOrigen;
    
    private String certificadoReexportacion;
    
    private String versionFormato;

    public Integer getIdAcuerdo() {
        return idAcuerdo;
    }

    public void setIdAcuerdo(Integer idAcuerdo) {
        this.idAcuerdo = idAcuerdo;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public Integer getIdEntidadCertificadora() {
        return idEntidadCertificadora;
    }

    public void setIdEntidadCertificadora(Integer idEntidadCertificadora) {
        this.idEntidadCertificadora = idEntidadCertificadora;
    }

    public Integer getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Integer idFormato) {
        this.idFormato = idFormato;
    }

    public Integer getIdTupa() {
        return idTupa;
    }

    public void setIdTupa(Integer idTupa) {
        this.idTupa = idTupa;
    }

    public Integer getTce() {
        return tce;
    }

    public void setTce(Integer tce) {
        this.tce = tce;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public Integer getSuce() {
        return suce;
    }

    public void setSuce(Integer suce) {
        this.suce = suce;
    }

    public Integer getMto() {
        return mto;
    }

    public void setMto(Integer mto) {
        this.mto = mto;
    }

    public Integer getNumOrden() {
        return numOrden;
    }

    public void setNumOrden(Integer numOrden) {
        this.numOrden = numOrden;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaRegistroOrden() {
        return fechaRegistroOrden;
    }

    public void setFechaRegistroOrden(Date fechaRegistroOrden) {
        this.fechaRegistroOrden = fechaRegistroOrden;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getTransmitido() {
        return transmitido;
    }

    public void setTransmitido(String transmitido) {
        this.transmitido = transmitido;
    }

    public String getCerrada() {
        return cerrada;
    }

    public void setCerrada(String cerrada) {
        this.cerrada = cerrada;
    }

    public String getEstadoRegistro() {
        return estadoRegistro;
    }

    public void setEstadoRegistro(String estadoRegistro) {
        this.estadoRegistro = estadoRegistro;
    }

    public Long getIdFormatoEntidad() {
        return idFormatoEntidad;
    }

    public void setIdFormatoEntidad(Long idFormatoEntidad) {
        this.idFormatoEntidad = idFormatoEntidad;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDesistido() {
        return desistido;
    }

    public void setDesistido(String desistido) {
        this.desistido = desistido;
    }

    public Integer getModoTramite() {
        return modoTramite;
    }

    public void setModoTramite(Integer modoTramite) {
        this.modoTramite = modoTramite;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getModificacionSuceXMto() {
        return modificacionSuceXMto;
    }

    public void setModificacionSuceXMto(String modificacionSuceXMto) {
        this.modificacionSuceXMto = modificacionSuceXMto;
    }

    public String getVigente() {
        return vigente;
    }

    public void setVigente(String vigente) {
        this.vigente = vigente;
    }

    public Integer getIdTasa() {
        return idTasa;
    }

    public void setIdTasa(Integer idTasa) {
        this.idTasa = idTasa;
    }

	public String getNombreEntidad() {
		return nombreEntidad;
	}

	public void setNombreEntidad(String nombreEntidad) {
		this.nombreEntidad = nombreEntidad;
	}

	public String getNombreFormato() {
		return nombreFormato;
	}

	public void setNombreFormato(String nombreFormato) {
		this.nombreFormato = nombreFormato;
	}

	public String getNombreAcuerdo() {
		return nombreAcuerdo;
	}

	public void setNombreAcuerdo(String nombreAcuerdo) {
		this.nombreAcuerdo = nombreAcuerdo;
	}

	public String getNombreSede() {
		return nombreSede;
	}

	public void setNombreSede(String nombreSede) {
		this.nombreSede = nombreSede;
	}

	public String getNombrePais() {
		return nombrePais;
	}

	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}

	public String getCertificadoOrigen() {
		return certificadoOrigen;
	}

	public void setCertificadoOrigen(String certificadoOrigen) {
		this.certificadoOrigen = certificadoOrigen;
	}

	public String getCertificadoReexportacion() {
		return certificadoReexportacion;
	}

	public void setCertificadoReexportacion(String certificadoReexportacion) {
		this.certificadoReexportacion = certificadoReexportacion;
	}

	public String getVersionFormato() {
		return versionFormato;
	}

	public void setVersionFormato(String versionFormato) {
		this.versionFormato = versionFormato;
	}
	//Ticket 149343 GCHAVEZ-05/07/2019
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Orden [tce=");
		builder.append(tce);
		builder.append(", idFormato=");
		builder.append(idFormato);
		builder.append(", idTupa=");
		builder.append(idTupa);
		builder.append(", orden=");
		builder.append(orden);
		builder.append(", mto=");
		builder.append(mto);
		builder.append(", suce=");
		builder.append(suce);
		builder.append(", numOrden=");
		builder.append(numOrden);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", fechaRegistroOrden=");
		builder.append(fechaRegistroOrden);
		builder.append(", fechaActualizacion=");
		builder.append(fechaActualizacion);
		builder.append(", estadoRegistro=");
		builder.append(estadoRegistro);
		builder.append(", bloqueado=");
		builder.append(bloqueado);
		builder.append(", transmitido=");
		builder.append(transmitido);
		builder.append(", cerrada=");
		builder.append(cerrada);
		builder.append(", idUsuario=");
		builder.append(idUsuario);
		builder.append(", idFormatoEntidad=");
		builder.append(idFormatoEntidad);
		builder.append(", desistido=");
		builder.append(desistido);
		builder.append(", modoTramite=");
		builder.append(modoTramite);
		builder.append(", tipoOperacion=");
		builder.append(tipoOperacion);
		builder.append(", modificacionSuceXMto=");
		builder.append(modificacionSuceXMto);
		builder.append(", vigente=");
		builder.append(vigente);
		builder.append(", idTasa=");
		builder.append(idTasa);
		builder.append(", idAcuerdo=");
		builder.append(idAcuerdo);
		builder.append(", idPais=");
		builder.append(idPais);
		builder.append(", idEntidadCertificadora=");
		builder.append(idEntidadCertificadora);
		builder.append(", nombreEntidad=");
		builder.append(nombreEntidad);
		builder.append(", nombreFormato=");
		builder.append(nombreFormato);
		builder.append(", nombreAcuerdo=");
		builder.append(nombreAcuerdo);
		builder.append(", nombreSede=");
		builder.append(nombreSede);
		builder.append(", nombrePais=");
		builder.append(nombrePais);
		builder.append(", certificadoOrigen=");
		builder.append(certificadoOrigen);
		builder.append(", certificadoReexportacion=");
		builder.append(certificadoReexportacion);
		builder.append(", versionFormato=");
		builder.append(versionFormato);
		builder.append("]");
		return builder.toString();
	}
	//Ticket 149343 GCHAVEZ-05/07/2019
}
