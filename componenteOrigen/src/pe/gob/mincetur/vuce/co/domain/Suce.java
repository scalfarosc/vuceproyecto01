package pe.gob.mincetur.vuce.co.domain;

import java.util.Date;

public class Suce {
    private Integer tce;
    private Integer suce;
    private Integer mts;
    private Integer ano;
    private Integer numSuce;
    private Date fechaRegistro;
    private String bloqueado;
    private String cerrada;
    private String expediente;
    private Integer idFormato;
    private Integer idTupa;
    private Integer idOrden;
    private Integer modificacionSuceId;
    private Integer evaluador;
    private String estadoRegistro;
    private String edicion;
    private String desistido;
    private String modificacionSuceXMto;
    private Integer mensajeId;
    private String tipoOperacion;
    private String rectificaDr;
    private String modificacionSuceXMts;
    private String pendienteCalificacion;

    public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getTce() {
        return tce;
    }

    public void setTce(Integer tce) {
        this.tce = tce;
    }

    public Integer getSuce() {
        return suce;
    }

    public void setSuce(Integer suce) {
        this.suce = suce;
    }

    public Integer getMts() {
        return mts;
    }

    public void setMts(Integer mts) {
        this.mts = mts;
    }

    public Integer getNumSuce() {
        return numSuce;
    }

    public void setNumSuce(Integer numSuce) {
        this.numSuce = numSuce;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getCerrada() {
        return cerrada;
    }

    public void setCerrada(String cerrada) {
        this.cerrada = cerrada;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

	public Integer getIdFormato() {
		return idFormato;
	}

	public void setIdFormato(Integer idFormato) {
		this.idFormato = idFormato;
	}

	public Integer getIdTupa() {
		return idTupa;
	}

	public void setIdTupa(Integer idTupa) {
		this.idTupa = idTupa;
	}

	public Integer getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Integer idOrden) {
		this.idOrden = idOrden;
	}

	public Integer getModificacionSuceId() {
		return modificacionSuceId;
	}

	public void setModificacionSuceId(Integer modificacionSuceId) {
		this.modificacionSuceId = modificacionSuceId;
	}

	public Integer getEvaluador() {
		return evaluador;
	}

	public void setEvaluador(Integer evaluador) {
		this.evaluador = evaluador;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

    public String getEdicion() {
        return edicion;
    }

    public void setEdicion(String edicion) {
        this.edicion = edicion;
    }

    public String getDesistido() {
        return desistido;
    }

    public void setDesistido(String desistido) {
        this.desistido = desistido;
    }

	public String getModificacionSuceXMto() {
		return modificacionSuceXMto;
	}

	public void setModificacionSuceXMto(String modificacionSuceXMto) {
		this.modificacionSuceXMto = modificacionSuceXMto;
	}

	public Integer getMensajeId() {
		return mensajeId;
	}

	public void setMensajeId(Integer mensajeId) {
		this.mensajeId = mensajeId;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getRectificaDr() {
		return rectificaDr;
	}

	public void setRectificaDr(String rectificaDr) {
		this.rectificaDr = rectificaDr;
	}

	public String getModificacionSuceXMts() {
		return modificacionSuceXMts;
	}

	public void setModificacionSuceXMts(String modificacionSuceXMts) {
		this.modificacionSuceXMts = modificacionSuceXMts;
	}

	public String getPendienteCalificacion() {
		return pendienteCalificacion;
	}

	public void setPendienteCalificacion(String pendienteCalificacion) {
		this.pendienteCalificacion = pendienteCalificacion;
	}

}
