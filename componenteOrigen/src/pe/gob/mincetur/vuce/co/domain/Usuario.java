package pe.gob.mincetur.vuce.co.domain;

public class Usuario {

	private Integer usuarioId;

	private Integer tipoDocumento;

	private String numeroDocumento;

	private Integer tipoDocumentoUS;

	private String numeroDocumentoUS;

	private String usuarioSol;

	private String nombre;

	private String email;
	
	private String recibeNotificacion;

	private String telefono;
	
	private String celular;

	private String fax;

	private String estado;

	private String direccion;
	
	private String referencia;
	
	private String area;
	
	private String subArea;
	
	private String cargo;

	private Integer distritoId;

	private Integer usuarioTipoId;

	private String principal;

	private String empresa;

	private Integer tipoPersona;

	private String esAgenteAduanas;

	private String esLaboratorio;
	
	private String codigoEntidadSunat;
	
	private String actividad;
	
	private Integer empresaId;
	
	private Integer entidadId;
	
	private String ingresoMR;
	
	private String ingresoUO;

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Integer getTipoDocumentoUS() {
		return tipoDocumentoUS;
	}

	public void setTipoDocumentoUS(Integer tipoDocumentoUS) {
		this.tipoDocumentoUS = tipoDocumentoUS;
	}

	public String getNumeroDocumentoUS() {
		return numeroDocumentoUS;
	}

	public void setNumeroDocumentoUS(String numeroDocumentoUS) {
		this.numeroDocumentoUS = numeroDocumentoUS;
	}

	public String getUsuarioSol() {
		return usuarioSol;
	}

	public void setUsuarioSol(String usuarioSol) {
		this.usuarioSol = usuarioSol;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getDistritoId() {
		return distritoId;
	}

	public void setDistritoId(Integer distritoId) {
		this.distritoId = distritoId;
	}

	public Integer getUsuarioTipoId() {
		return usuarioTipoId;
	}

	public void setUsuarioTipoId(Integer usuarioTipoId) {
		this.usuarioTipoId = usuarioTipoId;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Integer getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(Integer tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getEsAgenteAduanas() {
		return esAgenteAduanas;
	}

	public void setEsAgenteAduanas(String esAgenteAduanas) {
		this.esAgenteAduanas = esAgenteAduanas;
	}

	public String getEsLaboratorio() {
		return esLaboratorio;
	}

	public void setEsLaboratorio(String esLaboratorio) {
		this.esLaboratorio = esLaboratorio;
	}

	public String getCodigoEntidadSunat() {
		return codigoEntidadSunat;
	}

	public void setCodigoEntidadSunat(String codigoEntidadSunat) {
		this.codigoEntidadSunat = codigoEntidadSunat;
	}

	public String getRecibeNotificacion() {
		return recibeNotificacion;
	}

	public void setRecibeNotificacion(String recibeNotificacion) {
		this.recibeNotificacion = recibeNotificacion;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSubArea() {
        return subArea;
    }

    public void setSubArea(String subArea) {
        this.subArea = subArea;
    }

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public Integer getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Integer empresaId) {
		this.empresaId = empresaId;
	}

	public Integer getEntidadId() {
		return entidadId;
	}

	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}

	public String getIngresoMR() {
		return ingresoMR;
	}

	public void setIngresoMR(String ingresoMR) {
		this.ingresoMR = ingresoMR;
	}

	public String getIngresoUO() {
		return ingresoUO;
	}

	public void setIngresoUO(String ingresoUO) {
		this.ingresoUO = ingresoUO;
	}
	
}
