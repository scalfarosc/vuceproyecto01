package pe.gob.mincetur.vuce.co.domain.firma;

public class FacturaTercerOpOrigen {

	private String orden;
	private String numero;
	private String fecha;	
	private String facturadoTercerOp;
	private String paisTercerOp;
	private String nombreOperadorTercerOp;
	private String direccionOperadorTercerOp;
	private String numeroFacturaTercerOp;
	private String fechaFacturaTercerOp;
	public String getOrden() {
		return orden;
	}
	public void setOrden(String orden) {
		this.orden = orden;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getFacturadoTercerOp() {
		return facturadoTercerOp;
	}
	public void setFacturadoTercerOp(String facturadoTercerOp) {
		this.facturadoTercerOp = facturadoTercerOp;
	}
	public String getPaisTercerOp() {
		return paisTercerOp;
	}
	public void setPaisTercerOp(String paisTercerOp) {
		this.paisTercerOp = paisTercerOp;
	}
	public String getNombreOperadorTercerOp() {
		return nombreOperadorTercerOp;
	}
	public void setNombreOperadorTercerOp(String nombreOperadorTercerOp) {
		this.nombreOperadorTercerOp = nombreOperadorTercerOp;
	}
	public String getDireccionOperadorTercerOp() {
		return direccionOperadorTercerOp;
	}
	public void setDireccionOperadorTercerOp(String direccionOperadorTercerOp) {
		this.direccionOperadorTercerOp = direccionOperadorTercerOp;
	}
	public String getNumeroFacturaTercerOp() {
		return numeroFacturaTercerOp;
	}
	public void setNumeroFacturaTercerOp(String numeroFacturaTercerOp) {
		this.numeroFacturaTercerOp = numeroFacturaTercerOp;
	}
	public String getFechaFacturaTercerOp() {
		return fechaFacturaTercerOp;
	}
	public void setFechaFacturaTercerOp(String fechaFacturaTercerOp) {
		this.fechaFacturaTercerOp = fechaFacturaTercerOp;
	}
		
	
}
