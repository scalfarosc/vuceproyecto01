package pe.gob.mincetur.vuce.co.domain;

public class AdjuntoCertificadoOrigen {

    private Long adjuntoId;

    private Long djId;
    
    private Integer secuenciaMaterial;
    
    private Long coId;
    
    private Integer adjuntoRequeridoUo;

    private byte [] archivo;

    private Integer adjuntoTipo;
    
    private String nombreArchivo;
    
	public Long getAdjuntoId() {
		return adjuntoId;
	}

	public void setAdjuntoId(Long adjuntoId) {
		this.adjuntoId = adjuntoId;
	}

	public Long getDjId() {
		return djId;
	}

	public void setDjId(Long djId) {
		this.djId = djId;
	}

	public Integer getSecuenciaMaterial() {
		return secuenciaMaterial;
	}

	public void setSecuenciaMaterial(Integer secuenciaMaterial) {
		this.secuenciaMaterial = secuenciaMaterial;
	}

	public Long getCoId() {
		return coId;
	}

	public void setCoId(Long coId) {
		this.coId = coId;
	}

	public Integer getAdjuntoRequeridoUo() {
		return adjuntoRequeridoUo;
	}

	public void setAdjuntoRequeridoUo(Integer adjuntoRequeridoUo) {
		this.adjuntoRequeridoUo = adjuntoRequeridoUo;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public Integer getAdjuntoTipo() {
		return adjuntoTipo;
	}

	public void setAdjuntoTipo(Integer adjuntoTipo) {
		this.adjuntoTipo = adjuntoTipo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
}
