package pe.gob.mincetur.vuce.co.domain;

public class Solicitante {

	private Long            usuarioId;
	private Integer         documentoTipo;
	private String          documentoTipoDesc;
	private String          numeroDocumento;
	private String          nombre;
	private String          email;
	private String          telefono;
	private String          fax;
	private String          direccion;
	private String          ubigeo;
	private String          referencia;
	private String          actividad;
	private String          celular;
	private Integer         representanteLegal;
	private Integer         personaTipo;
	private String          cargo;
    private String 			tipoEmpresaExterna;
    private boolean 		agente;
    private boolean 		laboratorio;
    private String          tipoPersona;
	private Integer         documentoTipoSec;
	private String          documentoTipoSecDesc;
	private String          numeroDocumentoSec;
	private String          recibeNotificacion; 
	//GCHAVEZ - 28/02/2020 - Correcci�n en el env�o del usuario sol para la Firma Digital.
	private String 			usuarioSOL;

	public Long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public Integer getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(Integer documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReferencia() {
		return referencia;
	}
	public String getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public Integer getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(Integer representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	public Integer getPersonaTipo() {
		return personaTipo;
	}
	public void setPersonaTipo(Integer personaTipo) {
		this.personaTipo = personaTipo;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getDocumentoTipoDesc() {
		return documentoTipoDesc;
	}
	public void setDocumentoTipoDesc(String documentoTipoDesc) {
		this.documentoTipoDesc = documentoTipoDesc;
	}
	public String getTipoEmpresaExterna() {
		return tipoEmpresaExterna;
	}
	public void setTipoEmpresaExterna(String tipoEmpresaExterna) {
		this.tipoEmpresaExterna = tipoEmpresaExterna;
	}
	public boolean isAgente() {
		return agente;
	}
	public void setAgente(boolean agente) {
		this.agente = agente;
	}
	public boolean isLaboratorio() {
		return laboratorio;
	}
	public void setLaboratorio(boolean laboratorio) {
		this.laboratorio = laboratorio;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public Integer getDocumentoTipoSec() {
		return documentoTipoSec;
	}
	public void setDocumentoTipoSec(Integer documentoTipoSec) {
		this.documentoTipoSec = documentoTipoSec;
	}
	public String getDocumentoTipoSecDesc() {
		return documentoTipoSecDesc;
	}
	public void setDocumentoTipoSecDesc(String documentoTipoSecDesc) {
		this.documentoTipoSecDesc = documentoTipoSecDesc;
	}
	public String getNumeroDocumentoSec() {
		return numeroDocumentoSec;
	}
	public void setNumeroDocumentoSec(String numeroDocumentoSec) {
		this.numeroDocumentoSec = numeroDocumentoSec;
	}
	public String getRecibeNotificacion() {
		return recibeNotificacion;
	}
	public void setRecibeNotificacion(String recibeNotificacion) {
		this.recibeNotificacion = recibeNotificacion;
	}
	public String getUsuarioSOL() {
		return usuarioSOL;
	}
	public void setUsuarioSOL(String usuarioSOL) {
		this.usuarioSOL = usuarioSOL;
	}

}
