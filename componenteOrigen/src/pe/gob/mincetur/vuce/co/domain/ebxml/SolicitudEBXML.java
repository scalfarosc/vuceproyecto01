package pe.gob.mincetur.vuce.co.domain.ebxml;

import java.util.Date;

import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;

public abstract class SolicitudEBXML extends EbXML {

    private int id;
    
    private String numeroSuce;
    
    private Date fechaEmision;
    
    private Solicitante usuario;
    
    private Solicitante solicitante;
    
    private RepresentanteLegal representante;
    
    private Solicitante empresaExterna;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumeroSuce() {
		return numeroSuce;
	}

	public void setNumeroSuce(String numeroSuce) {
		this.numeroSuce = numeroSuce;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Solicitante getUsuario() {
		return usuario;
	}

	public void setUsuario(Solicitante usuario) {
		this.usuario = usuario;
	}

	public Solicitante getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}

	public RepresentanteLegal getRepresentante() {
		return representante;
	}

	public void setRepresentante(RepresentanteLegal representante) {
		this.representante = representante;
	}

	public Solicitante getEmpresaExterna() {
		return empresaExterna;
	}

	public void setEmpresaExterna(Solicitante empresaExterna) {
		this.empresaExterna = empresaExterna;
	}
    
    
	
}
