package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

public class SCAcuerdo {
	
	private Long            calificacionDrId;
	private Integer         secuenciaAcuerdo;
	private Integer         paisIsoId;
	private Integer         acuerdoInternacionalId;
	private String          calificacion;
	private String          norma;
	private String          criterioOrigen;
	private String          motivoCalificacionNegativa;
	
	public Long getCalificacionDrId() {
		return calificacionDrId;
	}
	public void setCalificacionDrId(Long calificacionDrId) {
		this.calificacionDrId = calificacionDrId;
	}
	public Integer getSecuenciaAcuerdo() {
		return secuenciaAcuerdo;
	}
	public void setSecuenciaAcuerdo(Integer secuenciaAcuerdo) {
		this.secuenciaAcuerdo = secuenciaAcuerdo;
	}
	public Integer getPaisIsoId() {
		return paisIsoId;
	}
	public void setPaisIsoId(Integer paisIsoId) {
		this.paisIsoId = paisIsoId;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	public String getNorma() {
		return norma;
	}
	public void setNorma(String norma) {
		this.norma = norma;
	}
	public String getCriterioOrigen() {
		return criterioOrigen;
	}
	public void setCriterioOrigen(String criterioOrigen) {
		this.criterioOrigen = criterioOrigen;
	}
	public String getMotivoCalificacionNegativa() {
		return motivoCalificacionNegativa;
	}
	public void setMotivoCalificacionNegativa(String motivoCalificacionNegativa) {
		this.motivoCalificacionNegativa = motivoCalificacionNegativa;
	}
	
	public Boolean validateRequiredFields(){
		return 
		this.calificacion==null?false:
		true;
	}
	
}
