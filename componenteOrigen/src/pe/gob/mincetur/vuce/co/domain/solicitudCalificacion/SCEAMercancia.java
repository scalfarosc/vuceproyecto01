package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

import java.math.BigDecimal;

public class SCEAMercancia {

	private Long expautId;
	private Integer secuenciaMercancia;
	private String nombreTecnico;
	private String nombreComercial;
	private String clasificacionArancelaria;
	private String indDiscrepancia;
	private String indProductor;
	private String indExportador;
	private String indProductorExportador;
	private String domicilioPlantaProduccion;
	private BigDecimal valorFobMercancia;
	private BigDecimal valorExWorkMercancia;
	private String umMercancia;
	private String reglaOrigen;
	private String exigenciaAcuerdo;
	private String material; ///////////////////////////////
	private String indDocumentacionMaterial;
	private String indAcuerdoDeminimis;
	private String indOrigenDeminimis;
	private String indAcuerdoIntermedio;
	private String indOrigenIntermedio;
	private String componenteAplicable;
	private String determinaIntermedio;
	private String indIncluyeAccesorio;
	private BigDecimal valorFobAccesorio;
	private BigDecimal valorExWorkAccesorio;
	private String indIncluyeJuego;
	private String indOrigenJuego;
	private String indNoOrigenJuego;
	private BigDecimal valorFobJuego;
	private BigDecimal valorExWorkJuego;
	private String origenEnvase;
	private String tratamientoOrigenEnvase;
	private BigDecimal valorFobEnvase;
	private BigDecimal valorExWorkEnvase;
	private String indTransaccionValor;
	private String indAumentoValor;
	private String indReduccionValor;
	private String indOtroValor;
	private String otroValor;
	private Double porcentajeValorOriginario;
	private BigDecimal valorFobValor;
	private BigDecimal valorExWorkValor;
	private String indAplicaAjusteMaterial;
	private String calculoAjusteMaterial;
	private String indAplicaAjusteMercancia;
	private String calculoAjusteMercancia;
	private String indRealizaProceso;
	private String descripcionProceso;
	private String indContrataTercero;
	private String descripcionTercero;
	private String indProcesoTerritorio;
	private String descripcionProcesoTerritorio;
	private String paisProcesoTerritorio;
	private String indPaisProcesoTerritorio;
	private String sustentoProceso;
	private String sistemaControlInventario;
	private String indMercanciaFungible;
	private String indMercanciaOriginaria;
	private String medidasMercanciaOriginaria;
	private String indMaterialFungible;
	private String indLotesMixtos;
	private String inventarioLotesMixtos;
	private String registroAcuerdoLotesMixtos;

	public Long getExpautId() {
		return expautId;
	}

	public void setExpautId(Long expautId) {
		this.expautId = expautId;
	}

	public Integer getSecuenciaMercancia() {
		return secuenciaMercancia;
	}

	public void setSecuenciaMercancia(Integer secuenciaMercancia) {
		this.secuenciaMercancia = secuenciaMercancia;
	}

	public String getNombreTecnico() {
		return nombreTecnico;
	}

	public void setNombreTecnico(String nombreTecnico) {
		this.nombreTecnico = nombreTecnico;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getClasificacionArancelaria() {
		return clasificacionArancelaria;
	}

	public void setClasificacionArancelaria(String clasificacionArancelaria) {
		this.clasificacionArancelaria = clasificacionArancelaria;
	}

	public String getIndDiscrepancia() {
		return indDiscrepancia;
	}

	public void setIndDiscrepancia(String indDiscrepancia) {
		this.indDiscrepancia = indDiscrepancia;
	}

	public String getIndProductor() {
		return indProductor;
	}

	public void setIndProductor(String indProductor) {
		this.indProductor = indProductor;
	}

	public String getIndExportador() {
		return indExportador;
	}

	public void setIndExportador(String indExportador) {
		this.indExportador = indExportador;
	}

	public String getIndProductorExportador() {
		return indProductorExportador;
	}

	public void setIndProductorExportador(String indProductorExportador) {
		this.indProductorExportador = indProductorExportador;
	}

	public String getDomicilioPlantaProduccion() {
		return domicilioPlantaProduccion;
	}

	public void setDomicilioPlantaProduccion(String domicilioPlantaProduccion) {
		this.domicilioPlantaProduccion = domicilioPlantaProduccion;
	}

	public BigDecimal getValorFobMercancia() {
		return valorFobMercancia;
	}

	public void setValorFobMercancia(BigDecimal valorFobMercancia) {
		this.valorFobMercancia = valorFobMercancia;
	}

	public BigDecimal getValorExWorkMercancia() {
		return valorExWorkMercancia;
	}

	public void setValorExWorkMercancia(BigDecimal valorExWorkMercancia) {
		this.valorExWorkMercancia = valorExWorkMercancia;
	}

	public String getUmMercancia() {
		return umMercancia;
	}

	public void setUmMercancia(String umMercancia) {
		this.umMercancia = umMercancia;
	}

	public String getReglaOrigen() {
		return reglaOrigen;
	}

	public void setReglaOrigen(String reglaOrigen) {
		this.reglaOrigen = reglaOrigen;
	}

	public String getExigenciaAcuerdo() {
		return exigenciaAcuerdo;
	}

	public void setExigenciaAcuerdo(String exigenciaAcuerdo) {
		this.exigenciaAcuerdo = exigenciaAcuerdo;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getIndDocumentacionMaterial() {
		return indDocumentacionMaterial;
	}

	public void setIndDocumentacionMaterial(String indDocumentacionMaterial) {
		this.indDocumentacionMaterial = indDocumentacionMaterial;
	}

	public String getIndAcuerdoDeminimis() {
		return indAcuerdoDeminimis;
	}

	public void setIndAcuerdoDeminimis(String indAcuerdoDeminimis) {
		this.indAcuerdoDeminimis = indAcuerdoDeminimis;
	}

	public String getIndOrigenDeminimis() {
		return indOrigenDeminimis;
	}

	public void setIndOrigenDeminimis(String indOrigenDeminimis) {
		this.indOrigenDeminimis = indOrigenDeminimis;
	}

	public String getIndAcuerdoIntermedio() {
		return indAcuerdoIntermedio;
	}

	public void setIndAcuerdoIntermedio(String indAcuerdoIntermedio) {
		this.indAcuerdoIntermedio = indAcuerdoIntermedio;
	}

	public String getIndOrigenIntermedio() {
		return indOrigenIntermedio;
	}

	public void setIndOrigenIntermedio(String indOrigenIntermedio) {
		this.indOrigenIntermedio = indOrigenIntermedio;
	}

	public String getComponenteAplicable() {
		return componenteAplicable;
	}

	public void setComponenteAplicable(String componenteAplicable) {
		this.componenteAplicable = componenteAplicable;
	}

	public String getDeterminaIntermedio() {
		return determinaIntermedio;
	}

	public void setDeterminaIntermedio(String determinaIntermedio) {
		this.determinaIntermedio = determinaIntermedio;
	}

	public String getIndIncluyeAccesorio() {
		return indIncluyeAccesorio;
	}

	public void setIndIncluyeAccesorio(String indIncluyeAccesorio) {
		this.indIncluyeAccesorio = indIncluyeAccesorio;
	}

	public BigDecimal getValorFobAccesorio() {
		return valorFobAccesorio;
	}

	public void setValorFobAccesorio(BigDecimal valorFobAccesorio) {
		this.valorFobAccesorio = valorFobAccesorio;
	}

	public BigDecimal getValorExWorkAccesorio() {
		return valorExWorkAccesorio;
	}

	public void setValorExWorkAccesorio(BigDecimal valorExWorkAccesorio) {
		this.valorExWorkAccesorio = valorExWorkAccesorio;
	}

	public String getIndIncluyeJuego() {
		return indIncluyeJuego;
	}

	public void setIndIncluyeJuego(String indIncluyeJuego) {
		this.indIncluyeJuego = indIncluyeJuego;
	}

	public String getIndOrigenJuego() {
		return indOrigenJuego;
	}

	public void setIndOrigenJuego(String indOrigenJuego) {
		this.indOrigenJuego = indOrigenJuego;
	}

	public String getIndNoOrigenJuego() {
		return indNoOrigenJuego;
	}

	public void setIndNoOrigenJuego(String indNoOrigenJuego) {
		this.indNoOrigenJuego = indNoOrigenJuego;
	}

	public BigDecimal getValorFobJuego() {
		return valorFobJuego;
	}

	public void setValorFobJuego(BigDecimal valorFobJuego) {
		this.valorFobJuego = valorFobJuego;
	}

	public BigDecimal getValorExWorkJuego() {
		return valorExWorkJuego;
	}

	public void setValorExWorkJuego(BigDecimal valorExWorkJuego) {
		this.valorExWorkJuego = valorExWorkJuego;
	}

	public String getOrigenEnvase() {
		return origenEnvase;
	}

	public void setOrigenEnvase(String origenEnvase) {
		this.origenEnvase = origenEnvase;
	}

	public String getTratamientoOrigenEnvase() {
		return tratamientoOrigenEnvase;
	}

	public void setTratamientoOrigenEnvase(String tratamientoOrigenEnvase) {
		this.tratamientoOrigenEnvase = tratamientoOrigenEnvase;
	}

	public BigDecimal getValorFobEnvase() {
		return valorFobEnvase;
	}

	public void setValorFobEnvase(BigDecimal valorFobEnvase) {
		this.valorFobEnvase = valorFobEnvase;
	}

	public BigDecimal getValorExWorkEnvase() {
		return valorExWorkEnvase;
	}

	public void setValorExWorkEnvase(BigDecimal valorExWorkEnvase) {
		this.valorExWorkEnvase = valorExWorkEnvase;
	}

	public String getIndTransaccionValor() {
		return indTransaccionValor;
	}

	public void setIndTransaccionValor(String indTransaccionValor) {
		this.indTransaccionValor = indTransaccionValor;
	}

	public String getIndAumentoValor() {
		return indAumentoValor;
	}

	public void setIndAumentoValor(String indAumentoValor) {
		this.indAumentoValor = indAumentoValor;
	}

	public String getIndReduccionValor() {
		return indReduccionValor;
	}

	public void setIndReduccionValor(String indReduccionValor) {
		this.indReduccionValor = indReduccionValor;
	}

	public String getIndOtroValor() {
		return indOtroValor;
	}

	public void setIndOtroValor(String indOtroValor) {
		this.indOtroValor = indOtroValor;
	}

	public String getOtroValor() {
		return otroValor;
	}

	public void setOtroValor(String otroValor) {
		this.otroValor = otroValor;
	}

	public Double getPorcentajeValorOriginario() {
		return porcentajeValorOriginario;
	}

	public void setPorcentajeValorOriginario(Double porcentajeValorOriginario) {
		this.porcentajeValorOriginario = porcentajeValorOriginario;
	}

	public BigDecimal getValorFobValor() {
		return valorFobValor;
	}

	public void setValorFobValor(BigDecimal valorFobValor) {
		this.valorFobValor = valorFobValor;
	}

	public BigDecimal getValorExWorkValor() {
		return valorExWorkValor;
	}

	public void setValorExWorkValor(BigDecimal valorExWorkValor) {
		this.valorExWorkValor = valorExWorkValor;
	}

	public String getIndAplicaAjusteMaterial() {
		return indAplicaAjusteMaterial;
	}

	public void setIndAplicaAjusteMaterial(String indAplicaAjusteMaterial) {
		this.indAplicaAjusteMaterial = indAplicaAjusteMaterial;
	}

	public String getCalculoAjusteMaterial() {
		return calculoAjusteMaterial;
	}

	public void setCalculoAjusteMaterial(String calculoAjusteMaterial) {
		this.calculoAjusteMaterial = calculoAjusteMaterial;
	}

	public String getIndAplicaAjusteMercancia() {
		return indAplicaAjusteMercancia;
	}

	public void setIndAplicaAjusteMercancia(String indAplicaAjusteMercancia) {
		this.indAplicaAjusteMercancia = indAplicaAjusteMercancia;
	}

	public String getCalculoAjusteMercancia() {
		return calculoAjusteMercancia;
	}

	public void setCalculoAjusteMercancia(String calculoAjusteMercancia) {
		this.calculoAjusteMercancia = calculoAjusteMercancia;
	}

	public String getIndRealizaProceso() {
		return indRealizaProceso;
	}

	public void setIndRealizaProceso(String indRealizaProceso) {
		this.indRealizaProceso = indRealizaProceso;
	}

	public String getDescripcionProceso() {
		return descripcionProceso;
	}

	public void setDescripcionProceso(String descripcionProceso) {
		this.descripcionProceso = descripcionProceso;
	}

	public String getIndContrataTercero() {
		return indContrataTercero;
	}

	public void setIndContrataTercero(String indContrataTercero) {
		this.indContrataTercero = indContrataTercero;
	}

	public String getDescripcionTercero() {
		return descripcionTercero;
	}

	public void setDescripcionTercero(String descripcionTercero) {
		this.descripcionTercero = descripcionTercero;
	}

	public String getIndProcesoTerritorio() {
		return indProcesoTerritorio;
	}

	public void setIndProcesoTerritorio(String indProcesoTerritorio) {
		this.indProcesoTerritorio = indProcesoTerritorio;
	}

	public String getDescripcionProcesoTerritorio() {
		return descripcionProcesoTerritorio;
	}

	public void setDescripcionProcesoTerritorio(
			String descripcionProcesoTerritorio) {
		this.descripcionProcesoTerritorio = descripcionProcesoTerritorio;
	}

	public String getPaisProcesoTerritorio() {
		return paisProcesoTerritorio;
	}

	public void setPaisProcesoTerritorio(String paisProcesoTerritorio) {
		this.paisProcesoTerritorio = paisProcesoTerritorio;
	}

	public String getIndPaisProcesoTerritorio() {
		return indPaisProcesoTerritorio;
	}

	public void setIndPaisProcesoTerritorio(String indPaisProcesoTerritorio) {
		this.indPaisProcesoTerritorio = indPaisProcesoTerritorio;
	}

	public String getSustentoProceso() {
		return sustentoProceso;
	}

	public void setSustentoProceso(String sustentoProceso) {
		this.sustentoProceso = sustentoProceso;
	}

	public String getSistemaControlInventario() {
		return sistemaControlInventario;
	}

	public void setSistemaControlInventario(String sistemaControlInventario) {
		this.sistemaControlInventario = sistemaControlInventario;
	}

	public String getIndMercanciaFungible() {
		return indMercanciaFungible;
	}

	public void setIndMercanciaFungible(String indMercanciaFungible) {
		this.indMercanciaFungible = indMercanciaFungible;
	}

	public String getIndMercanciaOriginaria() {
		return indMercanciaOriginaria;
	}

	public void setIndMercanciaOriginaria(String indMercanciaOriginaria) {
		this.indMercanciaOriginaria = indMercanciaOriginaria;
	}

	public String getMedidasMercanciaOriginaria() {
		return medidasMercanciaOriginaria;
	}

	public void setMedidasMercanciaOriginaria(String medidasMercanciaOriginaria) {
		this.medidasMercanciaOriginaria = medidasMercanciaOriginaria;
	}

	public String getIndMaterialFungible() {
		return indMaterialFungible;
	}

	public void setIndMaterialFungible(String indMaterialFungible) {
		this.indMaterialFungible = indMaterialFungible;
	}

	public String getIndLotesMixtos() {
		return indLotesMixtos;
	}

	public void setIndLotesMixtos(String indLotesMixtos) {
		this.indLotesMixtos = indLotesMixtos;
	}

	public String getInventarioLotesMixtos() {
		return inventarioLotesMixtos;
	}

	public void setInventarioLotesMixtos(String inventarioLotesMixtos) {
		this.inventarioLotesMixtos = inventarioLotesMixtos;
	}

	public String getRegistroAcuerdoLotesMixtos() {
		return registroAcuerdoLotesMixtos;
	}

	public void setRegistroAcuerdoLotesMixtos(String registroAcuerdoLotesMixtos) {
		this.registroAcuerdoLotesMixtos = registroAcuerdoLotesMixtos;
	}

}
