package pe.gob.mincetur.vuce.co.domain;

public class Auditoria {
	
	private Integer usuarioId;
	
	private String esExtranet;
	
	private Integer modoTramite;
	
	private String sesionId;
	
	private String detalleConexion;
	
	private Integer orden;
	
	private Integer suce;	
	
	private Integer dr;
	
	private Integer recursoOrigen;

	private Integer tipoOrigen;
	
	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getEsExtranet() {
		return esExtranet;
	}

	public void setEsExtranet(String esExtranet) {
		this.esExtranet = esExtranet;
	}

	public Integer getModoTramite() {
		return modoTramite;
	}

	public void setModoTramite(Integer modoTramite) {
		this.modoTramite = modoTramite;
	}

	public String getSesionId() {
		return sesionId;
	}

	public void setSesionId(String sesionId) {
		this.sesionId = sesionId;
	}

	public String getDetalleConexion() {
		return detalleConexion;
	}

	public void setDetalleConexion(String detalleConexion) {
		this.detalleConexion = detalleConexion;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getSuce() {
		return suce;
	}

	public void setSuce(Integer suce) {
		this.suce = suce;
	}

	public Integer getDr() {
		return dr;
	}

	public void setDr(Integer dr) {
		this.dr = dr;
	}

	public Integer getRecursoOrigen() {
		return recursoOrigen;
	}

	public void setRecursoOrigen(Integer recursoOrigen) {
		this.recursoOrigen = recursoOrigen;
	}

	public Integer getTipoOrigen() {
		return tipoOrigen;
	}

	public void setTipoOrigen(Integer tipoOrigen) {
		this.tipoOrigen = tipoOrigen;
	}
	
	

			
}
