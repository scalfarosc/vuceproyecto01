package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

import java.util.Date;

import pe.gob.mincetur.vuce.co.domain.Orden;

/**
 * Clase de datos b�sicos del Certificado de Origen
 * @author Erick Osc�tegui
 */

public class CertificadoOrigen extends Orden {

	private Long coId;
	private Integer acuerdoInternacionalId;
	private Integer paisIsoIdAcuerdoInt;
	private Integer entidadId;
	private String idUsuarioEvaluador;
	private Integer idSede;

	private String importadorNombre; // Acuerdo China
	private String importadorDireccion; // Acuerdo China
	private Integer importadorPais; // Acuerdo China
	private Integer secuenciaPaisImportador;  // 20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas)
	private Integer medioTransporte;
	private String observacion; // Acuerdo ACE Chile, China
	private String tieneDjPorCalificar;
	private Date fechaPartidaTransporte; // Acuerdo China
	private String numeroTransporte; // Acuerdo China
	private Integer modoTransCargaId;
	private Long puertoCargaId; // Acuerdo China
	private Integer modoTransDescargaId; // Acuerdo China
	private Long puertoDescargaId; // Acuerdo China
	private String incluyeProductor; // Acuerdo China
	private String emitidoPosteriori; // Acuerdo UE
	private String importadorRegistroFiscal; // Acuerdo Costa Rica, Panama, Guatemala
	private String importadorTelefono; // Acuerdo Costa Rica, Panama, Guatemala
	private String importadorFax; // Acuerdo Costa Rica, Panama, Guatemala
	private String importadorEmail; // Acuerdo Costa Rica, Panama, Guatemala

	private String esConfidencial;
	private String direccionAdicional; // Direccion Adicional complementaria a SUNAT
	private Integer paisDescargaId; // Acuerdo SGP (Union Europea)
	private Integer secuenciaPaisDescarga; // 20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas)
	
	
	//20140825_JMC_Intercambio_Empresa
	//Codigos que se envia en el EBXML de la Empresa
	private String paisIsoCodigo;
	private String acuerdoInternacionalCodigo;
	private Integer entidadCodigo;
	private String importadorPaisIsoCodigo;
	private String puertoCodigo;
	private Integer sedeCodigo;
	private Integer modoTransCargaCodigo;
	private String puertoCargaCodigo; 
	private Integer modoTransDescargaCodigo;
	private String puertoDescargaCodigo; 
	private String paisDescargaCodigo;
	private String paisCargaCodigo;	
	private String numeroDocumentoSolicitante;
	private String expedienteEntidad;//
	private Integer secuenciaCertificado;
	
	private String importadorCiudad; // JMC 06/07/2017 Version 2 Alianza Pacifico

	public Long getCoId() {
		return coId;
	}
	public void setCoId(Long coId) {
		this.coId = coId;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public Integer getPaisIsoIdAcuerdoInt() {
		return paisIsoIdAcuerdoInt;
	}
	public void setPaisIsoIdAcuerdoInt(Integer paisIsoIdAcuerdoInt) {
		this.paisIsoIdAcuerdoInt = paisIsoIdAcuerdoInt;
	}
	public Integer getEntidadId() {
		return entidadId;
	}
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}
	public String getImportadorNombre() {
		return importadorNombre;
	}
	public void setImportadorNombre(String importadorNombre) {
		this.importadorNombre = importadorNombre;
	}
	public String getImportadorDireccion() {
		return importadorDireccion;
	}
	public void setImportadorDireccion(String importadorDireccion) {
		this.importadorDireccion = importadorDireccion;
	}
	public Integer getImportadorPais() {
		return importadorPais;
	}
	public void setImportadorPais(Integer importadorPais) {
		this.importadorPais = importadorPais;
	}
	public Integer getMedioTransporte() {
		return medioTransporte;
	}
	public void setMedioTransporte(Integer medioTransporte) {
		this.medioTransporte = medioTransporte;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getIdUsuarioEvaluador() {
		return idUsuarioEvaluador;
	}
	public void setIdUsuarioEvaluador(String idUsuarioEvaluador) {
		this.idUsuarioEvaluador = idUsuarioEvaluador;
	}
	public Integer getIdSede() {
		return idSede;
	}
	public void setIdSede(Integer idSede) {
		this.idSede = idSede;
	}
	public String getTieneDjPorCalificar() {
		return tieneDjPorCalificar;
	}
	public void setTieneDjPorCalificar(String tieneDjPorCalificar) {
		this.tieneDjPorCalificar = tieneDjPorCalificar;
	}
	public String getIncluyeProductor() {
		return incluyeProductor;
	}
	public void setIncluyeProductor(String incluyeProductor) {
		this.incluyeProductor = incluyeProductor;
	}
	public Date getFechaPartidaTransporte() {
		return fechaPartidaTransporte;
	}
	public void setFechaPartidaTransporte(Date fechaPartidaTransporte) {
		this.fechaPartidaTransporte = fechaPartidaTransporte;
	}
	public String getNumeroTransporte() {
		return numeroTransporte;
	}
	public void setNumeroTransporte(String numeroTransporte) {
		this.numeroTransporte = numeroTransporte;
	}
	public Long getPuertoCargaId() {
		return puertoCargaId;
	}
	public void setPuertoCargaId(Long puertoCargaId) {
		this.puertoCargaId = puertoCargaId;
	}
	public Long getPuertoDescargaId() {
		return puertoDescargaId;
	}
	public void setPuertoDescargaId(Long puertoDescargaId) {
		this.puertoDescargaId = puertoDescargaId;
	}
	public String getEmitidoPosteriori() {
		return emitidoPosteriori;
	}
	public void setEmitidoPosteriori(String emitidoPosteriori) {
		this.emitidoPosteriori = emitidoPosteriori;
	}
	public String getImportadorRegistroFiscal() {
		return importadorRegistroFiscal;
	}
	public void setImportadorRegistroFiscal(String importadorRegistroFiscal) {
		this.importadorRegistroFiscal = importadorRegistroFiscal;
	}
	public String getImportadorTelefono() {
		return importadorTelefono;
	}
	public void setImportadorTelefono(String importadorTelefono) {
		this.importadorTelefono = importadorTelefono;
	}
	public String getImportadorFax() {
		return importadorFax;
	}
	public void setImportadorFax(String importadorFax) {
		this.importadorFax = importadorFax;
	}
	public String getImportadorEmail() {
		return importadorEmail;
	}
	public void setImportadorEmail(String importadorEmail) {
		this.importadorEmail = importadorEmail;
	}
	public String getEsConfidencial() {
		return esConfidencial;
	}
	public void setEsConfidencial(String esConfidencial) {
		this.esConfidencial = esConfidencial;
	}
	public Integer getModoTransCargaId() {
		return modoTransCargaId;
	}
	public void setModoTransCargaId(Integer modoTransCargaId) {
		this.modoTransCargaId = modoTransCargaId;
	}
	public Integer getModoTransDescargaId() {
		return modoTransDescargaId;
	}
	public void setModoTransDescargaId(Integer modoTransDescargaId) {
		this.modoTransDescargaId = modoTransDescargaId;
	}
	public String getDireccionAdicional() {
		return direccionAdicional;
	}
	public void setDireccionAdicional(String direccionAdicional) {
		this.direccionAdicional = direccionAdicional;
	}
	public Integer getPaisDescargaId() {
		return paisDescargaId;
	}
	public void setPaisDescargaId(Integer paisDescargaId) {
		this.paisDescargaId = paisDescargaId;
	}
	public String getPaisIsoCodigo() {
		return paisIsoCodigo;
	}
	public void setPaisIsoCodigo(String paisIsoCodigo) {
		this.paisIsoCodigo = paisIsoCodigo;
	}
	public String getAcuerdoInternacionalCodigo() {
		return acuerdoInternacionalCodigo;
	}
	public void setAcuerdoInternacionalCodigo(String acuerdoInternacionalCodigo) {
		this.acuerdoInternacionalCodigo = acuerdoInternacionalCodigo;
	}
	public Integer getEntidadCodigo() {
		return entidadCodigo;
	}
	public void setEntidadCodigo(Integer entidadCodigo) {
		this.entidadCodigo = entidadCodigo;
	}
	public String getImportadorPaisIsoCodigo() {
		return importadorPaisIsoCodigo;
	}
	public void setImportadorPaisIsoCodigo(String importadorPaisIsoCodigo) {
		this.importadorPaisIsoCodigo = importadorPaisIsoCodigo;
	}
	public String getPuertoCodigo() {
		return puertoCodigo;
	}
	public void setPuertoCodigo(String puertoCodigo) {
		this.puertoCodigo = puertoCodigo;
	}
	public Integer getSedeCodigo() {
		return sedeCodigo;
	}
	public void setSedeCodigo(Integer sedeCodigo) {
		this.sedeCodigo = sedeCodigo;
	}
	public Integer getModoTransCargaCodigo() {
		return modoTransCargaCodigo;
	}
	public void setModoTransCargaCodigo(Integer modoTransCargaCodigo) {
		this.modoTransCargaCodigo = modoTransCargaCodigo;
	}
	public Integer getModoTransDescargaCodigo() {
		return modoTransDescargaCodigo;
	}
	public void setModoTransDescargaCodigo(Integer modoTransDescargaCodigo) {
		this.modoTransDescargaCodigo = modoTransDescargaCodigo;
	}
	public String getPuertoCargaCodigo() {
		return puertoCargaCodigo;
	}
	public void setPuertoCargaCodigo(String puertoCargaCodigo) {
		this.puertoCargaCodigo = puertoCargaCodigo;
	}
	public String getPuertoDescargaCodigo() {
		return puertoDescargaCodigo;
	}
	public void setPuertoDescargaCodigo(String puertoDescargaCodigo) {
		this.puertoDescargaCodigo = puertoDescargaCodigo;
	}
	public String getPaisDescargaCodigo() {
		return paisDescargaCodigo;
	}
	public void setPaisDescargaCodigo(String paisDescargaCodigo) {
		this.paisDescargaCodigo = paisDescargaCodigo;
	}
	public String getPaisCargaCodigo() {
		return paisCargaCodigo;
	}
	public void setPaisCargaCodigo(String paisCargaCodigo) {
		this.paisCargaCodigo = paisCargaCodigo;
	}
	public String getNumeroDocumentoSolicitante() {
		return numeroDocumentoSolicitante;
	}
	public void setNumeroDocumentoSolicitante(String numeroDocumentoSolicitante) {
		this.numeroDocumentoSolicitante = numeroDocumentoSolicitante;
	}
	public String getExpedienteEntidad() {
		return expedienteEntidad;
	}
	public void setExpedienteEntidad(String expedienteEntidad) {
		this.expedienteEntidad = expedienteEntidad;
	}
	public Integer getSecuenciaCertificado() {
		return secuenciaCertificado;
	}
	public void setSecuenciaCertificado(Integer secuenciaCertificado) {
		this.secuenciaCertificado = secuenciaCertificado;
	}
	public Integer getSecuenciaPaisImportador() {
		return secuenciaPaisImportador;
	}
	public void setSecuenciaPaisImportador(Integer secuenciaPaisImportador) {
		this.secuenciaPaisImportador = secuenciaPaisImportador;
	}
	public Integer getSecuenciaPaisDescarga() {
		return secuenciaPaisDescarga;
	}
	public void setSecuenciaPaisDescarga(Integer secuenciaPaisDescarga) {
		this.secuenciaPaisDescarga = secuenciaPaisDescarga;
	}
	public String getImportadorCiudad() {
		return importadorCiudad;
	}
	public void setImportadorCiudad(String importadorCiudad) {
		this.importadorCiudad = importadorCiudad;
	}

}
