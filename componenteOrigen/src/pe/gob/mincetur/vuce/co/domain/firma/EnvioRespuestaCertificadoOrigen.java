package pe.gob.mincetur.vuce.co.domain.firma;

public class EnvioRespuestaCertificadoOrigen {
	
	private ContenidoRespuestaOrigen contenidoRespuesta;

	public ContenidoRespuestaOrigen getContenidoRespuesta() {
		return contenidoRespuesta;
	}

	public void setContenidoRespuesta(ContenidoRespuestaOrigen contenidoRespuesta) {
		this.contenidoRespuesta = contenidoRespuesta;
	}


}
