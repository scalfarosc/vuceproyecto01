package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

import java.util.Date;

public class SC {
	
	private Long            calificacionId;
	private Long            ordenId;
	private Integer         mto;
	private String          descripcionComercial;
	private Long            partidaArancelaria;
	private Integer         umFisicaId;
	private Long            ordenIdDJ;
	private Integer         mtoDJ;
	private Date            fechaCaducidad;
	
	private Long            suceId;
	private Long            calificacionDrBorradorId;
	private Long            calificacionDrId;
	private Long            drId;
	private Integer         sdr;
	
	public Long getDrId() {
		return drId;
	}
	public void setDrId(Long drId) {
		this.drId = drId;
	}
	public Integer getSdr() {
		return sdr;
	}
	public void setSdr(Integer sdr) {
		this.sdr = sdr;
	}
	public Long getSuceId() {
		return suceId;
	}
	public void setSuceId(Long suceId) {
		this.suceId = suceId;
	}
	public Long getCalificacionDrBorradorId() {
		return calificacionDrBorradorId;
	}
	public void setCalificacionDrBorradorId(Long calificacionDrBorradorId) {
		this.calificacionDrBorradorId = calificacionDrBorradorId;
	}
	public Long getCalificacionDrId() {
		return calificacionDrId;
	}
	public void setCalificacionDrId(Long calificacionDrId) {
		this.calificacionDrId = calificacionDrId;
	}
	public Long getCalificacionId() {
		return calificacionId;
	}
	public void setCalificacionId(Long calificacionId) {
		this.calificacionId = calificacionId;
	}
	public Long getOrdenId() {
		return ordenId;
	}
	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}
	public Integer getMto() {
		return mto;
	}
	public void setMto(Integer mto) {
		this.mto = mto;
	}
	public String getDescripcionComercial() {
		return descripcionComercial;
	}
	public void setDescripcionComercial(String descripcionComercial) {
		this.descripcionComercial = descripcionComercial;
	}
	public Long getPartidaArancelaria() {
		return partidaArancelaria;
	}
	public void setPartidaArancelaria(Long partidaArancelaria) {
		this.partidaArancelaria = partidaArancelaria;
	}
	public Integer getUmFisicaId() {
		return umFisicaId;
	}
	public void setUmFisicaId(Integer umFisicaId) {
		this.umFisicaId = umFisicaId;
	}
	public Long getOrdenIdDJ() {
		return ordenIdDJ;
	}
	public void setOrdenIdDJ(Long ordenIdDJ) {
		this.ordenIdDJ = ordenIdDJ;
	}
	public Integer getMtoDJ() {
		return mtoDJ;
	}
	public void setMtoDJ(Integer mtoDJ) {
		this.mtoDJ = mtoDJ;
	}
	public Date getFechaCaducidad() {
		return fechaCaducidad;
	}
	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
}
