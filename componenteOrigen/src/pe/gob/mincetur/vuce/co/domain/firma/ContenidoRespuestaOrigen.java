package pe.gob.mincetur.vuce.co.domain.firma;

public class ContenidoRespuestaOrigen {
	
	private RespuestaCert respuestaCertificado;

	public RespuestaCert getRespuestaCertificado() {
		return respuestaCertificado;
	}

	public void setRespuestaCertificado(RespuestaCert respuestaCertificado) {
		this.respuestaCertificado = respuestaCertificado;
	}
	
	

}
