package pe.gob.mincetur.vuce.co.domain.certificadoOrigen;

public class AdjuntoRequeridoDJMaterial {

	private Integer adjuntoRequeridoDj;
	private String descripcion;
	private String obligatorio;
	private String estado;
	private String esRequeridoXDj;
	private String esRequeridoXMaterialDj;

	public Integer getAdjuntoRequeridoDj() {
		return adjuntoRequeridoDj;
	}
	public void setAdjuntoRequeridoDj(Integer adjuntoRequeridoDj) {
		this.adjuntoRequeridoDj = adjuntoRequeridoDj;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getObligatorio() {
		return obligatorio;
	}
	public void setObligatorio(String obligatorio) {
		this.obligatorio = obligatorio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getEsRequeridoXDj() {
		return esRequeridoXDj;
	}
	public void setEsRequeridoXDj(String esRequeridoXDj) {
		this.esRequeridoXDj = esRequeridoXDj;
	}
	public String getEsRequeridoXMaterialDj() {
		return esRequeridoXMaterialDj;
	}
	public void setEsRequeridoXMaterialDj(String esRequeridoXMaterialDj) {
		this.esRequeridoXMaterialDj = esRequeridoXMaterialDj;
	}

}
