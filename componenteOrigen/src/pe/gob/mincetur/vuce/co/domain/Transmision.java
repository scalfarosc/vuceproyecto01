package pe.gob.mincetur.vuce.co.domain;

import java.util.Date;

public class Transmision {

	private Integer idTransaccionNtx;
	
	private Integer estadoVC;
	
    private Integer id;
    
    private Integer entidadId;

    private String transaccion; // Codigo de la transaccion (N1,N2,N3)
    
    private Integer tipoTransmision;
    
    private Date fechaRegistro;
	
    
    
    private Integer idVE;
    private Integer idTransaccion;
    private Integer estadoVE;    
    private Integer idTransmisionOrigen;
    
    private Integer entidad;
    

	public Integer getIdTransaccionNtx() {
		return idTransaccionNtx;
	}

	public void setIdTransaccionNtx(Integer idTransaccionNtx) {
		this.idTransaccionNtx = idTransaccionNtx;
	}

	public Integer getEstadoVC() {
		return estadoVC;
	}

	public void setEstadoVC(Integer estadoVC) {
		this.estadoVC = estadoVC;
	}

	public String getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipoTransmision() {
		return tipoTransmision;
	}

	public void setTipoTransmision(Integer tipoTransmision) {
		this.tipoTransmision = tipoTransmision;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getEntidadId() {
		return entidadId;
	}

	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}

	public Integer getIdVE() {
		return idVE;
	}

	public void setIdVE(Integer idVE) {
		this.idVE = idVE;
	}

	public Integer getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(Integer idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public Integer getEstadoVE() {
		return estadoVE;
	}

	public void setEstadoVE(Integer estadoVE) {
		this.estadoVE = estadoVE;
	}

	public Integer getIdTransmisionOrigen() {
		return idTransmisionOrigen;
	}

	public void setIdTransmisionOrigen(Integer idTransmisionOrigen) {
		this.idTransmisionOrigen = idTransmisionOrigen;
	}

	public Integer getEntidad() {
		return entidad;
	}

	public void setEntidad(Integer entidad) {
		this.entidad = entidad;
	}

	
}
