package pe.gob.mincetur.vuce.co.domain.firma;

public class ContenidoOrigen {
	
	private CodExporter codExporter;
	private Eh eh;
	private EhCert ehCert;
	
	public CodExporter getCodExporter() {
		return codExporter;
	}
	public void setCodExporter(CodExporter codExporter) {
		this.codExporter = codExporter;
	}
	public Eh getEh() {
		return eh;
	}
	public void setEh(Eh eh) {
		this.eh = eh;
	}
	public EhCert getEhCert() {
		return ehCert;
	}
	public void setEhCert(EhCert ehCert) {
		this.ehCert = ehCert;
	}

}
