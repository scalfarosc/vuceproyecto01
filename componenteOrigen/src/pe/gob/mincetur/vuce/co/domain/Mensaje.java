package pe.gob.mincetur.vuce.co.domain;

import java.util.Date;

public class Mensaje {
	private Integer mensajeId;
	private Integer vcId;
	private Integer buzonId;
	private Integer carpeta;
	private String notificacion;
	private String mensaje;
	private Date fechaRegistro;
	private Integer de;
	private String deTitulo;
	private Integer notificacionId;
	private String baselegal;
	private Integer relacionTramite;
	private Integer ordenId;
	private Integer mto;
	private Integer suceId;
	private Integer modificacionSuce;
	private Integer consultaTecnicaId;
	private String estadoRegistro;
	private String privada;
	private String resultadoConsulta;
	
	public String getBaselegal() {
		return baselegal;
	}

	public void setBaselegal(String baselegal) {
		this.baselegal = baselegal;
	}

	public String getDeTitulo() {
		return deTitulo;
	}

	public void setDeTitulo(String deTitulo) {
		this.deTitulo = deTitulo;
	}

	private Integer envioTipo;
	private Integer usuarioTipo;
	private String asunto;
	private String estado;
	private String hacia;
	private String espregunta;
	
	
	public String getEspregunta() {
		return espregunta;
	}

	public void setEspregunta(String espregunta) {
		this.espregunta = espregunta;
	}

	public Integer getDe() {
		return de;
	}

	public void setDe(Integer de) {
		this.de = de;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getHacia() {
		return hacia;
	}

	public void setHacia(String hacia) {
		this.hacia = hacia;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public Integer getMensajeId() {
		return mensajeId;
	}

	public void setMensajeId(Integer mensajeId) {
		this.mensajeId = mensajeId;
	}

	public Integer getVcId() {
		return vcId;
	}

	public void setVcId(Integer vcId) {
		this.vcId = vcId;
	}

	public Integer getBuzonId() {
		return buzonId;
	}

	public void setBuzonId(Integer buzonId) {
		this.buzonId = buzonId;
	}

	public Integer getCarpeta() {
		return carpeta;
	}

	public void setCarpeta(Integer carpeta) {
		this.carpeta = carpeta;
	}

	public String getNotificacion() {
		return notificacion;
	}

	public void setNotificacion(String notificacion) {
		this.notificacion = notificacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getEnvioTipo() {
		return envioTipo;
	}

	public void setEnvioTipo(Integer envioTipo) {
		this.envioTipo = envioTipo;
	}

	public Integer getUsuarioTipo() {
		return usuarioTipo;
	}

	public void setUsuarioTipo(Integer usuarioTipo) {
		this.usuarioTipo = usuarioTipo;
	}

	public Integer getNotificacionId() {
		return notificacionId;
	}

	public void setNotificacionId(Integer notificacionId) {
		this.notificacionId = notificacionId;
	}

	public Integer getRelacionTramite() {
		return relacionTramite;
	}

	public void setRelacionTramite(Integer relacionTramite) {
		this.relacionTramite = relacionTramite;
	}

	public Integer getOrdenId() {
		return ordenId;
	}

	public void setOrdenId(Integer ordenId) {
		this.ordenId = ordenId;
	}

	public Integer getMto() {
		return mto;
	}

	public void setMto(Integer mto) {
		this.mto = mto;
	}

	public Integer getSuceId() {
		return suceId;
	}

	public void setSuceId(Integer suceId) {
		this.suceId = suceId;
	}

	public Integer getModificacionSuce() {
		return modificacionSuce;
	}

	public void setModificacionSuce(Integer modificacionSuce) {
		this.modificacionSuce = modificacionSuce;
	}

	public Integer getConsultaTecnicaId() {
		return consultaTecnicaId;
	}

	public void setConsultaTecnicaId(Integer consultaTecnicaId) {
		this.consultaTecnicaId = consultaTecnicaId;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

    public String getPrivada() {
        return privada;
    }

    public void setPrivada(String privada) {
        this.privada = privada;
    }

	public String getResultadoConsulta() {
		return resultadoConsulta;
	}

	public void setResultadoConsulta(String resultadoConsulta) {
		this.resultadoConsulta = resultadoConsulta;
	}

}
