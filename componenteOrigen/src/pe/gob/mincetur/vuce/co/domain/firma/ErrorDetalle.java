package pe.gob.mincetur.vuce.co.domain.firma;

public class ErrorDetalle {
	
	String codigoError;
	
	String mensajeError;

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	

}
