package pe.gob.mincetur.vuce.co.domain;

public class RepresentanteLegal {
	
	private Integer id;
	private Integer tipodoc;
	private String tipoDocDesc;
    private String numdoc;
    private String nombre;
    private String direccion;
    

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTipoDocDesc() {
		return tipoDocDesc;
	}
	public void setTipoDocDesc(String tipoDocDesc) {
		this.tipoDocDesc = tipoDocDesc;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(Integer tipodoc) {
		this.tipodoc = tipodoc;
	}
	public String getNumdoc() {
		return numdoc;
	}
	public void setNumdoc(String numdoc) {
		this.numdoc = numdoc;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


    

}
