package pe.gob.mincetur.vuce.co.domain.firma;

public class CabeceraOrigenEnvio {
	
	private String codigoInteraccion;
	private String idServicio;
	private String idRemitente;
	private String paisRemitente;
	private String idDestinatario;
	private String paisDestinatario;
	private String idTransaccion;
	private String estadoTransaccion;
	private String fechaHora;
	private String tipoDocumento;
	private String idDocumento;
	
	public String getCodigoInteraccion() {
		return codigoInteraccion;
	}
	public void setCodigoInteraccion(String codigoInteraccion) {
		this.codigoInteraccion = codigoInteraccion;
	}
	public String getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}
	public String getIdRemitente() {
		return idRemitente;
	}
	public void setIdRemitente(String idRemitente) {
		this.idRemitente = idRemitente;
	}
	public String getPaisRemitente() {
		return paisRemitente;
	}
	public void setPaisRemitente(String paisRemitente) {
		this.paisRemitente = paisRemitente;
	}
	public String getIdDestinatario() {
		return idDestinatario;
	}
	public void setIdDestinatario(String idDestinatario) {
		this.idDestinatario = idDestinatario;
	}
	public String getPaisDestinatario() {
		return paisDestinatario;
	}
	public void setPaisDestinatario(String paisDestinatario) {
		this.paisDestinatario = paisDestinatario;
	}
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public String getEstadoTransaccion() {
		return estadoTransaccion;
	}
	public void setEstadoTransaccion(String estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getIdDocumento() {
		return idDocumento;
	}
	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}
	
}
