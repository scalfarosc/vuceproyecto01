package pe.gob.mincetur.vuce.co.domain;


public class AdjuntoFormato extends Adjunto {

    private Integer adjuntoRequerido;
    
    private String descripcion;
    
    private Integer formatoId;
    
    private Integer ordenId;
    
    private Integer mto;
    
    private Integer mensajeId;
    
    private Long drId;

    private Integer secuenciaDr;
    
    private Integer notificacionId;
    
    private String esDetalle;
    
    private Integer secuencia; //20140825_JMC_Intercambio_Empresa
    
    public AdjuntoFormato() {
    }
    
    public AdjuntoFormato(Adjunto adjunto) {
    	this.setArchivo(adjunto.getArchivo());
    	this.setEstado(adjunto.getEstado());
    	this.setNombre(adjunto.getNombre());
    	this.setTipo(adjunto.getTipo());
    }
    
    public Integer getAdjuntoRequerido() {
        return adjuntoRequerido;
    }

    public void setAdjuntoRequerido(Integer adjuntoRequerido) {
        this.adjuntoRequerido = adjuntoRequerido;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getFormatoId() {
        return formatoId;
    }

    public void setFormatoId(Integer formatoId) {
        this.formatoId = formatoId;
    }

    public Integer getOrdenId() {
        return ordenId;
    }

    public void setOrdenId(Integer ordenId) {
        this.ordenId = ordenId;
    }

    public Integer getMto() {
        return mto;
    }

    public void setMto(Integer mto) {
        this.mto = mto;
    }

	public Integer getMensajeId() {
		return mensajeId;
	}

	public void setMensajeId(Integer mensajeId) {
		this.mensajeId = mensajeId;
	}

	public Long getDrId() {
		return drId;
	}

	public void setDrId(Long drId) {
		this.drId = drId;
	}

	public Integer getSecuenciaDr() {
		return secuenciaDr;
	}

	public void setSecuenciaDr(Integer secuenciaDr) {
		this.secuenciaDr = secuenciaDr;
	}

	public Integer getNotificacionId() {
		return notificacionId;
	}

	public void setNotificacionId(Integer notificacionId) {
		this.notificacionId = notificacionId;
	}

	public String getEsDetalle() {
		return esDetalle;
	}

	public void setEsDetalle(String esDetalle) {
		this.esDetalle = esDetalle;
	}

	public Integer getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}

	
}
