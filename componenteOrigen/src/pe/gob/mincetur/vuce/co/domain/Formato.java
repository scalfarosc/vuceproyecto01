package pe.gob.mincetur.vuce.co.domain;

public class Formato {
    
    private int id;
    private String formato;
    private String nombre;
    private String estado;
    private int idEntidad;
    private int cantidadAdjuntos;
    private Integer tipoGeneracionExpediente;
    private String rectificaDr;
    private String fraccionaDr;
    private String drAutomatico;
    private String drConDigitador;
    private String modificacionSuceXMto;
    private String trabajaResolutor;
    private String aperturaSUCE;
    private String reemplazaDr;
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(int idEntidad) {
        this.idEntidad = idEntidad;
    }

    public int getCantidadAdjuntos() {
        return cantidadAdjuntos;
    }

    public void setCantidadAdjuntos(int cantidadAdjuntos) {
        this.cantidadAdjuntos = cantidadAdjuntos;
    }
    
	public Integer getTipoGeneracionExpediente() {
		return tipoGeneracionExpediente;
	}

	public void setTipoGeneracionExpediente(Integer tipoGeneracionExpediente) {
		this.tipoGeneracionExpediente = tipoGeneracionExpediente;
	}

	public String getRectificaDr() {
		return rectificaDr;
	}

	public void setRectificaDr(String rectificaDr) {
		this.rectificaDr = rectificaDr;
	}

	public String getFraccionaDr() {
		return fraccionaDr;
	}

	public void setFraccionaDr(String fraccionaDr) {
		this.fraccionaDr = fraccionaDr;
	}

	public String getDrAutomatico() {
		return drAutomatico;
	}

	public void setDrAutomatico(String drAutomatico) {
		this.drAutomatico = drAutomatico;
	}

	public String getDrConDigitador() {
		return drConDigitador;
	}

	public void setDrConDigitador(String drConDigitador) {
		this.drConDigitador = drConDigitador;
	}

	public String getModificacionSuceXMto() {
		return modificacionSuceXMto;
	}

	public void setModificacionSuceXMto(String modificacionSuceXMto) {
		this.modificacionSuceXMto = modificacionSuceXMto;
	}

	public String getTrabajaResolutor() {
		return trabajaResolutor;
	}

	public void setTrabajaResolutor(String trabajaResolutor) {
		this.trabajaResolutor = trabajaResolutor;
	}

	public String getAperturaSUCE() {
		return aperturaSUCE;
	}

	public void setAperturaSUCE(String aperturaSUCE) {
		this.aperturaSUCE = aperturaSUCE;
	}

	public String getReemplazaDr() {
		return reemplazaDr;
	}

	public void setReemplazaDr(String reemplazaDR) {
		this.reemplazaDr = reemplazaDR;
	}

}
