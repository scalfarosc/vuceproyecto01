package pe.gob.mincetur.vuce.co.domain.firma;

import java.util.ArrayList;
import java.util.List;

public class EnvioCertificadoOrigen {

	private CabeceraOrigenEnvio cabecera;
	private ContenidoOrigen contenido;
	
	public CabeceraOrigenEnvio getCabecera() {
		return cabecera;
	}
	public void setCabecera(CabeceraOrigenEnvio cabecera) {
		this.cabecera = cabecera;
	}
	public ContenidoOrigen getContenido() {
		return contenido;
	}
	public void setContenido(ContenidoOrigen contenido) {
		this.contenido = contenido;
	}
	public static EnvioCertificadoOrigen generateMockCertOrigen() {
		EnvioCertificadoOrigen origen = new EnvioCertificadoOrigen();
		CabeceraOrigenEnvio cabecera = new CabeceraOrigenEnvio();
		cabecera.setCodigoInteraccion("codigoInteraccion");
		cabecera.setEstadoTransaccion("estadoTransaccion");
		cabecera.setFechaHora("fechaHora");
		cabecera.setIdDestinatario("idDestinatario");
		cabecera.setIdDocumento("idDocumento");
		cabecera.setIdRemitente("idRemitente");
		cabecera.setIdServicio("idServicio");
		cabecera.setIdTransaccion("idTransaccion");
		cabecera.setPaisDestinatario("paisDestinatario");
		cabecera.setPaisRemitente("paisRemitente");
		cabecera.setTipoDocumento("tipoDocumento");
		origen.setCabecera(cabecera);
		
		ContenidoOrigen contenido = new ContenidoOrigen();
		CodExporter codExporter = new CodExporter();
		
		EnteComercio exportador = new EnteComercio();
		exportador.setCiudad("ciudadexportador");
		exportador.setDireccion("direccionexportador");
		exportador.setEmail("emailexportador");
		exportador.setFax("faxexportador");
		exportador.setIdentificacionImpuesto("identificacionImpuestoexportador");
		exportador.setNombreNegocio("nombreNegocioexportador");
		exportador.setPais("paisexportador");
		exportador.setTelefono("telefonoexportador");
		exportador.setUrl("urlexportador");
		codExporter.setExportador(exportador);
		
		EnteComercio importador = new EnteComercio();
		importador.setCiudad("ciudadimportador");
		importador.setDireccion("direccionimportador");
		importador.setEmail("emailimportador");
		importador.setFax("faximportador");
		importador.setIdentificacionImpuesto("identificacionImpuestoimportador");
		importador.setNombreNegocio("nombreNegocioimportador");
		importador.setPais("paisimportador");
		importador.setTelefono("telefonoimportador");
		importador.setUrl("urlimportador");
		codExporter.setImportador(importador);
		
		List<EnteComercio> productores = new ArrayList<EnteComercio>();
		EnteComercio productor = new EnteComercio(); 
		productor.setCiudad("ciudadproductor1");
		productor.setConfidencialidad("confidencialidadproductor1");
		productor.setDireccion("direccionproductor1");
		productor.setEmail("emailproductor1");
		productor.setExportador("exportadorproductor1");
		productor.setFax("faxproductor1");
		productor.setIdentificacionImpuesto("identificacionImpuestoproductor1");
		productor.setNombreNegocio("nombreNegocioproductor1");
		productor.setOrden("ordenproductor1");
		productor.setPais("paisproductor1");
		productor.setTelefono("telefonoproductor1");
		productor.setUrl("urlproductor1");
		productores.add(productor);
		
		productor = new EnteComercio();		
		productor.setCiudad("ciudadproductor2");
		productor.setConfidencialidad("confidencialidadproductor2");
		productor.setDireccion("direccionproductor2");
		productor.setEmail("emailproductor2");
		productor.setExportador("exportadorproductor2");
		productor.setFax("faxproductor2");
		productor.setIdentificacionImpuesto("identificacionImpuestoproductor2");
		productor.setNombreNegocio("nombreNegocioproductor2");
		productor.setOrden("ordenproductor2");
		productor.setPais("paisproductor2");
		productor.setTelefono("telefonoproductor2");
		productor.setUrl("urlproductor2");
		productores.add(productor);
		codExporter.setProductores(productores);
		
		List<FacturaOrigen> facturas = new ArrayList<FacturaOrigen>();
		FacturaOrigen factura = new FacturaOrigen();
		factura.setFecha("fechafactura1");
		factura.setNumero("numerofactura1");
		factura.setOrden("ordenfactura1");
		facturas.add(factura);
		
		factura = new FacturaOrigen();
		factura.setFecha("fechafactura2");
		factura.setNumero("numerofactura2");
		factura.setOrden("ordenfactura2");
		facturas.add(factura);
		codExporter.setFacturas(facturas);
		
		List<BienOrigen> bienes = new ArrayList<BienOrigen>();
		BienOrigen bien = new BienOrigen();
		bien.setCodigoItem("codigoItembien1");
		bien.setFechaDeclaracion("fechaDeclaracionbien1");
		bien.setMedidaItem("medidaItembien1");
		bien.setNombreItem("nombreItembien1");
		bien.setOrden("ordenbien1");
		bien.setOrdenFactura("ordenFacturabien1");
		bien.setOtraInstanciaItem("otraInstanciaItembien1");
		bien.setPesoItem("pesoItembien1");
		bien.setReglasOrigenItem("reglasOrigenItembien1");
		//bien.setRvcItem("rvcItembien1");
		bienes.add(bien);
		
		bien = new BienOrigen();
		bien.setCodigoItem("codigoItembien2");
		bien.setFechaDeclaracion("fechaDeclaracionbien2");
		bien.setMedidaItem("medidaItembien2");
		bien.setNombreItem("nombreItembien2");
		bien.setOrden("ordenbien2");
		bien.setOrdenFactura("ordenFacturabien2");
		bien.setOtraInstanciaItem("otraInstanciaItembien2");
		bien.setPesoItem("pesoItembien2");
		bien.setReglasOrigenItem("reglasOrigenItembien2");
		//bien.setRvcItem("rvcItembien2");
		bienes.add(bien);
		codExporter.setBienes(bienes);
		
		List<ComentarioOrigen> comentarios = new ArrayList<ComentarioOrigen>();
		ComentarioOrigen comentario = new ComentarioOrigen();
		comentario.setDireccion("direccioncomentario1");
		comentario.setEsTercero(true);
		comentario.setFechaFactura("fechaFacturacomentario1");
		comentario.setNombreNegocio("nombreNegociocomentario1");
		comentario.setNumeroFactura("numeroFacturacomentario1");
		comentario.setPais("paiscomentario1");
		comentarios.add(comentario);
		
		comentario = new ComentarioOrigen();
		comentario.setDireccion("direccioncomentario2");
		comentario.setEsTercero(true);
		comentario.setFechaFactura("fechaFacturacomentario2");
		comentario.setNombreNegocio("nombreNegociocomentario2");
		comentario.setNumeroFactura("numeroFacturacomentario2");
		comentario.setPais("paiscomentario2");
		comentarios.add(comentario);
		//codExporter.setComentarios(comentarios);
		
		codExporter.setAcronimoAcuerdo("acronimoAcuerdo");
		codExporter.setComentarioGeneral("comentarioGeneral");
		codExporter.setNombreAcuerdo("nombreAcuerdo");
		codExporter.setRemitente("remitente");
		codExporter.setVersion("version");
		
		contenido.setCodExporter(codExporter);
		
		Eh eh = new Eh();
		eh.setId("ideh");
		eh.setNombre("nombreeh");
		eh.setLocalidad("localidadeh");
		eh.setCiudad("ciudadeh");
		eh.setDireccion("direccioneh");
		eh.setEmail("emaileh");
		eh.setFax("faxeh");
		eh.setPais("paiseh");
		eh.setTelefono("telefonoeh");
		eh.setUrl("urleh");
		contenido.setEh(eh);
		
		EhCert ehCert = new EhCert();
		//ehCert.setCodigoControl("codigoControlehcert");
		//ehCert.setFecha("fechaehCert");
		//ehCert.setId("idehCert");
		contenido.setEhCert(ehCert);
		
		origen.setContenido(contenido);
		return origen;
	}
	
}
