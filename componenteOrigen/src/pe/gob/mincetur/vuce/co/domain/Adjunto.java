package pe.gob.mincetur.vuce.co.domain;

public class Adjunto {

    private int id;

    private String nombre;

    private byte [] archivo;

    private Integer tipo;

    private String estado;
    
    private Long borradorDrId;
    
    private Long calificacionDrBorradorId;
    
    private Long certificadoDrBorradorId;
    
    private Integer vcId;//vcId de la transmision para la empresa //20140825_JMC_Intercambio_Empresa
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Long getBorradorDrId() {
		return borradorDrId;
	}

	public void setBorradorDrId(Long borradorDrId) {
		this.borradorDrId = borradorDrId;
	}

	public Long getCalificacionDrBorradorId() {
		return calificacionDrBorradorId;
	}

	public void setCalificacionDrBorradorId(Long calificacionDrBorradorId) {
		this.calificacionDrBorradorId = calificacionDrBorradorId;
	}

	public Long getCertificadoDrBorradorId() {
		return certificadoDrBorradorId;
	}

	public void setCertificadoDrBorradorId(Long certificadoDrBorradorId) {
		this.certificadoDrBorradorId = certificadoDrBorradorId;
	}

	public Integer getVcId() {
		return vcId;
	}

	public void setVcId(Integer vcId) {
		this.vcId = vcId;
	}
	

}
