package pe.gob.mincetur.vuce.co.domain.solicitudCalificacion;

import java.math.BigDecimal;

public class SCDetalleDj {
	
	private String caracteristica;
	private BigDecimal valorUnitarioTotalFob;
	private BigDecimal valorUnitarioTotalExWork;
	private BigDecimal valorTotalFob;
	private BigDecimal valorTotalExWork;
	private BigDecimal demasGasto;
	private BigDecimal tipoCambio;
	
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public BigDecimal getValorUnitarioTotalFob() {
		return valorUnitarioTotalFob;
	}
	public void setValorUnitarioTotalFob(BigDecimal valorUnitarioTotalFob) {
		this.valorUnitarioTotalFob = valorUnitarioTotalFob;
	}
	public BigDecimal getValorUnitarioTotalExWork() {
		return valorUnitarioTotalExWork;
	}
	public void setValorUnitarioTotalExWork(BigDecimal valorUnitarioTotalExWork) {
		this.valorUnitarioTotalExWork = valorUnitarioTotalExWork;
	}
	public BigDecimal getValorTotalFob() {
		return valorTotalFob;
	}
	public void setValorTotalFob(BigDecimal valorTotalFob) {
		this.valorTotalFob = valorTotalFob;
	}
	public BigDecimal getValorTotalExWork() {
		return valorTotalExWork;
	}
	public void setValorTotalExWork(BigDecimal valorTotalExWork) {
		this.valorTotalExWork = valorTotalExWork;
	}
	public BigDecimal getDemasGasto() {
		return demasGasto;
	}
	public void setDemasGasto(BigDecimal demasGasto) {
		this.demasGasto = demasGasto;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
	
}
