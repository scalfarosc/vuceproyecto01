package pe.gob.mincetur.vuce.co.domain;

public class InteroperabilidadFirma {
	
	private String token;
	private Integer secuencia;
	private Long drId;
	private Integer sdr;
	private Integer acuerdoInternacionalId;
    private byte [] xml;
    private byte [] pdf;
    
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}
	public Long getDrId() {
		return drId;
	}
	public void setDrId(Long drId) {
		this.drId = drId;
	}
	public Integer getSdr() {
		return sdr;
	}
	public void setSdr(Integer sdr) {
		this.sdr = sdr;
	}
	public Integer getAcuerdoInternacionalId() {
		return acuerdoInternacionalId;
	}
	public void setAcuerdoInternacionalId(Integer acuerdoInternacionalId) {
		this.acuerdoInternacionalId = acuerdoInternacionalId;
	}
	public byte[] getXml() {
		return xml;
	}
	public void setXml(byte[] xml) {
		this.xml = xml;
	}
	public byte[] getPdf() {
		return pdf;
	}
	public void setPdf(byte[] pdf) {
		this.pdf = pdf;
	}

}
