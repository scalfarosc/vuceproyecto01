package pe.gob.mincetur.vuce.co.domain.dj;

public class DJExportador {
	private Long            djId;
	private Integer         secuenciaExportador;
	private Integer         documentoTipo;
	private String          numeroDocumento;
	private String nombre;
	private String          vigente;
	
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public Integer getSecuenciaExportador() {
		return secuenciaExportador;
	}
	public void setSecuenciaExportador(Integer secuenciaExportador) {
		this.secuenciaExportador = secuenciaExportador;
	}
	public Integer getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(Integer documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getVigente() {
		return vigente;
	}
	public void setVigente(String vigente) {
		this.vigente = vigente;
	}
	
}
