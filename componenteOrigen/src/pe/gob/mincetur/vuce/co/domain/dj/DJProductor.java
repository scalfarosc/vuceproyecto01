package pe.gob.mincetur.vuce.co.domain.dj;

public class DJProductor {
	
	private Long            djId;
	private Integer         secuenciaProductor;
	private String          numeroDocumento;
	private String          nombre;
	private String          lugarProduccion;
	public Long getDjId() {
		return djId;
	}
	public void setDjId(Long djId) {
		this.djId = djId;
	}
	public Integer getSecuenciaProductor() {
		return secuenciaProductor;
	}
	public void setSecuenciaProductor(Integer secuenciaProductor) {
		this.secuenciaProductor = secuenciaProductor;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getLugarProduccion() {
		return lugarProduccion;
	}
	public void setLugarProduccion(String lugarProduccion) {
		this.lugarProduccion = lugarProduccion;
	}
	
	

}
