package pe.gob.mincetur.vuce.co.exception;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.util.xml.XMLParseBean;

public class XMLParseException extends RuntimeException {
	
    private static final long serialVersionUID = 0L;
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
    
    public static final int WARNING = 1;
    public static final int ERROR = 2;
    public static final int FATAL = 3;
    private XMLParseBean message;
    
    public XMLParseException() {}

    public XMLParseException(Object o, int type, int line, String msg) {
        super(msg);
        message = new XMLParseBean();
        message.setType(type);
        message.setLine(line);
        message.setMessage(msg);
        logger.error(o.getClass().getName() + "|" + message);
        logger.debug("Error", this);
    }

    public XMLParseException(Object o, XMLParseBean msg) {
        super(msg.getMessage());
        this.message = msg;
        
        if (msg.getType() != WARNING && msg.getType() != ERROR && msg.getType() != FATAL)
            this.message.setType(ERROR);
        
        logger.error(o.getClass().getName() + "|" + msg.getMessage());
        logger.debug("Error", this);
    }

    public XMLParseBean getMessageBean(){
        return message;
    }

}
