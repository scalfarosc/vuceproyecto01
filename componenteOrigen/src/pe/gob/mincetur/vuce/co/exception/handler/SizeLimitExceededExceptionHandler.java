package pe.gob.mincetur.vuce.co.exception.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class SizeLimitExceededExceptionHandler implements HandlerExceptionResolver, Ordered {
	
	private String sizeLimitExceeded;
	
	private int order;

	public void setSizeLimitExceeded(String sizeLimitExceeded) {
		this.sizeLimitExceeded = sizeLimitExceeded;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) {
		if (e instanceof MaxUploadSizeExceededException)
			return new ModelAndView(sizeLimitExceeded);
		else
			return null;
	}

}
