package pe.gob.mincetur.vuce.co.exception;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.springframework.aop.ThrowsAdvice;

import pe.gob.mincetur.vuce.co.logic.AlertaUtil;

public class ExceptionAdvice implements ThrowsAdvice {
    
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_UTIL);
    
    public void afterThrowing(/*Method method, Object[] args, Object target, */Exception ex) throws Throwable {
    	if (!(ex instanceof InvocationTargetException)) {
    		// Si no viene del Gestor de Procesos
		    AlertaUtil.registrarErrorProceso(null, null, null, null, ex);
    	}
		throw ex;
	}
}
