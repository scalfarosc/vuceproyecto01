package pe.gob.mincetur.vuce.co.remoting.rest.ws.domain;

public class JsonResponseError {

	private String code;
	private String description;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
