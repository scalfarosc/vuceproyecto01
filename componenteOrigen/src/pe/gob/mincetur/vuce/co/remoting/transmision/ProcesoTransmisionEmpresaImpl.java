package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class ProcesoTransmisionEmpresaImpl implements ProcesoTransmisionEmpresa{
	
    private static Logger logger = Logger.getLogger(org.jlis.core.util.Constantes.LOGGER_SERVICE);
    
    private TransmisionService transmisionService;
    
    public void setTransmisionService(TransmisionService transmisionService) {
		this.transmisionService = transmisionService;
	}

	/**
     * Proceso bachero que procesa las transmisiones que envio la empresa y que estan pendientes de procesar por vuce 
     */
	public void procesarTransmisionesPendientes() {
		//Obtiene el listado de las transmisiones con EVC=Pendientes de Procesar
		//Por cada transmision se evalua, valida y registra la transmision de la Empresa
		
		try{
			logger.debug("INICIO DEL PROCESO DE PROCESO DE TRANSMISIONES PENDIENTES DE EMPRESA");
            HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
            filter.put("estadoVCRecibidoEmpresa", Util.stringValueOf(ConstantesCO.TRANSMISION_ESTADO_VC_RECIBIDO_DE_EMPRESA));
            filter.put("estadoVCParaEnviarEmpresa", Util.stringValueOf(ConstantesCO.TRANSMISION_ESTADO_VC_POR_PROCESAR));
            filter.put("tipoTransmision", Util.stringValueOf(ConstantesCO.TIPO_TRANSMISION_NOTIFICACION_EMPRESA_ORIGEN));
            
            List<Transmision> listaTransmisiones = transmisionService.loadList(filter);
            HashMap<Integer, List<Transmision>> transmisiones = new HashMap<Integer, List<Transmision>>();
            logger.debug("transmisiones.size()"+listaTransmisiones.size());
            for (Transmision transmision : listaTransmisiones) {
                //Procesa cada una de las transmisiones
            	transmisionService.procesarTransmisionEmpresa(transmision);
            }
            logger.debug("FIN DEL PROCESO DE PROCESO DE TRANSMISIONES PENDIENTES DE EMPRESA");
		} catch (Exception e){
			logger.error("ERROR EN EL PROCESO DE ENVIO DE MENSAJES PENDIENTES", e);		
		}
	}
}
