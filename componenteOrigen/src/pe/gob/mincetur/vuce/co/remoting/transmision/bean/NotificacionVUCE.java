package pe.gob.mincetur.vuce.co.remoting.transmision.bean;


public class NotificacionVUCE extends TransmisionVUCE {
	
	private int idTransmision;
    
    public NotificacionVUCE() {
        super();
        this.setTemplate("mensajesTransmision/Notificacion.vm");
    }

	public int getIdTransmision() {
		return idTransmision;
	}

	public void setIdTransmision(int idTransmision) {
		this.idTransmision = idTransmision;
	}

    
}
