package pe.gob.mincetur.vuce.co.remoting.ws.entidad;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.ws.soap.MTOM;


/**
 * 
 * @author Erick Oscategui
 */
//@MTOM
@WebService(serviceName="VuceOrigenWS")
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT,
		     use=SOAPBinding.Use.LITERAL)
public interface VuceOrigenWS {
	
	@WebMethod(operationName="generarXml", action="urn:generarXml") 
	public @XmlMimeType("application/octet-stream") DataHandler generarXml(
		@WebParam(name="token",
				  header=false,
				  mode=WebParam.Mode.IN) String token) throws Exception;

	@WebMethod(operationName="generarPdf", action="urn:generarPdf") 
	public @XmlMimeType("application/octet-stream") DataHandler generarPdf(
		@WebParam(name="token",
				  header=false,
				  mode=WebParam.Mode.IN) String token) throws Exception;
	
    @WebMethod(operationName="enviarXmlFirmado", action="urn:enviarXmlFirmado")
    public Integer enviarXmlFirmado(
    		@WebParam(name="token",
			  header=false,
			  mode=WebParam.Mode.IN) String token, 
            @WebParam(name="xml", 
                      header=false, 
                      mode=WebParam.Mode.IN)
            @XmlMimeType("application/octet-stream")DataHandler xml) throws Exception;

    @WebMethod(operationName="enviarPdfFirmado", action="urn:enviarPdfFirmado")
    public Integer enviarPdfFirmado(
    		@WebParam(name="token",
			  header=false,
			  mode=WebParam.Mode.IN) String token, 
            @WebParam(name="xml", 
                      header=false, 
                      mode=WebParam.Mode.IN)
            @XmlMimeType("application/octet-stream")DataHandler pdf) throws Exception;
    
    /*HT: SUNAT , metodo copiado de VuceCentralWS.java*/
    @WebMethod(operationName="obtenerCertificado", action="urn:obtenerCertificado")
	public @XmlMimeType("application/octet-stream") DataHandler obtenerCertificado(			
			@WebParam(name="certificadoOrigen",
	        header=false,
	        mode=WebParam.Mode.IN) String certificadoOrigen,
			@WebParam(name="codigoPais",
	        header=false,
	        mode=WebParam.Mode.IN) String codigoPais) throws Exception;

}
