package pe.gob.mincetur.vuce.co.remoting.ws.client;

import org.jlis.core.config.ApplicationConfig;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pe.gob.sunat.wwss.jboss_net.services.ConsultaRuc.DatosRucWS;

public final class WebServiceFactory {

	static {
    	System.setProperty("http.proxyHost", ApplicationConfig.getApplicationConfig().getProperty("co.proxy.host"));
    	System.setProperty("http.proxyPort", ApplicationConfig.getApplicationConfig().getProperty("co.proxy.port"));
    	System.setProperty("http.nonProxyHosts", ApplicationConfig.getApplicationConfig().getProperty("co.proxy.noProxyHosts"));
	}
    
	private static final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("pe/gob/mincetur/vuce/co/remoting/ws/client/clientConfig.xml");
	
    public static DatosRucWS getSunatConsultaRUCClient() {
        return (DatosRucWS)context.getBean("sunatConsultaRUCClient");
    }
    
    public static DatosRucWS getSunatConsultaRUCClientAntiguo() {
        return (DatosRucWS)context.getBean("sunatConsultaRUCClientAntiguo");
    }
    
}
