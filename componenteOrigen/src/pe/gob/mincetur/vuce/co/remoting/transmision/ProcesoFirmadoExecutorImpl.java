package pe.gob.mincetur.vuce.co.remoting.transmision;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import pe.gob.mincetur.vuce.co.logic.InteroperabilidadFirmasLogic;

public class ProcesoFirmadoExecutorImpl implements ProcesoFirmadoExecutor{
	
    private static Logger logger = Logger.getLogger(org.jlis.core.util.Constantes.LOGGER_SERVICE);
    
    private InteroperabilidadFirmasLogic iopLogic;
    
	private class TransmisionTask implements Runnable {
        
        private String token;        
        
        public TransmisionTask(String token) {
            this.token = token;            
        }
        
        public void run() {

            try {
            	NDC.push("CREAR PDF SIN FIRMA: "+this.token);
            	//logger.debug("CREAR PDF SIN FIRMA: "+this.token);
        		byte [] pdfGeneradoBytes = iopLogic.generarPdf(this.token);
    			iopLogic.enviarPdfFirmado(this.token, pdfGeneradoBytes);
                logger.debug("Confirmacion de CREAR PDF SIN FIRMA con exito, TOKEN=" + this.token);
                
            } catch (Exception e) {                
                logger.error("Ocurrio un error al CREAR PDF SIN FIRMA, TOKEN=" + this.token, e);
                
            } finally {
                NDC.pop();
                NDC.remove();
            }
        }
    }

	private ThreadPoolTaskExecutor firmadoTaskExecutorThreadPool;

	public void setFirmadoTaskExecutorThreadPool(
			ThreadPoolTaskExecutor firmadoTaskExecutorThreadPool) {
		this.firmadoTaskExecutorThreadPool = firmadoTaskExecutorThreadPool;
	}
	
	public void procesarGeneracionPDF(String token){
		firmadoTaskExecutorThreadPool.execute(new TransmisionTask(token));		
	}

	public InteroperabilidadFirmasLogic getIopLogic() {
		return iopLogic;
	}

	public void setIopLogic(InteroperabilidadFirmasLogic iopLogic) {
		this.iopLogic = iopLogic;
	}
	
	

}
