package pe.gob.mincetur.vuce.co.remoting.rest.ws.domain;

import java.util.Map;

public class JsonRequest {
	
	private Map<String, Object> data;
	
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
}
