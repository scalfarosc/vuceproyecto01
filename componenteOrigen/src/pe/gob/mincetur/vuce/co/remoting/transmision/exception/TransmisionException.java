package pe.gob.mincetur.vuce.co.remoting.transmision.exception;

public class TransmisionException  extends Exception {

    private static final long serialVersionUID = 1L;
    
    private Object objeto;
    
    public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	public TransmisionException(Exception e) {
        super(e);
    }
	
	public TransmisionException(String error) {
        super(error);
    }
	

}
