package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.jlis.service.ibatis.IbatisService;

import pe.gob.mincetur.vuce.co.bean.ErroresDatos;
import pe.gob.mincetur.vuce.co.bean.ErroresEmpresa;
import pe.gob.mincetur.vuce.co.dao.InteroperabilidadFirmaDAO;
import pe.gob.mincetur.vuce.co.dao.TransmisionDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.Orden;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;
import pe.gob.mincetur.vuce.co.domain.ebxml.MCT001EBXML;
import pe.gob.mincetur.vuce.co.domain.ebxml.MCT005EBXML;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.logic.AdjuntoLogic;
import pe.gob.mincetur.vuce.co.logic.certificadoOrigen.CertificadoOrigenLogic;
import pe.gob.mincetur.vuce.co.procesodr.ProcesoDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.acuerdo.Acuerdo26IOPEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionEmpresaVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionIOPVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransmisionVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.exception.TransmisionException;
import pe.gob.mincetur.vuce.co.service.TransmisionPackService;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.TransaccionType;
import pe.gob.mincetur.vuce.co.util.file.ZipUtil;
import pe.gob.mincetur.vuce.co.util.xml.XMLUtil;




public class TransmisionServiceImpl implements TransmisionService {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
	
	private AdjuntoLogic adjuntoLogic;
	
	private CertificadoOrigenLogic certificadoOrigenLogic;
	
	private TransmisionDAO transmisionDAO;
	
	private TransaccionEmpresaService transaccionEmpresaService;
	
	protected IbatisService ibatisService;

	//JMC 02/12/2016 ALIANZA PACIFICO
	
	private TransaccionPackService transaccionPackService;
	
	private InteroperabilidadFirmaDAO iopDAO;		
	
	private ProcesoDR procesoDR;
	
	private TransmisionPackService iopService;
	
	public void setProcesoDR(ProcesoDR procesoDR) {
		this.procesoDR = procesoDR;
	}

	public void setTransaccionPackService(
			TransaccionPackService transaccionPackService) {
		this.transaccionPackService = transaccionPackService;
	}

	public void setIopDAO(InteroperabilidadFirmaDAO iopDAO) {
		this.iopDAO = iopDAO;
	}

	public void setAdjuntoLogic(AdjuntoLogic adjuntoLogic) {
		this.adjuntoLogic = adjuntoLogic;
	}

	public void setCertificadoOrigenLogic(
			CertificadoOrigenLogic certificadoOrigenLogic) {
		this.certificadoOrigenLogic = certificadoOrigenLogic;
	}

	public void setTransmisionDAO(TransmisionDAO transmisionDAO) {
		this.transmisionDAO = transmisionDAO;
	}

	public void setTransaccionEmpresaService(
			TransaccionEmpresaService transaccionEmpresaService) {
		this.transaccionEmpresaService = transaccionEmpresaService;
	}
	
	public void setIbatisService(IbatisService ibatisService) {
		this.ibatisService = ibatisService;
	}
	
	public void setIopService(TransmisionPackService iopService) {
		this.iopService = iopService;
	}

	public List<Transmision> loadList(HashUtil<String, Integer> filter)
			throws Exception {
		return transmisionDAO.loadList(filter);
	}
	
	public void procesarTransmisionEmpresa(Transmision transmision) {
		TransaccionEmpresaVUCE mensajeTransmision = new TransaccionEmpresaVUCE();
		TransaccionEmpresaVUCE mensajeTransmisionResultado = new TransaccionEmpresaVUCE();
		ErroresEmpresa mensajeTransmisionResultadoConErrores = new ErroresEmpresa();
		Transmision transmisionRegistrada;
		//obtener los adjuntos de cada transmision (xml, ebxml, adjuntos)
		//filter.put("idTransaccion", TransaccionType.getTransaccionTypeByCodigo(transmisionVUCE.getTransaccion()).getId());
		//if (transmision.getIdTransaccion().equals(TransaccionType.N2.getId())) {
		//if (transmision.getTransaccion().equals(TransaccionType.N1.getCodigo())) {//En esta fase todos son N1
		
		try{
			
		if (transmision.getIdTransaccion().equals(TransaccionType.N1.getId())) {			
		
			List<Adjunto> adjuntos = adjuntoLogic.cargarArchivosTransmision(transmision);
			byte [] bytesXmlNotificacion = adjuntoLogic.getXMLFromList(adjuntos);
			byte [] bytesEBXML = adjuntoLogic.getEBXMLFromList(adjuntos);
			byte [] bytesZipAdjuntos = adjuntoLogic.getZIPFromList(adjuntos);
			
			
			//valida la estructura y adjuntos de la transmision
			HashMap<String, Object> bytesValidacionEstructura = transaccionEmpresaService.validaAdjuntosTransaccionEmpresa(bytesXmlNotificacion, bytesEBXML, transmision.getId());
			

				
				if (bytesValidacionEstructura==null){//OK
					
					
					//Se registra los datos del ebXML a las tablas temporales y se valida la transaccion	
					HashUtil filterResultado = new HashUtil<String, Object>();
					//String resultadoTransaccion = procesarTransaccionEmpresa(bytesXmlNotificacion, bytesEBXML, bytesZipAdjuntos, transmision);
					filterResultado = procesarTransaccionEmpresa(bytesXmlNotificacion, bytesEBXML, bytesZipAdjuntos, transmision);
					int formatoTmpId = (filterResultado.get("mct001TmpId")!=null?filterResultado.getInt("mct001TmpId"):filterResultado.getInt("mct005TmpId"));
					/*
					//Se valida y procesa la DATA de la transaccion
					int resultadoTransaccion = 0;
					HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
		            filter.put("transmision_id", transmision.getId());
		            filter.put("resultado", null);
		            
					resultadoTransaccion = transmisionDAO.procesarTransaccionEmpresa(filter);
					*/
					
					//Si fue OK se genera (N80); si fue Error se genera (N81)
					if (filterResultado.getString("resValidacion").equalsIgnoreCase("S")){//OK
	
						logger.debug("############ TransmisionServiceImpl.procesarTransmisionEmpresa: La Solicitud es CORRECTA");
						HashUtil<String, Object> filter = new HashUtil<String, Object>();
						HashUtil<String, Object> element = new HashUtil<String, Object>();
						
						//Registra el N80 en la tabla de adjuntos y transmision para que la Empresa lo obtenga
						if(filterResultado.get("mct001TmpId")!=null){
							//int mct001TmpId = filterResultado.getInt("mct001TmpId");
		                	//filter.put("mct001TmpId", mct001TmpId);                	
		                	filter.put("mct001TmpId", formatoTmpId);
		                    element = ibatisService.loadElement("certificadoOrigen.transmision.orden.element", filter);  
						} else if(filterResultado.get("mct005TmpId")!=null){
							//int mct005TmpId = filterResultado.getInt("mct005TmpId");
		                	//filter.put("mct005TmpId", mct005TmpId);                	
		                	filter.put("mct005TmpId", formatoTmpId);
		                    element = ibatisService.loadElement("declaracionJurada.transmision.orden.element", filter);  
						}  
	                    String orden = element.getString("ORDEN");
	                    
	                    logger.debug("############ ORDEN: " + element.getString("ORDEN"));
						/*
						//genera N80						
						double monto = 0.0;
	                    mensajeTransmision.setIdTransaccion(transmision.getId());//Debe ir otro codigo
	                    mensajeTransmision.setTransaccion(TransaccionType.N80.getCodigo());
	                    mensajeTransmision.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_ORDEN);                    
	                    mensajeTransmision.setNumeroDocumento(orden);                    
	                    mensajeTransmision.setMonto(monto);                    
	                    mensajeTransmision.setTipoDocumentoReferencia(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_NTX);                    
	                    mensajeTransmision.setNumeroDocumentoReferencia(transmision.getIdTransaccionNtx().toString());
	                    */                    
						//Crear registro de transmision N80
	                    mensajeTransmision.setTransaccion(TransaccionType.N80.getCodigo());
	                    mensajeTransmision.setIdTransaccion(TransaccionType.N80.getId());
	                    
						transmisionRegistrada = registrarTransmisionSalienteEmpresa(transmision.getEntidadId(), mensajeTransmision);
						transmisionRegistrada.setTransaccion(TransaccionType.N80.getCodigo());
						transmisionRegistrada.setIdTransaccionNtx(transmision.getIdTransaccionNtx());	
						//genera N80
						mensajeTransmisionResultado = transaccionEmpresaService.generarNotificacion(orden, transmisionRegistrada);
						//Encolar el archivo N80
						encolarMensajeEmpresa(transmisionRegistrada, mensajeTransmisionResultado,null,null);
						                    
	                    
	
					} else {//Error
						logger.debug("############ TransmisionServiceImpl.procesarTransmisionEmpresa: La Solicitud tiene ERRORES");
						
						String numeroDocumento="";
						mensajeTransmision.setTransaccion(TransaccionType.N81.getCodigo());
	                    mensajeTransmision.setIdTransaccion(TransaccionType.N81.getId());
						//Crear registro de transmision N81
						transmisionRegistrada = registrarTransmisionSalienteEmpresa(transmision.getEntidadId(), mensajeTransmision);
						transmisionRegistrada.setTransaccion(TransaccionType.N81.getCodigo());
						transmisionRegistrada.setIdTransaccionNtx(transmision.getIdTransaccionNtx());	
						//genera N81
						mensajeTransmisionResultado = transaccionEmpresaService.generarNotificacion(numeroDocumento, transmisionRegistrada);						
						
						
						//ErroresEmpresa beanErroresEmpresa = new ErroresEmpresa();	
						HashUtil<String, Object> filter = new HashUtil<String, Object>();      
						//filter.put("mct001TmpId", filterResultado.getInt("mct001TmpId"));						
						filter.put("formatoTmpId", formatoTmpId);
						List<ErroresDatos> listErroesDatos = (List<ErroresDatos>)ibatisService.loadGenericList("transmision_errores.list", filter);											
						mensajeTransmisionResultadoConErrores.setDatos(listErroesDatos);

						//Encolar el archivo N81
						encolarMensajeEmpresaConErrores(transmisionRegistrada, mensajeTransmisionResultado, mensajeTransmisionResultadoConErrores);
						
					}	
					
					
				} else {//Generar N81
					
					//Crear registro de transmision N81
					mensajeTransmision.setTransaccion(TransaccionType.N81.getCodigo());
                    mensajeTransmision.setIdTransaccion(TransaccionType.N81.getId());					
					transmisionRegistrada = registrarTransmisionSalienteEmpresa(transmision.getEntidadId(), mensajeTransmision);
					
					transmisionRegistrada.setTransaccion(TransaccionType.N81.getCodigo());
					transmisionRegistrada.setIdTransaccionNtx(transmision.getIdTransaccionNtx());
					
					//Registra el archivo N81 - Resultado de Validacion Estructura					
					encolarMensajeEmpresaConErrores(transmisionRegistrada, 
													(TransaccionEmpresaVUCE)bytesValidacionEstructura.get(ConstantesCO.NOMBRE_NOTIFICACION), 
													(ErroresEmpresa)bytesValidacionEstructura.get(ConstantesCO.NOMBRE_ERROR_EMPRESA)
													);
					
		            // Actualizo el estado de la tabla TRANSMISION_EMPRESA
		            transmision.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_PROCESADO_EMPRESA);
//****************************/transmisionDAO.updateEstadoTransmisionEmpresa(transmision);
		            transmisionDAO.updateEstadoTransmisionEmpresa(transmision);
					
					// Actualizo el estado de la tabla TRANSMISION
		            transmision.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_EMPRESA);
		            transmisionDAO.updateEstadoTransmision(transmision);
		            
				}
				
				
				
			

		}
		
		else if (transmision.getIdTransaccion().equals(TransaccionType.N6.getId())) {
			
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("idTransmision", transmision.getId());
			Orden orden = (Orden)ibatisService.loadObject("transmision.empresa.ordenByTransmisionId.element", filter);
			filter.put("suce", orden.getSuce());			
			Suce suce = (Suce)ibatisService.loadObject("suce.carga.element", filter);
			//Suce suce = suceService.loadSuceById(orden.getSuce());
			
			//Crear registro de transmision N6
						
            mensajeTransmision.setIdTransaccion(transmision.getId());//Debe ir otro codigo
            mensajeTransmision.setTransaccion(TransaccionType.N6.getCodigo());
            mensajeTransmision.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_SUCE);                    
            mensajeTransmision.setNumeroDocumento(String.valueOf(suce.getNumSuce()));            
            mensajeTransmision.setTipoDocumentoReferencia(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_ORDEN);                    
            mensajeTransmision.setNumeroDocumentoReferencia(String.valueOf(orden.getNumOrden()));
            
			//Encolar el archivo N6
			encolarMensajeEmpresa(transmision, mensajeTransmision,null,null);
			
            // Actualizo el estado de la transmision
            transmision.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_EMPRESA);
            transmisionDAO.updateEstadoTransmision(transmision);
			
			
		} 
		
		else if (transmision.getIdTransaccion().equals(TransaccionType.N8.getId())) {
			
			// Se debe obtener los datos del CPB y la Orden
			HashUtil<String, Object> filter = new HashUtil<String, Object>();
			filter.put("idTransmision", transmision.getId());
			DR dr = (DR)ibatisService.loadObject("suce.modificacionDrByTransmisionId.element", filter);
			//Suce suce = suceService.loadSuceById(dr.getSuceId().intValue());
			filter.put("suce", dr.getSuceId());	
			Suce suce = (Suce)ibatisService.loadObject("suce.carga.element", filter);

			mensajeTransmision.setIdTransaccion(transmision.getId());//Debe ir otro codigo
            mensajeTransmision.setTransaccion(TransaccionType.N8.getCodigo());
            mensajeTransmision.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_DR);                    
            mensajeTransmision.setNumeroDocumento(String.valueOf(dr.getDr()));            
            mensajeTransmision.setTipoDocumentoReferencia(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_SUCE);                    
            mensajeTransmision.setNumeroDocumentoReferencia(String.valueOf(suce.getNumSuce()));
            
            //encolarMensajeEmpresa(transmision, mensajeTransmision);
            
            //Convertimos el mensaje resultante a xml para enviar a la empresa            
            byte [] bytesXML =  transaccionEmpresaService.generarNotificacionRespuesta(mensajeTransmision);
            
            //Coloco en un mapa todos los archivos a comprimir para enviar a la empresa
	    	HashMap<String, byte []> archivosToEmpresa = new HashMap<String, byte[]>();
	    	archivosToEmpresa.put(ConstantesCO.ADJUNTO_NOMBRE_XML, bytesXML);
	    	
			String formato = (String)ibatisService.loadObject("suce.formato.element", filter);

			byte [] ebXML = obtenerEBXMLDR(formato, dr);
	    	
	    	if (ebXML!=null) {
	    		archivosToEmpresa.put(ConstantesCO.ADJUNTO_NOMBRE_EBXML, ebXML);
	    	}
	    	
	    	
	    	byte[] zipAdjuntos = null;
	    	filter.put("drId", dr.getDrId());
	    	List<Adjunto> adjuntos = (List<Adjunto>)ibatisService.loadGenericList("dr.adjunto.firmado.element", filter);
	    			//loadObject("dr.adjunto.firmado.element", filter);
	    	
	    	HashMap<String, byte[]> files = new HashMap<String, byte[]>();	
	    	 for (Adjunto adjunto : adjuntos) {	             
	                 files.put(adjunto.getNombre(), adjunto.getArchivo());	             
	         }
	    	
	    	zipAdjuntos = ZipUtil.compressFiles(files);
	    			
	    	//String formato = (String)ibatisService.loadObject("suce.formato.element", filter);
	    	
	    	encolarMensajeEmpresa(transmision, mensajeTransmision, ebXML, zipAdjuntos);
            
            // Actualizo el estado de la transmision
            transmision.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_EMPRESA);
            transmisionDAO.updateEstadoTransmision(transmision);

			
		} 
			//N6
			//N8
			
			
			
		} catch(Exception e){
			
			logger.debug("TransmisionServiceImpl.procesarTransmisionEmpresa: Error al momento de registrar la transmision saliente para la empresa",e);
			e.printStackTrace();

		}		
	}
	
    /**
     * Registra en la tabla "transmision" el envio del mensaje y deja el mensaje en la "cola"
     * @param xml
     * @param isEBXML
     * @param adjuntos
     */
    public void encolarMensajeEmpresa(Transmision transmision, TransaccionEmpresaVUCE mensajeTransmision, byte [] solicitudEBXML, byte [] Adjuntos) throws TransmisionException {
        try {
        	
            byte [] bytesXML =  transaccionEmpresaService.generarNotificacionRespuesta(mensajeTransmision);
            
            // Grabar los archivos "bytesXML" y "bytesEBXML" en BD. Al hacer esto, el mensaje a transmitir ya puede ser tomado por la Empresa
            adjuntoLogic.grabarArchivosTransmision(transmision, bytesXML, solicitudEBXML, Adjuntos);
            logger.debug("========================== MENSAJE "+transmision.getId()+" ENCOLADO ");
            
        } catch (Exception e) {
            throw new TransmisionException(e);
        }
    }	
    

    public void encolarMensajeEmpresaConErrores(Transmision transmision, TransaccionEmpresaVUCE mensajeTransmision, ErroresEmpresa mensajeTransmisionResultadoConErrores) throws TransmisionException {
        try {
        	
            byte [] bytesXML =  transaccionEmpresaService.generarNotificacionRespuesta(mensajeTransmision);
            
            byte [] bytesXMLErrores =  transaccionEmpresaService.generarMensajeErroresEmpresa(mensajeTransmisionResultadoConErrores);
            
            // Grabar los archivos "bytesXML" y "bytesEBXML" en BD. Al hacer esto, el mensaje a transmitir ya puede ser tomado por la Empresa
            adjuntoLogic.grabarArchivosTransmision(transmision, bytesXML, bytesXMLErrores, null);
            logger.debug("========================== MENSAJE "+transmision.getId()+" ENCOLADO ");
            /*
            adjuntoLogic.grabarArchivosTransmision(transmision, bytesXML, null, null);
            logger.debug("========================== MENSAJE "+transmision.getId()+" ENCOLADO ");
            */

        } catch (Exception e) {
            throw new TransmisionException(e);
        }
    }	  
    
    public HashUtil<String, Object> procesarTransaccionEmpresa(byte[] xmlNotificacion, byte[] ebXML, byte[] bytesZipAdjuntos, Transmision transmision) {    
    	String resValidacion = "";
    	HashUtil filterResultado = new HashUtil<String, Object>();
    	TransaccionEmpresaVUCE transaccionEmpresaVUCE = transaccionEmpresaService.getTransaccionEmpresaVUCEObjOfXmlNotificacion(xmlNotificacion);
    	String codigoFormato =transaccionEmpresaVUCE.getCodigoFormato(); //MCT001 - MCT005
    	
    	//Se parsea el ebXML
    	SolicitudEBXML formatoEbXml = (SolicitudEBXML)certificadoOrigenLogic.parserEbXmlToBean(xmlNotificacion, ebXML, bytesZipAdjuntos, transmision.getId(), codigoFormato);
    	
    	//Se registra los binarios adjuntos PDF (factura y formato) del ZIP
    	//List<AdjuntoFormato> adjuntosFormato = adjuntoLogic.grabarArchivosPDFTransmision(transmision.getIdTransaccionNtx() , ((MCT001EBXML)formatoEbXml).getAdjuntos());
    	adjuntoLogic.grabarArchivosPDFTransmision(transmision.getIdTransaccionNtx() , bytesZipAdjuntos);
    	
    	
		if(codigoFormato.equalsIgnoreCase(ConstantesCO.CO_CALIFICACION_ANTICIPADA)){//MCT005
			
			//Se registra el bean en las tablas temporales
	    	int mct005TmpId = certificadoOrigenLogic.insertDeclaracionJuradaEmpresa(((MCT005EBXML)formatoEbXml).getDeclaracionJurada());
	    	
	    	//productor
	    	for(int cont=0; cont<((MCT005EBXML)formatoEbXml).getListProductores().size(); cont++ ){
	    		DeclaracionJuradaProductor declaracionJuradaProductor = ((MCT005EBXML)formatoEbXml).getListProductores().get(cont);
	    		declaracionJuradaProductor.setMct005TmpId(Util.longValueOf(mct005TmpId));
	    		certificadoOrigenLogic.insertDeclaracionJuradaProductorEmpresa(declaracionJuradaProductor);
	    	}
	    	//material
	    	for(int cont=0; cont<((MCT005EBXML)formatoEbXml).getListMateriales().size(); cont++ ){
	    		DeclaracionJuradaMaterial declaracionJuradaMaterial = ((MCT005EBXML)formatoEbXml).getListMateriales().get(cont);
	    		declaracionJuradaMaterial.setMct005TmpId(Util.longValueOf(mct005TmpId));
	    		certificadoOrigenLogic.insertDeclaracionJuradaMaterialEmpresa(declaracionJuradaMaterial);
	    	}	 
	    	//Se graban los nombres de los adjuntos del material
	    	adjuntoLogic.registrarNombreAdjuntosDJMaterial(mct005TmpId, ((MCT005EBXML)formatoEbXml).getListMateriales());
	    			
	    	//Se graban los nomnbres de los adjuntos de la solicitud EBXML (en base al nombre)
	    	adjuntoLogic.registrarNombreAdjuntosDJ(mct005TmpId, ((MCT005EBXML)formatoEbXml).getAdjuntos());

	    	
	    	//Se realiza la validacion y procesamiento de los datos de la transmision    	
			HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
	        filter.put("mct005TmpId", Util.longValueOf(mct005TmpId));
	        filter.put("transmisionEmpresaId", Util.longValueOf(transmision.getId()));
	        filter.put("documentoTipo", Util.longValueOf(1));//RUC
	        filter.put("numeroDocumento", transaccionEmpresaVUCE.getRucUsuario());
	        filter.put("usuarioSol", transaccionEmpresaVUCE.getCodigoUsuario());
	        filter.put("indValidacion", null);
	        
	        resValidacion = transmisionDAO.procesarTransaccionDeclaracionJuradaEmpresa(filter);
	        
	        filterResultado.put("resValidacion", resValidacion);
	        filterResultado.put("mct001TmpId", null);
	        filterResultado.put("mct005TmpId", mct005TmpId);	    	
   
			
		} else if(codigoFormato.equalsIgnoreCase(ConstantesCO.CO_SOLICITUD)){//MCT001
			
			//Se registra el bean en las tablas temporales
	    	int mct001TmpId = certificadoOrigenLogic.insertCertificadoOrigenEmpresa(((MCT001EBXML)formatoEbXml).getCertificadoOrigen());  
	    	
	    	//Se graban los nomnbres de los adjuntos de la solicitud EBXML (en base al nombre)
	    	adjuntoLogic.registrarNombreAdjuntos(mct001TmpId, ((MCT001EBXML)formatoEbXml).getAdjuntos());
	    	
	    	
	    	for(int cont=0; cont<((MCT001EBXML)formatoEbXml).getListCertificadoOrigenMercancia().size(); cont++ ){
	    		CertificadoOrigenMercancia certificadoOrigenMercancia = ((MCT001EBXML)formatoEbXml).getListCertificadoOrigenMercancia().get(cont);
	    		certificadoOrigenMercancia.setCoId(Util.longValueOf(mct001TmpId));
	    		certificadoOrigenLogic.insertCertificadoOrigenMercanciaEmpresa(certificadoOrigenMercancia);
	    	}
	    	
	    	for (int cont=0; cont < ((MCT001EBXML)formatoEbXml).getListCoFactura().size(); cont++ ){
	    		COFactura coFactura = ((MCT001EBXML)formatoEbXml).getListCoFactura().get(cont);
	    		coFactura.setCoId(Util.longValueOf(mct001TmpId));
	    		certificadoOrigenLogic.insertCOFacturaEmpresa(coFactura);
	    	} 
	    	//Se realiza la validacion y procesamiento de los datos de la transmision    	
			HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
	        filter.put("mct001TmpId", Util.longValueOf(mct001TmpId));
	        filter.put("transmisionEmpresaId", Util.longValueOf(transmision.getId()));
	        filter.put("documentoTipo", Util.longValueOf(1));//RUC
	        filter.put("numeroDocumento", transaccionEmpresaVUCE.getRucUsuario());
	        filter.put("usuarioSol", transaccionEmpresaVUCE.getCodigoUsuario());
	        filter.put("indValidacion", null);
	        
	        resValidacion = transmisionDAO.procesarTransaccionEmpresa(filter);
	        
	        filterResultado.put("resValidacion", resValidacion);
	        filterResultado.put("mct001TmpId", mct001TmpId);
	        filterResultado.put("mct005TmpId", null);
		}
    	
    		
    	   	

            	
    	return filterResultado;
    }

    /**
     * Registra una transmision para que la empresa lo consuma 
     * @param idEntidad
     * @param transmisionEmpresaVUCE
     * @return
     * @throws Exception
     */
    //public Transmision registrarTransmisionSalienteEmpresa(int idEntidad, TransaccionEmpresaVUCE transmisionEmpresaVUCE) throws Exception { // 02/12/2016 Se comento xq ya no se usa el tipo de datos TransaccionEmpresaVUCE 
    public Transmision registrarTransmisionSalienteEmpresa(int idEntidad, TransmisionVUCE transmisionEmpresaVUCE) throws Exception { //JMC 02/12/2016 ALIANZA PACIFICO
        return registrarTransmision(idEntidad, transmisionEmpresaVUCE, ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_EMPRESA);
    }
    public Transmision registrarTransmisionIOPSalienteParaPack(int idEntidad, TransmisionVUCE transmisionVUCE) throws Exception {
        return registrarTransmision(idEntidad, transmisionVUCE, ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_A_IOP);
    }
    
    /**
     * Registra la transmision 
     * @param idEntidad
     * @param transmisionVUCE
     * @param estado
     * @return
     * @throws Exception
     */
    // JMC 02/12/2016 Alianza Pacifico
    //public Transmision registrarTransmision(int idEntidad, TransaccionEmpresaVUCE transmisionVUCE, int estado) throws Exception { // 02/12/2016 Se comento xq ya no se usa el tipo de datos TransaccionEmpresaVUCE 
    public Transmision registrarTransmision(int idEntidad, TransmisionVUCE transmisionVUCE, int estado) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idVC", null);
        filter.put("idEntidad", idEntidad);
        filter.put("estadoVC", estado);
        filter.put("idTransaccion", TransaccionType.getTransaccionTypeByCodigo(transmisionVUCE.getTransaccion()).getId());
        filter.put("tipoTransmision", ConstantesCO.TIPO_TRANSMISION_NOTIFICACION_EMPRESA_ORIGEN);
        
      
        if (transmisionVUCE instanceof TransaccionIOPVUCE) {
            filter.put("tceId", ((TransaccionIOPVUCE)transmisionVUCE).getTceId());
            filter.put("drIOPId", ((TransaccionIOPVUCE)transmisionVUCE).getDrIOPId());
            filter.put("idVC", null);
            filter.put("tceAlianzaId", null);
            
            transmisionDAO.registrarTransmisionIOP(filter);
            
            ((TransaccionIOPVUCE)transmisionVUCE).setTceAlianzaId(filter.getInt("tceAlianzaId"));
        } else {
        	transmisionDAO.registrarTransmision(filter);
        }
        
         
        //transmisionDAO.registrarTransmision(filter);
        
        int idVC = filter.getInt("idVC");
        Transmision transmision = transmisionDAO.loadTransmision(idVC);
        /*
        if (transmisionVUCE instanceof TransaccionVUCE) {
            // Seteamos el idVC
            ((TransaccionVUCE)transmisionVUCE).setIdTransaccion(idVC);
        }
        */
        return transmision;
    }
    
    public byte [] obtenerEBXMLDR(String formato, DR dr) throws Exception {
    	
    	byte [] ebXML = null;
    	if (formato.equalsIgnoreCase(ConstantesCO.CO_SOLICITUD)){//MCT001
    		MCT001EBXML mct001ebxml = certificadoOrigenLogic.convertirMCT001(dr);
    		ebXML = transaccionEmpresaService.generarEBXML((SolicitudEBXML)mct001ebxml);
    	}
    	
    	if (formato.equalsIgnoreCase(ConstantesCO.CO_CALIFICACION_ANTICIPADA)){//MCT005    		
    		MCT005EBXML mct005ebxml = new MCT005EBXML(); 
    		mct005ebxml = certificadoOrigenLogic.convertirMCT005(dr);
    		ebXML = transaccionEmpresaService.generarEBXML((SolicitudEBXML)mct005ebxml);
    	}
    	return ebXML;
	}
    // JMC 02/12/2016 Alianza Pacifico
    public Transmision loadTransmision(int idVC) throws Exception {
        return transmisionDAO.loadTransmision(idVC);
    }
    
    public void updateEstadoTransmision(Transmision transmision) throws Exception {
        transmisionDAO.updateEstadoTransmision(transmision);
    }

    public void encolarMensajePack(String token, Integer secuenciaMaxima, Transmision transmision, TransaccionIOPVUCE mensajeTransmision, byte [] solicitudEBXML, byte [] Adjuntos) throws TransmisionException {
        try {        	

            byte [] bytesXML =  transaccionPackService.generarMensajePack(mensajeTransmision);
            InteroperabilidadFirma iopGet = iopDAO.obtenerXml(token, secuenciaMaxima);
            solicitudEBXML = iopGet.getXml();
            
            // Grabar los archivos "bytesXML" y "bytesEBXML" en BD. Al hacer esto, el mensaje a transmitir ya puede ser tomado por la Empresa
            adjuntoLogic.grabarArchivosTransmision(transmision, bytesXML, solicitudEBXML, null);
            logger.debug("========================== MENSAJE "+transmision.getId()+" ENCOLADO ");
            
        } catch (Exception e) {
            throw new TransmisionException(e);
        }
    }

    public void encolarMensajeRespuestaPack(Transmision transmision, TransaccionIOPVUCE mensajeTransmision, byte [] solicitudEBXML, byte [] Adjuntos) throws TransmisionException {
        try {        	

            byte [] bytesXML =  transaccionPackService.generarMensajePack(mensajeTransmision);
            // Grabar los archivos "bytesXML" y "bytesEBXML" en BD. Al hacer esto, el mensaje a transmitir ya puede ser tomado por el Pack
            adjuntoLogic.grabarArchivosTransmision(transmision, bytesXML, solicitudEBXML, null);
            logger.debug("========================== MENSAJE "+transmision.getId()+" ENCOLADO ");
            
        } catch (Exception e) {
            throw new TransmisionException(e);
        }
    }
    
	public byte[] generarNotificacionRespuesta(TransaccionEmpresaVUCE mensajeTransmision) throws Exception {
		
		return XMLUtil.generarMensajeRespuesta(mensajeTransmision);
	}	

	//20170510 JMC Alianza Pacifico, para la Recepcion del Certificado
    public List<Transmision> loadListIOP(HashUtil<String, Integer> filter) throws Exception {
    	return transmisionDAO.loadListIOP(filter);
    }

    /**
     * Carga el XML de la BD y lo registra en las tablas de IO Alianza Pacifico 
     */    
    public Transmision procesarTransmisionIOPEntranteDePack(Transmision transmision) throws Exception {    	
		List<Adjunto> adjuntos = adjuntoLogic.cargarArchivosTransmisionIOP(transmision);		
		byte [] bytesEBXMLDR = adjuntoLogic.getEBXMLFromList(adjuntos);
		//Guardar Certificado en BD
		Acuerdo26IOPEBXML docResolutivoSecEBXML = (Acuerdo26IOPEBXML) procesoDR.procesarIOPRecepcionVUCEs(transmision, bytesEBXMLDR);    	
    	String numeroCertificado = docResolutivoSecEBXML.getNumeroCertificado();    	
    	String tipoDocumentoIOP = docResolutivoSecEBXML.getTipoDocumentoIOP();    	
    	String paisDestinoIOP = docResolutivoSecEBXML.getPaisDestinoIOP();
    	Integer codId = docResolutivoSecEBXML.getCodId();
    	
    	logger.debug("*********procesarTransmisionIOPEntranteDePack");
    	
    	//Valida Certificado Origen
    	String resultado = certificadoOrigenLogic.validaCOFD(codId);
    	
    	logger.debug("*************Resultado: "+ resultado);
    	
    	if( ConstantesCO.OPCION_SI.equals(resultado)){
	    	//Generar-PDF
    		byte [] pdfBytes = certificadoOrigenLogic.generarReporteAPBytes(codId);
    		certificadoOrigenLogic.insertArchivoCOFD(pdfBytes,codId);
	    	//Actualizar estado para que lo agarre VUCE y genere el N92 y encole a la entidad competente
	        transmision.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_POR_ENVIAR_IOP_A_VE);
	        transmisionDAO.updateEstadoTransmision(transmision);
    	} else {
	    	//Actualizar estado para que no se procese ni genere N92 ni envie a la entidad
	        transmision.setEstadoVC(ConstantesCO.TRANSMISION_ESTADO_VC_VALIDACION_PROCESADO_CON_ERRORES);
	        transmisionDAO.updateEstadoTransmision(transmision);
    	}    	
    	
    	iopService.procesarTransmisionRespuestaParaPack(codId, resultado);
    	
    	return null;
    }


}
