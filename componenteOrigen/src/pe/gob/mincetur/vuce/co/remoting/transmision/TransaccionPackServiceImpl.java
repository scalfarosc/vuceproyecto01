package pe.gob.mincetur.vuce.co.remoting.transmision;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.procesodr.ProcesoDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionIOPVUCE;
import pe.gob.mincetur.vuce.co.util.xml.XMLUtil;

public class TransaccionPackServiceImpl implements TransaccionPackService{
	
	private ProcesoDR procesoDR;
	
	public void setProcesoDR(ProcesoDR procesoDR) {
		this.procesoDR = procesoDR;
	}

	public byte[] generarMensajePack(TransaccionIOPVUCE mensajeTransmision) throws Exception {
		
		return XMLUtil.generarMensajePack(mensajeTransmision);
	}	

	public DocResolutivoSecEBXML procesarIOPRecepcionVUCEs(Transmision transmision, byte[] ebXMLDR) throws Exception {
		return procesoDR.procesarIOPRecepcionVUCEs(transmision,ebXMLDR);
	}
	
	public DocResolutivoSecEBXML procesarIOPRecepcionVUCEsUpdate(Transmision transmision, byte[] ebXMLDR) throws Exception {
		return procesoDR.procesarIOPRecepcionVUCEs(transmision,ebXMLDR);
	}

}
