package pe.gob.mincetur.vuce.co.remoting.transmision.bean;

import java.util.List;

public abstract class TransmisionVUCE {

	   private String template;
	    
	    private int entidad;
	    
	    private String fecha;

	    private String transaccion;
	    
	    private String tipoDocumento;
	    
	    private String numeroDocumento;
	    
	    private String tipoDocumentoReferencia;
	    
	    private String numeroDocumentoReferencia;
	    
	    private List<?> adjuntos;

		public String getTemplate() {
			return template;
		}

		public void setTemplate(String template) {
			this.template = template;
		}

		public int getEntidad() {
			return entidad;
		}

		public void setEntidad(int entidad) {
			this.entidad = entidad;
		}

		public String getFecha() {
			return fecha;
		}

		public void setFecha(String fecha) {
			this.fecha = fecha;
		}

		public String getTransaccion() {
			return transaccion;
		}

		public void setTransaccion(String transaccion) {
			this.transaccion = transaccion;
		}

		public String getTipoDocumento() {
			return tipoDocumento;
		}

		public void setTipoDocumento(String tipoDocumento) {
			this.tipoDocumento = tipoDocumento;
		}

		public String getNumeroDocumento() {
			return numeroDocumento;
		}

		public void setNumeroDocumento(String numeroDocumento) {
			this.numeroDocumento = numeroDocumento;
		}

		public String getTipoDocumentoReferencia() {
			return tipoDocumentoReferencia;
		}

		public void setTipoDocumentoReferencia(String tipoDocumentoReferencia) {
			this.tipoDocumentoReferencia = tipoDocumentoReferencia;
		}

		public String getNumeroDocumentoReferencia() {
			return numeroDocumentoReferencia;
		}

		public void setNumeroDocumentoReferencia(String numeroDocumentoReferencia) {
			this.numeroDocumentoReferencia = numeroDocumentoReferencia;
		}

		public List<?> getAdjuntos() {
			return adjuntos;
		}

		public void setAdjuntos(List<?> adjuntos) {
			this.adjuntos = adjuntos;
		}
	    
	    
}
