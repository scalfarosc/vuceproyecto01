/*
 * VuceCentralWSImpl.java
 */
package pe.gob.mincetur.vuce.co.remoting.ws.entidad;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.MTOM;

import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.cxf.attachment.ByteDataSource;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.logic.AdjuntoLogic;
import pe.gob.mincetur.vuce.co.logic.InteroperabilidadFirmasLogic;
import pe.gob.mincetur.vuce.co.remoting.transmision.ProcesoFirmadoExecutor;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;
import pe.gob.mincetur.vuce.co.util.file.ZipUtil;

/**
 * 
 * @author Erick Oscategui
 */
//@MTOM
@WebService
public class VuceOrigenWSImpl implements VuceOrigenWS {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
    
    private InteroperabilidadFirmasLogic iopLogic;   

    private ProcesoFirmadoExecutor procesoFirmadoExecutor; 
    
    @Resource
    private WebServiceContext wsContext;
    
    public void setIopLogic(InteroperabilidadFirmasLogic iopLogic) {
		this.iopLogic = iopLogic;
	}

	public void setProcesoFirmadoExecutor(
			ProcesoFirmadoExecutor procesoFirmadoExecutor) {
		this.procesoFirmadoExecutor = procesoFirmadoExecutor;
	}
	
	public DataHandler generarXml(String token)  throws Exception {
		logger.debug("INICIA - GENERAR XML FIRMADO: "+token);
    	DataHandler resultado = null;
    	try {
    		NDC.push("GENERAR XML: "+ token);
	    	byte [] xmlGenerado = iopLogic.generarXml(token);
			ByteArrayDataSource dsResultado = new ByteArrayDataSource(xmlGenerado, "application/xml");
			
			resultado = new DataHandler(dsResultado);
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
            NDC.pop();
            NDC.remove();
        }
    	
    	logger.debug("FINALIZA - GENERAR XML FIRMADO: "+token);
        return resultado;
    }
    
    public DataHandler generarPdf(String token)  throws Exception {
    	logger.debug("INICIA - GENERAR PDF FIRMADO: "+token);
    	DataHandler resultado = null;
    	try {
	    	byte [] pdfGenerado = iopLogic.generarPdf(token);
			ByteArrayDataSource dsResultado = new ByteArrayDataSource(pdfGenerado, "application/pdf");
			resultado = new DataHandler(dsResultado);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	logger.debug("FINALIZA - GENERAR PDF FIRMADO: "+token);
        return resultado;
    }

    public Integer enviarXmlFirmado(String token, DataHandler xml) throws Exception {
    	logger.debug("INICIA - ENVIAR XML FIRMADO: "+token);
    	Integer result = 0;
    	try {
	    	final InputStream xmlIs = xml.getInputStream();
	        BufferedReader xmlBR = new BufferedReader(
	        		new InputStreamReader(xmlIs));
	        		//new InputStreamReader(xmlIs, "UTF-8"));	                

	    	byte[] xmlBytes = IOUtils.toByteArray(xmlBR);
	    	iopLogic.enviarXmlFirmado(token, xmlBytes);
	    	//JMC 13/04/2018 Generar PDF sin firma - solucion alterna 	    	
	    	//transmisionExecutor.crearPDFSinFirma(token);
	    	procesoFirmadoExecutor.procesarGeneracionPDF(token);
	    	
	    	result = 1;
    	} catch(Exception e) {
    		logger.error("Ocurrio un error al ENVIAR XML FIRMADO", e);
    	}
    	logger.debug("FINALIZA - ENVIAR XML FIRMADO: "+token);
        return result;
    }
    
    public Integer enviarPdfFirmado(String token, DataHandler pdf) throws Exception {
    	logger.debug("INICIA - ENVIAR PDF FIRMADO: "+token);
    	Integer result = 0;
    	try {
	    	final InputStream pdfIs = pdf.getInputStream();
	    	byte[] pdfBytes = IOUtils.toByteArray(pdfIs);    	
	    	iopLogic.enviarPdfFirmado(token, pdfBytes);
	    	result = 1;
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	logger.debug("FINALIZA - ENVIAR PDF FIRMADO: "+token);
        return result;
    }
    
    /*HT: SUNAT*/
   	public DataHandler obtenerCertificado(String certificadoOrigen, String codigoPais) throws Exception {
   		
   		logger.debug("INICIA - ENVIAR CERTIFICADO : "+certificadoOrigen);
   		DataHandler dhArchivo = null;
       	try {		
       		RespuestaCert adjunto = iopLogic.obtenerCertificado(certificadoOrigen, codigoPais);						
   			HashMap<String, byte []> files = new HashMap<String, byte []>();
   			files.put(certificadoOrigen, adjunto.getArchivo());
   			
   			byte [] byteArchivo = ZipUtil.compressFiles(files);
   			DataSource dsArchivo = new ByteDataSource(byteArchivo);
   			dhArchivo = new DataHandler(dsArchivo);
   			
       	} catch(Exception e) {
       		e.printStackTrace();
       	}
       	logger.debug("FINALIZA - ENVIAR PDF FIRMADO: "+certificadoOrigen);
   		
   		return dhArchivo;
   	}

    
	
}
