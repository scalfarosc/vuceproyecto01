package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.remoting.transmision.exception.TransmisionException;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class TransmisionExecutorImpl implements TransmisionExecutor{
	
    private static Logger logger = Logger.getLogger(org.jlis.core.util.Constantes.LOGGER_SERVICE);
    
    private TransmisionService transmisionService;
    
    private Stack<Object> stack = new Stack<Object>();
    
    private boolean inicioEjecucion = false;
    
    private ThreadPoolTaskExecutor sunatAduanasOrigenTaskExecutorThreadPool;
    
    public void setTransmisionService(TransmisionService transmisionService) {
        this.transmisionService = transmisionService;
    }
    
    public void setInicioEjecucion(boolean inicioEjecucion) {
		this.inicioEjecucion = inicioEjecucion;
	}

	public boolean isInicioEjecucion() {
		return inicioEjecucion;
	}

	public Stack<Object> getStack() {
		return stack;
	}

	public void setSunatAduanasOrigenTaskExecutorThreadPool(
			ThreadPoolTaskExecutor sunatAduanasOrigenTaskExecutorThreadPool) {
		this.sunatAduanasOrigenTaskExecutorThreadPool = sunatAduanasOrigenTaskExecutorThreadPool;
	}

    private class TransmisionTask implements Runnable {
        
    	private String tipoProceso;
    	
		private TransmisionExecutor tex;
		
        private List<Transmision> transmisionesEntidad;
        
        public TransmisionTask(List<Transmision> transmisionesEntidad, TransmisionExecutor tex, String tipoProceso) {
            this.transmisionesEntidad = transmisionesEntidad;
            this.tex = tex;
            this.tipoProceso = tipoProceso;
        }
        
        public void run() {
        	if ("ENVIO".equals(tipoProceso)) {
        	    this.tex.setInicioEjecucion(true);
        	    this.tex.getStack().push(this);
        	}
        	
        	/////////////////////////////////////////////////////////////////////
        	//Esto es parte de la ETAPA 2 - Interoperabilidad de CO con Aduanas//
        	/////////////////////////////////////////////////////////////////////
        	/*
            try {
            	//boolean problemaTransmision = false;
                for (Transmision transmision : transmisionesEntidad) {
                	NDC.push("VC_ID: "+transmision.getId());
                	
                    // Invoco al envio de mensajes
                    if (transmision.getEntidad().equals(ConstantesCO.ENTIDAD_SUNAT_ORIGEN)) {
                        // Invocar a SUNAT
                        transmisionService.enviarMensajeSunat(transmision);
                    } else {
                        // Invocar a las Entidades
                        transmisionService.enviarMensajeEntidad(transmision);
                    }
                    NDC.pop();
                    NDC.remove();
                }
            } catch (TransmisionException e) {
                logger.error("ERROR EN EL ENVIO DEL MENSAJE", e);
            } catch (Exception e) {
                logger.error("ERROR EN EL ENVIO DEL MENSAJE", e);
            } finally {
            	NDC.pop();
                NDC.remove();
                if ("ENVIO".equals(tipoProceso)) {
                    this.tex.getStack().pop();
                }
            }
            */
        }
        
    }

	public void ejecutarTransmisiones(HashMap<Integer, List<Transmision>> transmisiones, String tipoProceso) {
        Iterator<Integer> it = transmisiones.keySet().iterator();
        
        /*if ("ENVIO".equals(tipoProceso)) {
        	this.inicioEjecucion = false;
        }
        */
        while (it.hasNext()) {
            Integer claveEntidad = it.next();
            List<Transmision> transmisionesEntidad = transmisiones.get(claveEntidad);
            switch (claveEntidad) {
                case ConstantesCO.ENTIDAD_SUNAT_ORIGEN : sunatAduanasOrigenTaskExecutorThreadPool.execute(new TransmisionTask(transmisionesEntidad, this, tipoProceso)); break;
            }
        }
        
        if ("ENVIO".equals(tipoProceso)) {
	        while (!this.inicioEjecucion || !this.stack.empty()) {
	        	try {
	                System.out.println("ESPERANDO AL PROCESO DE TRANSMISION... "+this.stack.empty()+" , "+this.inicioEjecucion);
	                Thread.sleep(5000);
	        	} catch (InterruptedException e) {
	               Thread.currentThread().interrupt();
	            }
	        	if (this.stack.empty()) break;
	        }
	        
	        this.inicioEjecucion = false;
	        this.stack.clear();
        }
    }

}
