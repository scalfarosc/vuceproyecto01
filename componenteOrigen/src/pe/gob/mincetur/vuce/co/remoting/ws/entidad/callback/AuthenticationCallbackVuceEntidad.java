package pe.gob.mincetur.vuce.co.remoting.ws.entidad.callback;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;

import pe.gob.mincetur.vuce.co.util.ComponenteOrigenUtil;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class AuthenticationCallbackVuceEntidad implements CallbackHandler {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
    
    /**
     * Para autenticar a VUCE Entidad
     */
    public void handle(Callback [] callbacks) throws IOException, UnsupportedCallbackException {
        BASE64Decoder base64Decoder = new BASE64Decoder();
        
        WSPasswordCallback pc = (WSPasswordCallback)callbacks[0];
        
        String userName = pc.getIdentifier();
        
        if (!userName.equals("VUCE_SIGNER_WS_SECURITY") && !userName.equals("WSSUNATVUCE")) { // SUNAT se conecta para obtener los adjuntos
            throw new SecurityException("Usuario "+userName+" incorrecto");
        }
        
        String secretKeyGUID = ApplicationConfig.getApplicationConfig().getProperty("co.secret.guid.vuceSigner"); //D0AFB431-CB92-4B00-8B59-93728E772DCB
        
        if (userName.equals("WSSUNATVUCE")) { // SUNAT se conecta para obtener los adjuntos
        	secretKeyGUID = ApplicationConfig.getApplicationConfig().getProperty("co.secret.guid.sunat"); //D0AFB431-CB92-4B00-8B59-93728E772DCB
        }
        
        String passKey = new String(base64Decoder.decodeBuffer(pc.getPassword()));
        String publicKeyGUID = passKey.substring(0, 36);
        byte [] hashCliente = passKey.substring(36, passKey.length()).getBytes();
        byte [] hash = ComponenteOrigenUtil.getHash(publicKeyGUID+secretKeyGUID);
        
        //System.out.println("AuthenticationCallback - userName: "+userName+", password: "+new String(hashCliente)+", password Local: "+new String(hash));
        BASE64Encoder base64Encoder = new BASE64Encoder();
        String password = base64Encoder.encode((publicKeyGUID+new String(ComponenteOrigenUtil.getHash(publicKeyGUID+secretKeyGUID))).getBytes());
        String clientPassword = pc.getPassword();
        
        System.out.println("AuthenticationCallback - userName: "+userName+", password: "+password+", password Local: "+clientPassword+"\nResultado: "+!password.equals(clientPassword));
        int i = 0;
        for (char x : clientPassword.toCharArray()) {
        	if (x == password.charAt(i)) {
        	logger.debug("CHAR "+x+" IGUAL");
        	i++;
        	} else {
        	logger.debug("CHAR "+x+" DIFERENTE A "+password.charAt(i));
        	}
        }
        
        if (i < 48) {//if (!password.equals(clientPassword)) {
            throw new SecurityException("Autenticacion incorrecta");
        }
    }
    
   
    

}
