package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.remoting.transmision.TransmisionService;

public class ProcesoTransmisionIOPImpl implements ProcesoTransmisionIOP {

	private static Logger logger = Logger.getLogger(org.jlis.core.util.Constantes.LOGGER_SERVICE);
    
    private TransmisionExecutor transmisionExecutor;
    
    private TransmisionService transmisionService;
    
    public void setTransmisionExecutor(TransmisionExecutor transmisionExecutor) {
        this.transmisionExecutor = transmisionExecutor;
    }
    
    public void setTransmisionService(TransmisionService transmisionService) {
        this.transmisionService = transmisionService;
    }

    /**
     * Proceso bachero. Lee de la tabla "transmision" los mensajes cuyo estado "evc" indica que estan pendientes de registrar en BD por vuce - Alianza Pacifico
     */

	public void procesarTransmisionesPendientes() {
		// TODO Auto-generated method stub
		String tipoProceso = "RECEPCION";
		
        try {
            // Obtengo una lista de Transmisiones pendientes
            logger.debug("INICIO DEL PROCESO DE ENVIO DE MENSAJES PENDIENTES IOP A SUNAT("+this.transmisionExecutor+")");
            
            HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
            filter.put("estadoVCEntidad", ConstantesCO.TRANSMISION_ESTADO_VC_POR_REGISTRAR_XML_IOP_A_BD);
            
            List<Transmision> listaTransmisiones = transmisionService.loadListIOP(filter);
            HashMap<Integer, List<Transmision>> transmisiones = new HashMap<Integer, List<Transmision>>();
            for (Transmision transmision : listaTransmisiones) {
                // Agrego el mensaje al componente de envio de mensajes
                /*Integer idEntidad = transmision.getEntidad();
                List<Transmision> transmisionesEntidad = transmisiones.get(idEntidad);
                if (transmisionesEntidad==null) {
                    transmisionesEntidad = new ArrayList<Transmision>();
                    transmisiones.put(idEntidad, transmisionesEntidad);
                }
                transmisionesEntidad.add(transmision);
                */
            	transmisionService.procesarTransmisionIOPEntranteDePack(transmision);
            }

            //Registrar los datos del Certificado en la BD, que estan en la transmision N92
            

            //transmisionExecutor.h(transmisiones, tipoProceso); //Se habilitara en la Etapa 2 - JMC
            logger.debug("FIN DEL PROCESO DE ENVIO DE MENSAJES PENDIENTES IOP");
        } catch (Exception e) {
            logger.error("ERROR EN EL PROCESO DE ENVIO DE MENSAJES PENDIENTES IOP", e);
        }		
		
		
	}	
}
