package pe.gob.mincetur.vuce.co.remoting.ws.client;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pe.gob.mincetur.vuce.co.dao.ConsultaRucDAO;
import pe.gob.sunat.wwss.jboss_net.services.ConsultaRuc.DatosRucWS;
import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanRso;
import ConsultaRuc.BeanSpr;
import ConsultaRuc.BeanT1144;
import ConsultaRuc.BeanT1150;
import ConsultaRuc.BeanT362;

public class ConsultaRUCCWS {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
    
	private final static Integer TIMEOUT = 5000;
	
	public static boolean servicioDisponible() {
		try {
			String urlWS = ApplicationConfig.getApplicationConfig().getProperty("sunat.ws.consultaRUC");
			HttpURLConnection connection = (HttpURLConnection) new URL(urlWS).openConnection();
			connection.setConnectTimeout(TIMEOUT);
			connection.setReadTimeout(TIMEOUT);
			connection.setRequestMethod("HEAD");
			int responseCode = connection.getResponseCode();
			connection.disconnect();
			return (200 <= responseCode && responseCode <= 399);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean servicioAntiguoDisponible() {
		try {
			String urlWS = ApplicationConfig.getApplicationConfig().getProperty("sunat.ws.consultaRUCAntiguo");
			HttpURLConnection connection = (HttpURLConnection) new URL(urlWS).openConnection();
			connection.setConnectTimeout(TIMEOUT);
			connection.setReadTimeout(TIMEOUT);
			connection.setRequestMethod("HEAD");
			int responseCode = connection.getResponseCode();
			connection.disconnect();
			return (200 <= responseCode && responseCode <= 399);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
    public static BeanDdp getDatosPrincipales(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
    	BeanDdp datosPrincipales = null;
    	Integer modoConsulta = null;
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return datosPrincipales;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Datos Principales");				
				crDao.insertTrazabilidad(ruc, "BeanDdp", null, modoConsulta);
				datosPrincipales = crDao.loadDatosPrincipales(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Datos Principales desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
        	if (!ConsultaRUCCWS.servicioDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanDdp", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Datos Principales");
	            		datosPrincipales = crDao.loadDatosPrincipales(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Datos Principales desde la BD", e);
    			}
        		return datosPrincipales;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "BeanDdp", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClient();
    			datosPrincipales = datosRucWSClient.getDatosPrincipales(ruc);
    			if (datosPrincipales != null) {
    				logger.info("Insertando en BD Datos Principales");
    				crDao.insertDatosPrincipales(ruc, datosPrincipales);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getDatosPrincipales'", e);
    		}
    	}
		return datosPrincipales;
    }
    
    public static BeanDds getDatosSecundarios(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        BeanDds datosSecundarios = null;
    	Integer modoConsulta = null;
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return datosSecundarios;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Datos Secundarios");				
				crDao.insertTrazabilidad(ruc, "BeanDds", null, modoConsulta);
				datosSecundarios = crDao.loadDatosSecundarios(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Datos Secundarios desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
        	if (!ConsultaRUCCWS.servicioAntiguoDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanDds", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Datos Secundarios");
	        			datosSecundarios = crDao.loadDatosSecundarios(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Datos Secundarios desde la BD", e);
    			}
        		return datosSecundarios;
        	}
        	
            try {
               	crDao.insertTrazabilidad(ruc, "BeanDds", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClientAntiguo();//WebServiceFactory.getSunatConsultaRUCClient();
               	datosSecundarios = datosRucWSClient.getDatosSecundarios(ruc);
    			if (datosSecundarios != null) {
    				logger.info("Insertando en BD Datos Secundarios");
    				crDao.insertDatosSecundarios(ruc, datosSecundarios);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getDatosSecundarios'", e);
    		}
    	}
    	
		return datosSecundarios;
    }
    
    public static BeanT1144 getDatosT1144(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        BeanT1144 datosT1144 = null;
    	Integer modoConsulta = null;
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return datosT1144;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Datos T1144");				
				crDao.insertTrazabilidad(ruc, "BeanT1144", null, modoConsulta);
				datosT1144 = crDao.loadDatosT1144(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Datos T1144 desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS)  
    		
        	if (!ConsultaRUCCWS.servicioAntiguoDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanT1144", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Datos T1144");
	        			datosT1144 = crDao.loadDatosT1144(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Datos T1144 desde la BD", e);
    			}
        		return datosT1144;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "BeanT1144", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClientAntiguo();//WebServiceFactory.getSunatConsultaRUCClient();
               	datosT1144 = datosRucWSClient.getDatosT1144(ruc);
    			if (datosT1144 != null) {
    				logger.info("Insertando en BD Datos T1144");
    				crDao.insertDatosT1144(ruc, datosT1144);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getDatosT1144'", e);
    		}
    	}
		return datosT1144;
    }
    
    public static BeanT362 [] getDatosT362(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        BeanT362 [] datosT362 = null;
    	Integer modoConsulta = null;    	
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return datosT362;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Datos T362");				
				crDao.insertTrazabilidad(ruc, "BeanT362", null, modoConsulta);
			} catch (Exception e) {
				logger.error("ERROR al procesar Datos T362 desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
    		
        	if (!ConsultaRUCCWS.servicioDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanT362", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Datos T362");
	        			datosT362 = crDao.loadDatosT362(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Datos T362 desde la BD", e);
    			}
        		return datosT362;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "BeanT362", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClient();
    			Object [] list = datosRucWSClient.getDatosT362(ruc);
    			datosT362 = new BeanT362 [list.length];
    			
    			for (int i=0; i < list.length; i++) {
    				Object obj = list[i];
    				datosT362[i] = (BeanT362)obj;
    			}               	
    			if (datosT362 != null) {
    				logger.info("Insertando en BD Datos T362");
    				crDao.insertDatosT362(ruc, datosT362);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getDatosT362'", e);
    		}    		
    	}

		return datosT362;
    }
    
    public static BeanRso [] getRepLegales(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        BeanRso [] representantesLegales = null;
    	Integer modoConsulta = null;    
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return representantesLegales;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Datos RepLegales");				
				crDao.insertTrazabilidad(ruc, "BeanRso", null, modoConsulta);
				representantesLegales = crDao.loadRepLegales(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Datos RepLegales desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
    		
        	if (!ConsultaRUCCWS.servicioDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanRso", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Datos RepLegales");
	        			representantesLegales = crDao.loadRepLegales(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Datos RepLegales desde la BD", e);
    			}
        		return representantesLegales;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "BeanRso", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClient();
    			Object [] list = datosRucWSClient.getRepLegales(ruc);
    			representantesLegales = new BeanRso [list.length];
    			
    			for (int i=0; i < list.length; i++) {
    				Object obj = list[i];
    				representantesLegales[i] = (BeanRso)obj;
    			}              	
    			if (representantesLegales != null) {
    				logger.info("Insertando en BD Datos RepLegales");
    				crDao.insertRepLegales(ruc, representantesLegales);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getRepLegales'", e);
    		}    		
    	}    	

		return representantesLegales;
    }
    
    public static String getDomicilioLegal(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        String domicilioLegal = null;
    	Integer modoConsulta = null;    	
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return domicilioLegal;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Domicilio Legal");				
				crDao.insertTrazabilidad(ruc, "DomicilioLegal", null, modoConsulta);
				domicilioLegal = crDao.loadDomicilioLegal(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Domicilio Legal desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
    		
        	if (!ConsultaRUCCWS.servicioDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "DomicilioLegal", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Domicilio Legal");
	        			domicilioLegal = crDao.loadDomicilioLegal(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Domicilio Legal desde la BD", e);
    			}
        		return domicilioLegal;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "DomicilioLegal", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClient();
               	domicilioLegal = datosRucWSClient.getDomicilioLegal(ruc);              	
    			if (domicilioLegal != null) {
    				logger.info("Insertando en BD Domicilio Legal");
    				crDao.insertDomicilioLegal(ruc, domicilioLegal);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getDomicilioLegal'", e);
    		}    		
    	}

		return domicilioLegal;
    }
    
    public static BeanSpr [] getEstablecimientosAnexos(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        BeanSpr [] establecimientosAnexos = null;
    	Integer modoConsulta = null;    	
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return establecimientosAnexos;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Establecimientos Anexos");				
				crDao.insertTrazabilidad(ruc, "BeanSpr", null, modoConsulta);
				establecimientosAnexos = crDao.loadEstAnexos(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Establecimientos Anexos desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
    		
        	if (!ConsultaRUCCWS.servicioDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanSpr", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Establecimientos Anexos");
	        			establecimientosAnexos = crDao.loadEstAnexos(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Establecimientos Anexos desde la BD", e);
    			}
        		return establecimientosAnexos;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "BeanSpr", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClient();
    			Object [] list = datosRucWSClient.getEstablecimientosAnexos(ruc);
    			establecimientosAnexos = new BeanSpr [list.length];
    			
    			for (int i=0; i < list.length; i++) {
    				Object obj = list[i];
    				establecimientosAnexos[i] = (BeanSpr)obj;
    			}              	
    			if (establecimientosAnexos != null) {
    				logger.info("Insertando en BD Establecimientos Anexos");
    				crDao.insertEstAnexos(ruc, establecimientosAnexos);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getEstablecimientosAnexos'", e);
    		}    		
    	}

		return establecimientosAnexos;
    }
    
    public static BeanT1150 [] getEstAnexosT1150(String ruc) {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
        ConsultaRucDAO crDao = (ConsultaRucDAO) ctx.getBean("consultaRucDAO");
        logger.info("Obteniendo ConsultaRucDAO " + crDao);
        BeanT1150 [] estAnexosT1150 = null;
    	Integer modoConsulta = null;    	
    	
    	try {
			modoConsulta = crDao.loadModoConsulta();
			logger.info("Modo Consulta = " + modoConsulta);
		} catch (Exception e1) {
			logger.error("ERROR al tratar de obtener el Modo Consulta desde la BD", e1);
			// Si no hay un modo consulta no podemos hacer nada mas
			return estAnexosT1150;
		}
    	
    	if (modoConsulta == 2) { // Si es solo contingencia, no debemos usar el Servicio Web para nada
    		logger.info("Solo Contingencia");
			try {
    			logger.info("Procesando en BD Datos T1150");				
				crDao.insertTrazabilidad(ruc, "BeanT1150", null, modoConsulta);
				estAnexosT1150 = crDao.loadDatosT1150(ruc);
			} catch (Exception e) {
				logger.error("ERROR al procesar Datos T1150 desde la BD", e);
			}
    	} else { // Casos 1 (Ambas) y 3 (Solo WS) 
    		
        	if (!ConsultaRUCCWS.servicioDisponible()) {
        		logger.info("Servicio SUNAT No Disponible");
        		try {
   	        		crDao.insertTrazabilidad(ruc, "BeanT1150", 2, modoConsulta);
	        		if (modoConsulta == 1) {
	        			logger.info("Obteniendo de BD Datos T1150");
	        			estAnexosT1150 = crDao.loadDatosT1150(ruc);
	        		}
    			} catch (Exception e) {
    				logger.error("ERROR al tratar de obtener Datos T1150 desde la BD", e);
    			}
        		return estAnexosT1150;
        	}

            try {
               	crDao.insertTrazabilidad(ruc, "BeanT1150", 1, modoConsulta);
               	DatosRucWS datosRucWSClient = WebServiceFactory.getSunatConsultaRUCClient();
    			Object [] list = datosRucWSClient.getEstAnexosT1150(ruc);
    			estAnexosT1150 = new BeanT1150 [list.length];
    			
    			for (int i=0; i < list.length; i++) {
    				Object obj = list[i];
    				estAnexosT1150[i] = (BeanT1150)obj;
    			}            	
    			if (estAnexosT1150 != null) {
    				logger.info("Insertando en BD Datos T362");
    				crDao.insertDatosT1150(ruc, estAnexosT1150);
    			}
    		} catch (Exception e) {
    			logger.error("ERROR al acceder al web service Consulta RUC 'getEstAnexosT1150'", e);
    		}    		
    	}
    	
    	if (!ConsultaRUCCWS.servicioDisponible()) {
    		return null;
    	}

		return estAnexosT1150;
    }
    
}
