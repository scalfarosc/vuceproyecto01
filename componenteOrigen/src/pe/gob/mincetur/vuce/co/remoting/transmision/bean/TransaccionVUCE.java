package pe.gob.mincetur.vuce.co.remoting.transmision.bean;

import java.util.List;

public class TransaccionVUCE extends TransmisionVUCE {
    
    private int idTransaccion;
    
    private String codigoFormato;
    
    private double monto;
    
    private List<String> notificacionesResueltas;
    
    public TransaccionVUCE() {
        super();
        this.setTemplate("mensajesTransmision/Transaccion.vm");
    }
    
    public int getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(int idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public String getCodigoFormato() {
        return codigoFormato;
    }

    public void setCodigoFormato(String codigoFormato) {
        this.codigoFormato = codigoFormato;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public List<String> getNotificacionesResueltas() {
        return notificacionesResueltas;
    }

    public void setNotificacionesResueltas(List<String> notificacionesResueltas) {
        this.notificacionesResueltas = notificacionesResueltas;
    }

}

