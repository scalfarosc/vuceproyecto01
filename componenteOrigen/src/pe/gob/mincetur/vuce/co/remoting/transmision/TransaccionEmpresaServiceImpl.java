package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.jlis.core.config.ApplicationConfig;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;

import pe.gob.mincetur.vuce.co.bean.ErroresDatos;
import pe.gob.mincetur.vuce.co.bean.ErroresEmpresa;
import pe.gob.mincetur.vuce.co.bean.ErroresEstructura;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionEmpresaVUCE;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;
import pe.gob.mincetur.vuce.co.util.TransaccionType;
import pe.gob.mincetur.vuce.co.util.file.ZipUtil;
import pe.gob.mincetur.vuce.co.util.stream.StreamUtil;
import pe.gob.mincetur.vuce.co.util.xml.XMLUtil;


public class TransaccionEmpresaServiceImpl implements TransaccionEmpresaService{
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);

	/**
	 * Valida la estructura de los adjuntos de la transaccion enviada por la Empresa
	 * @param xmlNotificacion
	 * @param ebXML 
	 * @param idNTX	  
	 * @return Retorna N80 / N81
	 * @throws Exception
	 */
	public HashMap<String, Object> validaAdjuntosTransaccionEmpresa(byte[] xmlNotificacion, byte[] ebXML, int idNTX) {

		NDC.push(Util.getDate());     
		logger.info("Entrando a TransaccionEmpresaLogicImpl.validaAdjuntosTransaccionEmpresa");
		
		Properties propiedades = new Properties();		
		//byte[] byteXmlRespuesta = null;	
		HashMap<String, Object> respuesta = new HashMap<String, Object>();	
		
		String keyError = "";

		try{
			//Configuramos el properties
			propiedades.load(getClass().getResourceAsStream("/message.properties")); 
			
			//Verificamos que haya enviado el XML de la transaccion, si es vacio se crea N81			
			if(xmlNotificacion==null){
				//genera ZIP respuesta N81 (XML+XMLErrores)
				keyError="vuce.empresa.transaccion.error.xml.no.enviado";				
				return generarNotificacionRespuestaError(idNTX, keyError);
				
			}			
			//Verificamos que haya enviado el EBXML de la transaccion, si es vacio se crea N81
			if(ebXML==null){
				//genera ZIP respuesta N81 (XML+XMLErrores)
				keyError="vuce.empresa.transaccion.error.ebxml.no.enviado";
				return generarNotificacionRespuestaError(idNTX, keyError);
			}
			
			//Valida la estructura y datos del XML de transaccion			
			respuesta = validaXMLTransaccion(xmlNotificacion, idNTX);
			if(respuesta!=null){				
				return respuesta;
			}			
			
			//Valida la estructura del EBXML de transaccion
			respuesta = validaEBXMLTransaccion(xmlNotificacion, ebXML, idNTX);
			if(respuesta!=null){				
				return respuesta;
			}

			
		} catch(Exception e){
			logger.error("Error de TransaccionEmpresaServiceImpl.procesarTransaccion"+e);			
		} finally{
            logger.info("Saliendo de TransaccionEmpresaServiceImpl.procesarTransaccion");
            NDC.pop();
            NDC.remove();
		}
		
		return respuesta;
	}
	
	/**
	 * Genera ZIP respuesta N81 (XML+XMLErrores)
	 * @param idNTX
	 * @param keyError
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> generarNotificacionRespuestaError(int idNTX, String keyError) throws Exception {
		
		HashMap<String, Object> respuesta = new HashMap<String, Object>();	
		Properties propiedades = new Properties();
		//Configuramos el properties
		propiedades.load(getClass().getResourceAsStream("/message.properties"));

		TransaccionEmpresaVUCE beanNotificacionResp = new TransaccionEmpresaVUCE();
		ErroresEmpresa beanErroresEmpresa = new ErroresEmpresa("mensajesTransmision/ErroresEmpresa.vm");
		
		ErroresEstructura beanErroresEstructura = new ErroresEstructura();

		//String nombreArchivoRespuestaErrorEmpresa = "Notificacion.xml";
		//String nombreArchivoErroresEmpresa = "ErroresEmpresa.xml";
		
		beanNotificacionResp.setTransaccion(TransaccionType.N81.getCodigo());
		beanNotificacionResp.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_NTX);
		beanNotificacionResp.setNumeroDocumento(Util.stringValueOf(idNTX));
		//respuesta.put(ConstantesCO.NOMBRE_NOTIFICACION, XMLUtil.generarMensajeRespuesta(beanNotificacionResp));				
		respuesta.put(ConstantesCO.NOMBRE_NOTIFICACION,beanNotificacionResp);
		
		beanErroresEstructura.setDescripcion(propiedades.getProperty(keyError));				
		beanErroresEmpresa.addEstructuras(beanErroresEstructura);
		
		//respuesta.put(ConstantesCO.NOMBRE_ERROR_EMPRESA, XMLUtil.generarMensajeErroresEmpresa(beanErroresEmpresa));
		respuesta.put(ConstantesCO.NOMBRE_ERROR_EMPRESA, beanErroresEmpresa);
    	
    	//return ZipUtil.compressFiles(respuestaToZip);
    	return respuesta;
	}
	
	/**
	 * Valida el XML de Transaccion
	 * @param xmlNotificacion
	 * @return ZIP respuesta N81 (XML+XMLErrores)
	 */
	public HashMap<String, Object> validaXMLTransaccion(byte[] xmlNotificacion, int idNTX) throws Exception{
		
		String keyError="";
		//Parseamos el xml de notificacion a objeto
		TransaccionEmpresaVUCE mensaje = (TransaccionEmpresaVUCE)XMLUtil.getMensajeTransmisionFromXML(TransaccionEmpresaVUCE.class, new ByteArrayInputStream(xmlNotificacion));
		if (mensaje==null){ //Si el el esquema es invalido o hubo algun problema en el parseo a objeto
			keyError="vuce.empresa.transaccion.error.xml.transaccion";	
			//Genera ZIP respuesta N81 (XML+XMLErrores)
			return generarNotificacionRespuestaError(idNTX, keyError);			
		} else{
			TransaccionType tipoTransaccion = TransaccionType.getTransaccionTypeByCodigo(mensaje.getTransaccion());
			if(tipoTransaccion==null){
				keyError="vuce.empresa.transaccion.error.transaccion.no.existe";				
				return generarNotificacionRespuestaError(idNTX, keyError);
			}
			mensaje.setIdTransaccion(tipoTransaccion.getId());
		} 
		logger.debug("El documento XML es valido, idNTX="+idNTX);
		return null;
	}
	
	public HashMap<String, Object> validaEBXMLTransaccion(byte[] xmlNotificacion, byte[] ebXML, int idNTX) throws Exception{
		String keyError="";		
		TransaccionEmpresaVUCE mensaje = (TransaccionEmpresaVUCE)XMLUtil.getMensajeTransmisionFromXML(TransaccionEmpresaVUCE.class, new ByteArrayInputStream(xmlNotificacion));
		String MCT00X = mensaje.getCodigoFormato().toLowerCase(); //MTC001-MCT005
		
		boolean esValido = true;
		
		
        if (XMLUtil.validarEsquema(StreamUtil.getBytesAsFile(ebXML), ApplicationConfig.getApplicationConfig().getApplicationResource("ruta.schemas.empresa", "/dj/"+MCT00X+".xsd").getFile())) {
            logger.debug("El documento es valido");
        } else {
        	esValido = false;
            logger.debug("El documento no es valido");
            //return itp011EbXml;
        }
		
		//Validamos que el formato no sea null
		if (esValido==false){
			keyError="vuce.empresa.transaccion.error.formato.ebxml.invalido";				
			return generarNotificacionRespuestaError(idNTX, keyError);
		}
		
		logger.debug("El documento EBXML es valido, idNTX="+idNTX);
		return null;
	}
	
	public TransaccionEmpresaVUCE getTransaccionEmpresaVUCEObjOfXmlNotificacion(byte[] xmlNotificacion) {
		TransaccionEmpresaVUCE mensaje = (TransaccionEmpresaVUCE)XMLUtil.getMensajeTransmisionFromXML(TransaccionEmpresaVUCE.class, new ByteArrayInputStream(xmlNotificacion));
		return mensaje; //MTC001-MCT005		
	}
	
	public byte[] generarNotificacionRespuesta(TransaccionEmpresaVUCE mensajeTransmision) throws Exception {
		
		return XMLUtil.generarMensajeRespuesta(mensajeTransmision);
	}	

	
	public byte[] generarMensajeErroresEmpresa(ErroresEmpresa mensajeTransmision) throws Exception {
		
		return XMLUtil.generarMensajeErroresEmpresa(mensajeTransmision);
	}
	
	
	public TransaccionEmpresaVUCE generarNotificacion(String numeroDocumento, Transmision transmision) throws Exception {
		TransaccionEmpresaVUCE mensajeTransmision = new TransaccionEmpresaVUCE();
		
		if(transmision.getTransaccion().equals(TransaccionType.N80.getCodigo())){
			double monto = 0.0;
            mensajeTransmision.setIdTransaccion(transmision.getId());//Debe ir otro codigo
            mensajeTransmision.setTransaccion(transmision.getTransaccion());
            mensajeTransmision.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_ORDEN);                    
            mensajeTransmision.setNumeroDocumento(numeroDocumento);                    
            mensajeTransmision.setMonto(monto);                    
            mensajeTransmision.setTipoDocumentoReferencia(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_NTX);                    
            mensajeTransmision.setNumeroDocumentoReferencia(transmision.getIdTransaccionNtx().toString());
			
		}
		
		
		if(transmision.getTransaccion().equals(TransaccionType.N81.getCodigo())){
			double monto = 0.0;
            mensajeTransmision.setIdTransaccion(transmision.getId());//Debe ir otro codigo
            mensajeTransmision.setTransaccion(transmision.getTransaccion());
            mensajeTransmision.setTipoDocumento(ConstantesCO.TIPO_DOCUMENTO_TRANSMISION_NTX);                    
            mensajeTransmision.setNumeroDocumento(transmision.getIdTransaccionNtx().toString());                    
            mensajeTransmision.setMonto(monto);                    
			
		}
		
		
		return mensajeTransmision;
		
	}	
	
	public ErroresEmpresa generarErroresNotificacion(String numeroDocumento, List<ErroresDatos> erroresDatos) throws Exception {
		ErroresEmpresa beanErroresEmpresa = new ErroresEmpresa();		
		beanErroresEmpresa.setDatos(erroresDatos);		
		return beanErroresEmpresa;		
	}
	
	public byte[] generarEBXML(SolicitudEBXML solicitudEbxml) throws Exception {
		
		HashUtil<String, Object> objectsDR = new HashUtil<String, Object>();
		objectsDR.put("formato", solicitudEbxml);
		AdjuntoFormato ebxml = XMLUtil.generateEBXML(solicitudEbxml, objectsDR);
		return ebxml.getArchivo();
	}	

	

}
