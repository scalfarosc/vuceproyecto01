package pe.gob.mincetur.vuce.co.remoting.rest.ws.entidad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.logic.InteroperabilidadFirmasLogic;
import pe.gob.mincetur.vuce.co.remoting.rest.ws.domain.JsonRequest;
import pe.gob.mincetur.vuce.co.remoting.rest.ws.domain.JsonResponse;
import pe.gob.mincetur.vuce.co.remoting.rest.ws.domain.JsonResponseError;
import pe.gob.mincetur.vuce.co.remoting.rest.ws.util.SpringApplicationContext;

@Path("/iop")
public class VuceOrigenRestWS {
	
	@POST
	@Path("/generarToken")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public JsonResponse generarToken(JsonRequest request) {
		InteroperabilidadFirmasLogic iopLogic = (InteroperabilidadFirmasLogic) SpringApplicationContext.getBean("interoperabilidadFirmasLogic");
		JsonResponse response = new JsonResponse();
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			InteroperabilidadFirma iop = new InteroperabilidadFirma();
			iop.setDrId(new Long((Integer)request.getData().get("drId")));
			iop.setSdr(new Integer((Integer)request.getData().get("sdr")));
			iop.setAcuerdoInternacionalId(26);
			UUID unique = UUID.randomUUID();
			String token = unique.toString();
			iop.setToken(token);
			iopLogic.registrarInteroperabilidad(iop);
			data.put("token", token);
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			JsonResponseError error = new JsonResponseError();
			error.setCode(e.getLocalizedMessage());
			error.setDescription(e.getMessage());
			List<JsonResponseError> errors = new ArrayList<JsonResponseError>();
			errors.add(error);
			response.setErrors(errors);
		}
		response.setData(data);
		return response;
	}

	@POST
	@Path("/obtenerToken")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public JsonResponse obtenerToken(JsonRequest request) {
		InteroperabilidadFirmasLogic iopLogic = (InteroperabilidadFirmasLogic) SpringApplicationContext.getBean("interoperabilidadFirmasLogic");
		JsonResponse response = new JsonResponse();
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			String token = (String) request.getData().get("token");
			data.put("token", token);
			Long drId = new Long((Integer)request.getData().get("drId"));
			Integer sdr = new Integer((Integer)request.getData().get("sdr"));
			InteroperabilidadFirma iop = iopLogic.obtenerToken(drId, sdr);
			data.put("token", iop.getToken());
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			JsonResponseError error = new JsonResponseError();
			error.setCode(e.getLocalizedMessage());
			error.setDescription(e.getMessage());
			List<JsonResponseError> errors = new ArrayList<JsonResponseError>();
			errors.add(error);
			response.setErrors(errors);
		}
		response.setData(data);
		return response;
	}
	/*
	@GET
	@Path("/actualizarPrimeraFirma")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public JsonResponse actualizarPrimeraFirma(
			@QueryParam("drId") long drId, 
			@QueryParam("sdr") int sdr, 
			@QueryParam("nombreFirmanteExp") String exportador, 
			@QueryParam("formato") String formato, 
			@QueryParam("posteriori") String posteriori) {
		InteroperabilidadFirmasLogic iopLogic = (InteroperabilidadFirmasLogic) SpringApplicationContext.getBean("interoperabilidadFirmasLogic");
		JsonResponse response = new JsonResponse();
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			iopLogic.actualizaFirmaExportador(drId, sdr, exportador, formato, posteriori);
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			JsonResponseError error = new JsonResponseError();
			error.setCode(e.getLocalizedMessage());
			error.setDescription(e.getMessage());
			List<JsonResponseError> errors = new ArrayList<JsonResponseError>();
			errors.add(error);
			response.setErrors(errors);
		}
		response.setData(data);
		return response;
	}
	*/
}