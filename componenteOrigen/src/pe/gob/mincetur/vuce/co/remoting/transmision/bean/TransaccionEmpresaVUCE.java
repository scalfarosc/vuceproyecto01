package pe.gob.mincetur.vuce.co.remoting.transmision.bean;

public class TransaccionEmpresaVUCE extends TransmisionVUCE {

    private int idTransaccion;
        
    private String codigoFormato;
    
    private String rucUsuario;
    
    private String codigoUsuario;
    
    private double monto;    
    
    public TransaccionEmpresaVUCE() {
        super();
        this.setTemplate("mensajesTransmision/Notificacion.vm");
    }


	public int getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(int idTransaccion) {
		this.idTransaccion = idTransaccion;
	}


	public String getRucUsuario() {
		return rucUsuario;
	}

	public void setRucUsuario(String rucUsuario) {
		this.rucUsuario = rucUsuario;
	}

	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public String getCodigoFormato() {
		return codigoFormato;
	}

	public void setCodigoFormato(String codigoFormato) {
		this.codigoFormato = codigoFormato;
	}


	public double getMonto() {
		return monto;
	}


	public void setMonto(double monto) {
		this.monto = monto;
	}
	
	
    
    /*private String tupa;
    
    private List<String> notificacionesResueltas;
    */
    
    
    
    
    
}
