package pe.gob.mincetur.vuce.co.remoting.transmision;

import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionIOPVUCE;

public interface TransaccionPackService {
	
	public byte[] generarMensajePack(TransaccionIOPVUCE mensajeTransmision) throws Exception;

}
