package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.HashMap;

import pe.gob.mincetur.vuce.co.bean.ErroresEmpresa;
import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.domain.ebxml.SolicitudEBXML;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionEmpresaVUCE;

public interface TransaccionEmpresaService {
	public HashMap<String, Object> validaAdjuntosTransaccionEmpresa(			
			byte[] xmlNotificacion, 
			byte[] ebXML, 
			int idNTX); 
	
	public HashMap<String, Object> generarNotificacionRespuestaError(int idNTX, String keyError) throws Exception;	
	
	public TransaccionEmpresaVUCE getTransaccionEmpresaVUCEObjOfXmlNotificacion(byte[] xmlNotificacion);
	
	public byte[] generarNotificacionRespuesta(TransaccionEmpresaVUCE mensajeTransmision) throws Exception;
	
	public TransaccionEmpresaVUCE generarNotificacion(String numeroDocumento, Transmision transmision) throws Exception;
	
	public byte[] generarMensajeErroresEmpresa(ErroresEmpresa mensajeTransmision) throws Exception;
	
	public byte[] generarEBXML(SolicitudEBXML ebxml) throws Exception;
}
