package pe.gob.mincetur.vuce.co.remoting.transmision.bean;

import java.util.List;

//JMC 02/12/2016 Alianza Pacifico
public class TransaccionIOPVUCE extends TransaccionVUCE {
    
	private Integer tceId;
	
	private Integer drIOPId;
	
    private String codigoAcuerdo;
    
    private String tipoDocumentoAcuerdo;
    
    private String codigoPaisOrigen;
    
    private String codigoPaisDestino;
    
    private String fechaGeneracion;
    
    private String numeroCertificado;
    
    private Integer tceAlianzaId;
    
    //private List<RespuestaIOP> respuestas;
    
    public TransaccionIOPVUCE() {
        super();
        this.setTemplate("mensajesTransmision/TransaccionAcuerdo.vm");
    }

	public Integer getTceId() {
		return tceId;
	}

	public void setTceId(Integer tceId) {
		this.tceId = tceId;
	}

	public Integer getDrIOPId() {
		return drIOPId;
	}

	public void setDrIOPId(Integer drIOPId) {
		this.drIOPId = drIOPId;
	}

	public String getCodigoAcuerdo() {
		return codigoAcuerdo;
	}

	public void setCodigoAcuerdo(String codigoAcuerdo) {
		this.codigoAcuerdo = codigoAcuerdo;
	}

	public String getTipoDocumentoAcuerdo() {
		return tipoDocumentoAcuerdo;
	}

	public void setTipoDocumentoAcuerdo(String tipoDocumentoAcuerdo) {
		this.tipoDocumentoAcuerdo = tipoDocumentoAcuerdo;
	}

	public String getCodigoPaisOrigen() {
		return codigoPaisOrigen;
	}

	public void setCodigoPaisOrigen(String codigoPaisOrigen) {
		this.codigoPaisOrigen = codigoPaisOrigen;
	}

	public String getCodigoPaisDestino() {
		return codigoPaisDestino;
	}

	public void setCodigoPaisDestino(String codigoPaisDestino) {
		this.codigoPaisDestino = codigoPaisDestino;
	}

	public String getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(String fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public String getNumeroCertificado() {
		return numeroCertificado;
	}

	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}

/*	public List<RespuestaIOP> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<RespuestaIOP> respuestas) {
		this.respuestas = respuestas;
	}
*/
	public Integer getTceAlianzaId() {
		return tceAlianzaId;
	}

	public void setTceAlianzaId(Integer tceAlianzaId) {
		this.tceAlianzaId = tceAlianzaId;
	}
    
}
