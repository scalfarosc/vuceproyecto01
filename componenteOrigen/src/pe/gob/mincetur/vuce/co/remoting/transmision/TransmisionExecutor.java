package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import pe.gob.mincetur.vuce.co.domain.Transmision;

public interface TransmisionExecutor {

	
	public void setInicioEjecucion(boolean inicioEjecucion);
	
	public boolean isInicioEjecucion();
	
	public Stack<Object> getStack();
	
    public void ejecutarTransmisiones(HashMap<Integer, List<Transmision>> transmisiones, String tipoProceso);
    
    //public void ejecutarEnvioLiquidacionPagos(HashUtil<String, ArchivoLiquidacion> files);
}
