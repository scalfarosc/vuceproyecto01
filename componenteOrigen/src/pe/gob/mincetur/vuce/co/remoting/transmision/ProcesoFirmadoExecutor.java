package pe.gob.mincetur.vuce.co.remoting.transmision;

public interface ProcesoFirmadoExecutor {
	
	public void procesarGeneracionPDF(String token);

}
