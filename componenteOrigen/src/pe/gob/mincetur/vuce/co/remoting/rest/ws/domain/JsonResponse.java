package pe.gob.mincetur.vuce.co.remoting.rest.ws.domain;

import java.util.List;
import java.util.Map;

public class JsonResponse {
	
	private Boolean success;
	private Map<String, Object> data;
	private List<JsonResponseError> errors;
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	public List<JsonResponseError> getErrors() {
		return errors;
	}
	public void setErrors(List<JsonResponseError> errors) {
		this.errors = errors;
	}
	
}
