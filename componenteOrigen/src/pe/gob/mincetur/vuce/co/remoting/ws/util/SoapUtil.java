package pe.gob.mincetur.vuce.co.remoting.ws.util;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.w3c.dom.Node;

public class SoapUtil {
	
	static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
    
	public static Object getObjectFromHeader(SoapMessage message, QName qname, Class<?> clazz) {
		Object obj = null;
		Header h = message.getHeader(qname);
		if (h!=null && h.getObject() instanceof Node) {
			try {
				obj = JAXBContext.newInstance(new Class[] { clazz }).createUnmarshaller().unmarshal((Node)h.getObject());
			} catch (JAXBException e) {
				logger.error("Ocurrio un error al intentar obtener el objeto ("+clazz+") del header del mensaje SOAP.", e);
			}
		}
		return obj;
	}
	
    public static String getIpCaller(WebServiceContext wsContext) {
        HttpServletRequest req = (HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        return req.getRemoteAddr();
    }
    
    public static String getIpCaller(SOAPMessageContext soapMessageContext) {
        HttpServletRequest req = (HttpServletRequest)soapMessageContext.get(MessageContext.SERVLET_REQUEST);
        return req.getRemoteAddr();
    }
    
}
