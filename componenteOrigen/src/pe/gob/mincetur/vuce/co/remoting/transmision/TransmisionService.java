package pe.gob.mincetur.vuce.co.remoting.transmision;

import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Transmision;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionEmpresaVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransaccionIOPVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.bean.TransmisionVUCE;
import pe.gob.mincetur.vuce.co.remoting.transmision.exception.TransmisionException;

public interface TransmisionService {
	
    public List<Transmision> loadList(HashUtil<String, Integer> filter) throws Exception;
    
    public void procesarTransmisionEmpresa(Transmision transmision);    
    
    public void encolarMensajeEmpresa(Transmision transmision, TransaccionEmpresaVUCE mensajeTransmision, byte [] solicitudEBXML, byte [] Adjuntos) throws TransmisionException;

    //JMC 02/12/2016  Alianza Pacifico
    public Transmision registrarTransmisionIOPSalienteParaPack(int idEntidad, TransmisionVUCE transmisionVUCE) throws Exception;
    
    public Transmision loadTransmision(int idVC) throws Exception;
    
    public void updateEstadoTransmision(Transmision transmision) throws Exception;
    
    public void encolarMensajePack(String token, Integer secuenciaMaxima, Transmision transmision, TransaccionIOPVUCE mensajeTransmision, byte [] solicitudEBXML, byte [] Adjuntos) throws TransmisionException;
    
    public List<Transmision> loadListIOP(HashUtil<String, Integer> filter) throws Exception;    
    
    public Transmision procesarTransmisionIOPEntranteDePack(Transmision transmision) throws Exception;
    
    public void encolarMensajeRespuestaPack(Transmision transmision, TransaccionIOPVUCE mensajeTransmision, byte [] solicitudEBXML, byte [] Adjuntos) throws TransmisionException;
 }
