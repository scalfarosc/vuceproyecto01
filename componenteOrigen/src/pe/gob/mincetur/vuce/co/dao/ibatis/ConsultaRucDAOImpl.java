package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanRso;
import ConsultaRuc.BeanSpr;
import ConsultaRuc.BeanT1144;
import ConsultaRuc.BeanT1150;
import ConsultaRuc.BeanT362;
import pe.gob.mincetur.vuce.co.dao.ConsultaRucDAO;

/**
 * DAO para las operaciones de Contingencia de Consulta RUC
 * @author Sapo
 *
 */
public class ConsultaRucDAOImpl extends SqlMapClientDaoSupport implements ConsultaRucDAO {
    
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_WS);
	
	@Override
	public BeanDdp loadDatosPrincipales(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        return (BeanDdp) getSqlMapClientTemplate().queryForObject("datosPrincipales.element", param);
	}

	@Override
	public void insertDatosPrincipales(String ruc, BeanDdp datosPrincipales) throws Exception {
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		param.put("ruc", ruc);
		param.put("codDep", datosPrincipales.getCod_dep());
		param.put("codDist", datosPrincipales.getCod_dist());
		param.put("codProv", datosPrincipales.getCod_prov());
		param.put("ddpCiiu", datosPrincipales.getDdp_ciiu());
		param.put("ddpDoble", datosPrincipales.getDdp_doble());
		param.put("ddpEstado", datosPrincipales.getDdp_estado());
		param.put("ddpFecact", datosPrincipales.getDdp_fecact());
		param.put("ddpFecalt", datosPrincipales.getDdp_fecalt());
        param.put("ddpFecbaj", datosPrincipales.getDdp_fecbaj());
        param.put("ddpFlag22", datosPrincipales.getDdp_flag22());
        param.put("ddpIdenti", datosPrincipales.getDdp_identi());
        param.put("ddpInter1", datosPrincipales.getDdp_inter1());
        param.put("ddpLllttt", datosPrincipales.getDdp_lllttt());
        param.put("ddpMclase", datosPrincipales.getDdp_mclase());
        param.put("ddpNombre", datosPrincipales.getDdp_nombre());
        param.put("ddpNomvia", datosPrincipales.getDdp_nomvia());
        param.put("ddpNomzon", datosPrincipales.getDdp_nomzon());
        param.put("ddpNumer1", datosPrincipales.getDdp_numer1());
        param.put("ddpNumreg", datosPrincipales.getDdp_numreg());
        param.put("ddpNumruc", datosPrincipales.getDdp_numruc());
        param.put("ddpReacti", datosPrincipales.getDdp_reacti());
        param.put("ddpRefer1", datosPrincipales.getDdp_refer1());
        param.put("ddpSecuen", datosPrincipales.getDdp_secuen());
        param.put("ddpTamano", datosPrincipales.getDdp_tamano());
        param.put("ddpTipvia", datosPrincipales.getDdp_tipvia());
        param.put("ddpTipzon", datosPrincipales.getDdp_tipzon());
        param.put("ddpTpoemp", datosPrincipales.getDdp_tpoemp());
        param.put("ddpUbigeo", datosPrincipales.getDdp_ubigeo());
        param.put("ddpUserna", datosPrincipales.getDdp_userna());
        param.put("descCiiu", datosPrincipales.getDesc_ciiu());
        param.put("descDep", datosPrincipales.getDesc_dep());
        param.put("descDist", datosPrincipales.getDesc_dist());
        param.put("descEstado", datosPrincipales.getDesc_estado());
        param.put("descFlag22", datosPrincipales.getDesc_flag22());
        param.put("descIdenti", datosPrincipales.getDesc_identi());
        param.put("descNumreg", datosPrincipales.getDesc_numreg());
        param.put("descProv", datosPrincipales.getDesc_prov());
        param.put("descTamano", datosPrincipales.getDesc_tamano());
        param.put("descTipvia", datosPrincipales.getDesc_tipvia());
        param.put("descTipzon", datosPrincipales.getDesc_tipzon());
        param.put("descTpoemp", datosPrincipales.getDesc_tpoemp());
        param.put("esactivo", datosPrincipales.isEsActivo());
        param.put("eshabido", datosPrincipales.isEsHabido());		
        getSqlMapClientTemplate().queryForObject("datosPrincipales.insert", param);
        return;
	}

	@Override
	public BeanDds loadDatosSecundarios(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        return (BeanDds) getSqlMapClientTemplate().queryForObject("datosSecundarios.element", param);
	}

	@Override
	public void insertDatosSecundarios(String ruc, BeanDds datosSecundarios) throws Exception {
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		param.put("ruc", ruc);
		param.put("ddsAparta", datosSecundarios.getDds_aparta());
		param.put("ddsAsient", datosSecundarios.getDds_asient());
		param.put("ddsCalifi", datosSecundarios.getDds_califi());
		param.put("ddsCentro", datosSecundarios.getDds_centro());
		param.put("ddsCierre", datosSecundarios.getDds_cierre());
		param.put("ddsComext", datosSecundarios.getDds_comext());
		param.put("ddsConsti", datosSecundarios.getDds_consti());
		param.put("ddsContab", datosSecundarios.getDds_contab());
        param.put("ddsDocide", datosSecundarios.getDds_docide());
        param.put("ddsDomici", datosSecundarios.getDds_domici());
        param.put("ddsEjempl", datosSecundarios.getDds_ejempl());
        param.put("ddsFactur", datosSecundarios.getDds_factur());
        param.put("ddsFecact", datosSecundarios.getDds_fecact());
        param.put("ddsFecnac", datosSecundarios.getDds_fecnac());
        param.put("ddsFecven", datosSecundarios.getDds_fecven());
        param.put("ddsFicha", datosSecundarios.getDds_ficha());
        param.put("ddsInicio", datosSecundarios.getDds_inicio());
        param.put("ddsLicenc", datosSecundarios.getDds_licenc());
        param.put("ddsMotbaj", datosSecundarios.getDds_motbaj());
        param.put("ddsMotemi", datosSecundarios.getDds_motemi());
        param.put("ddsNacion", datosSecundarios.getDds_nacion());
        param.put("ddsNfolio", datosSecundarios.getDds_nfolio());
        param.put("ddsNomcom", datosSecundarios.getDds_nomcom());
        param.put("ddsNrodoc", datosSecundarios.getDds_nrodoc());
        param.put("ddsNumfax", datosSecundarios.getDds_numfax());
        param.put("ddsNumruc", datosSecundarios.getDds_numruc());
        param.put("ddsOrient", datosSecundarios.getDds_orient());
        param.put("ddsPaispa", datosSecundarios.getDds_paispa());
        param.put("ddsPasapo", datosSecundarios.getDds_pasapo());
        param.put("ddsPatron", datosSecundarios.getDds_patron());
        param.put("ddsSexo", datosSecundarios.getDds_sexo());
        param.put("ddsTelef1", datosSecundarios.getDds_telef1());
        param.put("ddsTelef2", datosSecundarios.getDds_telef2());
        param.put("ddsTelef3", datosSecundarios.getDds_telef3());
        param.put("ddsUserna", datosSecundarios.getDds_userna());
        param.put("declara", datosSecundarios.getDeclara());
        param.put("descCierre", datosSecundarios.getDesc_cierre());
        param.put("descComext", datosSecundarios.getDesc_comext());
        param.put("descContab", datosSecundarios.getDesc_contab());
        param.put("descDocide", datosSecundarios.getDesc_docide());
        param.put("descDomici", datosSecundarios.getDesc_domici());
        param.put("descFactur", datosSecundarios.getDesc_factur());
        param.put("descMotbaj", datosSecundarios.getDesc_motbaj());
        param.put("descNacion", datosSecundarios.getDesc_nacion());
        param.put("descOrient", datosSecundarios.getDesc_orient());
        param.put("descSexo", datosSecundarios.getDesc_sexo());
        getSqlMapClientTemplate().queryForObject("datosSecundarios.insert", param);
        return;
	}
	

	@Override
	public BeanT1144 loadDatosT1144(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        return (BeanT1144) getSqlMapClientTemplate().queryForObject("datosT1144.element", param);
	}


	
	@Override
	public void insertDatosT1144(String ruc, BeanT1144 datosT1144) throws Exception {
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		param.put("ruc", ruc);
		param.put("codCiiu2", datosT1144.getCod_ciiu2());
		param.put("codCiiu3", datosT1144.getCod_ciiu3());
		param.put("codCorreo1", datosT1144.getCod_correo1());
		param.put("codCorreo2", datosT1144.getCod_correo2());
		param.put("codDepar1", datosT1144.getCod_depar1());
		param.put("codDepar2", datosT1144.getCod_depar2());
		param.put("codDepar3", datosT1144.getCod_depar3());
		param.put("codDepar4", datosT1144.getCod_depar4());
        param.put("codDepar5", datosT1144.getCod_depar5());
        param.put("codPaicap", datosT1144.getCod_paicap());
        param.put("codPaiori", datosT1144.getCod_paiori());
        param.put("codUserna", datosT1144.getCod_userna());
        param.put("desAsiento", datosT1144.getDes_asiento());
        param.put("desCiiu2", datosT1144.getDes_ciiu2());
        param.put("desCiiu3", datosT1144.getDes_ciiu3());
        param.put("desConleg", datosT1144.getDes_conleg());
        param.put("desDepar1", datosT1144.getDes_depar1());
        param.put("desDepar2", datosT1144.getDes_depar2());
        param.put("desDepar3", datosT1144.getDes_depar3());
        param.put("desDepar4", datosT1144.getDes_depar4());
        param.put("desDepar5", datosT1144.getDes_depar5());
        param.put("desParreg", datosT1144.getDes_parreg());
        param.put("desProind", datosT1144.getDes_proind());
        param.put("desRefnot", datosT1144.getDes_refnot());
        param.put("fecAct", datosT1144.getFec_act());
        param.put("fecConfir1", datosT1144.getFec_confir1());
        param.put("fecConfir2", datosT1144.getFec_confir2());
        param.put("indConleg", datosT1144.getInd_conleg());
        param.put("indCorreo1", datosT1144.getInd_correo1());
        param.put("indCorreo2", datosT1144.getInd_correo2());
        param.put("indNotifi", datosT1144.getInd_notifi());
        param.put("indProind", datosT1144.getInd_proind());
        param.put("numDepar", datosT1144.getNum_depar());
        param.put("numFax", datosT1144.getNum_fax());
        param.put("numKilom", datosT1144.getNum_kilom());
        param.put("numLote", datosT1144.getNum_lote());
        param.put("numManza", datosT1144.getNum_manza());
        param.put("numRuc", datosT1144.getNum_ruc());
        param.put("numTelef1", datosT1144.getNum_telef1());
        param.put("numTelef2", datosT1144.getNum_telef2());
        param.put("numTelef3", datosT1144.getNum_telef3());
        param.put("numTelef4", datosT1144.getNum_telef4()); 
        getSqlMapClientTemplate().queryForObject("datosT1144.insert", param);
        return;
	}
	

	
	@Override
	public Integer loadModoConsulta() throws Exception {
		Integer result = null;
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		getSqlMapClientTemplate().queryForObject("modoConsulta.element", param);
		result = param.getInt("modoConsulta");
        return result;
	}	
	
	@Override
	public void insertTrazabilidad(String ruc, String metodo, Integer resultadoConsulta, Integer modoOperacion) throws Exception {
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		param.put("ruc", ruc);
		param.put("metodo", metodo);
		param.put("resultadoConsulta", resultadoConsulta==null?resultadoConsulta:String.valueOf(resultadoConsulta));
		param.put("modoConsulta", String.valueOf(modoOperacion));
        getSqlMapClientTemplate().queryForObject("trazabilidad.insert", param);
        return;
	}

	@Override
	public BeanT362[] loadDatosT362(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        List<BeanT362> lista = getSqlMapClientTemplate().queryForList("datosT362.element", param);
        BeanT362[] result = new BeanT362[lista.size()];
        result = lista.toArray(result);
        return result; 
	}
	
	@Override
	public void insertDatosT362(String ruc, BeanT362[] datosT362) throws Exception {
		HashUtil<String, Object> param = null;
		for (BeanT362 bean : datosT362) {
			param = new HashUtil<String, Object>();
			param.put("ruc", ruc);
			param.put("descNumreg", bean.getDesc_numreg());
			param.put("t362Fecact", bean.getT362_fecact());
			param.put("t362Fecbaj", bean.getT362_fecbaj());
			param.put("t362Indice", bean.getT362_indice());
			param.put("t362Nombre", bean.getT362_nombre());
			param.put("t362Numreg", bean.getT362_numreg());
			param.put("t362Numruc", bean.getT362_numruc());
	        getSqlMapClientTemplate().queryForObject("datosT362.insert", param);			
		}
        return;
	}

	@Override
	public BeanRso[] loadRepLegales(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        List<BeanRso> lista = getSqlMapClientTemplate().queryForList("repLegales.element", param);
        BeanRso[] result = new BeanRso[lista.size()];
        result = lista.toArray(result);
        return result; 
	}

	@Override
	public void insertRepLegales(String ruc, BeanRso[] repLegales) throws Exception {
		HashUtil<String, Object> param = null;
		for (BeanRso bean : repLegales) {
			param = new HashUtil<String, Object>();
			param.put("ruc", ruc);
			param.put("codCargo", bean.getCod_cargo());
			param.put("codDepar", bean.getCod_depar());
			param.put("descDocide", bean.getDesc_docide());
			param.put("numOrdSuce", bean.getNum_ord_suce());
			param.put("rsoCargoo", bean.getRso_cargoo());
			param.put("rsoDocide", bean.getRso_docide());
			param.put("rsoFecact", bean.getRso_fecact());
			param.put("rsoFecnac", bean.getRso_fecnac());
			param.put("rsoNombre", bean.getRso_nombre());
			param.put("rsoNrodoc", bean.getRso_nrodoc());
			param.put("rsoNumruc", bean.getRso_numruc());
			param.put("rsoUserna", bean.getRso_userna());
			param.put("rsoVdesde", bean.getRso_vdesde());
	        getSqlMapClientTemplate().queryForObject("repLegales.insert", param);			
		}
        return;
	}

	@Override
	public String loadDomicilioLegal(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        String result = (String) getSqlMapClientTemplate().queryForObject("domicilio.element", param);
        return result; 
	}

	@Override
	public void insertDomicilioLegal(String ruc, String domicilioLegal) throws Exception {
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		param.put("ruc", ruc);
		param.put("domicilio", domicilioLegal);
        getSqlMapClientTemplate().queryForObject("domicilio.insert", param);
        return;
	}

	@Override
	public BeanSpr[] loadEstAnexos(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        List<BeanSpr> lista = getSqlMapClientTemplate().queryForList("anexos.element", param);
        BeanSpr[] result = new BeanSpr[lista.size()];
        result = lista.toArray(result);
        return result; 
	}

	
	@Override
	public void insertEstAnexos(String ruc, BeanSpr[] estAnexos) throws Exception {
		HashUtil<String, Object> param = null;
		for (BeanSpr bean : estAnexos) {
			param = new HashUtil<String, Object>();
			param.put("ruc", ruc);
			param.put("codDep", bean.getCod_dep());
			param.put("codDist", bean.getCod_dist());
			param.put("codProv", bean.getCod_prov());
			param.put("descDep", bean.getDesc_dep());
			param.put("descDist", bean.getDesc_dist());
			param.put("descProv", bean.getDesc_prov());
			param.put("descTipest", bean.getDesc_tipest());
			param.put("descTipvia", bean.getDesc_tipvia());
			param.put("descTipzon", bean.getDesc_tipzon());
			param.put("direccion", bean.getDireccion());
			param.put("sprCorrel", bean.getSpr_correl());
			param.put("sprFecact", bean.getSpr_fecact());
			param.put("sprInter1", bean.getSpr_inter1());
			param.put("sprLicenc", bean.getSpr_licenc());			
			param.put("sprNombre", bean.getSpr_nombre());
			param.put("sprNomvia", bean.getSpr_nomvia());			
			param.put("sprNomzon", bean.getSpr_nomzon());
			param.put("sprNumer1", bean.getSpr_numer1());			
			param.put("sprNumruc", bean.getSpr_numruc());
			param.put("sprRefer1", bean.getSpr_refer1());			
			param.put("sprTipest", bean.getSpr_tipest());
			param.put("sprTipvia", bean.getSpr_tipvia());			
			param.put("sprTipzon", bean.getSpr_tipzon());			
			param.put("sprUbigeo", bean.getSpr_ubigeo());			
			param.put("sprUserna", bean.getSpr_userna());			
	        getSqlMapClientTemplate().queryForObject("anexos.insert", param);			
		}
        return;
	}

	@Override
	public BeanT1150[] loadDatosT1150(String ruc) throws Exception {
        HashUtil<String, Object> param = new HashUtil<String, Object>();
        param.put("ruc", ruc);
        List<BeanT1150> lista = getSqlMapClientTemplate().queryForList("anexosT1150.element", param);
        BeanT1150[] result = new BeanT1150[lista.size()];
        result = lista.toArray(result);
        return result; 
	}

	@Override
	public void insertDatosT1150(String ruc, BeanT1150[] datosT1150) throws Exception {
		HashUtil<String, Object> param = null;
		for (BeanT1150 bean : datosT1150) {
			param = new HashUtil<String, Object>();
			param.put("ruc", ruc);
			param.put("codUserna", bean.getCod_userna());
			param.put("fecAct", bean.getFec_act());
			param.put("indConleg", bean.getInd_conleg());
			param.put("numCorrel", bean.getNum_correl());
			param.put("numDepar", bean.getNum_depar());
			param.put("numKilom", bean.getNum_kilom());
			param.put("numLote", bean.getNum_lote());
			param.put("numManza", bean.getNum_manza());
			param.put("numRuc", bean.getNum_ruc());			
	        getSqlMapClientTemplate().queryForObject("anexosT1150.insert", param);			
		}
        return;
	}
	
}
