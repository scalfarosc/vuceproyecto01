package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.TransmisionDAO;
import pe.gob.mincetur.vuce.co.domain.Transmision;

public class TransmisionDAOImpl extends SqlMapClientDaoSupport implements TransmisionDAO {
	
	private static Logger logger = Logger.getLogger(Constantes.LOGGER_DB);
	
    public List<Transmision> loadList(HashUtil<String, Integer> filter) {
        return getSqlMapClientTemplate().queryForList("transmision.list", filter);
    }
    
    public String procesarTransaccionDeclaracionJuradaEmpresa(HashUtil<String, Integer> filter){        
        getSqlMapClientTemplate().queryForObject("declaracionJurada.transmision.procesaDatosTransmisionEmpresa", filter);
        return filter.getString("indValidacion");
    }
    
    public String procesarTransaccionEmpresa(HashUtil<String, Integer> filter){        
        getSqlMapClientTemplate().queryForObject("certificadoOrigen.transmision.procesaDatosTransmisionEmpresa", filter);
        return filter.getString("indValidacion");
    }

    public int registrarTransmision(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("transmision.registrarTransmision", filter);
        return filter.getInt("idVC");
    }
    
    public Transmision loadTransmision(int idVC) {
        HashMap<String, Integer> filter = new HashMap<String, Integer>();
        filter.put("idVC", idVC);
        return (Transmision)getSqlMapClientTemplate().queryForObject("transmision.element", filter);
    }
    
    public void updateTransmision(Transmision transmision) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idVC", transmision.getId());
        filter.put("idVE", transmision.getIdVE());
        filter.put("estadoVC", transmision.getEstadoVC());
        filter.put("estadoVE", transmision.getEstadoVE());
        filter.put("idTransaccion", transmision.getIdTransaccion());
        getSqlMapClientTemplate().update("transmision.actualizarTransmision", filter);
    }    
    public void updateEstadoTransmision(Transmision transmision) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idVC", transmision.getId());
        filter.put("estadoVC", transmision.getEstadoVC());
        getSqlMapClientTemplate().queryForObject("transmision.actualizarEstado", filter);
    }
    
    public void updateEstadoTransmisionEmpresa(Transmision transmision) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("id", transmision.getId());
        filter.put("estadoVC", transmision.getEstadoVC());
        getSqlMapClientTemplate().queryForObject("transmision.empresa.actualizarEstado", filter);
    }

    //JMC 02/12/2016 ALIANZA PACIFICO
    public int registrarTransmisionIOP(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("transmision.registrarTransmisionIOP", filter);
        return filter.getInt("idVC");
    }


    public List<Transmision> loadListIOP(HashUtil<String, Integer> filter){
    	return getSqlMapClientTemplate().queryForList("transmision.listIOP", filter);
    }
}
