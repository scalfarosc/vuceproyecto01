package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.FormatoDAO;
import pe.gob.mincetur.vuce.co.domain.RepresentanteLegal;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoRequeridoCertificado;
import pe.gob.mincetur.vuce.co.domain.Formato;
import pe.gob.mincetur.vuce.co.domain.AdjuntoRequerido;

public class FormatoDAOImpl extends SqlMapClientDaoSupport implements FormatoDAO {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_DB);

    public HashMap<String, String> loadUbigeo(HashUtil<String, Object> filter) throws Exception {
        HashMap<String, String> element = new HashMap<String, String>();
        element = (HashMap<String, String>) getSqlMapClientTemplate().queryForObject("comun.ubigeo.element", filter);
        return element;
    }

    public HashMap<String, String> loadUbigeoByDistrito(HashUtil<String, Object> filter) throws Exception {
        HashMap<String, String> element = new HashMap<String, String>();
        element = (HashMap<String, String>) getSqlMapClientTemplate().queryForObject("comun.ubigeo.distrito.element", filter);
        return element;
    }

    public Solicitante loadSolicitanteConEmpresa(HashUtil<String, Object> filter) throws Exception {
        return (Solicitante) getSqlMapClientTemplate().queryForObject("comun.solicitante_con_empresa.element", filter);
    }

    public Solicitante loadSolicitanteSinEmpresa(HashUtil<String, Object> filter) throws Exception {
        return (Solicitante) getSqlMapClientTemplate().queryForObject("comun.solicitante_sin_empresa.element", filter);
    }

    public AdjuntoRequeridoCertificado loadRequerido(HashUtil<String, Object> filter) throws Exception {
        return (AdjuntoRequeridoCertificado) getSqlMapClientTemplate().queryForObject("formato.adjunto_requerido.element", filter);
    }

    public String loadPartida(HashUtil<String, Object> filter) {
        return (String) getSqlMapClientTemplate().queryForObject("formato.partida.element", filter);
    }

    public int cuentaAdjuntosRequeridos(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjuntos.requeridos.count", filter).toString());
    }

    public int cuentaAdjuntosRequeridosxTupa(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjuntos.requeridos_x_tupa.count", filter).toString());
    }

    public int cuentaAdjuntos(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjuntos.count", filter).toString());
    }

    public String loadTipoDoc(HashUtil<String, Object> filter) throws Exception {
        return (String) getSqlMapClientTemplate().queryForObject("comun.tipo_documento.element", filter);
    }


    public Solicitante loadSolicitanteFormato(HashUtil<String, Object> filter) throws Exception {
    	Solicitante solicitante = (Solicitante) getSqlMapClientTemplate().queryForObject("formato.orden.solicitante.element", filter);
    	if (solicitante.getTipoEmpresaExterna() != null) {
    		String tipoEmpresaExterna = solicitante.getTipoEmpresaExterna();
    		solicitante.setAgente(tipoEmpresaExterna.equalsIgnoreCase("1"));
    		solicitante.setLaboratorio(tipoEmpresaExterna.equalsIgnoreCase("2"));
    	}
        return solicitante;
    }

    public Solicitante loadUsuarioFormato(HashUtil<String, Object> filter) throws Exception {
    	Solicitante solicitante = (Solicitante) getSqlMapClientTemplate().queryForObject("formato.orden.usuario.element", filter);
    	if (solicitante.getTipoEmpresaExterna() != null) {
    		String tipoEmpresaExterna = solicitante.getTipoEmpresaExterna();
    		solicitante.setAgente(tipoEmpresaExterna.equalsIgnoreCase("1"));
    		solicitante.setLaboratorio(tipoEmpresaExterna.equalsIgnoreCase("2"));
    	} else {
    		solicitante.setAgente(false);
    		solicitante.setLaboratorio(false);
    	}
    	return solicitante;
    }

    public RepresentanteLegal loadRepresentanteFormato(HashUtil<String, Object> filter) throws Exception {
        return (RepresentanteLegal) getSqlMapClientTemplate().queryForObject("formato.orden.representante.element", filter);
    }

    public Solicitante loadEmpresaExternaFormato(HashUtil<String, Object> filter) throws Exception {
        return (Solicitante) getSqlMapClientTemplate().queryForObject("formato.orden.empresa_externa.element", filter);
    }

    public Solicitante loadEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
        return (Solicitante) getSqlMapClientTemplate().queryForObject("comun.empresa_externa.element", filter);
    }

    public void deleteProducto(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("formato.orden.producto.delete", filter);
    }

    public void validacionPreFormato(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("formato.validacionPreFormato", filter);
    }

    public void validacionPreTransmision(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("formato.validacionPreTransmision", filter);
    }

	public Long getFormatoDrId(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("dr.devuelve.formatodr.element", filter);
		return filter.getLong("formatoDrId");
	}

	public int cuentaAdjuntoRequerido(HashUtil<String, Object> filter) throws Exception {
	    return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjunto_requerido.count", filter).toString());
	}

	public int cuentaAdjuntoRequeridoxTupa(HashUtil<String, Object> filter) throws Exception {
	    return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjunto_requerido_x_tupa.count", filter).toString());
	}

    public String loadNombreUsuario(HashUtil<String, Object> filter) throws Exception {
        return (String) getSqlMapClientTemplate().queryForObject("comun.nombre_usuario.element", filter);
    }

    public String loadPartidaArancelaria(HashUtil<String, Object> filter) {
        return (String) getSqlMapClientTemplate().queryForObject("comun.partida_arancelaria.element", filter);
    }

    public Formato loadFormatoByTupa(HashUtil<String, Object> filter) throws Exception {
        return (Formato) getSqlMapClientTemplate().queryForObject("formato.elementByTupa", filter);
    }

	public JasperPrint ejecutarReporteConConexionBD(JasperReport report, Map parameters) throws Exception {
		return JasperFillManager.fillReport(report, parameters, getSqlMapClientTemplate().getDataSource().getConnection());
	}

    public AdjuntoRequerido loadRequeridoSolicitudCertificadoOrigen(HashUtil<String, Object> filter) throws Exception {
        return (AdjuntoRequerido) getSqlMapClientTemplate().queryForObject("formato.adjunto_requerido.element", filter);
    }

	public String obtenerAyuda(Map<String, Object> filtros) throws Exception {
		return (String) getSqlMapClientTemplate().queryForObject("formato.ayuda.texto", filtros);
	}

	public String permiteCrearFactura(Long ordenId, Long codigoCertificado) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("result",null);
		filtros.put("ordenId",ordenId);
		filtros.put("coId",codigoCertificado);
		getSqlMapClientTemplate().queryForObject("formato.orden.permite.factura", filtros);
		return (String) filtros.get("result");
	}

	public String permiteCrearProductor(Long calificacionUoId) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("result",null);
		filtros.put("calificacionUoId",calificacionUoId);
		getSqlMapClientTemplate().queryForObject("formato.orden.dj.permite.productor", filtros);
		return (String) filtros.get("result");
	}

	public String permiteConfirmarFinEval(Long coId, Long ordenId, Integer mto) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		//filtros.put("result",null);
		filtros.put("coId",coId);
		filtros.put("orden",ordenId);
		filtros.put("mto",mto);
		getSqlMapClientTemplate().queryForObject("formato.orden.permite.confirmar.fin_eval", filtros);
		return (String) filtros.get("result");
	}

    public int cuentaNotificacionesPendientes(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("certificado.notificaciones.pendientes.count", filter).toString());
    }

    public int cuentaAdjuntosRequeridosDJ(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjuntos.requeridos.dj.count", filter).toString());
    }

    public int cuentaNotificacionesSolicitudPendientes(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("solicitud.notificaciones.pendientes.count", filter).toString());
    }

	public String permiteCrearSolicitudRectif(Long suceId) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("result",null);
		filtros.put("suceId",suceId);
		getSqlMapClientTemplate().queryForObject("formato.orden.permite.crear.sol_rectif", filtros);
		return (String) filtros.get("result");
	}

	public String permiteApoderamientoXFrmt(Integer acuerdoInternacionalId) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("result",null);
		filtros.put("acuerdoInternacionalId",acuerdoInternacionalId);
		getSqlMapClientTemplate().queryForObject("proceso.certificado.permite.apoderamiento.x.frmt", filtros);
		return (String) filtros.get("result");
	}

    public int cuentaAdjuntosFacturaRequeridos(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.adjuntos.factura." + filter.getString("formato").trim().toLowerCase() + ".count", filter).toString());
    }

	public String acuerdoConValidacionProductor(Integer codigoAcuerdo) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("result",null);
		filtros.put("codigoAcuerdo",codigoAcuerdo);
		getSqlMapClientTemplate().queryForObject("formato.orden.validacion_productor", filtros);
		return (String) filtros.get("result");
	}
	
	public String es_version_antigua(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("formato.orden.validacion_productor", filter);
		return (String) filter.get("result");
	}
	
	public String es_dj_version_diferente(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("formato.orden.validacion_productor", filter);
		return (String) filter.get("result");
	}
	
	public String es_reemplazo_version_diferente(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("formato.orden.validacion_productor", filter);
		return (String) filter.get("result");
	}

}
