package pe.gob.mincetur.vuce.co.dao;

import java.sql.Connection;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.util.ConstantesCO;

public class DBSessionContext extends SqlMapClientDaoSupport {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_DB);
    
    public Connection getConnection() throws Exception {
    	return this.getDataSource().getConnection();
    }
    
    public void setUserNameToConnection() throws Exception {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        
        if (sra!=null) {
            // Se trata de un request que viene del browser
            logger.debug("SETTING ORACLE SESSION VARIABLE");
            HttpSession session = sra.getRequest().getSession();
            UsuarioCO usuario = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
            
            if (usuario!=null && usuario.getTipoOrigen()!=null) {
            	logger.debug("USUARIO EN CONEXION: "+usuario.getLogin()+" - "+usuario.getTipoOrigen());
                HashUtil<String, Object> filter = new HashUtil<String, Object>();
                filter.put("usuarioId", usuario.getIdUsuario());
                filter.put("esExtranet", usuario.getTipoOrigen().equals(ConstantesCO.AUT_TIPO_USUARIO_ORIGEN_EXT) ? "S" : "N");
                //filter.put("modoTramite", ConstantesVUCE.MODO_TRAMITE_PAGINA_WEB);
                filter.put("sesionId", session.getId());
                getSqlMapClientTemplate().queryForObject("vuce.dbSessionContextIdentifier", filter);
                
                /*HashMap<String, String> datos = ((HashMap<String, String>)getSqlMapClientTemplate().queryForObject("vuce.getSessionContextIdentifier"));
                String usuarioId = datos.get("USUARIO_ID");
                String esExtranet = datos.get("ES_EXTRANET");
                logger.debug("USUARIO EN CONEXION: "+usuarioId+", es extranet? "+esExtranet);
                */
                //getSqlMapClientTemplate().queryForObject("vuce.grabaId", filter);
            } else {
            	limpiarContexto();
            }
        } else {
        	limpiarContexto();
        }
    }
    
    public void limpiarContexto() {
    	logger.debug("LIMPIANDO EL USUARIO DE LA CONEXION");
        HashUtil<String, String> filter = new HashUtil<String, String>();
        filter.put("usuarioId", null);
        filter.put("esExtranet", "N");
        //filter.put("modoTramite", ConstantesVUCE.MODO_TRAMITE_PAGINA_WEB);
        
    	ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
    	if (sra!=null) {
	    	HttpSession session = sra.getRequest().getSession();
	    	filter.put("sesionId", session.getId());
    	} else {
    		filter.put("sesionId", null);
    	}
        getSqlMapClientTemplate().queryForObject("vuce.dbSessionContextIdentifier", filter);
    }
    
    public void setInicioContext(String idUsuario, String esExtranet, int modoTramite) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("usuarioId", Util.integerValueOf(idUsuario));
        filter.put("esExtranet", esExtranet);
        filter.put("modoTramite", modoTramite);
        filter.put("sesionId", null);
        getSqlMapClientTemplate().queryForObject("vuce.dbSessionContextIdentifier", filter);
        
    }
    
    public void setInicioContextEmpresaExterna(int vcId) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("vcId", vcId);
        getSqlMapClientTemplate().queryForObject("vuce.inicio.ctx.empresa.externa", filter);
        
    }
    
    public void setRegistraLog(String programa, int errorOracle, String mensaje) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("programa", programa);
        filter.put("errorOracle", errorOracle);
        filter.put("mensaje", mensaje);
        getSqlMapClientTemplate().queryForObject("vuce.registra.log", filter);
        
    }
    
}
