package pe.gob.mincetur.vuce.co.dao.ibatis.util;

import java.sql.SQLException;
import java.sql.Types;

import org.jlis.core.util.Util;

import com.ibatis.sqlmap.client.extensions.ParameterSetter;
import com.ibatis.sqlmap.client.extensions.ResultGetter;
import com.ibatis.sqlmap.client.extensions.TypeHandlerCallback;

public class IntegerHandler implements TypeHandlerCallback {

	public Object getResult(ResultGetter getter) throws SQLException {
        String value = String.valueOf(getter.getInt());
        if (getter.wasNull()) {
            return null;
        }
        return value;
	}

	public void setParameter(ParameterSetter setter, Object parameter) throws SQLException {
        if (parameter == null) {
            setter.setNull(Types.INTEGER);
        } else {
            setter.setInt(Integer.parseInt(parameter.toString()));
        }
	}

	public Object valueOf(String s) {
		return Util.getDate(s);
	}
}
