package pe.gob.mincetur.vuce.co.dao.ibatis;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.ContingenciaAuthDAO;

/**
 * DAOImpl para Contingencia VUCE
 * @author 
 *
 */
public class ContingenciaAuthDAOImpl extends SqlMapClientDaoSupport implements ContingenciaAuthDAO {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);

	@Override
	public Integer obtenerModoOperacion() throws Exception {
		logger.debug("obtenerModoOperacion");
		Integer result = null;
		HashUtil<String, Object> param = new HashUtil<String, Object>();
		getSqlMapClientTemplate().queryForObject("modoConsultaContingencia.element", param);
		result = param.getInt("modoConsulta");
        return result;		
	}

	@Override
	public String registraConsulta(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("regConsultaAuthContingencia.insert", filter);
		return "";
	}

	@Override
	public String solicitaContingencia(HashUtil<String, Object> filter) throws Exception {
		String codigo = null;
		String mensaje = null;
		getSqlMapClientTemplate().queryForObject("solicitudAuthContingencia.insert", filter);
		codigo = filter.getString("codigo");
		mensaje = filter.getString("mensaje");
		logger.debug("codigo " + codigo);
		logger.debug("mensaje " + mensaje);
		if (codigo != null) {
			int cod = Integer.parseInt(codigo);
			if (cod == 0) { // Exito
				mensaje = "OK";
			}
		}
		return mensaje;
	}

	@Override
	public HashUtil<String, Object> validaToken(HashUtil<String, Object> filter) throws Exception {
		HashUtil<String, Object> result = new HashUtil<String, Object>();
		String codigo = null;
		String mensaje = null;
		getSqlMapClientTemplate().queryForObject("validaToken.insert", filter);
		codigo = filter.getString("codigo");
		mensaje = filter.getString("mensaje");
		logger.debug("codigo " + codigo);
		logger.debug("mensaje " + mensaje);
		result.put("codigo", filter.getString("codigo"));
		result.put("mensaje", filter.getString("mensaje"));
		result.put("tipoUsuario", filter.getInt("tipoUsuario"));
		result.put("ruc", filter.getString("ruc"));
		result.put("usuario", filter.getString("usuario"));
		return result;
	}
    
	@Override
	public String obtenerMensajeIndexContingencia() throws Exception {
        String result = (String) getSqlMapClientTemplate().queryForObject("msje_index_contingencia.element");
		return result;
	}

	@Override
	public String obtenerMensajeCorreoEnviado() throws Exception {
        String result = (String) getSqlMapClientTemplate().queryForObject("msje_correo_enviado.element");
		return result;
	}

	@Override
	public String obtenerMensajeValidacionToken() throws Exception {
        String result = (String) getSqlMapClientTemplate().queryForObject("msje_validacion_token.element");
		return result;
	}

	@Override
	public String obtenerMensajeValidacionCaptura() throws Exception {
        String result = (String) getSqlMapClientTemplate().queryForObject("msje_validacion_captura.element");
		return result;
	}

}
