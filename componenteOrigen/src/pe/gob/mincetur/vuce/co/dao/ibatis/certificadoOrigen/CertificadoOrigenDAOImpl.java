package pe.gob.mincetur.vuce.co.dao.ibatis.certificadoOrigen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.certificadoOrigen.CertificadoOrigenDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.AdjuntoRequeridoDJMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;


/**
*Objeto :	CertificadoOrigenDAOImpl
*Descripcion :	Clase implementadora de persistencia de Certificado de Origen.
*Fecha de Creacion :	
*Autor :	E. Oscátegui - vuce
*------------------------------------------
*Modificaciones
*Codigo		Fecha					Nombre			Descripcion
*------------------------------------------
*001		05/07/2019				GCHAVEZ			Ticket 149343 - ERROR AL GENERAR DJ
*/

public class CertificadoOrigenDAOImpl extends SqlMapClientDaoSupport implements CertificadoOrigenDAO {

	/**
	 * Obtiene información del certificado de origen por el id
	 */
	public CertificadoOrigen getCertificadoOrigenById(HashUtil<String, Object> filter) throws Exception {
		String formato = (String) filter.get("formato");
		return (CertificadoOrigen)getSqlMapClientTemplate().queryForObject("certificadoOrigen." + formato.toLowerCase() + ".orden.element", filter);
	}

	/**
	 * Obtiene información de una factura por el id
	 */
	public COFactura getCOFacturaById(HashUtil<String, Object> filter) throws Exception {
		return (COFactura)getSqlMapClientTemplate().queryForObject("certificadoOrigen.factura." + filter.getString("formato").trim().toLowerCase() + ".element", filter);
	}

	/**
	 * Obtiene información de una mercancia por el id
	 */
	public CertificadoOrigenMercancia getCOMercanciaById(HashUtil<String, Object> filter) throws Exception {
		return (CertificadoOrigenMercancia)getSqlMapClientTemplate().queryForObject("certificadoOrigen.mercancia." + filter.getString("formato").trim().toLowerCase() + ".element", filter);
	}

	/**
	 * Actualiza el Certificado de Origen
	 */
	public void updateCertificadoOrigen(CertificadoOrigen certificadoOrigen) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.update", certificadoOrigen);
	}

	/**
	 * Carga adjuntos de Certificado de Origen
	 */
	public void cargarAdjuntoCertificadoOrigen(AdjuntoCertificadoOrigen adjunto) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.adjunto.insert", adjunto);
	}

	/**
	 * Inserta el registro de una factura
	 */
	public void insertCOFactura(HashUtil<String, Object> filter) throws Exception {
		COFactura factura = (COFactura) filter.get("factura");
		getSqlMapClientTemplate().queryForObject("co.factura." + filter.getString("formato").trim().toLowerCase() + ".insert", factura);		
	}

	/**
	 * Actualiza el registro de una factura
	 */
	public void updateCOFactura(HashUtil<String, Object> filter) throws Exception {
		COFactura factura = (COFactura) filter.get("factura");
		getSqlMapClientTemplate().queryForObject("co.factura." + filter.getString("formato").trim().toLowerCase() + ".update", factura);
	}

	/**
	 * Actualiza el Certificado de Origen
	 */
	public void deleteCOFactura(HashUtil<String, Object> filter) throws Exception {
		COFactura factura = (COFactura) filter.get("factura");
		getSqlMapClientTemplate().queryForObject("co.factura." + filter.getString("formato").trim().toLowerCase() + ".delete", factura);
	}

	/**
	 * Obtiene información de una Mercancía
	 */
	public CertificadoOrigenMercancia getCertificadoOrigenMercanciaById(HashUtil<String, Object> filter) throws Exception {
		return (CertificadoOrigenMercancia)getSqlMapClientTemplate().queryForObject("certificadoOrigen.mercancia." + filter.getString("formato").trim().toLowerCase() + ".element", filter);
	}

	/**
	 * Inserta una Mercancía con DJ existente
	 */
	public void insertCertificadoOrigenMercanciaDjExistente(HashUtil<String, Object> filter) throws Exception {
		CertificadoOrigenMercancia certificadoOrigenMercancia = (CertificadoOrigenMercancia) filter.get("certificadoOrigenMercancia");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.mercancia.dj_existente." + filter.getString("formato").trim().toLowerCase() + ".insert", certificadoOrigenMercancia);
	}

	/**
	 * Inserta una Mercancía cno DJ nueva
	 */
	public void insertCertificadoOrigenMercanciaDjNueva(CertificadoOrigenMercancia certificadoOrigenMercancia) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.mercancia.dj_nueva.insert", certificadoOrigenMercancia);
	}
	/**
	 * Inserta una Mercancía
	 * @param certificadoOrigenMercancia
	 * @throws Exception
	 */
	public void insertCertificadoOrigenMercancia(HashUtil<String, Object> filtro) throws Exception {
		CertificadoOrigenMercancia certificadoOrigenMercancia = (CertificadoOrigenMercancia) filtro.get("certificadoOrigenMercancia");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.mercancia." + filtro.getString("formato").trim().toLowerCase() + ".insert", certificadoOrigenMercancia);
	}
	/**
	 * Modifica una Mercancía
	 * @param certificadoOrigenMercancia
	 * @throws Exception
	 */
	public void updateCertificadoOrigenMercancia(HashUtil<String, Object> filtro) throws Exception {
		CertificadoOrigenMercancia certificadoOrigenMercancia = (CertificadoOrigenMercancia) filtro.get("certificadoOrigenMercancia");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.mercancia." + filtro.getString("formato").trim().toLowerCase() + ".update", certificadoOrigenMercancia);
	}

	/**
	 * Modifica una DJ
	 * @param certificadoOrigenMercancia
	 * @throws Exception
	 */
	public void updateMercanciaDeclaracionJurada(DeclaracionJurada dj) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.update", dj);
	}

	/**
	 * Elimina una Mercancía
	 * @param certificadoOrigenMercancia
	 * @throws Exception
	 */
	public void deleteCertificadoOrigenMercancia(HashUtil<String, Object> filtro) throws Exception {
		CertificadoOrigenMercancia certificadoOrigenMercancia = (CertificadoOrigenMercancia) filtro.get("certificadoOrigenMercancia");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.mercancia." + filtro.getString("formato").trim().toLowerCase() + ".delete", certificadoOrigenMercancia);
	}

	/**
	 * Elimina una DJ
	 * @param certificadoOrigenMercancia
	 * @throws Exception
	 */
	public void deleteMercanciaDeclaracionJurada(DeclaracionJurada dj) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.mercancia.delete", dj);
	}

	/**
	 * Obtiene información de una Declaración Jurada
	 */
	public DeclaracionJurada getCertificadoOrigenDeclaracionJuradaById(HashUtil<String, Object> filter) throws Exception {
		return (DeclaracionJurada)getSqlMapClientTemplate().queryForObject("certificadoOrigen.declaracion_jurada.element", filter);
	}

	/**
	 * Obtiene información de la lista de certificados de origen
	 */
	public List<DeclaracionJuradaProductor> getDJProductorByDJId(HashUtil<String, Object> filter) throws Exception {
		return (List<DeclaracionJuradaProductor>)getSqlMapClientTemplate().queryForList("certificadoOrigen.dj.productor.elements", filter);
	}

	/**
	 * Inserta una Declaración Jurada
	 * @param declaracionJurada
	 * @throws Exception
	 */
	public void insertDeclaracionJurada(DeclaracionJurada declaracionJurada) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.insert", declaracionJurada);
	}

	/**
	 * Inserta una Declaración Jurada sin Mercancia
	 * @param declaracionJurada
	 * @throws Exception
	 */
	public void insertDeclaracionJuradaSinMercancia(DeclaracionJurada declaracionJurada) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.calificacion_anticipada.dj.insert", declaracionJurada);
	}

	/**
	 * Modifica una Declaración Jurada
	 * @param declaracionJurada
	 * @throws Exception
	 */
	public void updateDeclaracionJurada(DeclaracionJurada declaracionJurada) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.update", declaracionJurada);
	}

	/**
	 * Modifica el Consolidado de una Declaración Jurada
	 * @param declaracionJurada
	 * @throws Exception
	 */
	public void updateDeclaracionJuradaConsolidado(DeclaracionJurada declaracionJurada) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.consolidado.update", declaracionJurada);
	}

	/**
	 * Inserta adjuntos de Declaración Jurada y de Material de Declaración Jurada
	 */
	public void cargarAdjuntoDeclaracionJurada(AdjuntoCertificadoOrigen adjunto) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.adjunto.insert", adjunto);
	}

	/**
	 * Elimina un adjunto de la declaración jurada
	 */
    public void eliminarAdjuntoDJ(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.adjunto.delete", filter);
    }

	/**
	 * Obtiene información de un Material de Declaración Jurada
	 */
	public DeclaracionJuradaMaterial getDeclaracionJuradaMaterialById(HashUtil<String, Object> filter) throws Exception {
		return (DeclaracionJuradaMaterial)getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.material.element", filter); // falta implementar el query
	}

	/**
	 * Inserta un Material de Declaración Jurada
	 */
	public void insertDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.material.insert", declaracionJuradaMaterial);
	}

	/**
	 * Modifica un Material de Declaración Jurada
	 * @param declaracionJuradaMaterial
	 * @throws Exception
	 */
	public void updateDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.material.update", declaracionJuradaMaterial);
	}

	/**
	 * Elimina un Material de Declaración Jurada
	 * @param declaracionJuradaMaterial
	 * @throws Exception
	 */
	public void deleteDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.material.delete", declaracionJuradaMaterial);
	}

	/**
	 * Obtiene información de un Productor de Declaración Jurada
	 */
	public DeclaracionJuradaProductor getDeclaracionJuradaProductorById(HashUtil<String, Object> filter) throws Exception {
		return (DeclaracionJuradaProductor)getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.productor.element", filter); // falta implementar el query
	}

	/**
	 * Inserta un Productor de Declaración Jurada
	 */
	public void insertDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.productor.insert", declaracionJuradaProductor);
	}

	/**
	 * Modifica un Productor de Declaración Jurada
	 * @param declaracionJuradaProductor
	 * @throws Exception
	 */
	public void updateDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.productor.update", declaracionJuradaProductor);
	}

	/**
	 * Elimina un Productor de Declaración Jurada
	 * @param declaracionJuradaProductor
	 * @throws Exception
	 */
	public void deleteDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.dj.productor.delete", declaracionJuradaProductor);
	}

	/**
	 *
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> getCertificadoOrigenDRById(HashUtil<String, Object> filter) throws Exception {
		return (HashMap<String, Object>)getSqlMapClientTemplate().queryForObject("certificado_origen." + filter.getString("formato") + ".dr.datos", filter);
	}


	/**
	 *
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> getCOMercanciaDRById(HashUtil<String, Object> filter) throws Exception {
		return (HashMap<String, Object>)getSqlMapClientTemplate().queryForObject("certificado_origen.mercancia.dr.datos", filter);
	}

	/**
	 *
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, Object> getCOFacturaDRById(HashUtil<String, Object> filter) throws Exception {
		return (HashMap<String, Object>)getSqlMapClientTemplate().queryForObject("certificado_origen.factura.dr.datos", filter);
	}

	/**
	 *
	 * @param filter
	 * @return
	 * @throws Exception
	 */
    public AdjuntoRequeridoDJMaterial loadAdjuntoRequeridoDJ(HashUtil<String, Object> filter) throws Exception {
        return (AdjuntoRequeridoDJMaterial) getSqlMapClientTemplate().queryForObject("dj.adjunto_requerido.element", filter);
    }

    /**
     *MTC002 Duplicado del Certificado de Origen
     */
	public CODuplicado getCertificadoOrigenMTC002ById(HashUtil<String, Object> filter) throws Exception {
		return (CODuplicado)getSqlMapClientTemplate().queryForObject("certificadoOrigen.mct002.orden.element", filter);
	}

	public void updateCertificadoOrigenDuplicado(CODuplicado coDuplicado) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.duplicado.orden.update", coDuplicado);
	}

	public void insertCertificadoOrigenDuplicadoOrigen(CODuplicado coDuplicado) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.duplicado.origen.insert", coDuplicado);
	}

	public String getCausalById(HashUtil<String, Object> filter) throws Exception {
		return (String)getSqlMapClientTemplate().queryForObject("certificado_origen.duplicado.causal.element", filter);
	}

    /**
     *MTC004 Anulacion del Certificado de Origen
     */
	public COAnulacion getCertificadoOrigenMTC004ById(HashUtil<String, Object> filter) throws Exception {
		return (COAnulacion)getSqlMapClientTemplate().queryForObject("certificadoOrigen.mct004.orden.element", filter);
	}

	public void updateCertificadoOrigenAnulacion(COAnulacion coAnulacion) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.anulacion.orden.update", coAnulacion);
	}

	public void insertCertificadoOrigenAnulacionOrigen(COAnulacion coAnulacion) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.anulacion.origen.insert", coAnulacion);
	}

    public void solicitaValidacionProductor(Map<String,Object> parametros) throws Exception {
    	getSqlMapClientTemplate().queryForObject("ddjj.solicita_validacion_productor", parametros);
    }

    public void aceptaSolicitudValidacion(Map<String,Object> parametros) throws Exception {
    	getSqlMapClientTemplate().queryForObject("ddjj.acepta_solicitud_validacion", parametros);
    }

    public void rechazaSolicitudValidacion(Map<String,Object> parametros) throws Exception {
    	getSqlMapClientTemplate().queryForObject("ddjj.rechaza_solicitud_validacion", parametros);
    }

    public void transmiteDjValidada(Map<String,Object> parametros) throws Exception {
    	getSqlMapClientTemplate().queryForObject("ddjj.transmite_dj_validada", parametros);
    }

	public void insertCertificadoOrigenReemplazoOrigen(COReemplazo coReemplazo) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.reemplazo.origen.insert", coReemplazo);
	}

	public void updateCertificadoOrigenReemplazo(COReemplazo coReemplazo) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.reemplazo.orden.update", coReemplazo);
	}

	public void insertSubsanacionSolicitud(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.subsanacion.solicitud.registrar", filter);
	}

	public void deleteSubsanacionSolicitud(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.subsanacion.solicitud.desistir", filter);
	}

	public int mtoCertificadoVigente(Long ordenId) throws Exception{
		Integer res;
		res = (Integer) getSqlMapClientTemplate().queryForObject("certificadoOrigen.mto.vigente", ordenId);

		if (res == null) {
			res = new Integer(-1);
		}

		return res;
	}

	/**
	 * Obtiene el id del adjunto correspondiente a la firma del DR
	 */
	public Long getAdjunFirmaId(HashUtil<String, Object> filter) throws Exception {
		return (Long)getSqlMapClientTemplate().queryForObject("certificadoOrigen.adjunto.firmaDr", filter);
	}

    public void registraExportadorAutorizadoDj(Map<String,Object> parametros) throws Exception {
    	getSqlMapClientTemplate().queryForObject("calificacionOrigen.registra.exportador.autorizado", parametros);
    }

    public void revocaExportadorAutorizadoDj(Map<String,Object> parametros) throws Exception {
    	getSqlMapClientTemplate().queryForObject("calificacionOrigen.revoca.exportador.autorizado", parametros);
    }

	public String devuelveEmpresaId(String tipoDoc, String numeroDoc) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		//filtros.put("result",null);
		filtros.put("tipoDoc",tipoDoc);
		filtros.put("numeroDoc",numeroDoc);
		getSqlMapClientTemplate().queryForObject("autenticacion.devuelve.empresa.id", filtros);
		return (String) filtros.get("result");
	}
	
	public String devuelveEmpresaIdOUsuPrincipalIdByUsuId(String tipoDoc, String numeroDoc) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("tipoDoc",tipoDoc);
		filtros.put("numeroDoc",numeroDoc);
		getSqlMapClientTemplate().queryForObject("autenticacion.devuelve.empresaId_usuPrincipalId", filtros);
		return (String) filtros.get("result");
	}

	/**
	 * Obtiene información de una Declaración Jurada
	 */
	public void actualizarFlgDjCompleta(CalificacionOrigen filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.update.dj.completa", filter);
	}

	/**
	 * Realiza la busqueda de partidas arancelarias
	 */
	public void obtenerPartidas(HashUtil<String, Object> filter) throws Exception {

		if(filter.getInt("opbusqueda") == 0) {
			getSqlMapClientTemplate().queryForObject("co.comun.busqueda.partida.cod", filter);
		} else {
			HashMap<String, Object> prmtConsulta = new HashMap<String, Object>();

			prmtConsulta.put("descripcion", filter.getString("descripcion"));
			prmtConsulta.put("aproximacion", filter.getString("aproximacion"));

			getSqlMapClientTemplate().queryForObject("co.comun.busqueda.partida.desc", prmtConsulta);
		}

	}

	/**
	 * Invoca la funcion que determina si se debe mostrar el boton para requerir la validacion del productor para la dj
	 */
	public String djBtnValidacionProd(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.validacion.btn", filter);
		return filter.getString("mostrar");
	}

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	/**
	 * Actualiza la Direccion Adicional de la Calificacion de Origen
	 */
	public void updateDireccionAdicionalCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("calificacionOrigen.direccionAdicional.update", filter);
	}	
	
	/**
	 * Obtiene una Calificación de Origen por su Id
	 */
	public CalificacionOrigen getCalificacionOrigenById(HashUtil<String, Object> filter) throws Exception {
		return (CalificacionOrigen)getSqlMapClientTemplate().queryForObject("calificacionOrigen.orden.element", filter);
	}

    /**
     * Registra una Calificación de Origen
     */
    public void insertCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
		CalificacionOrigen calificacionOrigen = (CalificacionOrigen) filter.get("calificacionOrigen");
		getSqlMapClientTemplate().queryForObject("calificacionOrigen.insert", calificacionOrigen);
	}

    /**
     * Actualiza información de Rol de una Calificación de Origen
     */
    public void updateRolCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
		CalificacionOrigen calificacionOrigen = (CalificacionOrigen) filter.get("calificacionOrigen");
		getSqlMapClientTemplate().queryForObject("calificacionOrigen.rol.update", calificacionOrigen);
	}

    /**
     * Actualiza información de Criterio de una Calificación de Origen
     */
    public void updateCriterioCalificacionOrigen(HashUtil<String, Object> filter) throws Exception {
		CalificacionOrigen calificacionOrigen = (CalificacionOrigen) filter.get("calificacionOrigen");
		getSqlMapClientTemplate().queryForObject("calificacionOrigen.criterio.update", calificacionOrigen);
	}

    /**
     * Registra una Declaración Jurada que tiene asociada una Calificación de Origen
     */
    public void insertDeclaracionJuradaCalificacion(HashUtil<String, Object> filter) throws Exception {
		DeclaracionJurada declaracionJurada = (DeclaracionJurada) filter.get("declaracionJurada");
		getSqlMapClientTemplate().queryForObject("calificacionOrigen.dj.insert", declaracionJurada);
	}

    /**
     * Devuelve 1 si la dj esta en proceso de validacion
     */
	public String djEnProcesoValidacion(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.fn.dj.proceso.validacion", filter);
		return filter.getString("bloquear");
	}

    /**
     * Registra una Calificación de Origen junto a una Mercancia y una Declaración Jurada
     */
    public void insertCalificacionOrigenConMercanciaDeclaracionJurada(HashUtil<String, Object> filter) throws Exception {
		CalificacionOrigen calificacionOrigen = (CalificacionOrigen) filter.get("calificacionOrigen");
		getSqlMapClientTemplate().queryForObject("calificacionOrigen.insert_califcacion_mercancia_declaracion", calificacionOrigen);
	}

    /**
     * Registra un mercancía a partir de una calificacion existente
     */
    public HashUtil<String, Object> enlazaDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.calificacion.enlaza.mercancia", filter);

		HashUtil<String, Object> res = new HashUtil<String, Object>();
		res.put("secuenciaMercancia", filter.get("secuenciaMercancia"));
		res.put("djId", filter.get("djId"));

		return res;
	}

    /**
     * Registra un mercancía nueva a partir de una calificacion existente
     */
    public HashUtil<String, Object> copiarDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception {
    	filter.put("copiaXModif", "N");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.calificacion.mercancia.copia", filter);

		HashUtil<String, Object> res = new HashUtil<String, Object>();
		res.put("calificacionUoId", filter.get("calificacionUoIdNew"));
		res.put("secuenciaMercancia", filter.get("secuenciaMercancia"));
		res.put("djId", filter.get("djId"));

		return res;
	}

    /*
     * Devuelve el número de materiales registrados para una DJ determinada
     */
    public int cuentaDJMateriales(HashUtil<String, Object> filter) {
        return Integer.parseInt(getSqlMapClientTemplate().queryForObject("formato.dj.count.materiales", filter).toString());
    }

    public DeclaracionJurada getDjByCalificacionUoId(HashUtil<String, Object> filter, String enIngles) throws Exception {
    	String query = "certificadoOrigen.dj.producto"+("S".equals(enIngles)?".ingles":"")+".element";
		return (DeclaracionJurada)getSqlMapClientTemplate().queryForObject(query, filter);

	}

    /**
     * Registra un calificación nueva a partir de una calificacion existente (para MCT005)
     */
    public HashUtil<String, Object> copiarDJCalificacionXCalificacion(HashUtil<String, Object> filter) throws Exception {
    	filter.put("copiaXModif", "N");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.calificacion.x.calificacion.copia", filter);

		HashUtil<String, Object> res = new HashUtil<String, Object>();
		res.put("calificacionUoId", filter.get("calificacionUoIdNew"));
		res.put("djId", filter.get("djId"));

		return res;
	}

    /**
     * Devuelve S si todos los materiales originarios de la dj tienen archivos adjuntos
     */
	public String djMaterialTieneAdj(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.fn.dj.material.tiene.adjunto", filter);
		return filter.getString("tiene");
	}



	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	/**
	 * Indica si una dj requiere ser validada por el productor
	 */
	public String reqValidacionProductor(HashMap<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("proceso_certificado.tramite_req_validacion_prod", filter);
		return (String) filter.get("reqValidacion");
	}

	/**
	 * Indica si una dj ha sido validada
	 */
	public String djValidada(HashMap<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("proceso_certificado.dj_validada", filter);
		return (String) filter.get("validada");
	}
	
	/**
	 * Indica si existe un productor validador
	 * NPCS: 08/02/2018
	 */
	public String existeProductorValidador(HashMap<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("proceso_dj.existe_productor_validador", filter);
		return (String) filter.get("existe");
	}

	public String obtenerCalificacionHash(HashMap<String, Object> filter) throws Exception{
		getSqlMapClientTemplate().queryForObject("proceso_calificacion.calificacion.hash", filter);
		return (String) filter.get("hash");
	}

	public void obtenerComparaCalificacionHash(HashMap<String, Object> filter) throws Exception{
		getSqlMapClientTemplate().queryForObject("validacion_calificacion.calificacion.compara.hash", filter);
	}

    public String obtenerIdiomaLlenado(Integer idAcuerdo) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("idAcuerdo", idAcuerdo);
		return (String)getSqlMapClientTemplate().queryForObject("comun.idiomallenado.element", filter).toString();
    }

	public void validarCertificadoEspacioDet(HashMap<String, Object> filter) throws Exception{
		getSqlMapClientTemplate().queryForObject("validacion_certificado.espacio.det", filter);
	}

	/**
	 * Obtiene la suma de los valores US$ de los materiales de una dj
	 */
	public double totalValorUSMateriales(HashMap<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("proceso_dj.total.valor.us.materiales", filter);
		//Ticket 149343 GCHAVEZ-05/07/2019
		return filter.get("result")==null?0:(Double) filter.get("result");
		//Ticket 149343 GCHAVEZ-05/07/2019
	}
	
	/**
	 * Obtiene los datos del propietario de la DJ calificada
	 */
    public HashUtil<String, Object> obtenerDatosPropietarioDJ(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.calificacion.obtenerDatosPropietarioDJ", filter);
		          
		HashUtil<String, Object> res = new HashUtil<String, Object>();
		res.put("numDocPropietarioDJ", filter.get("numDocPropietarioDJ"));
		
		return res;
	}
    
    /**
	 * Inserta el Certificado de Origen de Empresa
	 */
    ////20140825_JMC_Intercambio_Empresa
	public int insertCertificadoOrigenEmpresa(CertificadoOrigen certificadoOrigen) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.orden.insert.tmp", certificadoOrigen);
		return certificadoOrigen.getSecuenciaCertificado();
	}
	
	/**
	 * Inserta el registro de una factura de Empresa
	 */
	////20140825_JMC_Intercambio_Empresa
	public void insertCOFacturaEmpresa(COFactura factura) throws Exception {
		//COFactura factura = (COFactura) filter.get("factura");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.factura.insert.tmp", factura);		
	}


	/**
	 * Inserta una Mercancía de Empresa
	 */
	public void insertCertificadoOrigenMercanciaEmpresa(CertificadoOrigenMercancia certificadoOrigenMercancia) throws Exception {
		//CertificadoOrigenMercancia certificadoOrigenMercancia = (CertificadoOrigenMercancia) filtro.get("certificadoOrigenMercancia");
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.mercancia.insert.tmp", certificadoOrigenMercancia);
	}


	
	
    /**
	 * Inserta el Certificado de Origen de Empresa
	 */
	public int insertDeclaracionJuradaEmpresa(DeclaracionJurada declaracionJurada) throws Exception {
		getSqlMapClientTemplate().queryForObject("declaracionJurada.declaracion.insert.tmp", declaracionJurada);
		return Util.integerValueOf(declaracionJurada.getSecuenciaDeclaracionJurada());				
	}
	
	/**
	 * Inserta el registro de una factura de Empresa
	 */
	public void insertDeclaracionJuradaProductorEmpresa(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception {
		//COFactura factura = (COFactura) filter.get("factura");
		getSqlMapClientTemplate().queryForObject("declaracionJurada.productor.insert.tmp", declaracionJuradaProductor);		
	}


	/**
	 * Inserta una Mercancía de Empresa
	 */
	public void insertDeclaracionJuradaMaterialEmpresa(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception {
		//CertificadoOrigenMercancia certificadoOrigenMercancia = (CertificadoOrigenMercancia) filtro.get("certificadoOrigenMercancia");
		getSqlMapClientTemplate().queryForObject("declaracionJurada.material.insert.tmp", declaracionJuradaMaterial);
	}
	
	public byte[] loadCertificadoFD(HashUtil<String, Object> filter) throws Exception{
		InteroperabilidadFirma interoperabilidadFirma = (InteroperabilidadFirma)getSqlMapClientTemplate().queryForObject("certificadoOrigen.documentoFD.element", filter);
    	return interoperabilidadFirma.getPdf();
    }
	
	public String validaCOFD(HashMap<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.valida_coFD", filter);
		return (String) filter.get("resultado");
	}


	public void insertArchivoCOFD(HashMap<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.archivo.update_coFD", filter);
	}
	
	public void habilitaFirma(HashMap<String, Object> filter) throws Exception{
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.valida_firmaDigital", filter);
	}

	/**
	 * Obtiene el usuario que solicito la calificacion
	 */
    public HashUtil<String, Object> solicitanteCalificacion(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificadoOrigen.dj.calificacion.obtenerDatosSolicitanteDJ", filter);

		System.out.println("****numDocSolicitanteCalif:"+filter.get("numDocSolicitanteCalif"));
		
		HashUtil<String, Object> res = new HashUtil<String, Object>();
		res.put("numDocSolicitanteCalif", filter.get("numDocSolicitanteCalif"));
		
		return res;
	}
    
    //NPCS: 14/03/2019 REGENERACION DE MERCANCIAS 
	public byte[] loadCertificadoFDXML(HashUtil<String, Object> filter) throws Exception{
		InteroperabilidadFirma interoperabilidadFirma = (InteroperabilidadFirma)getSqlMapClientTemplate().queryForObject("certificadoOrigen.documentoFDXML.element", filter);
   	return interoperabilidadFirma.getXml();
   }
	    
    public void eliminarProductorMercanciasXML(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificadoOrigen.productor_mercancias_xml.delete", filter);
    }
    
    public void eliminarMercanciasXML(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificadoOrigen.mercancias_xml.delete", filter);
    }

}
