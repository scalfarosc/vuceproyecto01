package pe.gob.mincetur.vuce.co.dao.ibatis;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.TransmisionPackDAO;
import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;



public class TransmisionPackDAOImpl extends SqlMapClientDaoSupport implements TransmisionPackDAO {
	
	public DocResolutivoSecEBXML registrarDrIOP(DocResolutivoSecEBXML dr,Long suce)throws Exception {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("suce", suce.intValue());
		filter.put("drIOPId", null);
		filter.put("drIOP", null);
        getSqlMapClientTemplate().queryForObject("drIOP.insert", filter);
        
        dr.setDrIOP(filter.getLong("drIOP"));
        dr.setDrIOPId(filter.getInt("drIOPId"));
		dr.setSuce(suce.intValue());
        // Se invoca al registro de los datos del ebXML en el DR (especifico para cada formato)
		//registrarIOP(dr);
	    
		return dr;
	}
	// 2017.04.07 JMC Alianza Pacifico
	public Integer generaN85(Long drId,Integer sdr)throws Exception {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();		
		filter.put("drId", drId);
		filter.put("sdr", sdr);
		filter.put("vcId", null);
        getSqlMapClientTemplate().queryForObject("drIOP.generaN85", filter);
        
        return filter.getInt("vcId");
	}

	public Integer generaTransmisionN93(Integer codId) throws Exception {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();		
		filter.put("codId", codId);
		filter.put("vcId", null);
        getSqlMapClientTemplate().queryForObject("drIOP.generaTransmisionN93", filter);
        
        return filter.getInt("vcId");
	}
}
