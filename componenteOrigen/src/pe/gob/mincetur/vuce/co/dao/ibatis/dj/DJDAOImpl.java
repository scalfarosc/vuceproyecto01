package pe.gob.mincetur.vuce.co.dao.ibatis.dj;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.dj.DJDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.dj.AdjuntoRequeridoDJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJ;
import pe.gob.mincetur.vuce.co.domain.dj.DJExportador;
import pe.gob.mincetur.vuce.co.domain.dj.DJMaterial;
import pe.gob.mincetur.vuce.co.domain.dj.DJProductor;


public class DJDAOImpl extends SqlMapClientDaoSupport implements DJDAO{

	public HashUtil<String, Object> insertDJ(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("ddjj.inserta.dj", filter);
        return filter;
    }

    public void insertUsuarioDJ(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("ddjj.inserta.usuario.dj", filter);
    }

    public void transmiteDJ(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("ddjj.transmite.dj", filter);
    }

	public void updateDJ(DJ dj) throws Exception {
		getSqlMapClientTemplate().queryForObject("ddjj.update.dj", dj);
	}

	public DJ getDJById(HashUtil<String, Object> filter)throws Exception{
		return (DJ) getSqlMapClientTemplate().queryForObject("ddjj.dj.element", filter);
	}

	public DJMaterial getDJMaterialById(HashUtil<String, Object> filter)throws Exception{
		return (DJMaterial) getSqlMapClientTemplate().queryForObject("ddjj.dj_material.element", filter);
	}

	public DJProductor getDJProductorById(HashUtil<String, Object> filter)throws Exception{
		return (DJProductor) getSqlMapClientTemplate().queryForObject("ddjj.dj_productor.element", filter);
	}

	public DJExportador getDJExportadorById(HashUtil<String, Object> filter)throws Exception{
		return (DJExportador) getSqlMapClientTemplate().queryForObject("ddjj.dj_exportador.element", filter);
	}

	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter)throws Exception{
		return (UsuarioFormato) getSqlMapClientTemplate().queryForObject("formato.usuario_formato.element", filter);
	}

	public Solicitud getSolicitudById(HashUtil<String, Object> filter)throws Exception{
		return (Solicitud) getSqlMapClientTemplate().queryForObject("ddjj.solicitud.element", filter);
	}

	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter)throws Exception{
		return (Solicitante) getSqlMapClientTemplate().queryForObject("formato.solicitante.element", filter);
	}

	public Integer insertMaterial(DJMaterial obj) throws Exception{
		getSqlMapClientTemplate().queryForObject("ddjj.insert.material", obj);
		return obj.getSecuenciaMaterial();
	}
	public void updateMaterial(DJMaterial obj){
		getSqlMapClientTemplate().queryForObject("ddjj.update.material", obj);
	}
	public void deleteMaterial(DJMaterial obj){
		getSqlMapClientTemplate().queryForObject("ddjj.delete.material", obj);
	}

	public Integer insertProductor(DJProductor obj) throws Exception{
		getSqlMapClientTemplate().queryForObject("ddjj.insert.productor", obj);
		return obj.getSecuenciaProductor();
	}
	public void updateProductor(DJProductor obj){
		getSqlMapClientTemplate().queryForObject("ddjj.update.productor", obj);
	}
	public void deleteProductor(DJProductor obj){
		getSqlMapClientTemplate().queryForObject("ddjj.delete.productor", obj);
	}

	public Integer insertExportador(DJExportador obj) throws Exception{
		getSqlMapClientTemplate().queryForObject("ddjj.insert.exportador", obj);
		return obj.getSecuenciaExportador();
	}
	public void updateExportador(DJExportador obj){
		getSqlMapClientTemplate().queryForObject("ddjj.update.exportador", obj);
	}
	public void deleteExportador(DJExportador obj){
		getSqlMapClientTemplate().queryForObject("ddjj.delete.exportador", obj);
	}

	public void actualizarSeleccionEsExportador(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("ddjj.actualizarSeleccionEsExportador", filter);
	}
    public AdjuntoRequeridoDJ getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception {
        return (AdjuntoRequeridoDJ) getSqlMapClientTemplate().queryForObject("ddjj.adjunto_requerido.element", filter);
    }

    public void updateRepresentante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("ddjj.actualiza.representante_legal", filter);
    }

    public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("ddjj.actualiza.cargoDeclarante", filter);
    }

	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception {
		return (Integer)getSqlMapClientTemplate().queryForObject("ddjj.adjuntos_requeridos.count", filter);
	}

	public void modificarRol(long djId, int tipoUsuarioDj) throws Exception {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("djId", djId);
		filter.put("tipoUsuarioDj", tipoUsuarioDj);
        getSqlMapClientTemplate().queryForObject("ddjj.modifica.rol.dj", filter);
	}

}
