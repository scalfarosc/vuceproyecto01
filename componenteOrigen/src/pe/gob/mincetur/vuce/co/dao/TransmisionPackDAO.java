package pe.gob.mincetur.vuce.co.dao;

import pe.gob.mincetur.vuce.co.procesodr.domain.ebxml.DocResolutivoSecEBXML;

public interface TransmisionPackDAO {
	
	public DocResolutivoSecEBXML registrarDrIOP(DocResolutivoSecEBXML dr,Long suce)throws Exception;
	
	public Integer generaN85(Long drId,Integer sdr)throws Exception;
	
	public Integer generaTransmisionN93(Integer codId) throws Exception;

}
