package pe.gob.mincetur.vuce.co.dao;

import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.firma.BienOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.CodExporter;
import pe.gob.mincetur.vuce.co.domain.firma.EhCert;
import pe.gob.mincetur.vuce.co.domain.firma.EnteComercio;
import pe.gob.mincetur.vuce.co.domain.firma.FacturaOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.FacturaTercerOpOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;

public interface InteroperabilidadFirmaDAO {
    
	public InteroperabilidadFirma obtenerToken(Long drId, Integer sdr) throws Exception;
		
    public Integer registrarInteroperabilidad(InteroperabilidadFirma iop) throws Exception;

    public Integer getEtapa(String token) throws Exception;
    
    public void actualizaEstadoFirma(Long drId, Integer sdr, String estado) throws Exception;

    public void actualizaXml(String token, Integer secuencia, byte[] xml) throws Exception;

    public void actualizaPdf(String token, Integer secuencia, byte[] pdf) throws Exception;

    public InteroperabilidadFirma obtenerEtapa(String token, Integer secuencia) throws Exception;
    
    public InteroperabilidadFirma obtenerXml(String token, Integer secuencia) throws Exception;
    
    public InteroperabilidadFirma obtenerPdf(String token, Integer secuencia) throws Exception;
    
    public void actualizaFirmaExportador(Long drId, Integer sdr, String nombreExp, String formato, String posteriori) throws Exception;

    public boolean esMct001(Long drId, Integer sdr) throws Exception;

    public boolean esMct003(Long drId, Integer sdr) throws Exception;
    
    //JMC 28/03/2017 version 0.7

    public Long generaDrAlianza (Long drId, Integer sdr) throws Exception;
    
    public EnteComercio obtenerExportador(Long codId) throws Exception;

    public EnteComercio obtenerImportador(Long codId) throws Exception;
    
    public List<EnteComercio> obtenerProductores(Long codId) throws Exception;    
 
    public List<FacturaOrigen> obtenerFacturas(Long codId) throws Exception;

    public List<BienOrigen> obtenerBienes(Long codId) throws Exception;
    
    public EhCert obtenerCertificacionEH(Long codId) throws Exception;
    
    public CodExporter obtenerDetalleCertificado(Long drId, Integer sdr) throws Exception;

    public List<FacturaTercerOpOrigen> obtenerFacturasTercerOp(Long codId) throws Exception;
    
    public String actualizaDrAlianza (Integer codId) throws Exception;
    
    public String produccionControlada(Long drId) throws Exception;
    
    public RespuestaCert obtenerCertificado(HashUtil<String, Object> filter) throws Exception;
    
    public String tipoFirma(Long drId) throws Exception;
    
    public String permiteFirmaDigital(Long ordenId) throws Exception;
    
}
