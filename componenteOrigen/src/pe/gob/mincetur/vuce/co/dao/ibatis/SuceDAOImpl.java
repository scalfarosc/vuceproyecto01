package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.SuceDAO;
import pe.gob.mincetur.vuce.co.domain.ModificacionSuce;
import pe.gob.mincetur.vuce.co.domain.Suce;
import pe.gob.mincetur.vuce.co.procesodr.domain.ModificacionDR;
import pe.gob.mincetur.vuce.co.util.Rol;

public class SuceDAOImpl extends SqlMapClientDaoSupport implements SuceDAO {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_DB);

    public Suce loadSuceById(long suce) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", suce);
        return (Suce)getSqlMapClientTemplate().queryForObject("suce.id.element", filter);
    }

    public Suce loadSuceByIdAndUsuarioId(long suce, long usuarioId, long rol) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", suce);
        
        String query;
        
        if (rol == Rol.CO_ENTIDAD_FIRMA.getCodigo().longValue()) {
        	query = "suce.id_usuario.firmante.element";
        } else {
        	query = "suce.id_usuario.element";
        	
	        filter.put("usuarioId", usuarioId);
	        filter.put("rol", rol);
        }
        
        return (Suce)getSqlMapClientTemplate().queryForObject(query, filter);
    }

    public Integer registrarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.registrarBorradorNotificacionSubsanacion", filter);
        return filter.getInt("notificacionId");
    }

    public void actualizarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.actualizarBorradorNotificacionSubsanacion", filter);
    }

    public void enviarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.confirmarBorradorNotificacionSubsanacion", filter);
    }

    public void eliminarBorradorNotificacionSubsanacion(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.eliminarBorradorNotificacionSubsanacion", filter);
    }

    public ModificacionSuce loadModificacionSuceByIdMensajeId(HashUtil<String, Object> filter) throws Exception {
        return (ModificacionSuce)getSqlMapClientTemplate().queryForObject("usuario.suce.modificacion_suce.element", filter);
    }

    public ModificacionDR loadModificacionDrById(HashUtil<String, Object> filter) throws Exception {
        return (ModificacionDR)getSqlMapClientTemplate().queryForObject("suce.modificacion_dr.element", filter);
    }

    public HashUtil<String, Object> insertSubsanacion(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("usuario.suce.subsanacion.insert", filter);
    	return filter;
    }

	public void atiendeNotificacion(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("usuario.suce.subsanacion.notificacion.atiende", filter);
		return;
	}

    public HashUtil<String, Object> insertModificacionDr(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.modif_dr.insert", filter);
        return filter;
    }

    public void transmiteModificacionDr(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.trans_dr.insert", filter);
    }

    public void actualizaModificacionDr(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.actualiza_dr.insert", filter);
    }

    public void eliminaModificacionDr(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.elimina_dr.insert", filter);
    }

    public void aprobarModificacion(int suce, int modificacionSuce) throws Exception {
	    HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("suce", suce);
	    filter.put("modificacionSuceId", modificacionSuce);
	    getSqlMapClientTemplate().queryForObject("suce.resolutor.aprobarModificacion", filter);
	}

	public Integer [] rechazarModificacion(int suce, int modificacionSuce, String mensaje) throws Exception {
	    HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("suce", suce);
	    filter.put("modificacionSuce", modificacionSuce);
	    filter.put("mensaje", mensaje);
	    filter.put("mensajeId", null);
	    filter.put("mensajeIdTexto", null);
	    getSqlMapClientTemplate().queryForObject("suce.resolutor.rechazoModificacion", filter);
	    Integer [] result = new Integer[2];
	    result[0] = filter.getInt("mensajeId");
	    result[1] = filter.getInt("mensajeIdTexto");
	    return result;
	}

    public void transmiteSubsanacion(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("usuario.suce.subsanacion.transmite", filter);
    }

    public void actualizaSubsanacion(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("usuario.suce.subsanacion.actualiza", filter);
    }

    public void eliminaSubsanacion(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("usuario.suce.subsanacion.elimina", filter);
    }

    public void aprobarModificacionDr(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.modif_dr.eval.aprueba", filter);
    }

    public void rechazarModificacionDr(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.modif_dr.eval.rechaza", filter);
    }

    public HashUtil<String, Object> registrarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.dr.eval.registra", filter);
        return filter;
    }

    public HashUtil<String, Object> eliminarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.dr.eval.elimina", filter);
        return filter;
    }

    public void modificarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("suce.dr.eval.modifica", filter);
    }

    public void confirmarRectificacionDrEval(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("suce.dr.eval.confirma", filter);
    }

    public Suce loadSuceModificacionById(int suceId, int mto) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suceId", suceId);
        filter.put("mto", mto);
        return (Suce)getSqlMapClientTemplate().queryForObject("suce.mts.suce_subsanacion.elementById", filter);
    }

    public Suce loadSuceByNumeroAndUsuarioId(int numSuce, int usuarioId, int rol) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("numSuce", numSuce);
        filter.put("usuarioId", usuarioId);
        filter.put("rol", rol);
        return (Suce)getSqlMapClientTemplate().queryForObject("administracionEntidad.suceByNumero.element", filter);
    }

    public Suce loadSuceByNumero(int numeroSuce) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("suce", numeroSuce);
        return (Suce)getSqlMapClientTemplate().queryForObject("suce.element", filter);
    }

    public void desistirSuce(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("suce.desiste", filter);
    }

    public void aceptacionDesestimientoSuce(Integer vcId, int suce) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("suce", suce);
    	filter.put("vcId", vcId);
    	filter.put("mensaje", "-");
    	getSqlMapClientTemplate().queryForObject("suce.aceptacionDesestimientoSuce", filter);
    }

    public void rechazoDesestimientoSuce(int suce) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("suce", suce);
    	filter.put("mensaje", "-");
    	getSqlMapClientTemplate().queryForObject("suce.rechazoDesestimientoSuce", filter);
    }

    public String suceFlgPendienteCalif(Integer suceId) throws Exception{
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("suceId", suceId);
    	//HashMap res = (HashMap) getSqlMapClientTemplate().queryForObject("suce.flg.pendiente.calificacion", filter);
    	//return res.get("pendiente_calificacion").toString().trim();
    	return (String) getSqlMapClientTemplate().queryForObject("suce.flg.pendiente.calificacion", filter);
    }

}