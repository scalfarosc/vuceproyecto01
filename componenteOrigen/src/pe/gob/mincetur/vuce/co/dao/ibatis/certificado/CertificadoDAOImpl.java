package pe.gob.mincetur.vuce.co.dao.ibatis.certificado;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.certificado.CertificadoDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoRequeridoCertificado;
import pe.gob.mincetur.vuce.co.domain.certificado.Certificado;


public class CertificadoDAOImpl extends SqlMapClientDaoSupport implements CertificadoDAO {
	
	public HashUtil<String, Object> insertCertificado(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificado.inserta.certificado", filter);
        return filter;
    }
    
    public void insertUsuarioCertificado(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificado.inserta.usuario.certificado", filter);
    }
	
    public void transmiteCertificado(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificado.transmite.certificado", filter);
    }
    
	public Certificado getCertificadoById(HashUtil<String, Object> filter)throws Exception {
		return (Certificado) getSqlMapClientTemplate().queryForObject("certificado.certificado.element", filter);	
	}
	
	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter)throws Exception {
		return (UsuarioFormato) getSqlMapClientTemplate().queryForObject("formato.usuario_formato.element", filter);
	}

	public Solicitud getSolicitudById(HashUtil<String, Object> filter)throws Exception {
		return (Solicitud) getSqlMapClientTemplate().queryForObject("certificado.solicitud.element", filter);
	}
	
	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter)throws Exception {
		return (Solicitante) getSqlMapClientTemplate().queryForObject("formato.solicitante.element", filter);
	}
	
    public AdjuntoRequeridoCertificado getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception {
        return (AdjuntoRequeridoCertificado) getSqlMapClientTemplate().queryForObject("certificado.adjunto_requerido.element", filter);
    }

	public void updateRepresentante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificado.actualiza.representante_legal", filter);
    }
	
	public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("certificado.actualiza.cargoDeclarante", filter);
    }
	
	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception {
		return (Integer)getSqlMapClientTemplate().queryForObject("certificado.adjuntos_requeridos.count", filter);
	}
	
	
	// RESOLUTOR
	public void asignarEvaluador(Long ordenId, Integer usuarioId, Integer rol) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("usuarioId", usuarioId);
        filter.put("rol", rol);
        getSqlMapClientTemplate().queryForObject("certificado.asignarEvaluador", filter);
    }
    
	public void iniciarEvaluacionCertificado(Long ordenId) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        getSqlMapClientTemplate().queryForObject("certificado.iniciarEvaluacion", filter);
	}
	
	public HashUtil<String, Object> insertBorradorResolutivoDr(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("certificado.insertar_borrador", filter);
		return filter;
	}
    
    public void deleteBorradorResolutivoDr(HashUtil<String, Object> filter) throws Exception {
    	getSqlMapClientTemplate().queryForObject("certificado.eliminar_borrador", filter);
    }
    
    public void transmiteResolutor(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("certificado.transmite_borrador", filter);
    }
    
    public Solicitante getSolicitanteDRBorradorDetail(HashUtil<String, Object> filter)throws Exception{
		return (Solicitante) getSqlMapClientTemplate().queryForObject("certificado.resolutivo.borrador.solicitante.element", filter);
	}
    
    public Solicitante getSolicitanteDRDetail(HashUtil<String, Object> filter)throws Exception{
		return (Solicitante) getSqlMapClientTemplate().queryForObject("certificado.resolutivo.solicitante.element", filter);
	}
    
	public Certificado getCertificadoDRById(HashUtil<String, Object> filter)throws Exception {
		return (Certificado) getSqlMapClientTemplate().queryForObject("certificado.resolutivo.certificado.element", filter);	
	}
	
}
