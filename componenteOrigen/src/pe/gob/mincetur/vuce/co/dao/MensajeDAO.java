package pe.gob.mincetur.vuce.co.dao;

import java.util.HashMap;
import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Mensaje;

public interface MensajeDAO {

	public Mensaje loadMensaje(HashUtil<String, Object> filter) throws Exception;
	
	public Mensaje loadMensajeExtranet(HashUtil<String, Object> filter) throws Exception;
	
	public String loadNotificacion(HashUtil<String, Object> filter) throws Exception;
	
	public void updateMensajeLeido(HashUtil<String, Object> filter) throws Exception;
	
	public void inactivaMensaje(HashUtil<String, Object> filter) throws Exception;

	HashMap<String, Integer> getNotificacion(Integer idTransmision) throws Exception;

	List<String> getNotificacionesFromMensaje(Integer idNotificacion) throws Exception;
	
}
