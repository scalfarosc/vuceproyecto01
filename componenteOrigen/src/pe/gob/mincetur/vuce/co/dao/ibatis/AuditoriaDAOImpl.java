package pe.gob.mincetur.vuce.co.dao.ibatis;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.AuditoriaDAO;
import pe.gob.mincetur.vuce.co.domain.Auditoria;

public class AuditoriaDAOImpl extends SqlMapClientDaoSupport implements AuditoriaDAO  {

	public void insertInicioSesion(Auditoria auditoria) throws Exception {
		getSqlMapClientTemplate().insert("auditoria.sesion.insert", auditoria);		
	}
 
	public void insertInicioSesionContingencia(Auditoria auditoria) throws Exception {
		getSqlMapClientTemplate().insert("auditoria.sesion.contingencia.insert", auditoria);		
	}
	
	public void insertFinSesionContingencia(Auditoria auditoria) throws Exception {
		getSqlMapClientTemplate().insert("auditoria.fin.sesion.contingencia.insert", auditoria);		
	}
	
	public void insertAccesoSolicitud(Auditoria auditoria) throws Exception {
		getSqlMapClientTemplate().insert("auditoria.accesoSolicitud.insert", auditoria);		
	}

	public void insertAccesoSuce(Auditoria auditoria) throws Exception {
		getSqlMapClientTemplate().insert("auditoria.accesoSuce.insert", auditoria);		
	}

	public void insertAccesoDr(Auditoria auditoria) throws Exception {
		getSqlMapClientTemplate().insert("auditoria.accesoDr.insert", auditoria);		
	}

}
