package pe.gob.mincetur.vuce.co.dao.ibatis.util;

import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;

import org.jlis.core.util.Util;

import com.ibatis.sqlmap.client.extensions.ParameterSetter;
import com.ibatis.sqlmap.client.extensions.ResultGetter;
import com.ibatis.sqlmap.client.extensions.TypeHandlerCallback;

public class DateTimeHandler implements TypeHandlerCallback {

	public Object getResult(ResultGetter getter) throws SQLException {
        String value = getter.getString();
        if (getter.wasNull()) {
            return null;
        }
        return value;
	}

	public void setParameter(ParameterSetter setter, Object parameter) throws SQLException {
        if (parameter == null) {
            setter.setNull(Types.DATE);
        } else {
            try {
                setter.setTimestamp(Util.getTimestampObject(parameter.toString(), "dd/MM/yyyy"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
	}

	public Object valueOf(String s) {
		return Util.getDate(s);
	}
}
