package pe.gob.mincetur.vuce.co.dao.resolutor;

import java.util.Map;

import org.jlis.core.list.MessageList;
import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.AdjuntoCertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;


/**
 * Interfaz de persistencia de Certificado de Origen
 * @author Ricardo Montes
 */

public interface ResolutorDAO {

	public void designaEvaluador(Map<String,Object> parametros) throws Exception;

    public CertificadoOrigen aceptaDesignacionTCE(CertificadoOrigen co) throws Exception;

    public CertificadoOrigen rechazaDesignacionTCE(CertificadoOrigen co) throws Exception;

	public DeclaracionJurada evaluadorDJCalifica(DeclaracionJurada dj) throws Exception;
	
	public DeclaracionJurada evaluadorDJCalificaApruebaTransmite(DeclaracionJurada dj) throws Exception;

	public DeclaracionJurada evaluadorDJNoCalifica(DeclaracionJurada dj) throws Exception;
	
	public DeclaracionJurada evaluadorDJNoCalificaDeniegaTransmite(DeclaracionJurada dj) throws Exception;

	public void registrarBorradorDr(CertificadoOrigenDR coDR) throws Exception;
	
	public HashUtil<String, Object> confirmarBorradorDr(HashUtil<String, Object> parametros) throws Exception;

	public void generarBorradorDr(Map<String,Object> parametros) throws Exception;

	public void eliminarBorradorDr(Map<String,Object> parametros) throws Exception;

	public void actualizarObsBorradorDr(Map<String,Object> parametros) throws Exception;

	public CertificadoOrigenDR getCODrResolutorById(Map<String,Object> parametros) throws Exception;

    public void cargarAdjuntoCODR(AdjuntoCertificadoOrigenDR adjCODR) throws Exception;

	public DR getDRById(Long drId, Integer sdr) throws Exception;

    public void cerrarSuce(Map<String,Object> parametros) throws Exception;

	public void denegarSolicitud(Map<String, Object> filtros) throws Exception;

    public void actualizarDjXEvaluador(Map<String,Object> parametros) throws Exception;

    public void confirmarFinDJEval(Map<String,Object> parametros) throws Exception;

    public String obtenerTipoDr(Long drId, Integer sdr) throws Exception;

    public String obtenerTipoDrBorrador(Long borradorDrId) throws Exception;

    public boolean drEsRectificacion(Long drId, Integer sdr) throws Exception;

    public DR getDRBorradorById(Long drBorradorId) throws Exception;

    public CertificadoOrigenDR getCODrRectificacionById(Map<String,Object> parametros) throws Exception;

    public Integer registrarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception;

    public void actualizarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception;

    public void enviarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception;

    public void eliminarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception;

    public void cargarDatosFirmaAdjuntoCODR(HashUtil<String, Object> filter) throws Exception;

	public String subsanacionNoAtendida(Long suceId) throws Exception;

    public void confirmarDRFinFirma(Long drId, Long sdr) throws Exception;

	public String habilitarBtnFirma(Long suceId, Long drId, Long sdr) throws Exception;

    public int numDJsRechazadas(Map<String,Object> parametros) throws Exception;

    public String mostrarBtnFinalizacion(Map<String,Object> parametros) throws Exception;
}
