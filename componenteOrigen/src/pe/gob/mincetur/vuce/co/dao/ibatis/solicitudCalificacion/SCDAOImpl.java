package pe.gob.mincetur.vuce.co.dao.ibatis.solicitudCalificacion;

import java.util.List;
import java.util.Map;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.solicitudCalificacion.SCDAO;
import pe.gob.mincetur.vuce.co.domain.Solicitante;
import pe.gob.mincetur.vuce.co.domain.Solicitud;
import pe.gob.mincetur.vuce.co.domain.UsuarioFormato;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.AdjuntoRequeridoSC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SC;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCAcuerdo;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCDetalleDj;
import pe.gob.mincetur.vuce.co.domain.solicitudCalificacion.SCMaterial;

public class SCDAOImpl extends SqlMapClientDaoSupport implements SCDAO{
	
	public HashUtil<String, Object> insertSC(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("sc.inserta.sc", filter);
        return filter;
    }
    
    public void insertUsuarioSC(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("sc.inserta.usuario.sc", filter);
    }
	
    public void transmiteSC(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("sc.transmite.sc", filter);
    }
    
	public void updateSC(SC sc) throws Exception {		
		getSqlMapClientTemplate().queryForObject("sc.update.sc", sc);		
	}
    
	public SC getSCById(HashUtil<String, Object> filter)throws Exception{
		return (SC) getSqlMapClientTemplate().queryForObject("sc.sc.element", filter);	
	}
	
	public SCMaterial getSCMaterialById(HashUtil<String, Object> filter)throws Exception{
		return (SCMaterial) getSqlMapClientTemplate().queryForObject("sc.sc_material.element", filter);	
	}

	public UsuarioFormato getUsuarioFormatoById(HashUtil<String, Object> filter)throws Exception{
		return (UsuarioFormato) getSqlMapClientTemplate().queryForObject("formato.usuario_formato.element", filter);
	}

	public Solicitud getSolicitudById(HashUtil<String, Object> filter)throws Exception{
		return (Solicitud) getSqlMapClientTemplate().queryForObject("sc.solicitud.element", filter);
	}
	
	public Solicitante getSolicitanteDetail(HashUtil<String, Object> filter)throws Exception{
		return (Solicitante) getSqlMapClientTemplate().queryForObject("formato.solicitante.element", filter);
	}

	/*public void updateMaterial(SCMaterial obj){
		getSqlMapClientTemplate().queryForObject("sc.update.material", obj);	
	}*/
	
    public void updateRepresentante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("sc.actualiza.representante_legal", filter);
    }
    
    public void insertAcuerdoPais(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("sc.acuerdo.inserta", filter);
    }
    
    public void deleteAcuerdoPais(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("sc.acuerdo.elimina", filter);
    }
    
	public List<SCMaterial> getMaterialList(HashUtil<String, Object> filter) throws Exception{
		return getSqlMapClientTemplate().queryForList("sc.sc_material.lista", filter);
	}
	
    public void updateCargoDeclarante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("sc.actualiza.cargoDeclarante", filter);
    }
    
    public AdjuntoRequeridoSC getAdjuntoRequeridoById(HashUtil<String, Object> filter) throws Exception {
        return (AdjuntoRequeridoSC) getSqlMapClientTemplate().queryForObject("sc.adjunto_requerido.element", filter);
    }
    
	public Integer getAdjuntoRequeridoCount(HashUtil<String, Object> filter) throws Exception {
		return (Integer)getSqlMapClientTemplate().queryForObject("sc.adjuntos_requeridos.count", filter);
	}
    
	
	// RESOLUTOR
	public void asignarEvaluador(Long ordenId, Integer usuarioId, Integer rol) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("usuarioId", usuarioId);
        filter.put("rol", rol);
        getSqlMapClientTemplate().queryForObject("sc.asignarEvaluador", filter);
    }
    
   public void iniciarEvaluacionCalificacion(Long ordenId) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        getSqlMapClientTemplate().queryForObject("sc.iniciarEvaluacion", filter);
	}
	
	public SC insertBorradorResolutivoDr(SC sc) throws Exception {
		getSqlMapClientTemplate().queryForObject("sc.resolutivo.borrador.insert", sc);
		return sc;
	}
    
    public void deleteBorradorResolutivoDr(SC sc) throws Exception {
    	getSqlMapClientTemplate().queryForObject("sc.resolutivo.borrador.delete", sc);
    }
    
    public Solicitante getSolicitanteDRBorradorDetail(HashUtil<String, Object> filter)throws Exception{
		return (Solicitante) getSqlMapClientTemplate().queryForObject("sc.resolutivo.borrador.solicitante.element", filter);
	}
    
    public Solicitante getSolicitanteDRDetail(HashUtil<String, Object> filter)throws Exception{
		return (Solicitante) getSqlMapClientTemplate().queryForObject("sc.resolutivo.solicitante.element", filter);
	}
    
	public SC getSCDRById(HashUtil<String, Object> filter)throws Exception{
		return (SC) getSqlMapClientTemplate().queryForObject("sc.resolutivo.element", filter);	
	}
	
	public SCMaterial getSCMaterialDRById(HashUtil<String, Object> filter)throws Exception{
		return (SCMaterial) getSqlMapClientTemplate().queryForObject("sc.resolutor.material.element", filter);	
	}
	
	public void updateSCDR(SC sc) throws Exception {		
		getSqlMapClientTemplate().queryForObject("sc.resolutor.update", sc);		
	}
    
	public SCAcuerdo getSCAcuerdoDRById(HashUtil<String, Object> filter)throws Exception{
		return (SCAcuerdo) getSqlMapClientTemplate().queryForObject("sc.resolutor.acuerdo.element", filter);	
	}
	
	public void updateAcuerdoDR(SCAcuerdo obj){
		getSqlMapClientTemplate().queryForObject("sc.resolutor.acuerdo.update", obj);	
	}
	
    public void transmiteResolutor(HashUtil<String, Object> filter) throws Exception{
        getSqlMapClientTemplate().queryForObject("sc.resolutor.transmite", filter);
    }
    
	public List<SCAcuerdo> getAcuerdoList(HashUtil<String, Object> filter) throws Exception{
		return getSqlMapClientTemplate().queryForList("sc.resolutor.acuerdo.lista", filter);
	}
	
    public SCDetalleDj getDatosDJParaCalificacion(HashUtil<String, Object> filter) throws Exception {
    	return (SCDetalleDj) getSqlMapClientTemplate().queryForObject("sc.datos.dj", filter);
    }
	
}
