package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.Date;
import java.util.Random;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.bean.UsuarioCO;
import pe.gob.mincetur.vuce.co.dao.UsuariosDAO;
import pe.gob.mincetur.vuce.co.domain.Usuario;

public class UsuariosDAOImpl extends SqlMapClientDaoSupport implements UsuariosDAO {
    
    public void testGrabacionUsuarioContextIdentifier(HashUtil<String, Object> filter) {
        Random r = new Random();
        r.setSeed(new Date().getTime());
        try {
            Thread.sleep(Math.abs(r.nextInt()) % 10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getSqlMapClientTemplate().queryForObject("vuce.grabarEnTablaTestContextIdentifier", filter);
    }
    
    public int insertUsuario(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario.nuevo.insert", filter);
        return filter.getInt("usuario_id");
    }
    
    /*public int insertUsuarioEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario.empresaExterna.nuevo.insert", filter);
        return filter.getInt("usuario_id");
    }
    */
    public int insertEmpresa(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.nuevo.insert", filter);
        return filter.getInt("empresa_id");
    }
    
    public int insertEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.externa.nuevo.insert", filter);
        return filter.getInt("empresa_externa_id");
    }
    
    public void insertRepresentanteLegal(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.rep_legal.nuevo.insert", filter);
    }
    
    public void inactivarRepresentantesLegales(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.rep_legal.inactivar.insert", filter);
    }
    
    public void updateUsuario(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario.update", filter);
    }
    
    /*public void updateUsuarioEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario.empresaExterna.update", filter);
    }
    */
    public void updateEmpresa(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.update", filter);
    }
    
    public void updateEmpresaExterna(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.externa.update", filter);
    }
    
    public int existeUsuarioSol(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario_sol.existe", filter);
        return filter.getInt("existe");
    }
    
    public int existeUsuarioExtranet(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario_extranet.existe", filter);
        return filter.getInt("existe");
    }
    
    public int existeUsuarioDNI(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario_dni.existe", filter);
        return filter.getInt("existe");
    }
    
    public boolean existeEmpresa(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("empresa.existe", filter);
        return filter.getInt("existe") == 1;
    }
    
    public void eliminarRolesUsuario(int idUsuario) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("idUsuario", idUsuario);
        filter.put("idRol", -1);
        getSqlMapClientTemplate().queryForObject("usuario.eliminarRoles", filter);
    }
    
    public void registrarRolUsuario(int idUsuario, String rol) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("idUsuario", idUsuario);
    	filter.put("rol", rol);
        getSqlMapClientTemplate().queryForObject("usuario.registrarRol", filter);
    }
    
    public boolean usuarioTieneTramites(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("usuario.tieneTramites", filter);
        return filter.getInt("tiene") == 1;
    }
    
    public UsuarioCO loadUsuarioCOById(int idUsuario) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idUsuario", idUsuario);
        return (UsuarioCO)getSqlMapClientTemplate().queryForObject("usuarioCO.element", filter);
    }
    
    public Usuario loadUsuarioById(int idUsuario) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idUsuario", idUsuario);
        return (Usuario)getSqlMapClientTemplate().queryForObject("usuario.element", filter);
    }
    
    public Usuario loadEmpresaById(int idUsuario) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idUsuario", idUsuario);
        return (Usuario)getSqlMapClientTemplate().queryForObject("usuario.empresa.element", filter);
    }
    
	public UsuarioCO loadUsuarioVUCEByRucAndCodigoSol(String ruc, String codigoUsuario) throws Exception {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("rucUsuario", ruc);
		filter.put("codigoUsuario", codigoUsuario);
		return (UsuarioCO)getSqlMapClientTemplate().queryForObject("usuario.get.usuarioid", filter);
	}
	
}
