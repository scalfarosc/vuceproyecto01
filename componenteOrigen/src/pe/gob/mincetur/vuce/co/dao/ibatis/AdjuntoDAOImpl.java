package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.List;

import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.AdjuntoDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoFormatoCertificado;
import pe.gob.mincetur.vuce.co.procesodr.domain.AdjuntoMensaje;

public class AdjuntoDAOImpl extends SqlMapClientDaoSupport implements AdjuntoDAO {

    public List<AdjuntoFormatoCertificado> loadList(int orden, int mto) throws Exception {
        HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
        filter.put("orden", orden);
        filter.put("mto", mto);
        return getSqlMapClientTemplate().queryForList("adjunto.orden.list", filter);
    }

    public List<AdjuntoFormatoCertificado> loadModifSuceList(int mensajeId) throws Exception {
        HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
        filter.put("mensaje", mensajeId);
        return getSqlMapClientTemplate().queryForList("adjunto.modif_suce.list", filter);
    }

    public void insertAdjunto(Adjunto adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.insert", adjunto);
    }

    public void insertAdjuntoSuce(Adjunto adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.suce.insert", adjunto);
    }

    public void insertAdjuntoResolutorDR(AdjuntoFormatoCertificado adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.resolutor.dr.insert", adjunto);
    }

    public void updateAdjuntos(List<Adjunto> adjuntos) throws Exception {
        for (Adjunto adjunto : adjuntos) {
            this.updateAdjunto(adjunto);
        }
    }

    public int updateAdjunto(Adjunto adjunto) throws Exception {
        return getSqlMapClientTemplate().update("adjunto.update", adjunto);
    }

    public void eliminarAdjuntoFormato(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("adjunto.formatoCertificadoOrigen.delete", filter);
    }

    public void eliminarAdjuntoById(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("adjunto.delete", filter);
    }

    public Adjunto loadAdjunto(HashUtil<String, Object> filter) throws Exception{
    	return (Adjunto)getSqlMapClientTemplate().queryForObject("buzon.mensaje.adjunto.element", filter);
    }

    public void insertAdjuntoResolutorBorradorDR(Adjunto adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.resolutor.insert", adjunto);
    }

    public Adjunto loadAdjuntoDR(HashUtil<String, Object> filter) throws Exception{
    	return (Adjunto)getSqlMapClientTemplate().queryForObject("adjunto.adjunto.element", filter);
    }

    public void eliminarAdjuntoCalificacionById(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("adjunto.calificacion.delete", filter);
    }

    public void eliminarAdjuntoCertificadoById(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("adjunto.certificado.delete", filter);
    }

    public void insertAdjuntoNotificacion(Adjunto adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.notificacion.insert", adjunto);
    }

    public void insertAdjuntoResolutorDR(AdjuntoMensaje adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.adjunto_x_mensaje.insert", adjunto);
    }

    public void insertAdjuntoSubsanacion(AdjuntoMensaje adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.adjunto_x_mensaje.insert", adjunto);
    }

    public void insertAdjuntoFactura(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.factura." + filter.getString("formato").trim().toLowerCase() + ".insert", filter);
    }

    public void eliminarAdjuntoFactura(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("adjunto.factura." + filter.getString("formato").trim().toLowerCase() + ".delete", filter);
    }

    public void insertAdjuntoProductor(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.productor.insert", filter);
    }

    public void eliminarAdjuntoProductor(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("adjunto.productor.delete", filter);
    }
    //20140825_JMC_Intercambio_Empresa
    public List<Adjunto> loadListForTransmision(String keyQuery, int idTransmision) throws Exception {
        HashUtil<String, Integer> filter = new HashUtil<String, Integer>();
        filter.put("idTransmisionEmpresa", idTransmision);
        return getSqlMapClientTemplate().queryForList(keyQuery, filter);
    }
    //20140825_JMC_Intercambio_Empresa
    public void insertAdjuntoForTransmision(Adjunto adjunto) throws Exception {
        getSqlMapClientTemplate().insert("adjunto.transmision.insert", adjunto);
    }
    //20140825_JMC_Intercambio_Empresa
    public int insertAdjuntoForTransmisionEmpresa(Adjunto adjunto) throws Exception {
    	
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("tipo", adjunto.getTipo());
        filter.put("nombre", adjunto.getNombre());
        filter.put("archivo", adjunto.getArchivo());
        filter.put("estado", null);
        filter.put("mensajeId", null);
        filter.put("ordenId", null);
        filter.put("formatoId", null);
        filter.put("adjuntoRequerido", null);
        filter.put("vcId", null);
        filter.put("mto", null);
        filter.put("drId", null);
        filter.put("sdr", null);
        filter.put("modificable", null);
        filter.put("notificacionId", null);
        filter.put("drBorradorId", null);
        filter.put("esDetalle", null);
        filter.put("fechaCargadoEnVuce", null);
        filter.put("esAdjuntoVuce", null);        
        filter.put("id", null);
        
        getSqlMapClientTemplate().insert("adjunto.transmision.empresa.insert", filter);
        return Util.integerValueOf(filter.get("id"));   
    }   
    
	public void insertTransmisionForAdjunto(int transmisionEmpresaId,
			int idAdjunto) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("transmisionEmpresaId", transmisionEmpresaId);
        filter.put("idAdjunto", idAdjunto);
		getSqlMapClientTemplate().insert("empresa.transmision.x.adjunto", filter);		
	}
	
	public void insertNombreAdjuntoForTransmision(int mct001TmpId, AdjuntoFormato adjunto){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("mct001TmpId", mct001TmpId);	    
	    filter.put("nombreAdjunto", adjunto.getNombre());
	    filter.put("secuencia", adjunto.getSecuencia());
		getSqlMapClientTemplate().insert("adjunto.certificado.transmision.x.adjunto.insert", filter);		
		
	}
	public void insertNombreAdjuntoDJForTransmision(int mct005TmpId, AdjuntoFormato adjunto){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("mct005TmpId", mct005TmpId);	    
	    filter.put("nombreAdjunto", adjunto.getNombre());
	    filter.put("secuencia", adjunto.getSecuencia());
	    filter.put("adjuntoRequerido", adjunto.getAdjuntoRequerido());
		getSqlMapClientTemplate().insert("adjunto.declaracion.transmision.x.adjunto.insert", filter);		
		
	}	
	
	public void insertNombreAdjuntoDJMaterialForTransmision(int mct005TmpId, int secuenciaMaterial, Adjunto adjunto){
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("mct005TmpId", mct005TmpId);	    
	    filter.put("nombreAdjunto", adjunto.getNombre());
	    filter.put("secuencia", adjunto.getId());
	    filter.put("secuenciaMaterial", secuenciaMaterial);
		getSqlMapClientTemplate().insert("adjunto.declaracion.material.transmision.x.adjunto.insert", filter);		
		
	}
 
}
