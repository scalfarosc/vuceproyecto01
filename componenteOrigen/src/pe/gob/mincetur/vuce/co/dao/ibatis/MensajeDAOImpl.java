package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.HashMap;
import java.util.List;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.MensajeDAO;
import pe.gob.mincetur.vuce.co.domain.Mensaje;

public class MensajeDAOImpl extends SqlMapClientDaoSupport implements MensajeDAO {

	public Mensaje loadMensaje(HashUtil<String, Object> filter) throws Exception {
		return (Mensaje)getSqlMapClientTemplate().queryForObject("buzon.mensaje.element", filter);
	}
	
	public Mensaje loadMensajeExtranet(HashUtil<String, Object> filter) throws Exception {
		return (Mensaje)getSqlMapClientTemplate().queryForObject("buzon.mensaje.extranet.element", filter);
	}
	
    public String loadNotificacion(HashUtil<String, Object> filter) throws Exception {
        return (String)getSqlMapClientTemplate().queryForObject("buzon.notificacion.numero", filter);
    }
    
	public void updateMensajeLeido(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("buzon.leido.update", filter);     
    }
	
	public void inactivaMensaje(HashUtil<String, Object> filter) throws Exception {
		getSqlMapClientTemplate().queryForObject("inactiva_mensaje", filter);     
    }
	
	public HashMap<String, Integer> getNotificacion(Integer idTransmision) throws Exception{
		return (HashMap<String,Integer>)getSqlMapClientTemplate().queryForObject("notificacion.mensaje", idTransmision); 
	}
	
	public List<String> getNotificacionesFromMensaje(Integer idNotificacion) throws Exception{
		return (List<String>)getSqlMapClientTemplate().queryForList("mensaje.notificaciones.elements", idNotificacion); 
	}

}
