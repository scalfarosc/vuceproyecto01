package pe.gob.mincetur.vuce.co.dao;

import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Transmision;

public interface TransmisionDAO {
	
	public List<Transmision> loadList(HashUtil<String, Integer> filter);

	public String procesarTransaccionEmpresa(HashUtil<String, Integer> filter);
	
	public int registrarTransmision(HashUtil<String, Object> filter) throws Exception ;
	
	public Transmision loadTransmision(int idVC);
	
	public String procesarTransaccionDeclaracionJuradaEmpresa(HashUtil<String, Integer> filter);
	
	public void updateEstadoTransmision(Transmision transmision) throws Exception;
	
	public void updateEstadoTransmisionEmpresa(Transmision transmision) throws Exception;

	// JMC 02/12/2016 ALIANZA PACIFICO
	public int registrarTransmisionIOP(HashUtil<String, Object> filter) throws Exception;

    public List<Transmision> loadListIOP(HashUtil<String, Integer> filter);
}
