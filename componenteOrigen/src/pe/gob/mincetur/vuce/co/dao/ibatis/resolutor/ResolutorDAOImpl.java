package pe.gob.mincetur.vuce.co.dao.ibatis.resolutor;

import java.util.HashMap;
import java.util.Map;

import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.resolutor.ResolutorDAO;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.procesodr.domain.DR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.AdjuntoCertificadoOrigenDR;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;


/**
 * Clase implementadora de persistencia de Certificado de Origen
 * @author Ricardo Montes
 */

public class ResolutorDAOImpl extends SqlMapClientDaoSupport implements ResolutorDAO {

	/**
	 * Registra la aceptación de la designación
	 */
    public void designaEvaluador(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.designa.eval", parametros);
        return;
    }

	/**
	 * Registra la aceptación de la designación TCE
	 */
    public CertificadoOrigen aceptaDesignacionTCE(CertificadoOrigen co) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.acepta.designacion", co);
        return co;
    }

    /**
	 * Registra el rechazo de la designación TCE
	 */
    public CertificadoOrigen rechazaDesignacionTCE(CertificadoOrigen co) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.rechaza.designacion", co);
        return co;
    }

    /**
     * Registra la calificación del evaluador
     */
    public DeclaracionJurada evaluadorDJCalifica(DeclaracionJurada dj) throws Exception {
        getSqlMapClientTemplate().queryForObject("dj.evaluador.califica", dj);
        return dj;
    }

    /**
     * Registra la calificación del evaluador Aprueba Tramniste
     */
    public DeclaracionJurada evaluadorDJCalificaApruebaTransmite(DeclaracionJurada dj) throws Exception {
        getSqlMapClientTemplate().queryForObject("dj.evaluador.califica_transmite", dj);
        return dj;
    }
    
    /**
     * Registra la no calificación del evaluador
     */
    public DeclaracionJurada evaluadorDJNoCalifica(DeclaracionJurada dj) throws Exception {
        getSqlMapClientTemplate().queryForObject("dj.evaluador.no.califica", dj);
        return dj;
    }
    
    /**
     * Registra la no calificación del evaluador, deniega  y transmite
     */
    public DeclaracionJurada evaluadorDJNoCalificaDeniegaTransmite(DeclaracionJurada dj) throws Exception {
        getSqlMapClientTemplate().queryForObject("dj.evaluador.no.califica_transmite", dj);
        return dj;
    }

	/**
	 * Inserta el registro de un dr en borrador
	 */
	public void registrarBorradorDr(CertificadoOrigenDR coDR) throws Exception {
		getSqlMapClientTemplate().queryForObject("dr.borrador.registra", coDR);
	}

	/**
	 * Confirma el registro de un dr en borrador
	 */
	public HashUtil<String, Object> confirmarBorradorDr(HashUtil<String, Object> parametros) throws Exception { //2018.06.06 JMC Produccion Controlada Alianza Pacifico
		getSqlMapClientTemplate().queryForObject("resolutor.dr.borrador.confirmar", parametros);
		return parametros;
	}

	/**
	 * Genera el borrador de la DR
	 */
    public void generarBorradorDr(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.dr.generar_borrador", parametros);
        return;
    }

	/**
	 * Elimina el borrador de la DR
	 */
    public void eliminarBorradorDr(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.dr.eliminar_borrador", parametros);
        return;
    }

	/**
	 * Actualiza la observacion del borrador de la DR
	 */
    public void actualizarObsBorradorDr(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor." + parametros.get("formato").toString().trim().toLowerCase() + ".dr.actualizar.observacion", parametros);
        return;
    }

	/**
	 * Registra la aceptación de la designación TCE
	 */
    public CertificadoOrigenDR getCODrResolutorById(Map<String,Object> parametros) throws Exception {
        return (CertificadoOrigenDR) getSqlMapClientTemplate().queryForObject("resolutor.co." + parametros.get("formato").toString() + ".dr.element", parametros);
    }

	/**
	 * Registra un adjunto de la DR
	 */
    public void cargarAdjuntoCODR(AdjuntoCertificadoOrigenDR adjCODR) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.dr.adjunto.registro", adjCODR);
    }

	public DR getDRById(Long drId, Integer sdr) throws Exception {
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
		filter.put("drId", drId);
		filter.put("sdr", sdr);
		return (DR)getSqlMapClientTemplate().queryForObject("dr.element", filter);
	}

	/**
	 * Finaliza una suce
	 */
    public void cerrarSuce(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.suce.cerrar", parametros);
        return;
    }

	public void denegarSolicitud(Map<String, Object> filtros) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.denegar.solicitud", filtros);
        return;
	}


	/**
	 * Metodo para que el evaluador actualice los datos de la dj
	 */
    public void actualizarDjXEvaluador(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("dj.evaluador.modifica", parametros);
        return;
    }

	/**
	 * Finaliza la evaluacion de DJs
	 */
    public void confirmarFinDJEval(Map<String,Object> parametros) throws Exception {
        getSqlMapClientTemplate().queryForObject("dj.evaluador.confirma.fin_eval", parametros);
        return;
    }

    public String obtenerTipoDr(Long drId, Integer sdr) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
    	filter.put("drId", drId);
		filter.put("sdr", sdr);
		return (String)getSqlMapClientTemplate().queryForObject("dr.tipoDr", filter).toString();
    }

    public String obtenerTipoDrBorrador(Long borradorDrId) throws Exception {
    	HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("borradorDrId", borradorDrId);
    	return (String)getSqlMapClientTemplate().queryForObject("dr.borrador.tipoDr", filter);
    }

    public boolean drEsRectificacion(Long drId, Integer sdr) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("esRectificacion", null);
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        getSqlMapClientTemplate().queryForObject("administracionEntidad.resolutor.esRectificacion", filter);
        return filter.get("esRectificacion").toString().equalsIgnoreCase("S");
    }

    public DR getDRBorradorById(Long drBorradorId) throws Exception {
	    HashUtil<String, Object> filter = new HashUtil<String, Object>();
	    filter.put("drBorradorId", drBorradorId);
	    return (DR)getSqlMapClientTemplate().queryForObject("administracionEntidad.resolutor.drBorrador.element", filter);
	}

    public CertificadoOrigenDR getCODrRectificacionById(Map<String,Object> parametros) throws Exception {
        return (CertificadoOrigenDR) getSqlMapClientTemplate().queryForObject("resolutor.co." + parametros.get("formato").toString() + ".dr.rectificacion.element", parametros);
    }

    public Integer registrarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.supervisor.registrarNotificacionSubsanacionOrden", filter);
        return filter.getInt("notificacionId");
    }

    public void actualizarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.actualizarNotificacionSubsanacionOrden", filter);
    }

    public void enviarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.confirmarNotificacionSubsanacionOrden", filter);
    }

    public void eliminarNotificacionSubsanacionOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.evaluador.eliminarNotificacionSubsanacionOrden", filter);
    }

	/**
	 * Registra un datos de la firma adjunta de la DR
	 */
    public void cargarDatosFirmaAdjuntoCODR(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("resolutor.dr.datos.firma.adjunto.registro", filter);
    }

    /**
     * Devuelve 0 si no hay subsanaciones por atender y 1 si es que existen subsanaciones pendientes
     *
     * @param suceId
     * @return
     * @throws Exception
     */
	public String subsanacionNoAtendida(Long suceId) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		//filtros.put("result",null);
		filtros.put("suce_id",suceId);
		getSqlMapClientTemplate().queryForObject("subsanacion.no.atendida", filtros);
		return filtros.get("result").toString();
	}

	/**
	 * Registra la trazabilidad antes de finalizar el DR
	 */
    public void confirmarDRFinFirma(Long drId, Long sdr) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		filtros.put("dr_id",drId);
		filtros.put("sdr",sdr);
        getSqlMapClientTemplate().queryForObject("resolutor.dr.confirma.fin.firma", filtros);
    }

    /**
     * Devuelve N si no se debe mostrar el botón de impresión y S si se debe mostrar dicho botón
     *
     * @param suceId
     * @return
     * @throws Exception
     */
	public String habilitarBtnFirma(Long suceId, Long drId, Long sdr) throws Exception {
		Map<String, Object> filtros = new HashMap<String, Object>();
		//filtros.put("result",null);
		filtros.put("suce_id", suceId);
		filtros.put("dr_id", drId);
		filtros.put("sdr", sdr);
		getSqlMapClientTemplate().queryForObject("certificado.firma.habilitar", filtros);
		return filtros.get("result").toString();
	}

	/**
	 * Devuelve el numero de DJs que el evaluador haya indicado que no califican
	 * 
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
    public int numDJsRechazadas(Map<String,Object> parametros) throws Exception {
    	return (Integer) getSqlMapClientTemplate().queryForObject("resolutor.co." + parametros.get("formato").toString().trim() + ".num_dj.rechazadas", parametros);
    }

	/**
	 * Devuelve un indicador para mostrar u ocultar el boton de finalización de la SUCE
	 * 
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
    public String mostrarBtnFinalizacion(Map<String,Object> parametros) throws Exception {
		getSqlMapClientTemplate().queryForObject("administracionEntidad.resolutor.mostrarBtnFinalizacion", parametros);
		return parametros.get("result").toString();
    }

    
}
