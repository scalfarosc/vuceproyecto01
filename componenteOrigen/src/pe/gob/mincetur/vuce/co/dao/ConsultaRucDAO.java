package pe.gob.mincetur.vuce.co.dao;

import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanRso;
import ConsultaRuc.BeanSpr;
import ConsultaRuc.BeanT1144;
import ConsultaRuc.BeanT1150;
import ConsultaRuc.BeanT362;

public interface ConsultaRucDAO {
    
	public BeanDdp loadDatosPrincipales(String ruc) throws Exception;
	
    public void insertDatosPrincipales(String ruc, BeanDdp datosPrincipales) throws Exception;
    
	public BeanDds loadDatosSecundarios(String ruc) throws Exception;
	
    public void insertDatosSecundarios(String ruc, BeanDds datosSecundarios) throws Exception;    
    
	public BeanT1144 loadDatosT1144(String ruc) throws Exception;
	
    public void insertDatosT1144(String ruc, BeanT1144 datosT1144) throws Exception;  
    
	public BeanT362 [] loadDatosT362(String ruc) throws Exception;
	
    public void insertDatosT362(String ruc, BeanT362 [] datosT362) throws Exception;  
    
	public BeanRso [] loadRepLegales(String ruc) throws Exception;
	
    public void insertRepLegales(String ruc, BeanRso [] repLegales) throws Exception;
    
	public String loadDomicilioLegal(String ruc) throws Exception;
	
    public void insertDomicilioLegal(String ruc, String domicilioLegal) throws Exception;
	
	public BeanSpr [] loadEstAnexos(String ruc) throws Exception;
	
    public void insertEstAnexos(String ruc, BeanSpr [] estAnexos) throws Exception;
    
	public BeanT1150 [] loadDatosT1150(String ruc) throws Exception;
	
    public void insertDatosT1150(String ruc, BeanT1150 [] datosT1150) throws Exception;      
    
    public Integer loadModoConsulta() throws Exception;
    
    public void insertTrazabilidad(String ruc, String metodo, Integer resultadoConsulta, Integer modoOperacion) throws Exception;
    
}