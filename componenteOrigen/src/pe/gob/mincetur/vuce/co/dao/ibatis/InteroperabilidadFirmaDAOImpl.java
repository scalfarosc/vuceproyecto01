package pe.gob.mincetur.vuce.co.dao.ibatis;

import java.util.List;

import org.jlis.core.util.HashUtil;
import org.jlis.core.util.Util;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.InteroperabilidadFirmaDAO;
import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.firma.BienOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.CodExporter;
import pe.gob.mincetur.vuce.co.domain.firma.EhCert;
import pe.gob.mincetur.vuce.co.domain.firma.EnteComercio;
import pe.gob.mincetur.vuce.co.domain.firma.FacturaOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.FacturaTercerOpOrigen;
import pe.gob.mincetur.vuce.co.domain.firma.RespuestaCert;


public class InteroperabilidadFirmaDAOImpl extends SqlMapClientDaoSupport implements InteroperabilidadFirmaDAO {

	public InteroperabilidadFirma obtenerToken(Long drId, Integer sdr) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("secuencia", 1);
        return (InteroperabilidadFirma)getSqlMapClientTemplate().queryForObject("firma.obtenerToken", filter);
	}
	
    public Integer registrarInteroperabilidad(InteroperabilidadFirma iop) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", iop.getToken());
        filter.put("drId", iop.getDrId());
        filter.put("sdr", iop.getSdr());
        filter.put("acuerdoInternacionalId", iop.getAcuerdoInternacionalId());
        filter.put("secuencia", null);
        getSqlMapClientTemplate().queryForObject("firma.registrarInteroperabilidad", filter);
        return (Integer)filter.get("secuencia");
    }

    public Integer getEtapa(String token) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("secuencia", null);
        getSqlMapClientTemplate().queryForObject("firma.obtenerEtapaActual", filter);
        return (Integer)filter.get("secuencia");
    }
    
    public void actualizaEstadoFirma(Long drId, Integer sdr, String estado) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("estadoFirma", estado);
        getSqlMapClientTemplate().queryForObject("firma.actualizaEstadoSdr", filter);
    }
    
    public void actualizaXml(String token, Integer secuencia, byte[] xml) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("secuencia", secuencia);
        filter.put("xml", xml);
        getSqlMapClientTemplate().queryForObject("firma.actualizaXml", filter);
    }

    public void actualizaPdf(String token, Integer secuencia, byte[] pdf) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("secuencia", secuencia);
        filter.put("pdf", pdf);
        getSqlMapClientTemplate().queryForObject("firma.actualizaPdf", filter);
    }
    
    public InteroperabilidadFirma obtenerEtapa(String token, Integer secuencia) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("secuencia", secuencia);
        return (InteroperabilidadFirma)getSqlMapClientTemplate().queryForObject("firma.obtenerEtapa", filter);
    }    

    public InteroperabilidadFirma obtenerXml(String token, Integer secuencia) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("secuencia", secuencia);
        return (InteroperabilidadFirma)getSqlMapClientTemplate().queryForObject("firma.obtenerXml", filter);
    }    
    
    public InteroperabilidadFirma obtenerPdf(String token, Integer secuencia) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("token", token);
        filter.put("secuencia", secuencia);
        return (InteroperabilidadFirma)getSqlMapClientTemplate().queryForObject("firma.obtenerPdf", filter);
    }    
    
    public void actualizaFirmaExportador(Long drId, Integer sdr, String nombreExp, String formato, String posteriori) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("nombreExp", nombreExp);
        filter.put("formato", formato.toUpperCase());
        filter.put("posteriori", posteriori);
        getSqlMapClientTemplate().queryForObject("firma.actualizaPrimeraFirma", filter);
    }

    public boolean esMct001(Long drId, Integer sdr) throws Exception {
    	boolean result = false;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        Integer count = (Integer)getSqlMapClientTemplate().queryForObject("firma.es.mct001", filter);
        if (count.intValue() == 1) {
        	result = true;
        }
    	return result;
    }

    public boolean esMct003(Long drId, Integer sdr) throws Exception {
    	boolean result = false;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        Integer count = (Integer)getSqlMapClientTemplate().queryForObject("firma.es.mct003", filter);
        if (count.intValue() == 1) {
        	result = true;
        }
    	return result;
    }
    

    //JMC 07/04/2017 version 0.7
    
    
    public CodExporter obtenerDetalleCertificado(Long drId, Integer sdr) throws Exception {
    	CodExporter result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("sdr", sdr);
        result = (CodExporter)getSqlMapClientTemplate().queryForObject("firma.obtenerDetalleCertificadoAPDr", filter);
    	return result;
    }

            
    public EnteComercio obtenerExportador(Long codId) throws Exception {
    	EnteComercio result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = (EnteComercio)getSqlMapClientTemplate().queryForObject("firma.obtenerExportadorAPDr", filter);
    	return result;
    }  
    
    public EnteComercio obtenerImportador(Long codId) throws Exception {
    	EnteComercio result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = (EnteComercio)getSqlMapClientTemplate().queryForObject("firma.obtenerImportadorAPDr", filter);
    	return result;
    }    
    
    public List<EnteComercio> obtenerProductores(Long codId) throws Exception {
    	List<EnteComercio> result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = getSqlMapClientTemplate().queryForList("firma.obtenerProductoresAPDr", filter);
    	return result;
    }

    public List<FacturaOrigen> obtenerFacturas(Long codId) throws Exception {
    	List<FacturaOrigen> result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = getSqlMapClientTemplate().queryForList("firma.obtenerFacturasAPDr", filter);
    	return result;
    }
    
    public List<FacturaTercerOpOrigen> obtenerFacturasTercerOp(Long codId) throws Exception {
    	List<FacturaTercerOpOrigen> result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = getSqlMapClientTemplate().queryForList("firma.obtenerFacturasTercerOpAPDr", filter);
    	return result;
    }    

    public List<BienOrigen> obtenerBienes(Long codId) throws Exception {
    	List<BienOrigen> result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = getSqlMapClientTemplate().queryForList("firma.obtenerBienesAPDr", filter);
    	return result;
    }
    
    public EhCert obtenerCertificacionEH(Long codId) throws Exception {
    	EhCert result = null;
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
        result = (EhCert)getSqlMapClientTemplate().queryForObject("firma.obtenerDatosCertificacionEHAPDr", filter);
    	return result;
    }
    
    public Long generaDrAlianza (Long drId, Integer sdr) throws Exception{
    	Long result = null;		
		HashUtil<String, Object> filter = new HashUtil<String, Object>();		
    	filter.put("drId", drId);
        filter.put("sdr", sdr);
        filter.put("codId", result);
        result = (Long)getSqlMapClientTemplate().queryForObject("firma.generaDrAlianza", filter);
    			
    	return result;
    }

    public String actualizaDrAlianza (Integer codId) throws Exception{		
		HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("codId", codId);
		filter.put("certificadoId", null);
        getSqlMapClientTemplate().queryForObject("firma.actualizaDrAlianza", filter);    
        return filter.getString("certificadoId").toString();
    }
    
	public String produccionControlada(Long drId) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("produccionControlada", null);
        getSqlMapClientTemplate().queryForObject("firma.obtener.produccionControlada", filter);
        return filter.getString("produccionControlada").toString();
	}
	
	
	public RespuestaCert obtenerCertificado(HashUtil<String, Object> filter) throws Exception{
    	return (RespuestaCert)getSqlMapClientTemplate().queryForObject("adjunto.obtenerCertificado.element", filter);
    }

	public String tipoFirma(Long drId) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("drId", drId);
        filter.put("tipoFirma", null);
        getSqlMapClientTemplate().queryForObject("firma.obtener.tipoFirma", filter);
        return filter.getString("tipoFirma").toString();
	}
	
	public String permiteFirmaDigital(Long ordenId) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("ordenId", ordenId);
        filter.put("resultado", null);
        getSqlMapClientTemplate().queryForObject("firma.puede_firma_digital", filter);
        return filter.getString("resultado").toString();
	}

}
