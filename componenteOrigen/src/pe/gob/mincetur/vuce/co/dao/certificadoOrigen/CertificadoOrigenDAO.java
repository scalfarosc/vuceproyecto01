package pe.gob.mincetur.vuce.co.dao.certificadoOrigen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.AdjuntoCertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.InteroperabilidadFirma;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.AdjuntoRequeridoDJMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COAnulacion;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CODuplicado;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COFactura;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.COReemplazo;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CalificacionOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigen;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.CertificadoOrigenMercancia;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJurada;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaMaterial;
import pe.gob.mincetur.vuce.co.domain.certificadoOrigen.DeclaracionJuradaProductor;
import pe.gob.mincetur.vuce.co.procesodr.domain.certificadoOrigen.CertificadoOrigenDR;


/**
 * Interfaz de persistencia de Certificado de Origen
 * @author Erick Oscátegui
 */

public interface CertificadoOrigenDAO {

	public CertificadoOrigen getCertificadoOrigenById(HashUtil<String, Object> filter) throws Exception;

	public COFactura getCOFacturaById(HashUtil<String, Object> filter) throws Exception;

	public CertificadoOrigenMercancia getCOMercanciaById(HashUtil<String, Object> filter) throws Exception;

	public void updateCertificadoOrigen(CertificadoOrigen certificadoOrigen) throws Exception;

	public void cargarAdjuntoCertificadoOrigen(AdjuntoCertificadoOrigen adjunto) throws Exception;

	public void insertCOFactura(HashUtil<String, Object> filter) throws Exception;

	public void updateCOFactura(HashUtil<String, Object> filter) throws Exception;

	public void deleteCOFactura(HashUtil<String, Object> filter) throws Exception;

	public CertificadoOrigenMercancia getCertificadoOrigenMercanciaById(HashUtil<String, Object> filter) throws Exception;

	public void insertCertificadoOrigenMercanciaDjExistente(HashUtil<String, Object> filter) throws Exception;

	public void insertCertificadoOrigenMercanciaDjNueva(CertificadoOrigenMercancia certificadoOrigenMercancia) throws Exception;

	public void updateCertificadoOrigenMercancia(HashUtil<String, Object> filter) throws Exception;

	public void updateMercanciaDeclaracionJurada(DeclaracionJurada dj) throws Exception;

	public void deleteCertificadoOrigenMercancia(HashUtil<String, Object> filter) throws Exception;

	public void deleteMercanciaDeclaracionJurada(DeclaracionJurada dj) throws Exception;

	public DeclaracionJurada getCertificadoOrigenDeclaracionJuradaById(HashUtil<String, Object> filter) throws Exception;

	public List<DeclaracionJuradaProductor> getDJProductorByDJId(HashUtil<String, Object> filter) throws Exception;

	public void insertDeclaracionJurada(DeclaracionJurada declaracionJurada) throws Exception;

	public void insertDeclaracionJuradaSinMercancia(DeclaracionJurada declaracionJurada) throws Exception;

	public void updateDeclaracionJurada(DeclaracionJurada declaracionJurada) throws Exception;

	public void updateDeclaracionJuradaConsolidado(DeclaracionJurada declaracionJurada) throws Exception;

	public void cargarAdjuntoDeclaracionJurada(AdjuntoCertificadoOrigen adjunto) throws Exception;

    public void eliminarAdjuntoDJ(HashUtil<String, Object> filter) throws Exception;

	public DeclaracionJuradaMaterial getDeclaracionJuradaMaterialById(HashUtil<String, Object> filter) throws Exception;

	public void insertDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception;

	public void updateDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception;

	public void deleteDeclaracionJuradaMaterial(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception;

	public DeclaracionJuradaProductor getDeclaracionJuradaProductorById(HashUtil<String, Object> filter) throws Exception;

	public void insertDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception;

	public void updateDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception;

	public void deleteDeclaracionJuradaProductor(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception;

	public HashMap<String, Object> getCertificadoOrigenDRById(HashUtil<String, Object> filter) throws Exception;

	public HashMap<String, Object> getCOMercanciaDRById(HashUtil<String, Object> filter) throws Exception;

	public HashMap<String, Object> getCOFacturaDRById(HashUtil<String, Object> filter) throws Exception;

	public AdjuntoRequeridoDJMaterial loadAdjuntoRequeridoDJ(HashUtil<String, Object> filter) throws Exception;

	public CODuplicado getCertificadoOrigenMTC002ById(HashUtil<String, Object> filter) throws Exception;

	public void updateCertificadoOrigenDuplicado(CODuplicado coDuplicado) throws Exception;

	public void insertCertificadoOrigenDuplicadoOrigen(CODuplicado coDuplicado) throws Exception;

	public String getCausalById(HashUtil<String, Object> filter) throws Exception;

	public COAnulacion getCertificadoOrigenMTC004ById(HashUtil<String, Object> filter) throws Exception;

	public void updateCertificadoOrigenAnulacion(COAnulacion coAnulacion) throws Exception;

	public void insertCertificadoOrigenAnulacionOrigen(COAnulacion coAnulacion) throws Exception;

    public void solicitaValidacionProductor(Map<String,Object> parametros) throws Exception;

    public void aceptaSolicitudValidacion(Map<String,Object> parametros) throws Exception;

    public void rechazaSolicitudValidacion(Map<String,Object> parametros) throws Exception;

    public void transmiteDjValidada(Map<String,Object> parametros) throws Exception;
    
    //NPCS: 08/02/2018
    public String existeProductorValidador(HashMap<String, Object> filter) throws Exception;

	public void insertCertificadoOrigenReemplazoOrigen(COReemplazo coReemplazo) throws Exception;

	public void updateCertificadoOrigenReemplazo(COReemplazo coReemplazo) throws Exception;

	public void insertSubsanacionSolicitud(HashUtil<String, Object> filter) throws Exception;

	public void deleteSubsanacionSolicitud(HashUtil<String, Object> filter) throws Exception;

	public int mtoCertificadoVigente(Long ordenId) throws Exception;

	public Long getAdjunFirmaId(HashUtil<String, Object> filter) throws Exception;

    public void registraExportadorAutorizadoDj(Map<String,Object> parametros) throws Exception;

    public void revocaExportadorAutorizadoDj(Map<String,Object> parametros) throws Exception;

	public String devuelveEmpresaId(String tipoDoc, String numeroDoc) throws Exception;
	
	public String devuelveEmpresaIdOUsuPrincipalIdByUsuId(String tipoDoc, String numeroDoc) throws Exception;

	public void actualizarFlgDjCompleta(CalificacionOrigen filter) throws Exception;

	public void obtenerPartidas(HashUtil<String, Object> filter) throws Exception;

	public String djBtnValidacionProd(HashUtil<String, Object> filter) throws Exception;

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	public CalificacionOrigen getCalificacionOrigenById(HashUtil<String, Object> filter) throws Exception;
	public void updateDireccionAdicionalCalificacionOrigen(HashUtil<String, Object> filter) throws Exception;
	public void insertCalificacionOrigen(HashUtil<String, Object> filter) throws Exception;
	public void updateRolCalificacionOrigen(HashUtil<String, Object> filter) throws Exception;
	public void updateCriterioCalificacionOrigen(HashUtil<String, Object> filter) throws Exception;
	public void insertDeclaracionJuradaCalificacion(HashUtil<String, Object> filter) throws Exception;
	public void insertCalificacionOrigenConMercanciaDeclaracionJurada(HashUtil<String, Object> filter) throws Exception;
	public String djEnProcesoValidacion(HashUtil<String, Object> filter) throws Exception;
    public HashUtil<String, Object> enlazaDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception;
    public HashUtil<String, Object> copiarDJCalificacionMercancia(HashUtil<String, Object> filter) throws Exception;
    public int cuentaDJMateriales(HashUtil<String, Object> filter);
    public DeclaracionJurada getDjByCalificacionUoId(HashUtil<String, Object> filter, String enIngles) throws Exception;

    /**
     * Registra un calificación nueva a partir de una calificacion existente (para MCT005)
     */
    public HashUtil<String, Object> copiarDJCalificacionXCalificacion(HashUtil<String, Object> filter) throws Exception;
	public String djMaterialTieneAdj(HashUtil<String, Object> filter) throws Exception;

	/********************************************* CAMBIO PARA CALIFICACIÓN *******************************************************/

	public String reqValidacionProductor(HashMap<String, Object> filter) throws Exception;
	public String djValidada(HashMap<String, Object> filter) throws Exception;

	public String obtenerCalificacionHash(HashMap<String, Object> filter) throws Exception;
	public void obtenerComparaCalificacionHash(HashMap<String, Object> filter) throws Exception;

	public String obtenerIdiomaLlenado(Integer idAcuerdo) throws Exception;

	/*
	 * Ejecuta la validación que verifica que no se exceda el numero de lineas para la impresion de las mercancias
	 */
	public void validarCertificadoEspacioDet(HashMap<String, Object> filter) throws Exception;
	
	public double totalValorUSMateriales(HashMap<String, Object> filter) throws Exception;
	
	public HashUtil<String, Object> obtenerDatosPropietarioDJ(HashUtil<String, Object> filter) throws Exception;
	
	//20140825_JMC_Intercambio_Empresa
	public int insertCertificadoOrigenEmpresa(CertificadoOrigen certificadoOrigen) throws Exception ;
	public void insertCOFacturaEmpresa(COFactura factura) throws Exception ;
	public void insertCertificadoOrigenMercanciaEmpresa(CertificadoOrigenMercancia certificadoOrigenMercancia) throws Exception;
	
	public int insertDeclaracionJuradaEmpresa(DeclaracionJurada declaracionJurada) throws Exception;
	public void insertDeclaracionJuradaProductorEmpresa(DeclaracionJuradaProductor declaracionJuradaProductor) throws Exception;
	public void insertDeclaracionJuradaMaterialEmpresa(DeclaracionJuradaMaterial declaracionJuradaMaterial) throws Exception;
	
	//20140912 Reexportacion
	public void insertCertificadoOrigenMercancia(HashUtil<String, Object> filtro) throws Exception;

    public HashUtil<String, Object> solicitanteCalificacion(HashUtil<String, Object> filter) throws Exception;
    
	public byte[] loadCertificadoFD(HashUtil<String, Object> filter) throws Exception;
	
	
	public String validaCOFD(HashMap<String, Object> filter) throws Exception;

	public void insertArchivoCOFD(HashMap<String, Object> filter) throws Exception;
	
	public void habilitaFirma(HashMap<String, Object> filter) throws Exception;
	
    //NPCS: 14/03/2019 REGENERACION DE MERCANCIAS
    public void eliminarProductorMercanciasXML(HashUtil<String, Object> filter) throws Exception;
    
    public void eliminarMercanciasXML(HashUtil<String, Object> filter) throws Exception;

    public byte[] loadCertificadoFDXML(HashUtil<String, Object> filter) throws Exception;
}
