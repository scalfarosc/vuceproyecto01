package pe.gob.mincetur.vuce.co.dao;

import org.jlis.core.util.HashUtil;

/**
 * DAO para Contingencia VUCE
 * @author 
 *
 */
public interface ContingenciaAuthDAO {
	
	public Integer obtenerModoOperacion() throws Exception;

	public String registraConsulta(HashUtil<String, Object> filter) throws Exception;

	public String solicitaContingencia(HashUtil<String, Object> filter) throws Exception;
	
	public HashUtil<String, Object> validaToken(HashUtil<String, Object> filter) throws Exception;	
	
	public String obtenerMensajeIndexContingencia() throws Exception;
	
	public String obtenerMensajeCorreoEnviado() throws Exception;
	
	public String obtenerMensajeValidacionToken() throws Exception;
	
	public String obtenerMensajeValidacionCaptura() throws Exception;
	
}
