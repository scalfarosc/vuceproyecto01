package pe.gob.mincetur.vuce.co.dao.ibatis;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.jlis.core.util.HashUtil;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import pe.gob.mincetur.vuce.co.dao.OrdenDAO;
import pe.gob.mincetur.vuce.co.domain.Orden;

public class OrdenDAOImpl extends SqlMapClientDaoSupport implements OrdenDAO {

    private static Logger logger = Logger.getLogger(Constantes.LOGGER_DB);

    public Orden loadOrdenByNumero(Long numero) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("orden", numero);
        return (Orden) getSqlMapClientTemplate().queryForObject("orden.detalle.element.byNumero", filter);
    }
    
    public Orden loadOrdenById(Long idOrden) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("idOrden", idOrden);
        return (Orden) getSqlMapClientTemplate().queryForObject("orden.elementFromId", filter);
    }
    
    public HashUtil<String, Object> registrarTasaOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.registroTasaOrden", filter);
        return filter;
    }

    public int registrarNotificacionSubsanacion(Integer vcId, Long orden, Integer mto , String mensaje) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("orden", orden);
        filter.put("mto", mto);
        filter.put("vcId", vcId);
        filter.put("mensaje", mensaje);
        filter.put("notificacion", null);
        getSqlMapClientTemplate().queryForObject("transmision.notificacionSubsanacion.orden", filter);
        return filter.getInt("notificacion");
    }

    public void registrarNotificacionSubsanacionDetalle(int notificacion, String detalle) throws Exception {
        HashUtil<String, Object> filter = new HashUtil<String, Object>();
        filter.put("notificacion", notificacion);
        filter.put("descripcion", detalle);
        getSqlMapClientTemplate().queryForObject("transmision.notificacionSubsanacion.detalle", filter);
    }
    
    public Orden loadOrdenDetail(HashUtil<String, Object> filter) throws Exception {
        return (Orden)getSqlMapClientTemplate().queryForObject("orden.detalle.element", filter);
    }
    
    public HashUtil<String, Object> insertOrdenCab(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.inserta.cabecera", filter);
        return filter;
    }
    
    public void insertOrdenUsuario(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.inserta.usuario", filter);
    }

    public void insertOrdenSolicitante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.inserta.solicitante", filter);
    }

    public void insertOrdenRepresentante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.inserta.representante", filter);
    }
    
    public void updateOrdenRepresentante(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.actualiza.representante_legal", filter);
    }

    public HashUtil<String, Object> creaModifOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.modifica", filter);
        return filter;
    }

    public HashUtil<String, Object> transmiteOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.transmite", filter);
        return filter;
    }

    public void desistirOrden(HashUtil<String, Object> filter) throws Exception {
        getSqlMapClientTemplate().queryForObject("orden.desiste", filter);
    }
    
    /*public int existeModificaciones(HashUtil<String, Object> filter) throws Exception{
    	return Integer.parseInt(getSqlMapClientTemplate().queryForObject("orden.modificacion.count", filter).toString());
    }
    */
}
