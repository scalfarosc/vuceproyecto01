package pe.gob.mincetur.vuce.co.dao;

import java.util.List;

import org.jlis.core.util.HashUtil;

import pe.gob.mincetur.vuce.co.domain.Adjunto;
import pe.gob.mincetur.vuce.co.domain.AdjuntoFormato;
import pe.gob.mincetur.vuce.co.domain.certificado.AdjuntoFormatoCertificado;
import pe.gob.mincetur.vuce.co.procesodr.domain.AdjuntoMensaje;

public interface AdjuntoDAO {

    public List<AdjuntoFormatoCertificado> loadList(int orden, int mto) throws Exception;

    public void insertAdjunto(Adjunto adjunto) throws Exception;

    public void insertAdjuntoSuce(Adjunto adjunto) throws Exception;

    public void insertAdjuntoResolutorBorradorDR(Adjunto adjunto) throws Exception;

    public void insertAdjuntoResolutorDR(AdjuntoFormatoCertificado adjunto) throws Exception;

    public void updateAdjuntos(List<Adjunto> adjuntos) throws Exception;

    public int updateAdjunto(Adjunto adjunto) throws Exception;

    public void eliminarAdjuntoFormato(HashUtil<String, Object> filter) throws Exception;

    public void eliminarAdjuntoById(HashUtil<String, Object> filter) throws Exception;

    public Adjunto loadAdjunto(HashUtil<String, Object> filter) throws Exception;

    public List<AdjuntoFormatoCertificado> loadModifSuceList(int mensajeId) throws Exception;

    public Adjunto loadAdjuntoDR(HashUtil<String, Object> filter) throws Exception;

    public void eliminarAdjuntoCalificacionById(HashUtil<String, Object> filter) throws Exception;

    public void eliminarAdjuntoCertificadoById(HashUtil<String, Object> filter) throws Exception;

    public void insertAdjuntoNotificacion(Adjunto adjunto) throws Exception;

    public void insertAdjuntoResolutorDR(AdjuntoMensaje adjunto) throws Exception;

    public void insertAdjuntoSubsanacion(AdjuntoMensaje adjunto) throws Exception;

    public void insertAdjuntoFactura(HashUtil<String, Object> filter) throws Exception;

    public void eliminarAdjuntoFactura(HashUtil<String, Object> filter) throws Exception;
    
    public void insertAdjuntoProductor(HashUtil<String, Object> filter) throws Exception;

    public void eliminarAdjuntoProductor(HashUtil<String, Object> filter) throws Exception;
    
    //20140825_JMC_Intercambio_Empresa
    public List<Adjunto> loadListForTransmision(String keyQuery, int idTransmision) throws Exception;
    
    public void insertAdjuntoForTransmision(Adjunto adjunto) throws Exception;
    
    public int insertAdjuntoForTransmisionEmpresa(Adjunto adjunto) throws Exception;

    public void insertTransmisionForAdjunto(int transmisionEmpresaId, int idAdjunto) throws Exception;  
    
    public void insertNombreAdjuntoForTransmision(int mct001TmpId, AdjuntoFormato adjunto);
    
    public void insertNombreAdjuntoDJForTransmision(int mct005TmpId, AdjuntoFormato adjunto);
    
    public void insertNombreAdjuntoDJMaterialForTransmision(int mct005TmpId, int secuenciaMaterial, Adjunto adjunto);
}
