/*
 * JdbcUserDetailsServiceImpl.java
 *
 */

package pe.gob.mincetur.vuce.co.security;

import org.apache.log4j.Logger;
import org.jlis.core.util.Constantes;
import org.springframework.dao.DataAccessException;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.providers.encoding.ShaPasswordEncoder;
import org.springframework.security.userdetails.User;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

public class ComponenteOrigenUserDetailsServiceImpl implements UserDetailsService {
    
    private static Logger logger = Logger.getLogger(Constantes.LOGGER_SERVICE);
    
    private ShaPasswordEncoder passwordEncoder;
    
    public ComponenteOrigenUserDetailsServiceImpl() {
    }
    
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        // Obtener los roles y colocarlos en el arreglo "grantedAuthoryties"
        GrantedAuthority [] grantedAuthorities = { new GrantedAuthorityImpl("ROLE_USER") };
        String encodedPassword = this.passwordEncoder.encodePassword("", null);
        UserDetails userDetails = new User(username, encodedPassword, true, true, true, true, grantedAuthorities);
        return userDetails;
    }
    
    public void setPasswordEncoder(ShaPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    public ShaPasswordEncoder getPasswordEncoder() {
        return this.passwordEncoder;
    }
    
}
