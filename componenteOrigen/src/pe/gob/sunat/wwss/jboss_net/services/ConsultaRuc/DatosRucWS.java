package pe.gob.sunat.wwss.jboss_net.services.ConsultaRuc;

import ConsultaRuc.BeanDdp;
import ConsultaRuc.BeanDds;
import ConsultaRuc.BeanRso;
import ConsultaRuc.BeanSpr;
import ConsultaRuc.BeanT1144;
import ConsultaRuc.BeanT1150;
import ConsultaRuc.BeanT362;

public interface DatosRucWS {
    
	public BeanDdp getDatosPrincipales(String in0);
    
	public String getDomicilioLegal(String in0);
    
	public BeanT1144 getDatosT1144(String in0);
    
	public BeanDds getDatosSecundarios(String in0);
    
	public Object [] getRepLegales(String in0);
    
	public Object [] getEstablecimientosAnexos(String in0);
    
	public Object [] getEstAnexosT1150(String in0);
    
	public Object [] buscaRazonSocial(String in0);
    
	public Object [] getDatosT362(String in0);
	
}
