<%@page import="pe.gob.mincetur.vuce.co.bean.UsuarioCO"%>
<%@page import="pe.gob.mincetur.vuce.authenticationmanager.client.constant.AuthenticationManagerProviderConstants"%>
<%@page import="org.jlis.core.config.ApplicationConfig"%>
<%@page import="org.springframework.web.util.WebUtils"%>
<%@page import="pe.gob.mincetur.vuce.authenticationmanager.client.service.SessionService"%>
<%
UsuarioCO coUser = (UsuarioCO)request.getSession().getAttribute("USUARIO");

SessionService sessionService = new SessionService();

sessionService.setCookie("cliente_seleccionado", ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerClientId"), "240", response);

Cookie vuceSessionIdCookie = WebUtils.getCookie(request, "vuce_sesion_id");
Cookie idpCookie = WebUtils.getCookie(request, "idp");

String url = ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerLoginOptionsUrl") + "/" + ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerClientId");

if(vuceSessionIdCookie == null) {
	sessionService.cleanSession(request, response);
} else {
	if(idpCookie != null) {
		String providerId = idpCookie.getValue();
		if(AuthenticationManagerProviderConstants.OAUTH2SOL_PROVIDER_ID.equals(providerId)) {
			url = ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerSSORedirectUrl") + "?t=s&sesion_id=" + vuceSessionIdCookie.getValue();
		} else if (AuthenticationManagerProviderConstants.EXTRANET_PROVIDER_ID.equals(providerId)) {
			url = ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerSSORedirectUrl") + "?t=e&sesion_id=" + vuceSessionIdCookie.getValue();
		}
	} else {
		sessionService.cleanSession(request, response);
	}
}

response.sendRedirect(url);

%>