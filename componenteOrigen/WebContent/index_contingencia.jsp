<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="org.jlis.core.util.*"%>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<head>
    <title>VUCE - Autenticaci&oacute;n Contingencia</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="expires" content="-1" /><meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <!-- modified  -->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resource/css/login_contingencia.css">

    <style>
        /* BODY */
        /* Seccion Main Body */
        .main-body { align-items: center; color: #726d6d; display: flex; flex-direction: column; }
        .main-body > .wrapper { margin: auto; }
        .main-body .titulo { letter-spacing: -0.68px; }
        .main-body .titulo > h1 { color: #1a1818; font: 34px/40px robotoBold; margin: 0; }
        .main-body .titulo > p { font: 16px/19px robotoRegular; line-height: 1.4; margin: 0 15px; max-width: 800px; padding-top: 4px; }
        .main-body .btn { border: 1px solid transparent; cursor: pointer; font-size: 14px; text-align: center; }

        /* Seccion login contingencia */
        .main-body .formulario-ingreso { border: 1px solid #DBDBDB; border-radius: 8px; margin: 30px auto 0; max-width: 350px; }
        .main-body .formulario-ingreso > form { padding: 20px 40px; }
        .main-body .formulario-ingreso .contingencia-ruc { padding: 20px 0 10px; }
        .main-body .formulario-ingreso .contingencia-sol { padding: 10px 0; }
        .main-body .formulario-ingreso > form > div { font: 15px/18px robotoBold; text-align: left; }
        .main-body .formulario-ingreso > form label, .main-body .formulario-ingreso > form input { width: 100%; }
        .main-body .formulario-ingreso > form > div > input { border: 1px solid #b1b1b1; border-radius: 1px; font: 14px/16px robotoRegular; margin-top: 10px; max-width: 236px; padding: 11px 16px; }
        .main-body .formulario-ingreso > form input::placeholder { color: #c3c3c3; }
        .main-body .formulario-ingreso > form input:placeholder-shown { color: #c3c3c3 !important; }
        .main-body .formulario-ingreso > form input:-ms-input-placeholder { color: #c3c3c3 !important; }
        .main-body .formulario-ingreso > form > .btn { background-color: #0272bd; border-radius: 5px; color: white; font: 15px/48px robotoBold; margin: 10px 0; padding: 0 20px; }

        .main-body .g-recaptcha { padding-top: 10px; transform: scale(0.88); -webkit-transform: scale(0.88); transform-origin:0 0; -webkit-transform-origin:0 0; }
        .main-body .formulario-ingreso > form > .error_captcha { color: #C22821;  font: 12px/14px robotoRegular; margin-bottom: 10px; }

        @media all and (max-width: 958px) {
            section.main-body { padding: 80px 0; }
            .main-body .titulo > p, footer > .wrapper > small { align-items: stretch; }
        }
        @media all and (max-width: 800px) {
            .main-body { align-items: stretch; }
            .main-body > .wrapper { padding: 0 20px; }
        }
        @media screen and (max-width: 475px) {
            section.main-body { padding: 50px 0; }
            .main-body > .wrapper > .titulo { margin-bottom: 40px; }
            .main-body > .wrapper > .titulo > h1 { font: 28px/33px robotoBold; }
            .main-body > .wrapper > .titulo > p { font: 15px/18px robotoRegular; margin: 0 auto; max-width: 305px; }

            .main-body .formulario-ingreso { margin: 0 auto; max-width: 315px; }
            .main-body .formulario-ingreso > form { margin: 0 auto; max-width: 315px; padding: 20px 30px; }
            .main-body .formulario-ingreso > form > div { font: 14px/16px robotoBold; }
            .main-body .formulario-ingreso > form > div > input { max-width: 216px; }

            .main-body .g-recaptcha { transform:scale(0.83); -webkit-transform:scale(0.83); transform-origin:0 0; -webkit-transform-origin:0 0; }
        }
    </style>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script type="text/javascript">

        function login() {
            var f = document.formulario;
            f.action = "<%=request.getContextPath()%>/contingencia.htm?t=capturaDatosContingencia";
            f.target = window.top.name;
            f.usuario.value = f.usuario.value.trim();
            f.submit();
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function validarRUC(textbox) {
            if (textbox.value === '') {
                textbox.setCustomValidity('Debe ingresar su RUC');
            } else if (textbox.value.length<11){
                textbox.setCustomValidity('El RUC debe ser de 11 dígitos');
            } else {
                textbox.setCustomValidity('');
            }

            return true;
        }

    </script>
</head>
<body>
<header>
    <div class="wrapper">
        <nav>
            <a href="http://beta.vuce.gob.pe/Paginas/Inicio.aspx" target="_blank">
                <img src="<%=request.getContextPath()%>/imagenes/svg/logo-vuce.svg" alt="logo" width="113" height="53" />
            </a>
            <a href="https://www.gob.pe/mincetur" target="_blank">
                <img src="<%=request.getContextPath()%>/imagenes/svg/logo-mincetur.svg" alt="Ministerio de Comercio Exterior y Turismo" width="235" height="46" />
            </a>
        </nav>
        <section>
            <a href="http://beta.vuce.gob.pe/Paginas/Preguntas-frecuentes.aspx" target="_blank">
                <span>Preguntas Frecuentes</span>
                <img src="<%=request.getContextPath()%>/imagenes/svg/icon-preguntas-frecuentes.svg" />
            </a>
            <div>
                <a href="mailto:vuceayuda@vuce.gob.pe">vuceayuda@vuce.gob.pe</a>
                <a href="tel:012071510">
                    <span>Mesa de ayuda</span>
                    <span>(01)207-1510</span>
                    <img src="<%=request.getContextPath()%>/imagenes/svg/icon-mesa-de-ayuda.svg" />
                </a>
            </div>
        </section>
    </div>
</header>
<section class="main-body">
    <div class="wrapper">
        <div class="titulo">
            <h1>Bienvenido a la VUCE</h1>
            <p>
                El Sistema de Clave SOL de SUNAT temporalmente no se encuentra disponible. <br/>
                Por favor, ingresa tus datos y VUCE te enviar&aacute; un correo electr&oacute;nico con las indicaciones para acceder al Sistema.
            </p>
        </div>
        <div class="formulario-ingreso">
            <form name="formulario" method="post" onsubmit="login()">
                <jlis:value name="password" type="hidden" />
                <jlis:value name="tipoLogueo" type="hidden" value="R" />
                <jlis:value name="tipoContingencia" type="hidden" value='<%=request.getParameter("tipoContingencia")%>' />
                <c:if test="${param.tipoContingencia eq 's'}">
                    <div class="contingencia-ruc">
                        <label for="ruc">Ingresa los 11 d&iacute;gitos de tu RUC</label>
                        <input pattern="[0-9.]+" type="text" id="ruc" name="ruc" maxlength="11" required
                               oninput="setCustomValidity('')" oninvalid="validarRUC(this);"
                               onkeypress="return isNumberKey(event)" placeholder="N&uacute;mero de RUC" />
                    </div>
                </c:if>
                <div class="contingencia-sol">
                    <label for="usuario">Ingresa tu usuario SOL</label>
                    <input type="text" id="usuario" name="usuario" maxlength="20" value="" required
                           oninput="setCustomValidity('')"  oninvalid="setCustomValidity('Debe ingresar su usuario SOL');" placeholder="Usuario SOL" />
                </div>
                <div class="g-recaptcha" data-sitekey="6LdqYNMUAAAAAE-rmxLIjZMJ0wONdmb5y_TfVFnt"></div>
                <c:if test="${sessionScope.mensajeCaptcha!=null}">
                    <div class="error_captcha">
                        <span>${sessionScope.mensajeCaptcha}</span>
                    </div>
                </c:if>
                <input class="btn" type="submit" value="Generar acceso">
            </form>
        </div>
    </div>
</section>

<footer>
    <div class="wrapper">
        <div>
            <a href="http://beta.vuce.gob.pe/Paginas/Aviso-de-privacidad.aspx" target="_blank">Pol&iacute;ticas de privacidad</a>
            <a href="http://beta.vuce.gob.pe/Paginas/T%C3%A9rminos-y-Condiciones.aspx" target="_blank">Condiciones de uso</a>
        </div>
        <picture>
            <img src="<%=request.getContextPath()%>/imagenes/svg/iso-9001.svg" alt="ISO 9001" />
            <a href="https://www.gob.pe/mincetur" target="_blank">
                <img src="<%=request.getContextPath()%>/imagenes/svg/logo-mincetur.svg" alt="Ministerio de Comercio Exterior y Turismo" />
            </a>
        </picture><br />
        <hr />
        <small>
            &copy; Copyright 2020 - MINCETUR Todos los derechos reservados.
            Ante cualquier duda o problema contacte a
            <a href="tel:012071510">
                <span>Mesa de ayuda: (01)207-1510</span>
            </a>
            <a href="mailto:vuceayuda@vuce.gob.pe">vuceayuda@vuce.gob.pe</a>
        </small>
    </div>
</footer>
</body>
</html>