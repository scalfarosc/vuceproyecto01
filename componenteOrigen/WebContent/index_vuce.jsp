<%@page import="pe.gob.mincetur.vuce.co.util.*"%>
<html>
    <%@include file="/WEB-INF/jsp/taglibs.jsp"%>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Autenticaci�n</title>
        <meta http-equiv="Pragma" content="no-cache" />
        <link href="<%=contextName%>/resource/css/intranet.css" rel=stylesheet />
    </head>
<%
    OptionList listaModulosOrigen = OptionListFactory.getListaModulosOrigen();
    request.setAttribute("listaModulosOrigen", listaModulosOrigen);
    
    UsuarioCO usuarioObj = (UsuarioCO)request.getSession().getAttribute(Constantes.USUARIO);
    if (usuarioObj!=null && usuarioObj.getIdUsuario()!=null) {%>
    <script>
	    window.location.href = "/co/co.htm?method=principal";
    </script>
<%  } else {%>
    <script language="JavaScript1.2" type="text/javascript">
        
        function login() {
            var f = document.formulario;
            f.action = contextName+"/login";
            f.target = window.top.name;
            //f.usuario.value = trim(f.usuario.value.toUpperCase());
            f.submit();
        }

    </script>
    <body>
        <div id="body">
            <jsp:include page="/WEB-INF/jsp/header.jsp" />
            <form name="formulario" method="post">
                <jlis:value name="password" type="hidden" />
                <jlis:value name="tipoLogueo" type="hidden" value="R" />
                <jlis:value name="modulo" type="hidden" value="CO" />
                <div id="pcont">
                    <div id="cont">
                        <div id="conti">
                            <div id="login">
                                <h1>Componente Origen</h1>
                                <p>Para ingresar al sistema, selecciona una de las dos opciones de autenticaci�n.</p>
                                <p id="options">
                                    <strong>Componente Origen</strong>
	                                <span>
	                                <table>
                                    <!-- <tr><td style="text-align: right;">M�dulo:&nbsp;&nbsp;</td><td><jlis:selectSource name="modulo" source="listaModulosOrigen" scope="request" style="defaultElement=no" /></td></tr>
	                                <tr><td>&nbsp;</td></tr>
	                                <tr><td>&nbsp;</td></tr>-->
                                    <tr><td style="text-align: right;">RUC:&nbsp;&nbsp;</td><td><jlis:value name="ruc" size="11" value="20262996329"/></td></tr>
                                    <tr><td style="text-align: right;">Usuario:&nbsp;&nbsp;</td><td><jlis:value name="usuario" size="20" value="USUJAVIERPROD"/></td></tr>
	                                <tr><td>&nbsp;</td></tr>
	                                <tr><td style="text-align: center;" colspan="2"><INPUT class=auth_button id=submit_btn_id onclick=login() type=button value="Iniciar Sesi&oacute;n" name=firmar></td></tr>
	                                <tr><td>&nbsp;</td></tr>
	                                </table>
	                                </span>
	                                <table>
		                                <tr>
		                                	<td><input type="radio" id="rolUsuarioSOL_operacion" name="rolUsuarioSOL" value="O" />&nbsp;Operaci�n</td>
                                        </tr>
                                        <tr>
		                                	<td><input type="radio" id="rolUsuarioSOL_coordinador" name="rolUsuarioSOL" value="S" />&nbsp;Supervisi�n</td>
                                        </tr>
                                        <tr>
		                                	<td><input type="radio" id="rolUsuarioSOL_firma" name="rolUsuarioSOL" value="F" />&nbsp;Firmador</td>
                                        </tr>
                                        <tr>
                                            <td><input type="radio" id="rolUsuarioSOL_ambos" name="rolUsuarioSOL" value="T" />&nbsp;Supervisi�n / Operaci�n / Firmador</td>
		                                </tr>
	                                </table>
                                    <span id="expoimpo" >Usuarios Exportadores/Importadores</span>
                                    <a href="<%=contextName%>/logins.html?t=s">Autenticaci�n SOL</a>
                                    <%--a href="#">Autenticaci�n DNI</a--%>
                                    <span id="funcio" >Funcionarios</span>
                                    <a href="<%=contextName%>/logine.html?t=e">Autenticaci�n Extranet</a>

                                </p>
                            </div>
							<div id="login">
                                <!--p><a href="https://www.vuce.gob.pe/comunicados/3/comunicado_io_alianza_co.pdf">Entrada en vigencia de la Interoperabilidad en el Acuerdo Marco de la Alianza del Pac�fico</a></p-->
                                <p><a href="https://www.vuce.gob.pe/manual_vuce/manuales/usuarios/creacion_usuarios_secundarios_co.pdf">Manual de creaci�n de Usuario Secundario para el Componente Origen</a></p>
                            </div>
                        </div>
                    </div>
                    <!--span style="color: red; font-weight: bold;">Vigencia l�mite de uso alternativo 30/06/2014 - Uso Obligatorio de la VUCE a partir de 01/07/2014</span><br/-->
                    <span style="color: red;">Capacitaciones: Contactarse con su Entidad Certificadora. <a href="https://www.vuce.gob.pe/archivos_entidades/origen/Entidades_Delegadas.pdf" target="_blank">(Listado de Entidades a Nivel Nacional).</a></span><br/>
                    <span style="color: red;"><a href="https://www.vuce.gob.pe/marco_normativo/21 - D.S. 015-2013-MINCETUR.pdf" target="_blank">Decreto Supremo N� 015-2013-MINCETUR</a></span>
                </div>
             </form>
             <jsp:include page="/WEB-INF/jsp/footer.jsp" />
         </div>
    </body>
<%  }%>
</html>