<%@page import="pe.gob.mincetur.vuce.co.util.*"%>
<HTML>
    <%@include file="/WEB-INF/jsp/taglibs.jsp"%>
    <HEAD>
        <TITLE>COMPONENTE ORIGEN - Autenticación</TITLE>
        <meta http-equiv="Pragma" content="no-cache" />
        <LINK href="<%=contextName%>/resource/css/intranet.css" rel=stylesheet>
    </HEAD>
<%
    //OptionList listaModulosOrigen = OptionListFactory.getListaModulosOrigen();
    //request.setAttribute("listaModulosOrigen", listaModulosOrigen);
%>
    <script language="JavaScript1.2" type="text/javascript">

        function login() {
            var f = document.formulario;
            f.action = contextName+"/login";
            f.usuario.value = trim(f.usuario.value.toUpperCase());
            f.submit();
        }

        function seleccionTipoLogueo(tipoLogueo) {
            if (tipoLogueo.value=="R") {
                document.getElementById("ruc").disabled = false;
            } else {
                document.getElementById("ruc").disabled = true;
            }

            if (tipoLogueo.value=="E") {
            	document.getElementById("rol_evaluador").disabled = false;
                document.getElementById("rol_helpdesk").disabled = false;
                document.getElementById("rol_entidad_administrador").disabled = false;
                document.getElementById("rol_entidad_supervisor").disabled = false;
                document.getElementById("rol_sunat_especialista").disabled = false;
            } else {
                document.getElementById("rol_evaluador").disabled = true;
                document.getElementById("rol_helpdesk").disabled = true;
                document.getElementById("rol_entidad_administrador").disabled = true;
                document.getElementById("rol_entidad_supervisor").disabled = true;
                document.getElementById("rol_sunat_especialista").disabled = true;
            }
        }

		function mostrarSimulador() {
			var simulador = document.getElementById("simulador");
			simulador.style.display = simulador.style.display=="none" ? "block" : "none";
		}

    </script>
    <body>
        <div id="body">
            <jsp:include page="/WEB-INF/jsp/header.jsp" />
            <form name="formulario" method="post">
                <jlis:value name="password" type="hidden" />
                <jlis:value name="modulo" type="hidden" value="CO" />

                <div id="pcont">
                    <div id="cont">
                        <div id="conti">
                        	<div><a href="javascript:mostrarSimulador()">Simulador</a></div>
                            <div id="simulador" >
                                <table>
<%--                                <tr><td><input type="radio" name="tipoLogueo" value="R" onClick="seleccionTipoLogueo(this)" checked />&nbsp;Autenticación SOL&nbsp;&nbsp;</td><td><input type="radio" name="tipoUsuario" value="P" onClick="seleccionTipoUsuario(this.value)" checked />&nbsp;Principal</td><td><input type="radio" id="rolUsuarioSOL_operacion" name="rolUsuarioSOL" value="O" checked />&nbsp;Operación</td></tr>  --%>
                                <tr><td><input type="radio" name="tipoLogueo" value="E"  checked />&nbsp;Autenticación Extranet&nbsp;&nbsp;</td><%--<td><input type="radio" name="tipoUsuario" value="S" onClick="seleccionTipoUsuario(this.value)" />&nbsp;Secundario</td><td><input type="radio" id="rolUsuarioSOL_coordinador" name="rolUsuarioSOL" value="S" />&nbsp;Coordinador</td> --%></tr>
<%--                                <tr><td colspan="2"><input type="radio" name="tipoLogueo" value="D" onClick="seleccionTipoLogueo(this)" />&nbsp;Autenticación DNI&nbsp;&nbsp;</td></tr>  --%>
                                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr><td colspan="2"><strong>Roles del Usuario Extranet</strong></td></tr>
                                <tr><td colspan="2"><input type="checkbox" id="rol_evaluador" name="roles" value="CO.ENTIDAD.EVALUADOR" />CO.ENTIDAD.EVALUADOR</td></tr>
                                <tr><td colspan="2"><input type="checkbox" id="rol_entidad_supervisor" name="roles" value="CO.ENTIDAD.SUPERVISOR" />CO.ENTIDAD.SUPERVISOR</td></tr>
                                <tr><td colspan="2"><input type="checkbox" id="rol_firma_autorizada" name="roles" value="CO.ENTIDAD.FIRMA" />CO.ENTIDAD.FIRMA</td></tr> <!-- 28 -->
<%--                                <tr><td colspan="2"><input type="checkbox" id="rol_entidad_supervisor" name="roles" value="CO.ENTIDAD.SUPERVISOR" />CO.ENTIDAD.SUPERVISOR</td></tr> --%>
                                <tr><td colspan="2"><input type="checkbox" id="rol_helpdesk" name="roles" value="CO.ADMIN.HELP_DESK" />CO.ADMIN.HELP_DESK</td></tr>
                                <tr><td colspan="2"><input type="checkbox" id="rol_helpdesk" name="roles" value="CO.ENTIDAD.CONSULTA_DR" />CO.ENTIDAD.CONSULTA_DR</td></tr>
                                <tr><td colspan="2"><input type="checkbox" id="rol_sunat_especialista" name="roles" value="VUCE.SUNAT.ESPECIALISTA" />VUCE.SUNAT.ESPECIALISTA</td></tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr><td style="text-align: right;">RUC:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><jlis:value name="ruc" size="11" value="20504233104"/></td></tr>
                                <tr><td style="text-align: right;">Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><jlis:value name="usuario" size="20" value="20202020202020202020"/></td></tr>
                                <tr><td style="text-align: right;">Nombre en SUNAT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><jlis:value name="nombreCompleto" size="20" value="COMX - JUAN PEREZ"/></td></tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr><td style="text-align: center;" colspan="2"><INPUT class=auth_button id=submit_btn_id onclick=login() type=button value="Iniciar Sesi&oacute;n" name=firmar></td></tr>

                                </table>
                            </div>
                            <div id="login">
                                <h1>Componente Origen</h1>
                                <p>Para ingresar al sistema, selecciona una de las dos opciones de autenticación.</p>
                                <p id="options">
                                    
                                    <strong>Componente Origen</strong>
                                    
                                    <%--Módulo: <jlis:selectSource name="modulo" source="listaModulosOrigen" scope="request" style="defaultElement=no" />
                                    <br/>
                                    <br/--%>
                                    <span id="expoimpo" >Usuarios Exportadores/Importadores</span>
                                    
                                    <%--a href="<%=contextName%>/logins.html?t=s">Autenticación SOL</a--%>
                                    <a href="https://www.vuce.gob.pe/vuce/index.jsp?m=so">Autenticación SOL</a>
                                    
                                    <%--a href="https://www.vuce.gob.pe/vuce/index.jsp?m=do">Autenticación DNI</a--%>
                                    <span id="funcio" >Funcionarios</span>
                                    
                                    <%--a href="<%=contextName%>/logine.html?t=e">Autenticación Extranet</a--%>
                                    <a href="https://www.vuce.gob.pe/vuce/index.jsp?m=eo">Autenticación Extranet</a>
                                </p>
                            </div>
                            <div id="login">
                                <%-- <p><a href="/manual/Creacion_de_usuarios_CO.pdf">Manual de creación de Usuario Componente Origen</a></p> --%>
                            </div>
                        </div>
                    </div>
                    Estimado Usuario, próximamente estará disponible el servicio de autenticación por DNI, únicamente para usuarios que no cuenten con el registro de su RUC ante SUNAT. Si usted ya cuenta con RUC, solo podrá ingresar a través del botón: Autenticación SOL.
                </div>
             </form>
             <jsp:include page="/WEB-INF/jsp/footer.jsp" />
         </div>
    </body>
</HTML>