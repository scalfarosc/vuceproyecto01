<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>

        <table class="form">
			<tr>
				<td colspan="3"><h3 class="psubtitle"><span><b><jlis:label key="co.title.diferencias_campos" /></b></span></h3></td>
			</tr>
		</table>
		<jlis:table name="MTS_CAMBIOS_CAMPOS" pageSize="*" keyValueColumns="ORDEN_ID,MTO,SUCE_ID,TIPO_DIFERENCIA" source="tDiferenciasModificacionSUCE_cambios" 
            scope="request" navigationHeader="no" width="100%" >
			<jlis:tr>
				<jlis:td name="ORDEN_ID" nowrap="yes" />
				<jlis:td name="MTO" nowrap="yes" />
                <jlis:td name="SUCE_ID" nowrap="yes" />
				<jlis:td name="TIPO_DIFERENCIA" nowrap="yes"/>
				<jlis:td name="DIFERENCIA" nowrap="yes" width="50%"/>
				<jlis:td name="DATO ORIGINAL" nowrap="yes" width="25%"/>
				<jlis:td name="NUEVO DATO" nowrap="yes" width="25%"/>
			</jlis:tr>
			<jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
			<jlis:columnStyle column="2" columnName="MTO" hide="yes" />
			<jlis:columnStyle column="3" columnName="SUCE_ID" hide="yes" />
			<jlis:columnStyle column="4" columnName="TIPO_DIFERENCIA" hide="yes" />
			<jlis:columnStyle column="5" columnName="DESCRIPCION_DIFERENCIA" />
			<jlis:columnStyle column="6" columnName="DATO_ORIGEN" />
			<jlis:columnStyle column="7" columnName="DATO_NUEVO" />
		</jlis:table>

        <table class="form">
            <tr>
                <td colspan="3"><h3 class="psubtitle"><span><b><jlis:label key="co.title.diferencias_nuevos_registros" /></b></span></h3></td>
            </tr>
        </table>
        <jlis:table name="MTS_NUEVOS_REGISTROS" pageSize="*" keyValueColumns="ORDEN_ID,MTO,SUCE_ID,TIPO_DIFERENCIA" source="tDiferenciasModificacionSUCE_nuevos_registros" 
            scope="request" navigationHeader="no" width="100%">
            <jlis:tr>
                <jlis:td name="ORDEN_ID" nowrap="yes" />
                <jlis:td name="MTO" nowrap="yes" />
                <jlis:td name="SUCE_ID" nowrap="yes" />
                <jlis:td name="TIPO_DIFERENCIA" nowrap="yes"/>
                <jlis:td name="DIFERENCIA" nowrap="yes" width="100%"/>
                <jlis:td name="DATO ORIGINAL" />
                <jlis:td name="NUEVO DATO" />
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
            <jlis:columnStyle column="2" columnName="MTO" hide="yes" />
            <jlis:columnStyle column="3" columnName="SUCE_ID" hide="yes" />
            <jlis:columnStyle column="4" columnName="TIPO_DIFERENCIA" hide="yes" />
            <jlis:columnStyle column="5" columnName="DESCRIPCION_DIFERENCIA" />
            <jlis:columnStyle column="6" columnName="DATO_ORIGEN" hide="yes" />
            <jlis:columnStyle column="7" columnName="DATO_NUEVO" hide="yes" />
        </jlis:table>
        
        <table class="form">
            <tr>
                <td colspan="3"><h3 class="psubtitle"><span><b><jlis:label key="co.title.diferencias_registros_eliminados" /></b></span></h3></td>
            </tr>
        </table>
        <jlis:table name="MTS_REGISTROS_ELIMINADOS" pageSize="*" keyValueColumns="ORDEN_ID,MTO,SUCE_ID,TIPO_DIFERENCIA" source="tDiferenciasModificacionSUCE_registros_eliminados" 
            scope="request" navigationHeader="no" width="100%">
            <jlis:tr>
                <jlis:td name="ORDEN_ID" nowrap="yes" />
                <jlis:td name="MTO" nowrap="yes" />
                <jlis:td name="SUCE_ID" nowrap="yes" />
                <jlis:td name="TIPO_DIFERENCIA" nowrap="yes"/>
                <jlis:td name="DIFERENCIA" nowrap="yes" width="100%"/>
                <jlis:td name="DATO ORIGINAL" />
                <jlis:td name="NUEVO DATO" />
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
            <jlis:columnStyle column="2" columnName="MTO" hide="yes" />
            <jlis:columnStyle column="3" columnName="SUCE_ID" hide="yes" />
            <jlis:columnStyle column="4" columnName="TIPO_DIFERENCIA" hide="yes" />
            <jlis:columnStyle column="5" columnName="DESCRIPCION_DIFERENCIA" />
            <jlis:columnStyle column="6" columnName="DATO_ORIGEN" hide="yes" />
            <jlis:columnStyle column="7" columnName="DATO_NUEVO" hide="yes" />
        </jlis:table>
        