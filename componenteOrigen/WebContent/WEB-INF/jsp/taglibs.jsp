<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>
<%@page import="org.jlis.core.bean.*"%>
<%@page import="org.jlis.core.util.*"%>
<%@page import="org.jlis.core.list.*"%>
<%@page import="org.jlis.web.list.*"%>
<%@page import="org.jlis.web.util.*"%>
<%@page import="pe.gob.mincetur.vuce.co.bean.UsuarioCO"%>
<%
    String contextName = request.getContextPath();
    String imagenBusqueda = contextName+"/imagenes/busqueda.gif";
    String imagenBuscar = contextName+"/imagenes/buscar.gif";
    String imagenNuevo = contextName+"/imagenes/new.gif";
    String imagenEditar = contextName+"/imagenes/edit.gif";
    String imagenEliminar = contextName+"/imagenes/delete.gif";
    String imagenInicio = contextName+"/imagenes/inicio.gif";
    String imagenDatos = contextName+"/imagenes/link_down.gif";
    String imagenEditarDetalle = contextName+"/imagenes/editar.gif";
    String imagenObservaciones = contextName+"/imagenes/observaciones.gif";
    String imagenCalendario = contextName+"/imagenes/calendario.gif";
    String imagenZonas = contextName+"/imagenes/zonas.gif";
    String imagenUbicaciones = contextName+"/imagenes/ubicaciones.gif";
    String imagenGrupoClientes = contextName+"/imagenes/grupo_clientes.gif";
    String imagenInfo = contextName+"/imagenes/info.gif";
    String imagenTelefono = contextName+"/imagenes/telefono.gif";
    java.util.Date x = new java.util.Date();
    String windowName = "w_"+x.getTime();
%>
<%--!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" --%>
<link href="<%=contextName%>/resource/css/jscalendar-1.0/skins/aqua/theme.css" rel="stylesheet" type="text/css" media="all" title="Aqua" />
<link href="<%=contextName%>/resource/css/frameworkCSS.css" rel="stylesheet" type="text/css" >
<link href="<%=contextName%>/resource/css/site.css" rel="stylesheet" type="text/css"  media="screen">
<link href="<%=contextName%>/resource/css/menuCSS.css" rel="stylesheet" media="screen, projection" type="text/css">
<link href="<%=contextName%>/resource/css/subModal/subModal.css" rel="stylesheet" media="screen, projection" type="text/css">
<link href="<%=contextName%>/resource/css/tabcontent.css" rel="stylesheet" type="text/css" >
<script>
    var contextName = "/co";
    window.name = "<%=windowName%>";

</script>
<script src="<%=contextName%>/resource/js/jscalendar-1.0/calendar.js"></script>
<script src="<%=contextName%>/resource/js/jscalendar-1.0/calendar-functions.js"></script>
<c:if test="${empty idioma || idioma=='es'}">
<script src="<%=contextName%>/resource/js/jscalendar-1.0/lang/calendar-es.js"></script>
</c:if>
<c:if test="${idioma=='en'}">
<script src="<%=contextName%>/resource/js/jscalendar-1.0/lang/calendar-en.js"></script>
</c:if>
<script src="<%=contextName%>/resource/js/functions.js"></script>
<script src="<%=contextName%>/resource/js/date.js"></script>
<script src="<%=contextName%>/resource/js/menu.js"></script>
<script src="<%=contextName%>/resource/js/subModal/common.js"></script>
<script src="<%=contextName%>/resource/js/subModal/drag.js"></script>
<script src="<%=contextName%>/resource/js/ajax/core/factory.js"></script>
<script src="<%=contextName%>/resource/js/ajax/core/asynchronous.js"></script>
<script src="<%=contextName%>/resource/js/ajax/core/synchronous.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/array.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/select.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/label.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/page.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/percentBar.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/checkboxGroup.js"></script>
<script src="<%=contextName%>/resource/js/ajax/util/valueList.js"></script>
<script src="<%=contextName%>/resource/js/tabs/tabcontent.js"></script>
<script src="<%=contextName%>/resource/js/xp_progress.js"></script>
<script src="<%=contextName%>/resource/js/messageFunctions.js"></script>
<script src="<%=contextName%>/resource/js/xtree.js"></script>
<script src="<%=contextName%>/resource/js/webfxcheckboxtreeitem.js"></script>
<script src="<%=contextName%>/resource/js/numberFormat.js"></script>
<script src="<%=contextName%>/resource/js/jquery-1.3.2.js"></script>
<script src="<%=contextName%>/resource/js/jquery-maxlength.js"></script>

<script>
    var screenSize = getScreenSize();
    var screenWidth = screenSize.split(",")[0];
    var screenHeight = screenSize.split(",")[1];
</script>