<script>
$(function(){
	$('img[title!=""][help="yes"]').mouseover(function(e){
		if (this.title != '') {
			$('#contenidoHint').css("display", "block");
			$('#contenidoHint').html(this.title); //'<center>'+this.title + '</center>');
		    $('#popup').fadeIn('slow');
		    /*if ($(window).scrollTop() > $('#popup').offset().top) {
		    	$('#popup').css('margin-top', $(window).scrollTop() -  $('#popup').offset().top) + 15;
		    }*/

		    var anchoAdicional = 50;
		    if (this.title.length >= 1000) {
		    	$('#divContent').css("width", '450px');
		    	anchoAdicional = 100;
		    } else {
		    	$('#divContent').css("width", '300px');
		    }

		    $("#txtHint").val(this.title);
		    this.title = "";
		    var tPosX = e.pageX;

		    //if (($('#contenidoHint').attr("offsetWidth") + anchoAdicional + e.pageX) > window.screen.availWidth) {
		    var vwWidth;
		    var vwHeight;
		    if (navigator.userAgent.toUpperCase().indexOf('MSIE') > -1) {
		    	vwWidth = e.currentTarget.document.firstChild.offsetWidth;//e.fromElement.clientWidth;
		    	vwHeight = e.currentTarget.document.firstChild.offsetHeight;//e.fromElement.clientHeight;
		    } else {
		    	vwWidth = e.view.innerWidth;
		    	vwHeight = e.view.innerHeight;
		    }


		    if (($('#contenidoHint').attr("offsetWidth") + anchoAdicional + e.pageX) > vwWidth){
		    	tPosX -= ( $('#contenidoHint').attr("offsetWidth") + 50);
		    } else {
		    	tPosX += 10;
		    }

		    var tPosY = e.pageY - 100;

		    //if (((e.pageY + $('#contenidoHint').attr("offsetHeight") - 100) > window.screen.availHeight) && (tPosY - 250 > 0)) {
		    if (((e.pageY + $('#contenidoHint').attr("offsetHeight") - 100) > vwHeight) && (tPosY - 250 > 0)) {
		    	tPosY -= 250;
		    } else {
		    	tPosY - 100;
		    }


		    $('#popup').css({'position': 'absolute', 'top': tPosY, 'left': tPosX, 'opacity': 1});
		}

	});

	$('img[help="yes"]').mouseout(function(){
		if ($("#txtHint").val() != '') {
			this.title = $("#txtHint").val();
			$("#txtHint").val("");
		    $('#popup').fadeOut('fast');
		}
	});

	$('#close').click(function(){
	    $('#popup').fadeOut('slow');
	});
});
</script>
<input type="hidden" id="txtHint" value=""/>
<div id="popup" style="display: none;">
    <div id="divContent" class="content-popup">
        <!-- <div class="close"><a href="#" id="close">Ocultar</a></div>
        </br> -->
        <div id="contenidoHint"></div>
    </div>
</div>