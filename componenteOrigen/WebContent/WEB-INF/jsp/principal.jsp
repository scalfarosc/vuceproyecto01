<%@include file="/WEB-INF/jsp/taglibs.jsp"%>

<html>

    <head>
	    <title>Sistema COMPONENTE ORIGEN - Principal</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="robots" content="noindex,nofollow" />
    </head>

    <c:set var="activeInicio" value="active" scope="request" />
    
   <script language="JavaScript">
	
    function validarSubmit() {
        var f = document.formulario;
        return false;
    }

    
	
	</script>
	<style>
	
	.button-link {
	    background-color:#005A9C;
	    border: 1px solid #0d1a4c;
	    color:white;
	    padding: 5px 10px;
	    border-radius: 3px;
	    direction: rtl;
	    height: auto;
	    cursor: pointer;
	}

	.button-link:hover	{
		transition: 0.35s;
		background-color:#394b58;
		color: white;
	}	
	

    #wrapper {
        width: 100%;
    }
    #wrapper:after {
        clear: both;
        display: block;
        content: ' ';
    }
    #left_container{
    	float: left;
        width: 16%;
        height: 700px;
    }
    .container {
        float: left;
        width: 60%;
        margin-bottom: 10px;
    }
	#left_container .content {
        height: 78px;
        text-align:left;
        margin:15px;
        border-radius: 8px;  
    }
    #left_container .content p{
        color:#fff;
        font-size:12px;
        heigh:15px;
        padding:10px 5px;
    }
    #left_container .content p a{
     
        font-size:20px;
        heigh:20px;
        font-weight: bold; 
        color:#514C54;
    }
    .container .content {
        
    }

		
   </style>

    <body>
    	<div id="body">
    		<jsp:include page="/WEB-INF/jsp/header.jsp" />
    		<!-- CONTENT -->
    		<div id="contp"><div id="cont">
    			<form name="formulario" method="post" onSubmit="return validarSubmit();">
        			<input type="hidden" name="opcion" />
        			<input type="hidden" name="method" />

			        <p class="bienvenido">
			            <strong>${sessionScope.USUARIO.nombreCompleto}</strong>,
			            bienvenido al sistema COMPONENTE ORIGEN, desde aqu&iacute; podr&aacute;s realizar tus operaciones y tambi&eacute;n monitorearlas.

			        </p>
<div id="wrapper">
				
				 <c:if test="${ sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] != 'CO.ADMIN.HELP_DESK' &&
                     			 ( sessionScope.USUARIO.tipoOrigen != 'ET' ||
                     			   sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                     			   sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
                     			   sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA'
                     			 )}" >
               <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}" >
				<div id="left_container">

            			<div class="content" style ="background-color: #DCDCDC" >
						            <p><a href="javascript:listarCertificadoOrigen(0);">${cantidadSolicitudesCertificado}</a></p>
						            <div style ="background-color: #514C54"><p>Solicitudes </p></div> 
						</div>    
						<div class="content" style ="background-color: #DCDCDC" >  
						            <p><a href="javascript:listarCertificadoOrigen(1);">${cantidadBorradoresSolicitudesCertificado}</a></p>
						            <div style ="background-color: #514C54"><p>Borradores</p></div> 
				         </div> 
				         <div class="content" style ="background-color: #DCDCDC" > 
				            		<p><a href="javascript:listarDeclaracionJurada(0);">${cantidadDJRegistradas}</a></p>
						            <div style ="background-color: #514C54"><p>D. Juradas Registradas </p></div> 
						 </div>        
						 <div class="content" style ="background-color: #DCDCDC" >        
						            <p><a href="javascript:listarDeclaracionJurada(1);">${cantidadDJAsignadas}</a></p>
						            <div style ="background-color: #514C54"><p>D. Juradas Asignadas como productor</p></div> 
						   </div> 
				</div>  
				</c:if>
				 <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}" >
				 <div id="left_container">
				 
				 <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}">
				 	   <div class="content" style ="background-color: #DCDCDC" >
						            <p><a href="javascript:listarSolicitudesCertificadoEntidad(0);">${solicitudesSinAsignar}</a></p>
						            <div style ="background-color: #514C54"><p>Solicitudes por Asignar </p></div> 
						</div>  
						
						
						 <div class="content" style ="background-color: #DCDCDC" >
						            <p><a href="javascript:listarSolicitudesCertificadoEntidad(2);">${solicitudesPendientesRespuesta}</a></p>
						            <div style ="background-color: #514C54"><p>Solicitudes Pendientes de Aceptaci&oacute;n </p></div> 
						</div> 
						
						
						 <div class="content" style ="background-color: #DCDCDC" >
						            <p><a href="javascript:listarSolicitudesCertificadoEntidad(1);">${solicitudesAsignadas}</a></p>
						            <div style ="background-color: #514C54"><p>SUCEs Aceptada </p></div> 
						</div> 
					</c:if>
					 <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">	
						
							 <div class="content" style ="background-color: #DCDCDC" >
							            <p><a href="javascript:listarSolicitudesCertificadoEntidad(2);">${solicitudesPendientesRespuesta}</a></p>
							            <div style ="background-color: #514C54"><p>Solicitudes Pendientes de Aceptaci&oacute;n </p></div> 
							</div> 
							
							
							 <div class="content" style ="background-color: #DCDCDC" >
							            <p><a href="javascript:listarSolicitudesCertificadoEntidad(1);">${solicitudesAsignadas}</a></p>
							            <div style ="background-color: #514C54"><p>SUCEs Aceptadas </p></div> 
							</div> 
						</c:if>
						
						 <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA'}">
							 <div class="content" style ="background-color: #DCDCDC" >
							            <p><a href="javascript:listarSolicitudesCertificadoEntidad(1);">${solicitudesAsignadas}</a></p>
							            <div style ="background-color: #514C54"><p>SUCEs Aceptadas </p></div> 
							</div> 
						</c:if>
						
				 
				 </div>
				 
				 </c:if>
				  
				</c:if>
				
				<div class="container" style="text-align:left; padding: 20px; margin-left:20px; border: 1px solid #0d1a4c; background-color: #fff;">
				     	<h3 style="text-align: center"><b>COMUNICADO A LOS EXPORTADORES QUE TRAMITAN CERTIFICADOS DE ORIGEN</b></h3>
				     	<br/>
						
						<p>En relaci�n a la tramitaci�n de certificados de origen, en el contexto del Estado de Emergencia Nacional declarado, la Direcci�n de la Unidad de Origen, pone en conocimieno de los exportadores, lo siguiente:</p>
						<br/>
						<div style="margin-top:30px;">
						<a><button type="submit" formtarget="_blank" class="button-link link" onclick="window.open('https://www.vuce.gob.pe/comunicados/1/comunicado_exportadores.pdf','_blank');">Ver Comunicado completo</button></a>
						
						</div>
						<div style="margin-top:40px;">
						<p>
						Atentamente</p>
						<p style="font-weight: bold; font-style: italic;">Direcci�n de la Unidad de Origen</p>
						<br/>
						<p>Lima, 24 de marzo de 2020</p>
						
			
						</div>
				</div>
     	
			   <div class="container" style="text-align:left; padding: 20px; margin-left:20px; border: 1px solid #0d1a4c; background-color: #fff">
				     	<h3 style="text-align: center"><b>COMUNICADO</b></h3>
				     	<br/>
						<p>Estimados administrados VUCE,</p>
						<br/>
						<p>
						Por medio de la presente es grato informar que se encuentra disponible desde el 11/02/2019 en el Componente de Origen, la opci�n para los exportadores de <b> firmar digitalmente el CERTIFICADO DE ORIGEN en el ACUERDO MARCO DE LA ALIANZA PACIFICO</b>,  el cual ha reconocido la validez de la firma digital del certificado de origen, ello se alinea a la pol�tica de uso de "cero papel", facilitando  las exportaciones. 
						</p>
						<br/>
						<p>
						La implementaci�n de esta nueva funcionalidad ha representado un arduo esfuerzo entre los pa�ses miembros del acuerdo y un avance en la facilitaci�n del comercio.
						</p>
						<br/>
						<p>
						Firmar digitalmente tiene la ventaja de disminuir tiempos en la tramitaci�n, los costos de env�o, acredita la autenticidad del documento y adicionalmente cuando es aprobado y firmado digitalmente, se env�a de forma inmediata a la aduana del pa�s destino.  Por el momento solo est� disponible entre PERU y M�XICO (y viceversa) progresivamente se ir� habilitando para los dem�s pa�ses miembros del acuerdo. 
									</p>
						<div style="margin-top:30px;">
						<a><button type="submit" formtarget="_blank" class="button-link link" onclick="window.open('https://www.vuce.gob.pe/manual_vuce/manuales/usuarios/Cartilla_administrado-certificado-origen_v4.0.pdf','_blank');">Cartilla de funcionalidad de la Firma Electr�nica</button></a>
						
						</div>
						<div style="margin-top:40px;">
						<p>
						Atentamente</p>
				
						<p style="font-weight: bold; font-style: italic;">Plataforma VUCE</p>
						<br/>
			
						</div>
				</div>
			    			
			    			
			   	<div class="container" style="text-align:left; padding: 20px; margin-left:20px; border: 1px solid #0d1a4c; background-color: #fff">
					<h3 style="text-align: center"><b>MEDICI�N DE LA SATISFACCI�N DE LA VUCE</b></h3>
			     	<br/>
					<p>Estimado ${sessionScope.USUARIO.nombreCompleto},</p>
					<br/>
					<p>La VUCE es una herramienta de facilitaci�n del comercio exterior que permite a las partes involucradas en el comercio y transporte internacional gestionar, a trav�s de medios electr�nicos, los tr�mites requeridos por las diversas entidades competentes, de acuerdo a la normatividad vigente.</p>   
					<br/>
					<p>La informaci�n que suministres en esta encuesta se utilizar� exclusivamente para el an�lisis y medici�n de satisfacci�n de los servicios brindados por la VUCE y no ser� compartida con terceras partes. La encuesta es an�nima y s�lo tomar� aproximadamente 4 minutos.</p>
					<br/>
					<p>Sus comentarios son muy valiosos e importantes para nosotros.</p>
					<br/>
					<p>�Muchas gracias por dedicar su tiempo a participar en la encuesta!
					</p>
					<div style="margin-top:30px;">
					<a>
					<button type="submit" formtarget="_blank" class="button-link link" onclick="window.open('https://forms.gle/fMHThJe1PNm3KTkm6','_blank');">Completa la encuesta</button>
					</a>
					
					</div>
					<div style="margin-top:40px;">
				
					<p>Atentamente</p>
					<p style="font-weight: bold; font-style: italic;">Plataforma Vuce</p>
					<br/>
					</div>
				</div> 			
			      
</div>

<div style="height:500px;"></div>
        		</form>

        	</div></div>

			<jsp:include page="/WEB-INF/jsp/footer.jsp" />

        </div>
    </body>
</html>