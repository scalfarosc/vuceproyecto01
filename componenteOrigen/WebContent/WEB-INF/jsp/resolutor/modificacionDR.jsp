<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
    String controller = (String)request.getAttribute("controller");
%>
<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
<script type="text/javascript">

        var contador = 0;

        function validarSubmit() {
            var f = document.formulario;
            if (f.button.value=="cancelarButton") {
                return false;
            }
            if (f.button.value=="cerrarPopUpButton") {
                return validaCerraPopUp();
            }
            if (f.button.value=="crearModifButton") {
                disableButton("crearModifButton", true);
            }
            if (f.button.value=="rectificarDRButton") {
                if (!confirm("�Desea generar una rectificaci�n para este DR?")) {
                    return false;
                }
                disableButton("rectificarDRButton", true);
                disableButton("confirmarRectificacionDRButton", true);
                disableButton("eliminarRectificacionDRButton", true);
                disableButton("confirmarFraccionamientoDRButton", true);
                disableButton("eliminarFraccionamientoDRButton", true);
                disableButton("verDRFraccionamientoButton", true);
                disableButton("rectificarDRButton", true);
                disableButton("fraccionarDRButton", true);
            }
            if (f.button.value=="fraccionarDRButton") {
                if (!confirm("�Desea generar un fraccionamiento para este DR?")) {
                    return false;
                }
                disableButton("rectificarDRButton", true);
                disableButton("confirmarRectificacionDRButton", true);
                disableButton("eliminarRectificacionDRButton", true);
                disableButton("confirmarFraccionamientoDRButton", true);
                disableButton("eliminarFraccionamientoDRButton", true);
                disableButton("verDRFraccionamientoButton", true);
                disableButton("rectificarDRButton", true);
                disableButton("fraccionarDRButton", true);
            }
            if (f.button.value=="actualizarModifButton") {
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
            }
            if (f.button.value=="transmitirModifButton") {
            	if (!confirm("�Seguro que desea transmitir la Solicitud de Rectificaci�n de DR?")) {
                    return false;
                }
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
            }
            if (f.button.value=="eliminarModifButton") {
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
            }
            if (f.button.value=="eliminarButton") {
                if (!confirm("�Seguro que desea eliminar el archivo?")) {
                    return false;
                }
            }
            if (f.button.value=="aceptarModificacionDRButton") {
                if (!confirm("�Est� seguro que desea aceptar la Solicitud de Rectificaci�n del DR?")) {
                    return false;
                }
                disableButton("aceptarModificacionDRButton", true);
                disableButton("rechazarModificacionDRButton", true);
            }
            if (f.button.value=="aprobarButton") {


                if (!confirm("�Est� seguro que desea aprobar la Rectificaci�n del DR?")) {
                    return false;
                }


                //f.target = window.parent.name;


                /*var drId = f.drId.value;
                var sdr = f.sdr.value;
                var suceId = f.suceId.value;
                var ordenId = f.ordenId.value;
                var mto = f.mto.value;
                var controller = "/admentco.htm";
                var modificacionDrId = f.modificacionDrId.value;
                var mensajeId = f.mensajeId.value;
                showPopWin(contextName+controller+"?method=aprobarModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId, 500, 230, null);
                return false;*/
            }
            if (f.button.value=="rectificarButton") {


                if (!confirm("�Est� seguro que desea aprobar la Rectificaci�n del DR?")) {
                	var f = document.formulario;
                    f.action = contextName + "/admentco.htm";
                    f.method.value = "rectificarDR";
                    f.button.value = "aceptarButton";
                    f.target = "_self";
                }


                //f.target = window.parent.name;


                /*var drId = f.drId.value;
                var sdr = f.sdr.value;
                var suceId = f.suceId.value;
                var ordenId = f.ordenId.value;
                var mto = f.mto.value;
                var controller = "/admentco.htm";
                var modificacionDrId = f.modificacionDrId.value;
                var mensajeId = f.mensajeId.value;
                showPopWin(contextName+controller+"?method=aprobarModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId, 500, 230, null);
                return false;*/
            }
            if (f.button.value=="rechazarModificacionDRButton") {
                var drId = f.drId.value;
                var sdr = f.sdr.value;
                var suceId = f.suceId.value;
                var ordenId = f.ordenId.value;
                var mto = f.mto.value;
                var controller = "/admentco.htm";
                var modificacionDrId = f.modificacionDrId.value;
                var mensajeId = f.mensajeId.value;
                showPopWin(contextName+controller+"?method=rechazoModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId, 650, 450, null);
                return false;
            }
            if (f.button.value=="reemplazarDRButton") {
            	$("#divAprobacionRechazo").show();
            	return false;
            }
            return true;
        }

        function validarCampos(){
            var f = document.formulario;
            var ok = true;
            var mensaje="Debe ingresar : ";

            if (f.mensaje.value==""){
                mensaje +="\n -El mensaje";
                changeStyleClass("co.label.buzon.mensaje", "errorValueClass");
            }else changeStyleClass("co.label.buzon.mensaje", "labelClass");

            if(mensaje!="Debe ingresar : ") {
                ok= false;
                alert (mensaje);
            }
            return ok;
        }

        function guardarModificacion(){
            var f = document.formulario;
            if (validarCampos()) {
                f.action = contextName+"/origen.htm";
                f.method.value="guardarModificacionDR";
                f.button.value="crearModifButton";
            } else {
                f.button.value="cancelarButton";
            }
        }

        function transmitirModificacionResolutor() {
            var f = document.formulario;
            if(validarCampos()){
                f.action = contextName+"/origen.htm%>";
                f.method.value="transmiteModificacionDRResolutor";
                f.button.value="transmitirModifButton";
            }else{
                f.button.value="cancelarButton";
            }
        }

        function actualizarModificacion(){
            var f = document.formulario;
            f.action = contextName+"/<%=controller%>";
            f.method.value="actualizaModificacionDR";
            f.button.value="actualizarModifButton";
        }

        function eliminarModificacion(){
            var f = document.formulario;
            if (!confirm("�Est� seguro de eliminar este registro?")) {
                f.button.value="cancelarButton";
               } else{
                   f.action = contextName+"/<%=controller%>";
                f.method.value="eliminarModificacionDR";
                f.button.value="eliminarModifButton";
                f.target = window.parent.name;
                window.parent.hidePopWin(false);
           }

        }

        function transmitirModificacion(){
            var f = document.formulario;
            if(validarCampos()){
                f.action = contextName+"/<%=controller%>";
                f.method.value="transmiteModificacionDR";
                f.button.value="transmitirModifButton";
            }else{
                f.button.value="cancelarButton";
            }
        }

        function cargarArchivo() {
            var f = document.formulario;
            if (f.archivo.value !="") {
                f.action = contextName+"/<%=controller%>";
                f.method.value="cargarArchivoModificacionDR";
                f.button.value="cargarButton";
            } else {
                alert("Debe seleccionar alg�n archivo");
                f.button.value="cancelarButton";
            }
        }

        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/origen.htm";
            f.method.value="descargarAdjunto";
            f.idAdjunto.value=adjuntoId;
            f.submit();
        }

        function eliminarRegistro() {
	         var f = document.formulario;
	         var indice = -1;
	         if (f.seleccione!=null) {
	             if (eval(f.seleccione.length)) {
	                 for (i=0; i < f.seleccione.length; i++) {
	                     if (f.seleccione[i].checked){
	                         indice = i;
	                     }
	                 }
	             } else {
	                 if (f.seleccione.checked) {
	                     indice = 0;
	                 }
	             }
	         }
	         if (indice!=-1) {
	             f.action = contextName+"/<%=controller%>";
	             f.method.value="eliminarAdjuntoModificacionDR";
	             f.button.value="eliminarButton";

	         } else {
	             alert('Seleccione por lo menos un registro a eliminar');
	             f.button.value = 'cancelarButton';
	         }
        }

        function aceptarModificacionDR() {
            var f = document.formulario;
            f.action = contextName+"/adment.htm";
            f.method.value = "aceptarModificacionDR";
            f.button.value = "aceptarModificacionDRButton";
            f.target = window.parent.name;
        }

        function aprobarModificacionDR() {

            var f = document.formulario;
            f.action = contextName + "/admentco.htm";
            f.method.value = "aprobarModificacionDR";
            f.button.value = "aprobarButton";
            f.target = "_self";
            /*
            var f = document.formulario;
            f.action = contextName+"/admentco.htm";
            f.button.value = "aprobarModificacionDRButton";
            f.target = "_self";*/
        }

        function nuevaRectificacionDR() {

            var f = document.formulario;
            f.action = contextName + "/admentco.htm";
            f.method.value = "rectificarDR";
            f.button.value = "rectificarButton";
            f.target = window.parent.name;
            /*
            var f = document.formulario;
            f.action = contextName+"/admentco.htm";
            f.button.value = "aprobarModificacionDRButton";
            f.target = "_self";*/
        }



        function rechazarModificacionDR() {
            var f = document.formulario;
            f.action = contextName+"/admentco.htm";
            f.button.value = "rechazarModificacionDRButton";
            f.target = "_self";
        }

        function rectificarDR() {
            var f = document.formulario;
            f.action = contextName + "/<%=controller%>";
            f.method.value = "rectificarDR";
            f.button.value = "rectificarDRButton";
            f.target = window.parent.name;
        }

        function fraccionarDR() {
            var f = document.formulario;
            f.action = contextName + "/<%=controller%>";
            f.method.value = "fraccionarDR";
            f.button.value = "fraccionarDRButton";
            f.target = window.parent.name;
        }

        function reemplazarDR() {
        	var f = document.formulario;
        	f.button.value = "reemplazarDRButton";
        }

        function procederReemplazoDR() {
            var f = document.formulario;
            f.action = contextName + "/<%=controller%>";
            f.method.value = "reemplazarDR";
            f.button.value = "procederReemplazoDRButton";
            f.target = window.parent.name;
        }

        function validaCerraPopUp(){
			var f = document.formulario;			
			window.parent.hidePopWin(false);
			//window.parent.parent.cargarDatosDocResolutivo(window.parent.coDrId.value);
			return false;
        }

        $(document).ready(function() {
            $('textarea').focus(function() {
                $(this).removeClass("inputTextClass").addClass("focusField");
            });
            $('textarea').blur(function() {
                   if(this.value == this.defaultValue){
                       $(this).removeClass("focusField").addClass("inputTextClass");
                   }
            });

            var transmitido = document.getElementById("transmitido").value;
            if (transmitido=="S") {
                bloquearControles();
                $('input[name="tipoReemplazoDR"]').attr('readonly',false);
                $('input[name="tipoReemplazoDR"]').attr('disabled',false);
            }
        });


</script>

<body id="contModal">
    <jlis:messageArea width="100%" /><br/>
    <jlis:modalWindow />
    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
        <input type="hidden" name="idAdjunto">
        <jlis:value name="drId" type="hidden" />
        <jlis:value name="sdr" type="hidden" />
        <jlis:value name="ordenId" type="hidden" />
        <jlis:value name="suceId" type="hidden" />
        <jlis:value name="mto" type="hidden" />
        <jlis:value name="idFormato" type="hidden" />
        <jlis:value name="formato" type="hidden" />
        <jlis:value name="estadoRegistro" type="hidden" />
        <jlis:value name="modificacionDrId" type="hidden" />
        <jlis:value name="mensajeId" type="hidden" />
        <jlis:value name="transmitido" type="hidden" />
        <jlis:value name="controller" type="hidden" />
        <jlis:value name="numSuce" type="hidden" />
        <jlis:value name="tipoModificacionDR" type="hidden" />

        <co:validacionBotonesFormato formato="MTC001SDR" pagina="modificacionEvalDR" />

        <table>
             <tr>
             <%-- <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
	             <c:if test="${desestimiento == 'N'}"> --%>
		             <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aprobarButton" name="aprobarModificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Aprobar " alt="Pulse aqu� para aprobar la solicitud de modificaci�n del DR" /></td>
		             <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="rechazarButton" name="rechazarModificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Rechazar " alt="Pulse aqu� para rechazar la solicitud de modificaci�n del DR" /></td>
		             <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevaRectificacionDRButton" name="nuevaRectificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Iniciar Rectificaci�n DR " alt="Pulse aqu� para crear una nueva DR" /></td>
	         <%--     </c:if>
             </c:if> --%>

             <td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar Ventana " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" /></td>
             </tr>
        </table>
        <br>
        <div id="divAprobacionRechazo" style="display:none;padding:5px;">
            <input type="radio" name="tipoReemplazoDR" value="A" checked />Aprobaci�n&nbsp;<input type="radio" name="tipoReemplazoDR" value="R" />Rechazo&nbsp;&nbsp;&nbsp;&nbsp;
            <jlis:button code="CO.ENTIDAD.EVALUADOR" id="procederReemplazoDRButton" name="procederReemplazoDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Generar DR de Reemplazo" alt="Pulse aqu� para generar el DR de Reemplazo" />
        </div>
        <table class="form" width="96%">
            <tr>
                <th align="left">
                    <jlis:label key="co.label.buzon.mensaje"/>
                </th>
            </tr>
            <tr>
                <td>
                    <jlis:textArea name="mensaje" rows="10" style="width:98%"/>
                </td>
            </tr>
        </table>
        <br>
        <c:if test="${!empty modificacionDrId && transmitido == 'N' && desestimiento == 'N'}">
            <table>
                 <tr>
                     <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="cargarButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
                     <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="eliminarButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
                 </tr>
            </table>
            <table class="form">
                <tr>
                    <td>
                        <jlis:label key="co.label.documentos_adjuntar"/>
                        <jlis:label key="co.label.tipo_archivos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="file" id="archivo" name="archivo" size="80" />
                    </td>
                </tr>
            </table>
        </c:if>
        <c:if test="${!empty modificacionDrId}">
            <jlis:table  name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntos" scope="request" pageSize="*" fixedHeader="yes" navigationHeader="yes" width="95%" >
              <jlis:tr>
                    <jlis:td name="ADJUNTO ID" nowrap="yes" />
                    <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
                    <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
                    <jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
                <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
                <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes"  />
                <jlis:columnStyle column="4" columnName="SELECCIONE" editable="yes" align="center" hide="${hide}"/>
                <jlis:tableButton column="4" type="checkbox" name="seleccione" />
                <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
            </jlis:table>
        </c:if>
        <%--c:if test="${tipo == 'S' && estadoRegistro == 'S'}">
            <br/>
            <table class="form">
                <tr>
                    <th align="left">
                        <jlis:label labelClass="impar" key="co.label.notificaciones"/>
                    </th>
                </tr>
            </table>
            <jlis:table keyValueColumns="ID" name="notificaciones" source="tNotificaciones" scope="request" pageSize="*" width="60%" navigationHeader="yes"
                validator="pe.gob.mincetur.vuce.vc.web.util.NotificacionesSubsanacionRowValidator" method="validarEliminarMensaje" >
                <jlis:tr>
                    <jlis:td name="ID" nowrap="yes" width="5%"/>
                    <jlis:td name="DE" nowrap="yes" width="5%"/>
                    <jlis:td name="MENSAJE" nowrap="yes" width="70%" />
                    <jlis:td name="FECHA REGISTRO" nowrap="yes" width="25%"/>
                    <jlis:td name="" width="1%" style="valueCheckbox=SELECCIONE,on-off,S,N"  />
                    <jlis:td name="DETALLES" />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="ID" hide="yes"/>
                <jlis:columnStyle column="2" columnName="DE" hide="yes" />
                <jlis:columnStyle column="3" columnName="MENSAJE" editable="yes" />
                <jlis:columnStyle column="4" columnName="FECHA_REGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" />
                <jlis:columnStyle column="5" columnName="SELECCIONE" editable="${seleccionarNotificacion}" align="center" value="S" />
                <jlis:columnStyle column="6" columnName="DETALLES" hide="yes" />
                <jlis:tableButton column="5" type="valueCheckbox" uncheckedValue="N" />
                <jlis:tableButton column="3" type="link" onClick="cargarNotificacionDetalle" />
              </jlis:table>
        </c:if--%>
    </form>
    <script>
        window.parent.document.getElementById("popupTitle").innerHTML = "Solicitud de Rectificaci�n de DR";
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
    </script>
</body>
</html>