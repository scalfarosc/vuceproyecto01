<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Notificaci�n de Subsanaci�n de SUCE</title>
    </head>
    
    <script language="JavaScript"
        src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
    <script>
        $(document).ready(function() {
                var estado = document.getElementById("estado").value;
                if (estado == "T") {
                    $('select').attr('disabled','-1');
                    $('input[type="text"]').attr('readonly',true);
                    $('input[type="text"],select').addClass("readonlyInputTextClass");
                    $('textarea').attr('readonly',true);
                    $('textarea').addClass("readonlyInputTextClass");
                }
                
            });

        function validarSubmit() {
            var f = document.formulario;
            if (f.button.value == "cancelarButton") {
                window.parent.hidePopWin(true);
                return false;
            }
            if (f.button.value == "eliminarButton") {
                if (!confirm("�Desea eliminar la presente Notificaci�n de Subsanaci�n de SUCE?")) {
                    return false;
                }
                disableButton("aceptarButton", true);
                disableButton("enviarButton", true);
                disableButton("eliminarButton", true);
                disableButton("cancelarButton", true);
                disableButton("cargarAdjuntoButton", true);
                disableButton("eliminarAdjuntoButton", true);
            }
            if (f.button.value == "aceptarButton") {
                if (f.mensaje.value=="") {
                    alert("Debe ingresar el mensaje");
                    return false;
                }
                disableButton("aceptarButton", true);
                disableButton("enviarButton", true);
                disableButton("eliminarButton", true);
                disableButton("cargarAdjuntoButton", true);
                disableButton("eliminarAdjuntoButton", true);
            }
            if (f.button.value == "enviarButton") {
                if (!confirm("�Est� seguro que desea enviar al usuario la presente Notificaci�n de Subsanaci�n de SUCE?")) {
                    return false;
                }
                disableButton("aceptarButton", true);
                disableButton("enviarButton", true);
                disableButton("eliminarButton", true);
                disableButton("cargarAdjuntoButton", true);
                disableButton("eliminarAdjuntoButton", true);
            }
            return true;
        }
        
        function validarSubmitFormularioAdjuntos() {
            var f = document.formularioAdjuntos;
            if (f.button.value == "cancelarButton") {
                return false;
            }
            if (f.button.value == "cargarAdjuntoButton") {
                disableButton("aceptarButton", true);
                disableButton("enviarButton", true);
                disableButton("eliminarButton", true);
                disableButton("cargarAdjuntoButton", true);
                disableButton("eliminarAdjuntoButton", true);
            }
            if (f.button.value == "eliminarButton") {
                disableButton("aceptarButton", true);
                disableButton("enviarButton", true);
                disableButton("eliminarButton", true);
                disableButton("cargarAdjuntoButton", true);
                disableButton("eliminarAdjuntoButton", true);
            }
            return true;
        }
        
        /**
         * Registra una Notificaci�n de Subsanaci�n
        **/
        function aceptar() {
            var f = document.formulario;
            /*alert(f.ordenId.value);
            alert(f.mto.value);*/
            f.action = contextName + "/admentco.htm";
            f.method.value = "registrarNotificacionSubsanacionSuce";
            f.button.value = "aceptarButton";
            f.target = "_self";//window.parent.parent.name;
        }
        
        /**
         * Envia una Notificaci�n de Subsanaci�n
        **/
        function enviar() {
            var f = document.formulario;
            f.action = contextName + "/admentco.htm";
            f.method.value = "enviarNotificacionSubsanacionSuce";
            f.button.value = "enviarButton";
            f.target = window.parent.name;
        }
        
        /**
         * Elimina una Notificaci�n de Subsanaci�n
        **/
        function eliminar() {
            var f = document.formulario;
            f.action = contextName + "/admentco.htm";
            f.method.value = "eliminarNotificacionSubsanacionSuce";
            f.button.value = "eliminarButton";
            f.target = window.parent.name;
        }
        
        function cancelar() {
            var f = document.formulario;
            f.button.value = "cancelarButton";
        }
        
        /**
         * Carga un archivo de Notificaci�n de Subsanaci�n
        **/
        function cargarArchivo() {
            var f = document.formularioAdjuntos;
            /*alert(f.ordenId.value);
            alert(f.mto.value);*/
            if (f.archivo.value != "") {
                f.action = contextName + "/admentco.htm";
                f.method.value = "cargarArchivoNotificacion";
                f.ordenId.value = f.ordenId.value;
                f.button.value = "cargarAdjuntoButton";
            } else {
                alert("Debe seleccionar alg�n archivo");
                f.button.value = "cancelarButton";
            }
        }
        
        /**
         * Descarga un archivo de Notificaci�n de Subsanaci�n
        **/
        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formularioAdjuntos;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName + "/origen.htm";
            f.method.value = "descargarAdjunto";
            f.idAdjunto.value = adjuntoId;
            f.submit();
        }
        
        /**
         * Elimina un adjunto de Notificaci�n de Subsanaci�n
        **/
        function eliminarRegistro() {
            var f = document.formularioAdjuntos;
            var indice = -1;
            if (f.seleccione!=null) {
                if (eval(f.seleccione.length)) {
                    for (i=0; i < f.seleccione.length; i++) {
                        if (f.seleccione[i].checked){
                            indice = i;
                        }    
                    }
                } else {
                    if (f.seleccione.checked) {
                        indice = 0;
                    }
                }
            }
            if (indice!=-1) {
                f.action = contextName + "/admentco.htm";
                f.method.value = "eliminarAdjuntoNotificacion";
                f.button.value = "eliminarButton";
            } else {
                alert("Seleccione por lo menos un registro a eliminar");
                f.button.value = 'cancelarButton';
            }
        }
        
        $(document).ready(function() {     
            $('textarea').focus(function() {   
                $(this).removeClass("inputTextClass").addClass("focusField");   
            });   
            $('textarea').blur(function() {   
            	if (this.value == this.defaultValue) {   
                    $(this).removeClass("focusField").addClass("inputTextClass");   
            	}
            });   
        });  
        
    </script>
    <body id="contModal">
        <jlis:modalWindow />
        <jlis:messageArea width="100%" />
        <br />
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="ordenId" type="hidden" />
            <jlis:value type="hidden" name="mto" />
            <jlis:value name="suceId" type="hidden" />
            <jlis:value name="notificacionId" type="hidden" />
            <jlis:value name="mensajeId" type="hidden" />
            <jlis:value name="estado" type="hidden" />
            <jlis:value name="formato" type="hidden" />
            <jlis:value name="idAcuerdo" type="hidden" />
            <jlis:value name="estadoAcuerdoPais" type="hidden" />

            <table>
                <tr>
                    <c:if test="${estado == 'P'}">
                    	<c:if test="${!empty notificacionId}">
                    		<%-- 20170131_GBT ACTA CO-004-16 Y CO-009-16 punto 4.3.b--%>
                    		
                    		<c:if test="${estadoAcuerdoPais != 'I'}">
                    			<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="enviarButton" name="enviar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para enviar el mensaje y sus adjuntos" /></td>
                    		</c:if>
                    		<c:if test="${estadoAcuerdoPais == 'I'}">
                    			<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="enviarButton" editable="NO" name="enviar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para enviar el mensaje y sus adjuntos" /></td>
                    		</c:if>
                    	</c:if>
                    	<c:if test="${empty notificacionId}">
                    		<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aceptarButton" name="aceptar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Grabar" alt="Pulse aqu� para grabar los datos" /></td>
                    	</c:if>
                    	<c:if test="${!empty notificacionId}">
                    		<%-- 20170131_GBT ACTA CO-004-16 Y CO-009-16 punto 4.3.b--%>
                    		<c:if test="${estadoAcuerdoPais != 'I'}">
                    			<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aceptarButton" name="aceptar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar los datos" /></td>
                    			<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="eliminarButton" name="eliminar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar el mensaje y sus adjuntos" /></td>
                    		</c:if>
                    		<c:if test="${estadoAcuerdoPais == 'I'}">
                    			<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aceptarButton" editable="NO" name="aceptar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar los datos" /></td>
                    			<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="eliminarButton" editable="NO" name="eliminar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar el mensaje y sus adjuntos" /></td>
                    		</c:if>
                    	</c:if>
                    </c:if>
                    <td><jlis:button id="cancelarButton" name="cancelar()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cancelar la operaci�n y cerrar la ventana" /></td>
                </tr>
            </table>
            <table class="form" >
                <tr><th align="left"><jlis:label key="co.label.buzon.mensaje"/></th></tr>
                <tr><td><jlis:textArea name="mensaje" rows="10" style="width:100%" /></td></tr>
            </table>
            <div style="color:red; text-align:left;padding:10px;">
                <div>Luego de grabar se mostrar� la interface para Cargar archivos y Transmitir</div>
            </div>
        </form>
        <br>
        <c:if test="${!empty notificacionId}">
	        <form name="formularioAdjuntos" method="post" enctype="multipart/form-data" onSubmit="return validarSubmitFormularioAdjuntos();">
	            <input type="hidden" name="method" />
	            <input type="hidden" name="button">
	            <jlis:value name="ordenId" type="hidden" />
            	<jlis:value type="hidden" name="mto" />
	            <jlis:value name="suceId" type="hidden" />
	            <jlis:value name="notificacionId" type="hidden" />
	            <jlis:value name="idAdjunto" type="hidden" />
	            <jlis:value name="mensajeId" type="hidden" />
            	<jlis:value name="formato" type="hidden" />
	            <c:if test="${estado == 'P'}">
	            <table>
	                 <tr>
	                 <%-- 20170131_GBT ACTA CO-004-16 Y CO-009-16 punto 4.3.b--%>
	                 <c:if test="${estadoAcuerdoPais != 'I'}">
	                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="cargarAdjuntoButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
	                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="eliminarAdjuntoButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
	                 </c:if>
	                 <c:if test="${estadoAcuerdoPais == 'I'}">
	                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="cargarAdjuntoButton" editable="NO" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
	                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="eliminarAdjuntoButton" editable="NO" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
	                 </c:if>
	                 </tr>
	            </table>
	            <table>
	                <tr>
	                    <td>
	                        <jlis:label key="co.label.documentos_adjuntar"/><br/>
	                        <jlis:label key="co.label.tipo_archivos"/>
	                    </td>
	                </tr>
	                <tr>
	                    <td><input type="file" id="archivo" name="archivo" size="80"  /></td>
	                </tr>
	            </table>
	            </c:if>
	            <jlis:table name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntosNotificacion" scope="request" pageSize="*" navigationHeader="no" >
	                <jlis:tr>
	                    <jlis:td name="ADJUNTO ID" nowrap="yes" />
	                    <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
	                    <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
	                    <jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
	                </jlis:tr>
	                <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
	                <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
	                <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes"  />
	                <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
	                <c:if test="${estado == 'T'}">
	                	<jlis:columnStyle column="4" columnName="SELECCIONE" hide="yes"/>
	                </c:if>
	                <c:if test="${estado == 'P'}">
	                	<jlis:columnStyle column="4" columnName="SELECCIONE" align="center" editable="yes"/>
	                </c:if>
	                <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
	                <jlis:tableButton column="4" type="checkbox" name="seleccione" />
	            </jlis:table>
	        </form>
        </c:if>
        <script>
            window.parent.document.getElementById("popupTitle").innerHTML = "Notificaci�n de Subsanaci�n de SUCE";
            window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
        </script>
    </body>
</html>