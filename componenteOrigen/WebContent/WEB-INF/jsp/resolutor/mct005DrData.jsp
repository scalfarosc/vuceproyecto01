<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>
<html>
	<head>
	<meta http-equiv="Pragma" content="no-cache" />
	</head>
	<body>
        <jlis:value name="coDrId" type="hidden" />
		<table class="form">
			<tr>
				<td colspan="3" class="formatoCaption">Datos Generales</td>
			</tr>
			<c:if test="${ estadoRegistro == 'A' }" >
				<tr>
					<th width="200">RUC: </th>
					<td colspan="4"><jlis:value name="DR.djNumeroDocumento" editable="no" type="text" size="18" maxLength="15" align="center" style="width:92%"/></td>
				</tr>
				<tr>
					<th width="200">Documento Resolutivo: </th>
					<td colspan="4"><jlis:value name="DR.dr" editable="no" type="text" size="18" maxLength="15" align="center" style="width:92%"/></td>
				</tr>
			</c:if>
            <c:if test="${ sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' }">
				<tr>
					<th width="200">Expediente Entidad: </th>
					<td colspan="4"><jlis:value name="DR.expedienteEntidad" editable="no" type="text" size="24" maxLength="15" align="center" style="width:92%"/></td>
				</tr>
			</c:if>
		</table>
		<table class="form">
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
		</table>
		<table class="form">
			<tr>
				<td colspan="3" class="formatoCaption">Datos Declaración Jurada</td>
			</tr>
			<c:if test="${ estadoRegistro == 'A' }" >
				<tr>
					<th>Número de DJ: </th>
					<td colspan="4"><jlis:value name="DR.djCodigo" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
				</tr>
			</c:if>
			<tr>
				<th>Denominación: </th>
				<td colspan="4"><jlis:textArea name="DR.djDenominacion" editable="no" cols="60" rows="3"/></td>
			</tr>
			<tr>
				<th>Característica: </th>
				<td colspan="4"><jlis:textArea name="DR.djCaracteristica" editable="no" cols="60" rows="3"/></td>
			</tr>
			<tr>
				<th>Sub Partida Nacional: </th>
				<td colspan="4"><jlis:textArea name="DR.djPartidaArancelaria" editable="no" cols="60" rows="4"/></td>
			</tr>
			<c:if test="${ estadoRegistro == 'A' }" >
				<tr>
					<th width="200">Fecha de Inicio de Vigencia: </th>
					<td colspan="4"><jlis:value name="DR.djFechaInicioVigencia" editable="no" type="text" pattern="dd/MM/yyyy" align="center" style="width:92%"/></td>
				</tr>
				<tr>
					<th width="200">Fecha de Fin de Vigencia: </th>
					<td colspan="4"><jlis:value name="DR.djFechaFinVigencia" editable="no" type="text" pattern="dd/MM/yyyy" align="center" style="width:92%"/></td>
				</tr>
			</c:if>
		</table>
		<table class="form">
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
		</table>

	</body>
</html>