<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes Asignadas</title>
        <meta http-equiv="Pragma" content="no-cache" />
        <script type="text/javascript">
        
        //20170203_GBT ACTA CO-004-16 Y ACTA CO 009-16
    	$(document).ready(function() {
    		//Se cambi� a estadoAcuerdoPais2 para diferenciar de estadoAcuerdoPais por que mostraba alertas repetidas
            var estadoAcuerdoPais = '${estadoAcuerdoPais2}';
    		if (estadoAcuerdoPais=='I') {
    			alert("No se puede procesar la solicitud, el acuerdo comercial con este pa�s no se encuentra vigente");
    		}
    		
    	});
    
    

	    	// Aceptaci�n de asignaci�n por el evaluador
	    	function aceptarAsignacion(keyValues, keyValuesField){
	    		var f = document.formulario;
	            var orden = document.getElementById(keyValues.split("|")[0]).value;
	            var mto = document.getElementById(keyValues.split("|")[1]).value;
	            var suceId = document.getElementById(keyValues.split("|")[2]).value;
	            f.action = contextName+"/admentco.htm?method=aceptarDesignacion&orden="+orden+"&mto="+mto+"&suceId="+suceId;
	            f.target = "_self";
	            f.submit();
	            	            
	    	}

	    	// Rechazo de la asignaci�n por el evaluador
	    	function rechazarAsignacion(keyValues, keyValuesField){
	    		if (confirm("Esta seguro que desea rechazar la asignaci�n de la SUCE?")) {
		    		var f = document.formulario;
		            var orden = document.getElementById(keyValues.split("|")[0]).value;
		            var mto = document.getElementById(keyValues.split("|")[1]).value;
		            f.action = contextName+"/admentco.htm?method=rechazarDesignacion&orden="+orden+"&mto="+mto;
		            f.target = "_self";
		            f.submit();
	    		} else {
	    			return false;
	    		}
	    	}
        </script>
    </head>
    <body>
		<jlis:table keyValueColumns="ORDENID,MTO,SUCE_ID,NOMBREFORMATO" name="CO_PEND_ACEPTAR" key="resolutor.evaluador.pendiente_aceptacion.grilla"
			scope="request" pageSize="15" width="90%" navigationHeader="yes" filter="${filterBandeja}" validator="pe.gob.mincetur.vuce.co.web.util.SolicitudesPrioritariasRowValidator" >
				<jlis:tr>
					<jlis:td name="TUPA" nowrap="yes" />
					<jlis:td name="FORMATO" nowrap="yes" />
					<jlis:td name="NOMBRE" nowrap="yes" />
					<jlis:td name="ORDENID" nowrap="yes" />
					<jlis:td name="MTO" nowrap="yes" />
					<jlis:td name="SUCEID" nowrap="yes" width="8%" />
					<jlis:td name="ACUERDO INTERNACIONAL ID" nowrap="yes" />
					<jlis:td name="ACUERDO" nowrap="yes" width="20%" />
					<jlis:td name="PAIS" nowrap="yes" width="20%" />
					<jlis:td name="USUARIO" nowrap="yes" width="25%"/>
					<jlis:td name="SOLICITUD" nowrap="yes" />
					<jlis:td name="SUCE" nowrap="yes" width="8%" />
					<jlis:td name="NOMBRE EVALUADOR" nowrap="yes" width="25%"/>
					<jlis:td name="ESTADO REGISTRO ID" nowrap="yes" />
					<jlis:td name="ESTADO DEL REGISTRO" nowrap="yes" width="15%"/>
					<jlis:td name="FECHA DE REGISTRO" nowrap="yes" width="10%" />
					<jlis:td name="FECHA DE TRANSMISI�N" nowrap="yes" />
					<jlis:td name="NOMBRE ENTIDAD" nowrap="yes" />
					<jlis:td name="ENTIDAD" nowrap="yes" />
					<jlis:td name="TUPA ID" nowrap="yes" />
					<jlis:td name="FORMATO ID" nowrap="yes" />
					<jlis:td name="PRIORIDAD" nowrap="yes" />
					<jlis:td name="ACEPTAR" nowrap="yes" width="5%"/>
					<jlis:td name="RECHAZAR" nowrap="yes" width="5%"/>
				</jlis:tr>
				<jlis:columnStyle column="1" columnName="NOMBRETUPA" />
				<jlis:columnStyle column="2" columnName="NOMBREFORMATO" />
				<jlis:columnStyle column="3" columnName="NOMBRECUT" />
				<jlis:columnStyle column="4" columnName="ORDENID" hide="yes" />
				<jlis:columnStyle column="5" columnName="MTO" hide="yes" />
				<jlis:columnStyle column="6" columnName="SUCE_ID" hide="yes" />
				<jlis:columnStyle column="7" columnName="ACUERDOINTERNACIONALID" hide="yes" />
				<jlis:columnStyle column="8" columnName="NOMBREACUERDOINTERNACIONAL" />
				<jlis:columnStyle column="9" columnName="NOMBREPAIS" />
				<jlis:columnStyle column="10" columnName="NOMBREUSUARIO" />
				<jlis:columnStyle column="11" columnName="ORDEN" />
				<jlis:columnStyle column="12" columnName="SUCE" hide="yes" />
				<jlis:columnStyle column="13" columnName="NOMBREEVALUADOR" />
				<jlis:columnStyle column="14" columnName="ESTADOREGISTRO" hide="yes" />
				<c:choose>
				<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="15" columnName="DESCRIPCIONESTADOREGISTRO" hide="yes" />
				</c:when>
				<c:otherwise>
				<jlis:columnStyle column="15" columnName="DESCRIPCIONESTADOREGISTRO" />
				</c:otherwise>
				</c:choose>
				<jlis:columnStyle column="16" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" />
				<jlis:columnStyle column="17" columnName="FECHATRANSMISION" hide="yes" />
				<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' || sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="18" columnName="NOMBREENTIDAD" />
				</c:if>
				<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] != 'CO.CENTRAL.OPERADOR_FUNCIONAL' && sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] != 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="18" columnName="NOMBREENTIDAD" hide="yes" />
				</c:if>
				<jlis:columnStyle column="19" columnName="ENTIDADID" hide="yes" />
				<jlis:columnStyle column="20" columnName="TUPAID" hide="yes" />
				<jlis:columnStyle column="21" columnName="FORMATOID" hide="yes" />
				<jlis:columnStyle column="22" columnName="PRIORIDAD" hide="yes" />
				<c:choose>
				<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="23" columnName="ACEPTAR" hide="yes" />
				</c:when>
				<c:otherwise>
				<jlis:columnStyle column="23" columnName="ACEPTAR" editable="yes" align="center" />
				</c:otherwise>
				</c:choose>
				<c:choose>
				<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="24" columnName="RECHAZAR" hide="yes" />
				</c:when>
				<c:otherwise>
				<jlis:columnStyle column="24" columnName="RECHAZAR" editable="yes" align="center" />
				</c:otherwise>
				</c:choose>
				<c:choose>
     				<c:when test="${bloqueado == 'N'}">
						<jlis:tableButton column="23" type="imageButton" image="/co/imagenes/check_aceptar.gif" onClick="aceptarAsignacion" alt="Aceptar asignaci�n" />
				 	</c:when>
					<c:otherwise>
					    <jlis:tableButton column="23" type="imageButton" image="/co/imagenes/check_aceptar.gif" onClick="aceptarAsignacion" alt="Aceptar asignaci�n" />
					</c:otherwise>
				</c:choose>
 				<c:choose>
     					<c:when test="${bloqueado == 'N'}">
						<jlis:tableButton column="24" type="imageButton" image="/co/imagenes/rechazar.png" onClick="rechazarAsignacion" alt="Rechazar asignaci�n" />
				 	</c:when>
					<c:otherwise>
					    <jlis:tableButton column="24" type="imageButton" image="/co/imagenes/rechazar.png" onClick="rechazarAsignacion" alt="Rechazar asignaci�n" />
					</c:otherwise>
				</c:choose>
			    <%-- <jlis:tableButton column="23" type="imageButton" image="" onClick="" alt="Rechazar asignaci�n" /> --%>
		 	</jlis:table>

    </body>
</html>