<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema COMPONENTE ORIGEN - Suce</title>
<meta http-equiv="Pragma" content="no-cache" />
</head>
<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>

<jsp:include page="/WEB-INF/jsp/funciones.jsp" />

<script language="JavaScript">

	// Inicializaciones de JQuery
	$(document).ready(function() {
	    initializetabcontent("maintab");
	    var bloqueado = document.getElementById("bloqueado").value;
	    if (bloqueado == "S") {
	    	bloquearControles();
	    }
	    // Realiza la inicializaci�n de controles de acuerdo al estado de la SUCE y a otras condiciones
	    inicializarControles();
	});

	// Realiza la inicializaci�n de controles de acuerdo al estado de la SUCE y a otras condiciones
	function inicializarControles() {
		var numeroBorradoresDr = document.getElementById("tableSize_DOCS_RESOLUTIVOS").value;
		// Si hay un borrador de DR (o DR si ya se transmiti�) se deshabilitan los botones
		// Esta condici�n es suficiente, puesto que cuando se transmita el borrador, seguir� habien un elemento en la tabla
		if ( numeroBorradoresDr == 1 ) {
			$("#nuevoBorradorButton").attr('disabled',true).removeClass("buttonSubmitClass").addClass("buttonSubmitDisabledClass");
			$("#nuevoBorradorDenegButton").attr('disabled',true).removeClass("buttonSubmitClass").addClass("buttonSubmitDisabledClass");
		}
		$('#divDireccionAdicional').hide();	//oculta los datos de direcciones adicionales cuando abre la SUCE (ACCION SUCE)	
	}

	// Crea una Notificaci�n de Subsanaci�n
	function nuevaNotificacionSubsanacion() {
	    var f = document.formulario;
	    var ordenId = f.ordenId.value;
	    var mto = f.mto.value;
	    var suceId = f.suceId.value;
	    var formato = f.formato.value;
	    //20170131_GBT ACTA CO-004-16 Y CO-009-16
	    var estadoAcuerdoPais = f.estadoAcuerdoPais.value;
	    f.button.value = "modalButton";
	    showPopWin(contextName+"/admentco.htm?method=editarNotificacionSubsanacionSuce&ordenId="+ordenId+"&mto="+mto+"&suceId="+suceId+"&formato="+formato+"&estadoAcuerdoPais="+estadoAcuerdoPais, 650, 550, refrescar);
	}

	// Edita una Notificaci�n de Subsanaci�n
	function editarNotificacionSubsanacion(keyValues, keyValuesField) {
		var f = document.formulario;
	    var ordenId = f.ordenId.value;
	    var mto = f.mto.value;
	    var suceId = f.suceId.value;
	    var notificacionId = document.getElementById(keyValues.split("|")[1]).value;
	    var formato = f.formato.value;
	    //20170131_GBT ACTA CO-004-16 Y CO-009-16 se agrego idAcuerdo
        var idAcuerdo = f.idAcuerdo.value;
        var estadoAcuerdoPais = f.estadoAcuerdoPais.value;
	    f.button.value = "modalButton";
	    //20170131_GBT ACTA CO-004-16 Y CO-009-16 se agrego idAcuerdo
	    showPopWin(contextName+"/admentco.htm?method=editarNotificacionSubsanacionSuce&ordenId="+ordenId+"&idAcuerdo="+idAcuerdo+"&mto="+mto+"&suceId="+suceId+"&notificacionId="+notificacionId+"&formato="+formato+"&estadoAcuerdoPais="+estadoAcuerdoPais, 650, 550, refrescar);
	}

	// Refresca la p�gina
	function refrescar() {
	    var f = document.formulario;
	    f.action = contextName+"/admentco.htm";
	    f.method.value = "abrirSuce";
	    f.target = "_self";
	    f.submit();
	}

	// Muestra el detalle de la subsanaci�n
	function verModificacionSUCE(keyValues, keyValuesField, table) {
	    var f = document.formulario;
	    var ordenId = f.ordenId.value;
	    var mto = f.mto.value;
	    var modificacionSuceId = document.getElementById(keyValues.split("|")[0]).value;
	    var mensajeId = document.getElementById(keyValues.split("|")[1]).value;
	    var suceId = f.suceId.value;
	    var estadoRegistro = f.estadoRegistro.value;
	    var controller = f.controller.value;
	    var opcion = document.getElementById("opcion").value;
	    
	    //20170113_GBT ACTA CO-004-16 Y CO-009-16 se agrego idAcuerdo
	    var idAcuerdo = f.idAcuerdo.value;
        //alert(idAcuerdo);
        
        var estadoAcuerdoPais = f.estadoAcuerdoPais.value;
            
            
        
        //20170113_GBT ACTA CO-004-16 Y CO-009-16 se agrego idAcuerdo
	    showPopWin(contextName+"/admentco.htm?method=cargarModificacionSuce&ordenId="+ordenId+"&idAcuerdo="+idAcuerdo+"&mto="+mto+"&suceId="+suceId+"&estadoAcuerdoPais="+estadoAcuerdoPais+"&modificacionSuceId="+
	    		modificacionSuceId+"&mensajeId="+mensajeId+"&estadoRegistro="+estadoRegistro+"&controller="+controller+"&opcion="+opcion+"&formato="+f.formato.value+"&numDRBorrador=${numDRBorrador}", 720, 620, refrescar);
	    
	}

	// Crea el borrador de aprobaci�n de dr
	function crearBorradorAprobacionDr(){
		return crearBorradorDr('A');
	}

	// Crea el borrador de denegaci�n de dr
	function crearBorradorDenegacionDr(){
		return crearBorradorDr('R');
	}

	// Crea un borrador de dr, del tipo indicado
	function crearBorradorDr(tipoDr){
		var f = document.formulario;

		if (validarGeneracionDr(tipoDr)) {
			var suceId = $("#suce").val();
			f.action = "<%=contextName%>/admentco.htm";
	        f.method.value="generarBorrador";
	        f.button.value="generarBorradorDrButton";
	        f.tipoDr.value=tipoDr;
	        $("#nuevoBorradorButton").attr('disabled',true).removeClass("buttonSubmitClass").addClass("buttonSubmitDisabledClass");
			$("#nuevoBorradorDenegButton").attr('disabled',true).removeClass("buttonSubmitClass").addClass("buttonSubmitDisabledClass");
	        f.submit();
		} else {
			f.button.value="cancelarButton";
		}
	}

    function validarGeneracionDr(tipoDr){
    	var res = true;
    	//gbt 20190621 tk 149093. Se agreg� las siguientes dos lineas
    	var f=document.formulario;
    	var formato = f.formato.value;

    	// 20140924JFR Cambio por Reexportacion
    	<c:if test="${esReexportacion=='N'}">
    	if (tipoDr == 'A' && parseInt('${numDJsRechazadas}', 10)>0) {
   			alert('No puede generar un DR de Aprobaci�n debido a que existen DJs que se han marcado como "No califica"');
   			res = false;
   		//} else if (tipoDr == 'R' && parseInt('${numDJsRechazadas}', 10) == 0) {
   		//gbt 20190621 tk 149093. Se agreg� una condici�n m�s para que except�e el formato mct003 en la validaci�n 	
   		} else if (tipoDr == 'R' && parseInt('${numDJsRechazadas}', 10) == 0 && formato != 'mct003') {	
   			alert('No puede generar un DR de Denegaci�n debido a que todas las DJs de la SUCE han sido calificadas positivamente.');
   			res = false;
   		} else
 	    </c:if>
 	    if (parseInt('${numSubsanacionRecib}', 10) > 0) {
			res = confirm("Existe una subsanaci�n pendiente de aprobaci�n, esta seguro de continuar?");
    	}

		return res;
    }

	// Muestra los datos del documento resolutivo
    function verDatosDocResolutivo(keyValues, keyValuesField){
         var f=document.formulario;
         var mct001DrId = document.getElementById(keyValues.split("|")[0]).value;
         var ordenId = f.orden.value;
         var mto = f.mto.value;
         var numSuce = f.numSuce.value;
         var trabajaResolutor = "S";
         var formato = f.formato.value;
         var tipoDr = document.getElementById(keyValues.split("|")[2]).value;
         showPopWin("<%=contextName%>/admentco.htm?method=cargarDatosDocumentoResolutivo&mct001DrId="+mct001DrId+"&ordenId="+ordenId+"&mto="+mto+"&numSuce="+numSuce+"&trabajaResolutor="+trabajaResolutor+"&formato="+formato+"&codTipoDr="+tipoDr, 900, 550, null);
         f.button.value="modalButton";
         f.target = "_self";
    }

	// Recarga la invocaci�n de vista del documento resolutivo de esta p�gina, es invocada desde una pantalla hija - modificacionDR.jsp
    function cargarDatosDocResolutivo(coDrId){
         var f=document.formulario;
         var mct001DrId = coDrId;
         var ordenId = f.orden.value;
         var mto = f.mto.value;
         var numSuce = f.numSuce.value;
         var trabajaResolutor = "S";
         var formato = formato;
         showPopWin("<%=contextName%>/admentco.htm?method=cargarDatosDocumentoResolutivo&mct001DrId="+mct001DrId+"&ordenId="+ordenId+"&mto="+mto+"&numSuce="+numSuce+"&trabajaResolutor="+trabajaResolutor+"&formato="+formato, 900, 550, null);
         f.button.value="modalButton";
         f.target = "_self";
    }

    // Refresca la p�gina de abrirSuce
    function actualizar() {
        var f = document.formulario;
        f.action = "<%=contextName%>/admentco.htm";
        f.method.value="abrirSuce";
        f.submit();
    }

    // Muestra los datos del certificado origen
    function verDatosCertificado(){
    	var f = document.formulario;
    	f.action = contextName+"/origen.htm";
    	f.method.value = "cargarInformacionOrden";
    	f.submit();
    }

	// Crear o Editar DJ
	function abrirDJ(){
       	var f = document.formulario;
       	var orden = f.orden.value;
       	var mto = f.mto.value;
       	var idAcuerdo = f.idAcuerdo.value;
        var djId = $("#MCT005\\.djId").val();
	    var bloqueado = 'S';
	    var formato = f.formato.value.toLowerCase();
	    var puedeTransmitir = f.puedeTransmitir.value;
        //alert('idAcuerdo' + idAcuerdo);
        //alert('djId' + djId);
        if (djId == '') {
        	djId = '0';
        }
        //alert('en abrirDJ');
	    showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&formato="+formato+"&bloqueado="+bloqueado+"&modoFormato=S&puedeTransmitir="+puedeTransmitir, 1100, 550, null);
		f.button.value = "abrirDjButton";
	}

	function asociarDj(){
		return;
	}

    function aceptarDesestimientoSUCE() {
        var f = document.formulario;
        f.action = contextName+"/admentco.htm";
        f.method.value = "aceptarDesestimientoSUCE";
        f.button.value = "aceptarDesestimientoSUCEButton";
        f.target = "_self";
    }

    function rechazarDesestimientoSUCE() {
        var f = document.formulario;
        f.action = contextName+"/admentco.htm";
        f.method.value = "rechazarDesestimientoSUCE";
        f.button.value = "rechazarDesestimientoSUCEButton";
        f.target = "_self";
    }
    
  //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
    function estadoAcuerdoPaisInactivo(keyValues, keyValuesField){
    	var f = document.formulario;
    	f.button.value = "nuevaFacturaButton";
    	alert("No se puede procesar la solicitud, el acuerdo comercial con este pa�s no se encuentra vigente");
    	    	
	}



</script>

	<body>
		<div id="body"><jsp:include page="/WEB-INF/jsp/header.jsp" /> <!-- CONTENT -->

		<div id="contp">
		<div id="cont"><jlis:modalWindow onClose="actualizar()" />
		<form name="formulario" method="post" onSubmit="return validarSubmit();">
			<jlis:value type="hidden" name="method" />
			<jlis:value type="hidden" name="button" />
			<jlis:value type="hidden" name="ordenId" />
			<jlis:value type="hidden" name="suceId" />
			<jlis:value type="hidden" name="tipoDr" />
			<jlis:value type="hidden" name="MCT005.djId" />
			
			
			<table>
	                <tr>
	                    <td colspan="2" >&nbsp;</td>
	                </tr>
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.pais_acuerdo" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombrePais" editable="no" /></td>
	                </tr>
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.acuerdo" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombreAcuerdo" editable="no" /></td>
	                </tr>
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.entidad_certificadora" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombreEntidad" editable="no" /></td>
	                </tr>
                    <tr>
                        <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.tipo_certificado" />&nbsp;&nbsp;</th>
                        <td><jlis:value name="tipoCertificado" editable="no" /></td>
                    </tr>
	                <!-- tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.sede_entidad_certificadora" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombreSede" editable="no" /></td>
	                </tr -->
	                <tr>
	                    <td colspan="2" >&nbsp;</td>
	                </tr>
	            </table>

			<jsp:include page="/WEB-INF/jsp/informacionGeneral.jsp" />

        	<co:validacionBotonesFormato formato="Suce" />

			<div class="block btabs btable"><div class="blocka"><div class="blockb">
			<ul id="maintab" class="tabs">
				<li class="selected"><a href="#" rel="tabGeneral"><span>Datos del Solicitante</span></a></li>
                <c:if test="${!empty modificacionSuceXMts && modificacionSuceXMts =='S'}">
	                <li><a href="#" rel="tabNotificacionesSubsanacion"><span>Notif. de Subsanaci�n</span></a></li>
	                <li><a href="#" rel="tabSubsanaciones"><span>Subsanaciones</span></a></li>
	            </c:if>
				<li><a href="#" rel="tabCrearDr"><span>Docs. Resolutivos</span></a></li>
			</ul>
			<div id="tabGeneral" class="tabcontent">
				<jsp:include page="/WEB-INF/jsp/solicitante.jsp" />
			</div>
            <c:if test="${!empty modificacionSuceXMts && modificacionSuceXMts =='S'}">
	            <div id="tabNotificacionesSubsanacion" class="tabcontent">
	                <table class="form">
	                    <tr>
	                    	<%-- 20170131_GBT ACTA CO-004-16 3.2.b Y ACTA CO 009-16--%>
	                    	<c:if test="${estadoAcuerdoPais != 'I'}">
	                        <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevaNotificacionSubsanacionButton" name="nuevaNotificacionSubsanacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Nueva Notificaci�n" alt="Pulse aqu� para agregar una Nueva Notificaci�n de Subsanaci�n" /></td>
	                        </c:if>
	                        <c:if test="${estadoAcuerdoPais == 'I'}">
	                        <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevaNotificacionSubsanacionButton" name="estadoAcuerdoPaisInactivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Nueva Notificaci�n" alt="Pulse aqu� para agregar una Nueva Notificaci�n de Subsanaci�n" /></td>
	                        </c:if>
	                    </tr>
	                </table>
	                <jlis:table name="NOTIFICACION_SUCE" pageSize="6" keyValueColumns="SUCE_ID,NOTIFICACION_ID" source="tNotificacionesSuce" scope="request" navigationHeader="yes" width="100%" >
	                    <jlis:tr>
	                        <jlis:td name="SUCE_ID" />
	                        <jlis:td name="NOTIFICACION_ID" />
	                        <jlis:td name="FECHA REGISTRO" nowrap="yes" width="5%"/>
	                        <jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
	                        <jlis:td name="ESTADO" nowrap="yes" width="5%"/>
	                        <jlis:td name="FECHA ATENCI�N" nowrap="yes" width="5%"/>
	                        <jlis:td name="ACCION" nowrap="yes" width="5%"/>
	                    </jlis:tr>
	                    <jlis:columnStyle column="1" columnName="SUCE_ID" hide="yes" />
	                    <jlis:columnStyle column="2" columnName="NOTIFICACION_ID" hide="yes" />
	                    <jlis:columnStyle column="3" columnName="FECHA_RECEPCION" editable="no"  type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
	                    <jlis:columnStyle column="4" columnName="MENSAJE" />
	                    <jlis:columnStyle column="5" columnName="ESTADO" align="center" />
	                    <jlis:columnStyle column="6" columnName="FECHA_RESPUESTA" editable="no"  type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
	                    <jlis:columnStyle column="7" columnName="VER" align="center" editable="yes" validator="pe.gob.mincetur.vuce.co.web.util.NotificacionesSubsanacionCellValidator" />
	                    <jlis:tableButton column="7" type="imageButton" image="/vuce/imagenes/ver.gif" onClick="editarNotificacionSubsanacion" alt="" />
	                </jlis:table>
	            </div>
	            <div id="tabSubsanaciones" class="tabcontent">
	                <jlis:table name="SUBSANACION_SUCE" pageSize="6" keyValueColumns="MODIFICACION_SUCE,MENSAJE_ID" source="tSubsanacionesSuce" scope="request" navigationHeader="yes" width="100%">
	                    <jlis:tr>
	                        <jlis:td name="SUCE ID" nowrap="yes" />
	                        <jlis:td name="MODIFICACION SUCE" nowrap="yes" />
	                        <jlis:td name="FECHA REGISTRO" nowrap="yes" width="5%"/>
	                        <jlis:td name="MENSAJE ID" nowrap="yes"/>
	                        <jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
	                        <jlis:td name="ESTADO" nowrap="yes" width="5%"/>
	                        <jlis:td name="FECHA RESPUESTA" nowrap="yes" width="5%"/>
	                        <jlis:td name="VER" nowrap="yes" width="5%"/>
	                    </jlis:tr>
	                    <jlis:columnStyle column="1" columnName="SUCE_ID" hide="yes" />
	                    <jlis:columnStyle column="2" columnName="MODIFICACION_SUCE" hide="yes" />
	                    <jlis:columnStyle column="3" columnName="FECHA_REGISTRO" editable="no"  type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
	                    <jlis:columnStyle column="4" columnName="MENSAJE_ID" hide="yes" />
	                    <jlis:columnStyle column="5" columnName="MENSAJE" />
	                    <jlis:columnStyle column="6" columnName="ESTADO" align="center" />
	                    <jlis:columnStyle column="7" columnName="FECHA_RESPUESTA" editable="no"  type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
	                    <jlis:columnStyle column="8" columnName="VER" align="center" editable="yes" validator="pe.gob.mincetur.vuce.co.web.util.SubsanacionCellValidator" />
	                    <jlis:tableButton column="8" type="imageButton" image="/co/imagenes/ver.gif" onClick="verModificacionSUCE" alt="" />
	                </jlis:table>
	            </div>
	        </c:if>
			<div id="tabCrearDr" class="tabcontent">
				<table class="form">
					<tr>
						<td>
							<%-- 20170203_GBT ACTA CO-004-16 3.2.b Y ACTA CO 009-16--%>
	                    	<c:if test="${estadoAcuerdoPais != 'I'}">
	                        	<jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevoBorradorButton" name="crearBorradorAprobacionDr()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Emitir DR de Aprobaci�n" alt="Pulse aqu� para emitir un DR de aprobaci�n" />&nbsp;
								<jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevoBorradorDenegButton" name="crearBorradorDenegacionDr()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Emitir DR de Denegaci�n" alt="Pulse aqu� para emitir un DR de denegaci�n" />
	                        </c:if>
	                        <c:if test="${estadoAcuerdoPais == 'I'}">
								<jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevoBorradorButton" editable="NO" name="crearBorradorAprobacionDr()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Emitir DR de Aprobaci�n" alt="Pulse aqu� para emitir un DR de aprobaci�n" />&nbsp;
								<jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevoBorradorDenegButton" editable="NO" name="crearBorradorDenegacionDr()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Emitir DR de Denegaci�n" alt="Pulse aqu� para emitir un DR de denegaci�n" />
							</c:if>
						</td>
					<tr><td colspan="2"><hr/></td></tr>
				</table>
					<jlis:table keyValueColumns="CO_DR_ID,SUCE_ID,TIPO_DR_CODIGO" name="DOCS_RESOLUTIVOS"
                            source="tDocResolutivos" scope="request" pageSize="*" navigationHeader="no" >
	                    <jlis:tr>
	                        <jlis:td name="MCT001_DR_ID" nowrap="yes" />
	                        <jlis:td name="SUCE_ID" nowrap="yes" />
	                        <jlis:td name="N�MERO DE DOCUMENTO" nowrap="yes" />
	                        <jlis:td name="TIPO DE DR" nowrap="yes" />
	                        <jlis:td name="FECHA DE REGISTRO" nowrap="yes" />
	                        <jlis:td name="COD TIPO DR" nowrap="yes" />
	                        <jlis:td name="DATOS" nowrap="yes" />
	                        <jlis:td name="" nowrap="yes" />
	                    </jlis:tr>
	                    <jlis:columnStyle column="1" columnName="CO_DR_ID" hide="yes"/>
	                    <jlis:columnStyle column="2" columnName="SUCE_ID" hide="yes"/>
	                    <jlis:columnStyle column="3" columnName="DESCRIPCION" />
	                    <jlis:columnStyle column="4" columnName="TIPO_DR" />
	                    <jlis:columnStyle column="5" columnName="FECHA_REGISTRO" />
	                    <jlis:columnStyle column="6" columnName="TIPO_DR_CODIGO" hide="yes"/>
	                    <jlis:columnStyle column="7" columnName="DATOS" align="center" editable="yes" validator="pe.gob.mincetur.vuce.co.web.util.SuceCellValidator" method="mostrarIconoDrSuce" />
						<jlis:tableButton column="7" type="imageButton" image="" onClick="verDatosDocResolutivo" alt="" />
	                    <c:choose>
	      					<c:when test="${transmitido == 'S'}">
                    			<jlis:columnStyle column="8" columnName="TOTAL_ADJ" align="center" editable="yes" validator="pe.gob.mincetur.vuce.co.web.util.SuceCellValidator" method="mostrarAlertaFirmaSuce" />
								<jlis:tableButton column="8" type="imageButton" image="" alt="" />
						 	</c:when>
							<c:otherwise>
							    <jlis:columnStyle column="8" columnName="TOTAL_ADJ" align="center" hide="yes" />
							</c:otherwise>
						</c:choose>
	                </jlis:table>
			</div>
			
			</div></div></div>
		</form>
		</div>
		</div>
		<jsp:include page="/WEB-INF/jsp/footer.jsp" /></div>
	</body>
</html>