<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>
<html>
	<head>
	<meta http-equiv="Pragma" content="no-cache" />
	<script>

	 $(document).ready(function() {

		 /*if (document.getElementById("DR.aceptacion").value == 'S'){
			 $("#aceptacion").attr('checked', true);

		 } else {
			 $("#aceptacion").attr('checked', false);
		 }*/

	 });

	</script>
	</head>
	<body>
       	<jlis:value name="coDrId" type="hidden" />
		<table class="form">
            <c:if test="${ sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' }">
				<tr>
					<th width="200">Nro. del Certificado de la Entidad</th>
					<td colspan="4">
						<jlis:value name="DR.expedienteEntidad" editable="no" type="text" align="center" style="width:92%"/>
					</td>
				</tr>
			</c:if>
			<tr>
			<%-- 20140717_JMC NuevoReq - No se Visualiza N�mero de Certificado - Uni�n Europea--%>
				<th width="200">Nro. del Certificado Origen: </th>
				<td colspan="4"><jlis:value name="DR.drEntidadNuevo" editable="no" type="text" size="50"/></td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">Datos Iniciales</td>
			</tr>
			<tr>
				<th width="200">Pais del acuerdo</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.nombrePais" style="width:92%" />
				</td>
			</tr>
			<tr>
				<th width="200">Nombre del acuerdo comercial</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.nombreAcuerdo" style="width:92%" />
				</td>
			</tr>
			<tr>
				<th width="200">Entidad Certificadora</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.entidadNombre" style="width:92%" />
				</td>
			</tr>
			<!-- tr>
				<th width="200">Sede Entidad Certificadora</th>
				<td colspan="4">
					<jlis:value name="DR.sedeNombre" editable="no" type="text" size="50" />
				</td>
			</tr -->
			<tr>
				<th width="200">Certificado a Reemplazar</th>
				<td colspan="4">
					<jlis:value name="DR.drEntidad" editable="no" type="text" size="50" />
				</td>
			</tr>
			<tr>
				<th width="200">DR a Reemplazar </th>
				<td colspan="4">
					<jlis:value name="DR.drIdOrigen" editable="no" type="text" size="50" />
				</td>
			</tr>
            <c:if test="${idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 19 || idAcuerdo == 18 || idAcuerdo == 27}">
            <tr>
                <th width="200">N�mero de Referencia del Certificado</th>
                <td colspan="4">
                    <jlis:value name="nroReferenciaCertificadoOrigen" editable="no" type="text" size="50"/>
                </td>
            </tr>
            </c:if>
			<tr>
				<th width="200">Fecha Emision del Certificado Original</th>
				<td colspan="4">
					<jlis:value name="DR.fechaDrEntidad" editable="no" type="text" pattern="dd/MM/yyyy" align="center" style="width:92%"/>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">Causal</td>
			</tr>
			<tr>
				<th width="200">Causal</th>
				<td colspan="4">
					<jlis:value name="DR.causalDuplicadoCo" editable="no" type="text" size="50" />
				</td>
			</tr>
			<tr>
				<th width="200">Sustento adicional</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.sustentoAdicional" style="width:92%" />
				</td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">Observaciones</td>
			</tr>
			<tr>
				<th>Observaciones</th>
				<td colspan="4"><jlis:textArea rows="2" editable="no" onKeyUp="valida_longitud(this, 1000);" name="DR.observacion" style="width:92%" /></td>
			</tr>
			<tr>
				<th><jlis:label key="co.label.resolutor.dr.observacion.evaluador" /></th>
				<td colspan="4">
					<c:choose>
						<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' && (estadoRegistro != 'V' && estadoRegistro != 'A' || (esRectificacion == 'S' && estadoSDR == 'P'))}">
					      	<jlis:textArea rows="2" editable="yes" name="DR.observacionEvaluador" onKeyUp="valida_longitud(this, 1000);" style="width:92%" />
					  	</c:when>
					  	<c:otherwise>
					  		<jlis:textArea rows="2" editable="no" name="DR.observacionEvaluador" style="width:92%" />
					  	</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr class="requiredValueClass" style="display:${(enIngles=='S')?'yes':'none'}" >
                <th>&nbsp;</th>
                <td colspan="4"><jlis:label key="co.alert.obs.eval.ingles" /></td>
            </tr>
		</table>
	</body>
</html>