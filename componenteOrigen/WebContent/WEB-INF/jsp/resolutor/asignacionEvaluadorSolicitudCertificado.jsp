<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema VUCE - Asignaci�n Evaluador</title>
</head>

<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
<script>

	// Inicializaciones de JQuery
	$(document).ready(function() {
		tituloPopUp("Asignar Evaluador");
	    validaCambios();
	});

	// Validaciones antes de realizar el submit
	function validarSubmit() {

		var f = document.formulario;

	    if (f.button.value=="grabarButton") {
	    	disableButton("grabarButton", true);
        }
	    if (f.button.value=="cancelarButton") {
            return false;
        }
	    if (f.button.value=="cerrarPopUpButton") {
        	return validaCerraPopUp();
        }

	    return true;
	}

	// Validaciones de obligatoriedad de campos
	function validarCampos(){

	    var ok = true;
	    var mensaje="Debe ingresar o seleccionar : ";

	    if (document.getElementById("evaluadorId").value==""){
	        mensaje +="\n -El evaluador";
	        changeStyleClass("co.label.asignar_evaluador.evaluador", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.asignar_evaluador.evaluador", "labelClass");	
	    }

	    if (document.getElementById("prioridad").value==""){
	        mensaje +="\n -La prioridad";
	        changeStyleClass("co.label.asignar_evaluador.prioridad", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.asignar_evaluador.prioridad", "labelClass");	
	    }
	    
	    if (mensaje!="Debe ingresar o seleccionar : ") {
	        ok= false;
	        alert (mensaje);
	    }
	    return ok;
	}

	// Realiza la asignaci�n del evaluador
	function asignarEvaluador() {
		var f = document.formulario;
        if (validarCampos()) {
        	f.action = contextName+"/admentco.htm";
	        f.method.value="asignarEvaluadorSolicitudCertificado";
	        f.button.value="grabarButton";
	        f.target = window.parent.name;
		} else {
			f.button.value="cancelarButton";
		}
    }

	</script>
	<body id="contModal">
        <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="filtroAcuerdo" type="hidden" />
	        <jlis:value name="filtroEstado" type="hidden" />
	        <jlis:value name="opcionFiltro" type="hidden" />
	        <jlis:value name="numeroOrden" type="hidden" />
	        <jlis:value name="numeroSUCE" type="hidden" />
	        <jlis:value name="seleccione" type="hidden" />
	        <jlis:value name="ordenId" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        
			<table class="form">

				<tr>
					<td colspan="5" nowrap>
                        <jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
						<jlis:button code="CO.ENTIDAD.SUPERVISOR" id="grabarButton" name="asignarEvaluador()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Asignar Evaluador" alt="Pulse aqu� para asignar un Evaluador a la Solicitud de Certificado" />
					</td>
				</tr>

				<tr>
					<td colspan="5"><hr/></td>
				</tr>

				<tr>
					<th colspan="2" ><jlis:label key="co.title.asignar_evaluador" /></th>
				</tr>
				<tr>
					<th><jlis:label key="co.label.asignar_evaluador.solicitud" /></th>
					<td ><jlis:value name="numeroSolicitud" size="20" maxLength="20" type="text" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.asignar_evaluador.evaluador" /></th>
					<td ><jlis:selectProperty name="evaluadorId" key="resolutor.administrador.evaluadores_entidad.select" filter="${filterEvaluadores}" /></td>
				</tr>
				<tr id="trPrioridadEvaluador" style="display:none" >
					<th><jlis:label key="co.label.asignar_evaluador.prioridad" /></th>
					<td ><jlis:selectProperty name="prioridad" key="comun.maestro.prioridad" value="N" /></td>
				</tr>

			</table>

		</form>
	</body>
</html>