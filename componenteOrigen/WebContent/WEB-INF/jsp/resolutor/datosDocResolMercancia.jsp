<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Sistema VUCE</title>
    </head>
    <script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/acuerdos/acuerdo${idAcuerdo}/solicitud_co_acuerdo_${idAcuerdo}.js"></script>
    <script>

    $(document).ready(function() {
    	inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo();
    });


    function validarSubmit() {
        var f=document.formulario;
        if (f.button.value=="cancelarButton") {
            return false;
        }
        if (f.button.value=="cerrarPopUpButton") {
            return validaCerraPopUpNoAction();
        }
        return true;
    }


    </script>
    <body id="contModal">
		<%@include file="/WEB-INF/jsp/helpHint.jsp" %>
        <jlis:modalWindow />
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">

        <jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />

        <table class="form">
        	<!--tr><th width="200">Secuencia: </th><td colspan="4"><jlis:value name="MERCANCIA.SECUENCIA_MERCANCIA" editable="no" type="text" align="center" style="width:92%"/></td></tr-->
			<tr><th width="200">Item: </th><td colspan="4"><jlis:value name="MERCANCIA.ITEM_CO_MERCANCIA" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrSubpartidaArancelaria" style="display:none" ><th width="200">Subpartida Arancelaria: </th><td colspan="4"><jlis:value name="MERCANCIA.PARTIDA_ARANCELARIA" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrDenominacion" style="display:none" ><th width="200">Denominaci�n Comercial de la Mercanc�a: </th><td colspan="4"><jlis:value name="MERCANCIA.DENOMINACION" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr><th width="200">Descripci�n para Certificado: </th>
				<td colspan="4">
					<jlis:textArea name="MERCANCIA.DESCRIPCION" cols="65" rows="3" editable="no" />
					<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.DESCRIPCION" />
				</td>
			</tr>
			<tr id="trMercanciaDrEsMercanciaGranel" style="display:none" >
				<th width="200">Su mercanc�a es "a granel"?: </th>
				<td colspan="4">
					<jlis:value name="MERCANCIA.ES_MERCANCIA_GRANEL" size="50" editable="no" type="text" align="center" style="width:92%"/>
					<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.A_GRANEL" />
				</td>
			</tr>
			<tr id="trMercanciaDrMarca" style="display:none" ><th width="200">Marcas y n�meros de paquetes: </th><td colspan="4"><jlis:value name="MERCANCIA.MARCAS_NUMEROS" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrTipoBulto" style="display:none" ><th width="200">Tipo de Bulto: </th><td colspan="4"><jlis:value name="MERCANCIA.TIPO_BULTO" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrCantidadBulto" style="display:none" ><th width="200">Cantidad de Bulto: </th><td colspan="4"><jlis:value name="MERCANCIA.CANTIDAD_BULTO" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrCantidad" style="display:none" ><th width="200">Cantidad<label class="lblPesoBruto" style="display:none"> (Peso Bruto)</label>: </th><td colspan="4"><jlis:value name="MERCANCIA.CANTIDAD" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrUnidadMedida" style="display:none" ><th width="200">Unidad de Medida<label class="lblPesoBruto" style="display:none"> (Peso Bruto)</label>: </th><td colspan="4"><jlis:value name="MERCANCIA.NOMBRE_COMERCIAL" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			
			<tr><th width="200">Calificaci�n: </th><td colspan="4"><jlis:value name="MERCANCIA.CALIFICACION" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trPartidaSegunAcuerdo">
			<th width="200">
				<c:choose>
					<c:when test="${idAcuerdo == 4 || idAcuerdo == 5}">Naladisa: </c:when>
					<c:when test="${idAcuerdo == 6 || idAcuerdo == 15 || idAcuerdo == 17 || idAcuerdo == 20 || idAcuerdo == 21 || idAcuerdo == 22}">Sistema Armonizado (6 d�gitos): </c:when>
					<c:when test="${idAcuerdo == 7 || idAcuerdo == 25 }">Sistema Armonizado (6 d�gitos): </c:when>
					<c:when test="${idAcuerdo == 9 || idAcuerdo == 12 || idAcuerdo == 23|| idAcuerdo == 27}">Subpartida arancelaria (4 d�gitos): </c:when>
					<c:otherwise>Nandina</c:otherwise>
				</c:choose>
			</th>
			<td colspan="4"><jlis:value name="MERCANCIA.PARTIDA_SEGUN_ACUERDO" editable="no" type="text" align="center" style="width:92%"/>
			<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.SUCE.DETALLE_CERTIFICADO.PARTIDA_SEGUN_ACUERDO" /></td></tr>
			<tr id="trMercanciaDrNumeroFactura" style="display:none" ><th width="200">N�mero de Factura: </th><td colspan="4"><jlis:value name="MERCANCIA.NUMERO" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trMercanciaDrValorFacturado" style="display:none" ><th width="200">Valor facturado (US$): </th><td colspan="4"><jlis:value name="MERCANCIA.VALOR_FACTURADO_US" size="50" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr id="trNorma"><th width="200">Norma: </th><td colspan="4"><jlis:textArea name="MERCANCIA.norma" editable="no" cols="70" rows="3"/></td></tr>
			<tr id="trCriterioOrigen"><th width="200">Criterio Origen: </th><td colspan="4"><jlis:textArea name="MERCANCIA.criterio_origen" cols="70" rows="3" editable="no" /></td></tr>
			<tr id="trCriterioOrigenCertif"><th width="200">Criterio Origen en Certificado: </th><td colspan="4"><jlis:textArea name="MERCANCIA.criterio_origen_certificado" cols="70" rows="3" editable="no"/></td></tr>
			<tr id="trMercanciaDrPorcentajeCriterio" style="display:none" ><th width="200">Porcentaje de Valor basado en el criterio: </th><td colspan="4"><jlis:value name="MERCANCIA.PORCENTAJE_SEGUN_CRITERIO" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<c:if test="${ codTipoDr == 'R' }">
				<tr><th width="200">Detalle Denegaci�n: </th><td colspan="4"><jlis:textArea name="MERCANCIA.detalle_denegacion" editable="no" cols="70" rows="3"/></td></tr>
			</c:if>
		</table>
        <br/>

        <script>
            window.parent.document.getElementById("popupTitle").innerHTML = "Mercanc�a";
            window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
        </script>
    </body>
</html>

