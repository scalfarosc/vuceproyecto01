<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Insert title here</title>
    </head>
<%
    String controller = (String) request.getAttribute("controller");
%>
    <script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
    <script type="text/javascript">

        function validarSubmit() {
            var f = document.formulario;
            if (f.button.value=="cancelarButton") {
                window.parent.hidePopWin(true);
                return false;
            }
            if (f.button.value=="rechazarModificacionSuceButton") {
                if (!confirm("�Est� seguro que desea rechazar la Subsanaci�n de Notificaci�n?")) {
                	return false;
                }
                var ordenId = f.ordenId.value;
                var mto = f.mto.value;
                var suceId = f.suceId.value;
                var modificacionSuceId = f.modificacionSuceId.value;
                var mensajeId = f.mensajeId.value;
                var formato = f.formato.value;
                showPopWin(contextName+"/admentco.htm?method=rechazoModificacionSubsanacion&ordenId="+ordenId+"&mto="+mto+"&suceId="+suceId+"&modificacionSuceId="+modificacionSuceId+"&mensajeId="+mensajeId+"&formato="+formato, 650, 550, null);
                return false;
            }
            if (f.button.value=="abrirModificacionSUCEButton") {
                disableButton("aceptarModificacionSuceButton", true);
                disableButton("aprobarModificacionSuceButton", true);
                disableButton("rechazarModificacionSuceButton", true);
                disableButton("abrirModificacionSUCEButton", true);
            }
            if (f.button.value=="nullActionButton") {
                return false;
            }
            return true;
        }

        function validarAprobacion(){
        	var res = true;
			if (parseInt('${numDRBorrador}', 10) > 0) {
				res = confirm("Esta acci�n har� que los borradores de DR que existan sean eliminados, esta seguro de continuar?");
        	}
			return res;
        }

        function aprobarModificacionSuce() {
            var f = document.formulario;

        	if (validarAprobacion()){
                f.action = contextName + "/admentco.htm";
                f.method.value = "aprobarModificacionSuce";
                f.button.value = "aprobarModificacionSuceButton";
                //alert(f.modificacionSuceId.value);
                f.target = window.parent.name;
        	} else {
                f.button.value = "nullActionButton";
        	}
        }

        function rechazarModificacionSuce() {
            var f = document.formulario;
            //alert('entre a rechazar');
            f.action = contextName+"/admentco.htm";
            f.button.value = "rechazarModificacionSuceButton";
            f.target = "_self";
        }

        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/origen.htm";
            f.method.value = "descargarAdjunto";
            f.idAdjunto.value = adjuntoId;
            f.target = "_self";
            f.submit();
        }

/*        function cargarNotificacionDetalle(keyValues, keyValuesField) {
            var idNotificacion = document.getElementById(keyValues.split("|")[0]).value;
            var f = document.formulario;
            showPopWin(contextName+"/adment.htm?method=cargarNotificacionDetalle&idNotificacion="+idNotificacion, 500, 230, null);
        }*/

        function cancelar() {
            var f = document.formulario;
            f.button.value = "cancelarButton";
        }

        function abrirModificacionSUCE() {
        	//alert("estoy en resolutor");
            var f = document.formulario;
            f.action = contextName+"/origen.htm";
            f.method.value="cargarInformacionOrden";
            f.button.value="abrirModificacionSUCEButton";
            //alert(f.mtoSUCE.value);
            //alert(f.formato.value);
            f.mto.value = f.mtoSUCE.value;
            f.target = window.parent.name;
        }

        $(document).ready(function() {
            $('textarea').focus(function() {
                $(this).removeClass("inputTextClass").addClass("focusField");
            });
            $('textarea').blur(function() {
                if(this.value == this.defaultValue){
                    $(this).removeClass("focusField").addClass("inputTextClass");
                }
            });
        });

        </script>

<body id="contModal">
    <jlis:messageArea width="100%" /><br/>
    <jlis:modalWindow />
    <form name="formulario" method="post" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
        <jlis:value name="ordenId" type="hidden" />
        <jlis:value name="orden" type="hidden" />
        <jlis:value name="mto" type="hidden" />
        <jlis:value name="suceId" type="hidden" />
        <jlis:value name="idFormato" type="hidden" />
        <jlis:value name="formato" type="hidden" />
        <jlis:value name="modificacionSuceId" type="hidden" />
        <jlis:value name="mensajeId" type="hidden" />
        <jlis:value name="transmitido" type="hidden" />
        <jlis:value name="estadoRegistro" type="hidden" />
        <jlis:value name="tipo" type="hidden" />
        <jlis:value name="idAdjunto" type="hidden" />
        <jlis:value name="controller" type="hidden" />
        <jlis:value name="utilizaModificacionSuceXMto" type="hidden" />
        <jlis:value name="opcion" type="hidden" />
        <jlis:value name="idAcuerdo" type="hidden" />
        <jlis:value name="estadoAcuerdoPais" type="hidden" />

        <c:if test="${!empty mtoSUCE}">
        <jlis:value name="mtoSUCE" type="hidden" />
        </c:if>

        <br>
        <table>
             <tr>
                 <c:if test="${utilizaModificacionSuceXMto == 'S' && estadoRegistro != 'I'}">
                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="abrirModificacionSUCEButton" name="abrirModificacionSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Modificaci�n de SUCE " alt="Pulse aqu� para abrir la modificaci�n de la Suce" /></td>
                 </c:if>
                 <c:if test="${estadoRegistro != 'A' && estadoRegistro != 'R' && estadoRegistro != 'G' && estadoRegistroSUCE != 'D' && estadoRegistro != 'I'}">
	                 <%--c:if test="${estadoRegistro == 'T'}">
	                 <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aceptarModificacionSuceButton" name="aceptarModificacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Aceptar " alt="Pulse aqu� para aceptar" /></td>
	                 </c:if--%>
	                 <c:if test="${estadoRegistro == 'T' || estadoRegistro == 'C'}">
	                 	<%-- 20170131_GBT ACTA CO-004-16 Y ACTA CO-009-16 4.4.a--%>
	                 	<c:if test="${estadoAcuerdoPais != 'I'}">
                 	    	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aprobarModificacionSuceButton" name="aprobarModificacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Aprobar " alt="Pulse aqu� para aprobar" /></td>
		                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="rechazarModificacionSuceButton" name="rechazarModificacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Rechazar" alt="Pulse aqu� para rechazar" /></td>
                     	</c:if>
                     	<c:if test="${estadoAcuerdoPais == 'I'}">
                 	    	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aprobarModificacionSuceButton" editable="NO" name="aprobarModificacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Aprobar " alt="Pulse aqu� para aprobar" /></td>
		                 	<td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="rechazarModificacionSuceButton" editable="NO" name="rechazarModificacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Rechazar" alt="Pulse aqu� para rechazar" /></td>
                     	</c:if>
	                 </c:if>
                 </c:if>
                 <td><jlis:button id="cancelarButton" name="cancelar()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cancelar la operaci�n y cerrar la ventana" /></td>
             </tr>
        </table>
        <table class="form" >
            <tr><th align="left"><jlis:label key="co.label.buzon.mensaje"/>
            <!-- Resolutor: </br>
            Estado Registro: ${estadoRegistro}</br>
            utilizaModificacionSuceXMto: ${utilizaModificacionSuceXMto}</br>  -->
            </th></tr>
            <tr><td><jlis:textArea name="mensaje" rows="10" style="width:100%" editable="no" /></td></tr>
            <c:if test="${estadoRegistro == 'R'}">
	            <tr><th align="left"><jlis:label key="co.label.buzon.mensaje_rechazo"/></th></tr>
	            <tr><td><jlis:textArea name="motivoRechazo" rows="4" style="width:100%" editable="no" /></td></tr>
            </c:if>
        </table>
        <br>
        <h3 class="psubtitle"><span><b><jlis:label key="co.title.adjuntos_rechazo" /></b></span></h3>
        <jlis:table name="ADJUNTOSRECHAZO" keyValueColumns="ADJUNTO_ID" source="tAdjuntosRechazo" scope="request" pageSize="*" navigationHeader="no" >
            <jlis:tr>
                <jlis:td name="ADJUNTO ID" nowrap="yes" />
                <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
            <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
            <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes"  />
            <jlis:columnStyle column="4" columnName="SELECCIONE" hide="yes"  />
            <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
        </jlis:table>
        <br>
        <h3 class="psubtitle"><span><b><jlis:label key="co.title.adjuntos" /></b></span></h3>
        <jlis:table name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntos" scope="request" pageSize="*" navigationHeader="no" >
            <jlis:tr>
                <jlis:td name="ADJUNTO ID" nowrap="yes" />
                <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
            <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
            <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes"  />
            <jlis:columnStyle column="4" columnName="SELECCIONE" hide="yes"  />
            <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
        </jlis:table>
        <c:if test="${tipo == 'S' && estadoRegistro != 'R'}">
        <h3 class="psubtitle"><span><b><jlis:label key="co.label.notificaciones" /></b></span></h3>
            <jlis:table keyValueColumns="NOTIFICACION_ID" name="notificaciones" source="tNotificaciones" scope="request" pageSize="*" width="60%" navigationHeader="no" >
                <jlis:tr>
                    <jlis:td name="NOTIFICACION ID" nowrap="yes" width="5%"/>
                    <jlis:td name="FECHA RECEPCI�N" nowrap="yes" width="25%"/>
                    <jlis:td name="MENSAJE" nowrap="yes" width="70%" />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="NOTIFICACION_ID" hide="yes"/>
                <jlis:columnStyle column="2" columnName="FECHA_RECEPCION" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
                <jlis:columnStyle column="3" columnName="MENSAJE" editable="no" />
                <jlis:tableButton column="3" type="link" onClick="cargarNotificacionDetalle" />
              </jlis:table>
        </c:if>
    </form>
    <script>
        <c:if test="${tipo == 'S'}">
        window.parent.document.getElementById("popupTitle").innerHTML = "Subsanaci�n de Notificaci�n de SUCE";
        </c:if>
        <%-- <c:if test="${tipo == 'M'}">
        window.parent.document.getElementById("popupTitle").innerHTML = "Escrito";
        </c:if> --%>
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
    </script>
</body>
</html>