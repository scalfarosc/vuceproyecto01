<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%><%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes Asignadas</title>
        <meta http-equiv="Pragma" content="no-cache" />
        <script type="text/javascript">
        	// Permite revisar información del certificado
			function verCertificadoAsignado(keyValues, keyValuesField){
	            var f = document.formulario;
	            f.orden.value = document.getElementById(keyValues.split("|")[0]).value;
	            f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
	            f.formato.value = document.getElementById(keyValues.split("|")[2]).value;
	            f.action = contextName+"/origen.htm";
	            f.method.value = "cargarInformacionOrden";
	            f.submit();
		    }
        </script>
    </head>
    <body>
		<jlis:table keyValueColumns="ORDENID,MTO,NOMBREFORMATO" name="CO_ASIGNADOS" key="resolutor.supervisor-evaluador.asignadas.grilla"
		scope="request" pageSize="15"  navigationHeader="yes" filter="${filterBandeja}"
		divStyle="overflow:auto" fixedHeader="yes" validator="pe.gob.mincetur.vuce.co.web.util.SolicitudesPrioritariasRowValidator" >
			<jlis:tr>
				<jlis:td name="TUPA" nowrap="yes" />
				<jlis:td name="FORMATO" nowrap="yes" />
				<jlis:td name="NOMBRE" nowrap="yes" />
				<jlis:td name="ORDENID" nowrap="yes" />
				<jlis:td name="MTO" nowrap="yes" />
				<jlis:td name="SUCE ID" nowrap="yes" />
				<jlis:td name="ACUERDO INTERNACIONAL ID" nowrap="yes" />
				<jlis:td name="ACUERDO" nowrap="yes" width="12%" />
				<jlis:td name="PAIS" nowrap="yes" width="12%" />
				<jlis:td name="NOMBRE ENTIDAD" nowrap="yes" width="20%"/>
				<jlis:td name="SOLICITUD" nowrap="yes" width="8%" />
				<jlis:td name="SUCE" nowrap="yes" width="8%" />
				<jlis:td name="D.J./C.O." nowrap="yes" width="8%" />
				<jlis:td name="ESTADO REGISTRO ID" nowrap="yes" />
				<jlis:td name="FECHA DE TRANSMISIÓN" nowrap="yes" />
				<jlis:td name="USUARIO" nowrap="yes" />
				<jlis:td name="USUARIO ID" nowrap="yes" />
				<jlis:td name="ESTADO" nowrap="yes" width="10%"/>
				<jlis:td name="NOMBRE EVALUADOR" nowrap="yes" width="20%" />
				<jlis:td name="FECHA DE REGISTRO" nowrap="yes" width="10%" />
				<jlis:td name="ENTIDAD ID" nowrap="yes" />
				<jlis:td name="TUPA ID" nowrap="yes" />
				<jlis:td name="FORMATO ID" nowrap="yes" />
				<jlis:td name="PRIORIDAD" nowrap="yes" />
				<jlis:td name="ACCION SUCE" nowrap="yes" />
				<jlis:td name="CALIFICA DJ" nowrap="yes" />
				<jlis:td name="RECTIFICACION" nowrap="yes" />
			</jlis:tr>
			<jlis:columnStyle column="1" columnName="NOMBRETUPA" />
			<jlis:columnStyle column="2" columnName="NOMBREFORMATO" />
			<jlis:columnStyle column="3" columnName="NOMBRECUT" />
			<jlis:columnStyle column="4" columnName="ORDENID" hide="yes" />
			<jlis:columnStyle column="5" columnName="MTO" hide="yes" />
			<jlis:columnStyle column="6" columnName="SUCEID" hide="yes" />
			<jlis:columnStyle column="7" columnName="ACUERDOINTERNACIONALID" hide="yes" />
			<jlis:columnStyle column="8" columnName="NOMBREACUERDOINTERNACIONAL" />
			<jlis:columnStyle column="9" columnName="NOMBREPAIS" />
			<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' || sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
			<jlis:columnStyle column="10" columnName="NOMBREENTIDAD" />
			</c:if>
			<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] != 'CO.CENTRAL.OPERADOR_FUNCIONAL' && sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] != 'CO.ADMIN.HELP_DESK'}">
			<jlis:columnStyle column="10" columnName="NOMBREENTIDAD" hide="yes" />
			</c:if>
			<jlis:columnStyle column="11" columnName="ORDEN" />
			<jlis:columnStyle column="12" columnName="SUCE" editable="${(sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA')?'no':'yes'}" />
			<jlis:columnStyle column="13" columnName="CERTIFICADO" align="center" nowrap="yes" />
			<jlis:columnStyle column="14" columnName="ESTADOREGISTRO" hide="yes" />
			<jlis:columnStyle column="15" columnName="FECHATRANSMISION" hide="yes" />
			<jlis:columnStyle column="16" columnName="NOMBRE" hide="yes" />
			<jlis:columnStyle column="17" columnName="USUARIOID" hide="yes" />
			<jlis:columnStyle column="18" columnName="DESCRIPCIONESTADOREGISTRO" />
			<jlis:columnStyle column="19" columnName="NOMBREEVALUADOR" />
			<jlis:columnStyle column="20" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" />
			<jlis:columnStyle column="21" columnName="ENTIDADID" hide="yes" />
			<jlis:columnStyle column="22" columnName="TUPAID" hide="yes" />
			<jlis:columnStyle column="23" columnName="FORMATOID" hide="yes" />
			<jlis:columnStyle column="24" columnName="PRIORIDAD" hide="yes" />
        	<jlis:tableButton column="12" type="link" onClick="verCertificadoAsignado" />
			<c:choose>
      			<c:when test="${sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.FIRMA' && 
      			                  sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] != 'CO.CENTRAL.OPERADOR_FUNCIONAL' &&
                                  sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] != 'CO.ADMIN.HELP_DESK'}">
      				<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
						<jlis:columnStyle column="25" columnName="ACCIONSUCE" validator="pe.gob.mincetur.vuce.co.web.util.ResolutorAsignadosEstadosCellValidator" method="mostrarAccionSuce" editable="yes" align="center" />
						<jlis:tableButton column="25" type="imageButton" image="" onClick="" alt="" />
						<jlis:columnStyle column="26" columnName="EVALUAR" validator="pe.gob.mincetur.vuce.co.web.util.ResolutorAsignadosEstadosCellValidator" method="mostrarEvaluar" editable="yes" />
						<jlis:tableButton column="26" type="imageButton" image="" onClick="" alt="" />
						<jlis:columnStyle column="27" columnName="RECTIFICACION" validator="pe.gob.mincetur.vuce.co.web.util.ResolutorAsignadosEstadosCellValidator" method="mostrarRetifPendiente" editable="yes" />
						<jlis:tableButton column="27" type="imageButton" image="" onClick="" alt="" />
					</c:if>
					<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.EVALUADOR'}">
						<jlis:columnStyle column="25" columnName="ACCIONSUCE" hide="yes" />
						<jlis:columnStyle column="26" columnName="EVALUAR" hide="yes" />
						<jlis:columnStyle column="27" columnName="RECTIFICACION" hide="yes" />
					</c:if>
				</c:when>
				<c:otherwise>
      				<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA'}">
						<jlis:columnStyle column="25" columnName="ACCIONSUCE" validator="pe.gob.mincetur.vuce.co.web.util.ResolutorAsignadosEstadosCellValidator" method="mostrarAccionSuce" editable="yes" align="center" />
						<jlis:tableButton column="25" type="imageButton" image="" onClick="" alt="" />
						<jlis:columnStyle column="26" columnName="EVALUAR" hide="yes" />
						<jlis:columnStyle column="27" columnName="RECTIFICACION" hide="yes" />
					</c:if>
					<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                                  sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
                        <jlis:columnStyle column="25" columnName="ACCIONSUCE" hide="yes" />
						<jlis:tableButton column="25" type="imageButton" image="" onClick="" alt="" />
						<jlis:columnStyle column="26" columnName="EVALUAR" hide="yes" />
						<jlis:columnStyle column="27" columnName="RECTIFICACION" hide="yes" />
                    </c:if>
				</c:otherwise>
			</c:choose>
	 	</jlis:table>

    </body>
</html>