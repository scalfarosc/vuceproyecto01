<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes de Certificados</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <script>

	    function validarSubmit() {
	        var f = document.formulario;
	        if (f.button.value=="buscarButton") {
	            if (f.numero.value!="" && getSelectedRadioValue(f.opcionFiltro)=="") {
	                alert("Debe seleccionar la opci�n Solicitud o SUCE");
	                return false;
	            }
	        }
	        return true;
	    }
        
	    function buscar() {
	        var f = document.formulario;
	        f.action = contextName+"/admentco.htm";
	        f.method.value = "cargarListaSolicitudesCertificadoOrigen";
	        f.target = "_self";
	        f.button.value = "buscarButton";
            f.submit();
	    }

		function seleccionOpcion(obj) {
			var f = document.formulario;
			if (obj.value=="O") {
				f.numeroSUCE.value = "";
				f.numeroCO.value = "";
			} else if (obj.value=="S") {
				f.numeroOrden.value = "";
                f.numeroCO.value = "";
            } else if (obj.value=="C") {
                f.numeroOrden.value = "";
                f.numeroSUCE.value = "";
			}
		}
        
		function seleccionNumeroOrden() {
			var f = document.formulario;
			f.numeroSUCE.value = "";
			f.numeroCO.value = "";
			document.getElementById("opcionFiltro_O").checked = true;
		}
        
		function seleccionNumeroSUCE() {
			var f = document.formulario;
			f.numeroOrden.value = "";
			f.numeroCO.value = "";
			document.getElementById("opcionFiltro_S").checked = true;
		}
        
        function seleccionNumeroCO() {
            var f = document.formulario;
            f.numeroOrden.value = "";
            f.numeroSUCE.value = "";
            document.getElementById("opcionFiltro_C").checked = true;
        }
        
	    function nuevoCertificado() {
	        var f = document.formulario;
	        f.action = contextName + "/cer.htm";
	        f.method.value="nuevoCertificado";
	        f.submit();
	    }
        
	    function verTrazabilidad(keyValues, keyValuesField){
	    	var numero = document.getElementById(keyValues.split("|")[3]).value;
			var f = document.formulario;
            f.action = contextName + "/traza.htm?numero="+numero+"&rutaopcion=CER";
            f.method.value = "listarTrazas";
            f.submit();
	    }

	    /**
	     *	Invoca a la pantalla para elegir la tupa a realizar (en esta aplicaci�n, las tupas son acciones sobre el certificado de origen)
	     */
	    function crearOrden() {
	        var f = document.formulario;
	        f.action = contextName + "/origen.htm";
	        f.method.value="cargarTupas";
	        f.submit();
	    }
        
		/*function cargarlistaAcuerdos(obj) {
			var f = document.formulario;
			//var filter = obj.value;
			//loadSelectAJX("co.ajax.certificado_origen.acuerdos_pais.selectLoader", "obtenerListadoAcuerdos", "select_acuerdo", null, filter, null, null, null);
			var filter ="pais="+obj.value;
		    loadSelectAJX("ajax.selectLoader", "loadList", "filtroAcuerdo", "certificado_origen.acuerdos_pais.select", filter, null, null, null);
		}
        */
        
		// Carga la p�gina de detalle de orden para poder evaluar
    	function evaluarCertif(keyValues, keyValuesField){
    		var f = document.formulario;
	        var orden = document.getElementById(keyValues.split("|")[0]).value;
            var mto = document.getElementById(keyValues.split("|")[1]).value;
            var formato = document.getElementById(keyValues.split("|")[2]).value;
            f.orden.value = orden;
            f.mto.value = mto;
            f.formato.value = formato;
            f.action = contextName + "/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.target = "_self";
            f.submit();
	    }

    	// Carga la p�gina que permite realizar acciones sobre la SUCE
    	function editarSolicitudCalificada(keyValues, keyValuesField){
    		var f = document.formulario;
	        var orden = document.getElementById(keyValues.split("|")[0]).value;
            var mto = document.getElementById(keyValues.split("|")[1]).value;
            var formato = document.getElementById(keyValues.split("|")[2]).value;
            f.formato.value = formato.toLowerCase();
            //alert('abriendo la suce ' + formato);
            f.action = contextName+"/admentco.htm?method=abrirSuce&orden="+orden+"&mto="+mto;
            f.target = "_self";
            f.submit();
	    }

		function verCertificado(keyValues, keyValuesField){
            var f = document.formulario;
            f.orden.value = document.getElementById(keyValues.split("|")[0]).value;
            f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
            f.formato.value = document.getElementById(keyValues.split("|")[2]).value;
            //var formato = document.getElementById(keyValues.split("|")[2]).value;
            //alert(f.formato.value);
            //var frmlow = formato.toLowerCase();
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.submit();
	    }

		//Funci�n que reemplaza un par�metro de la invocaci�n onClick para los �conos de avance y retroceso en las tablas del JLIS
		function reemplazarPrmtPaginacionJLIS(prmtBusqueda, nvoValor){
			var indInicio;
			var indFin;
			var txtAReemplazar;
			var indIgual;
			var txtNuevo;

    		$("b").each(function (i){

    			if (this.className == 'first' || this.className == 'previous' || this.className == 'next' || this.className == 'last'){
    				indInicio= this.attributes.onclick.value.indexOf(prmtBusqueda+'=');

    				if (indInicio > -1) {

    					indFin = this.attributes.onclick.value.indexOf('&', indInicio);

    	    			if (indFin == -1) {
    	    				indFin = this.attributes.onclick.value.indexOf("'", indInicio);
    	    			}

    					txtAReemplazar = this.attributes.onclick.value.substring(indInicio, indFin);
    					indIgual = txtAReemplazar.indexOf('=');

    					txtNuevo = txtAReemplazar.replace(txtAReemplazar.substring(indIgual, txtAReemplazar.length), '='+nvoValor);

    					//alert(this.attributes.onclick.value);
    					this.attributes.onclick.value = this.attributes.onclick.value.replace(txtAReemplazar, txtNuevo);
    					//alert(this.attributes.onclick.value);

    					try{
    						console.log('M�todo reemplazarPrmtPaginacionJLIS: El par�metro "' + prmtBusqueda + '" ha sido reemplazado para el m�todo onClick de la clase ' + this.className + ', el nuevo valor es ' + nvoValor + '.');
    					} catch(e){

    					}

    				} else {
    					try{
    						console.log('Error en el m�todo reemplazarPrmtPaginacionJLIS: No existe el par�metro "' + prmtBusqueda + '" para el m�todo onClick de la clase ' + this.className + '.');
    					} catch(e){

    					}
    				}
    			}
    		});

		}


    </script>
    <body>
    	<div id="body">
     		<jsp:include page="/WEB-INF/jsp/header.jsp" />

    		<!-- CONTENT -->
			<div id="contp"><div id="cont">
        		<form name="formulario" method="post" onSubmit="return false;">
					<jlis:value type="hidden" name="method" />
					<jlis:value type="hidden" name="orden" />
					<jlis:value type="hidden" name="ordenId" />
					<jlis:value type="hidden" name="mto" />
					<jlis:value type="hidden" name="seleccionado" />
					<jlis:value type="hidden" name="entidad" value="9" />
					<jlis:value type="hidden" name="numero" value="" />
					<jlis:value type="hidden" name="formato" />
					<%-- <jlis:value type="hidden" name="button" /> --%>
					<input type="hidden" name="button" />

            		<jlis:messageArea width="100%" />

		            <div id="pageTitle">
            			<h1><strong><jlis:label labelClass="pageTitle"  key="co.title.suces" /></strong></h1>
            		</div>
                    
		            <br/>
                    
		            <p align="left">
		            	<jlis:label key="co.label.co_busqueda.pais_acuerdo" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<%-- 20170131_GBT ACTA CO-004-16 Y CO-009-16--%>
               			<%--jlis:selectProperty key="comun.pais_iso.acuerdo_vigente.select" name="filtroPais" style="defaultElement=2" onChange="listarSolicitudesCertificadoEntidad()" editable="yes" />&nbsp;&nbsp; --%>
		            	<jlis:selectProperty key="comun.pais_iso.acuerdos_all.select" name="filtroPais" style="defaultElement=2" onChange="listarSolicitudesCertificadoEntidad()" editable="yes" />&nbsp;&nbsp;
		            	<jlis:label key="co.label.acuerdo" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty name="filtroAcuerdo" key="${keyAcuerdos}" filter="${filtroAcuerdos}" onChange="listarSolicitudesCertificadoEntidad()" style="defaultElement=2" />&nbsp;&nbsp;
		                <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}">
	                    <jlis:label key="co.label.asignar_evaluador.evaluador" style="font-weight: bold;"/>&nbsp;&nbsp;
	                    <jlis:selectProperty name="filtroEvaluador" key="resolutor.administrador.evaluadores_entidad.select" filter="${filterEvaluadores}" onChange="listarSolicitudesCertificadoEntidad()" style="defaultElement=2" />
	                    </c:if>
                        <jlis:value type="radio" name="opcionFiltro" checkValue="C" onClick="seleccionOpcion(this)"/><jlis:label key="co.label.nco" style="font-weight: bold;"/>&nbsp;&nbsp;
                        <jlis:value name="numeroCO" size="25" onClick="seleccionNumeroCO()" />&nbsp;&nbsp;
					</p>
                    
		            <br/>
                    
					<p align="left">
						<jlis:label key="co.label.buzon.filtro.fechaDesde" style="font-weight:bold"/>&nbsp;&nbsp;
	                    <jlis:date form="formulario" name="fechaDesde" />&nbsp;&nbsp;
	                    <jlis:label key="co.label.buzon.filtro.fechaHasta" style="font-weight:bold"/>&nbsp;&nbsp;
	                    <jlis:date form="formulario" name="fechaHasta" />&nbsp;&nbsp;
	                    
						<jlis:label key="co.label.ruc" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="filtroRUC" size="11" maxLength="11" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						
		                <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA'}">
			            	<jlis:label key="co.label.estado" style="font-weight: bold;"/>&nbsp;&nbsp;
			            	<jlis:selectProperty key="comun.estadosCO.select" name="filtroEstado" style="defaultElement=no,width=250px" onChange="listarSolicitudesCertificadoEntidad()" />&nbsp;&nbsp;
		                </c:if>
					</p>
                    
		            <br/>
                    
					<p align="left">
		            	<jlis:label key="co.label.formato" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.entidad.formatosxEntidad.select" name="filtroFormato" style="defaultElement=2" onChange="listarSolicitudesCertificadoEntidad()" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="O" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.solicitud" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroOrden" size="8" onClick="seleccionNumeroOrden()" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="S" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.suce" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroSUCE" size="8" onClick="seleccionNumeroSUCE()" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						
						<jlis:label key="co.label.denominacion" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="filtroDenominacion" size="30" maxLength="100" />&nbsp;&nbsp;
						<jlis:label key="co.label.partidaArancelaria" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="filtroPartida" size="10" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);"/>&nbsp;&nbsp;
						
						<jlis:button id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Pulse aqu� para buscar" /></span>
					</p>

					<br/>

		            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
		            	<c:choose>
      						<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
		                			  		sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || 
                      						sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA' ||
                      						sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      						sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
								<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesAdministradorEval.jsp" />
  	    					</c:when>
  	    					<%--c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}">
								<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesAdministrador.jsp" />
  	    					</c:when>
      						<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
								<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesEvaluador.jsp" />
  	    					</c:when--%>
		                </c:choose>
		            </c:if>
			    </form>
        	</div></div>
         	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>