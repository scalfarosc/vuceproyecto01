<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema VUCE</title>
</head>

<script language="JavaScript" src="${pageContext.request.contextPath}/resource/js/subModal/subModal.js"></script>
<script language="JavaScript" src="${pageContext.request.contextPath}/resource/js/acuerdos/acuerdo${idAcuerdo}/solicitud_co_acuerdo_${idAcuerdo}.js"></script>
<script language="JavaScript" src="${pageContext.request.contextPath}/resource/js/firmas/vucecopy.min.js"></script>

<jsp:include page="/WEB-INF/jsp/funciones.jsp" />
<script>

	// Inicializaciones de JQuery
	$(document).ready(function() {
		var estadoRegistro = '${estadoRegistro}';
		if (estadoRegistro == 'V' || estadoRegistro == 'A') {
			tituloPopUp("Documento Resolutivo");
		} else {
			tituloPopUp("Borrador del Documento Resolutivo");
		}

	    initializetabcontent("maintab");
	    validaCambios();

	    if ($("#DR\\.sdr").val() != "") {
	    	$("#liAdjuntos").attr("style", "display:");
	    	$("#version").val($("#DR\\.sdr").val());
	    }

	    var idAcuerdo = '${idAcuerdo}';

	    // Solo cuando es un mct001 debemos realizar la inicializaci�n
	    if ( idAcuerdo != '' ) {
		    // Inicializa controles de DR seg�n el acuerdo
		    inicializacionDocumentoResolutivoSolicitudSegunAcuerdo();
	    	// Si a�n no se ha transmitido, no se muestra (pero s�lo se mostrar� si el dr del acuerdo lo dice)
	    	var estadoRegistro = '${estadoRegistro}';
	    	if ( estadoRegistro != 'V' && estadoRegistro != 'A' ) {
	    		$("#trNumeroExpediente").attr("style", "display:none");
	    	}

	    	//Si no existen datos generales que mostrar se oculta el titulo correspondiente
	    	if ((document.getElementById("trNroCertifEntidad") == undefined || tagOculto("trNroCertifEntidad")) &&
		    	tagOculto("trNroCertifOrigen") && tagOculto("trNumeroExpediente") &&
			    tagOculto("trFechaEmision") && tagOculto("trFechaVigencia"))  {

	    		$("#trDatosGeneralesTitle").attr("style", "display:none");
	    		$("#tbSeparadorDatosGenerales").attr("style", "display:none");
	    	}




			// Si es dr de denegaci�n, no se muestra la fecha de vigencia
			/*var tipoDr = $("#tipoDr").val();
			if ( tipoDr != '' && tipoDr != 'R' ) {
				$("#trFechaVigencia").attr("style", "display:");
			}*/
	    }

		bloquearFirma(estadoRegistro);
	});

	// Bloquea la informaci�n del firmante en caso sea necesario
	function bloquearFirma(estado) {
		/*alert("Estado: " + estado);
		alert("esRectificacion: ${esRectificacion}");
		alert("estadoSDR: ${estadoSDR}");
		alert("numAdjuntosCODR: ${numAdjuntosCODR}");*/
		//if ((estado == 'A' && '${esRectificacion}' == 'N') || ('${estadoSDR}' == 'A' &&  '${numAdjuntosCODR}' != '0')) {
		if ('${requiereFirma}' != 'S'){
			<c:if test="${(idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 18|| idAcuerdo == 19|| idAcuerdo == 27)}">
			$("#nroReferenciaCertificado" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $("#nroReferenciaCertificado" ).attr("disabled", true);
			</c:if>
		    $("#secuenciaFuncionario" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
	        $("#secuenciaFuncionario" ).attr("disabled", true);
	        $("#usuarioFirmante" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
	        $("#usuarioFirmante" ).attr("readonly", true);
	        $("#fechaFirma" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
	        $("#fechaFirma" ).attr("readonly", true);
	        
	        // 20151020_JFR: Acta.34:Firmas:6,8,15
	     	// 20150612_RPC: Acta.34:Firmas: Fecha Firma Exportador, Informacion Adicional: 18:AELC, 19:AC.UE
	     	// 20150701_RPC: Acta.36:Firmas: Fecha Firma Exportador, Informacion Adicional: 9:SGP FR, 10:SGP JP, 12:SGP NZ, 14:SGP UE, 24:SGP TR
	        <c:if test="${idAcuerdo == 6 || idAcuerdo == 8 || idAcuerdo == 9 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 14 || idAcuerdo == 15 || idAcuerdo == 18 || idAcuerdo == 19 || idAcuerdo == 24|| idAcuerdo == 27}">
	        $("#fechaFirmaExportador" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
	        $("#fechaFirmaExportador" ).attr("readonly", true);
	        $("#emitidoPosteriori2").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	        // $("#emitidoPosteriori2").attr("disabled", true);
	        </c:if>
		}
	}

	// Validaciones antes de realizar el submit
	function validarSubmit() {
	    var f = document.formulario;

	    if (f.button.value=="cancelarButton") {
            return false;
        }
	    if (f.button.value=="signButton") {
	    	return validaCerraPopUp();
        }	    
	    if (f.button.value=="printSignatureButton") {
	    	return imprimirSegundaFirma();
        }	    
	    if (f.button.value=="cerrarPopUpButton") {
        	return validaCerraPopUp();
        }
	    if (f.button.value=="printResumeButton") {
	    	//alert(f.idAcuerdo.value);
	    	return imprimirHojaResumenSUCE();
        }
	    if (f.button.value=="printResumeAnulacionButton") {
	    	return imprimirHojaResumenSUCE('1');
        }
        if (f.button.value=="grabarButton") {
            //disableButton("transmitirButton", true);
            $("#transmitirButton").attr("disabled", 'disabled');
        }
        if (f.button.value=="nuevaRectificacionDRButton") {
			var drId = $("#DR\\.drId").val();
			var sdr = $("#DR\\.sdr").val();
			var suceId = $("#DR\\.suceId").val();
			var ordenId = f.orden.value;
			var mto = f.mto.value;
			var controller = "/admentco.htm";
			showPopWin(contextName+controller+"?method=rectificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto, 650, 450, null);
			return false;
        }
	    return true;
	}

		function validarCampos(){

		    var ok = true;
		    var mensaje="Debe ingresar o seleccionar : ";

		    /*if (document.getElementById("DR.observacionEvaluador").value==""){
		        mensaje +="\n -La observaci�n del evaluador";
		        changeStyleClass("co.label.resolutor.dr.observacion.evaluador", "errorValueClass");
		    }else changeStyleClass("co.label.resolutor.dr.observacion.evaluador", "labelClass");*/

		    if (mensaje!="Debe ingresar o seleccionar : ") {
		        ok= false;
		        alert (mensaje);
		    }
		    return ok;
		}

		function grabarRegistro() {
			var f = document.formulario;
	        if(validarCampos()){
	        	f.action = contextName+"/admentco.htm";
		        f.method.value="grabarDrBorrador";
		        f.button.value="grabarButton";

			} else {
				f.button.value="cancelarButton";
			}
	    }

		function transmitirRegistro() {
			var f = document.formulario;
	        if(validarCampos()){
	        	f.action = contextName+"/admentco.htm";
		        f.method.value="confirmarDrBorrador";
		        f.button.value="grabarButton";
		        $("#transmitirButton").hide();
		        f.target = window.parent.name;
			} else {
				f.button.value="cancelarButton";
			}
	    }

		function eliminarRegistro() {
			var f = document.formulario;
	        if (!confirm("�Est� seguro de eliminar este registro?")) {
				f.button.value="cancelarButton";
	        } else{
		        f.action = "<%=contextName%>/admentco.htm";
		        f.method.value="eliminarDRBorrador";
		        f.button.value="eliminarButton";
		        f.target = window.parent.name;
	        }
	    }

		//20140611_JMCA PQT-08
		function validarGuardarDatosFirma(){
			var mensaje="Debe ingresar o seleccionar : ";
		    var ok = true;
		    
		    <c:if test="${(idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 18 || idAcuerdo == 19|| idAcuerdo == 27)}">
            if ( document.getElementById("nroReferenciaCertificado").value == "" ) {
                mensaje +="\n -N�mero de Referencia del Certificado";
                changeStyleClass("co.dr.adjunto.firma.nroReferenciaCertificado", "errorValueClass");
            } else changeStyleClass("co.dr.adjunto.firma.nroReferenciaCertificado", "labelClass");
		    </c:if>
		    if ( document.getElementById("secuenciaFuncionario").value == "" ) {
		        mensaje +="\n -Nombre de quien firma por la Entidad Certificadora";
		        changeStyleClass("co.dr.adjunto.firma.cetificador", "errorValueClass");
		    } else changeStyleClass("co.dr.adjunto.firma.cetificador", "labelClass");

		    if ( document.getElementById("usuarioFirmante").value == "" ) {
		        mensaje +="\n -Nombre de quien firma por la Empresa Exportadora";
		        changeStyleClass("co.dr.adjunto.firma.exportador", "errorValueClass");
		    } else {
		    	if (js_ContieneCaracter(document.getElementById("usuarioFirmante"), "co.dr.adjunto.firma.exportador", "El nombre de quien firma debe tener por lo menos una letra")) {
			        changeStyleClass("co.dr.adjunto.firma.exportador", "labelClass");
		    	} else {
		    		return false;
		    	}
		    }
		    
		    <c:if test="${idAcuerdo == 6 }">
		    if ( document.getElementById("cargoEmpresaExportador").value == "" ) {
		        mensaje +="\n -Cargo de quien firma por la Empresa Exportadora";
		        changeStyleClass("co.dr.adjunto.firma.cargoEmpresaExportador", "errorValueClass");
		    } else changeStyleClass("co.dr.adjunto.firma.cargoEmpresaExportador", "labelClass");

		    if ( document.getElementById("telefonoEmpresaExportador").value == "" ) {
		        mensaje +="\n -Tel�fono de quien firma por la Empresa Exportadora";
		        changeStyleClass("co.dr.adjunto.firma.telefonoEmpresaExportador", "errorValueClass");
		    } else changeStyleClass("co.dr.adjunto.firma.telefonoEmpresaExportador", "labelClass");
		    </c:if>
            
		    // 20151020_JFR: Acta.34:Firmas: 6,8,15
		    // 20150612_RPC: Acta.34:Firmas: 18,19
		    // 20150701_RPC: Acta.36:Firmas: 9,10,12,14,24
		    <c:if test="${idAcuerdo == 6 || idAcuerdo == 8 || idAcuerdo == 9  || idAcuerdo == 10 || idAcuerdo == 12  || idAcuerdo == 14 || idAcuerdo == 15 || idAcuerdo == 18  || idAcuerdo == 19 || idAcuerdo == 24 || idAcuerdo == 27}">
		    if (document.getElementById("fechaFirmaExportador").value == "") {
		    	mensaje +="\n -Fecha de Firma Empresa Exportadora";
		    	changeStyleClass("co.dr.adjunto.firma.fechaFirmaExportador", "errorValueClass");
		    } else {
		    	changeStyleClass("co.dr.adjunto.firma.fechaFirmaExportador", "labelClass");
		    }
		    </c:if>
		    
		    if ( document.getElementById("fechaFirma").value == "" ) {
		        mensaje +="\n -Fecha de Firma Entidad Certificadora";
		        changeStyleClass("co.dr.adjunto.firma.fecha", "errorValueClass");
		    } else changeStyleClass("co.dr.adjunto.firma.fecha", "labelClass");


		    if ( mensaje != "Debe ingresar o seleccionar : " ) {
		        ok = false;
		        alert (mensaje);
		    }
		    return ok;


		}
		function validarCargaArchivo(){
			var mensaje="Debe ingresar o seleccionar : ";
		    var ok = true;
		    		    
		    <c:if test="${(idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 18 || idAcuerdo == 19 || idAcuerdo == 27)}">
            if ( document.getElementById("nroReferenciaCertificado").value == "" ) {
                mensaje +="\n -N�mero de Referencia del Certificado";
                changeStyleClass("co.dr.adjunto.firma.nroReferenciaCertificado", "errorValueClass");
            } else changeStyleClass("co.dr.adjunto.firma.nroReferenciaCertificado", "labelClass");
		    </c:if>
			
		
		    if ( document.getElementById("secuenciaFuncionario").value == "" ) {
		        mensaje +="\n -Nombre de quien firma por parte de la Entidad Certificadora";
		        changeStyleClass("co.dr.adjunto.firma.cetificador", "errorValueClass");
		    } else changeStyleClass("co.dr.adjunto.firma.cetificador", "labelClass");

		    if ( document.getElementById("usuarioFirmante").value == "" ) {
		        mensaje +="\n -Nombre de quien firma por parte del Exportador";
		        changeStyleClass("co.dr.adjunto.firma.exportador", "errorValueClass");
		    } else {
		    	if (js_ContieneCaracter(document.getElementById("usuarioFirmante"), "co.dr.adjunto.firma.exportador", "El nombre de quien firma debe tener por lo menos una letra")) {
			        changeStyleClass("co.dr.adjunto.firma.exportador", "labelClass");
		    	} else {
		    		return false;
		    	}
		    }

		    if ( document.getElementById("fechaFirma").value == "" ) {
		        mensaje +="\n -Fecha de firma";
		        changeStyleClass("co.dr.adjunto.firma.fecha", "errorValueClass");
		    } else changeStyleClass("co.dr.adjunto.firma.fecha", "labelClass");

		    var nomArchivo = "";
		    if (navigator.appName.indexOf("Explorer") != -1) {
		    	nomArchivo = document.getElementById("archivo").value;
		    } else {
	        var f = document.formulario;
		        nomArchivo = f.archivo.value;
		    }
		    if ( nomArchivo == "" ) {
		        mensaje +="\n -Archivo";
		        //changeStyleClass("co.label.certificado.causal", "errorValueClass");
		    } //else changeStyleClass("co.label.certificado.causal", "labelClass");

		    if ( mensaje != "Debe ingresar o seleccionar : " ) {
		        ok = false;
		        alert (mensaje);
		    }
		    return ok;


		}
		
		function setSignButton() {
		    var f = document.formulario;
		    f.button.value="signButton";
		}
		
		function printSignature() {
		    var f = document.formulario;
		    f.button.value="printSignatureButton";
		}
		
		// Guardar Datos Firma //20140611_JMCA PQT-08
	    function guardarDatosFirma() {
	        var f = document.formulario;
	        //if (validarDatosFirma()) {
	        if (validarGuardarDatosFirma()) {
	            f.action = contextName+"/admentco.htm";
	            f.method.value = "guardarDatosFirmaCertificadoOrigenDRForm";
	            f.button.value = "cargarButton";
	            f.target = "";
	        } else {
	            alert("Debe seleccionar alg�n archivo");
	            f.button.value = "cancelarButton";
	        }
	    }

	    // Carga de archivos
	    function cargarArchivo() {
	        var f = document.formulario;
	        if (validarCargaArchivo()) {
	            f.action = contextName+"/admentco.htm";
	            f.method.value = "cargarArchivoCertificadoOrigenDRForm";
	            f.button.value = "cargarButton";
	            f.target = "";
	            //f.submit();
	        } else {
	            alert("Debe seleccionar alg�n archivo");
	            f.button.value = "cancelarButton";
	        }
	    }

	    // Descargar archivos adjuntos
	    function descargarAdjunto(keyValues, keyValuesField) {
	        var f = document.formulario;
	        var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
	        f.action = contextName+"/origen.htm";
	        f.method.value = "descargarAdjunto";
	        f.idAdjunto.value = adjuntoId;
	        f.submit();
	    }

	    function nuevaRectificacionDR() {
	        var f = document.formulario;
	        f.button.value="nuevaRectificacionDRButton";
	    }

        function eliminarRegistroAdjunto() {
            var f = document.formulario;
            var indice = -1;
            if (f.seleccione!=null) {
                if (eval(f.seleccione.length)) {
                    for (var i=0; i < f.seleccione.length; i++) {
                        if (f.seleccione[i].checked){
                            indice = i;
                        }
                    }
                } else {
                    if (f.seleccione.checked) {
                        indice = 0;
                    }
                }
            }
            if (indice!=-1) {
                f.action = contextName+"/admentco.htm";
                f.method.value="eliminarAdjunto";
                f.button.value="eliminarButton";
	            f.target = "";

            } else {
                alert('Seleccione por lo menos un registro a eliminar');
                f.button.value = 'cancelarButton';
            }
        }

		function finalizar() {
			var f = document.formulario;
	        f.action = "<%=contextName%>/admentco.htm";
	        f.method.value="cerrarSuce";
	        f.button.value="grabarButton";
	        f.target = window.parent.name;
	    }

		function verSolicitudRectificacionDR(keyValues, keyValuesField) {
			var f = document.formulario;
			var drId = document.getElementById(keyValues.split("|")[2]).value;//f.drId.value;
			var sdr = document.getElementById(keyValues.split("|")[3]).value;//f.sdr.value;
			var suceId = document.getElementById("DR.suceId").value;//f.suceId.value;
			var ordenId = f.orden.value;
			var mct001DrId = document.getElementById("DR.coDrId").value;
			var mto = f.mto.value;
			var controller = "/admentco.htm";
			var modificacionDrId = document.getElementById(keyValues.split("|")[0]).value;
			var mensajeId = document.getElementById(keyValues.split("|")[1]).value;
			var formato = f.formato.value;
			showPopWin(contextName + controller + "?method=cargarModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId+"&mct001DrId="+mct001DrId+"&formato="+formato, 710, 500, null);
		}

		function eliminarRectificacion() {
			var f = document.formulario;
	        if (!confirm("�Est� seguro de eliminar el registro de rectificaci\u00F3n?")) {
				f.button.value="cancelarButton";
	        } else{
		        f.action = "<%=contextName%>/admentco.htm";
		        f.method.value="eliminarRectificacionDR";
		        f.button.value="eliminarButton";
		        f.target = window.parent.name;
	        }
		}

		function confirmarRectificacion() {
			var f = document.formulario;
	        if (!confirm("�Est� seguro de confirmar el registro de rectificaci\u00F3n?")) {
				f.button.value="cancelarButton";
	        } else{
		        f.action = "<%=contextName%>/admentco.htm";
		        f.method.value="confirmarRectificacionDR";
		        f.button.value="eliminarButton";
		        f.target = window.parent.name;
	        }
		}

		function seleccionVersion(valor) {
			var f = document.formulario;
        	f.action = contextName+"/admentco.htm";
        	f.sdrSelect.value = valor;
	        f.method.value="cargarDatosDocumentoResolutivo";
	        f.button.value="grabarButton";
	        f.submit();
	    }

		function printResume() {
		    var f = document.formulario;
		    f.button.value="printResumeButton";

			if ('${formato}' == 'MCT004' || '${formato}' == 'mct004' ) {
				f.button.value="printResumeAnulacionButton";
			}

		}

</script>

	<body id="contModal">
		<%@include file="/WEB-INF/jsp/helpHint.jsp" %>
  	    <jlis:modalWindow />
	        <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="DR.coDrId" type="hidden" />
            <jlis:value name="DR.drId" type="hidden" />
            <jlis:value name="DR.sdr" type="hidden" />
            <jlis:value name="DR.suceId" type="hidden" />
            <jlis:value name="sdrSelect" type="hidden" />
            <jlis:value name="orden" type="hidden" />
            <jlis:value name="mto" type="hidden" />
            <jlis:value name="numSuce" type="hidden" />
	        <jlis:value name="bloqueado" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <jlis:value name="codTipoDr" type="hidden" />
	        <input type="hidden" name="idAdjunto">
	        <jlis:value name="tipoDr" type="hidden" />
	        <jlis:value name="borradorDrId" type="hidden" />
	        <jlis:value name="idAcuerdo" type="hidden" />
			<input class="vuce-copy-data" id="input_entidad" user="${requestScope['usuarioSol']}" drid="${requestScope['DR.drId']}" sdr="${requestScope['DR.sdr']}"/>
			
        	<co:validacionBotonesFormato formato="MTC001DR" />
        	
        	<!-- 2018.06.08 JMCA - Comentado es de ProduccionCompleta --> <!-- c:if test="${sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.FIRMA'}"-->
				<jlis:button id="printResumeButton" name="printResume()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Imprimir Certificado Origen " alt="Pulse aqu� para imprimir la hoja de resumen" />
			<!-- /c:if-->
			<jlis:button id="eliminarRectificacionButton" name="eliminarRectificacion()" code="CO.ENTIDAD.EVALUADOR" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Rectificaci�n" alt="Pulse aqu� para eliminar la rectificaci�n " />
			<jlis:button id="confirmarRectificacionButton" name="confirmarRectificacion()" code="CO.ENTIDAD.EVALUADOR" type="BUTTON_JAVASCRIPT_SUBMIT" title="Confirmar Rectificaci�n" alt="Pulse aqu� para finalizar" />
            <jlis:button id="eliminarButton" name="eliminarRegistro()" code="CO.ENTIDAD.EVALUADOR" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Borrador DR" alt="Pulse aqu� para eliminar el borrador de DR " />
            <jlis:button id="transmitirButton" name="transmitirRegistro()" code="CO.ENTIDAD.EVALUADOR" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para confirmar el borrador de DR " />
			<jlis:button id="finalizarButton" name="finalizar()" code="CO.ENTIDAD.FIRMA" type="BUTTON_JAVASCRIPT_SUBMIT" title="Finalizar" alt="Pulse aqu� para finalizar" />
			<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />

			<c:if test="${!empty rectificaDr && rectificaDr == 'S' && !empty filterVersionesDR && !empty filterVersionesDR.drId}">
            <span class="messageClass"><jlis:label key="co.label.versionDR"/></span>
            <jlis:selectProperty name="version" key="dr.version.lista" filter="${filterVersionesDR}" style="defaultElement=no" onChange="seleccionVersion(this.value)" />
            </c:if>
			<%--HELPDESK1--%>
            <div class="block btabs btable">
				<div class="blocka">
					<div class="blockb">
				        <ul id="maintab" class="tabs">
							<li class="selected"><a href="#" rel="tabGeneral"><span>Datos Generales </span></a></li>
							<!-- 2018.06.08 JMCA - Comentado es de ProduccionCompleta --> <!-- c:if test="${!empty formato && habilitarFirmas == 'S' && sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA' && ((formato == 'mct001' || formato == 'MCT001'||formato == 'mct003'||formato == 'MCT003') && !empty tipoDr && tipoDr == 'A' )}"-->
							<!--c:if test="${!empty formato && (formato == 'MCT001' || formato == 'MCT003' || formato == 'mct001' || formato == 'mct003') && (sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA' || sessionScope.USUARIO.rolActivo == 'CO.ADMIN.HELP_DESK') && (!empty tipoDr && tipoDr == 'A' && ((estadoSDR == 'A' && esRectificacion == 'S') || esRectificacion == 'N'))}"!-->
							<c:if test="${!empty formato && (formato != 'MCT004' && formato != 'mct004') && (sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA' || sessionScope.USUARIO.rolActivo == 'CO.ADMIN.HELP_DESK') && (!empty tipoDr && tipoDr == 'A' && ((estadoSDR == 'A' && esRectificacion == 'S') || esRectificacion == 'N'))}">
								<li id="liAdjuntos" style="display:none"><a href="#" rel="tabAdjuntos"><span>Firmas Adjuntas </span></a></li>
							</c:if>
							<c:if test="${!empty rectificaDr && rectificaDr == 'S' && ((estadoSDR == 'A' && esRectificacion == 'S' && numAdjuntosCODR>0) || esRectificacion == 'N')}">
								<c:if test='${estadoRegistro == "A" && "S" == suceCerrada}'>
								<li id="liSolRectif"><a href="#" rel="tabSolRectifDR"><span>Solicitudes de Rectificaci�n de DR</span></a></li>
								</c:if>
							</c:if>
						</ul>
						<div id="tabGeneral" class="tabcontent">
							<table class="form">
								<tr>
									<td colspan="5">
										<jlis:button code="CO.ENTIDAD.EVALUADOR" id="grabarButton" name="grabarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar" alt="Pulse aqu� para grabar el DR " />
									</td>
								</tr>

								<tr>
									<td colspan="5">
										<div id="pageTitle">
						       				<strong><jlis:value name="nombreFormato" editable="no" valueClass="pageTitle" /></strong>
						       			</div>
						    		<hr/>
						    		</td>
								</tr>
							</table>
							<jsp:include page="/WEB-INF/jsp/resolutor/${formato}DrData.jsp" />
						</div>
						<div id="tabAdjuntos" class="tabcontent">
												
				            <table id="tablaBotonesAdjunto">
					            <%--20140611_JMCA PQT-08--%>
				            	<tr>
									<td width="2%">&nbsp;</td>
									<td width="10%">
									<c:choose>
									  <c:when test="${tieneFirmaDigital && tieneSegundaFirma}">
									  	<jlis:button id="printSignatureButton" name="printSignature()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Ver Firma " alt="Pulse aqu� para visualizar la firma" />
									  </c:when>
									  <c:when test="${tieneFirmaDigital && !tieneSegundaFirma}">
										<input type='submit' id="signButton" onclick="setSignButton();" class="buttonSubmitClass vuce-copy-obtener" vuce-clipboard-target="input_entidad" value=" Firma Entidad " />
									  </c:when>
									  <c:otherwise>
										<jlis:button code="CO.ENTIDAD.FIRMA" id="cargarButton" name="guardarDatosFirma()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Grabar Datos" alt="Pulse aqu� para grabar los datos" />
									  </c:otherwise>
									</c:choose>									
									</td>
									<td></td>
				                </tr>

								<tr >									
									<td colspan="3" class="formatoCaption">&nbsp;</td>
								</tr>
				            
								<c:choose>
								  <c:when test="${tieneFirmaDigital}">
										<tr id="trDatosGeneralesTitle">									
											<td colspan="3" class="formatoCaption">&nbsp;&nbsp;DATOS FIRMA EMPRESA EXPORTADORA</td>
										</tr>
										
										<tr>
							        		<td></td>
											<td><jlis:label key="co.dr.adjunto.firma.exportador" /></td>
											<td>
												<jlis:value name="usuarioExportador" editable="no" type="text" align="center" style="width:75%" size="40" />
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.USUARIO_FIRMANTE_EXP" />
											</td>
										</tr>
										
										<!-- 20150612_RPC: Acta.34:Firmas -->
										<tr id="trFechaFirmaExportador" style="display:none">
							        		<td></td>
											<td><jlis:label key="co.dr.adjunto.firma.fechaFirmaExportador" /></td>
											<td>
												<jlis:date form="formulario" name="fechaExportador" value="${fechaExportador}" editable="no"/>
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.FECHA_FIRMA_EXP" />
											</td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										
										 <%--20140611_JMCA PQT-08--%>				            
										<tr id="trDatosGeneralesTitle">
											<td colspan="3" class="formatoCaption">&nbsp;&nbsp;DATOS FIRMA ENTIDAD CERTIFICADORA</td>
										</tr>
		
							        	<tr >
							        		<td width="2%"></td>
											<td width="40%"><jlis:label key="co.dr.adjunto.firma.cetificador" /></td>
											<td width="58%">								
												<jlis:value name="usuarioEntidad" editable="no" type="text" align="center" style="width:75%" size="40" />
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.USUARIO_FIRMANTE_EVA" />
											</td>
										</tr>
		
										<tr>
							        		<td></td>
											<th>
											<!-- 20151020_JFR: Acta.34:Firmas:6,8,15 -->
											<!-- 20150612_RPC: Acta.34:Firmas:18,19 -->
											<!-- 20150701_RPC: Acta.36:Firmas:9,10,12,14,24 -->
											<c:choose>
												<c:when test="${idAcuerdo == 6 || idAcuerdo == 8 || idAcuerdo == 9 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 14 || idAcuerdo == 15 || idAcuerdo == 18 || idAcuerdo == 19 || idAcuerdo == 24 || idAcuerdo == 26 || idAcuerdo == 27}">
													<jlis:label key="co.dr.adjunto.firma.fechaFirmaEntidad"  id="co.dr.adjunto.firma.fecha" />
												</c:when>
												<c:otherwise><jlis:label key="co.dr.adjunto.firma.fecha" /></c:otherwise>
											</c:choose>
											</th>
											<td>
												<jlis:date form="formulario" name="fechaEntidad" value="${fechaEntidad}" editable="no"/>
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.FECHA_FIRMA_EEV" />
											</td>
										</tr>


								  </c:when>
								  <c:otherwise>
										<tr id="trDatosGeneralesTitle">									
											<td colspan="3" class="formatoCaption">&nbsp;&nbsp;DATOS FIRMA EMPRESA EXPORTADORA</td>
										</tr>
										
										<tr>
							        		<td></td>
											<td><jlis:label key="co.dr.adjunto.firma.exportador" /></td>
											<td>
												<jlis:value name="usuarioFirmante" editable="yes" type="text" align="center" style="width:92%" size="50" /><span class="requiredValueClass">(*)</span>
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.USUARIO_FIRMANTE_EXP" />
											</td>
										</tr>
										<tr id="trCargoEmpresaExportador" style="display:none">
							        		<td></td>
											<th><jlis:label key="co.dr.adjunto.firma.cargoEmpresaExportador" /></th>
											<td>
												<jlis:value name="cargoEmpresaExportador" editable="yes" type="text" align="center" style="width:92%" size="50" /><span class="requiredValueClass">(*)</span>										
											</td>
										</tr>
										<tr id="trTelefonoEmpresaExportador" style="display:none">
							        		<td></td>
											<th><jlis:label key="co.dr.adjunto.firma.telefonoEmpresaExportador" /></th>
											<td>
												<jlis:value name="telefonoEmpresaExportador" editable="yes" type="text" align="center" style="width:92%" size="20" maxLength="20" /><span class="requiredValueClass">(*)</span>										
											</td>
										</tr>									
										
										<!-- 20150612_RPC: Acta.34:Firmas -->
										<tr id="trFechaFirmaExportador" style="display:none">
							        		<td></td>
											<td><jlis:label key="co.dr.adjunto.firma.fechaFirmaExportador" /></td>
											<td>
												<jlis:date form="formulario" name="fechaFirmaExportador" /><span class="requiredValueClass">(*)</span>
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.FECHA_FIRMA_EXP" />
											</td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										
										 <%--20140611_JMCA PQT-08--%>				            
										<tr id="trDatosGeneralesTitle">
											<td colspan="3" class="formatoCaption">&nbsp;&nbsp;DATOS FIRMA ENTIDAD CERTIFICADORA</td>
										</tr>
		
				
										<c:if test="${(idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 18 || idAcuerdo == 19 ||idAcuerdo == 27)}">
										<tr>
		                                    <td></td>
		                                    <td><jlis:label key="co.dr.adjunto.firma.nroReferenciaCertificado" /></td>
		                                    <td><%--20160526_NPC: Se agrega por ticket 98945 --%>	
		                                        <jlis:value name="nroReferenciaCertificado" editable="yes" type="text" align="center" style="width:92%" size="50" maxLength="6" onChange="validarEnteroPositivo(this,false,null)" /><span class="requiredValueClass">(*)</span>
		                                        <co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.NUMERO_REFERENCIA" />
		                                    </td>
		                                </tr>
		                                </c:if>
							        	<tr >
							        		<td width="2%"></td>
											<td width="40%"><jlis:label key="co.dr.adjunto.firma.cetificador" /></td>
											<td width="58%">								
												<%-- 20140618_JMC a�adido BUG 123--%>
												<c:choose>
									                <c:when test="${requiereFirma == 'N'}">							                    
									                    <jlis:selectProperty name="secuenciaFuncionario" key="comun.firmante.entidad.transmitido.select" editable="yes" filter="${filterFirmantesCertificador}" /><span class="requiredValueClass">(*)</span>
									                </c:when>
									                <c:otherwise>							                    
									                    <jlis:selectProperty name="secuenciaFuncionario" key="comun.firmante.entidad.select" editable="yes" filter="${filterFirmantesCertificador}" /><span class="requiredValueClass">(*)</span>
									                </c:otherwise>
									            </c:choose>
												
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.USUARIO_FIRMANTE_EVA" />
											</td>
										</tr>
		
										<tr>
							        		<td></td>
											<th>
											<!-- 20151020_JFR: Acta.34:Firmas:6,8,15 -->
											<!-- 20150612_RPC: Acta.34:Firmas:18,19 -->
											<!-- 20150701_RPC: Acta.36:Firmas:9,10,12,14,24 -->
											<c:choose>
												<c:when test="${idAcuerdo == 6 || idAcuerdo == 8 || idAcuerdo == 9 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 14 || idAcuerdo == 15 || idAcuerdo == 18 || idAcuerdo == 19 || idAcuerdo == 24 || idAcuerdo == 26}">
													<jlis:label key="co.dr.adjunto.firma.fechaFirmaEntidad"  id="co.dr.adjunto.firma.fecha" />
												</c:when>
												<c:otherwise><jlis:label key="co.dr.adjunto.firma.fecha" /></c:otherwise>
											</c:choose>
											</th>
											<td>
												<jlis:date form="formulario" name="fechaFirma" /><span class="requiredValueClass">(*)</span>
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.FECHA_FIRMA_EEV" />
											</td>
										</tr>
										
										<!-- 20150612_RPC: Acta.34:Firmas -->
										<tr height="10px"/>						
										<tr class="trInformacionAdicionalFirma" style="display:none">
											<td colspan="3" class="formatoCaption">&nbsp;&nbsp;Informaci�n Adicional del Certificado</td>	
										</tr>								
										<tr class="trInformacionAdicionalFirma" style="display:none">
							        		<td width="2%"></td>
											<th width="40%"><jlis:label key="co.label.certificado.salio_mercancia" id="co.label.certificado.salio_mercancia_2"/></th>
											<td width="58%">
												<jlis:selectSource name="emitidoPosteriori2" source="listaSiNo" scope="request" editable="${tieneFirmaDigital && tieneSegundaFirma ? 'no' : 'yes'}"/>
												<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.EMITIDO_POSTERIORI_2" />
											</td>
										</tr>
		
						            </table>
							        <br/>
									<c:if test="${!empty usuarioFirmante}">
										<table id="tablaSubtituloAdjunto" >
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
							                <tr>
												<td width="2%">&nbsp;</td>									
												<td width="10%"><jlis:button code="CO.ENTIDAD.FIRMA" id="cargarButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
												<td width="88%"><jlis:button code="CO.ENTIDAD.FIRMA" id="eliminarAdjButton" name="eliminarRegistroAdjunto()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" />
												</td>
							                </tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
							                <tr>
							                	<td>&nbsp;</td>
							                    <td colspan="2">
								                    <jlis:label key="co.label.documentos_adjuntar"/> <br />
								                    <jlis:label key="co.label.tipo_archivos"/>
							                    </td>
							                </tr>
			
							                <tr>
							                	<td>&nbsp;</td>
							                    <td colspan="2">
							                     <input type="file" id="archivo" name="archivo" size="87"  />
							                    </td>
							                </tr>
							            </table>
									</c:if>
		
							        <jlis:value name="ADJ_ELIM.djId" type="hidden" />
							        <jlis:value name="ADJ_ELIM.secuenciaMaterial" type="hidden" />
							        <jlis:value name="ADJ_ELIM.adjuntoId" type="hidden" />
		
							        <jlis:table  name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntosCODR" scope="request"
							            pageSize="*" navigationHeader="no" width="98%" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCopiadosTramitePadreRowValidator" >
							            <jlis:tr>
							                <jlis:td name="ADJUNTO ID" nowrap="yes" />
							                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" width="90%" />
							              	<jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
							            </jlis:tr>
							            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" />
							            <jlis:columnStyle column="2" columnName="NOMBRE_ARCHIVO" editable="yes" />
							            <jlis:tableButton column="2" type="link" onClick="descargarAdjunto" highlight="no" />
							            <jlis:columnStyle column="3" columnName="SELECCIONE" align="center" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCellValidator" editable="yes" />
							            <jlis:tableButton column="3" type="checkbox" name="seleccione" />
							        </jlis:table>
								  </c:otherwise>
								</c:choose>	
													            
							</table>
						</div>
				 		<div id="tabSolRectifDR" class="tabcontent">
				            <%-- <table class="form">
				                <tr>
				                    <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="nuevaRectificacionDRButton" name="nuevaRectificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Iniciar Rectificaci�n DR" alt="Pulse aqu� para crear una nueva DR" /></td>
				                    <td width="100%"></td>
				                </tr>
				            </table> --%>
					        <jlis:table name="MODIFICACION_DR" pageSize="*" keyValueColumns="MODIFICACION_DR,MENSAJE_ID,DR_ID,SDR" source="tModificacionesDR" scope="request" navigationHeader="no" width="100%">
					            <jlis:tr>
					                <jlis:td name="DR ID" nowrap="yes" />
					                <jlis:td name="SDR" nowrap="yes" />
					                <jlis:td name="MODIFICACION DR" nowrap="yes" />
					                <jlis:td name="FECHA" nowrap="yes" width="5%"/>
					                <jlis:td name="MENSAJE ID" nowrap="yes"/>
					                <jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
					                <jlis:td name="ESTADO" nowrap="yes" width="8%"/>
					                <jlis:td name="FECHA RESPUESTA" nowrap="yes" width="5%"/>
					            </jlis:tr>
					            <jlis:columnStyle column="1" columnName="DR_ID" hide="yes" />
					            <jlis:columnStyle column="2" columnName="SDR" hide="yes" />
					            <jlis:columnStyle column="3" columnName="MODIFICACION_DR" hide="yes" />
					            <jlis:columnStyle column="4" columnName="FECHA_REGISTRO" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
					            <jlis:columnStyle column="5" columnName="MENSAJE_ID" hide="yes" />
					            <jlis:columnStyle column="6" columnName="MENSAJE" editable="yes" />
					            <jlis:columnStyle column="7" columnName="DESCRIPCION_ESTADO_REGISTRO" align="center"/>
					            <jlis:columnStyle column="8" columnName="FECHA_RESPUESTA" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
					            <jlis:tableButton column="6" type="link" onClick="verSolicitudRectificacionDR" />
					        </jlis:table>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>

</html>