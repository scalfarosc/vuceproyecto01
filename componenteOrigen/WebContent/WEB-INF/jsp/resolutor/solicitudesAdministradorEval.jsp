<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@page import="org.jlis.core.bean.*"%>
<%@page import="org.jlis.core.util.*"%>
<%@page import="org.jlis.core.list.*"%>
<%@page import="org.jlis.web.list.*"%>
<%@page import="org.jlis.web.util.*"%>
<%
    String contextName = request.getContextPath();
%>
<%--!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" --%>
<script src="<%=contextName%>/resource/js/functions.js"></script>
<script src="<%=contextName%>/resource/js/menu.js"></script>
<script src="<%=contextName%>/resource/js/subModal/common.js"></script>
<script src="<%=contextName%>/resource/js/subModal/drag.js"></script>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes de Certificados Supervisor</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script>

		$(document).ready(function() {
		    //initializetabcontent("maintab");
		    validaCambios();

		    if ( document.getElementById("seleccionado").value == 0 ) {
               	$("#liSinAsignar").addClass("selected");
               	$("#tabSinAsignar").show();
               	$("#liAsignados").removeClass("selected").addClass("");
               	$("#tabAsignados").hide();
               	$("#liPendAceptacion").removeClass("selected").addClass("");
               	$("#tabPendAceptacion").hide();
			} else if ( document.getElementById("seleccionado").value == 1 ) {
               	$("#liSinAsignar").removeClass("selected").addClass("");
               	$("#tabSinAsignar").hide();
               	$("#liAsignados").addClass("selected");
               	$("#tabAsignados").show();
               	$("#liPendAceptacion").removeClass("selected").addClass("");
               	$("#tabPendAceptacion").hide();
			} else if ("${sessionScope.USUARIO.rolActivo}" == "CO.ENTIDAD.SUPERVISOR") {
				$("#liSinAsignar").removeClass("selected").addClass("");
               	$("#tabSinAsignar").hide();
               	$("#liAsignados").removeClass("selected").addClass("");
               	$("#tabAsignados").hide();
               	$("#liPendAceptacion").addClass("selected");
               	$("#tabPendAceptacion").show();
			} else if ("${sessionScope.USUARIO.rolActivo}" == "CO.ENTIDAD.EVALUADOR") {
				$("#liAsignados").removeClass("selected").addClass("");
               	$("#tabAsignados").hide();
               	$("#liPendAceptacion").addClass("selected");
               	$("#tabPendAceptacion").show();
			}
		});

    	// Carga la p�gina de detalle de orden para poder evaluar
    	function evaluarCertif(keyValues, keyValuesField){
    		var f = document.formulario;
	        var orden = document.getElementById(keyValues.split("|")[0]).value;
            var mto = document.getElementById(keyValues.split("|")[1]).value;
            var formato = document.getElementById(keyValues.split("|")[2]).value;
            f.orden.value = orden;
            f.mto.value = mto;
            f.formato.value = formato;
            f.action = contextName + "/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.target = "_self";
            f.submit();
	    }

	    /*function asignarEvaluador(keyValues, keyValuesField){
	    	var f = document.formulario;
	        var orden = document.getElementById(keyValues.split("|")[0]).value;
	        var ordenId = document.getElementById(keyValues.split("|")[1]).value;
	        var mto = document.getElementById(keyValues.split("|")[2]).value;
	        //alert(mto);
	        /*alert('orden = ' + orden);
	        alert('ordenId = ' + ordenId);*/
	        /*alert('voy a asignar evaluador')
	        showPopWin(contextName+"/admentco.htm?method=asignacionEvaluadorSolicitudCertificado&orden=" + orden + "&ordenId=" + ordenId + "&mto=" + mto, 400, 200, null);
	    }

    	function denegarSolicitud(keyValues, keyValuesField){
    		var f = document.formulario;
            var orden = document.getElementById(keyValues.split("|")[1]).value;
            var mto = document.getElementById(keyValues.split("|")[2]).value;
            f.action = contextName+"/admentco.htm?method=denegarSolicitud&orden="+orden+"&mto="+mto;
            f.target = "_self";
            f.submit();
    	}*/

	</script>
    <body>
		<br/>
        <ul id="maintab" class="tabs">

        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
           		<li id="liSinAsignar" class="selected" onclick="seleccionarTab(0)" ><a href="#" rel="tabSinAsignar"><span>Solicitudes por Asignar</span></a></li>
        </c:if>
        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
		              sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		              sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
           		<li id="liPendAceptacion" onclick="seleccionarTab(2)" ><a href="#" rel="tabPendAceptacion"><span>Solicitudes Pendientes de Aceptaci&oacute;n</span></a></li>
        </c:if>
        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
               	<li id="liAsignados" onclick="seleccionarTab(1)" ><a href="#" rel="tabAsignados"><span>SUCEs Aceptadas</span></a></li>
       	</c:if>
        <c:if test="${sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.SUPERVISOR' &&
                      ( sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || 
                      	sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA')}">
               	<li id="liAsignados" onclick="seleccionarTab(1)" ><a href="#" rel="tabAsignados"><span>SUCEs Aceptadas</span></a></li>
       	</c:if>
        </ul>

        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
        <div id="tabSinAsignar" class="tabcontent">
			<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesSinAsignar.jsp" />
        </div>
        </c:if>
        
        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' ||
		              sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		              sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
		<div id="tabPendAceptacion" class="tabcontent">
			<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesPendAceptacion.jsp" />
		</div>
        </c:if>

        <div id="tabAsignados" class="tabcontent">
			<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesAsignadas.jsp" />
        </div>

        <!-- <div id="tabNotificaciones" class="tabcontent">
        	Notificaciones
        </div>  -->

	    <script>

         	if (getCookie('indiceTab') != undefined){
         		if (getCookie('indiceTab') != '') {
             		document.getElementById("seleccionado").value = getCookie('indiceTab');
         		}
         	}

         	if (document.getElementById("seleccionado").value == '0') {
     			setCookie('maintab', 'tabSinAsignar');
     		} else if (document.getElementById("seleccionado").value == '1') {
     			setCookie('maintab', 'tabAsignados');
     		} else {
     			setCookie('maintab', 'tabPendAceptacion');
     		}
        	/*if (document.getElementById("seleccionado").value==1) {
               	$("#liRegi").removeClass("selected").addClass("");
               	$("#tabRegistrados").hide();
               	$("#liAsig").addClass("selected");
               	$("#tabAsignados").show();
               } else {
               	$("#liRegi").addClass("selected");
               	$("#tabRegistrados").show();
               	$("#liAsig").removeClass("selected").addClass("");
               	$("#tabAsignados").hide();
               }*/

         	initializetabcontent("maintab");

        </script>
    </body>
</html>