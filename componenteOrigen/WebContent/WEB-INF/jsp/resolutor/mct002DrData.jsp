<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>
<html>
	<head>
	<meta http-equiv="Pragma" content="no-cache" />
	<script>

	 $(document).ready(function() {

		 if (document.getElementById("DR.aceptacion").value == 'S'){
			 $("#aceptacion").attr('checked', true);

		 } else {
			 $("#aceptacion").attr('checked', false);
		 }

	 });

	</script>
	</head>
	<body>
		<table class="form">
        	<jlis:value name="coDrId" type="hidden" />
            <c:if test="${ sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' }">
	        	<tr>
	                    <td colspan="4"><h3 class="psubtitle"><span><b><jlis:label key="co.title.dr.datos_generales"  /></b></span></h3></td>
	            </tr>
				<tr>
					<th width="200">Nro. del Certificado de la Entidad</th>
					<td colspan="4">
						<jlis:value name="DR.expedienteEntidad" editable="no" type="text" align="center" style="width:92%"/>
					</td>
				</tr>
			</c:if>
			<%-- 20140717_JMC NuevoReq - No se Visualiza N�mero de Certificado - Uni�n Europea--%>
			<tr>
				<th width="200">Nro. del Certificado Origen: </th>
				<td colspan="4"><jlis:value name="DR.drEntidadNuevo" editable="no" type="text" size="50"/></td>
			</tr>
			
			<tr>
                    <td colspan="4"><h3 class="psubtitle"><span><b><jlis:label key="co.title.dr.datos_importacion"  /></b></span></h3></td>
            </tr>
			<tr>
				<td colspan="3" class="formatoCaption">Datos Iniciales</td>
			</tr>
			<tr>
				<th width="200">Pais del acuerdo</th>
				<td colspan="4">
					<jlis:value name="DR.nombrePais" editable="no" size="50" type="text"/>
				</td>
			</tr>
			<tr>
				<th width="200">Acuerdo Comercial</th>
				<td colspan="4">
					<jlis:value name="DR.nombreAcuerdo" editable="no" size="50" type="text"/>
				</td>
			</tr>
			<tr>
				<th width="200">Entidad Certificadora</th>
				<td colspan="4">
					<jlis:value name="DR.entidadNombre" editable="no" size="50" type="text"/>
				</td>
			</tr>
			<!-- tr>
				<th width="200">Sede Entidad Certificadora</th>
				<td colspan="4">
					<jlis:value name="DR.sedeNombre" editable="no" size="50" type="text"/>
				</td>
			</tr -->
			<tr>
				<th width="200">Certificado a Duplicar</th>
				<td colspan="4">
					<jlis:value name="DR.drEntidad" editable="no" size="50" type="text" />
				</td>
			</tr>
			<tr>
				<th width="200">DR por duplicar </th>
				<td colspan="4">
					<jlis:value name="DR.drIdOrigen" editable="no" size="50" type="text"/>
				</td>
			</tr>
            <c:if test="${idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 19 || idAcuerdo == 18 || idAcuerdo == 27}">
            <tr>
                <th width="200">N�mero de Referencia del Certificado</th>
                <td colspan="4">
                    <jlis:value name="nroReferenciaCertificadoOrigen" editable="no" type="text" size="50"/>
                </td>
            </tr>
            </c:if>
			<tr>
				<th width="200">Fecha Emision del Certificado Original</th>
				<td colspan="4">
					<jlis:value name="DR.fechaDrEntidad" editable="no" type="text" pattern="dd/MM/yyyy" align="center" style="width:92%"/>
				</td>
			</tr>
		</table>
		<table class="form">
			<tr>
				<td colspan="3" class="formatoCaption">Causal</td>
			</tr>
			<tr>
				<th width="200">Causal</th>
				<td colspan="4">
					<jlis:value name="DR.causalDuplicadoCo" editable="no" size="50" type="text"/>
				</td>
			</tr>
			<tr>
				<th width="200">Sustento adicional</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.sustentoAdicional" style="width:92%" />
				</td>
			</tr>
			<tr>
				<th width="200">&nbsp;</th>
				<td colspan="4">
				<jlis:value name="descAceptacion" editable="no" />
				</td>
			</tr>
			<tr>
				<th width="200">Acepto</th>
				<td colspan="4">
	            		<jlis:value name="DR.aceptacion" type="hidden" />
	                    <jlis:value name="aceptacion" type="checkbox" editable="no"  />
	                    <c:if test="${ sessionScope.USUARIO.roles['CO.USUARIO.OPERACION'] == 'CO.USUARIO.OPERACION' }">
	            		<span class="requiredValueClass">(*)</span>
	            		</c:if>
				</td>
			</tr>
		</table>
	</body>
</html>