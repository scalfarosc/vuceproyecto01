<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>
<html>
	<head>
	<meta http-equiv="Pragma" content="no-cache" />
	<script>

	 $(document).ready(function() {

		 if (document.getElementById("DR.aceptacion").value == 'S'){
			 $("#aceptacion").attr('checked', true);

		 } else {
			 $("#aceptacion").attr('checked', false);
		 }

	 });

	</script>
	</head>
	<body>
		<table class="form">
			<tr>
                <td colspan="4"><h3 class="psubtitle"><span><b><jlis:label key="co.title.dr.datos_generales"  /></b></span></h3></td>
            </tr>
            <c:if test="${ sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' }">
				<tr><th width="200">Nro. del Certificado de la Entidad</th><td colspan="4"><jlis:value name="DR.expedienteEntidad" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			</c:if>
			<tr>
                    <td colspan="4"><h3 class="psubtitle"><span><b><jlis:label key="co.title.dr.datos_importacion"  /></b></span></h3></td>
            </tr>
			<tr>
				<td colspan="3" class="formatoCaption">Datos Iniciales</td>
			</tr>
			<tr><th width="200">Certificado Origen (por anular)</th><td colspan="4"><jlis:value name="DR.drEntidad" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr><th width="200">DR (por anular)</th><td colspan="4"><jlis:value name="DR.drIdOrigen" editable="no" type="text" align="center" style="width:92%"/></td></tr>
			<tr>
				<th width="200">Fecha Emision del Certificado (por anular)</th>
				<td colspan="4">
					<jlis:value name="DR.fechaDrEntidad" editable="no" type="text" pattern="dd/MM/yyyy" align="center" style="width:92%"/>
				</td>
			</tr>
			<tr>
				<th width="200">Pais del acuerdo</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.nombrePais" style="width:92%" />
				</td>
			</tr>
			<tr>
				<th width="200">Nombre del acuerdo comercial</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.nombreAcuerdo" style="width:92%" />
				</td>
			</tr>
			<tr>
				<th width="200">Entidad Certificadora</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.entidadNombre" style="width:92%" />
				</td>
			</tr>
			<!-- tr>
				<th width="200">Sede Entidad Certificadora</th>
				<td colspan="4">
					<jlis:value name="DR.sedeNombre" editable="no" size="50" type="text" />
				</td>
			</tr -->
			<tr>
				<th width="200">&nbsp;</th>
				<td colspan="4">
	            	<jlis:value editable="no" name="descTextoAnulacion" />
				</td>

			</tr>
			<tr>
				<th width="200">Acepto</th>
				<td colspan="4">
	            		<jlis:value name="DR.aceptacion" type="hidden" />
	            		<jlis:value name="aceptacion" type="checkbox" editable="no"  />
	            		<c:if test="${ sessionScope.USUARIO.roles['CO.USUARIO.OPERACION'] == 'CO.USUARIO.OPERACION' }">
	            		<span class="requiredValueClass">(*)</span>
	            		</c:if>

				</td>
			</tr>
			<tr>
				<td colspan="3" class="formatoCaption">Motivación</td>
			</tr>
			<tr>
				<th width="200">Sustento adicional</th>
				<td colspan="4">
					<jlis:textArea rows="2" editable="no" name="DR.sustentoAdicional" style="width:92%" />
				</td>
			</tr>
		</table>
	</body>
</html>