<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%
    String contextName = request.getContextPath();
%>
<%--!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" --%>

<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes de Certificados Supervisor</title>
        <meta http-equiv="Pragma" content="no-cache" />
		<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
		<script type="text/javascript">

			$(document).ready(function() {
			    //initializetabcontent("maintab");
			    validaCambios();

	            if ( document.getElementById("seleccionado").value == 1 ) {
	               	$("#liSinAsignar").removeClass("selected").addClass("");
	               	$("#tabSinAsignar").hide();
	               	$("#liAsignados").addClass("selected");
	               	$("#tabAsignados").show();
				} else {
	               	$("#liSinAsignar").addClass("selected");
	               	$("#tabSinAsignar").show();
	               	$("#liAsignados").removeClass("selected").addClass("");
	               	$("#tabAsignados").hide();
				}

	            /*if ('${fechaDesdeAnt}' != ''){
	            	$("#fechaDesde").val('${fechaDesdeAnt}');
	            }

	            if ('${fechaHastaAnt}' != ''){
	            	$("#fechaHasta").val('${fechaHastaAnt}');
	            }*/

			});

		</script>
    </head>
    <body>
		<br/>
           	<ul id="maintab" class="tabs">
           		<li id="liSinAsignar" class="selected" onclick="seleccionarTab(0)" ><a href="#" rel="tabSinAsignar"><span>Solicitudes por Asignar </span></a></li>
               	<li id="liAsignados" onclick="seleccionarTab(1)" ><a href="#" rel="tabAsignados"><span>SUCEs Asignadas</span></a></li>
           	</ul>

        	<div id="tabSinAsignar" class="tabcontent">
				<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesSinAsignar.jsp" />
	        </div>
	        <div id="tabAsignados" class="tabcontent">
				<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesAsignadas.jsp" />
	        </div>

	    <script>

         	if (getCookie('indiceTab') != undefined){
         		if (getCookie('indiceTab') != '') {
             		document.getElementById("seleccionado").value = getCookie('indiceTab');
         		}
         	}

         	if (document.getElementById("seleccionado").value == '0') {
     			setCookie('maintab', 'tabSinAsignar');
     		} else {
     			setCookie('maintab', 'tabAsignados');
     		}
        	/*if ( document.getElementById("seleccionado").value == 1 ) {
               	$("#liSinAsignar").removeClass("selected").addClass("");
               	$("#tabSinAsignar").hide();
               	$("#liAsignados").addClass("selected");
               	$("#tabAsignados").show();
			} else {
               	$("#liSinAsignar").addClass("selected");
               	$("#tabSinAsignar").show();
               	$("#liAsignados").removeClass("selected").addClass("");
               	$("#tabAsignados").hide();
			}*/

         	initializetabcontent("maintab");

        </script>

    </body>
</html>