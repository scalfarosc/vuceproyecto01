<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Rechazo de Escrito/Subsanaci�n de SUCE</title>
    </head>

    <script language="JavaScript"
        src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
    <script>
        function validarSubmit() {
            var f = document.formulario;
            if (f.button.value=="cancelarButton") {
                window.parent.hidePopWin(true);
                return false;
            }
            return true;
        }

        function validarSubmitFormularioAdjuntos() {
            var f = document.formularioAdjuntos;
            if (f.button.value=="cargarButton") {
                disableButton("cargarButton", true);
                disableButton("eliminarButton", true);
            }
            if (f.button.value=="eliminarButton") {
                disableButton("cargarButton", true);
                disableButton("eliminarButton", true);
            }
            return true;
        }

        function aceptar() {
            var f = document.formulario;
            f.action = contextName + "/admentco.htm";
            f.method.value = "rechazarModificacionSubsanacion";
            f.button.value = "aceptarButton";
            f.target = window.parent.parent.name;
        }

        function cancelar() {
            var f = document.formulario;
            f.button.value = "cancelarButton";
        }

    </script>
    <body id="contModal">
        <jlis:modalWindow />
        <jlis:messageArea width="100%" />
        <br />
        <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="ordenId" type="hidden" />
            <jlis:value name="mto" type="hidden" />
            <jlis:value name="suceId" type="hidden" />
            <jlis:value name="formato" type="hidden" />
            <jlis:value name="modificacionSuceId" type="hidden" />
            <jlis:value name="mensajeId" type="hidden" />
            <table>
                <tr>
                    <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="aceptarButton" name="aceptar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Aceptar" alt="Pulse aqu� para aceptar la operaci�n" /></td>
                    <td><jlis:button code="CO.ENTIDAD.EVALUADOR" id="cancelarButton" name="cancelar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cancelar" alt="Pulse aqu� para cancelar la operaci�n y cerrar la ventana" /></td>
                </tr>
            </table>
            <table class="form" >
                <tr><th align="left"><jlis:label key="co.label.buzon.mensaje"/></th></tr>
                <tr><td><jlis:textArea name="mensaje" rows="10" style="width:100%" /></td></tr>
            </table>
            <div style="text-align:left;padding:10px;">
                <div>Si desea puede adjuntar un documento:</div>
            </div>
            <table>
                <tr>
                    <td>
                        <jlis:label key="co.label.documentos_adjuntar"/><br/>
                        <jlis:label key="co.label.tipo_archivos"/>
                    </td>
                </tr>
                <tr>
                    <td><input type="file" id="archivo" name="archivo" size="80"  /></td>
                </tr>
            </table>
        </form>
        <br>
        <script>
            window.parent.document.getElementById("popupTitle").innerHTML = "Rechazo";
            window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
        </script>
    </body>
</html>