<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>
<html>
	<head>
	<meta http-equiv="Pragma" content="no-cache" />
	</head>
	<script>

		function verMercancia(keyValues, keyValuesField){
	    	var drId = document.getElementById(keyValues.split("|")[0]).value;
	        var secuencia = document.getElementById(keyValues.split("|")[1]).value;
	        var idAcuerdo = '${idAcuerdo}';
	        //alert(idAcuerdo);
	        showPopWin(contextName+"/admentco.htm?method=cargarMercanciaFormDR&drId="+drId+"&secuencia="+secuencia+"&idAcuerdo="+idAcuerdo+"&codTipoDr=${codTipoDr}", 700, 430, null);
	    }

	</script>
	<body>
        <jlis:value name="coDrId" type="hidden" />
		<table class="form">
			<%--20140620_JMCA PQT-08 --%>
			<tr id="trMensajeAyudaDatosFirmante" style="display:none;">
				<td colspan="3" class="requiredValueClass">Por favor comunicarse con la entidad certificadora para consignar los datos del firmante.</td>
			</tr>
		
			<tr id="trDatosGeneralesTitle">
				<td colspan="3" class="formatoCaption">Datos Generales</td>
			</tr>
			<c:if test="${ sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' }">
				<c:if test="${ codTipoDr != 'R' }">
					<tr id="trNroCertifEntidad" style="display:">
						<th width="200">Nro. del Certificado de la Entidad</th>
						<td colspan="4"><jlis:value name="DR.expedienteEntidad" editable="no" type="text" align="center" style="width:92%"/></td>
					</tr>
				</c:if>
			</c:if>
			<tr id="trNroCertifOrigen" style="display:">
				<th width="200">Nro. del Certificado Origen: </th>
				<td colspan="4"><jlis:value name="DR.drEntidad" editable="no" type="text" size="18" maxLength="15" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trNumeroExpediente" style="display:none;" >
				<th width="200">Nro. de Expediente de la Entidad: </th>
				<td colspan="4"><jlis:value name="DR.numeroExpediente" editable="no" type="text" size="18" maxLength="15" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trFechaEmision" style="display:none" >
				<th width="200">Fecha de emisi&oacute;n: </th>
				<td colspan="4"><jlis:date form="formularioDr" name="DR.fechaEmision" editable="no"  size="20" pattern="dd/MM/yyyy"/></td>
			</tr>
			<tr id="trFechaVigencia" style="display:none" >
				<th width="200">Fecha de vigencia: </th>
				<td colspan="4"><jlis:date form="formularioDr" name="DR.fechaVigencia" editable="no"  size="20" pattern="dd/MM/yyyy"/></td>
			</tr>
			<%-- <tr>
				<th><jlis:label key="co.label.calificacion_acuerdo_dr.acuerdo_internacional_id" /></th>
				<td colspan="4"><jlis:value name="DR.nombreAcuerdo" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
			</tr> --%>
		</table>
		<table class="form" id="tbSeparadorDatosGenerales">
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
		</table>
		<table class="form">
			<tr id="trDatosImportacion">
				<td colspan="5" class="formatoCaption">Datos Importador</td>
			</tr>
			<tr id="trDatosIniciales" style="display:none">
				<td colspan="5" class="formatoCaption">Datos Iniciales</td>
			</tr>
			<tr>
				<th>Acuerdo ${acuerdo}
				<c:choose>
				      <c:when test="${idAcuerdo == 5}">
						 Internacional:
				  	  </c:when>
				  	  <c:otherwise>
				  	  	 Comercial:
				  	  </c:otherwise>
				</c:choose>
				</th>
				<td colspan="4"><jlis:value name="DR.nombreAcuerdo" editable="no" type="text" size="50" align="center" style="width:92%"/></td>
			</tr>
			<tr>
				<th>Pa�s del Acuerdo: </th>
				<td colspan="4"><jlis:value name="DR.nombrePais" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
			</tr>
			<tr>
				<th>Entidad Certificadora: </th>
				<td colspan="4"><jlis:value name="DR.entidadNombre" editable="no" type="text" size="50" align="center" style="width:92%"/></td>
			</tr>
			<!-- tr>
				<th>Sede de Entidad Certificadora: </th>
				<td colspan="4"><jlis:value name="DR.sedeNombre" editable="no" type="text" size="50" align="center" style="width:92%"/></td>
			</tr -->
			<tr id="trEmitidoPosteriori" style="display:none" >
				<th colspan="4" >
					La mercanc�a de este Certificado ya sali� del pa�s?:
				</th>
				<td><jlis:value name="DR.emitidoPosteriori" editable="no" type="text" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trEsConfidencial" style="display:none" >
				<th colspan="4" >
					Desea mantener esta informaci�n como "Confidencial"?
				</th>
				<td><jlis:value name="DR.esConfidencial" editable="no" type="text" align="center" style="width:92%"/></td>
			</tr>
			<tr class="trDatosImportacion2" style="display:none" >
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr class="trDatosImportacion2" style="display:none" >
				<td colspan="5" class="formatoCaption">Datos Importador</td>
			</tr>
			<tr id="trNombreImportador" style="display:none" >
				<th>Nombre Importador: </th>
				<td colspan="4"><jlis:textArea rows="2" editable="no" name="DR.importadorNombre" style="width:92%" /></td>
			</tr>
			<tr id="trDireccionImportador" style="display:none" >
				<th>Direcci�n Importador: </th>
				<td colspan="4"><jlis:textArea rows="2" editable="no" name="DR.importadorDireccion" style="width:92%" /></td>
			</tr>
			<tr id="trCiudadImportador" style="display:none" >
				<th>Ciudad Importador: </th>
				<td colspan="4"><jlis:textArea rows="2" editable="no" name="DR.importadorCiudad" style="width:92%" /></td>
			</tr>
			<tr id="trPaisImportador" style="display:none" >
				<th>Pa�s Importador: </th>
				<td colspan="4"><jlis:value name="DR.strImportadorPais" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trRegFiscalImportador" style="display:none" >
				<th>N�mero de registro fiscal: </th>
				<td colspan="4"><jlis:value name="DR.registroFiscalImportador" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trTelefonoImportador" style="display:none" >
				<th>N�mero de tel�fono: </th>
				<td colspan="4"><jlis:value name="DR.telefonoImportador" editable="no" type="text" size="20" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trFaxImportador" style="display:none" >
				<th>N�mero de fax: </th>
				<td colspan="4"><jlis:value name="DR.faxImportador" editable="no" type="text" size="20" align="center" style="width:92%"/></td>
			</tr>
			<tr id="trEmailImportador" style="display:none" >
				<th>Correo elect�nico: </th>
				<td colspan="4"><jlis:value name="DR.emailImportador" editable="no" type="text" size="70" align="center" style="width:92%"/></td>
			</tr>

			<tr class="trMediosTransporte" style="display:none" >
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr class="trMediosTransporte" style="display:none" >
				<td colspan="5" class="formatoCaption">Medios de Transporte y Ruta</td>
			</tr>
			<tr id="trFechaPartida" style="display:none" >
				<th>Fecha de Partida: </th>
				<td colspan="4"><jlis:date form="formulario" name="DR.fechaPartidaTransporte" editable="no"  size="20" pattern="dd/MM/yyyy"/></td>
			</tr>
			<tr id="trNumeroTransporte" style="display:none" >
				<th>Buque/Vuelo/Tren/Veh�culo No.: </th>
				<td colspan="4"><jlis:value name="DR.numeroTransporte" editable="no" type="text" size="70" align="center" style="width:92%"/></td>
			</tr>
            <tr id="trModoTransporteCarga" style="display:none" >
                <th>Tipo de Puerto carga: </th>
                <td colspan="4"><jlis:value name="DR.modoTransCarga" editable="no" type="text" align="center" style="width:92%" size="110" /></td>
            </tr>
			<tr id="trPuertoCarga" style="display:none" >
				<th>Puerto de carga: </th>
				<td colspan="4"><jlis:value name="DR.puertoCarga" editable="no" type="text" align="center" style="width:92%" size="110" /></td>
			</tr>
            <tr id="trModoTransporteDescarga" style="display:none" >
                <th>Tipo de Puerto descarga: </th>
                <td colspan="4"><jlis:value name="DR.modoTransDescarga" editable="no" type="text" align="center" style="width:92%" size="110"/></td>
            </tr>
			<tr id="trPuertoDescarga" style="display:none" >
				<th>Puerto de descarga: </th>
				<td colspan="4">
                    <span id="spanPaisDescarga" style="display:none"><jlis:value name="DR.paisDescarga" editable="no" type="text" align="center" style="width:50%" size="51"/>&nbsp;</span>
				    <jlis:value name="DR.puertoDescarga" editable="no" type="text" align="center" style="width:92%" size="51" />
				</td>
			</tr>
			<tr class="trRefProductoresMerc" style="display:none">
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr class="trRefProductoresMerc" style="display:none">
				<td colspan="5" class="formatoCaption">Referente a los productores de la Mercanc�a</td>
			</tr>
			<tr id="trTieneProductor" style="display:none" >
				<th colspan="4" >
					Desea que la informaci�n del productor de las mercanc�a para este Certificado sea incluida en el Formato Impreso del Certificado de Origen?:
				</th>
				<td><jlis:value name="DR.incluyeProductor" editable="no" type="text" align="center" style="width:92%"/></td>

			</tr>
			<tr class="trObservaciones" style="display:none">
				<td colspan="5">&nbsp;</td>
			</tr>
			<tr class="trObservaciones" style="display:none">
				<td colspan="5" class="formatoCaption">Observaciones</td>
			</tr>
			<tr>
				<th><jlis:label key="co.label.certificado.observacion" /></th>
				<td colspan="4"><jlis:textArea rows="2" editable="no" name="DR.observacion" style="width:92%" /></td>
			</tr>
			<tr>
				<th><jlis:label key="co.label.resolutor.dr.observacion.evaluador" /></th>
				<td colspan="4">
				<c:choose>
					<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' && (estadoRegistro != 'V' && estadoRegistro != 'A' || (esRectificacion == 'S' && estadoSDR == 'P'))}">
				      	<jlis:textArea rows="2" editable="yes" name="DR.observacionEvaluador" onKeyUp="valida_longitud(this, 1000);" style="width:92%" />
				  	</c:when>
				  	<c:otherwise>
				  		<jlis:textArea rows="2" editable="no" name="DR.observacionEvaluador" style="width:92%" />
				  	</c:otherwise>
				</c:choose>
				</td>
			</tr>
			<tr class="requiredValueClass" style="display:${(enIngles=='S')?'yes':'none'}" >
                <th>&nbsp;</th>
                <c:choose>
                    <c:when test="${idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9}">
                        <td colspan="4"><jlis:label key="co.alert.obs.eval.ingles_frances" /></td>
                    </c:when>
                    <c:otherwise>
                        <td colspan="4"><jlis:label key="co.alert.obs.eval.ingles" /></td>
                    </c:otherwise>
                </c:choose>
            </tr>
		</table>
		<table class="form">
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
		</table>
			<table class="form">
				<tr>
					<td colspan="3" class="formatoCaption">Facturas</td>
				</tr>
				<jlis:table keyValueColumns="ADJUNTO_ID" name="CO_Facturas" source="tFacturasDr" scope="request" pageSize="6" width="90%" navigationHeader="yes" >
					<jlis:tr>
						<jlis:td name="ES FAC. 3er PA�S" nowrap="yes" />
						<jlis:td name="NOMBRE OPERADOR 3er PA�S" nowrap="yes" />
						<jlis:td name="DIREC. OPERADOR 3er PA�S" nowrap="yes" />
						<jlis:td name="TIENE FACTURA 3er PA�S" nowrap="yes" />
						<jlis:td name="N�MERO" nowrap="yes" />
						<jlis:td name="FECHA" nowrap="yes" />
						<jlis:td name="ADJUNTO_ID" nowrap="yes" />
					</jlis:tr>
		            <c:choose>
		                <c:when test="${idAcuerdo == 2 || idAcuerdo == 5 || idAcuerdo == 6 || idAcuerdo == 7 || idAcuerdo == 8 || idAcuerdo == 9 || idAcuerdo == 10 || idAcuerdo == 11 || idAcuerdo == 12 || idAcuerdo == 15 || idAcuerdo == 16 || idAcuerdo == 17 || idAcuerdo == 19 || idAcuerdo == 20 || idAcuerdo == 21 || idAcuerdo == 22  || idAcuerdo == 25 || idAcuerdo == 26 || idAcuerdo == 27}">

							<jlis:columnStyle column="1" columnName="FACTURADOTERCERPAIS" />
							<jlis:columnStyle column="2" columnName="OPERADORTERCERPAIS" />
		                </c:when>
		                <c:otherwise>
							<jlis:columnStyle column="1" columnName="FACTURADOTERCERPAIS" hide="yes" />
							<jlis:columnStyle column="2" columnName="OPERADORTERCERPAIS" hide="yes" />
		                </c:otherwise>
		            </c:choose>
		            <c:choose>
		                <c:when test="${idAcuerdo == 6 || idAcuerdo == 9 || idAcuerdo == 10 || idAcuerdo == 11 || idAcuerdo == 15 || idAcuerdo == 16 || idAcuerdo == 17 || idAcuerdo == 20 || idAcuerdo == 21 || idAcuerdo == 22 || idAcuerdo == 23 || idAcuerdo == 25 || idAcuerdo == 26 || idAcuerdo == 27}">
							<jlis:columnStyle column="3" columnName="DIRECCIONTERCERPAIS" />
		                </c:when>
		                <c:otherwise>
							<jlis:columnStyle column="3" columnName="DIRECCIONTERCERPAIS" hide="yes" />
		                </c:otherwise>
		            </c:choose>
		            <c:choose>
		                <c:when test="${ idAcuerdo == 6 || idAcuerdo == 7 || idAcuerdo == 11 || idAcuerdo == 12 || idAcuerdo == 15 || idAcuerdo == 17 || idAcuerdo == 19 || idAcuerdo == 20 || idAcuerdo == 21 || idAcuerdo == 22 || idAcuerdo == 23 || idAcuerdo == 25 || idAcuerdo == 26 || idAcuerdo == 27}">
							<jlis:columnStyle column="4" columnName="TIENEFACTURATERCERPAIS" />
		                </c:when>
		                <c:otherwise>
							<jlis:columnStyle column="4" columnName="TIENEFACTURATERCERPAIS" hide="yes" />
		                </c:otherwise>
		            </c:choose>
					<jlis:columnStyle column="5" columnName="NUMERO" editable="yes" />
					<jlis:columnStyle column="6" columnName="FECHA" type="dateTime" pattern="dd/MM/yyyy"/>
		            <jlis:columnStyle column="7" columnName="ADJUNTO_ID" hide="yes" />
		            <jlis:tableButton column="5" type="link" onClick="descargarAdjunto" highlight="no" />
			 	</jlis:table>
			</table>
		<table class="form">
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
		</table>
		<table class="form">
			<tr>
				<td colspan="3" class="formatoCaption">Mercancias</td>
			</tr>
			<jlis:table keyValueColumns="MCT001_DR_ID,SECUENCIA_MERCANCIA" name="CO_Mercancias" source="tMercanciasDr" scope="request" pageSize="6" width="90%" navigationHeader="yes" >
					<jlis:tr>
						<jlis:td name="DR_ID" nowrap="yes" />
						<jlis:td name="SECUENCIA" nowrap="yes" />
						<jlis:td name="ITEM" nowrap="yes" />
						<jlis:td name="DESCRIPCI�N" nowrap="yes" />
						<jlis:td name="CALIFICACI�N" nowrap="yes" />
			            <c:choose>
			                <c:when test="${idAcuerdo == 4 || idAcuerdo == 5}">
								<jlis:td name="NALADISA" nowrap="yes" />
			                </c:when>
			                <c:when test="${idAcuerdo == 6 || idAcuerdo == 15}">
								<jlis:td name="SISTEMA ARMONIZADO" nowrap="yes" />
			                </c:when>
			                <c:when test="${idAcuerdo == 12 || idAcuerdo == 23 || idAcuerdo == 27}">
								<jlis:td name="SUBPARTIDA ARANCELARIA" nowrap="yes" />
			                </c:when>
			                <c:otherwise>
								<jlis:td name="NANDINA" nowrap="yes" />
			                </c:otherwise>
			            </c:choose>
						<jlis:td name="NORMA" nowrap="yes" />
						<jlis:td name="CRITERIO ORIGEN" nowrap="yes" />
						<jlis:td name="CRITERIO ORIGEN CERTIF." nowrap="yes" />
						<jlis:td name="DETALLE DENEGACI�N" nowrap="yes" />
						<jlis:td name="CANTIDAD" nowrap="yes" />
						<jlis:td name="UNIDAD MEDIDA" nowrap="yes" />
						<jlis:td name="FACTURA" nowrap="yes" />
						<jlis:td name="VALOR FACTURADO US" nowrap="yes" />
						<jlis:td name="VER" nowrap="yes" />
					</jlis:tr>
					<jlis:columnStyle column="1" columnName="MCT001_DR_ID" hide="yes"/>
					<jlis:columnStyle column="2" columnName="SECUENCIA_MERCANCIA" hide="yes"/>
					<jlis:columnStyle column="3" columnName="ITEM_CO_MERCANCIA" />
					<jlis:columnStyle column="4" columnName="DESCRIPCION"  />
					<jlis:columnStyle column="5" columnName="CALIFICACION"/>
					<jlis:columnStyle column="6" columnName="PARTIDA_SEGUN_ACUERDO" hide="yes" />
					<jlis:columnStyle column="7" columnName="NORMA"/>
					<jlis:columnStyle column="8" columnName="CRITERIO_ORIGEN" />
					<jlis:columnStyle column="9" columnName="CRITERIO_ORIGEN_CERTIFICADO" editable="no"/>
					<jlis:columnStyle column="10" columnName="DETALLE_DENEGACION" hide="yes"/>
					<jlis:columnStyle column="11" columnName="CANTIDAD" hide="yes" />
					<jlis:columnStyle column="12" columnName="NOMBRE_COMERCIAL" hide="yes" />
					<jlis:columnStyle column="13" columnName="NUMERO" hide="yes" />
					<jlis:columnStyle column="14" columnName="VALOR_FACTURADO_US" hide="yes" />
		            <%-- <c:choose>
		                <c:when test="${idAcuerdo != 4}">
							<jlis:columnStyle column="11" columnName="CANTIDAD" />
							<jlis:columnStyle column="12" columnName="NOMBRE_COMERCIAL" />
							<jlis:columnStyle column="13" columnName="FACTURA" />
							<jlis:columnStyle column="14" columnName="VALOR FACTURADO US" />
		                </c:when>
		                <c:otherwise>
		                </c:otherwise>
		            </c:choose> --%>
					<jlis:columnStyle column="15" columnName="VER" editable="yes" align="center" />
					<jlis:tableButton column="15" type="imageButton" image="/co/imagenes/ver.gif" onClick="verMercancia" alt="Ver Mercanc�a" />
		 	</jlis:table>
		</table>
		<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
		<table class="form">
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
		</table>
			<%-- <table class="form">
				<tr>
					<td colspan="3" class="formatoCaption">Sobre el certificado firmado</td>
				</tr>
				<tr >
					<th>Usuario que firma (Entidad Exportadora): </th>
					<td colspan="4"><jlis:value name="DR.usuarioFirmanteExp" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
				</tr>
				<tr >
					<th>Usuario que firma (Entidad Evaluadora): </th>
					<td colspan="4"><jlis:value name="DR.usuarioFirmanteEev" editable="no" type="text" size="30" align="center" style="width:92%"/></td>
				</tr>
				<tr >
					<th>Fecha firma de la Entidad Evaluadora: </th>
					<td colspan="4"><jlis:date form="formulario" name="DR.fechaFirmaEev" editable="no"  size="20" pattern="dd/MM/yyyy"/></td>
				</tr>
			</table> --%>
		</c:if>
	</body>
</html>