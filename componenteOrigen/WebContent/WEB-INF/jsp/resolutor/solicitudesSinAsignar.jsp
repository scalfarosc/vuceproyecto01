<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="org.jlis.core.util.*"%>
<%@page import="pe.gob.mincetur.vuce.co.bean.UsuarioCO"%>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    UsuarioCO usuario = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
    HashUtil<String, Object> filter = new HashUtil<String, Object>();
    filter.put("entidadId", usuario.getIdEntidad());
    request.setAttribute("filterEvaluadores", filter);
%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes Asignadas</title>
        <meta http-equiv="Pragma" content="no-cache" />
        <script type="text/javascript">
        	
        //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
        	$(document).ready(function() {
        		var estadoAcuerdoPais = '${estadoAcuerdoPais3}';
        		if (estadoAcuerdoPais=='I') {
        			alert("No se puede asignar la solicitud, el acuerdo comercial con este pa�s no se encuentra vigente");
        		}
        		
        	});
        
        // Permite revisar informaci�n del certificado
		function verCertificado(keyValues, keyValuesField){
            var f = document.formulario;
            f.orden.value = document.getElementById(keyValues.split("|")[1]).value;
            f.mto.value = document.getElementById(keyValues.split("|")[2]).value;
            f.formato.value = document.getElementById(keyValues.split("|")[3]).value;
            //var formato = document.getElementById(keyValues.split("|")[2]).value;
            //var frmlow = formato.toLowerCase();
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.submit();
	    }
        
        function seleccionSolicitudParaAsignar(keyValues, keyValuesField) {
        	
        }
        
		function asignarEvaluador() {
	    	var f = document.formulario;
	        
	    	if (document.getElementById("evaluadorId").value=="") {
		        alert("Debe seleccionar un Evaluador");
		        f.button.value = 'cancelarButton';
		        return;
		    }
	    	
	    	var indice = -1;
	    	var seleccione = "";
	    	
	    	if (f.seleccione!=null) {
			    if (eval(f.seleccione.length)) {
			        for (var i=0; i < f.seleccione.length; i++) {
			            if (f.seleccione[i].checked){
			            	seleccione = seleccione + f.seleccione[i].value + "*";
			                indice = i;
			            }
			        }
			        if (indice!=-1) {
			        	//seleccione = seleccione.replace(/*+$/, "");
			        	seleccione = seleccione.slice(0, -1);
			        }
			    } else {
			        if (f.seleccione.checked) {
			        	seleccione = f.seleccione.value;
			            indice = 0;
			        }
			    }
			}
			if (indice!=-1) {
				if (!confirm("�Est� seguro que desea asignar los registros seleccionados al Evaluador seleccionado?")) {
				    f.button.value = 'cancelarButton';
				    f.masivo.value = "N";
				} else {
			    	f.action = contextName+"/admentco.htm";
			        f.method.value="asignarEvaluadorSolicitudCertificado";
			        f.masivo.value = "S";
			        f.submit();
				}
			} else {
			    alert('Seleccione por lo menos un registro');
			    f.button.value = 'cancelarButton';
			    f.masivo.value = "N";
			}
	    }
        
        </script>
    </head>
    <body>
        <jlis:value name="masivo" type="hidden" />
        
        
        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}">        
        <p align="left" style="padding-left: 35px;">
		    <jlis:label key="co.label.asignar_evaluador.evaluador" />&nbsp;
		    <jlis:selectProperty name="evaluadorId" key="resolutor.administrador.evaluadores_entidad.select" filter="${filterEvaluadores}" />&nbsp;
            <jlis:button code="CO.ENTIDAD.SUPERVISOR" id="asignarButton" name="asignarEvaluador()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Asignar" alt="Pulse aqu� para asignar evaluador a las Solicitudes seleccionadas" />
        </p>
        </c:if>
        
		<jlis:table keyValueColumns="ORDEN,ORDENID,MTO,NOMBREFORMATO" name="CO_SIN_ASIGNAR" key="resolutor.administrador.solicitudes_sin_asignar.grilla" pageSize="15" width="90%" navigationHeader="yes" filter="${filterBandeja}" >
				<jlis:tr>
				    <jlis:td name="&nbsp;" width="1%" nowrap="yes" style="checkbox=formulario,seleccione,on-off" />
					<jlis:td name="TUPA" nowrap="yes" />
					<jlis:td name="FORMATO" nowrap="yes" />
					<jlis:td name="NOMBRE" nowrap="yes" />
					<jlis:td name="ORDENID" nowrap="yes" />
					<jlis:td name="MTO" nowrap="yes" />
					<jlis:td name="ACUERDO INTERNACIONAL ID" nowrap="yes" />
					<jlis:td name="ACUERDO" nowrap="yes" width="20%" />
					<jlis:td name="PAIS" nowrap="yes" width="10%" />
					<jlis:td name="USUARIO" nowrap="yes" width="10%"/>
					<jlis:td name="SOLICITUD" nowrap="yes" width="10%"/>
					<jlis:td name="ESTADO REGISTRO ID" nowrap="yes" />
					<jlis:td name="ESTADO DEL REGISTRO" nowrap="yes" width="15%"/>
					<jlis:td name="FECHA DE REGISTRO" nowrap="yes" width="10%" />
					<jlis:td name="FECHA DE TRANSMISI�N" nowrap="yes" />
					<jlis:td name="NOMBRE ENTIDAD" nowrap="yes" />
					<jlis:td name="ENTIDAD" nowrap="yes" />
					<jlis:td name="TUPA ID" nowrap="yes" />
					<jlis:td name="FORMATO ID" nowrap="yes" />
					<jlis:td name="ASIGNAR" nowrap="yes" />
					<jlis:td name="DENEGAR" nowrap="yes" />
				</jlis:tr>
				<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}">
				<jlis:columnStyle column="1" columnName="SELECCIONE" editable="yes" align="center" />
				</c:if>
				<c:if test="${sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.SUPERVISOR'}">
				<jlis:columnStyle column="1" columnName="SELECCIONE" hide="yes"/>
				</c:if>
				<jlis:columnStyle column="2" columnName="NOMBRETUPA" />
				<jlis:columnStyle column="3" columnName="NOMBREFORMATO" />
				<jlis:columnStyle column="4" columnName="NOMBRECUT" />
				<jlis:columnStyle column="5" columnName="ORDENID" hide="yes" />
				<jlis:columnStyle column="6" columnName="MTO" hide="yes" />
				<jlis:columnStyle column="7" columnName="ACUERDOINTERNACIONALID" hide="yes" />
				<jlis:columnStyle column="8" columnName="NOMBREACUERDOINTERNACIONAL" />
				<jlis:columnStyle column="9" columnName="NOMBREPAIS" />
				<jlis:columnStyle column="10" columnName="NOMBREUSUARIO" />
				<jlis:columnStyle column="11" columnName="ORDEN" editable="yes" />
				<jlis:columnStyle column="12" columnName="ESTADOREGISTRO" hide="yes" />
				<jlis:columnStyle column="13" columnName="DESCRIPCIONESTADOREGISTRO" />
				<jlis:columnStyle column="14" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" />
				<jlis:columnStyle column="15" columnName="FECHATRANSMISION" hide="yes" />
				<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' || sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="16" columnName="NOMBREENTIDAD" />
				</c:if>
				<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] != 'CO.CENTRAL.OPERADOR_FUNCIONAL' && sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] != 'CO.ADMIN.HELP_DESK'}">
				<jlis:columnStyle column="16" columnName="NOMBREENTIDAD" hide="yes" />
				</c:if>
				<jlis:columnStyle column="17" columnName="ENTIDADID" hide="yes" />
				<jlis:columnStyle column="18" columnName="TUPAID" hide="yes" />
				<jlis:columnStyle column="19" columnName="FORMATOID" hide="yes" />
				<jlis:columnStyle column="20" columnName="ASIGNAR" hide="yes" />
				<jlis:columnStyle column="21" columnName="DENEGAR" hide="yes" />
				<jlis:tableButton column="1" type="checkbox" name="seleccione" onClick="seleccionSolicitudParaAsignar" />
				<jlis:tableButton column="20" type="imageButton" image="/co/imagenes/flecha_asignar.gif" onClick="asignarEvaluador" alt="Asignar Evaluador" />
				<jlis:tableButton column="21" type="imageButton" image="/co/imagenes/rechazar.png" onClick="denegarSolicitud" alt="Denegar solicitud" />
        		<jlis:tableButton column="11" type="link" onClick="verCertificado" />
				<%-- <c:choose>
     				<c:when test="${bloqueado == 'N'}">
						<jlis:tableButton column="16" type="imageButton" image="/co/imagenes/flecha_asignar.gif" onClick="asignarEvaluador" alt="Asignar Evaluador" />
				 	</c:when>
					<c:otherwise>
					    <jlis:tableButton column="16" type="imageButton" image="/co/imagenes/flecha_asignar.gif" onClick="asignarEvaluador" alt="Asignar Evaluador" />
					</c:otherwise>
				</c:choose>  --%>
		 	</jlis:table>

    </body>
</html>