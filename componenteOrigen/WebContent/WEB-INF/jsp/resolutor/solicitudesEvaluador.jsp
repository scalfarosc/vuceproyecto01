<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@page import="org.jlis.core.bean.*"%>
<%@page import="org.jlis.core.util.*"%>
<%@page import="org.jlis.core.list.*"%>
<%@page import="org.jlis.web.list.*"%>
<%@page import="org.jlis.web.util.*"%>
<%
    String contextName = request.getContextPath();
%>
<%--!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" --%>
<script src="<%=contextName%>/resource/js/functions.js"></script>
<script src="<%=contextName%>/resource/js/menu.js"></script>
<script src="<%=contextName%>/resource/js/subModal/common.js"></script>
<script src="<%=contextName%>/resource/js/subModal/drag.js"></script>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes de Certificados</title>
        <meta http-equiv="Pragma" content="no-cache" />
        <script type="text/javascript">

        	// Carga la p�gina de detalle de orden para poder evaluar
	    	function evaluarCertif(keyValues, keyValuesField){
	    		var f = document.formulario;
		        var orden = document.getElementById(keyValues.split("|")[0]).value;
	            var mto = document.getElementById(keyValues.split("|")[1]).value;
	            var formato = document.getElementById(keyValues.split("|")[2]).value;
	            f.orden.value = orden;
	            f.mto.value = mto;
	            f.formato.value = formato;
	            f.action = contextName + "/origen.htm";
	            f.method.value = "cargarInformacionOrden";
	            f.target = "_self";
	            f.submit();
		    }

        	// Carga la p�gina que permite realizar acciones sobre la SUCE
	    	function editarSolicitudCalificada(keyValues, keyValuesField){
	    		var f = document.formulario;
		        var orden = document.getElementById(keyValues.split("|")[0]).value;
	            var mto = document.getElementById(keyValues.split("|")[1]).value;
	            var formato = document.getElementById(keyValues.split("|")[2]).value;
	            f.formato.value = formato.toLowerCase();
	            //alert('abriendo la suce ' + formato);
	            f.action = contextName+"/admentco.htm?method=abrirSuce&orden="+orden+"&mto="+mto;
	            f.target = "_self";
	            f.submit();
		    }

			function verCertificado(keyValues, keyValuesField){
	            var f = document.formulario;
	            f.orden.value = document.getElementById(keyValues.split("|")[0]).value;
	            f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
	            f.formato.value = document.getElementById(keyValues.split("|")[2]).value;
	            //var formato = document.getElementById(keyValues.split("|")[2]).value;
	            //alert(f.formato.value);
	            //var frmlow = formato.toLowerCase();
	            f.action = contextName+"/origen.htm";
	            f.method.value = "cargarInformacionOrden";
	            f.submit();
		    }

			//Funci�n que reemplaza un par�metro de la invocaci�n onClick para los �conos de avance y retroceso en las tablas del JLIS
			function reemplazarPrmtPaginacionJLIS(prmtBusqueda, nvoValor){
				var indInicio;
				var indFin;
				var txtAReemplazar;
				var indIgual;
				var txtNuevo;

        		$("b").each(function (i){

        			if (this.className == 'first' || this.className == 'previous' || this.className == 'next' || this.className == 'last'){
        				indInicio= this.attributes.onclick.value.indexOf(prmtBusqueda+'=');

        				if (indInicio > -1) {

        					indFin = this.attributes.onclick.value.indexOf('&', indInicio);

        	    			if (indFin == -1) {
        	    				indFin = this.attributes.onclick.value.indexOf("'", indInicio);
        	    			}

        					txtAReemplazar = this.attributes.onclick.value.substring(indInicio, indFin);
        					indIgual = txtAReemplazar.indexOf('=');

        					txtNuevo = txtAReemplazar.replace(txtAReemplazar.substring(indIgual, txtAReemplazar.length), '='+nvoValor);

        					//alert(this.attributes.onclick.value);
        					this.attributes.onclick.value = this.attributes.onclick.value.replace(txtAReemplazar, txtNuevo);
        					//alert(this.attributes.onclick.value);

        					try{
        						console.log('M�todo reemplazarPrmtPaginacionJLIS: El par�metro "' + prmtBusqueda + '" ha sido reemplazado para el m�todo onClick de la clase ' + this.className + ', el nuevo valor es ' + nvoValor + '.');
        					} catch(e){

        					}

        				} else {
        					try{
        						console.log('Error en el m�todo reemplazarPrmtPaginacionJLIS: No existe el par�metro "' + prmtBusqueda + '" para el m�todo onClick de la clase ' + this.className + '.');
        					} catch(e){

        					}
        				}
        			}
        		});

			}

        </script>
    </head>
    <body>
		<br/>

        <ul id="maintab" class="tabs">
			<li id="liPendAceptacion" onclick="seleccionarTab(0)" ><a href="#" rel="tabPendAceptacion"><span>SUCEs Pendientes de Aceptaci&oacute;n</span></a></li>
			<li id="liAsignados" onclick="seleccionarTab(1)" ><a href="#" rel="tabAsignados"><span>SUCEs Aceptadas</span></a></li>
		</ul>

		<div id="tabPendAceptacion" class="tabcontent">
			<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesPendAceptacion.jsp" />
		</div>

        <div id="tabAsignados" class="tabcontent">
			<jsp:include page="/WEB-INF/jsp/resolutor/solicitudesAsignadas.jsp" />
        </div>

	    <script>

         	if (getCookie('indiceTab') != undefined){
         		if (getCookie('indiceTab') != '') {
             		document.getElementById("seleccionado").value = getCookie('indiceTab');
         		}
         	}
         	if (document.getElementById("seleccionado").value == '0') {
     			setCookie('maintab', 'tabPendAceptacion');
     		} else {
     			setCookie('maintab', 'tabAsignados');
     		}
        	/*if ( document.getElementById("seleccionado").value == 1 ) {
               	$("#liPendAceptacion").removeClass("selected").addClass("");
               	$("#tabPendAceptacion").hide();
               	$("#liAsignados").addClass("selected");
               	$("#tabAsignados").show();
   			} else {
              	$("#liPendAceptacion").addClass("selected");
              	$("#tabPendAceptacion").show();
              	$("#liAsignados").removeClass("selected").addClass("");
              	$("#tabAsignados").hide();
   			}*/

         	initializetabcontent("maintab");


        </script>
    </body>
</html>