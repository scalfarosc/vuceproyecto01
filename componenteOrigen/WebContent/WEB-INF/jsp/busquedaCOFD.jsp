
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Busqueda de Certificados de Origen</title>
</head>
 <style>
    input, select{
      height: 21;
      padding:3px;
    }

    .srch input, select{
       box-sizing: border-box;
       -webkit-box-sizing: border-box;
	   -moz-box-sizing: border-box; 
	   width: 300px;
	   padding-top:3px;
    }

	.top_label{
	    float: left;
    	padding-top:5px;
    	margin-right:10px;
	}
	
	.top_label label{
        text-decoration: none;
        color:#565B5E;
		font-weight: bold;
		position: absolute;
		opacity: 1;
		transform: translateY(calc(-14px));
		font-size: 11px;
		display:block;
    } 
    
    .buttonSubmitClass{
       width: 100px;
    }
	

}
	
    </style>

<script type="text/javascript">

    function validarSubmit() {
    var f = document.formulario;
    if (f.button.value=="cancelarButton") {
      return false;
        }
    if (f.button.value=="cerrarPopUpButton") {
            return validaCerraPopUpNoAction();
        }
    return true;
  }

  function buscar(){
     var f = document.formulario;
     if(validaCampos()) {
       f.action = contextName+"/origen.htm";
       f.method.value="busquedaCOFD";
       f.button.value="busquedaCOFD";
     } else {
       f.button.value="cancelarButton";
     }
  }

    function validaCampos() {
      var f = document.formulario;
      return true;
    }

    function cargarPaises(obj) {
      var filter = "idAcuerdo="+obj.value;
      loadSelectAJX("ajax.selectLoader", "loadList", "pais", "comun.pais_iso_x_acuerdo.select", filter, null, null, null);
      }

    function descargarDocumento(keyValues, keyValuesField) {
    	var f = document.formulario;
    	var codId = document.getElementById(keyValues.split("|")[0]).value;
        var codCertificado = document.getElementById(keyValues.split("|")[1]).value;
        var certificado = document.getElementById(keyValues.split("|")[2]).value;
        f.action = contextName+"/origen.htm?method=descargarDocFD&codId="+codId+"&codCertificado="+codCertificado+"&certificado="+certificado;
        f.submit();
    }
    
    function changeVigencia(obj){
    	$("#mostrarNoVigentes").val(obj.value);
    }
    
    <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && ((sessionScope.USUARIO.login == 'EXTA0681')) }" >
    function generarDocumento(keyValues, keyValuesField) {
    	var f = document.formulario;
    	var codId = document.getElementById(keyValues.split("|")[0]).value;
        var codCertificado = document.getElementById(keyValues.split("|")[1]).value;
        var certificado = document.getElementById(keyValues.split("|")[2]).value;
        f.action = contextName+"/origen.htm?method=generarDocumento&codId="+codId+"&codCertificado="+codCertificado+"&certificado="+certificado;
        f.submit();
    }
</c:if> 
    
    
	$(document).ready(function(){

	    if("S"==$("#mostrarNoVigentes").val()) {
	    	$("#mostrar_S").attr('checked', true);
	    }
	    else{ 
	    	$("#mostrar_N").attr('checked', true);
	    	
	    }

	    $( "#selPaisExportador" ).change(function() {
	    	$("#paisExportador").val($("#selPaisExportador").val());
	    	});

	    
	    <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && (sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' ||
	    		sessionScope.USUARIO.roles['VUCE.SUNAT.ESPECIALISTA'] == 'VUCE.SUNAT.ESPECIALISTA'||
	    		sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL')}" >
  		  $(".tr_hd").show();
	    </c:if>  
	    
	});
	
</script>
<body>
<jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
	<div id="pageTitle">
		<h1><strong><jlis:label  labelClass="pageTitle"  key="co.title.busquedaCOFD" /></strong></h1>
	</div>
<jlis:messageArea width="100%" />
<form name="formulario" method="post" onSubmit="return validarSubmit();">
  	<input type="hidden" name="method" value=""/>
  	<input type="hidden" name="button" value="" />
	<jlis:value type="hidden" name="formato" />
	<jlis:value type="hidden" name="mostrarNoVigentes" />
	<jlis:value type="hidden" name="paisExportador" />
	
  <table class="form">
    <tr>
      <th><jlis:label key="co.label.co_busqueda.acuerdo" /></th>
      <td><jlis:selectProperty key="certificado_origen.acuerdos.select"  name="acuerdo" style="align:right;" onChange="cargarPaises(this);" editable="no" /></td>
    </tr>
    <tr>
        <th style="width:15%"><jlis:label key="co.label.nco" /></th>
        <td class="srch"><jlis:value name="numCertificado"  maxLength="32"  type="text" /></td>
    </tr>
    <tr class="tr_hd" style="display:none">
      <th><jlis:label key="co.label.idco" /><img src="/co/imagenes/help.png" title="Es el ID de Certificado de Origen proporcionado por el Exportador" help="yes"></th>
      <td class="srch"><jlis:value name="idCertificado" maxLength="44" type="text" /></td>
    </tr>
    <tr class="tr_hd" style="display:none">
        <th><jlis:label key="co.label.co_busquedaCOFD.registro_fiscal" />
        <img src="/co/imagenes/help.png" title="Es el n�mero de Registro �nico de Contribuyentes (RUC) o cualquier otro documento autorizado para realizar operaciones tributarias o aduaneras de conformidad con la legislaci�n nacional, indicado en el 'Campo 5 - Nombre y Domicilio del importador'." help="yes"></th>
        <td class="srch"><jlis:value name="numRegistroFiscal"  maxLength="32"  type="text" /></td>
    </tr>
    <tr class="tr_hd" style="display:none">
      <th><jlis:label key="co.label.co_busquedaCOFD.fechaEmision" /></th>
      <td>
         <div class="top_label">
		      <jlis:label key="co.label.co_busquedaCOFD.desde" />
		      <jlis:date name="fechaDesde" form="formulario" />
	     </div>
	     <div class="top_label">
		      <jlis:label key="co.label.co_busquedaCOFD.hasta" />
		      <jlis:date name="fechaHasta" form="formulario" />
	      </div>
      </td>
     </tr>

	<tr class="tr_hd" style="display:none">
      <th><jlis:label key="co.label.co_busquedaCOFD.paisExportador" /></th>
      <td class="srch"><jlis:selectProperty key="comun.pais_iso_x_acuerdo.select" filter="${filterAcuerdo}" name="selPaisExportador" style="align:right;" onChange="selPais(this);" /></td>
    </tr>
    <tr>
      <th><jlis:label key="co.label.co_busquedaCOFD.mostrarVigentes" /></th>
      <td>
      	<jlis:value type="radio" name="mostrar" checkValue="S" onClick="changeVigencia(this)" style="vertical-align:middle;"/><jlis:label key="co.label.si" style="text-align:center" />&nbsp;&nbsp;&nbsp;&nbsp;
      	<jlis:value type="radio" name="mostrar" checkValue="N" onClick="changeVigencia(this)" style="vertical-align:middle;" /><jlis:label key="co.label.no" style="text-align:center" />
      </td>
    </tr>    
    <tr>
    	<td></td>
        <td>
            <jlis:button id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Pulse aqu� para iniciar la busqueda" />
        </td>
    </tr>
  </table>
  <br/>

   <jlis:table keyValueColumns="COD_ID,CERTIFICADO_ID,CERTIFICADO" name="COFD" source="tCertificados" scope="request">
                <jlis:tr>
                    <jlis:td name="ID COD" nowrap="yes" />
                    <jlis:td name="ID CERTIFICADO" nowrap="yes" />
                    <jlis:td name="N� CERTIFICADO" nowrap="yes" />
                    <jlis:td name="NOMBRE IMPORTADOR" nowrap="yes"  />
                    <jlis:td name="FECHA EMISION" nowrap="yes"  />
                    <jlis:td name="FECHA VIGENCIA" nowrap="yes"  />
                    <jlis:td name="CERTIFICADO VIGENTE" nowrap="yes"  />
                    <jlis:td name="VER DOCUMENTO" nowrap="yes" width="5%" />
                    <jlis:td name="GENERAR DOCUMENTO" nowrap="yes" width="5%" />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="COD_ID"  align="center" hide="yes"/>
                <jlis:columnStyle column="2" columnName="CERTIFICADO_ID"  align="center" />
                <jlis:columnStyle column="3" columnName="CERTIFICADO" align="center" />
                <jlis:columnStyle column="4" columnName="NOMBRE_IMPORTADOR" align="center" />
                <jlis:columnStyle column="5" columnName="FECHA_EMISION"  align="center"  pattern="dd/MM/yyyy"/>
                <jlis:columnStyle column="6" columnName="FECHA_VIGENCIA"  align="center"  pattern="dd/MM/yyyy" hide="yes" />
                <jlis:columnStyle column="7" columnName="CERTIFICADO_VIGENTE"  align="center" hide="yes" />
                <jlis:columnStyle column="8" columnName="RESULTADO_VALIDACION" editable="yes" align="center" validator="pe.gob.mincetur.vuce.co.web.util.BusquedaCOFDCellValidator"/>
                <jlis:columnStyle column="9" columnName="GENERAR" editable="${(sessionScope.USUARIO.login == 'EXTA0681')?'yes':'no'}" align="center" hide="${(sessionScope.USUARIO.login == 'EXTA0681')?'no':'yes'}"/>
                <jlis:tableButton column="8" type="imageButton" />
                <jlis:tableButton column="9" type="link" onClick="generarDocumento" />
            </jlis:table>
       
</form>
        </div></div>
        <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
</body>
</html>
