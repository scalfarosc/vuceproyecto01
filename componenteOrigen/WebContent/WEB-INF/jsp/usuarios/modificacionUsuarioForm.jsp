<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
       <title>Sistema COMPONENTE ORIGEN</title>
       <meta http-equiv="Pragma" content="no-cache" />
    </head>

    <script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
    <script language="JavaScript">
        $(document).ready(function() {
            initializetabcontent("maintab");
            $('input[type="text"],select,textarea').focus(function() {
                $(this).removeClass("inputTextClass").addClass("focusField");
            });
            $('input[type="text"],select,textarea').blur(function() {
                   if(this.value == this.defaultValue){
                       $(this).removeClass("focusField").addClass("inputTextClass");
                   }
            });
        });

        function validarSubmit() {
            var f = document.formulario;
            var button = f.button;

            if (button.value=="actualizarUsuarioButton") {
            	if (document.getElementById("USUARIO.recibeNotificacion").checked){
                	if (document.getElementById("USUARIO.email").value=="") {
	                    changeStyleClass("co.title.datos_usuario.email", "errorValueClass");
	                    alert("Debe ingresar el email.");
	                    return false;
                	}

                }
                if (document.getElementById("USUARIO.nombre").value=="") {
                    changeStyleClass("co.title.datos_usuario.nombre", "errorValueClass");
                    alert("Debe ingresar los Apellidos y Nombres.");
                    return false;
                } else {
                	if (!js_ContieneCaracter(document.getElementById("USUARIO.nombre"), "co.title.datos_usuario.nombre", "El Nombre debe contener por lo menos una letra.")){
                		return false;
                	}
                }
                if (document.getElementById("USUARIO.tipodocumentoUS").value=="") {
                    changeStyleClass("co.title.datos_usuario.tipoDocUS", "errorValueClass");
                    alert("Debe ingresar el Tipo de Documento de identificaci�n.");
                    return false;
                }
                if (document.getElementById("USUARIO.numdocUS").value=="") {
                    changeStyleClass("co.title.datos_usuario.nrodocUS", "errorValueClass");
                    alert("Debe ingresar el N�mero de Documento de identificaci�n.");
                    return false;
                }
                if (document.getElementById("USUARIO.direccion").value=="") {
                    changeStyleClass("co.title.datos_usuario.direccion", "errorValueClass");
                    alert("Debe ingresar la direccion.");
                    return false;
                } else {
                	if (!js_ContieneCaracter(document.getElementById("USUARIO.direccion"), "co.title.datos_usuario.direccion", "La direcci�n debe contener por lo menos una letra.")){
                		return false;
                	}
                }
                if (document.getElementById("departamento").value=="") {
                    changeStyleClass("co.title.datos_usuario.departamento", "errorValueClass");
                    alert("Debe ingresar el departamento.");
                    return false;
                }
                if (document.getElementById("provincia").value=="") {
                    changeStyleClass("co.title.datos_usuario.provincia", "errorValueClass");
                    alert("Debe ingresar la provincia.");
                    return false;
                }
                if (document.getElementById("USUARIO.distrito").value=="") {
                    changeStyleClass("co.title.datos_usuario.distrito", "errorValueClass");
                    alert("Debe ingresar el distrito.");
                    return false;
                }
                <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
                if (document.getElementById("USUARIO.area").value=="") {
                    changeStyleClass("co.title.datos_usuario.area", "errorValueClass");
                    alert("Debe ingresar el �rea.");
                    return false;
                }
                if (document.getElementById("USUARIO.subArea").value=="") {
                    changeStyleClass("co.title.datos_usuario.subArea", "errorValueClass");
                    alert("Debe ingresar el sub �rea.");
                    return false;
                }
                </c:if>
                if (document.getElementById("USUARIO.cargo").value=="") {
                    changeStyleClass("co.label.cargo", "errorValueClass");
                    alert("Debe ingresar el Cargo.");
                    return false;
                } else{
                	if (!js_ContieneCaracter(document.getElementById("USUARIO.cargo"), "co.label.cargo", "El Cargo debe contener por lo menos una letra.")){
                		return false;
                	}
                }
                <c:if test="${mostrarDatosEmpresa}">
                if (document.getElementById("departamentoE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa.departamento", "errorValueClass");
                    alert("Debe ingresar el departamento de la empresa.");
                    return false;
                }
                if (document.getElementById("provinciaE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa.provincia", "errorValueClass");
                    alert("Debe ingresar la provincia de la empresa.");
                    return false;
                }
                if (document.getElementById("EMPRESA.distrito").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa.distrito", "errorValueClass");
                    alert("Debe ingresar el distrito de la empresa.");
                    return false;
                }
                </c:if>
                <c:if test="${mostrarDatosEmpresaExterna}">
                if (document.getElementById("departamentoEE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa_externa.departamento", "errorValueClass");
                    alert("Debe ingresar el departamento de la empresa.");
                    return false;
                }
                if (document.getElementById("provinciaEE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa_externa.provincia", "errorValueClass");
                    alert("Debe ingresar la provincia de la empresa.");
                    return false;
                }
                if (document.getElementById("EMPRESA_EXTERNA.distrito").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa_externa.distrito", "errorValueClass");
                    alert("Debe ingresar el distrito de la empresa.");
                    return false;
                }
                </c:if>
                <c:if test="${mostrarDatosEmpresaExterna}">
                if (document.getElementById("EMPRESA_EXTERNA.numdoc").value=="") {
                    changeStyleClass("co.title.datos_usuario.ruc", "errorValueClass");
                    alert("Debe ingresar el RUC de la empresa.");
                    return false;
                }
                </c:if>

            }
            return true;
        }

        function cargarProvincias(){
           var f = formulario.value;
           var dept = $('#departamento').val();
           var filter ="departamento="+dept;
           loadSelectAJX("ajax.selectLoader", "loadList", "provincia", "comun.provincia.select", filter, null, null,null);
        }
        function cargarDistritos(){
           var f = formulario.value;
           var dept = $('#departamento').val();
           var prov = $('#provincia').val();
           var filter ="departamento="+dept+"|provincia="+prov;
           loadSelectAJX("ajax.selectLoader", "loadList", "USUARIO.distrito", "comun.distrito.select", filter, null, null,null);
        }
        function cargarProvinciasEmpresa(){
           var f = formulario.value;
           var dept = $('#departamentoE').val();
           var filter ="departamento="+dept;
           loadSelectAJX("ajax.selectLoader", "loadList", "provinciaE", "comun.provincia.select", filter, null, null,null);
        }
        function cargarDistritosEmpresa(){
           var f = formulario.value;
           var dept = $('#departamentoE').val();
           var prov = $('#provinciaE').val();
           var filter ="departamento="+dept+"|provincia="+prov;
           loadSelectAJX("ajax.selectLoader", "loadList", "EMPRESA.distrito", "comun.distrito.select", filter, null, null,null);
        }
        function cargarProvinciasEmpresaExterna(){
            var f = formulario.value;
            var dept = $('#departamentoEE').val();
            var filter ="departamento="+dept;
            loadSelectAJX("ajax.selectLoader", "loadList", "provinciaEE", "comun.provincia.select", filter, null, null,null);
        }
        function cargarDistritosEmpresaExterna(){
            var f = formulario.value;
            var dept = $('#departamentoEE').val();
            var prov = $('#provinciaEE').val();
            var filter ="departamento="+dept+"|provincia="+prov;
            loadSelectAJX("ajax.selectLoader", "loadList", "EMPRESA_EXTERNA.distrito", "comun.distrito.select", filter, null, null,null);
        }
        function actualizarUsuario(){
            var f = document.formulario;
            f.action = contextName+"/usuario.htm?method=actualizarUsuarioEmpresa";
            f.button.value = "actualizarUsuarioButton";
        }

        function actualizarRepresentantesLegales() {
            var f = document.formulario;
            f.action = contextName+"/usuario.htm?method=actualizarRepresentantesLegales";
            f.button.value = "actualizarUsuarioButton";
        }

    </script>
    <body >
    <div id="body">
    <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
    <div id="contp"><div id="cont">
        <jlis:modalWindow/>
        <table class="tablaMain" cellpadding="0" cellspacing="0" id="tablaMain">
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>
                    <form name="formulario" id="registrarUsuarioId" method="post" onSubmit="return validarSubmit();">
                        <input type="hidden" name="method" />
                        <input type="hidden" name="button">
                        <input type="hidden" name="USUARIO.esEmpresa" value='N'/>
                        <input type="hidden" name="USUARIO.usuarioTipoId" value="${sessionScope.USUARIO.tipoUsuario}" />
                        <jlis:value type="hidden" name="USUARIO.idUsuario" />
                        <jlis:value type="hidden" name="USUARIO.idEntidad" />
                        <jlis:value type="hidden" name="USUARIO.principal" />
                        <jlis:value type="hidden" name="USUARIO.tipoPersona" />
                        <jlis:value type="hidden" name="USUARIO.nroRegistro" value="${sessionScope.USUARIO.nroRegistro}" />
                        <jlis:value type="hidden" name="nombreDatosEmpresa" />
                        
                        <jlis:value name="nombreFormato" editable="no" valueClass="title" value="Modificaci�n de Datos de ${sessionScope.USUARIO.nombreCompleto}" />
                        <br/>
                        
                        <c:if test="${sessionScope.USUARIO.principal}" >
                          <div><a href="/adminroles/" target="_blank;">Administraci�n de roles</a></div>
                        </c:if> 
                        
                        <jlis:messageArea width="100%" />
                        <br/>

            <div class="block btabs btable"><div class="blocka"><div class="blockb">
                <ul id="maintab" class="tabs">
                    <li class="selected"><a href="#" rel="tabGeneral"><span>Datos Generales</span></a></li>
                    <c:if test="${mostrarRepresentantesLegales}">
                        <li><a href="#" rel="tabRepLegales"><span>Representantes Legales</span></a></li>
                    </c:if>
                </ul>
                <div id="tabGeneral" class="tabcontent">
                        <table class="form" >
                            <tr>
                                <td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_usuario.titulo" /></b></span></h3></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.nombre" /></th>
                                <td><jlis:value name="USUARIO.nombre" size="80" align="left" editable="${ubigeoUsuarioEditable}" type="text"/><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
                            <tr>
                            </c:if>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
                            <tr style="display: none;">
                            </c:if>
                                <th><jlis:label key="co.title.datos_usuario.tipoDoc" /></th>
                                <td><jlis:selectProperty name="USUARIO.tipodocumento" editable="no" key="comun.tipo.documento"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.nrodoc" /></th>
                                <td><jlis:value name="USUARIO.numdoc" size="15" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.tipoDocUS" /></th>
                                <td><jlis:selectProperty name="USUARIO.tipodocumentoUS" key="comun.tipo.documento"/><span class="requiredValueClass">(*)</span></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.nrodocUS" /></th>
                                <td><jlis:value name="USUARIO.numdocUS" size="15" type="text" align="left" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'IT'}">
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.usuariosol" /></th>
                                <td><jlis:value name="USUARIO.usuariosol"  size="20" align="left" editable="no" type="text"/></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            </c:if>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.email" /></th>
                                <td colspan="4"><jlis:value name="USUARIO.email" size="35" editable="yes" type="text" align="left" onChange="validarEmail(this,false)" />
                                <jlis:value type="checkbox" checkValue="S" name="USUARIO.recibeNotificacion" />
                                <jlis:label key="co.title.datos_usuario.recibe_notificacion" />
                                </td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.telefono" /></th>
                                <td><jlis:value name="USUARIO.telefono" size="10" align="left" editable="yes" type="text"/></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.celular" /></th>
                                <td><jlis:value name="USUARIO.celular" size="10" align="left" editable="yes" type="text"/></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.fax" /></th>
                                <td><jlis:value name="USUARIO.fax" size="10" editable="yes"  type="text" align="left" /></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.departamento" /></th>
                                <td><jlis:selectProperty name="departamento" key="comun.departamento.select" onChange="cargarProvincias()" editable="${ubigeoUsuarioEditable}" /><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.provincia" /></th>
                                <td><jlis:selectProperty name="provincia" key="comun.provincia.select" filter="${filterProvincia}" editable="${ubigeoUsuarioEditable}" onChange="cargarDistritos()" /><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.distrito" /></th>
                                <td><jlis:selectProperty name="USUARIO.distrito" key="comun.distrito.select" filter="${filterDistrito}" editable="${ubigeoUsuarioEditable}" /><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                            	<th><jlis:label key="co.title.datos_usuario.direccion" /></th>
                                <td><jlis:value name="USUARIO.direccion" size="80" align="left" editable="${ubigeoUsuarioEditable}" type="text"/><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                            	<th><jlis:label key="co.title.datos_usuario.referencia" /></th>
                                <td><jlis:value name="USUARIO.referencia" size="80" align="left"  editable="yes" type="text"/></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.area" /></th>
                                <td><jlis:value name="USUARIO.area" size="80" align="left"  editable="yes" type="text"/><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.subArea" /></th>
                                <td><jlis:value name="USUARIO.subArea" size="80" align="left"  editable="yes" type="text"/><span class="requiredValueClass">(*)</span></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            </c:if>
                            <tr>
                            	<th><jlis:label key="co.label.cargo" /></th>
                                <td><jlis:value name="USUARIO.cargo" size="80" align="left" editable="yes" type="text"/><span class="requiredValueClass">(*)</span></td>
                            </tr>
                        </table>
                        <c:if test="${mostrarDatosEmpresa}">
                        <input type="hidden" name="EMPRESA.esEmpresa" value='S'/>
                        <jlis:value type="hidden" name="EMPRESA.idUsuario" />
                        <table class="form" >
                            <tr>
                                <td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_usuario_empresa.titulo" /></b></span></h3></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.razonsocial" /></th>
                                <td><jlis:value name="EMPRESA.nombre" size="80" align="left" type="text" editable="no" /></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.ruc" /></th>
                                <td><jlis:value name="EMPRESA.numdoc" size="15" type="text" align="left" editable="no" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.direccion" /></th>
                                <td><jlis:value name="EMPRESA.direccion" size="80" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.email" /></th>
                                <td><jlis:value name="EMPRESA.email" size="35" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.telefono" /></th>
                                <td><jlis:value name="EMPRESA.telefono" size="10" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.fax" /></th>
                                <td><jlis:value name="EMPRESA.fax" size="10" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa.departamento" /></th>
                                <td><jlis:selectProperty name="departamentoE" key="comun.departamento.select" editable="no" onChange="cargarProvinciasEmpresa()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa.provincia" /></th>
                                <td><jlis:selectProperty name="provinciaE" key="comun.provincia.select" filter="${filterProvinciaE}" editable="no" onChange="cargarDistritosEmpresa()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa.distrito" /></th>
                                <td><jlis:selectProperty name="EMPRESA.distrito" key="comun.distrito.select" filter="${filterDistritoE}" editable="no" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                        </table>
                        </c:if>
                        <c:if test="${mostrarDatosEmpresaExterna}">
                        <input type="hidden" name="EMPRESA_EXTERNA.esEmpresa" value='S'/>
                        <jlis:value type="hidden" name="EMPRESA_EXTERNA.idUsuario" />
                        <table class="form" >
                            <tr>
                                <td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="${tituloDatosGeneralesEmpresaExterna}" /></b></span></h3></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.razonsocial" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.nombre" size="80" align="left" type="text" editable="no"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.ruc" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.numdoc" size="15" type="text" align="left" editable="no" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.direccion" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.direccion" size="80" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.email" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.email" size="35" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.telefono" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.telefono" size="10" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.fax" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.fax" size="10" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa_externa.departamento" /></th>
                                <td><jlis:selectProperty name="departamentoEE" key="comun.departamento.select" editable="no" onChange="cargarProvinciasEmpresaExterna()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa_externa.provincia" /></th>
                                <td><jlis:selectProperty name="provinciaEE" key="comun.provincia.select" editable="no" filter="${filterProvinciaEE}" onChange="cargarDistritosEmpresaExterna()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa_externa.distrito" /></th>
                                <td><jlis:selectProperty name="EMPRESA_EXTERNA.distrito" key="comun.distrito.select" editable="no" filter="${filterDistritoEE}" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                        </table>
                        </c:if>

                </div>
                <c:if test="${mostrarRepresentantesLegales}">
                <div id="tabRepLegales" class="tabcontent">
                        <table class="form">
                            <tr>
                                <td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_usuario.representantes_legales.titulo" /></b></span></h3></td>
                            </tr>
                        </table>
                        <table class="form">
                            <tr>
                                <td>
                                   <jlis:button id="actualizarRepresentantesLegalesButton" name="actualizarRepresentantesLegales()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar Representantes Legales desde SUNAT" alt="Pulse aqu� para actualizar los Representantes Legales de la Empresa desde la informaci�n registrada en SUNAT" />
                                </td>
                            </tr>
                        </table>
                        <jlis:table keyValueColumns="DOCUMENTO_TIPO,NUMERO_DOCUMENTO" name="REPRESENTANTE_LEGAL"
                            source="tablaRepresentantesLegales" scope="request" pageSize="*" cellPadding="2" navigationHeader="no">
                            <jlis:tr>
                                <jlis:td name="ACTIVOS" width="1%" style="valueCheckbox=SELECCIONE,on-off,S,N"  />
                                <jlis:td name="DOCUMENTO_TIPO" />
                                <jlis:td name="TIPO DOC." nowrap="yes" width="10%"/>
                                <jlis:td name="NRO. DOC." nowrap="yes" width="10%"/>
                                <jlis:td name="NOMBRE" nowrap="yes" width="79%"/>
                            </jlis:tr>
                            <jlis:columnStyle column="1" columnName="SELECCIONE" align="center" hide="yes" value="S" />
                            <jlis:columnStyle column="2" columnName="DOCUMENTO_TIPO" hide="yes" />
                            <jlis:columnStyle column="3" columnName="DESCRIPCION" align="center" />
                            <jlis:columnStyle column="4" columnName="NUMERO_DOCUMENTO" align="center" />
                            <jlis:columnStyle column="5" columnName="NOMBRE" />
                            <%--jlis:tableButton column="1" type="valueCheckbox" name="SELECCIONE" uncheckedValue="N" /--%>
                        </jlis:table>
                </div>
                </c:if>
        </div></div></div>

                        <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' || (sessionScope.USUARIO.roles['VUCE.SUNAT.ESPECIALISTA'] != 'VUCE.SUNAT.ESPECIALISTA')}">
                        <table>
                            <tr>
                                <td>
                                   <jlis:button id="actualizarUsuarioButton" name="actualizarUsuario()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar Datos" alt="Pulse aqu� para actualizar los datos" />
                                </td>
                            </tr>
                        </table>
                        </c:if>
                    </form>
                </td>
            </tr>

        </table>
        </div></div>
        <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>

</html>