<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
       <title>Sistema COMPONENTE ORIGEN</title>
       <meta http-equiv="Pragma" content="no-cache" />
    </head>

    <script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
<%
    String ubigeoUsuarioEditable = (String)request.getAttribute("ubigeoUsuarioEditable");
    if (ubigeoUsuarioEditable==null) ubigeoUsuarioEditable = "yes";
    String select_departamento = "";
    String select_provincia = "";
    String select_USUARIO_distrito = "";
    if (ubigeoUsuarioEditable.equalsIgnoreCase("no")) {
    	select_departamento = "select_departamento";
    	select_provincia = "select_provincia";
    	select_USUARIO_distrito = "select_USUARIO_distrito";
    } else {
        select_departamento = "departamento";
        select_provincia = "provincia";
        select_USUARIO_distrito = "USUARIO.distrito";
    }
%>
    <script language="JavaScript">

        $(document).ready(function() {
            initializetabcontent("maintab");
            $('input[type="text"],select,textarea').focus(function() {
                $(this).removeClass("inputTextClass").addClass("focusField");
            });
            $('input[type="text"],select,textarea').blur(function() {
                if(this.value == this.defaultValue){
                    $(this).removeClass("focusField").addClass("inputTextClass");
                }
            });
<%          if (ubigeoUsuarioEditable.equalsIgnoreCase("no")) {%>
            var departamento = document.getElementById("id_departamento").value;
            document.getElementById("departamento").value = departamento;
            document.getElementById("<%=select_departamento%>").value = departamento;
            cargarProvincias();
<%          }%>

            if (document.getElementById("id_departamentoE")) {
                var departamentoE = document.getElementById("id_departamentoE").value;
                document.getElementById("departamentoE").value = departamentoE;
                document.getElementById("select_departamentoE").value = departamentoE;
                cargarProvinciasEmpresa();
            }
        });

        function validarSubmit() {
            var f = document.formulario;
            var button = f.button;
            if (button.value=="cancelarButton") {
            }
            if (button.value=="continuarButton") {
            	if (document.getElementById("USUARIO.recibeNotificacion").checked){
                	if (document.getElementById("USUARIO.email").value=="") {
	                    changeStyleClass("co.title.datos_usuario.email", "errorValueClass");
	                    alert("Debe ingresar el email.");
	                    return false;
                	}
                }
                if (document.getElementById("USUARIO.nombre").value=="") {
                    changeStyleClass("co.title.datos_usuario.nombre", "errorValueClass");
                    alert("Debe ingresar los Apellidos y Nombres.");
                    return false;
                } else {
                	if (!js_ContieneCaracter(document.getElementById("USUARIO.nombre"), "co.title.datos_usuario.nombre", "El Nombre debe contener por lo menos una letra.")){
                		return false;
                	}
                }
                if (document.getElementById("USUARIO.tipodocumentoUS").value=="") {
                    changeStyleClass("co.title.datos_usuario.tipoDocUS", "errorValueClass");
                    alert("Debe ingresar el Tipo de Documento de identificaci�n.");
                    return false;
                }
                if (document.getElementById("USUARIO.numdocUS").value=="") {
                    changeStyleClass("co.title.datos_usuario.nrodocUS", "errorValueClass");
                    alert("Debe ingresar el N�mero de Documento de identificaci�n.");
                    return false;
                }
                if (document.getElementById("USUARIO.direccion").value=="") {
                    changeStyleClass("co.title.datos_usuario.direccion", "errorValueClass");
                    alert("Debe ingresar la direccion.");
                    return false;
                } else {
                	if (!js_ContieneCaracter(document.getElementById("USUARIO.direccion"), "co.title.datos_usuario.direccion", "La direcci�n debe contener por lo menos una letra.")){
                		return false;
                	}
                }
                if (document.getElementById("departamento").value=="") {
                    changeStyleClass("co.title.datos_usuario.departamento", "errorValueClass");
                    alert("Debe ingresar el departamento.");
                    return false;
                }
                if (document.getElementById("provincia").value=="") {
                    changeStyleClass("co.title.datos_usuario.provincia", "errorValueClass");
                    alert("Debe ingresar la provincia.");
                    return false;
                }
                if (document.getElementById("USUARIO.distrito").value=="") {
                    changeStyleClass("co.title.datos_usuario.distrito", "errorValueClass");
                    alert("Debe ingresar el distrito.");
                    return false;
                }
                <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
                if (document.getElementById("USUARIO.area").value=="") {
                    changeStyleClass("co.title.datos_usuario.area", "errorValueClass");
                    alert("Debe ingresar el �rea.");
                    return false;
                }
                if (document.getElementById("USUARIO.subArea").value=="") {
                    changeStyleClass("co.title.datos_usuario.subArea", "errorValueClass");
                    alert("Debe ingresar el sub �rea.");
                    return false;
                }
                </c:if>
                if (document.getElementById("USUARIO.cargo").value=="") {
                    changeStyleClass("co.label.cargo", "errorValueClass");
                    alert("Debe ingresar el Cargo.");
                    return false;
                } else {
                	if (!js_ContieneCaracter(document.getElementById("USUARIO.cargo"), "co.label.cargo", "El Cargo debe contener por lo menos una letra.")){
                		return false;
                	}
                }
                <c:if test="${mostrarDatosEmpresa}">
                if (document.getElementById("departamentoE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa.departamento", "errorValueClass");
                    alert("Debe ingresar el departamento de la empresa.");
                    return false;
                }
                if (document.getElementById("provinciaE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa.provincia", "errorValueClass");
                    alert("Debe ingresar la provincia de la empresa.");
                    return false;
                }
                if (document.getElementById("EMPRESA.distrito").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa.distrito", "errorValueClass");
                    alert("Debe ingresar el distrito de la empresa.");
                    return false;
                }
                </c:if>
                <c:if test="${mostrarDatosEmpresaExterna}">
                if (document.getElementById("departamentoEE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa_externa.departamento", "errorValueClass");
                    alert("Debe ingresar el departamento de la empresa.");
                    return false;
                }
                if (document.getElementById("provinciaEE").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa_externa.provincia", "errorValueClass");
                    alert("Debe ingresar la provincia de la empresa.");
                    return false;
                }
                if (document.getElementById("EMPRESA_EXTERNA.distrito").value=="") {
                    changeStyleClass("co.title.datos_usuario_empresa_externa.distrito", "errorValueClass");
                    alert("Debe ingresar el distrito de la empresa.");
                    return false;
                }
                </c:if>
                <c:if test="${mostrarDatosEmpresaExterna}">
                if (document.getElementById("EMPRESA_EXTERNA.numdoc").value=="") {
                    changeStyleClass("co.title.datos_usuario.ruc", "errorValueClass");
                    alert("Debe ingresar el RUC de la empresa.");
                    return false;
                }
                </c:if>

            }
            return true;
        }

        function afterCargarProvincias() {
            var provincia = document.getElementById("id_provincia").value;
            if (provincia!="") {
                document.getElementById("provincia").value = provincia;
                document.getElementById("<%=select_provincia%>").value = provincia;

                cargarDistritos();
            }
        }

        function cargarProvincias(){
           var f = formulario.value;
           var dept = $('#departamento').val();
           var filter ="departamento="+dept;
           loadSelectAJX("ajax.selectLoader", "loadList", "<%=select_provincia%>", "comun.provincia.select", filter, null, afterCargarProvincias, null);
        }

        function afterCargarDistritos() {
            var distrito = document.getElementById("id_distrito").value;
            if (distrito!="") {
                document.getElementById("USUARIO.distrito").value = distrito;
                document.getElementById("<%=select_USUARIO_distrito%>").value = distrito;
            }
        }

        function cargarDistritos(){
           var f = formulario.value;
           var dept = $('#departamento').val();
           var prov = $('#provincia').val();
           var filter ="departamento="+dept+"|provincia="+prov;
           loadSelectAJX("ajax.selectLoader", "loadList", "<%=select_USUARIO_distrito%>", "comun.distrito.select", filter, null, afterCargarDistritos, null);
        }

        function afterCargarProvinciasEmpresa() {
            var provinciaE = document.getElementById("id_provinciaE").value;
            document.getElementById("provinciaE").value = provinciaE;
            document.getElementById("select_provinciaE").value = provinciaE;

            cargarDistritosEmpresa();
        }

        function cargarProvinciasEmpresa(){
           var f = formulario.value;
           var dept = $('#departamentoE').val();
           var filter ="departamento="+dept;

           loadSelectAJX("ajax.selectLoader", "loadList", "select_provinciaE", "comun.provincia.select", filter, null, afterCargarProvinciasEmpresa, null);
        }

        function afterCargarDistritosEmpresa() {
            var distritoE = document.getElementById("id_distritoE").value;
            document.getElementById("EMPRESA.distrito").value = distritoE;
            document.getElementById("select_EMPRESA_distrito").value = distritoE;
        }

        function cargarDistritosEmpresa(){
           var f = formulario.value;
           var dept = $('#departamentoE').val();
           var prov = $('#provinciaE').val();
           var filter ="departamento="+dept+"|provincia="+prov;
           loadSelectAJX("ajax.selectLoader", "loadList", "select_EMPRESA_distrito", "comun.distrito.select", filter, null, afterCargarDistritosEmpresa, null);
        }

        function afterCargarProvinciasEmpresaExterna() {
        	var provinciaEE = document.getElementById("id_provinciaEE").value;
        	document.getElementById("provinciaEE").value = provinciaEE;
            document.getElementById("select_provinciaEE").value = provinciaEE;

            cargarDistritosEmpresaExterna();
        }

        function cargarProvinciasEmpresaExterna(){
            var f = formulario.value;
            var dept = $('#departamentoEE').val();
            if (dept!="") {
                var filter ="departamento="+dept;
                loadSelectAJX("ajax.selectLoader", "loadList", "select_provinciaEE", "comun.provincia.select", filter, null, afterCargarProvinciasEmpresaExterna, null);
            } else {
            	document.getElementById("provinciaEE").value = "";
                document.getElementById("select_provinciaEE").value = "";
                cargarDistritosEmpresaExterna();
            }
        }

        function afterCargarDistritosEmpresaExterna() {
        	var distritoEE = document.getElementById("id_distritoEE").value;
        	document.getElementById("EMPRESA_EXTERNA.distrito").value = distritoEE;
        	document.getElementById("select_EMPRESA_EXTERNA_distrito").value = distritoEE;
        }

        function cargarDistritosEmpresaExterna() {
            var f = formulario.value;
            var dept = $('#departamentoEE').val();
            var prov = $('#provinciaEE').val();
            if (dept!="" && prov!="") {
                var filter ="departamento="+dept+"|provincia="+prov;
                loadSelectAJX("ajax.selectLoader", "loadList", "select_EMPRESA_EXTERNA_distrito", "comun.distrito.select", filter, null, afterCargarDistritosEmpresaExterna, null);
            } else {
            	document.getElementById("EMPRESA_EXTERNA.distrito").value = "";
            	document.getElementById("select_EMPRESA_EXTERNA_distrito").value = "";
            }
        }
        function nuevoRepresentateLegal(){
             showPopWin(contextName+"/usuario.htm?method=mostrarNuevoRepresentanteLegalForm", 710, 150, null);
        }
        function cancelar(){
            //window.top.location = contextName;
            var f = document.formulario;
            f.action = contextName+"/logout.htm";
            f.method.value="logout";
            f.button.value = "cancelarButton";
        }
        function continuar(){
            var f = document.formulario;
            f.action = contextName+"/usuario.htm";
            f.method.value = "terminosCondiciones";
            f.button.value = "continuarButton";
        }

        function seleccionTipoPersona() {
            if (!document.getElementById("tabDatosEmpresa")) return;

            var tipoPersona = document.getElementById("USUARIO.tipoPersona").value;
            if (tipoPersona == "1") {
                document.getElementById("tabDatosEmpresa").style.display = "none";
            } else {
                document.getElementById("tabDatosEmpresa").style.display = "block";
            }
        }

    </script>
    <body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
    <div id="contp"><div id="cont">
        <jlis:modalWindow/>
        <table class="tablaMain" cellpadding="0" cellspacing="0" id="tablaMain">
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>
                    <form name="formulario" id="registrarUsuarioId" method="post" onSubmit="return validarSubmit();">
                    <input type="hidden" name="method" />
                    <input type="hidden" name="button">
                    <input type="hidden" name="USUARIO.esEmpresa" value='N'/>
                    <input type="hidden" name="USUARIO.usuarioTipoId" value="${sessionScope.USUARIO.tipoUsuario}" />
                    <jlis:value type="hidden" name="USUARIO.principal" />
                    <jlis:value type="hidden" name="USUARIO.tipoPersona" />
                    <jlis:value type="hidden" name="USUARIO.nroRegistro" value="${sessionScope.USUARIO.nroRegistro}" />
                    <jlis:value type="hidden" name="nombreDatosEmpresa" />
                    <jlis:value type="hidden" name="id_departamento" />
                    <jlis:value type="hidden" name="id_provincia" />
                    <jlis:value type="hidden" name="id_distrito" />

                    <jlis:value name="nombreFormato" editable="no" valueClass="title" value="Creaci�n de Nuevo Usuario" />
                    <br/>
                    <jlis:messageArea width="100%" />
                    <br/>
            <div class="block btabs btable"><div class="blocka"><div class="blockb">
                <ul id="maintab" class="tabs">
                    <li class="selected"><a href="#" rel="tabGeneral"><span>Datos Generales</span></a></li>
                    <c:if test="${mostrarRepresentantesLegales}">
                        <li><a href="#" rel="tabRepLegales"><span>Representantes Legales</span></a></li>
                    </c:if>
                </ul>
                <div id="tabGeneral" class="tabcontent">
                        <table class="form" >
							<tr>
								<td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_usuario.titulo" /></b></span></h3></td>
							</tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.nombre" /></th>
                                <td><jlis:value name="USUARIO.nombre" size="80" align="left"  editable="${ubigeoUsuarioEditable}" type="text"/></td>
                                <td>&nbsp;</td>
                            </tr>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
                            <tr>
                            </c:if>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
                            <tr style="display: none;">
                            </c:if>
                                <th><jlis:label key="co.title.datos_usuario.tipoDoc" /></th>
                                <td><jlis:selectProperty name="USUARIO.tipodocumento" editable="no" key="comun.tipo.documento"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.nrodoc" /></th>
                                <td><jlis:value name="USUARIO.numdoc" size="15" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.tipoDocUS" /></th>
                                <td><jlis:selectProperty name="USUARIO.tipodocumentoUS" key="comun.tipo.documento"/><span class="requiredValueClass">(*)</span></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.nrodocUS" /></th>
                                <td><jlis:value name="USUARIO.numdocUS" size="15" type="text" align="left" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'IT'}">
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.usuariosol" /></th>
                                <td><jlis:value name="USUARIO.usuariosol"  size="20" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                            </tr>
                            </c:if>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.email" /></th>
                                <td colspan="4"><jlis:value name="USUARIO.email" size="35" editable="yes" type="text" align="left" onChange="validarEmail(this,false)" />
                                    <jlis:value type="checkbox" defaultCheck="yes" checkValue="S" name="USUARIO.recibeNotificacion" />
                                    <jlis:label key="co.title.datos_usuario.recibe_notificacion" />
                                </td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.telefono" /></th>
                                <td><jlis:value name="USUARIO.telefono" size="10" align="left" editable="yes" type="text"/></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.celular" /></th>
                                <td><jlis:value name="USUARIO.celular" size="10" align="left" editable="yes" type="text"/></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.fax" /></th>
                                <td><jlis:value name="USUARIO.fax" size="10" editable="yes"  type="text" align="left" /></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.departamento" /></th>
                                <td><jlis:selectProperty name="departamento" key="comun.departamento.select" editable="${ubigeoUsuarioEditable}" onChange="cargarProvincias()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.provincia" /></th>
                                <td><jlis:selectProperty name="provincia" key="select.empty" editable="${ubigeoUsuarioEditable}" onChange="cargarDistritos()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.distrito" /></th>
                                <td><jlis:selectProperty name="USUARIO.distrito" key="select.empty" editable="${ubigeoUsuarioEditable}" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                            	<th><jlis:label key="co.title.datos_usuario.direccion" /></th>
                                <td><jlis:value name="USUARIO.direccion" size="80" align="left" editable="${ubigeoUsuarioEditable}" type="text"/><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                            	<th><jlis:label key="co.title.datos_usuario.referencia" /></th>
                                <td><jlis:value name="USUARIO.referencia" size="80" align="left"  editable="yes" type="text"/></td>
                            </tr>
                            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.area" /></th>
                                <td><jlis:value name="USUARIO.area" size="80" align="left"  editable="yes" type="text"/><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.subArea" /></th>
                                <td><jlis:value name="USUARIO.subArea" size="80" align="left"  editable="yes" type="text"/><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            </c:if>
                            <tr>
                            	<th><jlis:label key="co.label.cargo" /></th>
                                <td><jlis:value name="USUARIO.cargo" size="80" align="left" editable="yes" type="text"/><span class="requiredValueClass">(*)</span></td>
                            </tr>
                        </table>
                <c:if test="${mostrarDatosEmpresa}">
                <input type="hidden" name="EMPRESA.esEmpresa" value='S'/>
                <jlis:value type="hidden" name="id_departamentoE" />
                <jlis:value type="hidden" name="id_provinciaE" />
                <jlis:value type="hidden" name="id_distritoE" />
                        <table class="form" >
							<tr>
								<td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_usuario_empresa.titulo" /></b></span></h3></td>
							</tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.razonsocial" /></th>
                                <td><jlis:value name="EMPRESA.nombre" size="80" align="left" type="text" editable="no" /></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.ruc" /></th>
                                <td><jlis:value name="EMPRESA.numdoc" size="15" type="text" align="left" editable="no" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.direccion" /></th>
                                <td><jlis:value name="EMPRESA.direccion" size="80" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.email" /></th>
                                <td><jlis:value name="EMPRESA.email" size="35" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.telefono" /></th>
                                <td><jlis:value name="EMPRESA.telefono" size="10" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.fax" /></th>
                                <td><jlis:value name="EMPRESA.fax" size="10" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa.departamento" /></th>
                                <td><jlis:selectProperty name="departamentoE" key="comun.departamento.select" editable="no" onChange="cargarProvinciasEmpresa()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa.provincia" /></th>
                                <td><jlis:selectProperty name="provinciaE" key="select.empty" editable="no" onChange="cargarDistritosEmpresa()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa.distrito" /></th>
                                <td><jlis:selectProperty name="EMPRESA.distrito" key="select.empty" editable="no" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                        </table>
                </c:if>
                <c:if test="${mostrarDatosEmpresaExterna}">
                <input type="hidden" name="EMPRESA_EXTERNA.esEmpresa" value='S'/>
                <jlis:value type="hidden" name="rucEE" />
                <jlis:value type="hidden" name="id_departamentoEE" />
                <jlis:value type="hidden" name="id_provinciaEE" />
                <jlis:value type="hidden" name="id_distritoEE" />
                        <table class="form" >
                            <tr>
                                <td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="${tituloDatosGeneralesEmpresaExterna}" /></b></span></h3></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.razonsocial" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.nombre" size="80" align="left" type="text" editable="no"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.ruc" /></th>
                                <td nowrap>
                                    <jlis:value name="EMPRESA_EXTERNA.numdoc" size="15" type="text" align="left" onChange="cargarDatosRUCEmpresaExterna(this.value)" /><div id="idMensajeRUC_EmpresaExterna" style="color:red;font-size:10px;">&nbsp;</div>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.direccion" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.direccion" size="80" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.email" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.email" size="35" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario.telefono" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.telefono" size="10" align="left" editable="no" type="text"/></td>
                                <td>&nbsp;</td>
                                <th><jlis:label key="co.title.datos_usuario.fax" /></th>
                                <td><jlis:value name="EMPRESA_EXTERNA.fax" size="10" editable="no" type="text" align="left" /></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa_externa.departamento" /></th>
                                <td><jlis:selectProperty name="departamentoEE" key="comun.departamento.select" editable="no" onChange="cargarProvinciasEmpresaExterna()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa_externa.provincia" /></th>
                                <td><jlis:selectProperty name="provinciaEE" key="select.empty" editable="no" onChange="cargarDistritosEmpresaExterna()" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                            <tr>
                                <th><jlis:label key="co.title.datos_usuario_empresa_externa.distrito" /></th>
                                <td><jlis:selectProperty name="EMPRESA_EXTERNA.distrito" key="select.empty" editable="no" /><span class="requiredValueClass">(*)</span></td>
                            </tr>
                        </table>
                </c:if>
                </div>
                <c:if test="${mostrarRepresentantesLegales}">
                <div id="tabRepLegales" class="tabcontent">
				        <table class="form">
							<tr>
								<td colspan="5"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_usuario.representantes_legales.titulo" /></b></span></h3></td>
							</tr>
						</table>
	                    <jlis:table keyValueColumns="DOCUMENTO_TIPO,NUMERO_DOCUMENTO" name="REPRESENTANTE_LEGAL"
	                        source="tablaRepresentantesLegales" scope="request" pageSize="*" cellPadding="2" navigationHeader="no">
				            <jlis:tr>
			                    <jlis:td name="ACTIVOS" width="1%" style="valueCheckbox=SELECCIONE,on-off,S,N"  />
				            	<jlis:td name="DOCUMENTO_TIPO" />
				            	<jlis:td name="TIPO DOC." nowrap="yes" width="10%"/>
				            	<jlis:td name="NRO. DOC." nowrap="yes" width="10%"/>
				            	<jlis:td name="NOMBRE" nowrap="yes" width="79%"/>
			                </jlis:tr>
				            <jlis:columnStyle column="1" columnName="SELECCIONE" align="center" hide="yes" value="S" />
				            <jlis:columnStyle column="2" columnName="DOCUMENTO_TIPO" hide="yes" />
				            <jlis:columnStyle column="3" columnName="DESCRIPCION" align="center" />
				            <jlis:columnStyle column="4" columnName="NUMERO_DOCUMENTO" align="center" />
				            <jlis:columnStyle column="5" columnName="NOMBRE" />
				            <%--jlis:tableButton column="1" type="valueCheckbox" name="SELECCIONE" uncheckedValue="N" /--%>
			        	</jlis:table>
                </div>
                </c:if>
        </div></div></div>

                <table>
                    <tr>
                        <td>
                           <jlis:button id="continuarButton" name="continuar()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Continuar >> " alt="Pulse aqu� para continuar el registro" />
                           <jlis:button id="cancelarButton" name="cancelar()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cancelar " alt="Pulse aqu� para cancelar el ingreso" />
                        </td>
                    </tr>
                </table>
                    </form>
                </td>
            </tr>

        </table>
        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>

</html>