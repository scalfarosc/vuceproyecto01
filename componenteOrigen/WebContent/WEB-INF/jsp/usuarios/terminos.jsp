<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
    request.setAttribute("noMostrarHeader", "S");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>T�rminos y Condiciones</title>
</head>
<script language="JavaScript">

        function validarSubmit() {
            var f = document.formulario;
            if (f.button.value=="buttonCancelar") {
            }
            if (f.button.value=="buttonContinuar") {
                var acepto = $('#chkDeclaro').is(":checked");
                if (!acepto) {
                    alert("Debe aceptar las Condiciones del Servicio");
                    return false;
                }
            }
            return true;
        }

		function cancelar() {
        	var f = document.formulario;
        	f.action = contextName+"/logout.htm";
            f.method.value="logout";
            f.button.value = "buttonCancelar";
        }
        
        function continuar() {
           	var f = document.formulario;
            f.action = contextName+"/usuario.htm";
            f.method.value = "grabarUsuarioEmpresa";
            f.button.value = "buttonContinuar";
        }
        
    </script>
<body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
		<form name="formulario" id="continuarId" method="post" onsubmit="return validarSubmit();">
		<input type="hidden" name="method" />
        <input type="hidden" name="button" />
        <jlis:value name="opcion" type="hidden" />
        <table class="tablaMain" cellpadding="0" cellspacing="0" id="tablaMain">
            <tr><td colspan="3">
             <div id="pageTitle">
					    <h1><strong>Creaci�n de Nuevos Usuarios</strong></h1>
			</div>
            
            </td></tr>
            
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td>
			       <table width="100%" border="0" cellspacing="0" cellpadding="6">
					<tr>
						<td  align="right">
							<br>
							<p ><b>CONDICIONES DEL SERVICIO</b></p>
							<p align="justify">
							<textarea cols="115" rows="15" readonly="readonly" class="inputTextClass">
1. Definici�n de La Ventanilla �nica de Comercio Exterior - VUCE y alcances de su componente de Origen:

La VUCE es un sistema integrado que permite a las partes involucradas en el comercio exterior y transporte internacional gestionar a trav�s de medios electr�nicos los tr�mites requeridos por las entidades competentes de acuerdo con la normatividad vigente, o solicitados por dichas partes, para el tr�nsito, ingreso o salida del territorio nacional de mercanc�as.

A trav�s del componente Origen de la VUCE se pondr� a disposici�n de los exportadores y de las Entidades Certificadoras, que act�an como entidades delegadas del MINCETUR-   Autoridad Competente-, un sistema por el cual se podr�n llevar por medios electr�nicos los procedimientos y procesos para tramitar la aprobaci�n de las declaraciones juradas de origen, requisito para expedir certificados de origen de tipo preferencial; solicitar la emisi�n de certificados de origen de tipo preferencial; y solicitar duplicado, reemplazo y anulaci�n de un certificado de origen.

Las reglas operativas que rigen el funcionamiento del Componente de Origen se encuentran en el Reglamento aprobado por Decreto Supremo N� 006-2013-MINCETUR y en los procedimientos que se aprueben para ello. 

2. Autenticaci�n  de usuarios para el del Componente de Origen

La VUCE cuenta con un sistema de autenticaci�n provisto por SUNAT que permite a los administrados y a los funcionarios p�blicos ingresar e identificarse v�lidamente en el sistema de la VUCE, a fin de llevar a cabo los procedimientos administrativos incorporados a la VUCE.

Para la autenticaci�n de los administrados en el sistema, es indispensable que estos cuenten con RUC porque  se requiere del uso de la Clave SOL proporcionada por SUNAT. En caso MINCETUR apruebe otras formas de autenticaci�n diferentes al RUC, ser� establecido por una Resoluci�n Ministerial.

3. Procedimientos administrativos realizados a trav�s de la VUCE

La VUCE proporciona a los administrados (usuarios), a manera de orientaci�n, los formularios electr�nicos necesarios para iniciar los procedimientos incorporados en la VUCE, proveyendo adicionalmente validaciones autom�ticas de dichos formularios para facilitar su correcto llenado. 

Asimismo, en los casos que se requiera del pago de un derecho de tramitaci�n administrativo, se realizar� directamente ante la Autoridad Competente, siendo registrado en el sistema de la VUCE.

Una vez transmitido la solicitud, y completado el formulario electr�nico, denominado Solicitud �nica de Comercio Exterior - SUCE, debidamente llenado y con el pago de derecho de tramitaci�n, en caso corresponda, se dar� inicio al procedimiento administrativo, asign�ndole un n�mero de expediente administrativo, que es a la vez el "n�mero de SUCE". Todas las actuaciones de un procedimiento administrativo iniciado a trav�s de la VUCE se adosan al expediente electr�nico.

Los requisitos documentarios exigidos en los procedimientos administrativos, son transmitidos a trav�s del sistema, sean documentos digitales o digitalizados, respetando las excepciones se�aladas en el Decreto Supremo N� 006-2013-MINCETUR. 

En virtud del principio de equivalencia funcional, los actos administrativos realizados a trav�s de la VUCE poseen la misma validez y eficacia jur�dica que los actos realizados de manera tradicional.

4. Notificaci�n electr�nica

De acuerdo al numeral 4.4, art�culo 4 del Decreto Legislativo N� 1036, las disposiciones y procedimientos que norman la VUCE tienen un car�cter especial. Por lo que, la Ley de Procedimiento Administrativo General-Ley N�27444 se aplica en forma supletoria en todo lo no previsto, as� como en las normas reglamentarias y complementarias.

El Reglamento Operativo del Componente de Origen de la VUCE aprobado por el Decreto Supremo N� 006-2013-MINCETUR  establece que todas las notificaciones electr�nicas generadas por la Entidad competente durante todo el procedimiento, estar�n disponibles para el administrado (usuario) en el Buz�n Electr�nico de la VUCE.

El administrado (usuario) tiene la posibilidad  de autorizar que se le env�en de forma complementaria copias de las notificaciones electr�nicas a su correo personal; sin embargo el medio oficial para recibir las notificaciones electr�nicas de los actos generados por la entidad competente, y que las mismas surjan efectos es a trav�s del "Buz�n Electr�nico" asignado al administrado (usuario) dentro del sistema VUCE. Por lo tanto, el administrado (usuario) mantiene la obligaci�n de revisar el "Buz�n Electr�nico" dentro de la VUCE durante todo el procedimiento.  

La recepci�n de la informaci�n depositada en el "Buz�n electr�nico" se verifica en los registros de la VUCE, el cual permite comprobar fehacientemente su acuse de recibe.

5. Responsabilidades de los administrados usuarios de la VUCE:

a. Los administrados (usuarios) son responsables del uso de la Clave  SOL para su autenticaci�n en la VUCE, as� como por el extrav�o, p�rdida o uso indebido de las mismas, en ese sentido se hacen plenamente responsables por los actos, solicitudes, documentos, anexos u cualquier otra informaci�n. Asimismo, son responsables de mantener actualizados los datos asociados a dichas claves, y de darles de baja o suspenderlas ante SUNAT cuando corresponda. Los administrados (usuarios) no pueden ceder bajo ninguna circunstancia sus Clave SOL.

b. Los administrados (usuarios) son responsables por el uso correcto del sistema VUCE para los fines que ha sido legalmente creado. En ese sentido, cualquier acto indebido, inmoral, ilegal, que afecte o no directa o indirectamente a terceros, habilitar� al Administrador de la VUCE a tomar las medidas correctivas que correspondan.

c. Los administrados (usuarios) tienen la responsabilidad de velar para que los archivos y/o documentos que transmitan por el sistema VUCE no contengan virus inform�ticos.

d. Los administrados (usuarios) son responsables de mantener como requisitos t�cnicos necesarios para poder acceder correctamente a la VUCE, los siguientes:

	i. Comprobar el correcto funcionamiento de la configuraci�n de su computador,
	ii. Asegurar la disponibilidad y calidad de sus conexiones a Internet, y
	iii. Revisar sus configuraciones internas para que estas sean incompatibles con los requerimientos de la VUCE.

e. La VUCE no se responsabiliza por problemas surgidos por incumplimiento de las condiciones se�aladas en este punto o ajenos a su control t�cnico.

6. Pol�tica de privacidad:	

El administrador de la VUCE y  las autoridades competentes garantizan la confidencialidad de los mensajes  y consultas enviadas a trav�s de la VUCE, de acuerdo a la normativa vigente. La VUCE permite que �nicamente los destinatarios de los mensajes electr�nicos y consultas puedan tener acceso a los mismos. 

En base a lo expuesto, manifiesto estar de acuerdo con las Condiciones del Servicio y Pol�tica de privacidad; as� mismo, yo administrado (usuario) autorizo a  la entidad competente que me notifique toda comunicaci�n vinculada al procedimiento administrativo al Buz�n electr�nico de la VUCE, y env�e una copia de las notificaciones a mi correo electr�nico personal en caso lo hubiera consignado como medio complementario.

                            </textarea>
						</td>
					</tr>
				</table> 
				<INPUT type="checkbox" id=chkDeclaro name=chkDeclaro">
					Acepto los t�rminos y condiciones &nbsp;&nbsp;
				<jlis:button id="buttonContinuar" name="continuar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Ingresar al Sistema" alt="Pulse aqu� para proceder a ingresar al sistema" />
				<jlis:button id="buttonCancelar" name="cancelar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cancelar" alt="Cancelar" />
                </td>
            </tr>
        </table>
        </form>
            </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>