<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="vuce"%>


        <%-- <co:validacionBotonesFormato pagina="modificacionesSuce" /> --%>
        <vuce:validacionBotonesFormato formato="MCT001" pagina="modificacionesSuce" />

        <table class="form">
        		<tr>
        		<%-- <c:if test="${(!empty notifPendientes) && (notifPendientes > 0)}"> --%>
	                <td><jlis:button code="CO.USUARIO.OPERACION" id="nuevaSubsanacionButton" name="nuevaSubsanacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Responder Notificación" alt="Pulse aquí para responder una Notificación de Subsanación de SUCE" /></td>
	            <%-- </c:if> --%>
				</tr>
		</table>
		<jlis:table name="MODIFICACION_SUCE" pageSize="*" keyValueColumns="MODIFICACION_SUCE,MENSAJE_ID,ESTADO_REGISTRO" source="tSubsanaciones" scope="request" navigationHeader="no" width="100%">
			<jlis:tr>
				<jlis:td name="SUCE ID" nowrap="yes" />
				<jlis:td name="MODIFICACION SUCE" nowrap="yes" />
				<jlis:td name="FECHA REGISTRO" nowrap="yes" width="5%"/>
				<jlis:td name="MENSAJE ID" nowrap="yes"/>
				<jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
                <jlis:td name="TIPO" nowrap="yes" width="8%"/>
				<jlis:td name="ESTADO" nowrap="yes" width="8%"/>
                <jlis:td name="FECHA RESPUESTA" nowrap="yes" width="5%"/>
                <jlis:td name="ESTADO_REGISTRO" nowrap="yes"/>
			</jlis:tr>
			<jlis:columnStyle column="1" columnName="SUCE_ID" hide="yes" />
			<jlis:columnStyle column="2" columnName="MODIFICACION_SUCE" hide="yes" />
			<jlis:columnStyle column="3" columnName="FECHA_REGISTRO" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
			<jlis:columnStyle column="4" columnName="MENSAJE_ID" hide="yes" />
			<jlis:columnStyle column="5" columnName="MENSAJE" editable="yes" />
			<jlis:columnStyle column="6" columnName="TIPO" align="center"/>
            <jlis:columnStyle column="7" columnName="ESTADO" align="center"/>
            <jlis:columnStyle column="8" columnName="FECHA_RESPUESTA" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
            <jlis:columnStyle column="9" columnName="ESTADO_REGISTRO" hide="yes"/>
            <jlis:tableButton column="5" type="link" onClick="verModificacion" />
		</jlis:table>
