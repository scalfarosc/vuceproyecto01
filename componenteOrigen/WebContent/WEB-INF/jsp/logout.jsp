<%@page import="org.jlis.core.config.ApplicationConfig"%>
<%@page import="pe.gob.mincetur.vuce.authenticationmanager.client.constant.AuthenticationManagerProviderConstants"%>
<%@page import="org.springframework.web.util.WebUtils"%>
<%@page import="pe.gob.mincetur.vuce.authenticationmanager.client.service.SessionService"%>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%
Cookie idpCookie = WebUtils.getCookie(request, "idp");

SessionService sessionService = new SessionService();
String url = ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerLoginOptionsUrl") + "/" + ApplicationConfig.getApplicationConfig().getProperty("authenticationManagerClientId");

if(idpCookie != null) {
	String providerKey = idpCookie.getValue();
	if (AuthenticationManagerProviderConstants.EXTRANET_PROVIDER_ID.equals(providerKey)) {
		url = ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLEXT");
	} else if (AuthenticationManagerProviderConstants.OAUTH2SOL_PROVIDER_ID.equals(providerKey)) {
		url = ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLSOL");
	}
} else {
	sessionService.cleanSession(request, response);
}

response.sendRedirect(url);
%>

