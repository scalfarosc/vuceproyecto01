<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@page import="org.jlis.core.util.*"%>
<%@page import="pe.gob.mincetur.vuce.co.bean.UsuarioCO"%>
<%
    String contextName = request.getContextPath();
    UsuarioCO usuarioCO = (UsuarioCO)session.getAttribute(Constantes.USUARIO);
    if (usuarioCO!=null) {
        request.setAttribute("rolActivo", usuarioCO.getRolActivo());
    }
%>

<style>

	.img1{
	  cursor: hand;
	  vertical-align:middle;
	  padding-bottom: 3px;
	}
	
</style>

<script language="JavaScript">
	function principal() {
	    var f = document.formulario;
	    f.action = contextName+"/co.htm";
	    f.method.value="principal";
        f.target = "_self";
	    f.submit();
	}

    function salir() {
        //document.location.href = "<%=contextName%>/${sessionScope.logoutController}";
        var f = document.formulario;
        f.action = contextName+"/logout.htm";
        f.method.value="logout";
        f.target = "_self";
        f.submit();
    }

    function listarBuzon() {
        var f = document.formulario;
        f.action = contextName + "/buzon.htm";
        f.method.value = "listarMensajes";
        f.target = "_self";
        f.submit();
    }

    function listarDDJJ() {
        var f = document.formulario;
        f.action = contextName + "/dj.htm";
        f.method.value = "listarDDJJ";
        f.target = "_self";
        f.submit();
    }

    function listarCalificacion() {
        var f = document.formulario;
        f.action = contextName + "/sc.htm";
        f.method.value = "listarSC";
        f.target = "_self";
        f.submit();
    }

    function listarCalificacionEntidad() {
        var f = document.formulario;
        f.action = contextName + "/adment.htm";
        f.method.value = "cargarListaCalificaciones";
        f.target = "_self";
        f.submit();
    }

    function listarCertificado() {
        var f = document.formulario;
        f.action = contextName + "/cer.htm";
        f.method.value = "listarCertificado";
        f.target = "_self";
        f.submit();
    }

    function listarCertificadoEntidad() {
        var f = document.formulario;
        f.action = contextName + "/adment.htm";
        f.method.value = "cargarListaCertificados";
        f.target = "_self";
        f.submit();
    }

    function refreshPage() {
        window.location.href = "<%=contextName%>/co.htm?method=principal";
    }

    function modificarDatosUsuario() {
        var f = document.formulario;
        f.action = contextName+"/usuario.htm?method=mostrarFormularioActualizarDatosUsuario";
        f.target = "_self";
        f.submit();
    }

    function servicios() {
        var f = document.formulario;
        f.action = contextName + "/servicios.htm";
        f.method.value = "servicios";
        f.target = "_self";
        f.submit();
    }

    function listarSolicitudesCalificacionExportador() {
    	var f = document.formulario;
        f.action = contextName + "/sce.htm";
        f.method.value = "listarSCE";
        f.target = "_self";
        f.submit();
    }

    function listarDocumentosResolutivos() {
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        f.method.value = "cargarListaDRs";
        f.target = "_self";
        f.submit();
    }
    
    function listarDocumentosResolutivosFirma() {
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        f.method.value = "cargarListaDRsFirma";
        f.target = "_self";
        f.submit();
    }
    
    /**
     * Muestra el listado de Certificados de Origen
     */
    function listarCertificadoOrigen(seleccionado) {
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        if (seleccionado != undefined){
        	f.action = f.action + "?seleccionado=" + seleccionado;
        }

        f.method.value = "listarCertificadoOrigen";
        f.target = "_self";
        f.submit();
    }

    /**
     * Muestra el listado de Declaraciones Juradas
     */
    function listarDeclaracionJurada(seleccionado) {
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        if (seleccionado != undefined){
        	f.action = f.action + "?seleccionado=" + seleccionado;
        }

        setCookie('indiceTab', '');
        f.method.value = "listarDeclaracionJurada";
        f.target = "_self";
        f.submit();
    }

    /**
     * Muestra el Buzón
     */
    function cargarBuzon(seleccionado) {
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        if (seleccionado != undefined){
        	f.action = f.action + "?seleccionado=" + seleccionado;
        }

        f.method.value = "listarCertificadoOrigen";
        f.target = "_self";
        f.submit();
    }

    /**
     * Muestra el listado de Solicitudes de Certificado para Entidad
     */
    function listarSolicitudesCertificadoEntidad(seleccionado) {
        var f = document.formulario;
        f.action = contextName + "/admentco.htm";
        if (seleccionado != undefined){
        	f.action = f.action + "?seleccionado=" + seleccionado;
        }

        setCookie('indiceTab', '');
        f.method.value = "cargarListaSolicitudesCertificadoOrigen";
        f.target = "_self";
        f.submit();
    }

    /**
     *	Invoca a la pantalla para elegir la tupa a realizar (en esta aplicación, las tupas son acciones sobre el certificado de origen)
     */
    function nuevoTramite() {
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        f.method.value="cargarTupas";
        f.submit();
    }
    
    function seleccionRolActivo(obj) {
    	var f = document.formularioRolesUsuario;
        f.action = contextName + "/usuario.htm";
        f.method.value="seleccionRolActivo";
        f.rolSeleccionado.value = obj.value;
        f.submit();
    }

</script>
	<form name="formularioRolesUsuario" method="post">
        <jlis:value name="method" type="hidden" />
        <input name="rolSeleccionado" type="hidden" />
    </form>
	<!-- HEAD -->
	<div id="head"><div><div>
		<h3>
		<c:if test="${empty noMostrarHeader && !empty sessionScope.USUARIO && !empty sessionScope.USUARIO.idUsuario && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.CONSULTA_DR'}">
		<a href="javascript:principal();">
		</c:if>
		<img src="/co/imagenes/vuce.jpg" alt="VUCE" />
		<c:if test="${empty noMostrarHeader && !empty sessionScope.USUARIO && !empty sessionScope.USUARIO.idUsuario && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.CONSULTA_DR'}">
		</a>
		</c:if>
		</h3>
        
		<c:if test="${empty ERROR_PAGE}">
		<c:if test="${empty noMostrarHeader && !empty sessionScope.USUARIO && !empty sessionScope.USUARIO.idUsuario}">
		<p style="text-align: left;">
		   <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && 
		              sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] != 'CO.ADMIN.HELP_DESK' &&
		              sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] != 'CO.CENTRAL.OPERADOR_FUNCIONAL' &&
                      sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] != 'CO.CENTRAL.SUPERVISOR_TECNICO' &&
                      sessionScope.USUARIO.roles['CO.ENTIDAD.CONSULTA_DR'] != 'CO.ENTIDAD.CONSULTA_DR'}" >
		   <label style="color: #ccc;">Rol activo:</label>
		   <jlis:selectSource name="rolActivo" source="LISTA_ROLES_USUARIO" scope="session" style="defaultElement=no" onChange="seleccionRolActivo(this)" />
		   </c:if>
		   
           <c:if test="${empty noMostrarHeader && !empty sessionScope.USUARIO && !empty sessionScope.USUARIO.idUsuario && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.CONSULTA_DR'}">
		   <a href="javascript:modificarDatosUsuario();">
		   </c:if>
		   <span style="color:#ccc;">${sessionScope.USUARIO.nombreCompleto}&nbsp;(${sessionScope.USUARIO.login})</span>
           <c:if test="${empty noMostrarHeader && !empty sessionScope.USUARIO && !empty sessionScope.USUARIO.idUsuario && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.CONSULTA_DR'}">
		   </a>
		   </c:if>
		   
		   <c:if test="${empty idioma || idioma=='es'}">
           <a href="javascript:salir();">Salir &raquo;</a>
           </c:if>
           <c:if test="${idioma=='en'}">
           <a href="javascript:salir();">Sign Out &raquo;</a>
           </c:if>
        </p>
        
        <%--c:if test="${sessionScope.MODULO_SELECCIONADO == 'CO'}"--%>
		<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] != 'CO.CENTRAL.OPERADOR_FUNCIONAL' &&
                      sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] != 'CO.CENTRAL.SUPERVISOR_TECNICO'}">
		<ul>
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' && sessionScope.USUARIO.roles['CO.USUARIO.OPERACION'] == 'CO.USUARIO.OPERACION'}" >
	            <li><a class="${activeNS}" href="javascript:nuevoTramite();" title="Permite crear un nuevo Certificado de Origen" >NUEVA SOLICITUD</a></li>
            </c:if>
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' || (sessionScope.USUARIO.tipoOrigen == 'ET' && sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK')}" >
	            <li><a class="${activeSO}" href="javascript:listarCertificadoOrigen(0);" title="Permite realizar o modificar un Certificado de Origen" >SOLICITUD</a></li>
	            <li><a class="${activeDJ}" href="javascript:listarDeclaracionJurada(0);" title="Permite validar una Declaración Jurada" >DECLARACIÓN JURADA</a></li>
	            <!-- <li><a class="${activeDJ}" href="javascript:cargarBuzon();" title="Permite realizar o modificar un Certificado de Origen" >BUZÓN</a></li>
	            <li><a class="${activeDJ}" href="javascript:listarServicios();" title="Permite realizar o modificar un Certificado de Origen" >SERVICIOS</a></li> -->
	            <%-- <li><a class="${activeDJ}" href="javascript:listarCertificadoOrigen();" title="Permite realizar o modificar una Mercancía - DJ" >MERCANCÍAS - DJ</a></li>--%>
            </c:if>
			            
            <%--20140624_JMC BUG 128--%>
            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' &&
                		  sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}" >               	
				<li><a class="${activeCE_AE}" href="javascript:listarSolicitudesCertificadoEntidad(0);" title="Muestra el listado de Certificados de Origen" >SOLICITUDES DE CERTIFICADO DE ORIGEN</a></li>
				<li><a class="${activeDJ}" href="javascript:listarDeclaracionJurada(0);" title="Permite validar una Declaración Jurada" >DECLARACIÓN JURADA</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' &&
                		  sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}" >
				<li><a class="${activeCE_AE}" href="javascript:listarSolicitudesCertificadoEntidad(0);" title="Muestra el listado de Certificados de Origen" >SOLICITUDES DE CERTIFICADO DE ORIGEN (RESOLUTOR)</a></li>
            </c:if>
            
            <%--20140624_JMC BUG 128--%>
            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' &&
                		  sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}" >                
				<li><a class="${activeCE_AE}" href="javascript:listarSolicitudesCertificadoEntidad(2);" title="Muestra el listado de Certificados de Origen" >SUCES</a></li>
				<li><a class="${activeDJ}" href="javascript:listarDeclaracionJurada(0);" title="Permite validar una Declaración Jurada" >DECLARACIÓN JURADA</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' &&
                		  sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA'}" >
				<li><a class="${activeCE_AE}" href="javascript:listarSolicitudesCertificadoEntidad(1);" title="Muestra el listado de Certificados de Origen" >SUCES</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' || (sessionScope.USUARIO.tipoOrigen == 'ET' && sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK')}">
            <li><a class="${activeDR}" href="javascript:listarDocumentosResolutivos();" title="Documentos Resolutivos">DOCUMENTO RESOLUTIVO</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' || (sessionScope.USUARIO.tipoOrigen == 'ET' && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.CONSULTA_DR')}" >
			    <li><a class="${activeBuzon}" href="javascript:listarBuzon();" title="Casilla Virtual en donde se depositan mensajes electrónicos de datos o documentos." >BUZ&Oacute;N ELECTR&Oacute;NICO</a></li>
            </c:if>
            
			<!--<c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' && sessionScope.USUARIO.roles['CO.USUARIO.FIRMA'] == 'CO.USUARIO.FIRMA'}" >
	            <li><a class="${activeDRFirma}" href="javascript:listarDocumentosResolutivosFirma();" title="Documentos Resolutivos Firma">INTEROPERABILIDAD FIRMAS</a></li>
	        </c:if>-->
	        
	        <li><a class="${activeServicios}" href="javascript:servicios();" title="Servicios" >SERVICIOS</a></li>
            
		</ul>
		</c:if>
		
        <c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                      sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO'}">
        <ul>
            <%--li><a class="${activeAlertas}" href="javascript:listarAlertas();" title="Alertas">ALERTAS</a></li--%>
            
            <%--c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                          sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO'}">
            <li><a class="${activeLogs}" href="javascript:listarLogs();" title="Logs">LOGS</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
                          sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO'}">
            <li><a class="${activeTransmisiones}" href="javascript:listarTransmisiones();" title="Transmisiones">TRANSMISIONES</a></li>
            </c:if--%>
            
            <c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL'}">
	            <li><a class="${activeSO}" href="javascript:listarCertificadoOrigen(0);" title="Permite realizar o modificar un Certificado de Origen" >SOLICITUD</a></li>
	            <li><a class="${activeDJ}" href="javascript:listarDeclaracionJurada(0);" title="Permite validar una Declaración Jurada" >DECLARACIÓN JURADA</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL'}">
				<li><a class="${activeCE_AE}" href="javascript:listarSolicitudesCertificadoEntidad(0);" title="Muestra el listado de Certificados de Origen" >SOLICITUDES DE CERTIFICADO DE ORIGEN (RESOLUTOR)</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL'}">
				<li><a class="${activeCE_AE}" href="javascript:listarSolicitudesCertificadoEntidad(2);" title="Muestra el listado de Certificados de Origen" >SUCES (RESOLUTOR)</a></li>
            </c:if>
            
            <c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL'}">
            <li><a class="${activeDR}" href="javascript:listarDocumentosResolutivos();" title="Documentos Resolutivos">DOCUMENTO RESOLUTIVO</a></li>
            </c:if>
            
            <li><a class="${activeBuzon}" href="javascript:listarBuzon();" title="Casilla Virtual en donde se depositan mensajes electrónicos de datos o documentos.">BUZ&Oacute;N ELECTR&Oacute;NICO</a></li>
            
            <li><a class="${activeServicios}" href="javascript:servicios();" title="Servicios" >SERVICIOS</a></li>
            
        </ul>
        </c:if>
		
		
		<%--/c:if--%>

        <%--c:if test="${sessionScope.MODULO_SELECCIONADO == 'SCE'}">
        <ul>
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' || (sessionScope.USUARIO.tipoOrigen == 'ET' && sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK')}">
            <li><a class="${activeDJ}" href="javascript:listarSolicitudesCalificacionExportador();" title="Muestra el listado de Solicitudes de Calificación para Exportador Autorizado" >SOLICITUDES CALIFICACIÓN EXPORTADOR AUTORIZADO</a></li>
            </c:if>
        </ul>
		</c:if--%>


        </c:if>
        </c:if>
	</div></div></div>

