<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String controller = (String)request.getAttribute("controller");
%>

	<script language="JavaScript">

    function crearRegistro() {
    	var f = document.formulario;
        f.target = "_self";

    	if (document.getElementById("REPRESENTANTE.id")!=null && document.getElementById("REPRESENTANTE.id").value==""){
   		 	f.button.value="cancelarButton";
	        alert("Debe Seleccionar el Representante Legal");
	        changeStyleClass("co.title.datos_usuario_empresa_representantelegal", "errorValueClass");
    	} else {
		    try {changeStyleClass("co.title.datos_usuario_empresa_representantelegal", "labelClass");}catch(e){}

		    if ( document.getElementById("buscarButton")!=null && document.getElementById("idAcuerdo").value == "" ) {
		    	alert("Debe Buscar un Certificado Original");
		    	f.button.value="cancelarButton";
			} else {

				<c:if test="${!empty formato && ( formato == 'MCT005' || formato == 'mct005' ) }">
					if ( document.getElementById("tipoCalificacion_2").checked && document.getElementById("calificacionUoId").value == '' ) {
						changeStyleClass("co.label.calificacion.copia.existente", "errorValueClass");
						alert("Debe seleccionar la calificaci�n a partir de la cual se copiar�n los datos de la presente solicitud.");

				    	f.button.value="cancelarButton";
					} else {
						try {changeStyleClass("co.label.calificacion.copia.existente", "labelClass");}catch(e){}

				</c:if>


			    f.action = contextName+"/origen.htm";
		        f.method.value="crearOrdenFormato";
		        f.button.value="nuevaButton";

		        <c:if test="${!empty formato && ( formato == 'MCT005' || formato == 'mct005' ) }">
					}
				</c:if>
			}
		}
    }

    function actualizarRepresentante(){
    	var f = document.formulario;
        f.target = "_self";
    	if (document.getElementById("REPRESENTANTE.id").value==""){
		 	f.button.value="cancelarButton";
	        alert("Debe Seleccionar el Representante Legal");
	        changeStyleClass("co.title.datos_usuario_empresa_representantelegal", "errorValueClass");
	    } else {
		    changeStyleClass("co.title.datos_usuario_empresa_representantelegal", "labelClass");
		    f.action = contextName+"/origen.htm";
	        f.method.value="actualizarRepresentante";
	        f.button.value="actualizarRepresentanteButton";
	    }
    }

    function crearModificacion() {
        var f = document.formulario;
        f.action = contextName+"/<%=controller%>";
        f.method.value="crearModificacionOrdenFormato";
        f.button.value="modifButton";
        f.target = "_self";
    }

    function transmitir() {
        var f = document.formulario;
        f.action = contextName+"/origen.htm";
        f.method.value="transmitirOrdenFormato";
        f.button.value="transButton";
        f.target = "_self";
    }


    function transmiteModificacionSuceMTO(){
        var f = document.formulario;
        f.action = contextName + "/origen.htm";
        f.method.value="transmiteSubsanacionSuceMTO";
        f.button.value="transButton";
        f.target = "_self";
    }

	function validarSubmit() {
        var f=document.formulario;
        f.target = "_self";
        if (f.button.value=="cancelarButton") {
            return false;
        }
        if (f.button.value=="regresarHistoricoButton") {
            history.go(-1);
            return false;
        }
        if (f.button.value=="modalButton") {
            return false;
        }
        if (f.button.value=="productoButton") {
            return false;
        }
        if (f.button.value=="solicitudesButton") {
	        return false;
        }
        if (f.button.value=="complementoButton") {
            return false;
        }
        if (f.button.value=="nuevoRespButton") {
            return false;
        }
        if (f.button.value=="nuevoEqTecButton") {
            return false;
        }
        if (f.button.value=="nuevoLugProdButton") {
            return false;
        }
        if (f.button.value=="eliminarRegistrosButton") {
            return false;
        }
        if (f.button.value=="nuevaFacturaButton"){
            return false;
        }
        if (f.button.value=="nuevaMercanciaButton"){
            return false;
        }
        if (f.button.value=="abrirDjButton"){
            return false;
        }
        if (f.button.value=="busqPartidasButton"){
            return false;
        }
        if (f.button.value=="generarBorradorDrButton"){
            return false;
        }
        if (f.button.value=="asignarEvaluadorButton"){
        	return false;
        }
        if (f.button.value == "partidaButton") {
            showPopWin(contextName + "/fmt.htm?method=cargarPartidas", 610, 400, null);
            return false;
        }
        if (f.button.value == "imprimirHojaResumenAvance") {
        	return false;
        }
        if (f.button.value == "backButton") {
        	return false;
        }
        if (f.button.value == "nuevoProductorButton") {
        	return false;
        }
        if (f.button.value == "solicitarValidacionButton") {
        	//return false;
        }
	    if ( f.button.value=="busqDJExistenteButton" ) {
        	return false;
        }
        if (f.button.value=="aceptarDesestimientoSUCEButton") {
            if (!confirm("�Est� seguro que desea aceptar el desistimiento de la SUCE?")) {
                return false;
            }
        }
        if (f.button.value=="rechazarDesestimientoSUCEButton") {
            if (!confirm("�Est� seguro que desea rechazar el desistimiento de la SUCE?")) {
                return false;
            }
        }

		if (f.button.value == "desisteButton") {
			var mensajeDesistimiento = "�Est� seguro que desea desistir el tr�mite?";
			var suce = document.getElementById("suce").value;
			var estadoRegistro = document.getElementById("estadoRegistro").value;
			if (estadoRegistro=="S") {
				if (suce=="") {
				    mensajeDesistimiento = "La Solicitud tiene Subsanaciones pendientes\n\n�Est� seguro que desea continuar con el desistimiento?";
			    } else {
			    	mensajeDesistimiento = "La SUCE tiene Subsanaciones pendientes\n\n�Est� seguro que desea continuar con el desistimiento?";
			    }
			} else {
				mensajeDesistimiento = "�Est� seguro que desea desistir el tr�mite?";
			}
			
			if (!confirm(mensajeDesistimiento)) {
				return false;
			}			
			f.action = contextName+"/origen.htm";
           	f.method.value="desisteOrdenFormato";
            disableButton("transButton", true);
            disableButton("modifButton", true);
            disableButton("desisteButton", true);
            disableButton("cancelarModificacionOrdenButton", true);
            disableButton("detalleButton", true);
            disableButton("copiaButton", true);
		}
        if (f.button.value == "cancelarModificacionOrdenButton") {
            if (!confirm("�Est� seguro de cancelar la modificaci�n de Solicitud?")) {
                return false;
            }
            f.action = contextName+"/<%=controller%>";
            f.method.value="desisteOrdenFormato";
            disableButton("transButton", true);
            disableButton("modifButton", true);
            disableButton("desisteButton", true);
            disableButton("cancelarModificacionOrdenButton", true);
            disableButton("detalleButton", true);
            disableButton("copiaButton", true);
        }
		if(f.button.value == "cancelarModificacionSUCEButton"){
            if (!confirm("�Est� seguro de cancelar la modificaci�n de SUCE?")) {
                return false;
            }
            f.action = contextName+"/origen.htm";
            f.method.value="cancelarSubsanacionSUCE";
            f.mtoSUCE.value = "";
            disableButton("transButton", true);
            disableButton("cancelarModificacionSUCEButton", true);
        }
		if (f.button.value=="nuevaModifButton") {
			var suceId = f.suce.value;
	    	var orden = f.orden.value;
	    	var mto = f.mto.value;
	    	var idFormato = f.idFormato.value;
	    	var formato = f.formato.value;
	    	var entidad = f.idEntidad.value;
	    	var modificacionSuceId = 0;
	    	var mensajeId = 0;
            var estadoRegistro = f.estadoRegistro.value;
	    	showPopWin(contextName+"/<%=controller%>?method=cargarSuce&suce="+suceId+"&orden="+orden+"&mto="+mto+"&idEntidad="+entidad+"&idFormato="+idFormato+"&formato="+formato+"&modificacionSuceId="+modificacionSuceId+"&mensajeId="+mensajeId+"&estadoRegistro="+estadoRegistro+"&tipo=M", 710, 500, null);
            return false;
        }
        if (f.button.value=="nuevaSubsanacionButton") {
            var suceId = f.suce.value;
            //alert("suceId " + suceId);
            var orden = f.orden.value;
            //alert("orden " + orden);
            var mto = f.mto.value;
            //alert("mto " + mto);
            var idFormato = f.idFormato.value;
            //alert("idFormato " + idFormato);
            var formato = f.formato.value;
            //alert("formato " + formato);
            var entidad = f.idEntidad.value;
            //alert("entidad " + entidad);
            
            //20170113_GBT ACTA CO-004-16 Y ACTA CO 009-16
            var idAcuerdo = f.idAcuerdo.value;
            
            //20170127_GBT ACTA CO-004-16 Y ACTA CO 009-16
            var estadoAcuerdoPais = f.estadoAcuerdoPais.value;
            
            
            var modificacionSuceId = 0;
            var mensajeId = 0;
            var estadoRegistro = f.estadoRegistro.value;
            if (suceId != '') {
            	//20170113_GBT ACTA CO-004-16 Y ACTA CO 009-16
            	//20170127_GBT ACTA CO-004-16 Y ACTA CO 009-16
            	showPopWin(contextName+"/origen.htm?method=cargarSuce&suce="+suceId+"&orden="+orden+"&idAcuerdo="+idAcuerdo+"&mto="+mto+"&idEntidad="+entidad+"&idFormato="+idFormato+"&formato="+formato+"&modificacionSuceId="+modificacionSuceId+"&mensajeId="+mensajeId+"&estadoRegistro="+estadoRegistro+"&tipo=S"+"&estadoAcuerdoPais="+estadoAcuerdoPais, 710, 500, null);
                return false;
            } else {
            	//20170113_GBT ACTA CO-004-16 Y ACTA CO 009-16
            	//20170127_GBT ACTA CO-004-16 Y ACTA CO 009-16
            	showPopWin(contextName+"/origen.htm?method=cargarNotifXSubsanacion&orden="+orden+"&mto="+mto+"&formato="+formato+"&estadoAcuerdoPais="+estadoAcuerdoPais, 710, 250, null);
                return false;
            }
        }
        if (f.button.value=="nuevaButton") {
            disableButton("nuevaButton", true);
        }
        if (f.button.value=="transButton") {
        	if (document.getElementById("REPRESENTANTE.id")!=null && document.getElementById("REPRESENTANTE.id").value==""){
                disableButton("transButton", true);
                changeStyleClass("co.title.datos_usuario_empresa_representantelegal", "errorValueClass");
                alert("Debe Seleccionar el Representante Legal");
                return false;
        	}
            if (typeof window.validarDetalle == "function" && !validarDetalle()) {
                return false;
            }
            disableButton("transButton", true);
            disableButton("modifButton", true);
            disableButton("desisteButton", true);
            disableButton("detalleButton", true);
            disableButton("copiaButton", true);
        }
        if (f.button.value=="modifButton") {
            disableButton("transButton", true);
            disableButton("modifButton", true);
            disableButton("desisteButton", true);
            disableButton("detalleButton", true);
            disableButton("copiaButton", true);
        }
        if (f.button.value=="detalleButton") {
            disableButton("transButton", true);
            disableButton("modifButton", true);
            disableButton("desisteButton", true);
            disableButton("detalleButton", true);
            disableButton("copiaButton", true);
        }
        if (f.button.value=="copiaButton") {
            disableButton("transButton", true);
            disableButton("modifButton", true);
            disableButton("desisteButton", true);
            disableButton("detalleButton", true);
            disableButton("copiaButton", true);
        }
        if (f.button.value=="otraInformacionButton") {
            var orden = f.orden.value;
            var mto = f.mto.value;
            var formato = f.formato.value;
            var frmlow = formato.toLowerCase();
            showPopWin(contextName+"/origen.htm?method=cargarOtraInformacionTramite&orden="+orden+"&mto="+mto+"&formato="+frmlow, 900, 550, null);
            return false;
        }
        if (f.button.value=="solicitarNuevoDRButton") {
            var suceId = f.suce.value;
            var orden = f.orden.value;
            var mto = f.mto.value;
            var idFormato = f.idFormato.value;
            var formato = f.formato.value;
            var entidad = f.idEntidad.value;
            var aperturaSuceId = 0;
            var mensajeId = 0;
            var estadoRegistro = f.estadoRegistro.value;
            showPopWin(contextName+"/<%=controller%>?method=cargarSolicitudNuevoDR&suce="+suceId+"&orden="+orden+"&mto="+mto+"&idEntidad="+entidad+"&idFormato="+idFormato+"&formato="+formato+"&aperturaSuceId="+aperturaSuceId+"&mensajeId="+mensajeId+"&estadoRegistro="+estadoRegistro, 710, 500, null);
            return false;
        }
        return true;
    }

    function nuevaModifSuce(){
    	var f = document.formulario;
    	f.button.value="nuevaModifButton";
        f.target = "_self";
    }

    function nuevaSubsanacionSuce(){
        var f = document.formulario;
        f.button.value="nuevaSubsanacionButton";
        f.target = "_self";
    }

	function verModificacion(keyValues, keyValuesField){
		var f = document.formulario;
        var suceId = f.suce.value;
    	var orden = f.orden.value;
    	var mto = f.mto.value;
    	var idFormato = f.idFormato.value;
    	var formato = f.formato.value;
    	var entidad = f.idEntidad.value;
    	var modificacionSuceId = document.getElementById(keyValues.split("|")[0]).value;
    	var mensajeId = document.getElementById(keyValues.split("|")[1]).value;
    	var estadoModif = document.getElementById(keyValues.split("|")[2]).value;
        var estadoRegistro = f.estadoRegistro.value;
      	//20170113_GBT ACTA CO-004-16 Y ACTA CO 009-16
        var idAcuerdo = f.idAcuerdo.value;
	    
      	//20170127_GBT ACTA CO-004-16 Y ACTA CO 009-16
        var estadoAcuerdoPais = f.estadoAcuerdoPais.value;
        
        showPopWin(contextName+"/origen.htm?method=cargarSuce&suce="+suceId+"&orden="+orden+"&idAcuerdo="+idAcuerdo+"&mto="+mto+"&idFormato="+idFormato+"&formato="+formato+"&idEntidad="+entidad+"&modificacionSuceId="+modificacionSuceId+"&mensajeId="+mensajeId+"&estadoRegistro="+estadoRegistro+"&estadoModif="+estadoModif+"&estadoAcuerdoPais="+estadoAcuerdoPais, 720, 620, null);
	}

	function verMTS(keyValues, keyValuesField) {
		var f = document.formulario;
        f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
        var formato = document.getElementById("formato").value;
        var frmlow = formato.toLowerCase();
        f.historico.value = "S";
        f.action = contextName+"/"+frmlow+".htm?method=cargarInformacionOrden";
        f.target = "_self";
        f.submit();
	}

    function verSolicitudNuevoDR(keyValues, keyValuesField){
        var f = document.formulario;
        var suceId = f.suce.value;
        var orden = f.orden.value;
        var mto = f.mto.value;
        var idFormato = f.idFormato.value;
        var formato = f.formato.value;
        var entidad = f.idEntidad.value;
        var aperturaSuceId = document.getElementById(keyValues.split("|")[0]).value;
        var mensajeId = document.getElementById(keyValues.split("|")[1]).value;
        var estadoRegistro = f.estadoRegistro.value;
        showPopWin(contextName+"/<%=controller%>?method=cargarSolicitudNuevoDR&suce="+suceId+"&orden="+orden+"&mto="+mto+"&idFormato="+idFormato+"&formato="+formato+"&idEntidad="+entidad+"&aperturaSuceId="+aperturaSuceId+"&mensajeId="+mensajeId+"&estadoRegistro="+estadoRegistro, 720, 620, null);
    }

    function adjuntarDocumento(keyValues, keyValuesField) {
    	var f = document.formulario;
        var adjunto = document.getElementById(keyValues.split("|")[0]).value;
        var idFormatoEntidad = f.idFormatoEntidad.value;
        var idFormato = f.idFormato.value;
        var orden = f.orden.value;
        var mto = f.mto.value;
        var bloqueado=f.bloqueado.value;
        var transmitido = f.transmitido.value;
        var controller = f.controller.value;
        showPopWin(contextName+"/origen.htm?method=cargarAdjuntos&adjunto="+adjunto+"&idFormatoEntidad="+idFormatoEntidad+"&idFormato="+idFormato+
                   "&orden="+orden+"&mto="+mto+"&bloqueado="+bloqueado+"&transmitido="+transmitido+"&controller="+controller, 750, 500, actualizar);
    }

    function desistir() {
        var f = document.formulario;
        f.button.value="desisteButton";
        f.target = "_self";
    }

    function cancelarModificacionOrden() {
        var f = document.formulario;
        f.button.value="cancelarModificacionOrdenButton";
        f.target = "_self";
    }

    function cancelarModificacionSUCE() {
        var f = document.formulario;
        f.button.value="cancelarModificacionSUCEButton";
        f.target = "_self";
    }

/*
 * Generales
 */
    function actualizar() {
        var f = document.formulario;
        f.action = contextName+"/origen.htm";
        f.method.value="cargarInformacionOrden";
        f.button.value="actualizarButton";
        f.target = "_self";
        f.submit();
    }

    function regresar() {

    	//alert("entre a regresar")
    	var f = document.formulario;
        f.target = "_self";

        <c:choose>

	        <c:when test="${!empty numSuce && !empty mtoSUCE && utilizaModificacionSuceXMto == 'S'}">
		        <c:choose>
			        <c:when test="${sessionScope.USUARIO.roles['VUCE.SUNAT.ESPECIALISTA'] != 'VUCE.SUNAT.ESPECIALISTA' && (opcion == 'FAE' || opcion == 'OE' || opcion == 'SE')}">

				        f.action = contextName+"/adment.htm";
				        if (f.opcion.value=="OE")
				            f.method.value="cargarListaOrdenes";
				        else if (f.opcion.value=="SE")
				            f.method.value="cargarListaSuces";
				        else if (f.opcion.value=="FAE")
				            f.method.value="abrirSuce";
			        </c:when>
			        <c:otherwise>
						//alert("aca??")
				        f.action = contextName+"/origen.htm";
				        f.method.value="cargarInformacionOrden";
				        f.versionOriginal.value = "S";
				        f.mtoSUCE.value = "";
				        //alert(f.numSuce.value);
			        </c:otherwise>
		        </c:choose>

	        </c:when>

        <c:otherwise>
	        <c:if test="${sessionScope.USUARIO.roles['VUCE.SUNAT.ESPECIALISTA'] != 'VUCE.SUNAT.ESPECIALISTA'}">
		        <c:if test="${historico=='S'}" >
		        	f.button.value="regresarHistoricoButton";
		        </c:if>

		        <c:if test="${historico==''}" >
		            if (f.opcion.value=="O" || f.opcion.value=="S")
		                f.action = contextName+"/fmt.htm";
		            else if (f.opcion.value=="FAE" || f.opcion.value=="OE" || f.opcion.value=="SE")
		                f.action = contextName+"/adment.htm";
		            if (f.opcion.value=="O" || f.opcion.value=="OE")
		                f.method.value="cargarListaOrdenes";
		            else if (f.opcion.value=="S")
		                f.method.value="cargarListaOrdenes";
		            else if (f.opcion.value=="SE")
		                f.method.value="cargarListaSuces";
		            else if (f.opcion.value=="FAE")
		                f.method.value="abrirSuce";
		            f.button.value="regresarButton";
		        </c:if>
	        </c:if>
        </c:otherwise>
	</c:choose>

        <c:if test="${historico==''}" >
        	f.button.value="regresarButton";
    	</c:if>
//alert(f.opcion.value);
  //  	if (f.opcion.value==""){
    		//f.action = contextName+"/fmt.htm";
    		//f.method.value="cargarListaOrdenes";
    		/*var numRecargas = 0;
    		if (f.numRecargas != undefined && f.numRecargas.value != ""){
       			numRecargas = -1*(parseInt(f.numRecargas.value, 10));
       			numRecargas = (numRecargas < 0 ? numRecargas - 1: numRecargas);
    		}

    		f.button.value="backButton";
    		if (numRecargas < 0) {
    			window.history.go( numRecargas );
        		//return false;
    		} else {
    			*/
    			f.action = contextName+"/origen.htm";
        		f.method.value="listarCertificadoOrigen";
        		//alert('este es?')
        		f.button.value="regresarButton";
                f.target = "_self";
    			/*window.history.back();
    		}*/

    //	}
    }

    function regresarSuceOriginal(){
    	var f = document.formulario;
        f.target = "_self";
		f.action = contextName+"/origen.htm";
    	f.method.value = "cargarInformacionOrdenVigente";
    	f.submit();
    }

	/*function verDatosDocResolutivo(keyValues, keyValuesField) {
		var f = document.formulario;
        var drId = document.getElementById(keyValues.split("|")[0]).value;
        var sdr = document.getElementById(keyValues.split("|")[1]).value;
        var numSuce = f.numSuce.value;
        var idFormatoEntidad = document.getElementById("idFormatoEntidad").value;
		showPopWin(contextName+"/<%=controller%>?method=cargarDatosDocumentoResolutivo&drId="+drId+"&sdr="+sdr+"&idFormatoEntidad="+idFormatoEntidad+"&numSuce="+numSuce, 900, 550, null);
	}*/

	function verDatosDocResol(keyValues, keyValuesField) {
		var f = document.formulario;
    	var drId = document.getElementById(keyValues.split("|")[0]).value;
        var sdr = document.getElementById(keyValues.split("|")[1]).value;
        var formatoDrId = document.getElementById("idFormatoEntidad").value;
        var ordenId = f.orden.value;
        var mto = f.mto.value;
        var numSuce = f.suce.value;
        var tupaId = f.idTupa.value;
        var formato = f.formato.value;
        var idAcuerdo = f.idAcuerdo.value;
		var adjuntoIdFirma = '';
		if (f.adjuntoIdFirma != undefined) {
			adjuntoIdFirma = f.adjuntoIdFirma.value;
		}
		showPopWin(contextName+"/origen.htm?method=cargarDatosDocumentoResolutivo&drId="+drId+"&sdr="+sdr+"&formatoDrId="+formatoDrId+"&numSuce="+numSuce+"&ordenId="+ordenId+"&mto="+mto+"&tupaId="+tupaId+"&formato="+formato+"&idAcuerdo="+idAcuerdo+"&adjuntoIdFirma="+adjuntoIdFirma, 900, 550, null);
	}

	function verAdjuntos(keyValues, keyValuesField) {
		var drId = document.getElementById(keyValues.split("|")[0]).value;
        var sdr = document.getElementById(keyValues.split("|")[1]).value;
		showPopWin(contextName+"/<%=controller%>?method=cargarAdjuntosDocResolutivo&drId="+drId+"&sdr="+sdr, 710, 500, null);
	}

	function otraInformacion() {
        var f = document.formulario;
        f.button.value="otraInformacionButton";
        f.target = "_self";
	}

	function solicitarNuevoDR() {
        var f = document.formulario;
        f.button.value="solicitarNuevoDRButton";
        f.target = "_self";
	}

    function imprimirHojaResumenSUCE(anulacion) {
        var f = document.formulario;
        f.action = contextName+"/origen.htm";

        if ( anulacion == undefined ) {
        	f.method.value = "imprimirHojaResumenSUCE";
        } else {
        	f.method.value = "imprimirResumenAnulacionSUCE";
        }

        f.button.value = "imprimirHojaResumenSUCEButton";
        f.target = "_blank";
    }

    function imprimirPrimeraFirma() {
        var f = document.formulario;
        f.action = contextName+"/origen.htm";
    	f.method.value = "verPrimeraFirma";
        f.button.value = "imprimirHojaResumenSUCEButton";
        f.target = "_blank";
    }

    function imprimirSegundaFirma() {
        var f = document.formulario;
        f.action = contextName+"/origen.htm";
    	f.method.value = "verSegundaFirma";
        f.button.value = "imprimirHojaResumenSUCEButton";
        f.target = "_blank";
    }
    
    function imprimirHojaResumenAvance() {
        var f = document.formulario;
        var supervisor = '${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}';

        if ( f.formato.value == 'mct002' || f.formato.value == 'MCT002' || f.formato.value == 'mct004' || f.formato.value == 'MCT004' ) {
        	var adjuntoIdFirma = '';

        	if (f.adjuntoIdFirma != undefined){
        		adjuntoIdFirma = f.adjuntoIdFirma.value;
        	}

        	if (adjuntoIdFirma == ''){
        		alert('Hasta el momento no se ha adjuntado la firma al DR de aprobaci�n.');
        	} else {
            	window.open(contextName+"/origen.htm?method=descargarAdjunto&idAdjunto=" + adjuntoIdFirma);
        	}
        } else {
        	window.open(contextName+"/origen.htm?method=imprimirHojaResumenAvance&formato=" + f.formato.value+"&orden=" + f.orden.value + "&mto="+f.mto.value + "&idAcuerdo=" + f.idAcuerdo.value ,"Vista_Previa");
        }
        return false;
    }

	// Carga la p�gina de Datos de Documento Resolutivo
	function cargarDocResol(drId, sdr) {
		var f = document.formulario;
        var formatoDrId = document.getElementById("idFormatoEntidad").value;
        var ordenId = f.orden.value;
        var mto = f.mto.value;
        var numSuce = f.suce.value;
        var tupaId = f.idTupa.value;
        var formato = f.formato.value;
		showPopWin(contextName+"/origen.htm?method=cargarDatosDocumentoResolutivo&drId="+drId+"&sdr="+sdr+"&formatoDrId="+formatoDrId+"&numSuce="+numSuce+"&ordenId="+ordenId+"&mto="+mto+"&tupaId="+tupaId+"&formato="+formato, 900, 550, null);
	}

	// Carga la p�gina de asignaci�n de solicitudes a un evaluador - Funcionalidad Resolutor
    function asignarEvaluador(){
    	var f = document.formulario;
    	var numeroSolicitud = document.getElementById("numeroSolicitud").value;
        var orden = document.getElementById("orden").value;
        var mto = document.getElementById("mto").value;
        showPopWin(contextName+"/admentco.htm?method=asignacionEvaluadorSolicitudCertificado&orden=" + orden + "&mto=" + mto + "&numeroSolicitud=" + numeroSolicitud, 400, 200, null);
    	f.button.value="nuevaFacturaButton";
    }

	// Denegaci�n de la solicitud por un supervisor - Funcionalidad Resolutor
	function denegarSolicitud(){
		var f = document.formulario;
        var orden = document.getElementById("orden").value;
        var mto = document.getElementById("mto").value;
        f.action = contextName+"/admentco.htm?method=denegarSolicitud&orden="+orden+"&mto="+mto;
        f.target = "_self";
        f.submit();
	}

	// Confirmaci�n de fin de evaluaci�n por un evaluador - Funcionalidad Resolutor
	function confirmarFinEval() {
		var f = document.formulario;
		f.action = contextName+"/origen.htm";
		f.method.value="confirmarFinDJEval";
		f.button.value="GrabarDetalleButton";
	}
	function nuevaNotificacionSubsanacionOrden() {
	    var f = document.formulario;
	    var ordenId = f.orden.value;
	    var mto = f.mto.value;
	    var formato = f.formato.value;
	    //var suceId = f.suceId.value;
	    /*alert("ordenId " + ordenId);
	    alert("mto " + mto);
	    alert("suceId " + suceId);*/
	    f.button.value = "nuevaFacturaButton";
	    showPopWin(contextName+"/admentco.htm?method=editarNotificacionSubsanacionOrden&ordenId="+ordenId+"&mto="+mto+"&formato="+formato, 650, 550, null);
	}

	function verModificacionOrden(keyValues, keyValuesField){
		var f = document.formulario;
    	f.orden.value = document.getElementById(keyValues.split("|")[0]).value;
    	f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
    	//var idFormato = f.idFormato.value;
    	var formato = f.formato.value;
    	//var entidad = f.idEntidad.value;
    	//var modificacionSuceId = document.getElementById(keyValues.split("|")[0]).value;
    	//var mensajeId = document.getElementById(keyValues.split("|")[1]).value;
    	//var estadoModif = document.getElementById(keyValues.split("|")[2]).value;
        //var estadoRegistro = f.estadoRegistro.value;
        f.method.value = "cargarInformacionOrden";
		f.button.value="GrabarDetalleButton";
        f.target = "_self";
        f.submit();
        //showPopWin(contextName+"/origen.htm?method=cargarInformacionOrden&orden="+orden+"&mto="+mto+"&formato="+formato, 720, 620, null);
	}

	function eliminarSubsanacionOrden(){
		var f = document.formulario;
    	var formato = f.formato.value;
        f.method.value = "eliminarSubsanacionSolicitud";
		f.button.value="GrabarDetalleButton";
        f.target = "_self";
        f.submit();
        //showPopWin(contextName+"/origen.htm?method=cargarInformacionOrden&orden="+orden+"&mto="+mto+"&formato="+formato, 720, 620, null);
	}



	</script>