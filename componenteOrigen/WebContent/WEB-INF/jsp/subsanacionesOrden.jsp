<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="vuce"%>

        <vuce:validacionBotonesFormato formato="MCT001" pagina="modificacionesSuce" />

        <table class="form">
        	<%-- <c:if test="${!empty notifPendientes && notifPendientes > 0}"> --%>
				<tr>
	                <td><jlis:button code="CO.USUARIO.OPERACION" id="nuevaSubsanacionButton" name="nuevaSubsanacionSuce()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Crear Subsanación para Responder Notificación" alt="Pulse aquí para responder una Notificación de Subsanación de la Solicitud" /></td>
	                <td width="100%"></td>
				</tr>
			<%-- </c:if> --%>
		</table>
		<jlis:table name="MODIFICACION_SUCE" pageSize="*" keyValueColumns="ORDEN_ID,MTO" source="tSubsanaciones" scope="request" navigationHeader="no" width="100%">
			<jlis:tr>
				<jlis:td name="FECHA REGISTRO" nowrap="yes" width="8%" />
				<jlis:td name="FECHA ACTUALIZACION" nowrap="yes" width="8%" />
				<jlis:td name="DESCRIPCION" nowrap="yes" width="25%"/>
				<jlis:td name="MENSAJE NOTIFICACION" nowrap="yes" width="50%"/>
				<jlis:td name="ORDEN_ID" nowrap="yes" />
                <jlis:td name="MTO" nowrap="yes" />
			</jlis:tr>
			<jlis:columnStyle column="1" columnName="FECHA_REGISTRO" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center" />
			<jlis:columnStyle column="2" columnName="FECHA_ACTUALIZACION" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center" />
			<jlis:columnStyle column="3" columnName="DESCRIPCION" editable="yes" />
			<jlis:columnStyle column="4" columnName="MENSAJE_NOTIFICACION" editable="no" />
			<jlis:columnStyle column="5" columnName="ORDEN_ID" hide="yes" />
			<jlis:columnStyle column="6" columnName="MTO" hide="yes" />
			<!-- jlis:value name="estadoAcuerdoPais" type="hidden" / -->
            <jlis:tableButton column="3" type="link" onClick="verModificacionOrden" />
		</jlis:table>
