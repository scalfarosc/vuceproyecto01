<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<c:if test="${empty idioma || idioma=='es'}">
	<div id="foot">
		<p class="copyright" align="right">Esta pantalla est� optimizada para Internet Explorer 7.0, Firefox 3.0, Google Chrome 3.0 y Safari 4.0 o versiones posteriores. Vea las indicaciones de <a href="https://www.vuce.gob.pe/manual_vuce/vuce_compatibilidad.pdf" target="_blank" style="color: #ccc;text-decoration:underline;">compatibilidad</a>.
        <c:if test="${!empty fechaHoraActual}">
        <jlis:label key="consulta_dr.label.fecha_hora" language="${idioma}" />${fechaHoraActual}
        </c:if>
		</p>
		
		<p><span class="mincetur"><b>Ministerio de Comercio Exterior y Turismo</b></span></p>
		<p class="copyright">&copy; Copyright 2010 - MINCETUR Todos los derechos reservados. Ante cualquier duda o problema contacte a Mesa de Ayuda: <img src="/co/imagenes/telefono.png" style="vertical-align: middle;" /> 207-1510 / 713-4646
           &nbsp;&nbsp;<img src="/co/imagenes/sobre.gif" style="vertical-align: middle;" /> vuceayuda@mincetur.gob.pe
        </p>
	</div>
</c:if>
<c:if test="${idioma=='en'}">
    <div id="foot">
        <p class="copyright" align="right">This screen is optimized for Internet Explorer 7.0, Firefox 3.0, Google Chrome 3.0 and Safari 4.0 or later versions.
        <c:if test="${!empty fechaHoraActual}">
        <jlis:label key="consulta_dr.label.fecha_hora" language="${idioma}" />${fechaHoraActual}
        </c:if>
        </p>
        
        <p><span class="mincetur"><b>Ministry of Foreign Trade and Tourism</b></span></p>
        <p class="copyright">&copy; Copyright 2010 - MINCETUR All rights reserved. Any doubt or problem, contact Help Desk: <img src="/co/imagenes/sobre.gif" style="vertical-align: middle;" /> origen@mincetur.gob.pe
        </p>
    </div>
</c:if>
