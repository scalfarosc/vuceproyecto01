<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript">

    function retornar(keyValues, keyValuesField){
        var p = window.parent;

        if (document.getElementById(keyValues.split('|')[2]).value == 'N' ) {
        	alert('La partida escogida no puede ser seleccionada.');
        	return false;
        }

        p.cargarDataPartida(document.getElementById(keyValues.split('|')[0]).value, document.getElementById(keyValues.split('|')[1]).value);

        p.hidePopWin(false);
   }

    function buscar(){
    	var f = document.formulario;

    	var r = new RegExp(new RegExp("\\d{2}$", "g"));

    	if($('#opbusqueda_0').attr("checked") && !r.test( $("#codigo").val() )) {
    		alert("La subpartida arancelaria debe contener por lo menos 2 d�gitos.");
    		f.button.value="cancelarButton";

    	} else if ($('#opbusqueda_1').attr("checked") && !$('#opBusqDesc_1').attr("checked") && !$('#opBusqDesc_2').attr("checked") ) {
    		alert("Debe indicar si la b�squeda por descripci�n ser� por aproximaci�n o exacta");
    		f.button.value="cancelarButton";

    	} else {
    		f.button.value="";
			f.action = contextName+"/origen.htm";
			f.method.value="obtenerPartidas";
    	}
	}

    function seleccionOpcion(obj) {
        if (obj.value=="0") {
        	$('#codigo').attr('readonly',false);
            $('#codigo').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#codigo').focus();
            $('#descripcion').val("");
            $('#descripcion').attr('readonly',true);
            $('#descripcion').removeClass("inputTextClass").addClass("readonlyInputTextClass");

            //$('#opBusqDesc').val("");
            $('#opBusqDesc_1').attr('readonly',true);
            $('#opBusqDesc_1').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#opBusqDesc_1').attr('disabled',true);
            $('#opBusqDesc_2').attr('readonly',true);
            $('#opBusqDesc_2').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#opBusqDesc_2').attr('disabled',true);


        } else if (obj.value=="1") {
            $('#codigo').val("");
            $('#codigo').attr('readonly',true);
            $('#codigo').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#descripcion').attr('readonly',false);
            $('#descripcion').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#descripcion').focus();

            $('#opBusqDesc_1').attr('readonly',false);
            $('#opBusqDesc_1').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#opBusqDesc_1').attr('disabled',false);
            $('#opBusqDesc_2').attr('readonly',false);
            $('#opBusqDesc_2').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#opBusqDesc_2').attr('disabled',false);
        }
    }

    $(document).ready(function() {
    	tituloPopUp('B�squeda de Partidas');
    	if($('#codigo').val()!="") {
     		$('#opbusqueda_0').attr("checked",true);
     		$('#codigo').attr('readonly',false);
            $('#codigo').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#codigo').focus();
            $('#descripcion').val("");
            $('#descripcion').attr('readonly',true);
            $('#descripcion').removeClass("inputTextClass").addClass("readonlyInputTextClass");

            $('#opBusqDesc_1').attr('readonly',true);
            $('#opBusqDesc_1').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#opBusqDesc_1').attr('disabled',true);
            $('#opBusqDesc_2').attr('readonly',true);
            $('#opBusqDesc_2').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#opBusqDesc_2').attr('disabled',true);

    	} else 	if($('#descripcion').val()!="") {
    		$('#opbusqueda_1').attr("checked",true);
    		  $('#codigo').val("");
              $('#codigo').attr('readonly',true);
              $('#codigo').removeClass("inputTextClass").addClass("readonlyInputTextClass");
              $('#descripcion').attr('readonly',false);
              $('#descripcion').removeClass("readonlyInputTextClass").addClass("inputTextClass");
              $('#descripcion').focus();

              $('#opBusqDesc_${opBusqDesc}').attr('checked', true);
     	} else $('#descripcion').focus();
	});

    function validarSubmit() {
		var f = document.formulario;
		if (f.button.value=="cancelarButton") {
			return false;
        }
		if (f.button.value=="cerrarPopUpButton") {
        	return validaCerraPopUp();
        }
		return true;
	}

</script>
<body id="contModal">
<jlis:messageArea width="100%" />
<form name="formulario" method="post"  onSubmit="return validarSubmit();">
    <input type="hidden" name="method" />
	<input type="hidden" name="button" value="" />
	<table class="form">
		<tr>
		<td><jlis:value type="radio" name="opbusqueda" checkValue="0" onClick="seleccionOpcion(this)" /> </td>
		<th><jlis:label key="co.label.partida_arancelaria"/></th>
		<td><jlis:value name="codigo" size="15" type="text" editable="no" maxLength="10" /></td>
		</tr>
		<tr>
			<td><jlis:value type="radio" name="opbusqueda" checkValue="1" defaultCheck="yes" onClick="seleccionOpcion(this)" /></td>
			<th><jlis:label key="co.label.partidas.descripcion"/></th>
			<td><jlis:value name="descripcion" size="80"  type="text"/></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<th>
				<jlis:value type="radio" name="opBusqDesc" checkValue="1" defaultCheck="yes" /><jlis:label key="co.label.partidas.busqueda.desc.exacta"/>&nbsp;
				<jlis:value type="radio" name="opBusqDesc" checkValue="2" defaultCheck="no" /><jlis:label key="co.label.partidas.busqueda.desc.aprox"/>
			</th>
		</tr>
		<tr>
		<td colspan="3">
            <jlis:button code="CO.USUARIO.OPERACION" id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Se puede buscar digitando parte de la descripci�n de la partida" />
		    <jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
		</td>
		</tr>
		<tr>
		<td colspan="3">
		    <table>
		    	<tr><td style="font-size:11px;font-color:blue"><i>Haga click <a href="http://www.aduanet.gob.pe/ol-ad-tg/ServletTGConsultaTablas" target="blank">aqu�</a> para descargar el arancel SUNAT (Item 1: NANDINA, Item 3: NANDUNI)</i></td></tr>
		    	<tr><td style="font-size:9px;font-color:blue"><i>NANDINA, contiene el listado de las Partidas Arancelarias oficiales segun SUNAT</i></td></tr>
		    	<tr><td style="font-size:9px;font-color:blue"><i>NANDUNI, contiene el listado de las Unidades de Medida por Partida Arancelaria</i></td></tr>
		    </table>
		</td>
		</tr>
	</table>
	<br/>
    <jlis:table  name="PARTIDA_ARANCELARIA" keyValueColumns="PARTIDATEXTO,DESCRIPCION,SELECCIONABLE"
        source="tPartidas" scope="request" pageSize="10" divStyle="overflow:auto;height:250px" fixedHeader="yes" navigationHeader="yes" width="98%"
        validator="pe.gob.mincetur.vuce.co.web.util.BusqPartidasRowValidator">
		<jlis:tr>
        	<jlis:td name="C�DIGO PARTIDA" nowrap="yes" />
        	<jlis:td name="C�DIGO" nowrap="yes" />
        	<jlis:td name="DESCRIPCI�N" nowrap="yes" />
        	<jlis:td name="SELECCIONABLE" nowrap="yes" />
        	<jlis:td name="IND_COLOR" nowrap="yes" />
        	<jlis:td name="CODIGO FORMATEADO" nowrap="yes" />
        </jlis:tr>
        <jlis:columnStyle column="1" columnName="PARTIDAARANCELARIA" hide="yes"/>
        <jlis:columnStyle column="2" columnName="PARTIDATEXTO" align="center" editable="yes" />
        <jlis:columnStyle column="3" columnName="DESCRIPCION" />
        <jlis:columnStyle column="4" columnName="SELECCIONABLE" hide="yes" />
        <jlis:columnStyle column="5" columnName="INDCOLOR" hide="yes" />
        <jlis:columnStyle column="6" columnName="PARTIDAFORMATEADA" hide="yes" />
        <jlis:tableButton column="2" type="link" onClick="retornar" />
    </jlis:table>
    </form>

</body>
</html>