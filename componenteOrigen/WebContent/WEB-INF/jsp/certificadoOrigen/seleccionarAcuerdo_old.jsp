<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema COMPONENTE ORIGEN - Seleccionar Acuerdo</title>
<meta http-equiv="Pragma" content="no-cache" />
</head>

<script language="JavaScript">

		$(document).ready(function() {
		    
		    /*var bloqueado = document.getElementById("bloqueado").value;
    	    if (bloqueado == "S") {
    	    	bloquearControles();
    	    }*/
    	    
		});

	    function crearOrden(keyValues, keyValuesField) {
	        var f = document.formulario;
	        var idFormato = document.getElementById("idFormato").value;
            var idEntidad = document.getElementById("idEntidad").value;
            var idTupa = document.getElementById("idTupa").value;
            var formato = document.getElementById("formato").value;
	        f.action = contextName+"/origen.htm?method=nuevaOrden&idFormato="+idFormato+"&idEntidad="+idEntidad+"&idTupa="+idTupa+"&formato="+formato;
	        //f.button.value = 'nuevaOrdenButton';
	        f.submit();
	    }
    </script>
	<body>
		<div id="body"><jsp:include page="/WEB-INF/jsp/header.jsp" />
		<div id="contp">
		<div id="cont">
		<form name="formulario" method="post" >
			<jlis:value type="hidden" name="method" />
			<!--<jlis:value type="hidden" name="button" />-->
		    <jlis:value name="idTupa" type="hidden" />
		    <jlis:value name="idEntidad" type="hidden" />
		    <jlis:value name="idFormato" type="hidden" />
		    <jlis:value name="formato" type="hidden" />
			
			<!-- No lo usamos porque contiene demasiada l�gica que al momento no se est� seguro si se utilizar� -->
			<%--<jsp:include page="/WEB-INF/jsp/informacionGeneral.jsp" /> --%>
			
			<div>
				<p align="center" >
					<table width="40%">
						<tr>
							<td align="left" width="30%" colspan="2" style="font-weight: bold; " >Seleccione un Acuerdo:</td>
						</tr>
						<tr>
							<td align="left" width="30%" colspan="2" >&nbsp;</td>
						</tr>
						<tr>
							<td align="left" width="30%" ><jlis:label key="co.label.acuerdo" style="font-weight: bold; "/></td>
							<td align="right" width="70%" ><jlis:selectProperty key="comun.acuerdo.select" name="acuerdo" style="align:right;" /></td>		
						</tr>
						<tr>
							<td align="left" width="30%" colspan="2" >&nbsp;</td>
						</tr>
						<tr>
							<td align="left" width="30%" ><jlis:label key="co.label.pais_acuerdo" style="font-weight: bold; align:left;"/></td>
							<td align="right" width="70%" ><jlis:selectProperty key="certificadoOrigen.paises_acuerdo.select" name="pais" style="align:right;" /></td>		
						</tr>
						<tr>
							<td align="left" width="30%" colspan="2" >&nbsp;</td>
						</tr>
						<tr>
							<td align="left" width="30%" ><jlis:label key="co.label.entidad_certificadora" style="font-weight: bold; align:left;"/></td>
							<td align="right" width="70%" ><jlis:selectProperty key="comun.entidad.select" name="entidad" style="align:right;" /></td>		
						</tr>
						<tr>
							<td align="left" width="30%" colspan="2" >&nbsp;</td>
						</tr>
						<tr>
							<td align="left" width="30%" ><jlis:button id="regresarButton" editable="always" name="listarCertificadoOrigen()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para regresar" /></td>
							<td align="right" width="70%" ><jlis:button id="nuevaButton" code="CO.USUARIO.OPERACION" name="crearOrden()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Siguiente" alt="Pulse aqu� para crear un nuevo Certificado de Origen" /></td>		
						</tr>
					</table>
				</p>
			</div>
		</form>
		</div>
		</div>
		<jsp:include page="/WEB-INF/jsp/footer.jsp" />
		</div>
	</body>
</html>