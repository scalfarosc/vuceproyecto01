<%
	String controller = (String) request.getAttribute("controller");
%>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script>

		$(document).ready(function() {
				tituloPopUp("Rechazo de DJ");
			    //validaCambios();
		});

	 	// Funci�n general de validaciones para los popups
		function validarSubmit() {
		    var f = document.formulario;

		    if (f.button.value=="cancelarButton") {
	            return false;
	        }
		    if (f.button.value=="cerrarPopUpButton") {
	        	return validaCerraPopUp();
	        }
		    return true;
		}

	 	// Resolver la Validaci�n de Declaraci�n Jurada
		function confirmarRechazoDJ(respuesta) {
			var f = document.formulario;
			//alert(f.formato.value);
	        if(confirm("Esta seguro que desea rechazar la declaraci\u00f3n jurada?")){
	        	f.action = contextName+"/origen.htm";
		        f.method.value="rechazarDeclaracionJurada";
		        f.button.value="rechazarDJButton";
		        //f.target = window.parent.parent.name;
		        if (window.parent.parent.name == 'undefined') {
		        	f.target = window.parent.name;
		        } else {
		        	f.target = window.parent.parent.parent.name;
		        }

			} else {
				f.button.value="cancelarButton";
			}
	    }

	 	// Resolver la Validaci�n de Declaraci�n Jurada
		function confirmarRechazoTransmiteDJ(respuesta) {
			var f = document.formulario;
			//alert(f.formato.value);
	        if(confirm("Con esta acci�n usted resolver� la solicitud de calificaci�n de declaraci�n jurada, generar� y transmitir� el documento resolutivo.\n �Est� seguro que desea proceder?")){
	        	f.action = contextName+"/origen.htm";
		        f.method.value="rechazarTransmitirDeclaracionJurada";
		        f.button.value="rechazarDJButton";
		        //f.target = window.parent.parent.name;
		        if (window.parent.parent.name == 'undefined') {
		        	f.target = window.parent.name;
		        } else {
		        	f.target = window.parent.parent.parent.name;
		        }

			} else {
				f.button.value="cancelarButton";
			}
	    }
	 	
	 	
	 	
			 	

	</script>

	<body id="contModal">
  	    <!-- <jlis:modalWindow onClose="actualizar()" /> -->
	    <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
	        <jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <jlis:value name="calificacionUoId" type="hidden" />
	        <jlis:value name="djCalificada" type="hidden" value="S" />            
			
			<jlis:button code="CO.ENTIDAD.EVALUADOR" id="confirmaButton" name="confirmarRechazoTransmiteDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Rechazar, Denegar y Transmitir" alt="Pulse aqu� para confirmar el rechazo, la denegaci�n y la denegaci�n de la declaraci�n jurada" />
			<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />

			<br/>

			<table class="form">
				<tr>
					<td width="15%"><jlis:label key="co.label.dj.detalle.denegacion" /></td>
					<td><jlis:textArea rows="2" editable="yes" name="observacion" style="width:92%" /></td>
				</tr>
			</table>

		</form>
	</body>
</html>