<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript">

        function validarSubmit() {
            var f=document.formulario;
            if (f.button.value=="cerrarPopUpButton") {
            	return validaCerraPopUpNoAction();
            }
            return true;
        }

        function verOrden(keyValues, keyValuesField) {
             var f = document.formulario;
             var orden = document.getElementById(keyValues.split("|")[0]).value;
             var mto = document.getElementById(keyValues.split("|")[1]).value;
             var formato = document.getElementById(keyValues.split("|")[2]).value;
             var frmlow = formato.toLowerCase();
             f.action = contextName+"/origen.htm?opcion=O";
             f.method.value = "cargarInformacionOrden";
             f.orden.value = orden;
             f.mto.value = mto;
             f.target = window.parent.name;
             f.submit();
        }

        function verSuce(keyValues, keyValuesField) {
            var f = document.formulario;
            var orden = document.getElementById(keyValues.split("|")[0]).value;
            var mto = document.getElementById(keyValues.split("|")[1]).value;
            var formato = document.getElementById(keyValues.split("|")[2]).value;
            var frmlow = formato.toLowerCase();
            f.action = contextName+"/"+frmlow+".htm?opcion=S";
            f.method.value = "cargarInformacionOrden";
            f.orden.value = orden;
            f.mto.value = mto;
            f.target = window.parent.name;
            f.submit();
       }

</script>

<body id="contModal">
    <jlis:messageArea width="100%" /><br/>
    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
        <jlis:value name="orden" type="hidden" />
        <jlis:value name="mto" type="hidden" />
    	<table>
             <tr>
             	<td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" /></td>
             </tr>
        </table>
        <div class="block btabs btable"><div class="blocka"><div class="blockb">
        <c:if test="${empty mostrarJerarquia || mostrarJerarquia=='N'}">
        	<b/>
        	<span class="requiredValueClass">
        		No se ha encontrado informaci�n a mostrar
        	</span>
        </c:if>
        <ul id="maintab" class="tabs">
            <c:if test="${!empty mostrarJerarquia && mostrarJerarquia=='S'}">
            <li><a href="#" rel="tabJerarquia"><span>Jerarqu�a del Tr�mite</span></a></li>
            </c:if>
            <li><a href="#" rel="tabTrazabilidad"><span>Trazabilidad del Tr�mite</span></a></li>
        </ul>
        <c:if test="${!empty mostrarJerarquia && mostrarJerarquia=='S'}">
        <div id="tabJerarquia" class="tabcontent">
            <c:if test="${empty noHayJerarquia || noHayJerarquia=='S'}">
            No existen otros tr�mites asociados
            </c:if>
            <c:if test="${empty noHayJerarquia || noHayJerarquia!='S'}">
            <jlis:table name="JERARQUIA" keyValueColumns="ORDEN_ID,MTO,FORMATO" source="tJerarquia" scope="request" pageSize="*" navigationHeader="no" width="95%" >
                  <jlis:tr>
                        <jlis:td name="ORDEN ID" nowrap="yes" />
                        <jlis:td name="MTO" nowrap="yes" />
                        <jlis:td name="SOLICITUD" nowrap="yes" width="5%"/>
                        <jlis:td name="SUCE" nowrap="yes" width="5%"/>
                        <jlis:td name="DR" nowrap="yes" width="5%"/>
                        <jlis:td name="DR ORIGEN" nowrap="yes" width="5%"/>
                        <jlis:td name="TUPA" nowrap="yes" width="5%"/>
                        <jlis:td name="FORMATO" nowrap="yes" width="10%" />
                        <jlis:td name="EXPEDIENTE" nowrap="yes" width="5%"/>
                        <jlis:td name="DR ENTIDAD" nowrap="yes" width="5%"/>
                        <jlis:td name="FECHA SUCE" nowrap="yes" width="5%"/>
                        <jlis:td name="FECHA DR" nowrap="yes" width="5%"/>
                  </jlis:tr>
                    <jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
                    <jlis:columnStyle column="2" columnName="MTO" hide="yes" />
                    <jlis:columnStyle column="3" columnName="ORDEN" align="center" />
                    <jlis:columnStyle column="4" columnName="SUCE" align="center" />
                    <jlis:columnStyle column="5" columnName="DR" align="center" />
                    <jlis:columnStyle column="6" columnName="DR_ORIGEN" align="center" />
                    <jlis:columnStyle column="7" columnName="TUPA" align="center" />
                    <jlis:columnStyle column="8" columnName="FORMATO" align="center" />
                    <jlis:columnStyle column="9" columnName="EXPEDIENTE_ENTIDAD" align="center" hide="yes" />
                    <jlis:columnStyle column="10" columnName="DR_ENTIDAD" align="center" />
                    <jlis:columnStyle column="11" columnName="FECHA_SUCE" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
                    <jlis:columnStyle column="12" columnName="FECHA_DR" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
                    <%-- <jlis:tableButton column="3" type="link" onClick="verOrden" />
                    <jlis:tableButton column="4" type="link" onClick="verSuce" /> --%>
            </jlis:table>
            </c:if>
        </div>
        </c:if>
        <div id="tabTrazabilidad" class="tabcontent">

         <jlis:table keyValueColumns="traza_tce_id" name="trazas" source="listadoTrazabilidad" scope="request" pageSize="*" cellPadding="2" navigationHeader="no" >
            <jlis:tr>
            	<jlis:td name="${nombreColumna}" nowrap="yes" width="3%"/>
            	<jlis:td name="FECHA" nowrap="yes" width="5%"/>
		    	<jlis:td name="SECUENCIA" nowrap="yes" width="5%"/>
            	<jlis:td name="ETAPA" nowrap="yes" width="20%"/>
                <jlis:td name="DESCRIPCI�N DETALLADA" nowrap="yes" width="50%"/>
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="orden" align="center" hide="yes"/>
            <jlis:columnStyle column="2" columnName="fecha" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" nowrap="yes"/>
            <jlis:columnStyle column="3" columnName="secuencia" hide="yes" align="center"/>
            <jlis:columnStyle column="4" columnName="etapa" />
            <jlis:columnStyle column="5" columnName="descripcion_detallada" />
         </jlis:table>
         
        </div>
        </div></div></div>
    </form>
    <script>
        initializetabcontent("maintab");
        window.parent.document.getElementById("popupTitle").innerHTML = "Informaci�n Adicional del Tr�mite";
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";

    </script>
</body>
</html>