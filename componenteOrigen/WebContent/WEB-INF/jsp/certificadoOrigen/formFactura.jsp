<%
	String controller = (String) request.getAttribute("controller");
%>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/acuerdos/acuerdo${idAcuerdo}/solicitud_co_acuerdo_${idAcuerdoVersion}.js"></script>
	<script>

		$(document).ready(function() {
			tituloPopUp("Factura");
		    validaCambios();
		    var bloqueado = document.getElementById("bloqueado").value;
    	    if (bloqueado == "S") {
    	    	bloquearControles();
    	    }

    	    document.getElementById("CO_FACTURA.indicadorTercerPais").value = '${requestScope["CO_FACTURA.indicadorTercerPais"]}';
    	    document.getElementById("CO_FACTURA.tieneFacturaTercerPais").value = '${requestScope["CO_FACTURA.tieneFacturaTercerPais"]}';
    	    //alert(document.getElementById("CO_FACTURA.indicadorTercerPais").value);

    	    // Estas inicializaciones son necesarias pero para todos los acuerdos
    	    if ( document.getElementById("CO_FACTURA.indicadorTercerPais").value == '' ) {
    	    	document.getElementById("CO_FACTURA.indicadorTercerPais").value = 'N';
    	    }
    	    if ( document.getElementById("CO_FACTURA.tieneFacturaTercerPais").value == '' ) {
    	    	document.getElementById("CO_FACTURA.tieneFacturaTercerPais").value = 'N';
    	    }

    	    //alert(document.getElementById("CO_FACTURA.indicadorTercerPais").value);
    	    //alert('${requestScope["CO_FACTURA.indicadorTercerPais"]}');

    	    // Inicializa controles de la factura seg�n el acuerdo
    	    inicializacionFacturaSolicitudSegunAcuerdo();

	    });

	 	// Funci�n general de validaciones para los popups
		function validarSubmit() {
		    var f = document.formulario;

		    if (f.button.value=="cancelarButton") {
	            return false;
	        }
		    if (f.button.value=="cerrarPopUpButton") {
	        	return validaCerraPopUp();
	        }
	        if (f.button.value=="eliminarButton") {
	            disableButton("eliminarAdjButton", true);
	            disableButton("eliminarAdjBButton", true);
	        }

		    return true;
		}

	 	// Graba la Factura
		function grabarRegistro() {
			var f = document.formulario;
	        if (validarCamposFacturaSolicitudCertificado() &&
	        	js_ContieneNumero(document.getElementById("CO_FACTURA.numero"), "co.label.factura.numero", "La factura debe tener por lo menos un n�mero")) {
	        	f.action = contextName+"/origen.htm";
		        f.method.value = "grabarCOFactura";
		        f.button.value = "grabarButton";

			} else {
				f.button.value="cancelarButton";
			}
	    }

	 	// Eliminar la Factura
		function eliminarRegistro() {
			var f = document.formulario;
	        if (!confirm("�Est� seguro de eliminar este registro?")) {
				f.button.value="cancelarButton";
	        } else{
		        f.action = contextName+"/origen.htm";
		        f.method.value = "eliminarCOFactura";
		        f.button.value = "eliminarButton";
		        f.target = window.parent.name;
	        }
	    }

	 	// Valida que la fechad de la Factura no sea posterior a la fecha actual
		function validarFechaFactura(obj){
		    var hoy = new Date();
		    var cad = obj.value;
		    var elem = cad.split('/');
            var fechaFac = new Date(elem[2],elem[1]-1,elem[0]);
            var fechaHoy = new Date(hoy.getFullYear(),hoy.getMonth(), hoy.getDate());
		    if (fechaFac > fechaHoy){
		        alert("La fecha de la Factura debe ser anterior o igual a la fecha actual");
				obj.value="";
		    }
		}

        function cargarArchivo() {
            var f = document.formulario;
            var fA = document.formularioAdjuntos;
            if(fA.archivo.value !=""){
                fA.coId.value = document.getElementById("CO_FACTURA.coId").value;
                fA.ordenId.value = f.orden.value;
                fA.mtoAdj.value = f.mto.value;
                fA.formatoId.value = f.idFormato.value;
                fA.formatoAdj.value = f.formato.value;
                fA.bloqueadoAdj.value = f.bloqueado.value;
                fA.secuenciaFactura.value = f.secuencia.value;
                fA.idAcuerdoAdj.value = f.idAcuerdo.value;
                fA.action = contextName+"/origen.htm";
                fA.method.value="cargarAdjuntoFactura";
                f.button.value="cargarAdjButton";
            }else {
                alert("Debe seleccionar alg�n archivo");
                f.button.value="cancelarButton";
            }
        }

        function eliminarArchivo() {
        	var f = document.formulario;
            var indice = -1;
            if (f.seleccione!=null) {
                if (eval(f.seleccione.length)) {
                    for (i=0; i < f.seleccione.length; i++) {
                        if (f.seleccione[i].checked){
                            indice = i;
                        }
                    }
                } else {
                    if (f.seleccione.checked) {
                        indice = 0;
                    }
                }
            }
            if (indice!=-1) {
                f.action = contextName+"/origen.htm";
                f.method.value="eliminarAdjuntoFactura";
                f.button.value="eliminarAdjButton";
            } else {
                alert('Seleccione por lo menos un registro a eliminar');
                f.button.value = 'cancelarButton';
            }
        }

        // Descargar archivos adjuntos
        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/origen.htm";
            f.method.value = "descargarAdjunto";
            f.idAdjunto.value = adjuntoId;
            f.submit();
        }

        function validarSubmitFormularioAdjuntos() {
            var f = document.formularioAdjuntos;

            if (f.button.value == 'cancelarButton') {
            	return false;
            } else {
	            if (f.button.value=="cargarButton") {
	                disableButton("cargarButton", true);
	                disableButton("eliminarButton", true);
	            }
	            if (f.button.value=="eliminarButton") {
	                disableButton("cargarButton", true);
	                disableButton("eliminarButton", true);
	            }
	            return true;
            }
        }

	</script>

	<body id="contModal">
		<%@include file="/WEB-INF/jsp/helpHint.jsp" %>
	    <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="secuencia" type="hidden" />
	        <jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <jlis:value name="idFormato" type="hidden" />
	        <jlis:value name="CO_FACTURA.coId" type="hidden" />
	        <jlis:value name="bloqueado" type="hidden" />
	        <jlis:value name="idAcuerdo" type="hidden" />
            <jlis:value name="idAdjunto" type="hidden" />
            <jlis:value name="faltaAdjunto" type="hidden" />
            <!-- 20150610_RPC: Acta.33: Mostrar Factura Acuerdos AELC/UE -->
            <jlis:value name="CO_FACTURA.mostrarFactura" type="hidden" />

			<co:validacionBotonesFormato formato="MCT001" pagina="formFactura" />

			<table class="form">

				<tr>
					<td colspan="2" >
						<jlis:button code="CO.USUARIO.OPERACION" id="grabarButton" name="grabarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar Factura" alt="Pulse aqu� para grabar la factura " />
					    <jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Factura" alt="Pulse aqu� para eliminar la factura " />


	                	<c:if test="${!empty tAdjuntosFactura}">
							<jlis:button code="CO.USUARIO.OPERACION" id="eliminarAdjButton" name="eliminarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Adjunto" alt="Pulse aqu� para eliminar los archivos seleccionados" />
		  				</c:if>


						<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
					</td>
				</tr>

				<tr>
					<td colspan="2" ><hr/></td>
				</tr>
				<tr class="trOperadorTercerPais" style="display:none" >
					<th colspan="2" >
						<jlis:label key="co.label.factura.tercer_pais" />&nbsp;
						<%-- <jlis:value type="checkbox" name="CO_FACTURA.indicadorTercerPais" checkValue="S" onChange="manejaNombreOperador(this);" />&nbsp; --%>
						<jlis:selectSource name="CO_FACTURA.indicadorTercerPais" source="listaSiNo" scope="request" onChange="manejaNombreOperador(this);" />&nbsp;
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.SUCE.FACTURA.IND_TERCER_PAIS" />
					</th>
				</tr>
				
				<tr class="trPaisOperadorTercerPais" style="display:none"><!-- JMC 28/04/2017 Alianza -->
					<th><jlis:label key="co.label.factura.pais_operador" /></th>
					<td >
						<jlis:selectProperty name="CO_FACTURA.paisOperadorTercerPais" key="comun.pais_iso_alfa2.select"/><span id="spanSelectPaisOperadorTercerPais"  class="requiredValueClass">(*)</span>
					</td>
				</tr>   
				
				<tr class="trNombreOperadorTercerPais" style="display:none" >
					<th><jlis:label key="co.label.factura.nombre_operador" /></th>
					<td >
						<jlis:textArea name="CO_FACTURA.nombreOperadorTercerPais" rows="4" cols="35" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);"/><span id="spanNombreOperador" class="requiredValueClass">(*)</span>&nbsp;
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.SUCE.FACTURA.NOMBRE_TERCER_PAIS" />
					</td>
				</tr>
				<tr class="trDireccionOperadorTercerPais" style="display:none" >
					<th><jlis:label key="co.label.factura.direccion_operador" /></th>
					<td >
						<jlis:textArea name="CO_FACTURA.direccionOperadorTercerPais" rows="4" cols="35" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);"/><span id="spanDireccionOperador" class="requiredValueClass">(*)</span>
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.SUCE.FACTURA.DIRECCION_TERCER_PAIS" />
					</td>
				</tr>
				<tr class="trTieneFacturaOperadorTercerPais" style="display:none" >
					<th colspan="2">
						<jlis:label key="co.label.factura.tiene_factura_operador" />&nbsp;
						<%-- <jlis:value type="checkbox" name="CO_FACTURA.tieneFacturaTercerPais" checkValue="S" /> --%>
						<jlis:selectSource name="CO_FACTURA.tieneFacturaTercerPais" source="listaSiNo" scope="request" />
						<span id="spanTieneFacturaOperador" style="display:none" class="requiredValueClass">(*)</span>&nbsp;
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.SUCE.FACTURA.TIENE_FACT_TERCER_PAIS" />
					</th>
				</tr>
				<tr>
					<th width="45%" ><jlis:label key="co.label.factura.numero" /></th>
					<td width="55%" ><jlis:value name="CO_FACTURA.numero" size="30" maxLength="30" type="text"/><span class="requiredValueClass">(*)</span></td>
				</tr>
				<!-- 20150610_RPC: Acta.33: Mostrar Factura Acuerdos AELC/UE -->
				<tr class="trMostrarFactura" style="display:none">
					<th><jlis:label key="co.label.factura.mostrar_factura" /></th>
					<td><jlis:value type="checkbox" name="mostrarFactura" onClick="document.getElementById('CO_FACTURA.mostrarFactura').value = this.checked ? 'S' : 'N'" /></td>
				</tr>
				<tr>
					<th width="45%" ><jlis:label key="co.label.factura.fecha" /></th>
					<td width="55%" ><jlis:date form="formulario" name="CO_FACTURA.fecha" onChange="validarFechaFactura(this)" /><span class="requiredValueClass">(*)</span></td>
				</tr>

			</table>
        <!-- <form name="formularioAdjuntos" method="post" enctype="multipart/form-data" onSubmit="return validarSubmitFormularioAdjuntos();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="coId" type="hidden" />
            <jlis:value name="ordenId" type="hidden" />
           	<jlis:value name="mtoAdj" type="hidden" />
        	<jlis:value name="formatoAdj" type="hidden" />
            <jlis:value name="formatoId" type="hidden" />
        	<jlis:value name="bloqueadoAdj" type="hidden" />
        	<jlis:value name="secuenciaFactura" type="hidden" />
        	<jlis:value name="idAcuerdoAdj" type="hidden" /> -->


            <table>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"><span class="requiredValueClass"><jlis:label key="co.label.factura.adjunto" /></span></td>
				</tr>
            </table>
            <c:if test="${empty tAdjuntosFactura}">
	            <table>
	                <tr>
	                    <td>
	                    <jlis:label key="co.label.documentos_adjuntar"/> <br />
	                    <jlis:label key="co.label.tipo_archivos"/>
	                    </td>
	                </tr>
	                <tr>
	                    <td>
	                     <input type="file" id="archivo" name="archivo" size="75"  />
	                    </td>
	                </tr>
	            </table>
	        </c:if>
	        <br/>

	        <jlis:value name="ADJ_ELIM.djId" type="hidden" />
	        <jlis:value name="ADJ_ELIM.secuenciaMaterial" type="hidden" />
	        <jlis:value name="ADJ_ELIM.adjuntoId" type="hidden" />

	        <jlis:table  name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntosFactura" scope="request"
	            pageSize="*" navigationHeader="no" width="98%" >
	            <jlis:tr>
	                <jlis:td name="ADJUNTO ID" nowrap="yes" />
	                <jlis:td name="ADJUNTOTIPO" nowrap="yes" />
	                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" width="83%" />
              	 		<jlis:td name="TAMA�O (KB)" nowrap="yes" width="7%" />
	              	<jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
	            </jlis:tr>
	            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes"/>
	            <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
	            <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes" />
           		<jlis:columnStyle column="4" columnName="TAMANO" />
	            <jlis:columnStyle column="5" columnName="SELECCIONE" align="center" editable="yes" />
	            <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
	            <jlis:tableButton column="5" type="checkbox" name="seleccione" />
	        </jlis:table>

        </form>
	</body>
</html>