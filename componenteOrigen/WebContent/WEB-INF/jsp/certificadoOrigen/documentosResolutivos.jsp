<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema CO - Principal</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <c:set var="activeDR" value="active" scope="request" />
    <script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
    <script language="JavaScript">
        function validarSubmit() {
            var f = document.formulario;
            return true;
        }

        function buscar() {
            var f = document.formulario;
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarListaDRs";
            f.target = "_self";
            f.button.value = "buscarButton";
        }

        function seleccionSoloPendientes() {
            var f = document.formulario;
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarListaDRs";
            f.target = "_self";
            f.button.value = "buscarButton";
            f.submit();
        }

        function verDr(keyValues, keyValuesField) {
            var f = document.formulario;
            var drId = document.getElementById(keyValues.split("|")[5]).value;
            var sdr = document.getElementById(keyValues.split("|")[6]).value;
            var formato = document.getElementById(keyValues.split("|")[2]).value;
            var numSuce = document.getElementById(keyValues.split("|")[4]).value;
            var tupaId = document.getElementById(keyValues.split("|")[7]).value;
            var ordenId = document.getElementById(keyValues.split("|")[0]).value;
            var mto = document.getElementById(keyValues.split("|")[1]).value;
            showPopWin(contextName+"/origen.htm?method=cargarDatosDocumentoResolutivo&drId="+drId+"&sdr="+sdr+"&numSuce="+
            		numSuce+"&ordenId="+ordenId+"&mto="+mto+"&tupaId="+tupaId+"&formato="+formato, 900, 550, null);
        }

		function verCertificado(keyValues, keyValuesField){
            var f = document.formulario;
            f.orden.value = document.getElementById(keyValues.split("|")[0]).value;
            f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
            f.formato.value = document.getElementById(keyValues.split("|")[2]).value;
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.submit();
	    }
		
		function descargarAdjuntoDR(keyValues, keyValuesField){
			var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[8]).value;
            f.action = contextName+"/origen.htm";
            f.method.value="descargarAdjunto";
            f.idAdjunto.value=adjuntoId;
            f.submit();
		}
		
        function actualizar() {
            var f = document.formulario;
            f.action = contextName + "/origen.htm";
            f.method.value = "cargarListaDRs";
            f.target = "_self";
            f.submit();
        }
        
		function seleccionOpcion(obj) {
			var f = document.formulario;
			if (obj.value=="O") {
				f.numeroSUCE.value = "";
				f.numeroCO.value = "";
			} else if(obj.value=="S") {
				f.numeroOrden.value = "";
				f.numeroCO.value = "";
			} else if (obj.value=="C") {
				f.numeroOrden.value = "";
				f.numeroSUCE.value = "";
			} 
		}
		
		function seleccionNumeroOrden() {
			var f = document.formulario;
			f.numeroSUCE.value = "";
			if(document.getElementById("numeroCO")) f.numeroCO.value = "";
			document.getElementById("opcionFiltro_O").checked = true;
		}

		function seleccionNumeroSUCE() {
			var f = document.formulario;
			f.numeroOrden.value = "";
			if(document.getElementById("numeroCO")) f.numeroCO.value = "";
			document.getElementById("opcionFiltro_S").checked = true;
		}
		
		function seleccionNumeroCO() {
			var f = document.formulario;
			f.numeroOrden.value = "";
			f.numeroSUCE.value = "";
			document.getElementById("opcionFiltro_C").checked = true;
		}
       

    </script>
    <body >
        <div id="body">
            <jsp:include page="/WEB-INF/jsp/header.jsp" />
            <!-- CONTENT -->
        	<div id="contp"><div id="cont">
                <jlis:modalWindow onClose="actualizar()"/>
                <form name="formulario" method="post" onsubmit="return validarSubmit();" >
                    <input type="hidden" name="method" />
            	    <input type="hidden" name="button" />
                    <input type="hidden" name="tipo" />
                    <input type="hidden" name="numero" />
                    <input type="hidden" name="orden" />
                    <input type="hidden" name="mto" />
                    <input type="hidden" name="formato" />
                    <input type="hidden" name="rutaopcion" />
                    <input type="hidden" name="pantallaDocumentosResolutivos" />
                    <input type="hidden" name="idAdjunto" />
            	    <div id="pageTitle" style="text-align:left;">
            		<h1><strong><jlis:label labelClass="pageTitle"  key="co.title.drs" /></strong></h1>
            		</div>
                    
		            <br/>
		            <p align="left">
		            	<jlis:label key="co.label.acuerdo" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<%-- 20170131_GBT ACTA CO-004-16 Y CO-009-16--%>
		            	<%-- jlis:selectProperty key="comun.acuerdo.select" name="filtroAcuerdo" style="defaultElement=2" onChange="actualizar()" />&nbsp;&nbsp;--%>
		            	<jlis:selectProperty key="comun.acuerdo.todos.select" name="filtroAcuerdo" style="defaultElement=2" onChange="actualizar()" />&nbsp;&nbsp;
		            	<jlis:label key="co.label.filtro.entidad" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.entidad.select" name="filtroEntidad" style="defaultElement=2,width=250px" onChange="actualizar()" />&nbsp;&nbsp;
					</p>
                    
		            <p align="left">
		            	<jlis:label key="co.label.formato" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.entidad.formatosxEntidad.select" name="filtroFormato" style="defaultElement=2" onChange="actualizar()" />&nbsp;&nbsp;
		                <%--jlis:label key="co.label.estado" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.estadosCO.select" name="filtroEstado" style="defaultElement=no,width=250px" />&nbsp;&nbsp;--%>

						<jlis:value type="radio" name="opcionFiltro" checkValue="O" onClick="seleccionOpcion(this)"/><jlis:label key="co.label.solicitud" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroOrden" size="8" onClick="seleccionNumeroOrden()" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="S" onClick="seleccionOpcion(this)"/><jlis:label key="co.label.suce" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroSUCE" size="8" onClick="seleccionNumeroSUCE()" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="C" onClick="seleccionOpcion(this)"/><jlis:label key="co.label.nco" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroCO" size="15" onClick="seleccionNumeroCO()"  />&nbsp;&nbsp;
						<jlis:button id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Pulse aqu� para buscar" />
					</p>

		            <br/>
					<%--HELPDESK2--%>
                    <div class="block btabs btable"><div class="blocka"><div class="blockb">
                    <ul id="maintab" class="tabs">
                        <li class="selected"><a href="#" rel="tabDRs" ><span>DRs</span></a></li>
                        <li id="idTabSolicitudesRectificacionDR"><a href="#" rel="tabSolicitudesRectificacionDR"><span>Solicitudes de Rectificaci�n de DR en Proceso</span></a></li>
                    </ul>
                    <div id="tabDRs" class="tabcontent">
                        <jlis:table keyValueColumns="ORDEN_ID,MTO,FORMATO,ORDEN,SUCE,DR_ID,SDR,TUPA,ADJUNTO_ID" name="DRs" key="formato.drs.grilla" filter="${filterDRs}" pageSize="15" width="98%"
                            validator="pe.gob.mincetur.vuce.co.web.util.DocumentosResolutivosSUCEResolutorRowValidator" >
            	            <jlis:tr>
            	                <jlis:td name="ORDEN ID" nowrap="yes" />
            	                <jlis:td name="MTO" nowrap="yes" />
            	                <jlis:td name="FORMATO_ID" nowrap="yes" />
            	                <jlis:td name="TUPA" nowrap="yes" width="5%"/>
            	                <jlis:td name="FORMATO" nowrap="yes" width="5%"/>
            	                <jlis:td name="NOMBRE" nowrap="yes" width="5%"/>
            	                <jlis:td name="ACUERDO INTERNACIONAL" nowrap="yes" width="18%"/>
            	                <jlis:td name="ENTIDAD CERTIFICADORA" nowrap="yes" width="20%"/>
            	                <jlis:td name="PA�S" nowrap="yes" width="8%"/>
            	                <jlis:td name="SOLICITUD" nowrap="yes" width="5%"/>
                                <jlis:td name="SUCE" nowrap="yes" width="5%"/>
                                <jlis:td name="EXPEDIENTE" nowrap="yes" width="5%"/>
                                <jlis:td name="DR" nowrap="yes" width="5%"/>
                                <jlis:td name="CO" nowrap="yes" width="10%"/>
                                <jlis:td name="TIPO" nowrap="yes" width="5%"/>
            	                <jlis:td name="REGISTRO" nowrap="yes" width="8%"/>
                                <jlis:td name="DR_ID" nowrap="yes" />
                                <jlis:td name="SDR" nowrap="yes" />
                                <jlis:td name="ANULADO" nowrap="yes" />
                                <jlis:td name="ENTREGADO" nowrap="yes" />
                                <jlis:td name="VER LINK SUCE" nowrap="yes" />
                                <jlis:td name="ADJUNTO" nowrap="yes" />
                                <jlis:td name="ADJUNTO_ID" nowrap="yes" />
            	            </jlis:tr>
            	            <jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
            	            <jlis:columnStyle column="2" columnName="MTO" hide="yes"/>
            	            <jlis:columnStyle column="3" columnName="FORMATO_ID" hide="yes"/>
            	            <jlis:columnStyle column="4" columnName="TUPA" nowrap="yes" />
            	            <jlis:columnStyle column="5" columnName="FORMATO" />
            	            <jlis:columnStyle column="6" columnName="NOMBRE_FORMATO" />
            	            <jlis:columnStyle column="7" columnName="NOMBRE_ACUERDO_INTERNACIONAL" />
            	            <jlis:columnStyle column="8" columnName="NOMBRE_ENTIDAD_UO" />
            	            <jlis:columnStyle column="9" columnName="NOMBRE_PAIS" />
            	            <jlis:columnStyle column="10" columnName="ORDEN" align="center" />
            	            <jlis:columnStyle column="11" columnName="SUCE" align="center" editable="yes" />
                            <jlis:columnStyle column="12" columnName="EXPEDIENTE_ENTIDAD" hide="yes" />
                            <jlis:columnStyle column="13" columnName="DR" align="center" editable="yes"/>
                            <jlis:columnStyle column="14" columnName="CERTIFICADO" align="center" />
                            <jlis:columnStyle column="15" columnName="TIPODR" align="center" />
            	            <jlis:columnStyle column="16" columnName="FECHA_REGISTRO" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
                            <jlis:columnStyle column="17" columnName="DR_ID" hide="yes" />
                            <jlis:columnStyle column="18" columnName="SDR" hide="yes" />
                            <jlis:columnStyle column="19" columnName="ANULADO" hide="yes" />
                            <jlis:columnStyle column="20" columnName="ENTREGADO" hide="yes" />
                            <jlis:columnStyle column="21" columnName="VISIBLE_LINK_SUCE" hide="yes" />
                            <jlis:columnStyle column="22" columnName="CON_ADJUNTO" editable="yes" align="center" validator="pe.gob.mincetur.vuce.co.web.util.DocumentosResolutivosSUCECellValidator" />
                            <jlis:columnStyle column="23" columnName="ADJUNTO_ID" hide="yes" />
            	            <%--jlis:tableButton column="9" type="link" onClick="verOrden" /--%>
            	            <jlis:tableButton column="11" type="link" onClick="verCertificado" />
                            <jlis:tableButton column="13" type="link" onClick="verDr" />
                            <jlis:tableButton column="22" type="imageButton" onClick="descargarAdjuntoDR" />
            	        </jlis:table>
                        <br/>
                        <table style="margin: 0 auto; width: 96%"><tr><td class="labelClass" nowrap>Leyenda:&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="codigoGrisClass" style="border: 1px solid black;" width="10px" nowrap>&nbsp;&nbsp;</td><td class="labelClass" nowrap>&nbsp;ANULADO</td><td width="95%"></td></tr></table>
                    </div>
                    <div id="tabSolicitudesRectificacionDR" class="tabcontent">
                        <table class="form">
                            <tr>
                                <td>
                                    <jlis:value type="checkbox" name="soloPendientes" checkValue="S" defaultCheck="S" onClick="seleccionSoloPendientes()" />
                                    <label style="font-size:14px;font-weight:bold;">&nbsp;Mostrar s�lo Pendientes</label>
                                </td>
                            </tr>
                        </table>
                        <jlis:table name="MODIFICACION_DR" pageSize="15" keyValueColumns="ORDEN_ID,MTO,FORMATO,ORDEN,SUCE,DR_ID,SDR,TUPA"
                                    key="formato.solicitudesRectificacionDR.grilla" filter="${filterSolicitudesRectificacionDR}" width="100%">
                            <jlis:tr>
            	                <jlis:td name="ORDEN ID" nowrap="yes" />
            	                <jlis:td name="MTO" nowrap="yes" />
            	                <jlis:td name="FORMATO_ID" nowrap="yes" />
            	                <jlis:td name="TUPA" nowrap="yes" width="5%"/>
            	                <jlis:td name="FORMATO" nowrap="yes" width="5%"/>
            	                <jlis:td name="NOMBRE" nowrap="yes" width="5%"/>
            	                <jlis:td name="ACUERDO INTERNACIONAL" nowrap="yes" width="10%"/>
            	                <jlis:td name="ENTIDAD CERTIFICADORA" nowrap="yes" width="10%"/>
            	                <jlis:td name="PA�S" nowrap="yes" width="8%"/>
            	                <jlis:td name="SOLICITUD" nowrap="yes" width="5%"/>
                                <jlis:td name="SUCE" nowrap="yes" width="5%"/>
                                <jlis:td name="EXPEDIENTE" nowrap="yes" width="5%"/>
                                <jlis:td name="CO" nowrap="yes" width="10%"/>
                                <jlis:td name="DR" nowrap="yes" width="5%"/>
                                <jlis:td name="TIPO" nowrap="yes" width="5%"/>
                                <jlis:td name="MENSAJE_ID" nowrap="yes" width="8%"/>
            	                <jlis:td name="MENSAJE" nowrap="yes" width="20%"/>
            	                <jlis:td name="REGISTRO" nowrap="yes" width="8%"/>
            	                <jlis:td name="ESTADO" nowrap="yes" width="8%"/>
                                <jlis:td name="DR_ID" nowrap="yes" />
                                <jlis:td name="SDR" nowrap="yes" />
                                <jlis:td name="MODIFICACION DR" nowrap="yes" />
            	            </jlis:tr>
                            <jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
            	            <jlis:columnStyle column="2" columnName="MTO" hide="yes"/>
            	            <jlis:columnStyle column="3" columnName="FORMATO_ID" hide="yes"/>
            	            <jlis:columnStyle column="4" columnName="TUPA" nowrap="yes" />
            	            <jlis:columnStyle column="5" columnName="FORMATO" />
            	            <jlis:columnStyle column="6" columnName="NOMBRE_FORMATO" />
            	            <jlis:columnStyle column="7" columnName="NOMBRE_ACUERDO_INTERNACIONAL" />
            	            <jlis:columnStyle column="8" columnName="NOMBRE_ENTIDAD_UO" />
            	            <jlis:columnStyle column="9" columnName="NOMBRE_PAIS" />
            	            <jlis:columnStyle column="10" columnName="ORDEN" align="center" />
            	            <jlis:columnStyle column="11" columnName="SUCE" align="center" />
                            <jlis:columnStyle column="12" columnName="EXPEDIENTE_ENTIDAD" hide="yes" />
                            <jlis:columnStyle column="13" columnName="CERTIFICADO" align="center" />
                            <jlis:columnStyle column="14" columnName="DR" align="center" editable="yes"/>
                            <jlis:columnStyle column="15" columnName="TIPODR" align="center" />
                            <jlis:columnStyle column="16" columnName="MENSAJE_ID" hide="yes" />
                            <jlis:columnStyle column="17" columnName="MENSAJE" />
            	            <jlis:columnStyle column="18" columnName="FECHA_REGISTRO" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
            	            <jlis:columnStyle column="19" columnName="DESCRIPCION_ESTADO" align="center"/>
                            <jlis:columnStyle column="20" columnName="DR_ID" hide="yes" />
                            <jlis:columnStyle column="21" columnName="SDR" hide="yes" />
                            <jlis:columnStyle column="22" columnName="MODIFICACION_DR" hide="yes" />
            	            <jlis:tableButton column="14" type="link" onClick="verDr" />
                            <%--jlis:tr>
                                <jlis:td name="ORDEN ID" nowrap="yes" />
                                <jlis:td name="MTO" nowrap="yes" />
                                <jlis:td name="FORMATO_ID" nowrap="yes" />
                                <jlis:td name="ENTIDAD ID" nowrap="yes" />
                                <jlis:td name="DR ID" nowrap="yes" />
                                <jlis:td name="SDR" nowrap="yes" />
                                <jlis:td name="MODIFICACION DR" nowrap="yes" />
                                <jlis:td name="ENTIDAD" nowrap="yes" width="10%"/>
                                <jlis:td name="TUPA" nowrap="yes" width="5%"/>
                                <jlis:td name="FORMATO" nowrap="yes" width="5%"/>
                                <jlis:td name="SOLICITUD" nowrap="yes" width="5%"/>
                                <jlis:td name="SUCE" nowrap="yes" width="5%"/>
                                <jlis:td name="EXPEDIENTE" nowrap="yes" width="5%"/>
                                <jlis:td name="DR" nowrap="yes" width="5%"/>
                                <jlis:td name="CO" nowrap="yes" width="5%"/>
                                <jlis:td name="TIPO" nowrap="yes" width="5%"/>
                                <jlis:td name="MENSAJE ID" nowrap="yes"/>
                                <jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
                                <jlis:td name="FECHA REGISTRO" nowrap="yes" width="5%"/>
                                <jlis:td name="ESTADO" nowrap="yes" width="8%"/>
                            </jlis:tr>
                            <jlis:columnStyle column="1" columnName="ORDEN_ID" hide="yes" />
                            <jlis:columnStyle column="2" columnName="MTO" hide="yes"/>
                            <jlis:columnStyle column="3" columnName="FORMATO_ID" hide="yes"/>
                            <jlis:columnStyle column="4" columnName="ENTIDAD_ID" hide="yes"/>
                            <jlis:columnStyle column="5" columnName="DR_ID" hide="yes" />
                            <jlis:columnStyle column="6" columnName="SDR" hide="yes" />
                            <jlis:columnStyle column="7" columnName="MODIFICACION_DR" hide="yes" />
                            <jlis:columnStyle column="8" columnName="NOMBRE_ENTIDAD" />
                            <jlis:columnStyle column="9" columnName="TUPA" />
                            <jlis:columnStyle column="10" columnName="FORMATO" />
                            <jlis:columnStyle column="11" columnName="ORDEN" align="center" />
                            <jlis:columnStyle column="12" columnName="SUCE" align="center" />
                            <jlis:columnStyle column="13" columnName="EXPEDIENTE_ENTIDAD" align="center" nowrap="yes" />
                            <jlis:columnStyle column="14" columnName="DR" align="center" editable="yes" />
                            <jlis:columnStyle column="15" columnName="DR_ENTIDAD" align="center" nowrap="yes" />
                            <jlis:columnStyle column="16" columnName="TIPO_DR" align="center" nowrap="yes" />
                            <jlis:columnStyle column="17" columnName="MENSAJE_ID" hide="yes" />
                            <jlis:columnStyle column="18" columnName="MENSAJE" />
                            <jlis:columnStyle column="19" columnName="FECHA_REGISTRO" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
                            <jlis:columnStyle column="20" columnName="ESTADO" align="center"/>
                            <jlis:tableButton column="14" type="link" onClick="verDr" / --%>
                        </jlis:table>
                    </div>

                    </div></div></div>
                </form>
            </div></div>
            <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
    <script>
        initializetabcontent("maintab");

        <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
        if (document.getElementById("tableSize_MODIFICACION_DR").value==0) {
        	$("#idTabSolicitudesRectificacionDR").hide();
        }
        </c:if>
    </script>
</html>