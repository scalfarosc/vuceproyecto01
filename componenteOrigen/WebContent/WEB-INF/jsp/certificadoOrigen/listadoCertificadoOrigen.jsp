<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Certificados</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <c:set var="activeSO" value="active" scope="request" />
    <script>

	    function validarSubmit() {
	        var f = document.formulario;
	        if (f.button.value=="buscarButton") {
	            if (f.numero.value!="" && getSelectedRadioValue(f.opcionFiltro)=="") {
	                alert("Debe seleccionar la opci�n Solicitud o SUCE");
	                return false;
	            }
	        }
	        return true;
	    }
        
	    function buscar() {
	        var f = document.formulario;
	        f.action = contextName+"/origen.htm";
	        f.method.value = "listarCertificadoOrigen";
	        f.target = "_self";
	        f.button.value = "buscarButton";
	        if($("#liBorrador").hasClass("selected"))
	          $("#seleccionado").val("1");
	        else
	          $("#seleccionado").val("0");
        }

		function seleccionOpcion(obj) {
			var f = document.formulario;
			if (obj.value=="O") {
				f.numeroSUCE.value = "";
			} else {
				f.numeroOrden.value = "";
			}
		}

		function seleccionNumeroOrden() {
			var f = document.formulario;
			f.numeroSUCE.value = "";
			document.getElementById("opcionFiltro_O").checked = true;
		}

		function seleccionNumeroSUCE() {
			var f = document.formulario;
			f.numeroOrden.value = "";
			document.getElementById("opcionFiltro_S").checked = true;
		}

		function verCertificado(keyValues, keyValuesField){
            var f = document.formulario;
            f.orden.value = document.getElementById(keyValues.split("|")[0]).value;
            f.mto.value = document.getElementById(keyValues.split("|")[1]).value;
            f.formato.value = document.getElementById(keyValues.split("|")[3]).value;
            //f.idTupa.value =document.getElementById(keyValues.split("|")[4]).value;
            //f.idFormato.value=document.getElementById(keyValues.split("|")[5]).value;
            //var frmlow = formato.toLowerCase();
            //alert(f.formato.value);
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.submit();
	    }

	    function nuevoCertificado() {
	        var f = document.formulario;
	        f.action = contextName + "/cer.htm";
	        f.method.value="nuevoCertificado";
	        f.submit();
	    }

	    function verTrazabilidad(keyValues, keyValuesField){
            var f = document.formulario;
	    	var numeroOrden = document.getElementById(keyValues.split("|")[2]).value;
	    	var numeroSuce;
	    	if (document.getElementById(keyValues.split("|")[6]) != null) { // Se consider� la suce como parte de los keys
	    		numeroSuce = document.getElementById(keyValues.split("|")[6]).value;
	    	} else { // si no se consider�, entonces la haremos vac�a para que se vuelva n�mero de orden seg�n el algoritmo de debajo
				numeroSuce = "";
	    	}
			var rutaopcion = (numeroSuce!="" && numeroSuce!="null") ? "S" : "O";

            f.action = contextName + "/traza.htm?tipo=0&numeroOrden="+numeroOrden+"&numeroSuce="+numeroSuce+"&rutaopcion="+rutaopcion; //"&rutaopcion=CER";
            f.method.value = "listarTrazas";
            f.submit();
	    }

    </script>

    <body>
    	<div id="body">
     		<jsp:include page="/WEB-INF/jsp/header.jsp" />

    		<!-- CONTENT -->
			<div id="contp"><div id="cont">
        		<form name="formulario" method="post">
					<jlis:value type="hidden" name="method" />
					<jlis:value type="hidden" name="orden" />
					<jlis:value type="hidden" name="mto" />
					<jlis:value type="hidden" name="formato" />
					<jlis:value type="hidden" name="seleccionado" />
					<jlis:value type="hidden" name="entidad" value="9" />
					<jlis:value type="hidden" name="numero" value="" />
					<jlis:value type="hidden" name="idTupa" />
					<jlis:value type="hidden" name="idFormato" />

					<jlis:value type="hidden" name="button" />

		            <div id="pageTitle">
            			<h1><strong><jlis:label labelClass="pageTitle"  key="co.title.certificados" /></strong></h1>
            		</div>

		            <br/>
		            <p align="left">
		            	<jlis:label key="co.label.acuerdo" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.acuerdo.todos.select" name="filtroAcuerdo" style="defaultElement=2" onChange="listarCertificadoOrigen()" />&nbsp;&nbsp;	            	
		            	<jlis:label key="co.label.filtro.entidad" style="font-weight: bold;"/>&nbsp;&nbsp;		            	
            			<c:choose>
	      					<c:when test="${sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' ||
		                    sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
		                    sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO')}">
								<jlis:selectProperty key="comun.entidad.select.all" name="filtroEntidad" style="defaultElement=2,width=250px" onChange="listarCertificadoOrigen()" />&nbsp;&nbsp;
						 	</c:when>
							<c:otherwise>
								<jlis:selectProperty key="comun.entidad.select" name="filtroEntidad" style="defaultElement=2,width=250px" onChange="listarCertificadoOrigen()" />&nbsp;&nbsp;
							</c:otherwise>
				  		</c:choose>
		            	
					</p>

		            <p align="left">
		            	<jlis:label key="co.label.formato" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.entidad.formatosxEntidad.select" name="filtroFormato" style="defaultElement=2" onChange="listarCertificadoOrigen()" />&nbsp;&nbsp;
		                <jlis:label key="co.label.estado" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.estadosCO.select" name="filtroEstado" style="defaultElement=no,width=250px" onChange="listarCertificadoOrigen()" />&nbsp;&nbsp;
					</p>

		            <p align="left">
						<jlis:value type="radio" name="opcionFiltro" checkValue="O" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.solicitud" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroOrden" size="8" onClick="seleccionNumeroOrden()" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="S" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.suce" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="numeroSUCE" size="8" onClick="seleccionNumeroSUCE()" maxLength="10" onChange="validarEnteroPositivo(this, false);" onBlur="validarEnteroPositivo(this, false);" />&nbsp;&nbsp;
						
		                <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' ||
		                    sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
		                    sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO')}">
		                <jlis:label key="co.label.filtro.tipoDocumento" style="font-weight:bold"/>
		                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                <jlis:selectProperty name="tipoDocumento"  key="comun.tipo_documento.select"/>
		                <jlis:label key="co.label.filtro.numeroDocumento" style="font-weight:bold"/>&nbsp;&nbsp;
		                <jlis:value name="numeroDocumento" size="10" type="text"/>&nbsp;&nbsp;
		                <jlis:label key="co.title.filtro.nombre" style="font-weight:bold"/>&nbsp;&nbsp;
		                <jlis:value name="nombre" size="50" type="text"/>&nbsp;&nbsp;
		                </c:if>
		                
						<jlis:button id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Pulse aqu� para buscar" />
					</p>

		            <br/>

		            <ul id="maintab" class="tabs">
		            	<li id="liCertif" class="selected" onclick="seleccionarTab(0)" ><a href="#" rel="tabCertificados" onClick="activarCertificados()"><span>Solicitud-SUCE</span></a></li>
		                <li id="liBorrador" onclick="seleccionarTab(1)" ><a href="#" rel="tabBorradores"><span>Borradores</span></a></li>
		               
		               <c:if test="${sessionScope.USUARIO.roles['CO.ENTIDAD.FIRMA'] == 'CO.ENTIDAD.FIRMA' ||
		                    sessionScope.USUARIO.roles['CO.USUARIO.FIRMA'] == 'CO.USUARIO.FIRMA'}">
		              
		                <li id="liPendiente" onclick="seleccionarTab(2)" ><a href="#" rel="tabPendientes"><span>Pendientes de Firma Digital</span></a></li>
		                </c:if>
		                <%--<li><a href="#" rel="tabNotificaciones"><span>Notificaciones</span></a></li> --%>
		            </ul>
		            <div id="tabCertificados" class="tabcontent" >
		            	<jlis:table keyValueColumns="ORDENID,MTO,ORDEN,NOMBREFORMATO,TUPA_ID,FORMATO_ID,SUCE" name="CO" key="certificadoOrigen.certificados.grilla" filter="${filterGrilla}" pageSize="6" width="90%" navigationHeader="yes" validator="pe.gob.mincetur.vuce.co.web.util.CertificadoOrigenRowValidator" >
							<jlis:tr>
								<jlis:td name="TUPA" nowrap="yes" />
								<jlis:td name="FORMATO" nowrap="yes" />
								<jlis:td name="NOMBRE" nowrap="yes" />
								<jlis:td name="ORDENID" nowrap="yes" />
								<jlis:td name="MTO" nowrap="yes" />
								<jlis:td name="ACUERDO COMERCIAL" nowrap="yes"  width="18%"/>
								<jlis:td name="ENTIDAD CERTIFICADORA" nowrap="yes" width="20%"/>
								<jlis:td name="PA�S" nowrap="yes" width="12%" />
								<jlis:td name="SOLICITUD" nowrap="yes" width="9%" />
								<jlis:td name="SUCE" nowrap="yes" width="9%" />
								
                        <jlis:td name="T. DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="NRO DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="SOLICITANTE" nowrap="yes" width="15%"/>
								
								<jlis:td name="ESTADO" nowrap="yes" width="7%"/>
								<jlis:td name="FECHA DE REGISTRO" nowrap="yes" width="7%"/>
								<jlis:td name="TUPA ID" nowrap="yes"/>
								<jlis:td name="FORMATO ID" nowrap="yes"/>
								<jlis:td name="TRAZABILIDAD" nowrap="yes" width="7%"/>
                        		<jlis:td name="&nbsp;" nowrap="yes" />
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="NOMBRETUPA" />
							<jlis:columnStyle column="2" columnName="NOMBREFORMATO" />
							<jlis:columnStyle column="3" columnName="NOMBRECUT" />
							<jlis:columnStyle column="4" columnName="ORDENID" hide="yes" />
							<jlis:columnStyle column="5" columnName="MTO" hide="yes" />
							<jlis:columnStyle column="6" columnName="NOMBREACUERDOINTERNACIONAL" />
							<jlis:columnStyle column="7" columnName="NOMBREENTIDAD" />
							<jlis:columnStyle column="8" columnName="NOMBREPAIS" />
							<jlis:columnStyle column="9" columnName="ORDEN" editable="yes" />
							<jlis:columnStyle column="10" columnName="SUCE" editable="yes" validator="pe.gob.mincetur.vuce.co.web.util.CertificadoOrigenCellValidator" />
							
                    <jlis:columnStyle column="11" columnName="TIPO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="12" columnName="NUMERO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="13" columnName="SOLICITANTE" hide="${hideDatosSoliciante}" />
							
							<jlis:columnStyle column="14" columnName="ESTADOREGISTRO" />
							<jlis:columnStyle column="15" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
							<jlis:columnStyle column="16" columnName="TUPA_ID" hide="yes" />
							<jlis:columnStyle column="17" columnName="FORMATO_ID" hide="yes" />
							<jlis:columnStyle column="18" columnName="VERTRAZABILIDAD" editable="yes" align="center" />
							<jlis:columnStyle column="19" columnName="NUMERONOTIFICACION" align="center" validator="pe.gob.mincetur.vuce.co.web.util.NotificacionCertificadoCellValidator"/>
        					<jlis:tableButton column="9" type="link" onClick="verCertificado" />
        					<jlis:tableButton column="10" type="link" onClick="verCertificado" />
        					<jlis:tableButton column="18" type="link" onClick="verTrazabilidad" />
                    		<jlis:tableButton column="19" type="imageButton" />
					 	</jlis:table>
			        </div>
			        <div id="tabBorradores" class="tabcontent">
						<jlis:table keyValueColumns="ORDENID,MTO,ORDEN,NOMBREFORMATO,TUPA_ID,FORMATO_ID" name="CO_Borradores" key="certificadoOrigen.certificados.borradores.grilla" filter="${filterGrilla}" pageSize="6" navigationHeader="yes" width="90%" >
							<jlis:tr>
								<jlis:td name="TUPA" nowrap="yes" />
								<jlis:td name="FORMATO" nowrap="yes" />
								<jlis:td name="NOMBRE" nowrap="yes" />
								<jlis:td name="ORDENID" nowrap="yes" />
								<jlis:td name="MTO" nowrap="yes" />
								<jlis:td name="ACUERDO COMERCIAL" nowrap="yes"  width="18%"/>
								<jlis:td name="ENTIDAD CERTIFICADORA" nowrap="yes" width="20%"/>
								<jlis:td name="PA�S" nowrap="yes" width="18%" />
								<jlis:td name="SOLICITUD" nowrap="yes" width="10%" />
								
                        <jlis:td name="T. DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="NRO DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="SOLICITANTE" nowrap="yes" width="15%"/>
								
								<jlis:td name="ESTADO" nowrap="yes" width="7%"/>
								<jlis:td name="FECHA DE REGISTRO" nowrap="yes" width="7%"/>
								<jlis:td name="TUPA ID" nowrap="yes"/>
								<jlis:td name="FORMATO ID" nowrap="yes"/>
								<jlis:td name="TRAZABILIDAD" nowrap="yes" width="6%"/>
								<%-- <jlis:td name="VER" nowrap="yes" width="4%"/> --%>
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="NOMBRETUPA" />
							<jlis:columnStyle column="2" columnName="NOMBREFORMATO" />
							<jlis:columnStyle column="3" columnName="NOMBRECUT" />
							<jlis:columnStyle column="4" columnName="ORDENID" hide="yes" />
							<jlis:columnStyle column="5" columnName="MTO" hide="yes" />
							<jlis:columnStyle column="6" columnName="NOMBREACUERDOINTERNACIONAL" />
							<jlis:columnStyle column="7" columnName="NOMBREENTIDAD" />
							<jlis:columnStyle column="8" columnName="NOMBREPAIS" />
							<jlis:columnStyle column="9" columnName="ORDEN" editable="yes" />
							
                    <jlis:columnStyle column="10" columnName="TIPO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="11" columnName="NUMERO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="12" columnName="SOLICITANTE" hide="${hideDatosSoliciante}" />
							
							<jlis:columnStyle column="13" columnName="ESTADOREGISTRO" />
							<jlis:columnStyle column="14" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
							<jlis:columnStyle column="15" columnName="TUPA_ID" hide="yes" />
							<jlis:columnStyle column="16" columnName="FORMATO_ID" hide="yes" />
							<jlis:columnStyle column="17" columnName="VERTRAZABILIDAD" editable="yes" align="center" />
							<%-- <jlis:columnStyle column="10" columnName="VER" hide="yes" /> --%>
        					<jlis:tableButton column="9" type="link" onClick="verCertificado" />
                    		<jlis:tableButton column="17" type="link" onClick="verTrazabilidad" />
					 	</jlis:table>
			        </div>
		            <div id="tabPendientes" class="tabcontent" >
		            	<jlis:table keyValueColumns="ORDENID,MTO,ORDEN,NOMBREFORMATO,TUPA_ID,FORMATO_ID,SUCE" name="CO_Pendientes" key="certificadoOrigen.certificados.pendiente.firma.grilla" filter="${filterGrilla}" pageSize="6" width="90%" navigationHeader="yes" validator="pe.gob.mincetur.vuce.co.web.util.CertificadoOrigenRowValidator" >
							<jlis:tr>
								<jlis:td name="TUPA" nowrap="yes" />
								<jlis:td name="FORMATO" nowrap="yes" />
								<jlis:td name="NOMBRE" nowrap="yes" />
								<jlis:td name="ORDENID" nowrap="yes" />
								<jlis:td name="MTO" nowrap="yes" />
								<jlis:td name="ACUERDO COMERCIAL" nowrap="yes"  width="18%"/>
								<jlis:td name="ENTIDAD CERTIFICADORA" nowrap="yes" width="20%"/>
								<jlis:td name="PA�S" nowrap="yes" width="12%" />
								<jlis:td name="SOLICITUD" nowrap="yes" width="9%" />
								<jlis:td name="SUCE" nowrap="yes" width="9%" />
								
                        <jlis:td name="T. DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="NRO DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="SOLICITANTE" nowrap="yes" width="15%"/>
								
								<jlis:td name="ESTADO" nowrap="yes" width="7%"/>
								<jlis:td name="FECHA DE REGISTRO" nowrap="yes" width="7%"/>
								<jlis:td name="TUPA ID" nowrap="yes"/>
								<jlis:td name="FORMATO ID" nowrap="yes"/>
								<jlis:td name="TRAZABILIDAD" nowrap="yes" width="7%"/>
                        		<jlis:td name="&nbsp;" nowrap="yes" />
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="NOMBRETUPA" />
							<jlis:columnStyle column="2" columnName="NOMBREFORMATO" />
							<jlis:columnStyle column="3" columnName="NOMBRECUT" />
							<jlis:columnStyle column="4" columnName="ORDENID" hide="yes" />
							<jlis:columnStyle column="5" columnName="MTO" hide="yes" />
							<jlis:columnStyle column="6" columnName="NOMBREACUERDOINTERNACIONAL" />
							<jlis:columnStyle column="7" columnName="NOMBREENTIDAD" />
							<jlis:columnStyle column="8" columnName="NOMBREPAIS" />
							<jlis:columnStyle column="9" columnName="ORDEN" editable="yes" />
							<jlis:columnStyle column="10" columnName="SUCE" editable="yes" validator="pe.gob.mincetur.vuce.co.web.util.CertificadoOrigenCellValidator" />
							
                    <jlis:columnStyle column="11" columnName="TIPO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="12" columnName="NUMERO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="13" columnName="SOLICITANTE" hide="${hideDatosSoliciante}" />
							
							<jlis:columnStyle column="14" columnName="ESTADOREGISTRO" />
							<jlis:columnStyle column="15" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
							<jlis:columnStyle column="16" columnName="TUPA_ID" hide="yes" />
							<jlis:columnStyle column="17" columnName="FORMATO_ID" hide="yes" />
							<jlis:columnStyle column="18" columnName="VERTRAZABILIDAD" editable="yes" align="center" />
							<jlis:columnStyle column="19" columnName="NUMERONOTIFICACION" align="center" validator="pe.gob.mincetur.vuce.co.web.util.NotificacionCertificadoCellValidator"/>
        					<jlis:tableButton column="9" type="link" onClick="verCertificado" />
        					<jlis:tableButton column="10" type="link" onClick="verCertificado" />
        					<jlis:tableButton column="18" type="link" onClick="verTrazabilidad" />
                    		<jlis:tableButton column="19" type="imageButton" />
					 	</jlis:table>
			        </div>			        
			        <!-- <div id="tabNotificaciones" class="tabcontent">
			        	Notificaciones
			        </div>  -->

				    <script>

			         	if (getCookie('indiceTab') != undefined){
			         		if (getCookie('indiceTab') != '') {
			             		document.getElementById("seleccionado").value = getCookie('indiceTab');
			         		}
			         	}
			         	if (document.getElementById("seleccionado").value == '0') {
			     			setCookie('maintab', 'tabCertificados');
			     		} else {
			     			setCookie('maintab', 'tabBorradores');
			     		}
			         	
			        	/*if ( document.getElementById("seleccionado").value == 1 ) {
		                	$("#liCertif").removeClass("selected").addClass("");
		                	$("#tabCertificados").hide();
		                	$("#liBorrador").addClass("selected");
		                	$("#tabBorradores").show();
		                } else {
		                	$("#liCertif").addClass("selected");
		                	$("#tabCertificados").show();
		                	$("#liBorrador").removeClass("selected").addClass("");
		                	$("#tabBorradores").hide();
		                }*/
				    
			         	initializetabcontent("maintab");

		                $("#divCargando").hide();
		                $("#divPestanas").show();
			        </script>
			    </form>
        	</div></div>
         	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>