<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/ddjj/ddjj_comun.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/acuerdos/acuerdo${idAcuerdo}/ddjj_acuerdo_${idAcuerdo}.js"></script>
	<script>

    $(document).ready(function() {
    	var tipoPais = '${tipoPais}';
    	var tituloVentana = 'Materiales';
    	//Ticket 149343 GCHAVEZ-05/07/2019
    	if(${disabledGrabar}){
    		disableButton("grabarButton", true);
        	}
        //Ticket 149343 GCHAVEZ-05/07/2019
    	if (tipoPais == 'P') {
    		tituloVentana = 'Materiales de Per�';
    	} else if (tipoPais == 'N') {
    		tituloVentana = 'Materiales No Originarios';
    	}
		tituloPopUp(tituloVentana);

		cargarUMs();

		if (document.getElementById("DJ_MATERIAL.itemMaterial").value == ''){
			$("#itemRow").attr("style", "display:none");
		}

       	inicializacionDeclaracionJuradaMaterialSegunAcuerdo();

        var bloqueado = document.getElementById("bloqueado").value;
       	if(bloqueado=="S") {
       		bloquearControles();
		}

       	if (tipoPais == 'P') {
       		manejarTipoDocumento(document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo"),1);
       	}

    	// Para los tipos de documento
    	//alert('${requestScope["DJ_MATERIAL.fabricanteDocumentoTipo"]}');
    	//document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value = '${requestScope["DJ_MATERIAL.fabricanteDocumentoTipo"]}';

	});


	// Validaci�n antes del submit
	function validarSubmit() {
	    var f = document.formulario;

	    if (f.button.value == "cancelarButton") {
	    	return false;
        }
	    if (f.button.value == "busqPartidasButton") {
        	return false;
        }
        if (f.button.value == "cargarButton") {
            disableButton("cargarButton", true);
            disableButton("eliminarButton", true);
        }
        if (f.button.value == "eliminarButton") {
            disableButton("cargarButton", true);
            disableButton("eliminarButton", true);
        }
	    if (f.button.value == "cerrarPopUpButton") {
        	return validaCerraPopUp();
        }

	    return true;
	}


	// Graba un material
	function grabarRegistro() {
		var f = document.formulario;
		if (validarCamposDeclaracionJuradaMaterial()) {
			habilitarXAcuerdo();

	        f.action = contextName+"/origen.htm";
	        f.method.value="grabarDeclaracionJuradaMaterialForm";
	        f.button.value="grabarButton";
            disableButton("grabarButton", true);
            disableButton("eliminarButton", true);
            f.submit();
		} else {
			f.button.value="cancelarButton";
		}
	}

	// Elimina un material
	function eliminarRegistro() {
		var f = document.formulario;
        if (!confirm("�Est� seguro de eliminar este registro?")) {
			f.button.value="cancelarButton";
        } else{
	        f.action = contextName+"/origen.htm";
	        f.method.value="eliminarDeclaracionJuradaMaterialForm";
	        f.button.value="eliminarButton";
	        f.secuenciaMaterial.value=document.getElementById("DJ_MATERIAL.secuenciaMaterial").value;

            disableButton("grabarButton", true);
            disableButton("eliminarButton", true);

	        f.target = window.parent.name;
	        window.parent.hidePopWin(false);
            f.submit();
        }
    }

    // B�squeda de Partidas Arancelarias
    function buscarPartidas() {
    	var f = document.formulario;
    	showPopWin(contextName+"/origen.htm?method=cargarPartidas", 510, 400, null);
    	f.button.value="busqPartidasButton";
    }

	// Esto es invocado desde la b�squeda de partidas
    function cargarDataPartida(codPartida, descPartida) {
    	document.getElementById("DJ_MATERIAL.partidaArancelaria").value = codPartida;
    	document.getElementById("DJ_MATERIAL.partidaArancelariaDesc").value = codPartida + ' ' + descPartida;
    	cargarUMs();
    }

	// Carga la lista de unidades de medida de acuerdo a la partida arancelaria seleccionada
	function cargarUMs() {
		var f = document.formulario;
		var partida = document.getElementById("DJ_MATERIAL.partidaArancelaria").value;
		if ( partida == '' ){
			loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.umFisicaId", "select.empty", null, null, null,null);
		} else {
			var filter = "partida=" + partida;
			//loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.umFisicaId", "comun.partida.unidad_fisica.select", filter, null, afterCargarUm, null);
			//loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.umFisicaId", "comun.um_fisica${(enIngles=='S')?'.ingles':''}.select", filter, null, afterCargarUm, null);
			loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.umFisicaId", "comun.um_fisica.select", filter, null, afterCargarUm, null);
		}
	}

	// Se invoca despues de la carga asincrona del combo
	function afterCargarUm() {
	    var djPartida = $("#DJ_MATERIAL\\.partidaArancelaria").val();
	    var umfisica = '${umFisicaId}';
		if ( djPartida != '') {
			document.getElementById("DJ_MATERIAL.umFisicaId").value = $("#umFisicaId").val();
		}
	}

    // Carga de archivos
    function cargarArchivo() {
        var f = document.formulario;
        if (f.archivo.value != "") {
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarArchivoDeclaracionJuradaMaterialForm";
            f.button.value = "cargarButton";
        } else {
            alert("Debe seleccionar alg�n archivo");
            f.button.value = "cancelarButton";
        }
    }

    // Descargar archivos adjuntos
    function descargarAdjunto(keyValues, keyValuesField) {
        var f = document.formulario;
        var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
        f.action = contextName+"/origen.htm";
        f.method.value = "descargarAdjunto";
        f.idAdjunto.value = adjuntoId;
        f.submit();
    }

    function eliminarRegistroAdjunto() {
        var f = document.formulario;
        var indice = -1;
        if (f.seleccione!=null) {
            if (eval(f.seleccione.length)) {
                for (i=0; i < f.seleccione.length; i++) {
                    if (f.seleccione[i].checked){
                        indice = i;
                    }
                }
            } else {
                if (f.seleccione.checked) {
                    indice = 0;
                }
            }
        }
        if (indice!=-1) {
            f.action = contextName+"/origen.htm";
            f.method.value="eliminarArchivoDeclaracionJuradaMaterialForm";
            f.button.value="eliminarButton";

        } else {
            alert('Seleccione por lo menos un registro a eliminar');
            f.button.value = 'cancelarButton';
        }
    }

    function limpiarRUC(){
        $("#DJ_MATERIAL\\.fabricanteNumeroDocumento").val("");
        $("#DJ_MATERIAL\\.fabricanteNombre").val("");
    }

    
    function afterValidarEsProductor() {
		$("#loadingIcon").hide();
    	var flag = document.getElementById("flagEsProductor").value;
        document.getElementById("flagEsProductor").value = "";
    	if (flag=="N") {
    		alert("El Fabricante � Proveedor no tiene rol PRODUCTOR");
    		return;
    	}
    }
    // Maneja la interacci�n con SUNAT para validar el RUC
    // control: 1: combo de tipo de documento 2: caja de texto de n�mero de documento
    function cargaDatosProductorRUC(control) {
    	if(control=='2'){
	    	if ($("#DJ_MATERIAL\\.fabricanteDocumentoTipo").val() == '1'){
	    		var numeroDocumento = document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value;
	    		var tipoDocumento   = $("#DJ_MATERIAL\\.fabricanteDocumentoTipo").val();    		
	    		var tipoRol         = document.getElementById("tipoRol").value;
	    		
	    		limpiarRUC();
		        /* JMC - 12 Julio - comentado porque cualquier RUC puede ser el Fabricante-Provedor 
		        
		        var djId = document.getElementById("djId").value;
		        var filter = tipoDocumento+'|'+numeroDocumento+'|'+djId;
		        loadElementAJX("co.ajax.certificado.material.validaProductor.elementLoader", "validarEsProductor", null, filter, null, afterValidarEsProductor);
		        */
		        /* 10 Junio - Comentado debido a que no realizaba correctamente la validez de un Productor
		        if ( (tipoRol == '1' || tipoRol == '6') && "${sessionScope.USUARIO.numRUC}" == trim(numeroDocumento)){
		        	alert("Al ser exportador no puede registrarse a s� mismo como fabricante del material");
		        	document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value = '';
		        } else {*/
		        if (numeroDocumento != "") {
		            $("#loadingRUC").show();
		            var filter = "ruc=" + numeroDocumento + "|subPantalla=1";
		            loadElementAJX("co.ajax.ruc.elementLoader", "cargarDatosDJMaterialProdRUC", null, filter, null, validaCargaRUC);
		        }
		        //}
	    	}
    	}
    	// Esto s�lo es v�lido cuando se invoca desde el combo
    	if ( control == 1 ) {
        	manejarTipoDocumento(document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo"),2);
    	}
    }

    // Maneja la l�gica para habilitar o deshabilitar el nombre y direcci�n seg�n el tipo de documento.
    // Acci�n: 1:inicio 2:combo (indica desde donde se invoca para poder limpiar las cajas de ser necesario)
    function manejarTipoDocumento(combo, accion) {
    	if ( combo.value == 2 ) { // Si aun no se ingresa nada, o se vuelve a  el tipo es DNI se habilitan las cajas de texto.
            $("#DJ_MATERIAL\\.fabricanteNombre").removeClass("readonlyTextAreaClass").addClass("textAreaClass");
            $("#DJ_MATERIAL\\.fabricanteNombre").attr("readonly",false);
    	} else { // Caso contrario se deshabilitan las cajas de texto
            $("#DJ_MATERIAL\\.fabricanteNombre").removeClass("textAreaClass").addClass("readonlyTextAreaClass");
            $("#DJ_MATERIAL\\.fabricanteNombre").attr("readonly",true);
    	}
		if ( accion == 2 ) { // Esto s�lo se realiza cuando se ha cambiado el combo
			document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value = '';
			document.getElementById("DJ_MATERIAL.fabricanteNombre").value = '';
		}
    }

    function validaCargaRUC(){
        $("#loadingRUC").hide();
        if($("#DJ_MATERIAL\\.nombre").val() == ""){
            $("#mensajeRUC").html("El n�mero de RUC consultado no es v�lido. Debe verificar el n�mero y volver a ingresar.");
            $("#DJ_MATERIAL\\.fabricanteNumeroDocumento").css('border', '1px solid red');
    	} else {
            $("#mensajeRUC").html("");
            $("#DJ_MATERIAL\\.fabricanteNumeroDocumento").css('border', '1px solid #0B2C6F');
		}
	}

	</script>
	<body id="contModal">
		<%@include file="/WEB-INF/jsp/helpHint.jsp" %>
	    <jlis:messageArea width="100%" />
        <br/>
	    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
	        <input type="hidden" name="idAdjunto">
	        <input type="hidden" id="flagEsProductor" name="flagEsProductor" />
            <jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
            <jlis:value name="idFormatoEntidad" type="hidden" />
            <jlis:value name="secuenciaMaterial" type="hidden" />
	        <jlis:value name="bloqueado" type="hidden" />
	        <jlis:value name="coId" type="hidden" />
	        <jlis:value name="djId" type="hidden" />
	        <jlis:value name="tipoPais" type="hidden" />
	        <jlis:value name="umFisicaId" type="hidden" />
	        <jlis:value name="DJ_MATERIAL.djId" type="hidden"  />
            <jlis:value name="DJ_MATERIAL.secuenciaMaterial" type="hidden"  />
	        <jlis:value name="idFormato" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <jlis:value name="adjunto" type="hidden" />
	        <jlis:value name="tce" type="hidden" />
	        <jlis:value name="permiteAdicional" type="hidden" />
	        <jlis:value name="transmitido" type="hidden" />
	        <jlis:value name="controller" type="hidden" />
	        <jlis:value name="esDetalle" type="hidden" />
	        <jlis:value name="flgCambioClasif" type="hidden" />
	        <jlis:value name="modoProductorValidador" type="hidden" />
	        <jlis:value name="esValidador" type="hidden" />
	        <jlis:value name="modoFormato" type="hidden" />
	        <jlis:value name="idAcuerdo" type="hidden" />
	        <jlis:value name="estadoDj" type="hidden" />
	        <jlis:value name="criterioArancelario" type="hidden" />
	        <jlis:value name="idPaisSegundoComponente" type="hidden" />
	        <jlis:value name="tempPaisProcedencia" type="hidden" />
	        <jlis:value name="tempPaisOrigen" type="hidden" />
	        <jlis:value name="tipoRol" type="hidden" />
	        <jlis:value name="secuenciaOrigen" type="hidden" />

			<co:validacionBotonesFormato formato="DJ" pagina="formMaterial" />

			<table class="form">
				<tr>
					<td colspan="2">
						<jlis:button code="CO.USUARIO.OPERACION" id="grabarButton" name="grabarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar Material" alt="Pulse aqu� para grabar el material" />
					    <jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Material" alt="Pulse aqu� para eliminar el material" />
					    <jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
					     <%-- ${estadoDj} --%>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
                <tr id="itemRow">
					<th><jlis:label key="co.label.dj_material.item_material" /></th>
					<td><jlis:value name="DJ_MATERIAL.itemMaterial" size="22" maxLength="22" onBlur="validarEnteroPositivo(this,false,null)" type="text" style="text-align:right" editable="no" /><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr>
					<th><jlis:label key="co.label.dj_material.descripcion" /> </th>
					<td><jlis:textArea name="DJ_MATERIAL.descripcion" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);"/><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr>
					<th><jlis:label key="co.label.partida_arancelaria" /></th>
                    <td>
						<jlis:button code="CO.USUARIO.OPERACION" id="buscarPartidasButton" type="BUTTON_JAVASCRIPT_IMAGE_SUBMIT" name="buscarPartidas()" alt="Buscar Partida" image="<%=imagenBuscar%>" />
						<jlis:value type="hidden" name="DJ_MATERIAL.partidaArancelaria" />
						<jlis:textArea name="DJ_MATERIAL.partidaArancelariaDesc" editable="no" rows="2" style="width:75%" /><span class="requiredValueClass">(*)</span>
					</td>
				</tr>
				<tr id="trMaterialPartidaAcuerdo" style="display:none" >
                    <th><jlis:label key="co.label.dj.${idAcuerdo}.naladisa" /></th>
					<td>
						<jlis:value name="DJ_MATERIAL.partidaSegunAcuerdo" size="10" maxLength="8" type="text" onBlur="validarNaladisa(this)" /><span class="requiredValueClass">(*)</span>
					</td>
                </tr>
                <tr id="trProcedencia" style="display:none" >
					<th><jlis:label key="co.label.dj_material.pais_procedencia" /></th>
					<td><jlis:selectProperty key="comun.pais_iso.select" name="DJ_MATERIAL.paisProcedencia" /><span id="spanPaisProcedencia" style="display:none" class="requiredValueClass">(*)</span>
					</td>
                </tr>
                <tr id="trOrigen" style="display:none" >
					<th><jlis:label key="co.label.dj_material.pais_origen" /></th>
					<td><jlis:selectProperty key="comun.pais_iso.select" name="DJ_MATERIAL.paisOrigen" /><span id="spanPaisOrigen" style="display:none" class="requiredValueClass">(*)</span></td>
                </tr>
                <tr id="trDocumentoTipoFabricante" style="display:none" >
					<th><jlis:label key="co.label.dj_material.fabricante_documento_tipo" /></th>
					<td><jlis:selectProperty key="comun.documento_tipo.select" name="DJ_MATERIAL.fabricanteDocumentoTipo" onChange="cargaDatosProductorRUC(1)" /><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr id="trDocumentoNumeroFabricante" style="display:none" >
					<th><jlis:label key="co.label.dj_material.fabricante_numero_documento" /></th>
					<td><jlis:value name="DJ_MATERIAL.fabricanteNumeroDocumento" size="25" maxLength="25" type="text" onChange="cargaDatosProductorRUC(2)"/><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr id="trNombreFabricante" style="display:none" >
					<th><jlis:label key="co.label.dj_material.fabricante_nombre" /></th>
					<td><jlis:textArea name="DJ_MATERIAL.fabricanteNombre" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);"/><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr>
					<th><jlis:label key="co.label.dj_material.um_fisica_id" /></th>
					<td><jlis:selectProperty key="select.empty" name="DJ_MATERIAL.umFisicaId" /><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr>
					<th><jlis:label key="co.label.dj_material.cantidad" /></th>
					<td><jlis:value name="DJ_MATERIAL.cantidad" size="22" maxLength="22" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/><span class="requiredValueClass">(*)</span></td>
                </tr>
                <tr id="trPesoMaterial" style="display:none" >
					<th><jlis:label key="co.label.dj_material.peso_material" /></th>
					<td>
						<jlis:value name="DJ_MATERIAL.pesoNeto" size="22" maxLength="22" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/><span id="spanPesoMaterial" class="requiredValueClass">(*)</span>
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.##.PESO" material="si" codigoPais="${tipoPais}" />
					</td>
                </tr>
                <tr >
					<th><jlis:label key="co.label.dj_material.valor_en_us" /></th>
					<td>
						<jlis:value name="DJ_MATERIAL.valorUs" size="22" maxLength="22" onBlur="validarDoubleNegativoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/><span id="spanValorUs" style="display:none" class="requiredValueClass">(*)</span>
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.##.VALOR_US" material="si" codigoPais="${tipoPais}" acuerdoId="${idAcuerdo}" />
					</td>
                </tr>
				<c:if test="${estadoDj == 'T'}">
	                <tr>
						<th><jlis:label key="co.label.dj_material.porcentaje_valor" /></th>
						<td>
							<jlis:value name="DJ_MATERIAL.porcentajeValor" size="22" maxLength="22" onBlur="validarDoubleNegativoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/><%-- span class="requiredValueClass">(*)</span--%>
							<%-- co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.##.PORC_VALOR_US" material="si" codigoPais="${tipoPais}" acuerdoId="${idAcuerdo}" /--%>
						</td>
	                </tr>
				</c:if>

			</table>

			<table id="tablaSubtituloAdjunto" style="display:none" >
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="requiredValueClass"><jlis:label key="co.label.dj.material.subtitulo.adjunto" /></span>
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.##.JUSTIFICACION" material="si" codigoPais="${tipoPais}" acuerdoId="${idAcuerdo}" />
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
                <tr>
					<td><jlis:button code="CO.USUARIO.OPERACION" id="cargarButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
					<td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" name="eliminarRegistroAdjunto()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
                </tr>
            </table>
            <table id="tablaBotonesAdjunto" style="display:none" >
                <tr>
                    <td>
                    <jlis:label key="co.label.documentos_adjuntar"/> <br />
                    <jlis:label key="co.label.material.tipo_archivos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                     <input type="file" id="archivo" name="archivo" size="87"  />
                    </td>
                </tr>
            </table>
	        <br/>

	        <jlis:value name="ADJ_ELIM.djId" type="hidden" />
	        <jlis:value name="ADJ_ELIM.secuenciaMaterial" type="hidden" />
	        <jlis:value name="ADJ_ELIM.adjuntoId" type="hidden" />

			<span id="tablaMaterialAdjuntos" style="display:none" >
		        <jlis:table name="ADJUNTOS" keyValueColumns="ADJUNTOID" source="tAdjuntosMaterial" scope="request"
		            pageSize="*" navigationHeader="no" width="98%" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCopiadosTramitePadreRowValidator" >
		            <jlis:tr>
		                <jlis:td name="ADJUNTO ID" nowrap="yes" />
		                <jlis:td name="DJID" nowrap="yes" />
		                <jlis:td name="SECUENCIAMATERIAL" nowrap="yes" />
		                <!-- jlis:td name="COID" nowrap="yes" / -->
		                <jlis:td name="ADJUNTOREQUERIDOUO" nowrap="yes" />
		                <jlis:td name="ADJUNTOTIPO" nowrap="yes" />
		                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" width="83%" />
	              	 	<jlis:td name="TAMA�O (KB)" nowrap="yes" width="7%" />
		              	<jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
		            </jlis:tr>
		            <jlis:columnStyle column="1" columnName="ADJUNTOID" editable="no" />
		            <jlis:columnStyle column="2" columnName="DJID" editable="no" hide="yes" />
		            <jlis:columnStyle column="3" columnName="SECUENCIAMATERIAL" editable="no" hide="yes" />
		            <!-- jlis:columnStyle column="4" columnName="COID" editable="no" hide="yes" / -->
		            <jlis:columnStyle column="4" columnName="ADJUNTOREQUERIDOUO" editable="no" hide="yes" />
		            <jlis:columnStyle column="5" columnName="ADJUNTOTIPO" editable="no" hide="yes" />
		            <jlis:columnStyle column="6" columnName="NOMBREARCHIVO" editable="yes" />
	           		<jlis:columnStyle column="7" columnName="TAMANO" />
		            <jlis:columnStyle column="8" columnName="SELECCIONE" align="center" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCellValidator" editable="yes" />
		            <jlis:tableButton column="6" type="link" onClick="descargarAdjunto" highlight="no" />
		            <jlis:tableButton column="8" type="checkbox" name="seleccione" />
		        </jlis:table>
			</span>

		</form>

	</body>
</html>