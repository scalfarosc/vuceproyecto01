<%
	String controller = (String) request.getAttribute("controller");
%>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/ddjj/ddjj_comun.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/acuerdos/acuerdo${idAcuerdo}/ddjj_acuerdo_${idAcuerdoVersion}.js"></script>
	<script>

	 $(document).ready(function() {
			tituloPopUp("DJ Productor - Certificado de Origen");
		    //initializetabcontent("maintab");
		    validaCambios();

        	var bloqueado = document.getElementById("bloqueado").value;

        	habilitarAdjunto();

        	if(bloqueado=="S"){
        		bloquearControles();

        		if ('${rolExportador}' == '4' || '${rolExportador}' == '5') {
        			$('#tablaMensajesAdjunto').attr("style", "display:none");
        			$('#tablaBotonesAdjunto').attr("style", "display:none");
        		}

            }

        	if ($("#rolExportador").val() == "1") {
        		$("#DJ_PRODUCTOR\\.esValidador").attr("checked", true);
                $("#DJ_PRODUCTOR\\.esValidador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
                $("#DJ_PRODUCTOR\\.esValidador").attr("disabled",true);
        	}

        	inicializacionDeclaracionJuradaProductor();

        	if ('${requestScope["DJ_PRODUCTOR.documentoTipo"]}' != ''){
        		$("#DJ_PRODUCTOR\\.documentoTipo").val('${requestScope["DJ_PRODUCTOR.documentoTipo"]}');
        	}
        	manejarTipoDocumento('${requestScope["DJ_PRODUCTOR.documentoTipo"]}',1);

        	if ($("#DJ_PRODUCTOR\\.numeroDocumento").val() != '' && $("#DJ_PRODUCTOR\\.documentoTipo").val() == '${tipoDocumentoSolicitante}' && $("#DJ_PRODUCTOR\\.numeroDocumento").val() == '${numDocumentoSolicitante}') {
        		$("#DJ_PRODUCTOR\\.documentoTipo").attr('disabled',true).removeClass("inputTextClass").addClass("readonlyInputTextClass");
            	$("#DJ_PRODUCTOR\\.numeroDocumento").attr('disabled',true).removeClass("inputTextClass").addClass("readonlyInputTextClass");

            	$("#eliminarButton").attr('disabled',true).removeClass("buttonSubmitClass").addClass("buttonSubmitDisabledClass");
        	}

	    });

	 	function habilitarAdjunto() {

	 		if ('${secuenciaProductor}' != '' && '${secuenciaProductor}' != '0' && ('${rolExportador}' == '4' || '${rolExportador}' == '5') ) {
        		$('#tablaSubtituloAdjunto').attr("style", "display:");
        		$('#tablaBotonesAdjunto').attr("style", "display:");
        		$('#tablaMensajesAdjunto').attr("style", "display:");
        		$('#tablaDJProductorAdjuntos').attr("style", "display:");
	 		}

	 	}

		function validarSubmit() {
		    var f=document.formulario;

		    if (f.button.value=="cancelarButton") {
	            return false;
	        }
		    if (f.button.value=="cerrarPopUpButton") {
	        	return validaCerraPopUp();
	        }
		    return true;
		}

        var rucBlur = false;

		function validarCampos(){

			var ok = true;
		    var mensaje="Debe ingresar o seleccionar : ";

		    if (document.getElementById("DJ_PRODUCTOR.documentoTipo").value==""){
		        mensaje +="\n -El tipo de documento";
		        changeStyleClass("co.label.usuario_dj.documento_tipo", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.documento_tipo", "labelClass");

		    if (document.getElementById("DJ_PRODUCTOR.numeroDocumento").value==""){
		        mensaje +="\n -El n�mero de documento";
		        changeStyleClass("co.label.usuario_dj.numero_documento", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.numero_documento", "labelClass");

		    if (document.getElementById("DJ_PRODUCTOR.nombre").value==""){
		        mensaje +="\n -El nombre del productor";
		        changeStyleClass("co.label.usuario_dj.nombre", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.nombre", "labelClass");

		    if (document.getElementById("DJ_PRODUCTOR.direccion").value==""){
		        mensaje +="\n -La Direcci�n SUNAT del productor";
		        changeStyleClass("co.label.usuario_dj.direccion", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.direccion", "labelClass");
		    


		    <c:if test="${idAcuerdo == 26}" >
		    
			    if (document.getElementById("DJ_PRODUCTOR.telefono").value==""){
			        mensaje +="\n -El Tel�fono del productor";
			        changeStyleClass("co.label.usuario_dj.telefono", "errorValueClass");
			    }else changeStyleClass("co.label.usuario_dj.telefono", "labelClass");
		    
		    
			    if ((document.getElementById("DJ_PRODUCTOR.fax").value=="")
			    	&&(document.getElementById("DJ_PRODUCTOR.email").value=="")){
			        mensaje +="\n -El fax � El Email";
			        changeStyleClass("co.label.usuario_dj.fax", "errorValueClass");
			        changeStyleClass("co.label.usuario_dj.email", "errorValueClass");
			    }else {
			    	changeStyleClass("co.label.usuario_dj.fax", "labelClass");
			    	changeStyleClass("co.label.usuario_dj.email", "labelClass");
			    }
			    
			    if (document.getElementById("DJ_PRODUCTOR.pais").value==""){
			        mensaje +="\n -El pa�s del productor";
			        changeStyleClass("co.label.usuario_dj.pais", "errorValueClass");
			    }else changeStyleClass("co.label.usuario_dj.pais", "labelClass");
			    
			    
			    if (document.getElementById("DJ_PRODUCTOR.ciudad").value==""){
			        mensaje +="\n -La Ciudad del productor";
			        changeStyleClass("co.label.usuario_dj.ciudad", "errorValueClass");
			    }else changeStyleClass("co.label.usuario_dj.ciudad", "labelClass");
			        

	        </c:if>	
		    
		    <c:if test="${idAcuerdo != 26}" >
			    if ((document.getElementById("DJ_PRODUCTOR.telefono").value=="")
			    	&&(document.getElementById("DJ_PRODUCTOR.fax").value=="")
			    	&&(document.getElementById("DJ_PRODUCTOR.email").value=="")){
			        mensaje +="\n -El Telefono � El fax � El Email";
			        changeStyleClass("co.label.usuario_dj.telefono", "errorValueClass");
			        changeStyleClass("co.label.usuario_dj.fax", "errorValueClass");
			        changeStyleClass("co.label.usuario_dj.email", "errorValueClass");
			    }else {
			    	changeStyleClass("co.label.usuario_dj.telefono", "labelClass");
			    	changeStyleClass("co.label.usuario_dj.fax", "labelClass");
			    	changeStyleClass("co.label.usuario_dj.email", "labelClass");
			    }

		    </c:if>	
		    
		    if (document.getElementById("DJ_PRODUCTOR.direccionAdicional").value==""){
		        mensaje +="\n -La direcci�n del productor";
		        changeStyleClass("co.label.usuario_dj.direccion_adicional", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.direccion_adicional", "labelClass");
		    
		    mensaje = validaProdXAcuerdo(mensaje);

	    	if (mensaje=="Debe ingresar o seleccionar : ") {

		    	if ($("#rolExportador").val() == "1") {
		    		if (document.getElementById("DJ_PRODUCTOR.documentoTipo").value == document.getElementById("tipoDocumentoSolicitante").value &&
			    		document.getElementById("DJ_PRODUCTOR.numeroDocumento").value == document.getElementById("numDocumentoSolicitante").value) {
			    		mensaje="El solicitante no puede ser registrado como productor.";
			    	}
		    	} else if (document.getElementById("rolExportador").value == "2") {
		    		if (document.getElementById("DJ_PRODUCTOR.documentoTipo").value == document.getElementById("tipoDocumentoSolicitante").value &&
			    		document.getElementById("DJ_PRODUCTOR.numeroDocumento").value == document.getElementById("numDocumentoSolicitante").value) {
			    		if (document.getElementById("DJ_PRODUCTOR.esValidador") != undefined &&
			    			verificarEsValidador() &&
			    			!document.getElementById("DJ_PRODUCTOR.esValidador").checked) {
			    			mensaje="El productor a registrar debe ser validador.";
			    		}
			    	} else {
			    		if (document.getElementById("DJ_PRODUCTOR.esValidador") != undefined && document.getElementById("DJ_PRODUCTOR.esValidador").checked) {
			    			mensaje="El productor a registrar no debe ser validador.";
			    		}
			    	}
		    	}

	    	}

	    	if ($("#trProductorEmail").attr("style") == 'display:' || $("#trProductorEmail").attr("style") == '') {
	    		if (document.getElementById("DJ_PRODUCTOR.email").value!="" && !js_email(document.getElementById("DJ_PRODUCTOR.email").value)){

	    			if (mensaje == "Debe ingresar o seleccionar : ") {
	    				mensaje = "";
	    			} else {
	    				mensaje +="\n\n ";
	    			}
	    			mensaje +="La direcci�n de correo es inv�lida";
			        changeStyleClass("co.label.usuario_dj.email", "errorValueClass");
			    }else changeStyleClass("co.label.usuario_dj.email", "labelClass");
	    	}

		    if (mensaje!="Debe ingresar o seleccionar : ") {
		        ok= false;
		        alert (mensaje);
		    } else {
		    }

	    	return ok;
		}

		function grabarRegistro() {
			var f = document.formulario;

			if (!rucBlur){
		        if(validarCampos()){
		        	$("#DJ_PRODUCTOR\\.documentoTipo").attr('disabled',false).removeClass("readonlyInputTextClass").addClass("inputTextClass");
		        	$("#DJ_PRODUCTOR\\.numeroDocumento").attr('disabled',false).removeClass("readonlyInputTextClass").addClass("inputTextClass");
	                $("#DJ_PRODUCTOR\\.esValidador").attr('disabled',false);

		        	f.action = contextName+"/origen.htm";
			        f.method.value="grabarDJProductor";
			        f.button.value="grabarButton";

		            disableButton("grabarButton", true);
		            disableButton("eliminarButton", true);
		            disableButton("cargarAdjButton", true);
		            disableButton("eliminarAdjButton", true);

		            f.submit();

				} else {
					 rucBlur = false;
					 f.button.value="cancelarButton";
				}

			} else {
				 rucBlur = false;
				f.button.value="cancelarButton";
			}
	    }

		function eliminarRegistro() {
			var f = document.formulario;
	        if (!confirm("�Est� seguro de eliminar este registro?")) {
				f.button.value="cancelarButton";
	        } else{
		        f.action = contextName+"/origen.htm";
		        f.method.value="eliminarDJProductor";
		        f.button.value="eliminarButton";

	            disableButton("grabarButton", true);
	            disableButton("eliminarButton", true);
	            disableButton("cargarAdjButton", true);
	            disableButton("eliminarAdjButton", true);

		        f.target = window.parent.name;
		        f.submit();
	        }
	    }

	    function actualizar() {
	        var f = document.formulario;
	        f.action = contextName+"/origen.htm";
	        f.method.value="cargarDeclaracionJuradaProductor";
	        f.submit();
	    }

        /*function evalCambioTDoc(){
        	if ($("#DJ_PRODUCTOR\\.documentoTipo").val() == '1'){
        		$("#DJ_PRODUCTOR\\.nombre").attr('readonly',true).removeClass("inputTextClass").addClass("readonlyInputTextClass");
        		$("#DJ_PRODUCTOR\\.direccion").attr('readonly',true).removeClass("inputTextClass").addClass("readonlyInputTextClass");
        	} else {
        		$("#DJ_PRODUCTOR\\.nombre").attr('readonly',false).removeClass("readonlyInputTextClass").addClass("inputTextClass");
        		$("#DJ_PRODUCTOR\\.direccion").attr('readonly',false).removeClass("readonlyInputTextClass").addClass("inputTextClass");
        	}
        }*/

	    function limpiarRUC(){
	        $("#DJ_PRODUCTOR\\.numeroDocumento").val("");
	        $("#DJ_PRODUCTOR\\.nombre").val("");
	        $("#DJ_PRODUCTOR\\.direccion").val("");
	        $("#DJ_PRODUCTOR\\.telefono").val("");
	    }

        var alertProductorNoPermitido = 'El rol seleccionado para la DJ no le permite registrarse como productor.';

        function evalCargaData(){
    		rucBlur = true;
    		//alert('evalCargaData');
			if ($.trim($("#DJ_PRODUCTOR\\.documentoTipo").val()) != '' && $.trim($("#DJ_PRODUCTOR\\.numeroDocumento").val()) != '' ) {
        		if (productorNoPermitido()) {
        			alert(alertProductorNoPermitido);
        			limpiarRUC();
        			return false;
        		} else {
        			cargaDatosProductorRUC();
        		}
        	} else {
        		rucBlur = false;
        		return false;
        	}
        }

        function productorNoPermitido(){
        	return ($.trim($("#rolExportador").val()) != '4' && $.trim($("#tipoDocumentoSolicitante").val()) == $.trim($("#DJ_PRODUCTOR\\.documentoTipo").val()) && $.trim($("#numDocumentoSolicitante").val()) == $.trim($("#DJ_PRODUCTOR\\.numeroDocumento").val()));
        }

        // Esta funci�n se encarga de poblar la informaci�n desde SUNAT en base al RUC.
	    function cargaDatosProductorRUC(){
	    	if ( $("#DJ_PRODUCTOR\\.documentoTipo").val() == '1' ) { // S�lo para el caso del RUC
		        var numeroDocumento = document.getElementById("DJ_PRODUCTOR.numeroDocumento").value;
		        var permitido = !productorNoPermitido();
		        limpiarRUC();
		        if (numeroDocumento != "") {
		        	if (permitido) {
			            $("#loadingRUC").show();
			            var filter = "ruc=" + numeroDocumento + "|subPantalla=1";			            
			            loadElementAJX("co.ajax.ruc.elementLoader", "cargarDatosDJProductorRUC", null, filter, null, validaCargaRUC);
		        	} else {
		        		alert(alertProductorNoPermitido);
		        	}
		        }
	    	}
	    	rucBlur = false;
	    }

        // Esta funci�n maneja la l�gica del cargado de informaci�n de SUNAT despu�s de realizada dicha invocaci�n.
	    function validaCargaRUC() {
	        $("#loadingRUC").hide();
	        if( $("#DJ_PRODUCTOR\\.nombre").val() == "" ) {
	            $("#mensajeRUC").html("El n�mero de RUC consultado no es v�lido. Debe verificar el n�mero y volver a ingresar.");
	            $("#DJ_PRODUCTOR\\.numeroDocumento").css('border', '1px solid red');
	    	} else {
	            $("#mensajeRUC").html("");
	            $("#DJ_PRODUCTOR\\.numeroDocumento").css('border', '1px solid #0B2C6F');
			}
		}

        // Maneja la l�gica para habilitar o deshabilitar el nombre y direcci�n seg�n el tipo de documento.
        // Acci�n: 1:inicio 2:combo (indica desde donde se invoca para poder limpiar las cajas de ser necesario)
        function manejarTipoDocumento(valor, accion) {
        	if ( valor != '1' ) { // Si el tipo es DNI se habilitan las cajas de texto.
                $("#DJ_PRODUCTOR\\.nombre").removeClass("readonlyTextAreaClass").addClass("textAreaClass");
                $("#DJ_PRODUCTOR\\.nombre").attr("readonly",false);
                $("#DJ_PRODUCTOR\\.direccion").removeClass("readonlyTextAreaClass").addClass("textAreaClass");
                $("#DJ_PRODUCTOR\\.direccion").attr("readonly",false);

        	} else { // Caso contrario se deshabilitan las cajas de texto
                $("#DJ_PRODUCTOR\\.nombre").removeClass("textAreaClass").addClass("readonlyTextAreaClass");
                $("#DJ_PRODUCTOR\\.nombre").attr("readonly",true);
                $("#DJ_PRODUCTOR\\.direccion").removeClass("textAreaClass").addClass("readonlyTextAreaClass");
                $("#DJ_PRODUCTOR\\.direccion").attr("readonly",true);

        	}
    		if ( accion == 2 ) { // Esto s�lo se realiza cuando se ha cambiado el combo
    			document.getElementById("DJ_PRODUCTOR.numeroDocumento").value = '';
    			document.getElementById("DJ_PRODUCTOR.nombre").value = '';
    			document.getElementById("DJ_PRODUCTOR.direccion").value = '';
    			document.getElementById("DJ_PRODUCTOR.direccionAdicional").value = '';
    			document.getElementById("DJ_PRODUCTOR.telefono").value = '';
    			document.getElementById("DJ_PRODUCTOR.fax").value = '';
    		}
    		manejarLongitudesTipoDocumento(valor);
        }

		// Maneja la l�gica de las longitudes del campo seg�n el tipo de documento ingresado
		function manejarLongitudesTipoDocumento(combo) {
            if ( combo.value == '1' ) { // Si es RUC tiene m�ximo 11
            	$("#DJ_PRODUCTOR\\.numeroDocumento").attr("maxlength",'11');
            } else if ( combo.value == '2' ) { //  Si es DNI tiene m�ximo 8
    			$("#DJ_PRODUCTOR\\.numeroDocumento").attr("maxlength",'8');
            } else if ( combo.value == 3 ) { // Si es CARNET DE EXTRANJER�A tiene m�ximo 15
            	$("#DJ_PRODUCTOR\\.numeroDocumento").attr("maxlength",'15');
            } else if ( combo.value == '' ) { // Si no es ninguno, s�lo puede tener m�ximo 15
            	$("#DJ_PRODUCTOR\\.numeroDocumento").attr("maxlength",'15');
            }
		}

	    // Carga de archivos
	    function cargarArchivo() {
	        var f = document.formulario;
	        if (f.archivo.value != "") {
	            f.action = contextName+"/origen.htm";
	            f.method.value = "cargarArchivoDeclaracionJuradaProductorForm";
	            f.button.value = "cargarButton";
	        } else {
	            alert("Debe seleccionar alg�n archivo");
	            f.button.value = "cancelarButton";
	        }
	    }

	    // Descargar archivos adjuntos
	    function descargarAdjunto(keyValues, keyValuesField) {
	        var f = document.formulario;
	        var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
	        f.action = contextName+"/origen.htm";
	        f.method.value = "descargarAdjunto";
	        f.idAdjunto.value = adjuntoId;
	        f.submit();
	    }

	    function eliminarRegistroAdjunto() {
	        var f = document.formulario;
	        var indice = -1;
	        if (f.seleccione!=null) {
	            if (eval(f.seleccione.length)) {
	                for (i=0; i < f.seleccione.length; i++) {
	                    if (f.seleccione[i].checked){
	                        indice = i;
	                    }
	                }
	            } else {
	                if (f.seleccione.checked) {
	                    indice = 0;
	                }
	            }
	        }
	        if (indice!=-1) {
	            f.action = contextName+"/origen.htm";
	            f.method.value="eliminarDeclaracionJuradaProductorForm";
	            f.button.value="eliminarButton";

	        } else {
	            alert('Seleccione por lo menos un registro a eliminar');
	            f.button.value = 'cancelarButton';
	        }
	    }

        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/origen.htm";
            f.method.value="descargarAdjunto";
            f.idAdjunto.value=adjuntoId;
            f.submit();
        }
	</script>
	<body id="contModal">
		<jlis:modalWindow onClose="actualizar()" />
		<%@include file="/WEB-INF/jsp/helpHint.jsp" %>
	    <jlis:messageArea width="100%" />
	    <br/>
	    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
	    	<input type="hidden" name="method" />
	    	<input type="hidden" name="button">
	    	<jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        <jlis:value name="idFormatoEntidad" type="hidden" />
	        <jlis:value name="calificacionUoId" type="hidden" />
			<jlis:value name="secuenciaProductor" type="hidden" />
			<jlis:value name="secuenciaMercancia" type="hidden" />
			<jlis:value name="bloqueado" type="hidden" />
	        <%-- <jlis:value name="modoProductorValidador" type="hidden" />
	        <jlis:value name="esValidador" type="hidden" />
	        <jlis:value name="modoFormato" type="hidden" /> --%>
	        <jlis:value name="idAcuerdo" type="hidden" />
	        <jlis:value name="rolExportador" type="hidden" />
	        <jlis:value name="tipoDocumentoSolicitante" type="hidden" />
	        <jlis:value name="numDocumentoSolicitante" type="hidden" />
	        <jlis:value name="djEnProcesoValida" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <input type="hidden" name="idAdjunto" />
	        <jlis:value name="tempPaisProductor" type="hidden" />

	        <c:if test="${rolExportador != 1}" >
	        	<jlis:value name="DJ_PRODUCTOR.esValidador" type="hidden" />
	        </c:if>

			<co:validacionBotonesFormato formato="DJ" pagina="formDJProductor" />
			<table class="form">
				<tr>
					<td colspan="2">
						<jlis:button code="CO.USUARIO.OPERACION" id="grabarButton" name="grabarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar Productor" alt="Pulse aqu� para grabar el productor " />
					    <jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Productor" alt="Pulse aqu� para eliminar el productor " />
        							<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<tr>
					<td colspan="2"><span class="requiredValueClass"><jlis:label key="co.label.dj.subtitulo.datos_productor" /></span></td>
				</tr>
                <tr>
                    <th><jlis:label key="co.label.usuario_dj.documento_tipo" /></th>
					<%-- <td><jlis:selectProperty name="DJ_PRODUCTOR.documentoTipo" key="comun.tipo_documento.juridica.select" editable="yes" onChange="evalCambioTDoc()" /><span class="requiredValueClass">(*)</span></td> --%>
					<td><jlis:selectProperty name="DJ_PRODUCTOR.documentoTipo" key="comun.tipo_documento_inc_dni_carnet.juridica.select" editable="yes" onChange="manejarTipoDocumento(this.value,2)" /><span class="requiredValueClass">(*)</span></td>
                </tr>
				<tr>
                    <th><jlis:label key="co.label.usuario_dj.numero_documento" /></th>
					<td><jlis:value name="DJ_PRODUCTOR.numeroDocumento" size="15" maxLength="15" type="text" onChange="cargaDatosProductorRUC()" onBlur="evalCargaData()"/><span class="requiredValueClass">(*)</span><img id="loadingRUC" style="display:none" src="/co/imagenes/loading.gif"/></td>
                </tr>

				<tr id="trProductorPais" style="display:none"><!-- JMC 28/04/2017 Alianza -->
					<th><jlis:label key="co.label.usuario_dj.pais" /></th>
					<td >
						<jlis:selectProperty name="DJ_PRODUCTOR.pais" key="comun.pais_iso_alfa2.select"/><span id="spanSelectPaisImportador"  class="requiredValueClass">(*)</span>
					</td>
				</tr>                 
                <tr>
                    <th><jlis:label key="co.label.usuario_dj.nombre" /></th>
                    <td>
                    	<jlis:textArea name="DJ_PRODUCTOR.nombre" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);" editable="no" /><span class="requiredValueClass">(*)</span>
                    </td>
                </tr>
                <tr>
                    <th><jlis:label key="co.label.usuario_dj.direccion" /></th>
                    <td>
                    	<jlis:textArea name="DJ_PRODUCTOR.direccion" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);" editable="no" /><span class="requiredValueClass">(*)</span>
          			                <%--co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.PRODUCTOR.DIRECCION" /--%>
                    </td>
                </tr>
                
                <tr>
                    <th><jlis:label key="co.label.usuario_dj.direccion_adicional" /></th>
                    <td>
                    	<jlis:textArea name="DJ_PRODUCTOR.direccionAdicional" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);"  /><span class="requiredValueClass">(*)</span>
          			               <c:if test="${idAcuerdo != 26}" ><co:mostrarAyuda etiqueta="MCT001.DJ.PRODUCTOR.DIRECCION_ADICIONAL" /></c:if>	
          			               <c:if test="${idAcuerdo == 26}" ><co:mostrarAyuda etiqueta="MCT001.26.DJ.PRODUCTOR.DIRECCION_ADICIONAL" /></c:if>	
          			                
                    </td>
                </tr>
				
                <tr id="trProductorCiudad" style="display:none"><!-- JMC 28/04/2017 Alianza -->
                    <th><jlis:label key="co.label.usuario_dj.ciudad" /></th>
                    <td>
                    	<jlis:value name="DJ_PRODUCTOR.ciudad" size="50" maxLength="50" type="text" /><span class="requiredValueClass">(*)</span>
                    </td>
                </tr>				             
                
                <tr id="trProductorTelefono" >
                    <th><jlis:label key="co.label.usuario_dj.telefono" /></th>
                    <td>
                    	<jlis:value name="DJ_PRODUCTOR.telefono" size="25" maxLength="20" type="text" />
                    	<span class="requiredValueClass">
                    		<c:if test="${idAcuerdo == 26}" >
                    	   		(*)
                    	   	</c:if>	
                    		<c:if test="${idAcuerdo != 26}" >
                    	   		(**)
                    	   	</c:if>	
                    	</span>
                    </td>
                </tr>
                <tr id="trProductorFax" >
                    <th><jlis:label key="co.label.usuario_dj.fax" /></th>
                    <td>
                    	<jlis:value name="DJ_PRODUCTOR.fax" size="25" maxLength="20" type="text" /><span class="requiredValueClass">(**)</span>
                    </td>
                </tr>
                
				<tr id="trProductorEmail" >
					<th><jlis:label key="co.label.usuario_dj.email" /></th>
					<td>
						<jlis:value name="DJ_PRODUCTOR.email" size="80" maxLength="70" /><span class="requiredValueClass">(**)</span>
					</td>
				</tr>
				
                <c:if test="${rolExportador == 1}" >
	                <tr id="trEsValidador" style="display:none" >
	                    <th><jlis:label key="co.label.usuario_dj.validador" /></th>
	                    <td>
	                    	<jlis:value type="checkbox" name="DJ_PRODUCTOR.esValidador" checkValue="S" />&nbsp;<co:mostrarAyuda etiqueta="MCT001.1.DJ.INFORMACION.ES_VALIDADOR" />
	          			                <co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.INFORMACION.ES_VALIDADOR" />
	                    </td>
	                </tr>
                </c:if>
				<tr>
					<td colspan="2" style="color:red; text-align:rigth;padding:10px;">						
							<span class="requiredValueClass">(**)  Obligatorio al menos registrar un dato solicitado.</span>						
					</td>
				</tr>
                <tr class="ruc"><td colspan="2" ><span id="mensajeRUC" class="requiredValueClass"></span></td></tr>
			</table>
			
			<table id="tablaSubtituloAdjunto" style="display:none">
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="requiredValueClass"><jlis:label key="co.label.dj.productor.subtitulo.adjunto" /></span>
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.##.JUSTIFICACION" material="si" codigoPais="${tipoPais}" acuerdoId="${idAcuerdo}" />
					</td>
				</tr>
            </table>
			<table id="tablaBotonesAdjunto" style="display:none">
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
                <tr>
					<td><jlis:button code="CO.USUARIO.OPERACION" id="cargarAdjButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
					<td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarAdjButton" name="eliminarRegistroAdjunto()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
                </tr>
            </table>
            <table id="tablaMensajesAdjunto" style="display:none">
                <tr>
                    <td>
                    <jlis:label key="co.label.documentos_adjuntar"/> <br />
                    <jlis:label key="co.label.material.tipo_archivos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                     <input type="file" id="archivo" name="archivo" size="87"  />
                    </td>
                </tr>
            </table>
	        <br/>

			<span id="tablaDJProductorAdjuntos" style="display:none">
		        <jlis:table name="ADJUNTOS" keyValueColumns="ADJUNTOID" source="tAdjDJProductor" scope="request"
		            pageSize="*" navigationHeader="no" width="98%" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCopiadosTramitePadreRowValidator" >
		            <jlis:tr>
		                <jlis:td name="ADJUNTO ID" nowrap="yes" />
		                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" width="83%" />
	              	 	<jlis:td name="TAMA�O (KB)" nowrap="yes" width="7%" />
		              	<jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
		            </jlis:tr>
		            <jlis:columnStyle column="1" columnName="ADJUNTOID" editable="no" hide="yes"/>
		            <jlis:columnStyle column="2" columnName="NOMBREARCHIVO" editable="yes" />
	           		<jlis:columnStyle column="3" columnName="TAMANO" />
		            <jlis:columnStyle column="4" columnName="SELECCIONE" align="center" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCellValidator" editable="yes" />
		            <jlis:tableButton column="2" type="link" onClick="descargarAdjunto" highlight="no" />
		            <jlis:tableButton column="4" type="checkbox" name="seleccione" />
		        </jlis:table>
			</span>
		</form>
	</body>
</html>