<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Seleccionar Acuerdo</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <script>

		// Valida la informaci�n general de la orden del certificado de origen
		function validarCampos() {

			var mensaje="Debe ingresar o seleccionar : ";
		    var ok = true;

		    if ( document.getElementById("acuerdo").value == "" ) {
		        mensaje +="\n -El Acuerdo Internacional";
		        changeStyleClass("co.label.acuerdo", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.acuerdo", "labelClass");
		    }
		    if ( document.getElementById("pais").value == "" ) {
		        mensaje +="\n -El Pa�s del Acuerdo";
		        changeStyleClass("co.label.pais_acuerdo", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.pais_acuerdo", "labelClass");
		    }
		    if ( document.getElementById("entidadCertificadora").value == "" ) {
		        mensaje +="\n -La Entidad Certificadora";
		        changeStyleClass("co.label.entidad_certificadora", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.entidad_certificadora", "labelClass");
		    }
		    /*if ( document.getElementById("sede").value == "" ) {
		        mensaje +="\n -La Sede de la Entidad Certificadora";
		        changeStyleClass("co.label.sede_entidad_certificadora", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.sede_entidad_certificadora", "labelClass");
		    }
		    */

		    if ( mensaje != "Debe ingresar o seleccionar : " ) {
		        ok = false;
		        alert(mensaje);
		    }
		    return ok;
		}

		// Envia a la p�gina de creaci�n de orden
	    function crearOrden() {
	        var f = document.formulario;
	        //if (validarCampos()) {
	        
		        f.action = contextName+"/origen.htm";
		        f.method.value = "nuevaOrden";
	        /*} else {
	        	return false;
	        }*/
	    }

		// Carga los paises en base al acuerdo seleccionado
		function cargarPaisAcuerdo(obj) {
			var f = document.formulario;
			var filter = obj.value;
			loadSelectAJX("co.ajax.certificado_origen.paises_acuerdo.selectLoader", "obtenerPaisesAcuerdo", "pais", null, filter, null, null, null);
		}

		// Carga los acuerdos en base al pais seleccionado
		function cargarlistaAcuerdos(obj) {
			var f = document.formulario;
			var filter = obj.value;
			$("#mensajeFD").hide();
	        inicializaTipoCertificado();
			loadSelectAJX("co.ajax.certificado_origen.acuerdos_pais.selectLoader", "obtenerListadoAcuerdos", "acuerdo", null, filter, null, null, null);
		}
        
		// Carga las sedes en base a la entidad seleccionada
		function cargarSedesEntidad(obj) {
			var f = document.formulario;
            var filter = "entidad=" + obj.value;
            loadSelectAJX("ajax.selectLoader", "loadList", "sede", "comun.sede_x_entidad.select", filter, null, null,null);
		}

		// Como el bot�n es de tipo submit, manejamos el caso que no se hayan validado los campos
		function validarSalida() {
	        var f=document.formulario;
	        f.target = "_self";
			if (!validarCampos()) {
				return false;
			}
			return true;
		}
		
		function seleccionTipoCertificado() {
			var f = document.formulario;
			var tipoCertificado = getSelectedRadioValue(f.tipoCertificado);
			
			// Si es Certificado de Origen
			if (tipoCertificado==1) {
				document.getElementById("certificadoOrigen").value="S";
				document.getElementById("certificadoReexportacion").value="N";
	            //loadSelectAJX("ajax.selectLoader", "loadList", "pais", "comun.pais_iso.acuerdo_vigente.select", null, null, null,null);
			}
			// Si es Certificado de Reexportacion
			else if (tipoCertificado==2) {
				document.getElementById("certificadoReexportacion").value="S";
				document.getElementById("certificadoOrigen").value="N";
				//loadSelectAJX("ajax.selectLoader", "loadList", "pais", "comun.pais_iso.acuerdo_vigente.reexportacion.select", null, null, null,null);
			}
		}
		
		function inicializaTipoCertificado(){
			$("#tr_tipoCertificado").hide();
			$('input:radio[name=tipoCertificado]')[0].checked = true;
	    	document.getElementById("certificadoOrigen").value="S";
			document.getElementById("certificadoReexportacion").value="N";
		}
		
		function validaAcuerdo(obj){
			var f = document.formulario;
			if (f.formato.value == "mct005") {
				$("#tr_tipoCertificado").hide();
				return;
			}
			
			var filter = obj.value;
			document.getElementById("aceptaReexportacion").value = "";
	        loadElementAJX("co.ajax.certificado.aceptaReexportacion.elementLoader", "aceptaReexportacion", null, filter, null, afterValidaAcuerdo);

		}
		
		function afterValidaAcuerdo(){
			var aceptaReexportacion = document.getElementById("aceptaReexportacion").value;
			inicializaTipoCertificado();
			if(aceptaReexportacion == "S"){
				$("#tr_tipoCertificado").show();
			}else{
				$("#tr_tipoCertificado").hide();
			}
			validarMensajeFirmaDigital();
		}


		//NPCS 26/02/2019 CO-2019-001 IO Alianza
		function validarMensajeFirmaDigital(){
			var f = document.formulario;
			var filter = f.idFormato.value+"%7C"+f.pais.value+"%7C"+f.acuerdo.value+"%7C"+f.entidadCertificadora.value;
			loadElementAJX("co.ajax.certificado.seleccion_acuerdo.elementLoader", "mostrarMensajeFD", null, filter, null, afterValidaMensajeFD);
			}

		function afterValidaMensajeFD(){
			var f = document.formulario;
			if(f.mostrarMensajeFD.value=="S") {
				$("#mensajeFD").html("La "+$('#entidadCertificadora option:selected').text()+" est� en proceso de habilitar la firma digital, por ello usted deber� firmar el documento de forma manuscrita (f�sica)");
				$("#mensajeFD").show();
			}
			else $("#mensajeFD").hide();
			}
		
		
    </script>
    <body>
    	<div id="body">
     		<jsp:include page="/WEB-INF/jsp/header.jsp" />
    		<!-- CONTENT -->
			<div id="contp"><div id="cont">
        		<form name="formulario" method="post" onsubmit="return validarSalida()" >
				    <input type="hidden" name="method" />

				    <jlis:value type="hidden" name="idTupa" />
				    <jlis:value type="hidden" name="idEntidad" />
				    <jlis:value type="hidden" name="idFormato" />
				    <jlis:value type="hidden" name="formato" />
				    <jlis:value name="mostrarMensajeFD" type="hidden"  />
				    <jlis:value name="certificadoOrigen" type="hidden" value="S"/>
                    <jlis:value name="certificadoReexportacion" type="hidden" value="N"/>
                    <jlis:value type="hidden" name="aceptaReexportacion" />

				    <div id="pageTitle">
				        <h1><strong><jlis:value name="nombreFormato" editable="no" valueClass="pageTitle" /></strong></h1>
				    </div>

		            <div id="pageTitle">
            			<h1><strong><jlis:label labelClass="pageTitle"  key="co.title.seleccione_acuerdo" /></strong></h1>
            		</div>
                    
		            <br/>
                    
					<div>
						<p align="center" >
							<table width="40%">
								<tr>
									<td align="left" width="30%" colspan="2" >&nbsp;</td>
								</tr>
                                
                                <tr>
                                    <td align="left" width="30%" colspan="2" >&nbsp;</td>
                                </tr>
								<tr>
									<td align="left" width="30%" ><jlis:label key="co.label.pais_acuerdo" style="font-weight: bold; align:left;" /></td>
									<td align="right" width="70%" ><jlis:selectProperty key="comun.pais_iso.acuerdo_vigente.select" name="pais" style="align:right;" onChange="cargarlistaAcuerdos(this);" /></td>
								</tr>
								<tr>
									<td align="left" width="30%" colspan="2" >&nbsp;</td>
								</tr>
								<tr>
									<td align="left" width="30%" ><jlis:label key="co.label.acuerdo" style="font-weight: bold; "/></td>
									<td align="right" width="70%" ><jlis:selectProperty key="select.empty" name="acuerdo" style="align:right;" onChange="validaAcuerdo(this);"/></td>
								</tr>
								<tr>
									<td align="left" width="30%" colspan="2" >&nbsp;</td>
								</tr>
								<tr>
									<td align="left" width="30%" ><jlis:label key="co.label.entidad_certificadora" style="font-weight: bold; align:left;"/></td>
									<td align="right" width="70%" ><jlis:selectProperty key="comun.entidad.select" name="entidadCertificadora" style="align:right;" onChange="validarMensajeFirmaDigital();"/></td>
								</tr>
								<tr>
									<td align="left" colspan="2" >&nbsp;
									<div id="mensajeFD" style="color:#1e90ff; display:none;"></div>
									
									</td>
								</tr>
								<%-- <tr>
									<td align="left" width="30%" colspan="2" >&nbsp;</td>
								</tr>
								<tr>
									<td align="left" width="30%" ><jlis:label key="co.label.sede_entidad_certificadora" style="font-weight: bold; align:left;"/></td>
									<td align="right" width="70%" ><jlis:selectProperty key="select.empty" name="sede" style="align:right;" /></td>
								</tr> --%>
								<tr><td colspan="2">&nbsp;</td></tr>
								<tr id="tr_tipoCertificado" style="display:none">
                                    <td align="center" style="text-align: center" width="30%" colspan="2" nowrap >
                                        <input type="radio" name="tipoCertificado" value="1" onClick="seleccionTipoCertificado()" checked ><jlis:label key="co.label.certificado_origen" style="font-weight: bold; align:left;" />&nbsp;
                                        <input type="radio" name="tipoCertificado" value="2" onClick="seleccionTipoCertificado()"><jlis:label key="co.label.certificado_reexportacion" style="font-weight: bold; align:left;" />
                                    </td>
                                </tr>

								<tr>
									<td align="right" width="30%" >&nbsp;</td>
									<td align="right" width="70%" >
										<jlis:button id="regresarButton" editable="always" name="listarCertificadoOrigen()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para regresar" />
										&nbsp;<jlis:button code="CO.USUARIO.OPERACION" id="nuevaButton" name="crearOrden()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Continuar" alt="Pulse aqu� para crear un nuevo Certificado de Origen" />
									</td>
								</tr>
							</table>
						</p>
					</div>

			    </form>
        	</div></div>
         	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>