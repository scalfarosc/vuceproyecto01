<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema CO - Principal</title>
        <meta http-equiv="Pragma" content="no-cache" /> 
    </head>
    <c:set var="activeNS" value="active" scope="request" />
    <script language="JavaScript">

    function crearOrden(keyValues, keyValuesField) {
        var f = document.formulario;
        var formatoid = document.getElementById(keyValues.split("|")[0]).value;
        var entidad = document.getElementById(keyValues.split("|")[1]).value;
        var formato = document.getElementById(keyValues.split("|")[2]).value;
        var idTupa = document.getElementById(keyValues.split("|")[3]).value;
        var frmlow = formato.toLowerCase();
        //f.action = contextName+"/"+frmlow+".htm?method=nuevaOrden&idFormato="+formatoid+"&idEntidad="+entidad+"&idTupa="+idTupa;
        f.method.value = "cargarFormatoTupa";
        f.idFormato.value = formatoid;
        f.idEntidad.value = entidad;
        f.idTupa.value = idTupa;
        f.formato.value = frmlow;
        f.action = contextName+"/origen.htm";
        f.submit();
    }

    </script>
    <body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
	
    <form name="formulario" method="post" onSubmit="return validarSubmit();">
        <jlis:value type="hidden" name="idFormato" />
        <jlis:value type="hidden" name="idEntidad" />
        <jlis:value type="hidden" name="idTupa" />
        <jlis:value type="hidden" name="formato" />
		<jlis:value type="hidden" name="method" />
		
		<jlis:value type="hidden" name="entidad" />
		
		<jlis:messageArea width="100%" />
			            <br/>
			            <div id="pageTitle">
			            <h1><strong><jlis:label  labelClass="pageTitle"  key="co.title.tupas" /></strong></h1>
			            </div>
			            <br/>
			            <jlis:table keyValueColumns="FORMATO_ID,ENTIDAD_ID,FORMATO,TUPA_ID" name="FORMATOS" source="tFormatos" scope="request" pageSize="15" cellPadding="2" width="100%">
				            <jlis:tr>
				                <jlis:td name="FORMATO ID" nowrap="yes" />
				                <jlis:td name="ENTIDAD ID" nowrap="yes" />
				                <jlis:td name="TUPA" nowrap="yes" width="6%" />
				                <jlis:td name="FORMATO" nowrap="yes" width="6%" />
			                    <jlis:td name="NOMBRE" nowrap="yes" width="70%" />
			                    <jlis:td name="TOTAL SOLICITUDES" nowrap="yes" width="6%" />
			                    <jlis:td name="ESTADO" nowrap="yes" width="6%" />
			                    <jlis:td name="TUPA_ID" nowrap="yes" width="6%" />
			                    <jlis:td name="ESPECIFICACION PAGO" nowrap="yes" width="20%" />
                                <jlis:td name="PLAZO" nowrap="yes" width="5%" />
				            </jlis:tr>
				            <jlis:columnStyle column="1" columnName="FORMATO_ID" hide="yes"/>
				            <jlis:columnStyle column="2" columnName="ENTIDAD_ID" hide="yes"/>
				            <jlis:columnStyle column="3" columnName="TUPA" align="center" nowrap="yes"/>
				            <jlis:columnStyle column="4" columnName="FORMATO" align="center" />
				            <jlis:columnStyle column="5" columnName="NOMBRE" editable="yes" />
				            <jlis:columnStyle column="6" columnName="TOTAL" type="int" align="center" style="font-size:12px; color:blue; font-weight:600" hide="yes" />
				            <jlis:columnStyle column="7" columnName="ESTADO" align="center" hide="yes"/>
				            <jlis:columnStyle column="8" columnName="TUPA_ID" align="center" hide="yes"/>
				            <jlis:columnStyle column="9" columnName="ESPECIFICACION_PAGO" />
                            <jlis:columnStyle column="10" columnName="PLAZO" align="center" />
							<jlis:tableButton column="5" type="link" onClick="crearOrden" />
				        </jlis:table>
				        
        </form>
        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>