<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema VUCE</title>
</head>

<script language="JavaScript" src="${pageContext.request.contextPath}/resource/js/subModal/subModal.js"></script>
<script>

	// Inicializaciones de JQuery
	$(document).ready(function() {
		tituloPopUp("Subsanaci�n de Solicitud");
		validaCambios();
	});

 	// Validaciones antes de realizar el submit
	function validarSubmit() {
	    var f = document.formulario;

	    if (f.button.value=="cancelarButton") {
            return false;
        }
	    if (f.button.value=="cerrarPopUpButton") {
    		window.parent.hidePopWin(false);
        }
	    if (f.button.value=="crearSubsanacionSolicitudButton") {
        	//return validaCerraPopUp();
        }

	    return true;
	}

    function validarSeleccionNotificacion() {
    	var tableSize = parseInt(document.getElementById("tableSize_notificaciones").value);
        var ok = false;
        for (var i=1; i <= tableSize; i++) {
            ok = ok || document.getElementById("tableCheckBox_"+i+",notificaciones.SELECCIONE").checked;
        }
        if (!ok) {
            alert("Debe seleccionar una notificaci�n");
        }
        return ok;
    }

    function crearSubsanacion(){
		var f = document.formulario;
        if ( validarSeleccionNotificacion() ) {

    		f.action = contextName+"/origen.htm";
            f.method.value="crearSubsanacionSolicitud";
            f.button.value="crearSubsanacionSolicitudButton";
            f.target = window.parent.name;

    	} else{

    		f.button.value = "cancelarButton";
    		//return false;

    	}

    }

</script>

	<body id="contModal">
	    <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
	        <jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <jlis:value name="estadoAcuerdoPais" type="hidden" />
	        <table>
	             <tr>
		             <%-- 20170131_GBT ACTA CO-004-16 3.2.a ACTA CO 009-16 --%>
		             <%-- gbt code="CO.USUARIO.OPERACION" id="crearSubsanacionButton" name="crearSubsanacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Crear Subsanaci�n" alt="Pulse aqu� para crear la modificaci�n de la Solicitud-aqui" --%>
				     <c:if test="${estadoAcuerdoPais == 'I'}">
 		             	<td><jlis:button code="CO.USUARIO.OPERACION" id="crearSubsanacionButton" editable="NO" name="crearSubsanacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Crear Subsanaci�n" alt="Pulse aqu� para crear la modificaci�n de la Solicitud" /></td>
	              	 </c:if>
	              	 <c:if test="${estadoAcuerdoPais != 'I'}">
 		             	<td><jlis:button code="CO.USUARIO.OPERACION" id="crearSubsanacionButton" name="crearSubsanacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Crear Subsanaci�n" alt="Pulse aqu� para crear la modificaci�n de la SolicitudGBT" /></td>
	              	 </c:if>
	             
				     <td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" /></td>
	             </tr>
	        </table>
			<table class="form">

				<tr>
					<jlis:table keyValueColumns="NOTIFICACION_ID" name="notificaciones" source="tNotificaciones" scope="request" pageSize="*" width="60%" navigationHeader="no" >
		            	<jlis:tr>
		                    <jlis:td name="ID" nowrap="yes" width="5%"/>
		                    <jlis:td name="MENSAJE" nowrap="yes" width="70%" />
		                    <jlis:td name="FECHA REGISTRO" nowrap="yes" width="25%"/>
		                    <jlis:td name="" width="1%" style="valueCheckbox=SELECCIONE,on-off,S,N"  />
		                </jlis:tr>
		                <jlis:columnStyle column="1" columnName="NOTIFICACION_ID" hide="yes"/>
		                <jlis:columnStyle column="2" columnName="MENSAJE_ENTIDAD" editable="no" />
		                <jlis:columnStyle column="3" columnName="FECHA_REGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" />
		                <jlis:columnStyle column="4" columnName="SELECCIONE" editable="yes" align="center" value="S" />
		                <jlis:tableButton column="4" type="valueCheckbox" uncheckedValue="N" />
					</jlis:table>
				</tr>
			</table>

		</form>
	</body>
</html>