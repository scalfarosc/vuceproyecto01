<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema VUCE</title>
</head>

<script language="JavaScript" src="${pageContext.request.contextPath}/resource/js/subModal/subModal.js"></script>
<script>

	// Inicializaciones de JQuery
	$(document).ready(function() {
		tituloPopUp("Calificar Validación DJ");
		validaCambios();
	});

 	// Validaciones antes de realizar el submit
	function validarSubmit() {
	    var f = document.formulario;

	    if (f.button.value=="cancelarButton") {
            return false;
        }
	    if (f.button.value=="cerrarPopUpButton") {
        	return validaCerraPopUp();
        }
	    return true;
	}

 	// Resolver la Validación de Declaración Jurada
	function resolverValidacionDJ(respuesta) {
		var f = document.formulario;
        f.respuesta.value = respuesta;
        f.action = contextName + "/origen.htm";
        f.method.value = "resolverCalificacionValidacionDJ";
        f.button.value = "resolverButton";
        f.target = window.parent.name;
        f.submit();
    }

</script>

	<body id="contModal">
	    <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="secuencia" type="hidden" />
	        <jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        <jlis:value name="idAcuerdo" type="hidden" />
	        <jlis:value name="respuesta" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
	        <jlis:value name="djId" type="hidden" />
	        <jlis:value name="idEntidad" type="hidden" />

			<table class="form">

				<tr>
					<th colspan="4" ><jlis:label key="co.label.dj.calificacion.validacion" /></th>
				</tr>

				<tr>
					<td width="20%" >&nbsp;</td>
					<td width="30%" ><img id="imgAcepta" src="/co/imagenes/check_aceptar.gif" onClick="resolverValidacionDJ('S')" style="cursor: pointer;" title="Aceptar la Validación de la Declaración Jurada" /></td>
					<td width="30%" ><img id="imgRechaza" src="/co/imagenes/rechazar.png" onClick="resolverValidacionDJ('N')" style="cursor: pointer;" title="Rechaza la Validación de la Declaración Jurada" /></td>
					<td width="20%" >&nbsp;</td>
				</tr>
			</table>

		</form>
	</body>
</html>