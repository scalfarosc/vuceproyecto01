<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript">


    function validarSubmit() {
        var f=document.formulario;

        if (f.button.value=="cerrarPopUpButton") {
            validaCerraPopUp();
        }
        return true;
    }

</script>

<body id="contModal">
    <jlis:messageArea width="100%" /><br/>
    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
        <h3 class="psubtitle"><span><b><jlis:value name="titulo" editable="no" /></b></span></h3>
       	<table>
	    	<tr>
	        	<td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana" /></td>
			</tr>
		</table>
        <br/>
        <jlis:table name="PAISES" keyValueColumns=""  key="${key}" scope="request"
            pageSize="*" navigationHeader="yes" width="98%" filter="${filterG2}">
            <jlis:tr>
                <jlis:td name="ID" nowrap="yes" />
                <jlis:td name="PAIS" nowrap="yes" />
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="ID" hide="yes" />
            <jlis:columnStyle column="2" columnName="NAME" editable="no" />
        </jlis:table>
    </form>
    <script>
        window.parent.document.getElementById("popupTitle").innerHTML = "Paises";
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";

    </script>
</body>
</html>