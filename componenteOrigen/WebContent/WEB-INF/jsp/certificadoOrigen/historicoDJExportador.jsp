<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Solicitudes Asignadas</title>
        <meta http-equiv="Pragma" content="no-cache" />
        <script type="text/javascript">
			$(document).ready(function() {
				bloquearControles();
			});


			function validarSubmit() {
			    var f = document.formulario;

			    if (f.button.value=="cerrarPopUpButton") {
		        	return validaCerraPopUp();
		        }
			    return true;
			}
        </script>
    </head>
    <body id="contModal">
    <form name="formulario" method="post" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
			<table class="form">
				<tr>
					<td colspan="2" >
						<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana." />
					</td>
				</tr>

				<tr>
					<td colspan="2" ><hr/></td>
				</tr>
				<tr>
					<th width="45%" ><jlis:label key="co.label.dj.historico.numero.documento" /></th>
					<td width="55%" ><jlis:value name="numeroDocumento" size="20" maxLength="20" type="text"/>
				</tr>
				<tr>
					<th width="45%" ><jlis:label key="co.label.dj.historico.razon.social" /></th>
					<td width="55%" ><jlis:value name="razonSocial" size="70" maxLength="70" type="text"/>
				</tr>
			</table>
		    <jlis:table keyValueColumns="FECHA_REGISTRO" name="HISTORICO_DJ" source="tDJHistoricoExp" scope="request" pageSize="6" divStyle="overflow:auto;height:250px" fixedHeader="yes" navigationHeader="yes" width="90%" >
		    	<jlis:tr>
			        <jlis:td name="TIPO ACCION" nowrap="yes" />
			        <jlis:td name="REGISTRO" nowrap="yes" />
			        <jlis:td name="INICIO AUTORIZACI&Oacute;N" nowrap="yes" />
			        <jlis:td name="FIN AUTORIZACI&Oacute;N" nowrap="yes" />
		     	</jlis:tr>
		    	<jlis:columnStyle column="1" columnName="TIPO_ACCION" />
			    <jlis:columnStyle column="2" columnName="FECHA_REGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
			    <jlis:columnStyle column="3" columnName="INICIO_AUTORIZACION" type="dateTime" pattern="dd/MM/yyyy" />
			    <jlis:columnStyle column="4" columnName="FIN_AUTORIZACION" type="dateTime" pattern="dd/MM/yyyy" />
		     </jlis:table>
	</form>
    </body>
</html>
