<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript">

    function retornarAnt(keyValues, keyValuesField){
        var f = window.parent;

        var djId = document.getElementById(keyValues.split('|')[0]).value;
        var umFisicaId = document.getElementById(keyValues.split('|')[1]).value;
        var denominacion = document.getElementById(keyValues.split('|')[2]).value;
        var partidaArancelaria = document.getElementById(keyValues.split('|')[3]).value;
        var denominacionArancelaria = document.getElementById(keyValues.split('|')[4]).value;
        var dj = document.getElementById(keyValues.split('|')[5]).value;
        var naladisa = document.getElementById(keyValues.split('|')[6]).value;

		f.cargarDataDJ(djId, umFisicaId, denominacion, partidaArancelaria, denominacionArancelaria, undefined, dj, naladisa);

        f.hidePopWin(false);
   }

    function retornar(keyValues, keyValuesField){
        var f = window.parent;

        var calificacionUoId = document.getElementById(keyValues.split('|')[1]).value;

		f.cargarDataDJ(calificacionUoId, document.formulario.borrador.value);

        f.hidePopWin(false);
   }

    function buscar(){
	   var f = document.formulario;
	   f.action = contextName;
	   f.action += "/origen.htm";
	   //f.action = contextName+"/sns001.htm";
	   f.method.value="obtenerDJs";
	}

    function seleccionOpcion(obj) {
        if (obj.value=="0") {
        	$('#codigo').attr('readonly',false);
            $('#codigo').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#codigo').focus();
            $('#descripcion').attr('readonly',true);
            $('#descripcion').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#descNomCientifico').attr('readonly',true);
            $('#descNomCientifico').removeClass("inputTextClass").addClass("readonlyInputTextClass");
        } else if (obj.value=="1") {
            $('#codigo').attr('readonly',true);
            $('#codigo').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#descripcion').attr('readonly',false);
            $('#descripcion').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#descripcion').focus();
            $('#descNomCientifico').attr('readonly',true);
            $('#descNomCientifico').removeClass("inputTextClass").addClass("readonlyInputTextClass");
        } else if (obj.value=="2") {
        	$('#codigo').attr('readonly',true);
            $('#codigo').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#descripcion').attr('readonly',true);
            $('#descripcion').removeClass("inputTextClass").addClass("readonlyInputTextClass");
            $('#descNomCientifico').attr('readonly',false);
            $('#descNomCientifico').removeClass("readonlyInputTextClass").addClass("inputTextClass");
            $('#descNomCientifico').focus();
        }
    }

    $(document).ready(function() {
    	tituloPopUp('B�squeda de Mercanc�a con DJ Vigente X Entidad');
    	$('#descripcion').focus();
    	if ( $("#formato").val() == 'MCT005' || $("#formato").val() == 'mct005') {
    		$('#id_mct005').show();
    	}
	});

    function validarSubmit() {
		var f = document.formulario;
		if (f.button.value=="cancelarButton") {
			return false;
        }
		if (f.button.value=="cerrarPopUpButton") {
			return validaCerraPopUpNoAction();
        }
	    if ( f.button.value=="busqDJExistenteButton" ) {
        	return false;
        }
		return true;
	}

</script>
<body id="contModal">
<jlis:messageArea width="100%" />
<form name="formulario" method="post"  onSubmit="return validarSubmit();">
    <input type="hidden" name="method" />
    <jlis:value type="hidden" name="origen" />
    <jlis:value type="hidden" name="clase" />
	<input type="hidden" name="button" value="" />
    <jlis:value type="hidden" name="codigoProducto" />
    <jlis:value type="hidden" name="tBusq" />
    <jlis:value type="hidden" name="entidad" />
    <jlis:value type="hidden" name="acuerdo" />
    <jlis:value type="hidden" name="orden" />
    <jlis:value type="hidden" name="mto" />
    <jlis:value type="hidden" name="borrador" />
    <jlis:value type="hidden" name="formato" />
	<table class="form">
		<tr>
			<th><jlis:label key="co.label.mercancia.busqueda.dj"/></th>
			<td><jlis:value name="dj" size="80"  type="text"/></td>
		</tr>
		<tr>
			<th><jlis:label key="co.label.mercancia.busqueda.denominacion"/></th>
			<td><jlis:textArea name="denominacion" cols="40" rows="2" /></td>
		</tr>
		<tr>
			<th><jlis:label key="co.label.mercancia.busqueda.partida_arancelaria"/></th>
			<td><jlis:value name="partida" size="50" type="text" /></td>
		</tr>
		<tr>
			<th><jlis:label key="co.label.mercancia.busqueda.fecha_inicio_desde"/></th>
			<td><jlis:date name="fechaDesde" form="formulario" />
			&nbsp;&nbsp;<span id="id_mct005" style="display:none"><b><jlis:label key="co.label.mercancia.busqueda.entidad_certificadora"/></b>
			&nbsp;<jlis:selectProperty key="comun.entidad.select" name="entidadCertificadora" style="width=230"  /></span></td>
			
		</tr>
		<tr>
			<th><jlis:label key="co.label.mercancia.busqueda.fecha_inicio_hasta"/></th>
			<td><jlis:date name="fechaHasta" form="formulario" /></td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<th colspan="5" >&nbsp;</th>
		</tr>
		<tr>
		<td colspan="5">
            <jlis:button code="CO.USUARIO.OPERACION" id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Se puede buscar digitando parte de la denomminaci�n." />
		    <jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" /></td>
		</tr>
	</table>
	<br/>
    <jlis:table  name="DJ" keyValueColumns="DJID,CALIFICACIONUOID,UMFISICAID,DENOMINACION,PARTIDAARANCELARIA,DENOMINACIONARANCELARIA,DJ,NALADISA"
        source="tDJs" scope="request" pageSize="10" divStyle="overflow:auto;height:250px" fixedHeader="yes" navigationHeader="yes" width="98%" >
        <jlis:tr>
        	<jlis:td name="DJID" nowrap="yes" />
        	<jlis:td name="CALIFICACIONUOID" nowrap="yes" />
        	<jlis:td name="UMFISICAID" nowrap="yes" />
        	<jlis:td name="DJ" />
        	<jlis:td name="NOMBRE DEL PRODUCTOR" />
        	<jlis:td name="FEC. INICIO VIGENCIA" nowrap="yes" />
        	<jlis:td name="FEC. FIN VIGENCIA" nowrap="yes" />
            <jlis:td name="DENOMINACION" />
        	<jlis:td name="PARTIDA ARANCELARIA" nowrap="yes" />
        	<jlis:td name="DENOMINACION ARANCELARIA" nowrap="yes" />
        	<jlis:td name="NALADISA" nowrap="yes" />
        </jlis:tr>
        <jlis:columnStyle column="1" columnName="DJID" align="center" hide="yes" />
        <jlis:columnStyle column="2" columnName="CALIFICACIONUOID" align="center" hide="yes" />
        <jlis:columnStyle column="3" columnName="UMFISICAID" align="center" hide="yes" />
        <jlis:columnStyle column="4" columnName="DJ" editable="yes" align="center" />
        <jlis:columnStyle column="5" columnName="NOMBREPRODUCTOR" align="center" />
        <jlis:columnStyle column="6" columnName="FECHAINICIOVIGENCIA" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm"  />
        <jlis:columnStyle column="7" columnName="FECHAFINVIGENCIA"  align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
        <jlis:columnStyle column="8" columnName="DENOMINACION" align="center" />
        <jlis:columnStyle column="9" columnName="PARTIDAARANCELARIA" align="center" />
        <jlis:columnStyle column="10" columnName="DENOMINACIONARANCELARIA" align="center" />
        <jlis:columnStyle column="11" columnName="NALADISA" hide="yes" />
        <jlis:tableButton column="4" type="link" onClick="retornar" />
    </jlis:table>
    </form>

</body>
</html>