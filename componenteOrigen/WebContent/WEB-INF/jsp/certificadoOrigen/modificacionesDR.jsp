<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%-- @taglib uri="/tags/vuce-mincetur" prefix="vuce" --%>

        <%-- <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
        <vuce:validacionBotonesFormato pagina="modificacionesDR" />
        </c:if>

        <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
        <vuce:validacionBotonesFormato pagina="modificacionesDRResolutor" />
        </c:if> --%>

        <%-- <c:if test="${vigente=='S'}">
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}"> --%>
            <table class="form">
                <tr>
                    <td><jlis:button code="CO.USUARIO.OPERACION,CO.ENTIDAD.EVALUADOR" id="nuevaSolicitudRectificacionDRButton" name="nuevaSolicitudRectificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Nueva Solicitud de Rectificación" alt="Pulse aquí para crear una Solicitud de Rectificación" /></td>
                    <td width="100%"></td>
                </tr>
            </table>
           <%--  </c:if>
            <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && size_tModificacionesDR==0}">
            <table class="form">
                <tr>
                    <td><jlis:button code="VUCE.ENTIDAD.EVALUADOR" id="nuevaSolicitudRectificacionDRButton" name="nuevaSolicitudRectificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Iniciar Rectificación de DR" alt="Pulse aquí para iniciar la Rectificación del DR" /></td>
                    <td width="100%"></td>
                </tr>
            </table>
             </c:if>
        </c:if> --%>
        <jlis:table name="MODIFICACION_DR" pageSize="*" keyValueColumns="MODIFICACION_DR,MENSAJE_ID,DR_ID,SDR" source="tModificacionesDR" scope="request" navigationHeader="no" width="100%">
            <jlis:tr>
                <jlis:td name="DR ID" nowrap="yes" />
                <jlis:td name="SDR" nowrap="yes" />
                <jlis:td name="MODIFICACION DR" nowrap="yes" />
                <jlis:td name="FECHA" nowrap="yes" width="5%"/>
                <jlis:td name="MENSAJE ID" nowrap="yes"/>
                <jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
                <jlis:td name="ESTADO" nowrap="yes" width="8%"/>
                <jlis:td name="FECHA RESPUESTA" nowrap="yes" width="5%"/>
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="DR_ID" hide="yes" />
            <jlis:columnStyle column="2" columnName="SDR" hide="yes" />
            <jlis:columnStyle column="3" columnName="MODIFICACION_DR" hide="yes" />
            <jlis:columnStyle column="4" columnName="FECHA_REGISTRO" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
            <jlis:columnStyle column="5" columnName="MENSAJE_ID" hide="yes" />
            <jlis:columnStyle column="6" columnName="MENSAJE" editable="yes" />
            <jlis:columnStyle column="7" columnName="ESTADO" align="center"/>
            <jlis:columnStyle column="8" columnName="FECHA_RESPUESTA" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
            <jlis:tableButton column="6" type="link" onClick="verSolicitudRectificacionDR" />
        </jlis:table>
