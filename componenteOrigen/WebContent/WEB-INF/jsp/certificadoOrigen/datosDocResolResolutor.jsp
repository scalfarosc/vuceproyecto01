<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>
	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/acuerdos/acuerdo${idAcuerdo}/solicitud_co_acuerdo_${idAcuerdo}.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/firmas/vucecopy.min.js"></script>

	<jsp:include page="/WEB-INF/jsp/funciones.jsp" />
	<script>

	$(document).ready(function() {
	    initializetabcontent("maintab");
	    var idAcuerdo = '${idAcuerdo}';

	    // Esto s�lo deber�a ocurrir en un mct001, este c�digo se coloca para prevenir el caso
	    // de los otros formatos
	    if (idAcuerdo != '') {
		    // Inicializa controles de DR seg�n el acuerdo
		    inicializacionDocumentoResolutivoSolicitudSegunAcuerdo();
		    
	    	//Si no existen datos generales que mostrar se oculta el titulo correspondiente
	    	if ((document.getElementById("trNroCertifEntidad") == undefined || tagOculto("trNroCertifEntidad")) &&
		    	tagOculto("trNroCertifOrigen") && tagOculto("trNumeroExpediente") && 
			    tagOculto("trFechaEmision") && tagOculto("trFechaVigencia"))  {
	    			
	    		$("#trDatosGeneralesTitle").attr("style", "display:none");
	    		$("#tbSeparadorDatosGenerales").attr("style", "display:none");
	    	}
	    }
	    //20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados
	    if ($("#DR\\.sdr").val() != "") {
	    	$("#liAdjuntos").attr("style", "display:");
	    	$("#version").val($("#DR\\.sdr").val());
	    }
	});

	function validarSubmit() {
	    var f=document.formulario;
	    if (f.button.value=="cancelarButton") {
            return false;
        }
	    if (f.button.value=="signButton") {
            return false;
        }	    
	    if (f.button.value=="printResumeButton") {
	    	//alert(f.idAcuerdo.value);
	    	return imprimirHojaResumenSUCE();
        }
	    if (f.button.value=="printResumeAnulacionButton") {
	    	return imprimirHojaResumenSUCE('1');
        }
	    if (f.button.value=="downloadSignatureButton"){
	    	return imprimirHojaResumenAvance();

	    }
	    if (f.button.value=="cerrarPopUpButton") {
        	return validaCerraPopUpNoAction();
        }
        if (f.button.value=="nuevaSolicitudRectificacionDRButton") {
            var drId = $("#DETALLE\\.dr_id").val();
            var sdr = $("#DETALLE\\.sdr").val();
            var suceId = $("#DETALLE\\.suce_id").val();
            var controller = "/origen.htm";
            var ordenId = f.orden.value;
            var mto = f.mto.value;
            var modificacionDrId = 0;
            var mensajeId = 0;
            showPopWin(contextName+controller+"?method=cargarModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId, 710, 500, null);
            return false;
        }
	    return true;
	}

	function printResume() {
	    var f = document.formulario;
	    f.button.value="printResumeButton";

		if ('${formato}' == 'MCT004' || '${formato}' == 'mct004' ) {
			f.button.value="printResumeAnulacionButton";
		}
	}

	function setSignButton() {
	    var f = document.formulario;
	    f.button.value="signButton";
	}
	
	function downloadSignature() {
	    var f = document.formulario;
	    f.button.value="downloadSignatureButton";
	}

    function nuevaSolicitudRectificacionDR() {
        var f = document.formulario;
        f.button.value="nuevaSolicitudRectificacionDRButton";
        var drId = $("#DETALLE\\.dr_id").val();
        var sdr = $("#DETALLE\\.sdr").val();
        var suceId = $("#DETALLE\\.suce_id").val();
        var ordenId = f.orden.value;
        var mto = f.mto.value;
        var controller = "/origen.htm";
        var modificacionDrId = 0;
        var mensajeId = 0;
        var formato = f.formato.value;
        showPopWin(contextName+controller+"?method=cargarModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId+"&formato="+formato, 710, 500, null);
        return false;
    }

    function verSolicitudRectificacionDR(keyValues, keyValuesField) {
        var f = document.formulario;
        var drId = $("#DETALLE\\.dr_id").val();
        var sdr = $("#DETALLE\\.sdr").val();
        var suceId = $("#DETALLE\\.suce_id").val();
        var ordenId = f.orden.value;
        var mto = f.mto.value;
        var controller = "/origen.htm";
        var modificacionDrId = document.getElementById(keyValues.split("|")[0]).value;
        var mensajeId = document.getElementById(keyValues.split("|")[1]).value;
        var formato = f.formato.value;
        showPopWin(contextName+controller+"?method=cargarModificacionDR&controller="+controller+"&drId="+drId+"&sdr="+sdr+"&suceId="+suceId+"&ordenId="+ordenId+"&mto="+mto+"&modificacionDrId="+modificacionDrId+"&mensajeId="+mensajeId+"&formato="+formato, 710, 500, null);
    }

    // Descargar archivos adjuntos
    function descargarAdjunto(keyValues, keyValuesField) {
        var f = document.formulario;
        var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
        f.action = contextName+"/origen.htm";
        f.method.value = "descargarAdjunto";
        f.idAdjunto.value = adjuntoId;
        f.submit();
    }

	function seleccionVersion(valor) {
		var f = document.formulario;
    	f.action = contextName+"/origen.htm";
    	f.drId.value = $("#DETALLE\\.dr_id").val();
    	f.sdrSelect.value = valor;
        f.method.value="cargarDatosDocumentoResolutivo";
        f.button.value="grabarButton";
        f.submit();
    }

	</script>
	<body id="contModal">
		<form name="formulario" method="post" onSubmit="return validarSubmit();">
			<input type="hidden" name="method" />
			<input type="hidden" name="button">
            <jlis:value name="DETALLE.dr_id" type="hidden" />
            <jlis:value name="DETALLE.sdr" type="hidden" />
            <jlis:value name="DETALLE.suce_id" type="hidden" />
            <jlis:value name="orden" type="hidden" />
            <jlis:value name="mto" type="hidden" />
            <jlis:value name="formato" type="hidden" />
            <jlis:value name="idAcuerdo" type="hidden" />
			<jlis:value name="adjuntoIdFirma" type="hidden" />
			<jlis:value name="idAdjunto" type="hidden" />
            <jlis:value name="drId" type="hidden" />
			<jlis:value name="sdrSelect" type="hidden" />
            <jlis:value name="suceCerrada" type="hidden" />
            <jlis:value name="estadoRegistro" type="hidden" />
			<input class="vuce-copy-data" id="input_exportador" user="${requestScope['usuarioSol']}" drid="${requestScope['DETALLE.dr_id']}" sdr="${requestScope['DETALLE.sdr']}"/>
            
			<%-- <jlis:value type="hidden" name="DETALLE.certificado" /> --%>
			<c:if test="${!empty formato && ( formato != 'MCT005' && formato != 'mct005' ) && estadoRegistro != 'H'}">
				<c:if test="${habilitarBtnFirma == 'S' || vigente== 'S'}">
					<jlis:button id="printResumeButton" name="printResume()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Imprimir Certificado Origen " alt="Pulse aqu� para imprimir la hoja de resumen" />
				</c:if>
			</c:if>
			<c:if test="${!empty formato && ( formato == 'MCT004' || formato == 'mct004' ) }">
				<jlis:button id="printResumeButton" name="printResume()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Imprimir Documento de Anulaci�n " alt="Pulse aqu� para imprimir la hoja de resumen" />
			</c:if>
			<%--
			<c:if test="${!empty formato && ( formato == 'MCT004' || formato == 'mct004' ) }">
				<jlis:button id="downloadSignatureButton" name="downloadSignature()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Descargar el DR firmado " alt="Pulse aqu� para descargar la firma del DR" />
			</c:if>
			 --%>
			<input type='submit' id="signButton" onclick="setSignButton();" class="buttonSubmitClass vuce-copy-obtener" vuce-clipboard-target="input_exportador" value=" Firmar SUNAT " />			 
			<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana" /></tr>

			<c:if test="${!empty filterVersionesDR && !empty filterVersionesDR.drId}">
            <span class="messageClass"><jlis:label key="co.label.versionDR"/></span>
            <jlis:selectProperty name="version" key="dr.version.lista" filter="${filterVersionesDR}" style="defaultElement=no" onChange="seleccionVersion(this.value)" />
            </c:if>
		</form>
		<jlis:modalWindow />

		<co:validacionBotonesFormato formato="MTC001SDR" />
<%-- HELPDESK3--%>
		<ul id="maintab" class="tabs">
			<li class="selected"><a href="#" rel="tabGeneral"><span>Detalle</span></a></li>
			<%--20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados--%>
			<c:if test="${!empty formato && (formato != 'MCT004' && formato != 'mct004') && (sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.FIRMA' || sessionScope.USUARIO.rolActivo == 'CO.ADMIN.HELP_DESK') && (!empty tipoDr && tipoDr == 'A' && ((estadoSDR == 'A' && esRectificacion == 'S') || esRectificacion == 'N'))}">
					<li id="liAdjuntos" style="display:none"><a href="#" rel="tabAdjuntos"><span>Firmas Adjuntas</span></a></li>
			</c:if>
					
							
			<c:if test="${permiteRectificar == 'S'}">
				<li><a href="#" rel="tabSolRectifDR"><span>Solicitudes de Rectificaci�n de DR</span></a></li>
			</c:if>
		</ul>

	    <div id="pageTitle">
	        <strong><jlis:value name="nombreFormato" editable="no" valueClass="pageTitle" /></strong>
	    </div>
		<hr/>
		<div id="tabGeneral" class="tabcontent">
			<jsp:include page="/WEB-INF/jsp/resolutor/${formato}DrData.jsp" />
		</div>
		<%--20140722_JMC NuevoReq: El HELPDESK visualizara y descargara los documentos firmados--%>
		<div id="tabAdjuntos" class="tabcontent">
            <table id="tablaBotonesAdjunto">
	            <%--20140611_JMCA PQT-08--%>
            	<tr>
					<td width="2%">&nbsp;</td>
					<td width="10%"><jlis:button code="CO.ENTIDAD.FIRMA" id="cargarButton" name="guardarDatosFirma()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Grabar Datos" alt="Pulse aqu� para grabar los datos" /></td>
					</td>
                </tr>
            
				<tr id="trDatosGeneralesTitle">									
					<td colspan="3" class="formatoCaption">&nbsp;&nbsp;DATOS FIRMA EMPRESA EXPORTADORA</td>
				</tr>
				
				<tr>
	        		<td></td>
					<td><jlis:label key="co.dr.adjunto.firma.exportador" /></td>
					<td>
						<jlis:value name="usuarioFirmante" editable="no" type="text" align="center" style="width:92%" size="50" /><span class="requiredValueClass">(*)</span>
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.USUARIO_FIRMANTE_EXP" />
					</td>
				</tr>
				<tr id="trCargoEmpresaExportador" style="display:none">
	        		<td></td>
					<th><jlis:label key="co.dr.adjunto.firma.cargoEmpresaExportador" /></th>
					<td>
						<jlis:value name="cargoEmpresaExportador" editable="no" type="text" align="center" style="width:92%" size="50" /><span class="requiredValueClass">(*)</span>										
					</td>
				</tr>
				<tr id="trTelefonoEmpresaExportador" style="display:none">
	        		<td></td>
					<th><jlis:label key="co.dr.adjunto.firma.telefonoEmpresaExportador" /></th>
					<td>
						<jlis:value name="telefonoEmpresaExportador" editable="no" type="text" align="center" style="width:92%" size="20" maxLength="20" /><span class="requiredValueClass">(*)</span>										
					</td>
				</tr>									
                
                <!-- 20150612_RPC: Acta.34:Firmas -->
                <tr id="trFechaFirmaExportador" style="display:none">
                    <td></td>
                    <td><jlis:label key="co.dr.adjunto.firma.fechaFirmaExportador" /></td>
                    <td>
                        <jlis:date form="formulario" name="fechaFirmaExportador" /><span class="requiredValueClass">(*)</span>
                        <co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.FECHA_FIRMA_EXP" />
                    </td>
                </tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				
				 <%--20140611_JMCA PQT-08--%>				            
				<tr id="trDatosGeneralesTitle">
					<td colspan="3" class="formatoCaption">&nbsp;&nbsp;DATOS FIRMA ENTIDAD CERTIFICADORA</td>
				</tr>


				<c:if test="${idAcuerdo == 14 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 24 || idAcuerdo == 9 || idAcuerdo == 18 || idAcuerdo == 19 || idAcuerdo == 27}"}">
				<tr>
                                <td></td>
                                <td><jlis:label key="co.dr.adjunto.firma.nroReferenciaCertificado" /></td>
                                <td>
                                    <jlis:value name="nroReferenciaCertificado" editable="no" type="text" align="center" style="width:92%" size="50" maxLength="6" onChange="validarEnteroPositivo(this,false,null)" /><span class="requiredValueClass">(*)</span>
                                    <co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.NUMERO_REFERENCIA" />
                                </td>
                            </tr>
                            </c:if>
	        	<tr >
	        		<td width="2%"></td>
					<td width="40%"><jlis:label key="co.dr.adjunto.firma.cetificador" /></td>
					<td width="58%">								
						<%-- 20140618_JMC a�adido BUG 123--%>
						<c:choose>
			                <c:when test="${requiereFirma == 'N'}">
			                    <jlis:selectProperty name="secuenciaFuncionario" key="comun.firmante.entidad.transmitido.select" editable="no" filter="${filterFirmantesCertificador}" /><span class="requiredValueClass">(*)</span>
			                </c:when>
			                <c:otherwise>							                    
			                    <jlis:selectProperty name="secuenciaFuncionario" key="comun.firmante.entidad.select" editable="no" filter="${filterFirmantesCertificador}" /><span class="requiredValueClass">(*)</span>
			                </c:otherwise>
			            </c:choose>
						
						<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.USUARIO_FIRMANTE_EVA" />
					</td>
				</tr>
                
                <tr>
                    <td></td>
                    <th>
                    <!-- 20151020_JFR: Acta.34:Firmas:6,8,15 -->
                    <!-- 20150612_RPC: Acta.34:Firmas:18,19 -->
                    <!-- 20150701_RPC: Acta.36:Firmas:9,10,12,14,24 -->
                    <c:choose>
                        <c:when test="${idAcuerdo == 6 || idAcuerdo == 8 || idAcuerdo == 9 || idAcuerdo == 10 || idAcuerdo == 12 || idAcuerdo == 14 || idAcuerdo == 15 || idAcuerdo == 18 || idAcuerdo == 19 || idAcuerdo == 24 || idAcuerdo == 26 || idAcuerdo == 27}">
                            <jlis:label key="co.dr.adjunto.firma.fechaFirmaEntidad"  id="co.dr.adjunto.firma.fecha" />
                        </c:when>
                        <c:otherwise><jlis:label key="co.dr.adjunto.firma.fecha" /></c:otherwise>
                    </c:choose>
                    </th>
                    <td>
                        <jlis:date form="formulario" name="fechaFirma" /><span class="requiredValueClass">(*)</span>
                        <co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.FECHA_FIRMA_EEV" />
                    </td>
                </tr>
                
                <!-- 20150612_RPC: Acta.34:Firmas -->
                <tr height="10px"/>                     
                <tr class="trInformacionAdicionalFirma" style="display:none">
                    <td colspan="3" class="formatoCaption">&nbsp;&nbsp;Informaci�n Adicional del Certificado</td>   
                </tr>                               
                <tr class="trInformacionAdicionalFirma" style="display:none">
                    <td width="2%"></td>
                    <th width="40%"><jlis:label key="co.label.certificado.salio_mercancia" id="co.label.certificado.salio_mercancia_2"/></th>
                    <td width="58%">
                        <jlis:selectSource name="emitidoPosteriori2" source="listaSiNo" scope="request" editable="${(requestScope['DR.emitidoPosteriori'] == 'NO' || !empty requestScope['DR.emitidoPosteriori2']) ? 'yes' : 'no'}"/>
                        <co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DR.DETALLE_CERTIFICADO.EMITIDO_POSTERIORI_2" />
                    </td>
                </tr>
                
            </table>
	        <br/>
			<c:if test="${!empty usuarioFirmante}">
				<table id="tablaSubtituloAdjunto" >
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
	                <tr>
						<td width="2%">&nbsp;</td>									
						<td width="10%"><jlis:button code="CO.ENTIDAD.FIRMA" id="cargarButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
						<td width="88%"><jlis:button code="CO.ENTIDAD.FIRMA" id="eliminarAdjButton" name="eliminarRegistroAdjunto()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" />
						</td>
	                </tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
	                <tr>
	                	<td>&nbsp;</td>
	                    <td colspan="2">
		                    <jlis:label key="co.label.documentos_adjuntar"/> <br />
		                    <jlis:label key="co.label.tipo_archivos"/>
	                    </td>
	                </tr>

	                <tr>
	                	<td>&nbsp;</td>
	                    <td colspan="2">
	                     <input type="file" id="archivo" name="archivo" size="87"  />
	                    </td>
	                </tr>
	            </table>
			</c:if>

	        <jlis:value name="ADJ_ELIM.djId" type="hidden" />
	        <jlis:value name="ADJ_ELIM.secuenciaMaterial" type="hidden" />
	        <jlis:value name="ADJ_ELIM.adjuntoId" type="hidden" />

	        <jlis:table  name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntosCODR" scope="request"
	            pageSize="*" navigationHeader="no" width="98%" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCopiadosTramitePadreRowValidator" >
	            <jlis:tr>
	                <jlis:td name="ADJUNTO ID" nowrap="yes" />
	                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" width="90%" />
	              	<jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
	            </jlis:tr>
	            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" />
	            <jlis:columnStyle column="2" columnName="NOMBRE_ARCHIVO" editable="yes" />
	            <jlis:tableButton column="2" type="link" onClick="descargarAdjunto" highlight="no" />
	            <jlis:columnStyle column="3" columnName="SELECCIONE" align="center" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCellValidator" editable="yes" />
	            <jlis:tableButton column="3" type="checkbox" name="seleccione" />
	        </jlis:table>
		</div>
		
 		<div id="tabSolRectifDR" class="tabcontent">
            <table class="form">
                <tr>
                    <td><jlis:button code="CO.USUARIO.OPERACION" id="nuevaRectificacionDRButton" name="nuevaSolicitudRectificacionDR()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Nueva Solicitud de Rectificaci�n" alt="Pulse aqu� para crear una Solicitud de Rectificaci�n" /></td>
                    <td width="100%"></td>
                </tr>
            </table>
	        <jlis:table name="MODIFICACION_DR" pageSize="*" keyValueColumns="MODIFICACION_DR,MENSAJE_ID,DR_ID,SDR" source="tModificacionesDR" scope="request" navigationHeader="no" width="100%">
	            <jlis:tr>
	                <jlis:td name="DR ID" nowrap="yes" />
	                <jlis:td name="SDR" nowrap="yes" />
	                <jlis:td name="MODIFICACION DR" nowrap="yes" />
	                <jlis:td name="FECHA" nowrap="yes" width="5%"/>
	                <jlis:td name="MENSAJE ID" nowrap="yes"/>
	                <jlis:td name="MENSAJE" nowrap="yes" width="85%"/>
	                <jlis:td name="ESTADO" nowrap="yes" width="8%"/>
	                <jlis:td name="FECHA RESPUESTA" nowrap="yes" width="5%"/>
	            </jlis:tr>
	            <jlis:columnStyle column="1" columnName="DR_ID" hide="yes" />
	            <jlis:columnStyle column="2" columnName="SDR" hide="yes" />
	            <jlis:columnStyle column="3" columnName="MODIFICACION_DR" hide="yes" />
	            <jlis:columnStyle column="4" columnName="FECHA_REGISTRO" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
	            <jlis:columnStyle column="5" columnName="MENSAJE_ID" hide="yes" />
	            <jlis:columnStyle column="6" columnName="MENSAJE" editable="yes" />
	            <jlis:columnStyle column="7" columnName="DESCRIPCION_ESTADO_REGISTRO" align="center"/>
	            <jlis:columnStyle column="8" columnName="FECHA_RESPUESTA" editable="no" type="dateTime" pattern="dd/MM/yyyy HH:mm" align="center"/>
	            <jlis:tableButton column="6" type="link" onClick="verSolicitudRectificacionDR" />
	        </jlis:table>
		</div>
		<script>
			window.parent.document.getElementById("popupTitle").innerHTML = "Documento resolutivo";
			window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
		</script>
	</body>
</html>