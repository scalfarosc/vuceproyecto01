<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema COMPONENTE ORIGEN</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/ddjj/ddjj_comun.js"></script>
	<script language="JavaScript" src="<%=contextName%>/resource/js/acuerdos/acuerdo${idAcuerdo}/ddjj_acuerdo_${idAcuerdo}.js"></script>

	<script>
	//var msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per� o Chile, o que todos los insumos sean originarios del Per� o Chile, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';
	//var msgCriterio2 = 'Si el criterio que se aplica a su mercanc�a es un cambio de clasificaci�n arancelaria, Ud. no debe incluir la informaci�n vinculada al valor.';

	var alertaMaterial = '';

	var msgMaterialPeru = '';
	var msgMaterial2doComp = '';
	var msgMaterial3erComp = '';

	$(document).ready(function() {
		tituloPopUp("Declaraci�n Jurada");
   		initializetabcontent("maintab");

   		var bloqueado = document.getElementById("bloqueado").value;
   		cargarUMs();

   		var djId = $("#djId").val();
		/*10 Junio-JMC, comentado 
   		if ('${djCalificada}' == 'S') {
   		
		    var f=document.formulario;

   			if (window.parent.parent.name == 'undefined') {
	        	f.target = window.parent.name;
	        } else {
	        	f.target = window.parent.parent.name;
	        }
        	f.action = contextName+"/origen.htm";
	        f.method.value="cargarInformacionOrden";
	        //f.button.value="aprobarDJButton";
   			f.button.value="cancelarButton";
			f.submit();

   		}
		*/

   		/*if (djId != '0') {
   			//$("#DJ\\.tipoRolDj_1").attr('disabled',true);
   			//$("#DJ\\.tipoRolDj_2").attr('disabled',true);
   				if ($("#DJ\\.cumpleTotalmenteObtenido").val() == 'S') {
   					$("#DJ\\.criterioQueCumple_1").attr('checked',true);
   				} else if ($("#DJ\\.cumpleCriterioCambioClasif").val() == 'S') {
   					$("#DJ\\.criterioQueCumple_2").attr('checked',true);
   				} else if ($("#DJ\\.cumpleOtroCriterio").val() == 'S') {
   					$("#DJ\\.criterioQueCumple_3").attr('checked',true);
   				}

   				$("#partidaSegunAcuerdo").val($("DJ\\.partidaSegunAcuerdo").val());
   				//cargarOrigenSecuencia();
   				//mostrarTagMercancia($("#DJ\\.cumpleTotalmenteObtenido").val() == 'S');

   				/*if ($("#DJ\\.tipoRolDj_2").attr('checked')) {
   					$("#liMateriales").attr("style", "display:none");
   				}*/
   		/*} else {
   			document.getElementById("DJ.cumpleTotalmenteObtenido").value = 'S';
   			$("#DJ\\.criterioQueCumple_1").attr('checked',true);
   			contadorRadioUno++;
   		}*/

		// Inicializaci�n de controles seg�n acuerdo
   		inicializacionDeclaracionJuradaSegunAcuerdo();

   		if(bloqueado=="S") {
   			bloquearControles();
   			//$("#buscarPartidasButton").attr("disabled", true);
   			<%-- <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
   				if (document.getElementById("DJ.partidaSegunAcuerdo").value==""){
   					$("#DJ\\.partidaSegunAcuerdo").attr('readonly',false).removeClass("readonlyInputTextClass").addClass("inputTextClass");

   				}
   				$("#DJ\\.secuenciaNorma").removeClass("readonlyInputTextClass").addClass("inputTextClass");
   				$("#DJ\\.secuenciaOrigen").removeClass("readonlyInputTextClass").addClass("inputTextClass");
   				$("#DJ\\.secuenciaNorma").attr("disabled", false);
   				$("#DJ\\.secuenciaOrigen").attr("disabled", false);
   			</c:if> --%>
   		}/* else {
   			if (($("#trNorma").attr("style") == 'display:' || $("#trNorma").attr("style") == '') && $("#DJ\\.secuenciaNorma").attr("options").length == 2){
    			$("#DJ\\.secuenciaNorma").attr("options")[1].selected = true;
    			cargarOrigenSecuencia();
    		}
   		}*/

/*   		if ($("#DJ\\.calificacion").val() != ''){
   			if ($("#DJ\\.calificacion").val() == 'A'){
   				$("#tdCalificacion").attr("innerHTML", "APROBADA");
   			} else if ($("#DJ\\.calificacion").val() == 'R'){
   				$("#tdCalificacion").attr("innerHTML", "RECHAZADA");
   			}

   			$("#trCalificacion").attr("style", "display:");
   		}*/

   		//20140626_JMC BUG 124
   		<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">   			
   			$("#trDemasGasto").attr("style", "display:");
   			$("#trPorcentajeDemasGastos").attr("style", "display:");   			
		</c:if>
   		// Si estamos en modo productor validador, no son editables los tipos de rol
   		var modoProductorValidador = document.getElementById("modoProductorValidador").value;
   		if ( modoProductorValidador == 'S' ) {
   			$("#trTipoRolDj").attr("style", "display:none");
   		}

		$("#lnkPaisesG2").mouseover(function () {
			$(this).css('cursor', 'hand');
			$("#lnkPaisesG2").css('color', 'black');
			$("#lnkPaisesG2").css('text-decoration', 'underline');
		});

		$("#lnkPaisesG2").mouseout(function () {
			$(this).css('cursor', 'default');
			$("#lnkPaisesG2").css('color', '#3A6EA5');
			$("#lnkPaisesG2").css('text-decoration', '');
		});

	});

	 	function bloquearPantalla() {
	 		//$("#USUARIO_DJ\\.nombre").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	 		//$("#USUARIO_DJ\\.documentoTipo").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	 		//$("#USUARIO_DJ\\.numeroDocumento").removeClass("inputTextClass").addClass("readonlyInputTextClass");

			$("#DJ\\.denominacion").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.caracteristica").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.partidaArancelaria").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.denominacionArancelaria").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.umFisicaId").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.cumpleTotalmenteObtenido").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.cumpleCriterioCambioClasif").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			$("#DJ\\.cumpleOtroCriterio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	 	}

		function validarSubmit() {

		    var f=document.formulario;

		    if (f.button.value == "nuevoSubProdButton") {
			    return false;
		    }
		    if (f.button.value == "cancelarButton") {
	            return false;
	        }
		    if (f.button.value == "nuevoMaterialPeruButton") {
			    return false;
		    }
		    if (f.button.value == "cerrarPopUpButton") {
	        	return validaCerraPopUpNoAction();
	        }
		    if (f.button.value == "busqPartidasButton") {
	        	return false;
	        }
		    if (f.button.value == "nuevoProductorButton"){
		    	return false;
		    }
		    if (f.button.value == "actualizarConsolidadoButton"){
		    	//return false;
		    }
		    if (f.button.value == "aprobarDJButton"){
		    	//return false;
		    }
		    if (f.button.value == "nuevaAutorizacionButton"){
		    	return false;
		    }

		    return true;
		}

		function validarCampos(){

		    var ok = true;
		    var mensaje = "Debe ingresar o seleccionar : ";

		    var lblDenominacion = "co.label.dj.denominacion";

	        <c:if test="${idAcuerdo == 16}">
	        	lblDenominacion = "co.label.dj.${idAcuerdo}.denominacion";
		 	</c:if>

		    if ( $.trim(document.getElementById("DJ.denominacion").value) == "" ) {
		        mensaje +="\n -La denominaci�n";
		        changeStyleClass(lblDenominacion, "errorValueClass");
		    } else {
		    	changeStyleClass(lblDenominacion, "labelClass");
		    }

		    if ( $.trim(document.getElementById("DJ.caracteristica").value) == "" ) {
				mensaje +="\n -Las caracter�sticas";
		        changeStyleClass("co.label.dj.caracteristica", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.caracteristica", "labelClass");
		    }

		    if ( $.trim(document.getElementById("DJ.partidaArancelaria").value) == "" ) {
		        mensaje +="\n -La subpartida arancelaria";
		        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
		    }

		    if ( $.trim(document.getElementById("DJ.umFisicaId").value) == "" ) {
		        mensaje +="\n -La unidad de medida";
		        changeStyleClass("co.label.dj.um_fisica_id", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.um_fisica_id", "labelClass");
		    }

		    /*if (($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == ""){

			    if ($.trim(document.getElementById("DJ.secuenciaNorma").value)==""){
			        mensaje +="\n -La norma";
			        changeStyleClass("co.label.dj.norma", "errorValueClass");
			    }else changeStyleClass("co.label.dj.norma", "labelClass");

			    if ($.trim(document.getElementById("DJ.secuenciaOrigen").value)==""){
			        mensaje +="\n -El criterio de origen";
			        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
			    }else changeStyleClass("co.label.dj.criterio_origen", "labelClass");

		    }

		    if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("DJ.partidaSegunAcuerdo").value)==""){
		        mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.${idAcuerdo}.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.${idAcuerdo}.naladisa').innerHTML).length - 1);
		        changeStyleClass("co.label.dj.${idAcuerdo}.naladisa", "errorValueClass");
		    }else changeStyleClass("co.label.dj.${idAcuerdo}.naladisa", "labelClass"); */

		    if ( mensaje != "Debe ingresar o seleccionar : " ) {
		        ok = false;
		        alert(mensaje);
		    } else if ( !document.getElementById("DJ.aceptacion").checked ) {
		    	 mensaje = "Debe indicar su aceptaci�n de la Declaraci�n Jurada marcando el check respectivo";
			     changeStyleClass("co.label.dj.acepto_dj", "errorValueClass");
			     ok = false;
			     alert(mensaje);
			} else {
				changeStyleClass("co.label.dj.acepto_dj", "labelClass");
			}

		    return ok;
		}

		function validarCamposEvaluador() {

		    var ok = true;
		    var mensaje="Debe ingresar o seleccionar : ";

		    /*if (validarPartidaSegunAcuerdo() && $.trim(document.getElementById("DJ.partidaSegunAcuerdo").value)==""){
		    	mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.${idAcuerdo}.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.${idAcuerdo}.naladisa').innerHTML).length - 1);
		        changeStyleClass("co.label.dj.${idAcuerdo}.naladisa", "errorValueClass");
		    }else changeStyleClass("co.label.dj.${idAcuerdo}.naladisa", "labelClass");

		    if ($.trim(document.getElementById("DJ.secuenciaNorma").value)==""){
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    }else changeStyleClass("co.label.dj.norma", "labelClass");

		    if ($.trim(document.getElementById("DJ.secuenciaOrigen").value)==""){
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    }else changeStyleClass("co.label.dj.criterio_origen", "labelClass");*/

		    if(mensaje!="Debe ingresar o seleccionar : ") {
		        ok= false;
		        alert (mensaje);
		    }
		    return ok;

		}

		function grabarRegistro() {
			var f = document.formulario;
			var validarGrabacion;

			<c:choose>
		      	<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
		      		validarGrabacion = validarCamposEvaluador();
		  	  	</c:when>
		  		<c:otherwise>
		  			validarGrabacion = validarCampos();
		 		</c:otherwise>
		 	</c:choose>

	        if ( validarGrabacion ) {
	        	f.action = contextName+"/origen.htm";
		        f.method.value = "grabarMercanciaDeclaracionJurada";
		        <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
		        	f.method.value="evaluadorActualizaDeclaracionJurada";
		        	$("#DJ\\.partidaSegunAcuerdo").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		        </c:if>
		        f.button.value="grabarButton";
	        	//$("#USUARIO_DJ\\.documentoTipo").attr('disabled',false);
	        	//$("#USUARIO_DJ\\.numeroDocumento").attr('disabled',false);

    			//$("#DJ\\.tipoRolDj_1").attr('disabled',false);
    			//$("#DJ\\.tipoRolDj_2").attr('disabled',false);

    			//$("#DJ\\.secuenciaNorma").attr('disabled',false);
    			//$("#DJ\\.secuenciaOrigen").attr('disabled',false);

    			//if ($("#permiteApoderamientoXFrmt").val() == 'S') {
    				//$("#DJ\\.tipoRolDj_4").attr('disabled',false);
				//}

    			//if ('${reqValidacionProd}' == 'S') {
    				//$("#DJ\\.tipoRolDj_5").attr('disabled',false);
				//} else {
					//$("#DJ\\.tipoRolDj_6").attr('disabled',false);
				//}

    			/*if ($("#DJ\\.criterioQueCumple_1").attr('checked')) {
    				$("#DJ\\.cumpleTotalmenteObtenido").val("S");
    				$("#DJ\\.cumpleCriterioCambioClasif").val("N");
    				$("#DJ\\.cumpleOtroCriterio").val("N");
    			} else if ($("#DJ\\.criterioQueCumple_2").attr('checked')) {
    				$("#DJ\\.cumpleTotalmenteObtenido").val("N");
    				$("#DJ\\.cumpleCriterioCambioClasif").val("S");
    				$("#DJ\\.cumpleOtroCriterio").val("N");
    			} else if ($("#DJ\\.criterioQueCumple_3").attr('checked')) {
    				$("#DJ\\.cumpleTotalmenteObtenido").val("N");
    				$("#DJ\\.cumpleCriterioCambioClasif").val("N");
    				$("#DJ\\.cumpleOtroCriterio").val("S");
    			} else {
    				$("#DJ\\.cumpleTotalmenteObtenido").val("N");
    				$("#DJ\\.cumpleCriterioCambioClasif").val("N");
    				$("#DJ\\.cumpleOtroCriterio").val("N");
    			}*/

			} else {
				f.button.value="cancelarButton";
			}
	    }

	    function nuevoMaterial(tipoPais){
        	var f = document.formulario;
        	var idFormatoEntidad = f.idFormatoEntidad.value;
        	var orden=f.orden.value;
        	var mto=f.mto.value;
        	var bloqueado=f.bloqueado.value;
            var secuenciaMaterial = 0;
        	var djId = f.djId.value;
        	//var flgCambioClasif = '0';
        	var modoProductorValidador = f.modoProductorValidador.value;
        	var esValidador = f.esValidador.value;
        	var modoFormato = f.modoFormato.value;
        	var idAcuerdo = f.idAcuerdo.value;
        	var estadoDj = f.estadoDj.value;
        	var criterioArancelario = obtenerCriterioArancelario();
        	var idPaisSegundoComponente = document.getElementById("DJ.paisIsoIdPorAcuerdoInt").value;
        	var secuenciaOrigen = document.getElementById("DJ.secuenciaOrigen").value;
        	var tipoRol = document.getElementById("tipoRol").value;
        	//alert(idPaisSegundoComponente);

        	/*if ($("#DJ\\.cumpleCriterioCambioClasif").attr("checked")) {
        		flgCambioClasif = '1';
        	}*/

        	if (tipoPais == 'P' && msgMaterialPeru != '') {
        		alert(msgMaterialPeru);
        	}

        	showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaMaterialForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaMaterial="+secuenciaMaterial+"&tipoPais="+tipoPais+"&djId="+djId+"&criterioArancelario="+criterioArancelario+"&bloqueado="+bloqueado+"&modoProductorValidador="+modoProductorValidador+"&esValidador="+esValidador+"&modoFormato="+modoFormato+"&idAcuerdo="+idAcuerdo+"&estadoDj="+estadoDj+"&idPaisSegundoComponente="+idPaisSegundoComponente+"&secuenciaOrigen="+secuenciaOrigen+"&tipoRol="+tipoRol, 700, 500, null);
        	f.button.value="nuevoMaterialPeruButton";
        }


    	/**
    	*	Los valores para el flag ser�n:
    		0: No cumploe ningun criterio
    	*   1: Cumple Criterio Totalmente Obtenido
    	*   2: Cumple Criterio Cambio Clasificacion Arancelaria
    	*   3: Otro Criterio
    	**/
	    function obtenerCriterioArancelario() {
    		// Se maneja as� para mantener compatibilidad
	    	return document.getElementById("criterioArancelario").value;;
	    }

	    function editarMaterialPeru(keyValues, keyValuesField){
	    	editarMaterial(keyValues, keyValuesField, "P"); //, 0);
	    }

	    function editarMaterialOtro(keyValues, keyValuesField){
	    	editarMaterial(keyValues, keyValuesField, "N"); //, 0);
	    }

	    function editarMaterial(keyValues, keyValuesField, tipoPais, bloqueado){
	    	var f = document.formulario;
        	var idFormatoEntidad = f.idFormatoEntidad.value;
        	var orden=f.orden.value;
        	var mto=f.mto.value;
        	var bloqueado=f.bloqueado.value;
            var secuenciaMaterial = document.getElementById(keyValues.split("|")[1]).value;
        	var djId = f.djId.value;
        	//var flgCambioClasif = '0';
        	var modoProductorValidador = f.modoProductorValidador.value;
        	var esValidador = f.esValidador.value;
        	var modoFormato = f.modoFormato.value;
        	var idAcuerdo = f.idAcuerdo.value;
        	var estadoDj = f.estadoDj.value;
        	var criterioArancelario = obtenerCriterioArancelario();
        	var idPaisSegundoComponente = document.getElementById("DJ.paisIsoIdPorAcuerdoInt").value;
        	var secuenciaOrigen = document.getElementById("DJ.secuenciaOrigen").value;
        	var tipoRol = document.getElementById("tipoRol").value;

        	/*if ($("#DJ\\.cumpleCriterioCambioClasif").checked) {
        		flgCambioClasif = '1';
        	}*/

        	showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaMaterialForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaMaterial="+secuenciaMaterial+"&tipoPais="+tipoPais+"&djId="+djId+"&criterioArancelario="+criterioArancelario+"&bloqueado="+bloqueado+"&modoProductorValidador="+modoProductorValidador+"&esValidador="+esValidador+"&modoFormato="+modoFormato+"&idAcuerdo="+idAcuerdo+"&estadoDj="+estadoDj+"&idPaisSegundoComponente="+idPaisSegundoComponente+"&secuenciaOrigen="+secuenciaOrigen+"&tipoRol="+tipoRol, 700, 500, null);
        	f.button.value="nuevoMaterialPeruButton";
        }

	    // B�squeda de Partidas Arancelarias
	    function buscarPartidas(){
	    	var f = document.formulario;
	    	showPopWin(contextName+"/origen.htm?method=cargarPartidas", 600, 500, null);
	    	f.button.value="busqPartidasButton";
	    }

		// Carga la lista de unidades de medida de acuerdo a la partida arancelaria seleccionada
		function cargarUMs(){
			var f = document.formulario;
			var partida = document.getElementById("DJ.partidaArancelaria").value;
			if ( partida == '' ){
				loadSelectAJX("ajax.selectLoader", "loadList", "DJ.umFisicaId", "select.empty", null, null, null,null);
			} else {
				var filter ="partida="+partida;
				//loadSelectAJX("ajax.selectLoader", "loadList", "DJ.umFisicaId", "comun.partida.unidad_fisica.select", filter, null, afterCargarUm,null);
				//loadSelectAJX("ajax.selectLoader", "loadList", "DJ.umFisicaId", "comun.um_fisica${(enIngles=='S')?'.ingles':''}.select", filter, null, afterCargarUm,null);				
				loadSelectAJX("ajax.selectLoader", "loadList", "DJ.umFisicaId", "comun.um_fisica.select", filter, null, afterCargarUm,null);
			}
		}

		// Se invoca despues de la carga asincrona del combo
		function afterCargarUm() {
		    var djPartida = $("#DJ\\.partidaArancelaria").val();
			if ( djPartida != '') {
				document.getElementById("DJ.umFisicaId").value = $("#umFisicaId").val();
			}
		}

		function cargarDataPartida(codigo, descripcion){
			$("#DJ\\.partidaArancelaria").val(codigo);
			$("#DJ\\.denominacionArancelaria").val(codigo + " " + descripcion);
			// Esta funci�n es dependiente del acuerdo
			cargarDataPartidaSegunAcuerdo();
			// Invocaci�n as�ncrona para carga de unidades de medida
			cargarUMs();
		}

		function seleccionarDj(){

			var f = window.parent;

	        if ($("#umFisicaId").val() != '' && $.trim(f.document.getElementById("CO_MERCANCIA.descripcion").value) == ''){

				var djId = $("#djId").val();
		        var umFisicaId = $("#umFisicaId").val();
		        var denominacion = $("#DJ\\.denominacion").val();
		        var partidaArancelaria = "0000000000" + $("#DJ\\.partidaArancelaria").val();
		        var denominacionArancelaria = $("#DJ\\.denominacionArancelaria").val();
		        var secuenciaMercancia = $("#secuenciaMercancia").val();
		        var naladisa = $("#DJ\\.partidaSegunAcuerdo").val();
		        //alert(naladisa)
		        f.cargarDataDJFormato(djId, umFisicaId, denominacion, partidaArancelaria.substr(partidaArancelaria.length - 10), denominacionArancelaria, secuenciaMercancia, undefined, naladisa);
		        //f.habilitarNaladisa();
			} /*else {

				alert("Antes de salir debe grabar la declaraci�n jurada.");

			}*/

	        f.hidePopWin(false);

		}

		function seleccionarDjFormato(){

			var currentF = document.formulario;
			var f = window.parent;
	        var djId = $("#djId").val();
	        //alert('voy a asociar en el padre y el id es ' + djId);
	        if (currentF.bloqueado.value != 'S'){
	        	f.asociarDj(djId);
	        }
	        /*f.hidePopWin(false);*/
	        currentF.button.value = "cancelarButton";
	        validaCerraPopUpNoAction();
		}

		function salirDJ(){

			var f = window.parent;
			f.hidePopWin(false);

		}

	    function actualizar() {
	    	//alert('entre al actualizar');
	       	var f = document.formulario;
	       	f.action = contextName+"/origen.htm";
	        f.method.value="cargarDeclaracionJuradaForm";
	        f.button.value="actualizarButton";
	        f.target = "_self";
	        f.submit();
		}

	    /*function actualizarCalificacionAnticipada() {
	       	var f = document.formulario;
	        f.action = contextName+"/origen.htm";
            f.method.value = "cargarInformacionOrden";
	        f.button.value="actualizarButton";
	        f.target = "_self";
	        f.submit();
		}*/

	    function changeRol(val){
	    	if ( val == '1'){
	    		//$("tr.dataProductor").attr("style", "display:");
	    		//$("#liMateriales").attr("style", "display:");
	    		alert(alertaOptExportador);//"Ud. Deber� registrar la informacion del productor. El Productor deber� validar la informaci�n contenida en la DJ");
	    	} else if ( val == '2'){
	    		//$("tr.dataProductor").attr("style", "display:none");
	    		//$("#liMateriales").attr("style", "display:none");

		        //$("#USUARIO_DJ\\numeroDocumento").val("");
		        //$("#USUARIO_DJ\\nombre").val("");
	    	}/* if ( val == '3'){
	    		$("tr.dataProductor").attr("style", "display:none");
	    		$("#liExportador").attr("style", "display:");

		        //$("#USUARIO_DJ\\numeroDocumento").val("");
		        //$("#USUARIO_DJ\\nombre").val("");
	    	}*/
	    }

	    /*function limpiarRUC(){
	        $("#USUARIO_DJ\\numeroDocumento").val("");
	        $("#USUARIO_DJ\\nombre").val("");
	    }

	    function cargaDatosProductorRUC(){
	    	//alert('entre al ruc');
	        var numeroDocumento = document.getElementById("USUARIO_DJ.numeroDocumento").value;
	        limpiarRUC();
	        if (numeroDocumento != "") {
	            $("#loadingRUC").show();
	            var filter = "ruc=" + numeroDocumento;
	            loadElementAJX("co.ajax.ruc.elementLoader", "cargarDatosDJProductorRUC", null, filter, null, validaCargaRUC);
	        }
	    }

	    function validaCargaRUC(){
	        $("#loadingRUC").hide();
	        if($("#USUARIO_DJ\\.nombre").val() == ""){
	            $("#mensajeRUC").html("El n�mero de RUC consultado no es v�lido. Debe verificar el n�mero y volver a ingresar.");
	            $("#USUARIO_DJ\\.numeroDocumento").css('border', '1px solid red');
	    	} else {
	            $("#mensajeRUC").html("");
	            $("#USUARIO_DJ\\.numeroDocumento").css('border', '1px solid #0B2C6F');
			}
		}*/

	    function nuevoProductor(){
        	var f = document.formulario;
        	var idFormatoEntidad = f.idFormatoEntidad.value;
        	var orden=f.orden.value;
        	var mto=f.mto.value;
        	var bloqueado="N";//f.bloqueado.value;
            var secuenciaProductor = 0;
        	var djId = f.djId.value;
        	var modoProductorValidador = f.modoProductorValidador.value;
        	var esValidador = f.esValidador.value;
        	var modoFormato = f.modoFormato.value;
        	var idAcuerdo = f.idAcuerdo.value;

        	var exportador = "0";
        	if ($("#DJ\\.tipoRolDj_1").attr('checked')){
        		exportador = "1";
        	}

            showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaProductorForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaProductor="+secuenciaProductor+"&djId="+djId+"&exportador="+exportador+"&bloqueado="+bloqueado+"&modoProductorValidador="+modoProductorValidador+"&esValidador="+esValidador+"&modoFormato="+modoFormato+"&idAcuerdo="+idAcuerdo, 700, 450, null);
            //showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaMaterialForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaMaterial="+secuenciaMaterial+"&djId="+djId, 700, 500, null);
        	f.button.value="nuevoProductorButton";
	    }

	    function editarProductor(keyValues, keyValuesField){
        	var f = document.formulario;
        	var idFormatoEntidad = f.idFormatoEntidad.value;
        	var orden=f.orden.value;
        	var mto=f.mto.value;
        	var bloqueado=f.bloqueado.value;
            var secuenciaProductor = document.getElementById(keyValues.split("|")[0]).value;
        	var djId = f.djId.value;
        	var modoProductorValidador = f.modoProductorValidador.value;
        	var esValidador = f.esValidador.value;
        	var modoFormato = f.modoFormato.value;
        	var estadoReg = $("#DJ\\.estadoRegistro").val();
        	var idAcuerdo = f.idAcuerdo.value;

        	var exportador = "0";
        	if ($("#DJ\\.tipoRolDj_1").attr('checked')){
        		exportador = "1";
        		if (estadoReg != 'N' && estadoReg != 'O' && estadoReg != 'Q' && estadoReg != 'T' && estadoReg != 'I') {
        			bloqueado = "N";
        		}
        	} else {
        		//bloqueado = "S";
        	}
        	/*alert(exportador);
        	alert(bloqueado);*/
			//alert(bloqueado);
            showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaProductorForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaProductor="+secuenciaProductor+"&djId="+djId+"&exportador="+exportador+"&bloqueado="+bloqueado+"&modoProductorValidador="+modoProductorValidador+"&esValidador="+esValidador+"&modoFormato="+modoFormato+"&idAcuerdo="+idAcuerdo, 700, 450, null);
            //showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaMaterialForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaMaterial="+secuenciaMaterial+"&djId="+djId, 700, 500, null);
        	f.button.value="nuevoProductorButton";
	    }

	    // Actualiza el Consolidado de las Mercanc�as
	    function verConsolidadoAnt(){
	    	document.formulario.button.value="actualizarConsolidadoButton";
	    	if ($("#DJ\\.demasGasto").val() == ''){
				alert('Debe registrar el valor de "Dem�s Gasto"');
	    	} else {
	    		$("#CONSOLIDADO\\.mensaje").val("");
	    		var djId = $("#djId").val();
		    	var demasGasto = $("#DJ\\.demasGasto").val();
		    	var porcentajeSegunCriterio = $("#DJ\\.porcentajeSegunCriterio").val();
		    	var pesoNetoMercancia = $("#DJ\\.pesoNetoMercancia").val();
		    	var valorUsFabrica = $("#DJ\\.valorUsFabrica").val();

		    	// Si es vac�o se coloca una N para diferenciar
		    	if (porcentajeSegunCriterio == '') {
		    		porcentajeSegunCriterio = 'N';
		    	}

		    	if (pesoNetoMercancia == '') {
		    		pesoNetoMercancia = 'N';
		    	}

		    	if (valorUsFabrica == '') {
		    		valorUsFabrica = 'N';
		    	}

		    	var filter = "djId=" + djId +"|demasGasto=" + demasGasto + "|porcentajeSegunCriterio=" + porcentajeSegunCriterio +"|pesoNetoMercancia=" + pesoNetoMercancia + "|valorUsFabrica=" + valorUsFabrica;
		    	//alert(filter);
		    	loadElementAJX("co.ajax.certificado.material.elementLoader", "actualizarConsolidado", null, filter, null, mostrarMsgConsolidado);
	    	}
	    }
	    

	    function validarConsolidado(){
	    	var mensaje="Debe registrar : ";
	        var ok = true;

	        if ($("#DJ\\.valorUs").val() == ''){
	        	if ('${idAcuerdo}' == '11' || '${idAcuerdo}' == '12' || '${idAcuerdo}' == '14' || '${idAcuerdo}' == '18' || '${idAcuerdo}' == '19' || '${idAcuerdo}' == '23') {
	        		mensaje +="\n -El valor en US$ (precio unitario de la mercanc�a).";
	        	} else {
		        	mensaje +="\n -El valor en US";
	        	}
	        } /*else {
	        	if (parseInt($("#sumaValorUSMaterial").val(), 10) > parseInt($("#DJ\\.valorUs").val(), 10)){
		        	mensaje +="\n -El valor en US";

	        		if ('${idAcuerdo}' == '11' || '${idAcuerdo}' == '12' || '${idAcuerdo}' == '14' || '${idAcuerdo}' == '18' || '${idAcuerdo}' == '19' || '${idAcuerdo}' == '23') {
		        		mensaje += " (precio unitario de la mercanc�a)";
		        	}

	        		mensaje +=" que debe ser mayor a la sumatoria de los valores de los materiales";
	        	}
	        }*/

	        if (($("#requieredValorUS").attr("style") == "display:" || $("#requieredValorUS").attr("style") == "") && $("#DJ\\.valorUsFabrica").val() == ''){

	        	if ('${idAcuerdo}' == '12'){
	        		mensaje +="\n -El Valor en f�brica o el costo de producci�n.";
	        	} else {
	        		mensaje +="\n -El valor en US$ (franco f�brica).";
	        	}
	        }

	        if (($("#spanPorcentajeSegunCriterio").attr("style") == "display:" || $("#spanPorcentajeSegunCriterio").attr("style") == "") && $("#DJ\\.porcentajeSegunCriterio").val() == ''){
	        	mensaje +="\n -El Procentaje de VCR.";
	        }

	        if ( mensaje != "Debe registrar : " ) {
	            ok = false;
	            alert (mensaje);
	        }
	        return ok;
	    }
		/*
	    function validarConsolidado(){
	    	var mensaje="Debe registrar : ";
	        var ok = true;
			
	        if ($("#DJ\\.valorUs").val() == ''){
	        	if ('${idAcuerdo}' == '11' || '${idAcuerdo}' == '12' || '${idAcuerdo}' == '14' || '${idAcuerdo}' == '18' || '${idAcuerdo}' == '19' || '${idAcuerdo}' == '23') {
	        		mensaje +="\n -El valor en US$ (precio unitario de la mercanc�a).";
	        		changeStyleClass("co.label.dj.valor.us.precio.unitario", "errorValueClass");
	        	} else {
		        	mensaje +="\n -El valor en US$";
		        	changeStyleClass("co.label.dj.valor_en_us", "errorValueClass");
	        	}
	        } else {
	        	if ('${idAcuerdo}' == '11' || '${idAcuerdo}' == '12' || '${idAcuerdo}' == '14' || '${idAcuerdo}' == '18' || '${idAcuerdo}' == '19' || '${idAcuerdo}' == '23') {	        		
	        		changeStyleClass("co.label.dj.valor.us.precio.unitario", "labelClass");
	        	} else {		        	
		        	changeStyleClass("co.label.dj.valor_en_us", "labelClass");
	        	}	        	
	        } 
	       	
	        
	        if ( $("#DJ\\.porcentajeDemasGastos").val() == '') {
		        mensaje +="\n -El % de los demas gastos en base al valor FOB total de la mercancia";
		        changeStyleClass("co.label.dj.prctj_valor_fob_demas_gasto", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.prctj_valor_fob_demas_gasto", "labelClass");
		    }

	        if ( $("#DJ\\.demasGasto").val() == '') {
		        mensaje +="\n -Dem�s gastos (Mano de Obra, Otros gastos de fabricaci�n, gastos indirectos) y Utilidad US$";
		        changeStyleClass("co.label.dj.demas_gasto", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.demas_gasto", "labelClass");
		    }	        
	        
      
			
	        if (($("#requieredValorUS").attr("style") == "display:" || $("#requieredValorUS").attr("style") == "") && $("#DJ\\.valorUsFabrica").val() == ''){

	        	if ('${idAcuerdo}' == '12'){
	        		mensaje +="\n -El Valor en f�brica o el costo de producci�n.";
	        	} else {
	        		mensaje +="\n -El valor en US$ (franco f�brica).";	        		
	        	}	        	
	        	changeStyleClass("co.label.dj.${idAcuerdo}.valor.us", "errorValueClass");
	        	
	        } else {	        	
	        	changeStyleClass("co.label.dj.${idAcuerdo}.valor.us", "labelClass");
	        }
	        

	        if (($("#spanPorcentajeSegunCriterio").attr("style") == "display:" || $("#spanPorcentajeSegunCriterio").attr("style") == "") && $("#DJ\\.porcentajeSegunCriterio").val() == ''){
	        	mensaje +="\n -El Procentaje de VCR.";
	        }

	        if ( mensaje != "Debe registrar : " ) {
	            ok = false;
	            alert (mensaje);
	        }
	        return ok;
	    }
		*/


		function verConsolidado() {
			document.formulario.button.value="actualizarConsolidadoButton";
	    	if (validarConsolidado()){
	    		//$("#CONSOLIDADO\\.mensaje").val("");
	    		var f = document.formulario;
	    		f.action = contextName+"/origen.htm";
		        f.method.value="actualizarConsolidado";


	    		var djId = $("#djId").val();
		    	var valorUs = $("#DJ\\.valorUs").val();
		    	var porcentajeSegunCriterio = $("#DJ\\.porcentajeSegunCriterio").val();
		    	var pesoNetoMercancia = $("#DJ\\.pesoNetoMercancia").val();
		    	var valorUsFabrica = $("#DJ\\.valorUsFabrica").val();

		    	// Si es vac�o se coloca una N para diferenciar
		    	if (porcentajeSegunCriterio == '') {
		    		$("#DJ\\.porcentajeSegunCriterio").val('N');
		    	}

		    	if (pesoNetoMercancia == '') {
		    		$("#DJ\\.pesoNetoMercancia").val('N');
		    	}

		    	if (valorUsFabrica == '') {
		    		$("#DJ\\.valorUsFabrica").val('N');
		    	}

		    	//var filter = "djId=" + djId +"|demasGasto=" + demasGasto + "|porcentajeSegunCriterio=" + porcentajeSegunCriterio +"|pesoNetoMercancia=" + pesoNetoMercancia + "|valorUsFabrica=" + valorUsFabrica;
		    	//alert(filter);
		    	//loadElementAJX("co.ajax.certificado.material.elementLoader", "actualizarConsolidado", null, filter, null, mostrarMsgConsolidado);
	    	} else {
	    		document.formulario.button.value="cancelarButton";
	    	}

	    }

	    function mostrarMsgConsolidado(){
	    	alert($("#CONSOLIDADO\\.mensaje").val());
	    	contador = 0;
	    }

	    function mostrarTagMercancia(ocultar){
			if ($("#djId").val() != "0"){
		    	if (ocultar) {
					$("#liMateriales").attr("style", "display:none");
				} else {
					$("#liMateriales").attr("style", "display:");
				}
			}
	    }

	    /* function seleccionObtenido(obj) {
	    	if (obj.checked){
	    		$("#DJ\\.cumpleTotalmenteObtenido").val("S");
	    	} else {
	    		$("#DJ\\.cumpleTotalmenteObtenido").val("N");
	    	}
	    }

	    function seleccionClasif(obj){
	    	if (obj.checked){
	    		$("#DJ\\.cumpleCriterioCambioClasif").val("S");
	    	} else {
	    		$("#DJ\\.cumpleCriterioCambioClasif").val("N");
	    	}
	    } */

	    function adjuntarDocumentoDJ(keyValues, keyValuesField) {
	    	var f = document.formulario;
	        var adjuntoRequeridoDj = document.getElementById(keyValues.split("|")[0]).value;
	        var djId = f.djId.value;
	        var bloqueado = f.bloqueado.value;
        	var modoProductorValidador = f.modoProductorValidador.value;
        	var esValidador = f.esValidador.value;
        	var modoFormato = f.modoFormato.value;
	        showPopWin(contextName+"/origen.htm?method=cargarAdjuntosDJ&adjuntoRequeridoDj="+adjuntoRequeridoDj+"&djId="+djId+"&bloqueado="+bloqueado+"&modoFormato="+modoFormato, 750, 500, actualizar);
	    }
    
		function aprobarDJ() {
			var f = document.formulario;		
			if(confirm("Con esta acci�n usted resolver� la solicitud de calificaci�n de declaraci�n jurada, generar� y transmitir� el documento resolutivo.\n �Est� seguro que desea proceder?")){
					if ($("#partidaSegunAcuerdo").val() == '') {
		        		alert("Debe ingresar y grabar: \n -La partida seg�n acuerdo");
		        	} else {
			        	f.action = contextName+"/origen.htm";
				        f.method.value="aprobarTransmitirDeclaracionJurada";
				        f.button.value="aprobarDJButton";
				        f.calificacionUoId.value = window.parent.document.getElementById("CALIFICACION.calificacionUoId").value;
				        f.target = "_self";		        	
		        	}					
			}else {
				//Cancela
				f.button.value="cancelarButton";
			}


	    }

		function rechazarDJAnt() {
			var f = document.formulario;
			//alert(f.formato.value);
	        if(confirm("Esta seguro que desea rechazar la declaraci\u00f3n jurada?")){
	        	f.action = contextName+"/origen.htm";
	        	
	        	if(confirm("OK=Rechazar y Transmitir / Cancel = Solo Rechazar")){
	        		f.method.value="rechazarDeclaracionJurada";
	        	}else {
	        		f.method.value="rechazarTransmitirDeclaracionJurada";
				}

	        	f.button.value="rechazarDJButton";
		        f.calificacionUoId.value = window.parent.document.getElementById("CALIFICACION.calificacionUoId").value;
		        //f.target = window.parent.parent.name;
		        if (window.parent.parent.name == 'undefined') {
		        	f.target = window.parent.name;
		        } else {
		        	f.target = window.parent.parent.name;
		        }        	


			} else {
				f.button.value="cancelarButton";
			}
	    }

	    function rechazarDJ(){
        	var f = document.formulario;
        	var orden=f.orden.value;
        	var mto=f.mto.value;
        	var formato=f.formato.value;
        	var calificacionUoId = window.parent.document.getElementById("CALIFICACION.calificacionUoId").value;

            showPopWin(contextName+"/origen.htm?method=cargarDJRechazoForm&orden="+orden+"&mto="+mto+"&formato="+formato+"&calificacionUoId="+calificacionUoId, 400, 115, null);
            //showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaMaterialForm&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&secuenciaMaterial="+secuenciaMaterial+"&djId="+djId, 700, 500, null);
        	f.button.value="nuevoProductorButton";
	    }

		// Carga la lista de secuencias de origen
		/*function cargarCriterioOrigen(item, contador){

			var filter = "acuerdoId=" + document.getElementById("idAcuerdo").value + "|secuenciaNorma=" + document.getElementById("DJ.secuenciaNorma").value + "|secuencia=";
			//alert(contador);
			if (contador == 0) { // S�lo cuando es la primera vez debe realizarse el query
				if (item == "1"){ // cumple totalmente obtenido
					filter += FILTRO_SECUENCIA_1;
				} else if (item == "2") { // cumple criterio cambio clasificacion
					filter += FILTRO_SECUENCIA_2;
				} else { // otro criterio
					filter += FILTRO_SECUENCIA_3;
				}
				//alert(filter);
				loadSelectAJX("ajax.selectLoader", "loadList", "DJ.secuenciaOrigen", "criterio_origen.secuencia", filter, null, afterCargarOrigenSecuencia, null);
			}
		}

	    function cargarOrigenSecuencia() {
			var f = formulario.value;
			var acuerdoId = document.getElementById("idAcuerdo").value;
			var secuenciaNorma = document.getElementById("DJ.secuenciaNorma").value;
			var filter = "acuerdoId=" + acuerdoId + "|secuenciaNorma=" + secuenciaNorma + "|secuencia=";
			//alert('filtro secuencia 1 ' + FILTRO_SECUENCIA_1);
			//alert('filtro secuencia 2 ' + FILTRO_SECUENCIA_2);

			if ($("#DJ\\.cumpleTotalmenteObtenido").val() == "S") {
				filter += FILTRO_SECUENCIA_1;
			} else if ($("#DJ\\.cumpleCriterioCambioClasif").val() == "S") {
				filter += FILTRO_SECUENCIA_2;
			} else if ($("#DJ\\.cumpleOtroCriterio").val() == "S") {
				filter += (FILTRO_SECUENCIA_3!=undefined?FILTRO_SECUENCIA_3:-1);
			}

			//alert('filtro ' + filter);
			loadSelectAJX("ajax.selectLoader", "loadList", "DJ.secuenciaOrigen", "criterio_origen.secuencia", filter, null, afterCargarOrigenSecuencia, null);
	    }

	    function afterCargarOrigenSecuencia() {
	    	if ($("#secuenciaOrigen").val() != ""){
	    		$("#DJ\\.secuenciaOrigen").val($("#secuenciaOrigen").val());
	    	} else if ($("#bloqueado").val() != 'S'){
	    		if (($("#trCriterioOrigen").attr("style") == 'display:' || $("#trCriterioOrigen").attr("style") == '') && $("#DJ\\.secuenciaOrigen").attr("options").length == 2){
	    			$("#DJ\\.secuenciaOrigen").attr("options")[1].selected = true;
	    		}
	    	}
    		cargarOrigenCertificado();
	    }

	    function cargarOrigenCertificado(){
	       var f = formulario.value;
	       var acuerdoId = document.getElementById("idAcuerdo").value;
	       var secuenciaNorma = document.getElementById("DJ.secuenciaNorma").value;
	       var secuenciaOrigen = document.getElementById("DJ.secuenciaOrigen").value;
	       var filter = "acuerdoId=" + acuerdoId + "|secuenciaNorma=" + secuenciaNorma + "|secuenciaOrigen=" + secuenciaOrigen;
       	   $("#DJ\\.criterioOrigenCertificado").attr('disabled',false);
		   $("#DJ\\.criterioOrigenCertificado").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		   //alert('cargarOrigenCertificado' + filter);
	       loadSelectAJX("ajax.selectLoader", "loadList", "DJ.criterioOrigenCertificado", "criterio_origen.certificado", filter, null, afterCargarOrigenCertificado, null);
	    }

		// Se invoca despues de la carga asincrona del combo
		function afterCargarOrigenCertificado() {
			//var a = "a";
			//alert(document.getElementById("DJ.criterioOrigenCertificado").options.length);
			if (document.getElementById("DJ.criterioOrigenCertificado").options.length > 1) {
				document.getElementById("DJ.criterioOrigenCertificado").selectedIndex = 1;
		       	$("#DJ\\.criterioOrigenCertificado").attr('disabled',true);
				$("#DJ\\.criterioOrigenCertificado").removeClass("inputTextClass").addClass("readonlyInputTextClass");
			}
		}
*/

		// Solicita la validaci�n del productor
		function solicitarValidacion() {
			var f = document.formulario;
	        f.action = contextName+"/origen.htm";
	        f.method.value="solicitarValidacionDJProductor";
	        f.button.value="solicitarValidacionButton";
	    }

		function transmitirDjValidada() {
			var f = document.formulario;

	          f.action = contextName+"/origen.htm";
	          f.method.value="transmitirDjValidada";
	          f.button.value="transmitirDjButton";

	          f.target = window.parent.name;
	    }
		
		function cerrarVentana(){
			var f = document.formulario;
			f.button.value = "cancelarButton";
			<c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
				var modoFormato = f.modoFormato.value;
				if (modoFormato == 'S') {
					//alert('entrare a seleccionar dj formato del evaluador');
					seleccionarDjFormato();
				} else {
					salirDJ();
				}

				return false;
			</c:if>

			//alert(f.djId.value);
			if (f.djId.value == '0') {
				cerrarPopUp();
			} else {
				var modoProductorValidador = f.modoProductorValidador.value;
				var modoFormato = f.modoFormato.value;
				var modoLectura = f.modoLectura.value;
		   		var bloqueado = document.getElementById("bloqueado").value;
				//alert(modoProductorValidador);
				/*alert(modoFormato);
				alert(modoLectura);
				alert(bloqueado);*/
				if (modoProductorValidador == 'S' || modoLectura == 'S') { // En estos casos no hay un material padre, debemos cerrar el popup solamente
					cerrarPopUp();
				} else if (bloqueado == 'S' && f.estadoDj.value != 'N' & f.estadoDj.value != 'O') {
					cerrarPopUp();
				} else if (modoFormato == 'S' && ( $("#formato").val() == 'MCT005' || $("#formato").val() == 'mct005')) {
					seleccionarDjFormato();
				} else {
					seleccionarDj();
				}

			}

		}

	    function nuevaAutorizacionDJ(){
        	var f = document.formulario;
        	var djId = f.djId.value;
        	var idAcuerdo = f.idAcuerdo.value;
        	var idEntidad = f.idEntidadCertificadora.value;
        	var iniVigencia = (new Date($("#DJ\\.fechaInicioVigencia").val())).format("dd/mm/yyyy", false);
        	var finVigencia=(new Date($("#DJ\\.fechaFinVigencia").val())).format("dd/mm/yyyy", false);
        	var modoProductorValidador = 'S';
        	var esDJRegistrada = $("#esDJRegistrada").val();
        	var tipoRol = f.tipoRol.value;

        	showPopWin(contextName+"/origen.htm?method=cargarRegistroDJAutorizacionExp&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad+"&modoProductorValidador="+modoProductorValidador+"&iniVigencia="+iniVigencia+"&finVigencia="+finVigencia+"&esDJRegistrada="+esDJRegistrada+"&tipoRol="+tipoRol, 700, 500, null);
        	f.button.value="nuevaAutorizacionButton";
        }

		function revocarAutorizacion(keyValues, keyValuesField) {
			var f = document.formulario;

        	f.action = contextName+"/origen.htm";
	        f.method.value="reevocarDJAutorizacionExp";
	        f.button.value="revocarDjAutButton";
	        f.usuarioEmpId.value = document.getElementById(keyValues.split("|")[1]).value;

	        f.target = "_self";//window.parent.name;

	        f.submit();
	    }

	    function historicoAutorizacionDJ(keyValues, keyValuesField){
        	var f = document.formulario;
        	var djId = f.djId.value;
        	var idAcuerdo = f.idAcuerdo.value;
        	var idEntidad = f.idEntidadCertificadora.value;
        	var modoProductorValidador = 'S';
        	var usuarioEmpId=document.getElementById(keyValues.split("|")[1]).value;
        	var numeroDocumento=document.getElementById(keyValues.split("|")[2]).value;
        	var razonSocial=document.getElementById(keyValues.split("|")[3]).value;
        	var tipoRol = f.tipoRol.value;

        	showPopWin(contextName+"/origen.htm?method=cargarHistoricoDJAutorizacionExp&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad+"&modoProductorValidador="+modoProductorValidador+"&usuarioEmpId="+usuarioEmpId+"&numeroDocumento="+numeroDocumento+"&razonSocial="+razonSocial+"&tipoRol="+tipoRol, 700, 500, null);
        	f.button.value="nuevaAutorizacionButton";
        }

		 // Si existe una alerta al cargar el tab de material esta se mostrar� mediante este metodo
		 function emitirAlertaMaterial(){
		 	if ($("#bloqueado").val() != 'S' && alertaMaterial != ''){
		 		alert(alertaMaterial);
		 	}
		 }

		 function cargarPaisesXComponente(){
		 	var idAcuerdo = document.formulario.idAcuerdo.value;

		     showPopWin(contextName+"/origen.htm?method=cargarPaisesXComponente&idAcuerdo="+idAcuerdo, 400, 500, null);

		 }

	</script>
	<body id="contModal">
		<jlis:modalWindow />
	        <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="orden" type="hidden" />
	        <jlis:value name="mto" type="hidden" />
	        <jlis:value name="formato" type="hidden" />
            <jlis:value name="idFormatoEntidad" type="hidden" />
            <jlis:value name="coId" type="hidden" />
            <jlis:value name="djId" type="hidden" />
            <jlis:value name="secuenciaMercancia" type="hidden" />
            <jlis:value name="idAcuerdo" type="hidden" />
            <jlis:value name="idPais" type="hidden" />
            <jlis:value name="idEntidadCertificadora" type="hidden" />
            <jlis:value name="secuencia" type="hidden" />
            <jlis:value name="umFisicaId" type="hidden" />
            <jlis:value name="bloqueado" type="hidden" />
            <jlis:value name="tipoPais" type="hidden" />
            <jlis:value name="secuenciaOrigen" type="hidden" />
            <jlis:value name="DJ.cumpleTotalmenteObtenido" type="hidden" />
            <jlis:value name="DJ.cumpleCriterioCambioClasif" type="hidden" />
            <jlis:value name="DJ.cumpleOtroCriterio" type="hidden" />
            <jlis:value name="DJ.estadoRegistro" type="hidden" />
            <jlis:value name="DJ.fechaInicioVigencia" type="hidden" />
            <jlis:value name="DJ.fechaFinVigencia" type="hidden" />
            <input type="hidden" name="partidaSegunAcuerdo">
	        <jlis:value name="modoProductorValidador" type="hidden" />
	        <jlis:value name="esValidador" type="hidden" />
	        <jlis:value name="modoFormato" type="hidden" />
			<jlis:value name="CONSOLIDADO.mensaje" type="hidden" />
	        <jlis:value name="estadoDj" type="hidden" />
	        <jlis:value name="usuarioEmpId" type="hidden" />
	        <jlis:value name="permiteApoderamientoXFrmt" type="hidden" />
	        <jlis:value name="modoLectura" type="hidden" />
			<jlis:value name="modoValidadorLectura" type="hidden" />
	        <jlis:value name="matPeruEmpty" type="hidden" />
	        <jlis:value name="mat2doCompEmpty" type="hidden" />
	        <jlis:value name="mat3erCompEmpty" type="hidden" />
	        <jlis:value name="calificacion" type="hidden" />
	        <jlis:value name="numDj" type="hidden" />
	        <jlis:value name="esDJRegistrada" type="hidden" />
	        <jlis:value name="tipoRol" type="hidden" />
	        <jlis:value name="vigente" type="hidden" />
	        <jlis:value name="ordTransmitida" type="hidden" />
	        <jlis:value name="puedeTransmitir" type="hidden" />
	        <jlis:value name="djCalificada" type="hidden" />
	        <jlis:value name="reqValidacionProd" type="hidden" />
            <jlis:value name="DJ.paisIsoIdPorAcuerdoInt" type="hidden" value="${paisIsoIdPorAcuerdoInt}" />
            <jlis:value name="DJ.secuenciaOrigen" type="hidden" value="${secuenciaOrigen}" />
	        <jlis:value name="criterioArancelario" type="hidden" />
	        <jlis:value name="sumaValorUSMaterial" type="hidden" />
	        <input type="hidden" name="calificacionUoId" id="calificacionUoId" >

			<co:validacionBotonesFormato formato="DJ" />
			<%-- 20170203_GBT ACTA CO-004-16 3.2.b Y ACTA CO 009-16--%>
	        <c:if test="${estadoAcuerdoPais != 'I'}">
	        <jlis:button code="CO.ENTIDAD.EVALUADOR" id="calificaButton" name="aprobarDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Califica" alt="Pulse aqu� para aprobar la declaraci�n jurada" />
		    <jlis:button code="CO.ENTIDAD.EVALUADOR" id="noCalificaButton" name="rechazarDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="No Califica" alt="Pulse aqu� para rechazar la declaraci�n jurada" />
		    </c:if>
		    <c:if test="${estadoAcuerdoPais == 'I'}">
	        <jlis:button code="CO.ENTIDAD.EVALUADOR" id="calificaButton" editable="NO" name="aprobarDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Califica" alt="Pulse aqu� para aprobar la declaraci�n jurada" />
		    <jlis:button code="CO.ENTIDAD.EVALUADOR" id="noCalificaButton" editable="NO" name="rechazarDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="No Califica" alt="Pulse aqu� para rechazar la declaraci�n jurada" />
		    </c:if>
       		<%-- <jlis:button code="CO.ENTIDAD.EVALUADOR" id="salirButton" name="salirDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Salir" alt="Pulse aqu� para salir" /> --%>
           	<jlis:button id="cerrarPopUpButton" name="cerrarVentana()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
		    <jlis:button code="CO.USUARIO.OPERACION" id="transmitirDjButton" name="transmitirDjValidada()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir Validaci�n de DJ" alt="Pulse aqu� para transmitir la validaci�n de declaraci�n jurada" />
            
			<c:if test="${!empty acuerdo}">
				<table>
	                <tr>
	                    <td colspan="2" >&nbsp;</td>
	                </tr>
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.pais_acuerdo" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombrePais" editable="no" /></td>
	                </tr>
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.acuerdo" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="acuerdo" editable="no" /></td>
	                </tr>
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.entidad_certificadora" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="entidad" editable="no" /></td>
	                </tr>
	                <tr>
	                    <td colspan="2" >&nbsp;</td>
	                </tr>
	            </table>
            </c:if>

			<c:if test="${calificacion == 'R'}" >
				<table class="form">
					<tr>
						<td width="10%">Detalle :</td>
						<td><jlis:textArea rows="2" editable="yes" name="DJ.detalleDenegacion" style="width:92%" /></td>
					</tr>
				</table>
			</c:if>

           	<%-- </br>
           	Tipo Rol: ${tipoRol}
           	</br>
           	Bloqueado: ${bloqueado}
           	</br>
           	Modo Productor Validador: ${modoProductorValidador}
           	</br>
           	Es Validador: ${esValidador}
           	</br>
           	Estado: ${estadoDj}
           	</br>
           	Estado Registro CO: ${estadoRegistroCO}
           	</br>
           	Modo Formato: ${modoFormato}
           	</br> --%>

            <div class="block btabs btable"><div class="blocka"><div class="blockb">
		        <ul id="maintab" class="tabs">
					<li class="selected"><a href="#" rel="tabGeneral"><span>Producto</span></a></li>
					<c:if test="${djId != 0}" >
						<c:if test="${modoValidadorLectura == 'S' || (estadoDj != 'Q' && (tipoRol !='1' || reqValidacion == 'N')) || (estadoDj == 'P' && (empty esModificacion || esModificacion!='S')) || modoProductorValidador == 'S' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' || sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' || esValidadorDJ == 'S'}" >
							<li id="liMateriales" onclick="emitirAlertaMaterial()"><a href="#" rel="tabMaterial"><span>Materiales</span></a></li>
						</c:if>
	                	<c:if test="${modoValidadorLectura == 'S' || (estadoDj != 'Q' && (tipoRol !='1' || reqValidacion == 'N')) || (estadoDj == 'P' && (empty esModificacion || esModificacion!='S')) || modoProductorValidador == 'S' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' || sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' || esValidadorDJ == 'S'}" >
	                		<%-- <c:if test="${!(utilizaModificacionSuceXMto == 'S' && ordenVigente == 'N')}"> --%>
								<li id="liAdjuntos"><a href="#" rel="tabAdjuntos"><span>Adjuntos</span></a></li>
							<%-- </c:if> --%>
		                </c:if>
		                <c:if test="${estadoDj == 'A'}" >
	                		<c:if test="${(esDJRegistrada == 'S' && ( tipoRol == '2' || tipoRol == '3')) || (modoProductorValidador == 'S' && tipoRol == '1')}">
              					<li id="liAutorizaciones"><a href="#" rel="tabAutorizaciones"><span>Autorizaciones</span></a></li>
              				</c:if>
		                </c:if>
	                </c:if>
				</ul>
				<div id="tabGeneral" class="tabcontent" >
					<table class="form">
						<tr>
							<td colspan="2" >
								<jlis:button code="CO.USUARIO.OPERACION" id="grabarButton" name="grabarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar" alt="Pulse aqu� para grabar" />
								<%-- <c:if test="${sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.EVALUADOR'}">
							    	<jlis:button code="CO.USUARIO.OPERACION" id="salirButton" name="seleccionarDj()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Salir" alt="Pulse aqu� para salir" />
								</c:if> --%>
							</td>
						</tr>
						<tr>
							<td colspan="2"><hr/></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><span class="requiredValueClass"><jlis:label key="co.label.dj.subtitulo.detalle_mercancia" /></span></td>
						</tr>
		                <tr>
		                    <th>
		                    	<c:choose>
			      					<c:when test="${idAcuerdo == 16}">
										<jlis:label key="co.label.dj.${idAcuerdo}.denominacion" />
								 	</c:when>
									<c:otherwise>
										<jlis:label key="co.label.dj.denominacion" />
									</c:otherwise>
						  		</c:choose>
		                    </th>
		                    <td>
		                    	<jlis:textArea name="DJ.denominacion" rows="2" cols="90" onKeyUp="valida_longitud(this, 500);" onChange="valida_longitud(this, 500);"/><span class="requiredValueClass">(*)</span>
		                    	<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MERCANCIA.DENOMINACION" />
							</td>
		                </tr>
		                <tr>
		                    <th><jlis:label key="co.label.dj.caracteristica" /></th>
		                    <td><jlis:textArea name="DJ.caracteristica" rows="2" cols="90" onKeyUp="valida_longitud(this, 1000);" onChange="valida_longitud(this, 1000);"/><span class="requiredValueClass">(*)</span></td>
		                </tr>
		                <tr>
		                    <th><jlis:label key="co.label.partida_arancelaria" /></th>
		                    <td>
								<jlis:button code="CO.USUARIO.OPERACION" id="buscarPartidasButton" type="BUTTON_JAVASCRIPT_IMAGE_SUBMIT" name="buscarPartidas()" alt="Buscar Partida" image="<%=imagenBuscar%>" />
								<jlis:value type="hidden" name="DJ.partidaArancelaria" onChange="cargarUMs()" />
								<jlis:textArea name="DJ.denominacionArancelaria" editable="no" rows="2" cols="78" /><span class="requiredValueClass">(*)</span>
						    </td>
						</tr>
                        <tr>
                            <th colspan="2"><span class="requiredValueClass"><jlis:label key="co.label.dj.versionActualizadaArancelAduanas" /></span></th>
                        </tr>
		                <tr>
		                    <th><jlis:label key="co.label.dj.um_fisica_id" /></th>
		                    <td><jlis:selectProperty name="DJ.umFisicaId" key="select.empty" editable="yes"/><span class="requiredValueClass">(*)</span></td>
		                </tr>
		                <tr>
		                    <th><jlis:label key="co.label.dj.um_referencial" /></th>
							<td><jlis:textArea name="DJ.cantidadUmRefer" rows="2" cols="90" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);"/></td>
		                </tr>
						<tr class="textoJuramentoClass" >
							<td colspan="2"><strong><jlis:label key="co.label.dj.acepto_dj_texto" /></strong></td>
						</tr>
						<tr class="textoJuramentoClass" >
							<th><jlis:label key="co.label.dj.acepto_dj" /></th>
							<td ><jlis:value type="checkbox" name="DJ.aceptacion" checkValue="S" /><span class="requiredValueClass">(*)</span></td>
						</tr>
					</table>
				</div>
                <c:if test="${djId != 0}" >

               	<c:if test="${modoValidadorLectura == 'S' || (estadoDj != 'Q' && (tipoRol !='1' || reqValidacion == 'N')) || (estadoDj == 'P' && (empty esModificacion || esModificacion!='S')) || modoProductorValidador == 'S' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' || sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' || esValidadorDJ == 'S'}" >
				<div id="tabMaterial" class="tabcontent">
					<table class="form">
						<tr><td><span class="requiredValueClass"><jlis:label key="co.label.dj.titulo.tabla.materia_peru" /></span></td></tr>
						<tr>
							<td><jlis:button code="CO.USUARIO.OPERACION" id="nuevoMaterialPeruButton" name="nuevoMaterial(\"P\")" type="BUTTON_JAVASCRIPT_SUBMIT" title="Adicionar Material Originario de Per�" alt="Pulse aqu� para agregar un Nuevo Material Originario de Per�" /></td>
						</tr>
					</table>
					<jlis:table keyValueColumns="DJ_ID,SECUENCIA_MATERIAL" name="DJ_MATERIAL_PERU" source="tMaterialesPeru" scope="request" pageSize="*" width="90%" navigationHeader="no" >
							<jlis:tr>
								<jlis:td name="DJ_ID" nowrap="yes" />
								<jlis:td name="SECUENCIA_MATERIAL" nowrap="yes" />
								<jlis:td name="MATERIAL" nowrap="yes"  />
								<jlis:td name="PARTIDA_ARANCELARIA_COD" nowrap="yes"  />
								<jlis:td name="PARTIDA ARANCELARIA" nowrap="yes"  />
								<jlis:td name="FABRICANTE" nowrap="yes"  />
								<jlis:td name="UNIDAD_MEDIDA_UO" nowrap="yes"  />
								<jlis:td name="PA�S" nowrap="yes"  />
								<jlis:td name="CANTIDAD" nowrap="yes"  />
								<jlis:td name="UNIDAD" nowrap="yes"  />
								<jlis:td name="VALOR" nowrap="yes"  />
								<jlis:td name="% VALOR FOB" nowrap="yes"  />
								<c:choose>
			      					<c:when test="${bloqueado == 'N'}">
										<jlis:td name="EDITAR" nowrap="yes" width="2%" />
								 	</c:when>
									<c:otherwise>
										    <jlis:td name="VER" nowrap="yes" width="2%" />
									</c:otherwise>
						  		</c:choose>
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="DJ_ID" hide="yes" />
							<jlis:columnStyle column="2" columnName="SECUENCIA_MATERIAL" hide="yes" />
							<jlis:columnStyle column="3" columnName="DESCRIPCION"/>
							<jlis:columnStyle column="4" columnName="PARTIDA_ARANCELARIA" hide="yes" />
							<jlis:columnStyle column="5" columnName="PARTIDA_ARANCELARIA_DESC" />
							<jlis:columnStyle column="6" columnName="FABRICANTE_NOMBRE" />
							<jlis:columnStyle column="7" columnName="UM_FISICA_ID" hide="yes" />
							<jlis:columnStyle column="8" columnName="PAIS_PROCEDENCIA" hide="yes" />
							<jlis:columnStyle column="9" columnName="CANTIDAD" align="right" />
							<jlis:columnStyle column="10" columnName="UNIDAD_MEDIDA" align="center" />
							<jlis:columnStyle column="11" columnName="VALOR_US" align="right" />
							<jlis:columnStyle column="12" columnName="PORCENTAJE_VALOR" align="right" />
							<jlis:columnStyle column="13" columnName="EDITAR" editable="yes" align="center" />
							<c:choose>
		      					<c:when test="${bloqueado == 'N'}">
									<jlis:tableButton column="13" type="imageButton" image="/co/imagenes/editar.gif" onClick="editarMaterialPeru" alt="Editar Material" />
							 	</c:when>
							<c:otherwise>
								    <jlis:tableButton column="13" type="imageButton" image="/co/imagenes/ver.gif" onClick="editarMaterialPeru" alt="Ver Material" />
							</c:otherwise>
							</c:choose>
				 	</jlis:table>

					<table class="form">
							<tr><td>
								<span class="requiredValueClass">
									<jlis:label key="co.label.dj.titulo.tabla.${idAcuerdo}.materia_segundo_componente" />
									<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MATERIAL.DETALLE_PAISES_BENEFICIARIOS" />
									&nbsp;<a id="lnkPaises2doComponente" onClick="javascript:cargarPaisesXComponente();" style="display:none">Lista de Paises</a>
								</span></td></tr>
							<tr><td><jlis:button code="CO.USUARIO.OPERACION" id="nuevoMaterialSegundoComponenteButton" name="" type="BUTTON_JAVASCRIPT_SUBMIT" title="" alt="" /></td></tr>

					</table>

					<jlis:table keyValueColumns="DJ_ID,SECUENCIA_MATERIAL" name="DJ_MATERIAL_SEGUNDO_COMPONENTE" source="tMaterialesSegundoComponente" scope="request" pageSize="*" width="90%" navigationHeader="no" >
							<jlis:tr>
								<jlis:td name="DJ_ID" nowrap="yes" />
								<jlis:td name="SECUENCIA_MATERIAL" nowrap="yes" />
								<jlis:td name="MATERIAL" nowrap="yes"  />
								<jlis:td name="PARTIDA_ARANCELARIA_COD" nowrap="yes" />
								<jlis:td name="PARTIDA ARANCELARIA" nowrap="yes"  />
								<jlis:td name="FABRICANTE" nowrap="yes"  />
								<jlis:td name="UNIDAD_MEDIDA_UO" nowrap="yes" />
								<jlis:td name="PA�S DE PROCEDENCIA" nowrap="yes"  />
								<jlis:td name="CANTIDAD" nowrap="yes"  />
								<jlis:td name="UNIDAD" nowrap="yes" />
								<jlis:td name="VALOR" nowrap="yes"  />
								<jlis:td name="% VALOR FOB" nowrap="yes"  />
								<c:choose>
			      					<c:when test="${bloqueado == 'N'}">
										<jlis:td name="EDITAR" nowrap="yes" width="2%" />
								 	</c:when>
									<c:otherwise>
										    <jlis:td name="VER" nowrap="yes" width="2%" />
									</c:otherwise>
						  		</c:choose>
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="DJ_ID" hide="yes" />
							<jlis:columnStyle column="2" columnName="SECUENCIA_MATERIAL" hide="yes" />
							<jlis:columnStyle column="3" columnName="DESCRIPCION"/>
							<jlis:columnStyle column="4" columnName="PARTIDA_ARANCELARIA" hide="yes" />
							<jlis:columnStyle column="5" columnName="PARTIDA_ARANCELARIA_DESC" />
							<jlis:columnStyle column="6" columnName="FABRICANTE_NOMBRE" />
							<jlis:columnStyle column="7" columnName="UM_FISICA_ID" hide="yes" />
							<jlis:columnStyle column="8" columnName="PAIS_PROCEDENCIA" />
							<jlis:columnStyle column="9" columnName="CANTIDAD" align="right" />
							<jlis:columnStyle column="10" columnName="UNIDAD_MEDIDA" align="center" />
							<jlis:columnStyle column="11" columnName="VALOR_US" align="right" />
							<jlis:columnStyle column="12" columnName="PORCENTAJE_VALOR" align="right" />
							<jlis:columnStyle column="13" columnName="EDITAR" editable="yes" align="center" />
							<c:choose>
		      					<c:when test="${bloqueado == 'N'}">
									<jlis:tableButton column="13" type="imageButton" image="/co/imagenes/editar.gif" onClick="editarMaterialSegundoComponente" alt="Editar Material" />
							 	</c:when>
							<c:otherwise>
								    <jlis:tableButton column="13" type="imageButton" image="/co/imagenes/ver.gif" onClick="editarMaterialSegundoComponente" alt="Ver Material" />
							</c:otherwise>
							</c:choose>
				 	</jlis:table>

					<c:if test="${idAcuerdo == 5 || idAcuerdo == 11 || idAcuerdo == 14 || idAcuerdo == 19 || idAcuerdo == 20 || idAcuerdo == 21 || idAcuerdo == 22 || idAcuerdo == 23 || idAcuerdo == 25}">
						<table class="form">
							<tr>
								<tr><td><span class="requiredValueClass"><jlis:label key="co.label.dj.titulo.tabla.${idAcuerdo}.materia_tercer_componente" /></span></td></tr>
								<td>
									<jlis:button code="CO.USUARIO.OPERACION" id="nuevoMaterialTercerComponenteButton" name="" type="BUTTON_JAVASCRIPT_SUBMIT" title="" alt="" />
									&nbsp;<a id="lnkPaisesG2" onClick="javascript:cargarPaisesXComponente();" style="display:none">Lista de Paises</a>
								</td>
							</tr>
						</table>

						<jlis:table keyValueColumns="DJ_ID,SECUENCIA_MATERIAL" name="DJ_MATERIAL_TERCER_COMPONENTE" source="tMaterialesTercerComponente" scope="request" pageSize="*" width="90%" navigationHeader="no" >
								<jlis:tr>
									<jlis:td name="DJ_ID" nowrap="yes" />
									<jlis:td name="SECUENCIA_MATERIAL" nowrap="yes" />
									<jlis:td name="MATERIAL" nowrap="yes"  />
									<jlis:td name="PARTIDA_ARANCELARIA_COD" nowrap="yes" />
									<jlis:td name="PARTIDA ARANCELARIA" nowrap="yes"  />
									<jlis:td name="FABRICANTE" nowrap="yes"  />
									<jlis:td name="UNIDAD_MEDIDA_UO" nowrap="yes" />
									<jlis:td name="PA�S DE PROCEDENCIA" nowrap="yes"  />
									<jlis:td name="CANTIDAD" nowrap="yes"  />
									<jlis:td name="UNIDAD" nowrap="yes"  />
								    <jlis:td name="VALOR" nowrap="yes"  />
								    <jlis:td name="% VALOR FOB" nowrap="yes"  />
									<c:choose>
				      					<c:when test="${bloqueado == 'N'}">
											<jlis:td name="EDITAR" nowrap="yes" width="2%" />
									 	</c:when>
										<c:otherwise>
											    <jlis:td name="VER" nowrap="yes" width="2%" />
										</c:otherwise>
							  		</c:choose>
								</jlis:tr>
								<jlis:columnStyle column="1" columnName="DJ_ID" hide="yes" />
								<jlis:columnStyle column="2" columnName="SECUENCIA_MATERIAL" hide="yes" />
								<jlis:columnStyle column="3" columnName="DESCRIPCION"/>
								<jlis:columnStyle column="4" columnName="PARTIDA_ARANCELARIA" hide="yes" />
								<jlis:columnStyle column="5" columnName="PARTIDA_ARANCELARIA_DESC" />
								<jlis:columnStyle column="6" columnName="FABRICANTE_NOMBRE" />
								<jlis:columnStyle column="7" columnName="UM_FISICA_ID" hide="yes" />
								<jlis:columnStyle column="8" columnName="PAIS_PROCEDENCIA" />
								<jlis:columnStyle column="9" columnName="CANTIDAD" align="right" />
								<jlis:columnStyle column="10" columnName="UNIDAD_MEDIDA" align="center" />
							    <jlis:columnStyle column="11" columnName="VALOR_US" align="right" />
							    <jlis:columnStyle column="12" columnName="PORCENTAJE_VALOR" align="right" />
								<jlis:columnStyle column="13" columnName="EDITAR" editable="yes" align="center" />
								<c:choose>
			      					<c:when test="${bloqueado == 'N'}">
										<jlis:tableButton column="13" type="imageButton" image="/co/imagenes/editar.gif" onClick="editarMaterialTercerComponente" alt="Editar Material" />
								 	</c:when>
								<c:otherwise>
									    <jlis:tableButton column="13" type="imageButton" image="/co/imagenes/ver.gif" onClick="editarMaterialTercerComponente" alt="Ver Material" />
								</c:otherwise>
								</c:choose>
					 	</jlis:table>
					</c:if>

					<table class="form">
							<tr><td><span class="requiredValueClass"><jlis:label key="co.label.dj.titulo.tabla.materia_otros" /></span></td></tr>
							<tr><td><jlis:button code="CO.USUARIO.OPERACION" id="nuevoMaterialNoOriginarioButton" name="nuevoMaterial(\"N\")" type="BUTTON_JAVASCRIPT_SUBMIT" title="Adicionar Material No Originario" alt="Pulse aqu� para agregar un Nuevo Material No Originario" /></td></tr>
					</table>

					<jlis:table keyValueColumns="DJ_ID,SECUENCIA_MATERIAL" name="DJ_MATERIAL_NO_ORIGINARIO" source="tMaterialesNoOriginarios" scope="request" pageSize="*" width="90%" navigationHeader="no" >
							<jlis:tr>
								<jlis:td name="DJ_ID" nowrap="yes" />
								<jlis:td name="SECUENCIA_MATERIAL" nowrap="yes" />
								<jlis:td name="MATERIAL" nowrap="yes"  />
								<jlis:td name="PARTIDA_ARANCELARIA_COD" nowrap="yes" />
								<jlis:td name="PARTIDA ARANCELARIA" nowrap="yes"  />
								<jlis:td name="FABRICANTE_NOMBRE" nowrap="yes"  />
								<jlis:td name="UNIDAD_MEDIDA_UO" nowrap="yes"  />
								<jlis:td name="PA�S DE PROCEDENCIA" nowrap="yes"  />
								<jlis:td name="CANTIDAD" nowrap="yes"  />
								<jlis:td name="UNIDAD" nowrap="yes"  />
								<jlis:td name="VALOR" nowrap="yes"  />
								<jlis:td name="% VALOR FOB" nowrap="yes"  />
								<c:choose>
			      					<c:when test="${bloqueado == 'N'}">
										<jlis:td name="EDITAR" nowrap="yes" width="2%" />
								 	</c:when>
									<c:otherwise>
										    <jlis:td name="VER" nowrap="yes" width="2%" />
									</c:otherwise>
						  		</c:choose>
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="DJ_ID" hide="yes" />
							<jlis:columnStyle column="2" columnName="SECUENCIA_MATERIAL" hide="yes" />
							<jlis:columnStyle column="3" columnName="DESCRIPCION"/>
							<jlis:columnStyle column="4" columnName="PARTIDA_ARANCELARIA" hide="yes" />
							<jlis:columnStyle column="5" columnName="PARTIDA_ARANCELARIA_DESC" />
							<jlis:columnStyle column="6" columnName="FABRICANTE_NOMBRE" hide="yes" />
							<jlis:columnStyle column="7" columnName="UM_FISICA_ID" hide="yes" />
							<jlis:columnStyle column="8" columnName="PAIS_PROCEDENCIA" />
							<jlis:columnStyle column="9" columnName="CANTIDAD" align="right" />
							<jlis:columnStyle column="10" columnName="UNIDAD_MEDIDA" align="center" />
							<jlis:columnStyle column="11" columnName="VALOR_US" align="right" />
							<jlis:columnStyle column="12" columnName="PORCENTAJE_VALOR" align="right" />
							<jlis:columnStyle column="13" columnName="EDITAR" editable="yes" align="center" />
							<c:choose>
		      					<c:when test="${bloqueado == 'N'}">
									<jlis:tableButton column="13" type="imageButton" image="/co/imagenes/editar.gif" onClick="editarMaterialOtro" alt="Editar Material" />
							 	</c:when>
							<c:otherwise>
								    <jlis:tableButton column="13" type="imageButton" image="/co/imagenes/ver.gif" onClick="editarMaterialOtro" alt="Ver Material" />
							</c:otherwise>
							</c:choose>
				 	</jlis:table>

					<%-- <c:if test="${tipoRol == 2}"> --%>
						<table class="form">
							<tr>
								<td colspan="2" ><h2 class="psubtitle" style="font:bold 12px Arial"><span>Dem�s Gastos y Utilidades</span></h2></td>
							</tr>
							<%--20140626_JMC BUG 124--%>
							<c:if test="${estadoDj == 'T' || estadoDj == 'L' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'}">
								<tr>
									<th><jlis:label key="co.label.dj.demas_gasto" /></th>
									<td>
										<jlis:value name="DJ.demasGasto" size="22" maxLength="22" editable="no" onBlur="validarDoubleNegativoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>
										<span class="requiredValueClass">(*)</span>
										<%-- co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MATERIAL.DEMAS_GASTO" material="si" codigoPais="${tipoPais}" acuerdoId="${idAcuerdo}" /--%>
									</td>
								</tr>
								<tr>
									<th><jlis:label key="co.label.dj.prctj_valor_fob_demas_gasto" /></th>
									<td><jlis:value name="DJ.porcentajeDemasGastos" size="22" maxLength="22" editable="no" onBlur="validarDoubleNegativoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>
										<span class="requiredValueClass">(*)</span>
										<%-- co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MATERIAL.PORC_DEMAS_GASTO" material="si" codigoPais="${tipoPais}" acuerdoId="${idAcuerdo}" /--%>
									</td>
								</tr>
							</c:if>
							<tr >
								<th>
								<c:choose>
									<c:when test="${idAcuerdo == '11' || idAcuerdo == '12' || idAcuerdo == '14' || idAcuerdo == '18' || idAcuerdo == '19' || idAcuerdo == '23'||idAcuerdo == '27'}">
										<jlis:label key="co.label.dj.valor.us.precio.unitario" />
									</c:when>
									<c:otherwise>
										<jlis:label key="co.label.dj.valor_en_us" />
									</c:otherwise>
								</c:choose>
								</th>
								<td>
									<jlis:value name="DJ.valorUs" size="22" editable="yes" maxLength="22" onBlur="validarDoubleNegativoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>
									<span id="requieredValorUSPrecUnit" style="display:" class="requiredValueClass">(*)</span>
								</td>
							</tr>

							<tr style="display:none">
								<th><jlis:label key="co.label.dj.prctj_valor_fob_mercancia" /></th>
								<td>
									<jlis:value name="prctjValorFobMercancia" size="22"  maxLength="22" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>
								</td>
							</tr>
							<%-- <c:if test="${estadoDj == 'T' || estadoDj == 'L'}" > --%>
								<tr id="trPorcentajeSegunCriterio" style="display:none" >
									<th><jlis:label key="co.label.dj.prctj_segun_criterio" /></th>
									<td>
										<jlis:value name="DJ.porcentajeSegunCriterio" size="22" maxLength="22" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right" pattern="0.000000"/>
										<span id="spanPorcentajeSegunCriterio" style="display:none" class="requiredValueClass">(*)</span>
										<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MATERIAL.PORCENTAJE_VCR" />
									</td>
								</tr>
								<%-- <tr id="trValorUSPrecUnit" style="display:none" >
									<th><jlis:label key="co.label.dj.valor.us.precio.unitario" /></th>
									<td>
										<jlis:value name="DJ.valorUS" size="22" maxLength="22" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>

									</td>
								</tr> --%>
							<%-- </c:if> --%>
							<tr id="trValorUS" style="display:none" >
								<th><jlis:label key="co.label.dj.${idAcuerdo}.valor.us" /></th>
								<td>
									<jlis:value name="DJ.valorUsFabrica" size="22" maxLength="22" onBlur="validarDoubleNegativoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>
									<span id="requieredValorUS" style="display:none" class="requiredValueClass">(*)</span>
									<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MATERIAL.VALOR_US_FABRICA" />
								</td>
							</tr>
							<tr id="trPesoNetoMercancia" style="display:none" >
								<th><jlis:label key="co.label.dj.peso_neto_mercancia" /></th>
								<td>
									<jlis:value name="DJ.pesoNetoMercancia" size="22" maxLength="22" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000" />
									<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.CONSOLIDADO.PESO" />
								</td>
							</tr>
						</table>

						<table class="form">
							<tr>
								<td><jlis:button code="CO.USUARIO.OPERACION" id="grabarConsolidadoButton" name="verConsolidado()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Calcular dem�s Gastos y Utilidades" alt="Pulse aqu� para calcular dem�s gastos y utilidades" /></td>
							</tr>
						</table>
					<%-- </c:if> --%>

				</div>
				<div id="tabAdjuntos" class="tabcontent">
					<jsp:include page="/WEB-INF/jsp/certificadoOrigen/adjuntosRequeridosDJ.jsp" />
				</div>
           <c:if test="${estadoDj == 'A'}" >
          		<c:if test="${(esDJRegistrada == 'S' && ( tipoRol == '2' || tipoRol == '3')) || (modoProductorValidador == 'S' && tipoRol == '1')}">
		            <div id="tabAutorizaciones" class="tabcontent">
		              <table class="form">
		                <tr>
		                  <td>
		                    <jlis:button code="CO.USUARIO.OPERACION" id="nuevaAutorizacionButton" name="nuevaAutorizacionDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Registrar Nuevo Exportador" alt="Pulse aqu� para agregar un Nuevo Exportador" />&nbsp;
		                  </td>
		                </tr>
		              </table>
		              <jlis:table keyValueColumns="DJ_ID,USUARIO_ID_EMP_AUT,NUMERO_DOCUMENTO,NOMBRE" name="DJ_AUTORIZACION" source="tAutorizaciones" scope="request" pageSize="*" width="90%" navigationHeader="no" >
		                <jlis:tr>
		                  <jlis:td name="DJ_ID" nowrap="yes" />
		                  <jlis:td name="USUARIO_ID_EMP_AUT" nowrap="yes" />
		                  <jlis:td name="RUC" nowrap="yes"  />
		                  <jlis:td name="EXPORTADOR" nowrap="yes"  />
		                  <jlis:td name="REGISTRO" nowrap="yes"  />
		                  <jlis:td name="INICIO AUT." nowrap="yes"  />
		                  <jlis:td name="FIN AUT." nowrap="yes"  />
		                  <jlis:td name="REVOCACI�N" nowrap="yes"  />
		                  <jlis:td name="ESTADO" nowrap="yes"  />
		                  <jlis:td name="HISTORIAL" nowrap="yes"  />
		                  <jlis:td name="REVOCAR" nowrap="yes"  />
		                </jlis:tr>
		                <jlis:columnStyle column="1" columnName="DJ_ID" hide="yes" />
		                <jlis:columnStyle column="2" columnName="USUARIO_ID_EMP_AUT" hide="yes" />
		                <jlis:columnStyle column="3" columnName="NUMERO_DOCUMENTO" />
		                <jlis:columnStyle column="4" columnName="NOMBRE" />
		                <jlis:columnStyle column="5" columnName="REGISTRO" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="6" columnName="INICIO_AUTORIZACION" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="7" columnName="FIN_AUTORIZACION" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="8" columnName="REVOCACION" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="9" columnName="DESCRIPCION_ESTADO_AUT" />
		                <jlis:columnStyle column="10" columnName="HISTORIAL" editable="yes" align="center" />
		                <jlis:columnStyle column="11" columnName="REVOCAR" editable="yes" align="center" validator="pe.gob.mincetur.vuce.co.web.util.DJAutorizacionCellValidator"/>
		                <jlis:tableButton column="10" type="imageButton" image="/co/imagenes/ver.gif" onClick="historicoAutorizacionDJ" alt="Ver Historial" />
		        		<jlis:tableButton column="11" type="imageButton" image="" onClick="" alt="" />
		               </jlis:table>
		             </div>
					</c:if>
                </c:if>
				</c:if>
                </c:if>
			</div></div></div>
		</form>
	</body>
</html>