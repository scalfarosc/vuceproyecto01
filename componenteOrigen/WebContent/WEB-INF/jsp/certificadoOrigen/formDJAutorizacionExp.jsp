<%
	String controller = (String) request.getAttribute("controller");
%>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script>

	 $(document).ready(function() {
			tituloPopUp("Exportador - Certificado de Origen");
		    //initializetabcontent("maintab");
		    validaCambios();


            /*var bloqueado = document.getElementById("bloqueado").value;
        	if(bloqueado=="S"){
        		bloquearControles();
            }*/

            if ($("#fechaInicio").val() == ''){
            	$("#fechaInicio").val((new Date()).format("dd/mm/yyyy", false));
            }
            if ($("#fechaFin").val() == ''){
            	$("#fechaFin").val($("#finVigencia").val());
            }

            if ($("#numeroDocumento").val()!='') {

            	if ('${tipoRol}' == '3' && '${usuTDoc}' == $("#documentoTipo").val() &&
            		'${usuNumDoc}' == $("#numeroDocumento").val()){

            		$("#fechaInicio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	   				$("#fechaFin").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	   				$("#fechaInicio").attr("disabled", true);
	   				$("#fechaFin").attr("disabled", true);
            	}

            }

	    });

		function validarSubmit() {
		    var f=document.formulario;

		    if (f.button.value=="cancelarButton") {
	            return false;
	        }
		    if (f.button.value=="cerrarPopUpButton") {
	        	return validaCerraPopUp();
	        }
		    return true;
		}

		function validarCampos(){

		    var ok = true;
		    var mensaje="Debe ingresar o seleccionar : ";

		    if (document.getElementById("documentoTipo").value==""){
		        mensaje +="\n -El tipo de documento";
		        changeStyleClass("co.label.usuario_dj.documento_tipo", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.documento_tipo", "labelClass");

		    if (document.getElementById("numeroDocumento").value==""){
		        mensaje +="\n -El n�mero de documento";
		        changeStyleClass("co.label.usuario_dj.numero_documento", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.numero_documento", "labelClass");

		    if (document.getElementById("nombre").value==""){
		        mensaje +="\n -El nombre del productor";
		        changeStyleClass("co.label.usuario_dj.nombre", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.nombre", "labelClass");

		    if (document.getElementById("direccion").value==""){
		        mensaje +="\n -La direcci�n del productor";
		        changeStyleClass("co.label.usuario_dj.direccion", "errorValueClass");
		    }else changeStyleClass("co.label.usuario_dj.direccion", "labelClass");

		    if (document.getElementById("fechaInicio").value==""){
		        mensaje +="\n -La fecha de inicio de la autorizaci�n";
		        changeStyleClass("co.label.dj.autorizacion.fecha.inicio", "errorValueClass");
		    }else changeStyleClass("co.label.dj.autorizacion.fecha.inicio", "labelClass");	    

		    if (document.getElementById("fechaFin").value=="" && !document.getElementById('fechaFin').disabled){
		        mensaje +="\n -La fecha de fin de la autorizaci�n";
		        changeStyleClass("co.label.dj.autorizacion.fecha.fin", "errorValueClass");
		    }else changeStyleClass("co.label.dj.autorizacion.fecha.fin", "labelClass");

		    if (document.getElementById("fechaInicio").value!="" && document.getElementById("fechaFin").value!=""){
		    	var inicio = $("#fechaInicio").val().split('/');
	    		var fin = $("#fechaFin").val().split('/');
	            var fechaInicio = new Date(inicio[2],inicio[1]-1,inicio[0]);
	            var fechaFin = new Date(fin[2],fin[1]-1,fin[0]);
			    if (fechaFin < fechaInicio){
			    	mensaje +="\n -En la fecha de fin de autorizaci�n un valor superior a la fecha de inicio.";
			        //$("#fechaFin").val("");
			    }
		    }

		    if (mensaje!="Debe ingresar o seleccionar : ") {
		        ok= false;
		        alert (mensaje);
		    } else {
		    	if ('${tipoRol}' == '3' && '${usuTDoc}' == $("#documentoTipo").val() &&
	            	'${usuNumDoc}' == $("#numeroDocumento").val()){
		    		ok = confirm('Sr. Productor: �Est� seguro de querer declararse como Exportador? Al realizar esta acci�n, se cambiar� su rol de "Productor" a "Productor / Exportador" en la presente Declaraci�n Jurada. Este cambio es irreversible');
		    	}
		    }
		    return ok;
		}

		function grabarRegistro() {
			var f = document.formulario;
	        if(validarCampos()){
   				$("#fechaInicio").attr("disabled", false);
   				$("#fechaFin").attr("disabled", false);
	        	f.action = contextName+"/origen.htm";
		        f.method.value="registrarDJAutorizacionExp";
		        f.button.value="grabarButton";
		        //f.target = window.parent.name;
			} else {
				f.button.value="cancelarButton";
			}
	    }

	    function limpiarRUC(){
	        $("#numeroDocumento").val("");
	        $("#nombre").val("");
	        $("#direccion").val("");
	    }

	    function cargaDatosExportadorRUC(){
	    	if ($("#documentoTipo").val() == '1'){
		        var numeroDocumento = document.getElementById("numeroDocumento").value;
		        limpiarRUC();
		        if (numeroDocumento != "") {
		            $("#loadingRUC").show();
		            var filter = "ruc=" + numeroDocumento + "|subPantalla=1";
		            loadElementAJX("co.ajax.ruc.elementLoader", "cargarDatosDJExportadorRUC", null, filter, null, validaCargaRUC);
		        }
	    	}
	    }

	    function validaCargaRUC(){
	        $("#loadingRUC").hide();
	        if($("#.nombre").val() == ""){
	            $("#mensajeRUC").html("El n�mero de RUC consultado no es v�lido. Debe verificar el n�mero y volver a ingresar.");
	            $("#.numeroDocumento").css('border', '1px solid red');

	            if ('${tipoRol}' == '3') {
	        		$("#fechaInicio").removeClass("readonlyInputTextClass").addClass("inputTextClass");
	   				$("#fechaFin").removeClass("readonlyInputTextClass").addClass("inputTextClass");
	   				$("#fechaInicio").attr("disabled", false);
	   				$("#fechaFin").attr("disabled", false);
	   				$("#imgCalendar_fechaInicio").attr("style", "display:");
	   				$("#imgCalendar_fechaFin").attr("style", "display:");
	   				$("#fechaFin").val("");
	            }
	    	} else {
	            $("#mensajeRUC").html("");
	            $("#.numeroDocumento").css('border', '1px solid #0B2C6F');

	            if ('${tipoRol}' == '3') {
	            	if ( '${usuTDoc}' == $("#documentoTipo").val() && '${usuNumDoc}' == $("#numeroDocumento").val() ) {
    		    		//$("#fechaInicio").val($("#iniVigencia").val());
    		    		$("#fechaInicio").val((new Date()).format("dd/mm/yyyy", false));
    		    		$("#fechaFin").val($("#finVigencia").val());

                		$("#fechaInicio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
    	   				$("#fechaFin").removeClass("inputTextClass").addClass("readonlyInputTextClass");
    	   				$("#fechaInicio").attr("disabled", true);
    	   				$("#fechaFin").attr("disabled", true);
    	   				$("#imgCalendar_fechaInicio").attr("style", "display:none");
    	   				$("#imgCalendar_fechaFin").attr("style", "display:none");
    	   				alert('El RUC ingresado es igual al del productor que gener� la declaraci�n. La vigencia ser� la misma que la de la declaraci�n');
    	   				//$("#fechaFin").val( ( new Date() ).format("dd/mm/yyyy", false) );
                	} else {
                		$("#fechaInicio").removeClass("readonlyInputTextClass").addClass("inputTextClass");
    	   				$("#fechaFin").removeClass("readonlyInputTextClass").addClass("inputTextClass");
    	   				$("#fechaInicio").attr("disabled", false);
    	   				$("#fechaFin").attr("disabled", false);
    	   				$("#imgCalendar_fechaInicio").attr("style", "display:");
    	   				$("#imgCalendar_fechaFin").attr("style", "display:");
    	   				//$("#fechaFin").val("");
                	}
	            }
			}
		}

	    function validarFechaIni(obj){

	    	if ($("#fechaInicio").val() != ''){
	    		var elem = $("#fechaInicio").val().split('/');
	    		var min = $("#iniVigencia").val().split('/');
	    		var max = $("#finVigencia").val().split('/');
	            var fechaMin = new Date(min[2],min[1]-1,min[0]);
	            var fechaMax = new Date(max[2],max[1]-1,max[0]);
	            var fechaIni = new Date(elem[2],elem[1]-1,elem[0]);
			    if (fechaIni < fechaMin){
			        alert("La fecha de inicio de la autorizaci�n no puede ser menor a la fecha de inicio de vigencia de la declaraci�n.");
			        $("#fechaInicio").val("");
			    } else if (fechaIni > fechaMax) {
			        alert("La fecha de inicio de la autorizaci�n no puede ser mayor a la fecha de fin de vigencia de la declaraci�n.");
			        $("#fechaInicio").val("");
			    }
	    	}

		}

	    function validarFechaFin(obj){

	    	if ($("#fechaFin").val() != ''){
	    		var elem = $("#fechaFin").val().split('/');
	    		var min = $("#iniVigencia").val().split('/');
	    		var max = $("#finVigencia").val().split('/');
	            var fechaMin = new Date(min[2],min[1]-1,min[0]);
	            var fechaMax = new Date(max[2],max[1]-1,max[0]);
	            var fechaFin = new Date(elem[2],elem[1]-1,elem[0]);
	            if (fechaFin < fechaMin){
			        alert("La fecha de fin de la autorizaci�n no puede ser menor a la fecha de inicio de vigencia de la declaraci�n.");
			        $("#fechaFin").val("");
			    } else if (fechaFin > fechaMax){
			        alert("La fecha de fin de la autorizaci�n no puede ser mayor a la fecha de fin de vigencia de la declaraci�n.");
			        $("#fechaFin").val("");
			    }
	    	}

		}

	</script>
	<body id="contModal">
  	    <jlis:modalWindow onClose="actualizar()" />
	        <jlis:messageArea width="100%" />
        <br/>
        <form name="formulario" method="post" onSubmit="return validarSubmit();">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
            <jlis:value name="djId" type="hidden" />
            <jlis:value name="calificacionUoId" type="hidden" />
            <jlis:value name="idAcuerdo" type="hidden" />
            <jlis:value name="idEntidad" type="hidden" />
            <jlis:value name="iniVigencia" type="hidden" />
            <jlis:value name="finVigencia" type="hidden" />
            <jlis:value name="modoProductorValidador" type="hidden" />
	        <jlis:value name="esDJRegistrada" type="hidden" />
	        <jlis:value name="tipoRol" type="hidden" />

            <!-- <div class="block btabs btable">
				<div class="blocka">
					<div class="blockb">
				        <ul id="maintab" class="tabs">
							<li class="selected"><a href="#" rel="tabGeneral"><span>Datos del Productor</span></a></li>
						</ul>
						<div id="tabGeneral" class="tabcontent">  -->
							<table class="form">
								<tr>
									<td colspan="2">
										<jlis:button code="CO.USUARIO.OPERACION" id="grabarButton" name="grabarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Grabar Exportador" alt="Pulse aqu� para registrar al exportador " />
            							<jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
									</td>
								</tr>
								<tr>
									<td colspan="2"><hr/></td>
								</tr>
								<tr>
									<td colspan="2"><span class="requiredValueClass"><jlis:label key="co.label.dj.subtitulo.adm.exportador" /></span></td>
								</tr>
				                <tr>
				                    <th><jlis:label key="co.label.usuario_dj.documento_tipo" /></th>
									<%-- <td><jlis:selectProperty name="DJ_PRODUCTOR.documentoTipo" key="comun.tipo_documento.juridica.select" editable="yes" onChange="evalCambioTDoc()" /><span class="requiredValueClass">(*)</span></td> --%>
									<td><jlis:selectProperty name="documentoTipo" key="comun.tipo_documento.juridica.select" editable="yes" onChange="cargaDatosExportadorRUC()" /><span class="requiredValueClass">(*)</span></td>
				                </tr>
								<tr>
				                    <th><jlis:label key="co.label.usuario_dj.numero_documento" /></th>
									<td><jlis:value name="numeroDocumento" size="11" maxLength="11" type="text" onChange="cargaDatosExportadorRUC()"/><span class="requiredValueClass">(*)</span><img id="loadingRUC" style="display:none" src="/co/imagenes/loading.gif"/></td>
				                </tr>
				                <tr>
				                    <th><jlis:label key="co.label.usuario_dj.nombre" /></th>
				                    <td>
				                    	<jlis:textArea name="nombre" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);" editable="no" /><span class="requiredValueClass">(*)</span>
				                    </td>
				                </tr>
				                <tr>
				                    <th><jlis:label key="co.label.usuario_dj.direccion" /></th>
				                    <td>
				                    	<jlis:textArea name="direccion" rows="2" cols="70" onKeyUp="valida_longitud(this, 250);" onChange="valida_longitud(this, 250);" editable="no" /><span class="requiredValueClass">(*)</span>
				                    </td>
				                </tr>
				                <tr>
				                    <th><jlis:label key="co.label.dj.autorizacion.fecha.inicio" /></th>
				                    <td>
				                    	<jlis:date form="formulario" name="fechaInicio" size="20" pattern="dd/MM/yyyy" onChange="validarFechaIni()" /><span class="requiredValueClass">(*)</span>
				                    </td>
				                </tr>
				                <tr>
				                    <th><jlis:label key="co.label.dj.autorizacion.fecha.fin" /></th>
				                    <td>
				                    	<jlis:date form="formulario" name="fechaFin" size="20" pattern="dd/MM/yyyy" onChange="validarFechaFin()" /><span class="requiredValueClass">(*)</span>
				                    </td>
				                </tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
				                <tr class="ruc"><td colspan="2" ><span id="mensajeRUC" class="requiredValueClass"></span></td></tr>
							</table>

					<!-- 	</div>

					</div>
				</div>
			</div> -->
		</form>
	</body>
</html>