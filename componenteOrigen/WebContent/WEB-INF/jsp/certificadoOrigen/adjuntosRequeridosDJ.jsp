<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>

<table class="form">
	<tr>
    	<td><h3 class="psubtitle"><span><b><jlis:label key="co.title.documentos_dj_adjuntar"  /></b></span></h3>	</td>
	</tr>
	<tr>
 		<td>&nbsp;</td>
	</tr>
</table>

<jlis:table name="ADJUNTO_REQUERIDO" keyValueColumns="ADJUNTO_REQUERIDO_UO" key="dj.adjunto_requerido.grilla" pageSize="*" navigationHeader="no" width="100%" filter="${filterADJ}">
	<jlis:tr>
		<jlis:td name="ADJUNTO REQUERIDO" nowrap="yes" />
		<jlis:td name="OBLIGATORIO" nowrap="yes" width="4%"/>
		<jlis:td name="DESCRIPCIÓN" nowrap="yes" width="92%"/>
		<jlis:td name="ADJUNTOS" nowrap="yes" width="4%"/>
		<jlis:td name="XX" />
	</jlis:tr>
	<jlis:columnStyle column="1" columnName="ADJUNTO_REQUERIDO_UO" hide="yes" />
	<jlis:columnStyle column="2" columnName="OBLIGATORIO" align="center" />
	<jlis:columnStyle column="3" columnName="DESCRIPCION" editable="yes" />
	<jlis:columnStyle column="5" columnName="XX" hide="yes" />
	<jlis:tableButton column="3" type="link" onClick="adjuntarDocumentoDJ" />
	<jlis:columnStyle column="4" columnName="CONTEO" align="center" style="font-size:14px; color:blue; font-weight:600"/>
</jlis:table>

<table class="form">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
	<c:if test="${idAcuerdo == 26}"><td><jlis:label key="co.label.dj.adjuntos.proceso_productivo.26" labelClass="labelHelpClass" /></td></c:if>
	<c:if test="${idAcuerdo != 26}"><td><jlis:label key="co.label.dj.adjuntos.proceso_productivo" labelClass="labelHelpClass" /></td></c:if>
	</tr>
</table>
