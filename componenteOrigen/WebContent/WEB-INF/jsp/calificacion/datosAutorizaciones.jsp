<script language="JavaScript">

$(document).ready(function() {
	var estadoAcuerdoPais = '${estadoAcuerdoPais}';
	//alert(estadoAcuerdoPais);
	//if (estadoAcuerdoPais=='I') {
	//	alert("No se puede asignar la solicitud, el acuerdo comercial con este pa�s no se encuentra vigente");
	//}
	
});
	    function nuevaAutorizacion(){
        	var f = document.formulario;
        	var djId = f.djId.value;
        	var idAcuerdo = f.idAcuerdo.value;
        	var idEntidad = f.idEntidadCertificadora.value;
        	var calificacionUoId = document.getElementById("CALIFICACION.calificacionUoId").value;
        	var iniVigencia = (new Date($("#iniVigencia").val())).format("dd/mm/yyyy", false);
        	var finVigencia=(new Date($("#finVigencia").val())).format("dd/mm/yyyy", false);
        	var modoProductorValidador = 'S';
        	var esDJRegistrada = $("#esDJRegistrada").val();
        	var tipoRol = getSelectedRadioValue(document.formulario.elements["CALIFICACION.tipoRolCalificacion"]); //f.tipoRol.value;

        	showPopWin(contextName+"/origen.htm?method=cargarRegistroDJAutorizacionExp&calificacionUoId="+calificacionUoId+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad+"&modoProductorValidador="+modoProductorValidador+"&iniVigencia="+iniVigencia+"&finVigencia="+finVigencia+"&esDJRegistrada="+esDJRegistrada+"&tipoRol="+tipoRol, 700, 500, null);
        	f.button.value="nuevaAutorizacionButton";
        }
		
	    function revocarAutorizacion(keyValues, keyValuesField) {
	        var f = document.formulario;
	        f.action = contextName+"/origen.htm";
	        f.method.value="reevocarDJAutorizacionExp";
	        f.button.value="actualizarButton";
	        f.usuarioEmpId.value = document.getElementById(keyValues.split("|")[2]).value;
	        f.target = "_self";
	        f.submit();
	    }

      function historicoAutorizacion(keyValues, keyValuesField){
    	var f = document.formulario;
    	var calificacionUoId =document.getElementById(keyValues.split("|")[0]).value;
    	var usuarioEmpId=document.getElementById(keyValues.split("|")[2]).value;
    	var numeroDocumento=document.getElementById(keyValues.split("|")[3]).value;
    	var razonSocial=document.getElementById(keyValues.split("|")[4]).value;

    	showPopWin(contextName+"/origen.htm?method=cargarHistoricoDJAutorizacionExp&calificacionUoId="+calificacionUoId+"&usuarioEmpId="+usuarioEmpId+"&numeroDocumento="+numeroDocumento+"&razonSocial="+razonSocial, 700, 500, null);
    	f.button.value="nuevaAutorizacionButton";
    }

</script>
		 <table class="form">
		                <tr>
		                  <td>
		                  	<%-- 20170131_GBT ACTA CO-004-16 Y ACTA CO-009-16 3.3--%>
		                  	<c:if test="${estadoAcuerdoPais != 'I'}">
		                    	<jlis:button code="CO.USUARIO.OPERACION" id="nuevaAutorizacionButton" name="nuevaAutorizacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Registrar Nuevo Exportador" alt="Pulse aqu� para agregar un Nuevo Exportador" />&nbsp;
		                    </c:if>
		                    <c:if test="${estadoAcuerdoPais == 'I'}">
		                    	<jlis:button code="CO.USUARIO.OPERACION" id="nuevaAutorizacionButton" editable="NO" name="nuevaAutorizacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Registrar Nuevo Exportador" alt="Pulse aqu� para agregar un Nuevo Exportador" />&nbsp;
		                    </c:if>
		                  </td>
		                </tr>
		 </table>
		 
	<br/>
		<jlis:table keyValueColumns="CALIFICACION_UO_ID,DJ_ID,USUARIO_ID_EMP_AUT,NUMERO_DOCUMENTO,NOMBRE" name="AUTORIZACION" source="tAutorizaciones" scope="request" pageSize="*" navigationHeader="no" >
		                <jlis:tr>
		                  <jlis:td name="CALIFICACION_UO_ID" nowrap="yes" />
		                  <jlis:td name="DJ_ID" nowrap="yes" />
		                  <jlis:td name="USUARIO_ID_EMP_AUT" nowrap="yes" />
		                  <jlis:td name="RUC" nowrap="yes" width="10%" />
		                  <jlis:td name="EXPORTADOR" nowrap="yes" width="38%" />
		                  <jlis:td name="REGISTRO" nowrap="yes" width="10%" />
		                  <jlis:td name="INICIO AUT." nowrap="yes"  width="10%"/>
		                  <jlis:td name="FIN AUT." nowrap="yes" width="10%" />
		                  <jlis:td name="REVOCACI�N" nowrap="yes" width="5%"/>
		                  <jlis:td name="ESTADO" nowrap="yes" width="7%" />
		                  <jlis:td name="HISTORIAL" nowrap="yes" width="5%"/>
		                  <jlis:td name="REVOCAR" nowrap="yes"  width="5%"/>
		                </jlis:tr>
		                <jlis:columnStyle column="1" columnName="CALIFICACION_UO_ID" hide="yes" />
		                <jlis:columnStyle column="2" columnName="DJ_ID" hide="yes" />
		                <jlis:columnStyle column="3" columnName="USUARIO_ID_EMP_AUT" hide="yes" />
		                <jlis:columnStyle column="4" columnName="NUMERO_DOCUMENTO" />
		                <jlis:columnStyle column="5" columnName="NOMBRE" />
		                <jlis:columnStyle column="6" columnName="REGISTRO" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="7" columnName="INICIO_AUTORIZACION" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="8" columnName="FIN_AUTORIZACION" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="9" columnName="REVOCACION" type="dateTime" pattern="dd/MM/yyyy" />
		                <jlis:columnStyle column="10" columnName="DESCRIPCION_ESTADO_AUT" />
		                <jlis:columnStyle column="11" columnName="HISTORIAL" editable="yes" align="center" />
		                <jlis:columnStyle column="12" columnName="REVOCAR" editable="yes" align="center" validator="pe.gob.mincetur.vuce.co.web.util.DJAutorizacionCellValidator"/>
		                <jlis:tableButton column="11" type="imageButton" image="/co/imagenes/ver.gif" onClick="historicoAutorizacion" alt="Ver Historial" />
		        		<jlis:tableButton column="12" type="imageButton" image="" onClick="" alt="" />
		               </jlis:table>

