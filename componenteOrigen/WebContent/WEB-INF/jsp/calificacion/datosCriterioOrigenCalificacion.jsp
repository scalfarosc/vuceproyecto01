<script language="JavaScript">

	// Inicializaci�n de esta p�gina
	$(document).ready(function() {

		// Seteamos el valor de los radiobuttons de acuerdo a lo elegido
		if ($("#CALIFICACION\\.cumpleTotalmenteObtenido").val() == 'S') {
			$("#CALIFICACION\\.criterioQueCumple_1").attr('checked',true);
		} else if ($("#CALIFICACION\\.cumpleCriterioCambioClasif").val() == 'S') {
			$("#CALIFICACION\\.criterioQueCumple_2").attr('checked',true);
		} else if ($("#CALIFICACION\\.cumpleOtroCriterio").val() == 'S') {
			$("#CALIFICACION\\.criterioQueCumple_3").attr('checked',true);
		}
		// flagCriterio = 1 : Proviene de la inicializaci�n
		document.getElementById("flagCriterio").value = 1;

/*		if ( '${requestScope["CALIFICACION.secuenciaNorma"]}' != '' ) {
			$("#CALIFICACION\\.secuenciaNorma").val('${requestScope["CALIFICACION.secuenciaNorma"]}');
		}

		if ( '${requestScope["CALIFICACION.secuenciaOrigen"]}' != '' ) {
			$("#CALIFICACION\\.secuenciaOrigen").val('${requestScope["CALIFICACION.secuenciaOrigen"]}');
		}
*/
		//alert('${requestScope["CALIFICACION.secuenciaOrigen"]}');
		//alert(document.getElementById("CALIFICACION.secuenciaNorma").value);
		//alert(document.getElementById("CALIFICACION.secuenciaOrigen").value);

		//$("#partidaSegunAcuerdo").val($("CALIFICACION\\.partidaSegunAcuerdo").val());
		cargarOrigenSecuencia();

   		if ($("#CALIFICACION\\.calificacion").val() != '') {
   			if ($("#DJ\\.calificacion").val() == 'A'){
   				$("#tdCalificacion").attr("innerHTML", "APROBADA");
   			} else if ($("#DJ\\.calificacion").val() == 'R'){
   				$("#tdCalificacion").attr("innerHTML", "RECHAZADA");
   			}

   			$("#trCalificacion").attr("style", "display:");
   		}

	    var bloqueado = document.getElementById("bloqueado").value;
   	    if (bloqueado == "S") {
   	    	bloquearControles();
   	    }

   	    inicializacionCalificacionOrigenSegunAcuerdo();
	});

	// Graba los datos del criterio de calificaci�n de origen
	function grabarCriterioCalificacion() {

		if ($("#CALIFICACION\\.criterioQueCumple_1").attr('checked')) {
			$("#CALIFICACION\\.cumpleTotalmenteObtenido").val("S");
			$("#CALIFICACION\\.cumpleCriterioCambioClasif").val("N");
			$("#CALIFICACION\\.cumpleOtroCriterio").val("N");
		} else if ($("#CALIFICACION\\.criterioQueCumple_2").attr('checked')) {
			$("#CALIFICACION\\.cumpleTotalmenteObtenido").val("N");
			$("#CALIFICACION\\.cumpleCriterioCambioClasif").val("S");
			$("#CALIFICACION\\.cumpleOtroCriterio").val("N");
		} else if ($("#CALIFICACION\\.criterioQueCumple_3").attr('checked')) {
			$("#CALIFICACION\\.cumpleTotalmenteObtenido").val("N");
			$("#CALIFICACION\\.cumpleCriterioCambioClasif").val("N");
			$("#CALIFICACION\\.cumpleOtroCriterio").val("S");
		}

		var f = document.formulario;
		if ( validarCamposCriterioCalificacion() && confirmacionesCriterioCalificacion() ) {
			f.action = contextName+"/origen.htm";
			f.method.value="actualizaCriterioCalificacionOrigen";
			f.button.value="GrabarRolCalificacionButton";
		} else {
			f.button.value="cancelarButton";
		}
	}

	// Carga la lista de secuencias de origen
/*	function cargarCriterioOrigen(item, contador){

		var filter = "acuerdoId=" + document.getElementById("idAcuerdo").value + "|secuenciaNorma=" + '${requestScope["CALIFICACION.secuenciaNorma"]}' + "|secuencia=";
		//if (contador == 0) { // S�lo cuando es la primera vez debe realizarse el query
			if (item == "1"){ // cumple totalmente obtenido
				filter += FILTRO_SECUENCIA_1;
			} else if (item == "2") { // cumple criterio cambio clasificacion
				filter += FILTRO_SECUENCIA_2;
			} else { // otro criterio
				filter += (FILTRO_SECUENCIA_3 != undefined ? FILTRO_SECUENCIA_3 : -1);
			}
			//alert(filter);
			loadSelectAJX("ajax.selectLoader", "loadList", "CALIFICACION.secuenciaOrigen", "criterio_origen.secuencia", filter, null, afterCargarOrigenSecuencia, null);
		//}
	}*/

	// Carga la secuencia Origen
    function cargarOrigenSecuencia() {
		var f = formulario.value;
		var secuenciaNorma = -1;
		var acuerdoId = document.getElementById("idAcuerdo").value;
		var flagCriterio = document.getElementById("flagCriterio").value;

		// Verificamos el valor de secuencia norma dependiendo del origen de la invocaci�n
		secuenciaNorma = ( flagCriterio == 1 ? ('${requestScope["CALIFICACION.secuenciaNorma"]}' == '' ? document.getElementById("CALIFICACION.secuenciaNorma").value :'${requestScope["CALIFICACION.secuenciaNorma"]}') : document.getElementById("CALIFICACION.secuenciaNorma").value );

		// Generamos el filtro a ejecutar
		var filter = "acuerdoId=" + acuerdoId + "|secuenciaNorma=" + secuenciaNorma + "|secuencia=";

		if ($("#CALIFICACION\\.cumpleTotalmenteObtenido").val() == "S") {
			filter += FILTRO_SECUENCIA_1;
		} else if ($("#CALIFICACION\\.cumpleCriterioCambioClasif").val() == "S") {
			filter += FILTRO_SECUENCIA_2;
		} else if ($("#CALIFICACION\\.cumpleOtroCriterio").val() == "S") {
			filter += (FILTRO_SECUENCIA_3!=undefined?FILTRO_SECUENCIA_3:-1);
		}

		// Seteamos la norma
		$("#CALIFICACION\\.secuenciaNorma").val(secuenciaNorma);

		loadSelectAJX("ajax.selectLoader", "loadList", "CALIFICACION.secuenciaOrigen", "criterio_origen.secuencia", filter, null, afterCargarOrigenSecuencia, null);
    }

	// Funci�n que se ejectua despu�s de la invocaci�n as�ncrona que llena la secuencia origen
    function afterCargarOrigenSecuencia() {
    	var secuenciaOrigen = -1;
		//var flagCriterio = document.getElementById("CALIFICACION.criterioQueCumple").value;
		var flagCriterio = document.getElementById("flagCriterio").value;

		// Verificamos el valor de secuencia norma dependiendo del origen de la invocaci�n
		secuenciaOrigen = ( flagCriterio == 1 ? ('${requestScope["CALIFICACION.secuenciaOrigen"]}' == '' ? document.getElementById("CALIFICACION.secuenciaOrigen").value : '${requestScope["CALIFICACION.secuenciaOrigen"]}') : document.getElementById("CALIFICACION.secuenciaOrigen").value );

    	if ( secuenciaOrigen != "" ) {
			$("#CALIFICACION\\.secuenciaOrigen").val(secuenciaOrigen);
    	} else if ($("#bloqueado").val() != 'S'){
    		if (($("#trCriterioOrigen").attr("style") == 'display:' || $("#trCriterioOrigen").attr("style") == '') && $("#CALIFICACION\\.secuenciaOrigen").attr("options").length == 2){
    			$("#CALIFICACION\\.secuenciaOrigen").attr("options")[1].selected = true;
    		}
    	}

		cargarOrigenCertificado();
    }

	// Carga el origen del certificado
    function cargarOrigenCertificado(){
		var f = formulario.value;
		var secuenciaNorma = -1;
		var secuenciaOrigen = -1;
		var acuerdoId = document.getElementById("idAcuerdo").value;
		var flagCriterio = document.getElementById("flagCriterio").value;

		// Verificamos el valor de secuencia norma dependiendo del origen de la invocaci�n
		secuenciaNorma = ( flagCriterio == 1 ? ( '${requestScope["CALIFICACION.secuenciaNorma"]}' == '' ? document.getElementById("CALIFICACION.secuenciaNorma").value : '${requestScope["CALIFICACION.secuenciaNorma"]}') : document.getElementById("CALIFICACION.secuenciaNorma").value );
		secuenciaOrigen = ( flagCriterio == 1 ? ( '${requestScope["CALIFICACION.secuenciaOrigen"]}' == '' ? document.getElementById("CALIFICACION.secuenciaOrigen").value : '${requestScope["CALIFICACION.secuenciaOrigen"]}' ) : document.getElementById("CALIFICACION.secuenciaOrigen").value );

		if(secuenciaOrigen == '3'){
			$('#trPorcentajeSegunCriterio').hide();
		}
		
		// Generamos el filtro a ejecutar
       var filter = "acuerdoId=" + acuerdoId + "|secuenciaNorma=" + secuenciaNorma + "|secuenciaOrigen=" + secuenciaOrigen;
   	   $("#CALIFICACION\\.criterioOrigenCertificado").attr('disabled',false);
	   $("#CALIFICACION\\.criterioOrigenCertificado").removeClass("readonlyInputTextClass").addClass("inputTextClass");
	   //alert('cargarOrigenCertificado' + filter);
       loadSelectAJX("ajax.selectLoader", "loadList", "CALIFICACION.criterioOrigenCertificado", "criterio_origen.certificado", filter, null, afterCargarOrigenCertificado, null);
    }

	// Se invoca despues de la carga as�ncrona que llena el origen del certificado
	function afterCargarOrigenCertificado() {
		if (document.getElementById("CALIFICACION.criterioOrigenCertificado").options.length > 1) {
			document.getElementById("CALIFICACION.criterioOrigenCertificado").selectedIndex = 1;
	       	$("#CALIFICACION\\.criterioOrigenCertificado").attr('disabled',true);
			$("#CALIFICACION\\.criterioOrigenCertificado").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		}
		// Maneja los valores que dependen de los combos din�micos de esta p�gina
   	    manejarValoresDependientesDeCombosDinamicosSegunAcuerdo();

	}

	// Crear o Editar DJ
	function abrirDJ(){
       	var f = document.formulario;
       	
       	var orden = f.orden.value;
       	var mto = f.mto.value;
       	var idAcuerdo = f.idAcuerdo.value;
	    var bloqueado = f.bloqueado.value;
	    var formato = f.formato.value.toLowerCase();
	    var puedeTransmitir = f.puedeTransmitir.value;
	    var calificacionUoId = document.getElementById("CALIFICACION.calificacionUoId").value;
	    var calificacion = document.getElementById("CALIFICACION.calificacion").value;
	    //alert(calificacionUoId);

	    // Si existe lo usamos, caso contrario le ponemos un valor de cero
        var djId = ( document.getElementById("CALIFICACION.djId").value == '' ? '0' : document.getElementById("CALIFICACION.djId").value );
	    var secuenciaOrigen = ( document.getElementById("CALIFICACION.secuenciaOrigen").value == '' ? '-1' : document.getElementById("CALIFICACION.secuenciaOrigen").value );
	    var paisIsoIdPorAcuerdoInt = ( document.getElementById("CALIFICACION.paisIsoIdxAcuerdoInt").value == '' ? '-1' : document.getElementById("CALIFICACION.paisIsoIdxAcuerdoInt").value );
	    var secuenciaMercancia = document.getElementById("secuencia") != null ? document.getElementById("secuencia").value : "";
	    var tipoRolCalificacion = document.getElementById("tipoRolCalificacion") != null ? document.getElementById("tipoRolCalificacion").value : "";
	    var esPropietarioDatos = document.getElementById("esPropietarioDatos") != null ? document.getElementById("esPropietarioDatos").value : "";
        //alert(djId);
        /*alert(orden);
        alert(mto);
        alert(idAcuerdo);
        alert(bloqueado);
        alert(formato);
        alert(puedeTransmitir);*/
        var criterioArancelario = obtenerCriterioArancelario();
        showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&formato="+formato+"&bloqueado="+bloqueado+"&modoFormato=S&puedeTransmitir="+puedeTransmitir+"&calificacionUoId="+calificacionUoId+"&secuenciaOrigen="+secuenciaOrigen+"&paisIsoIdPorAcuerdoInt="+paisIsoIdPorAcuerdoInt+"&criterioArancelario="+criterioArancelario+"&calificacion="+calificacion+"&secuenciaMercancia="+secuenciaMercancia+"&tipoRolCalificacion="+tipoRolCalificacion+"&esPropietarioDatos="+esPropietarioDatos, 1000, 550, null);
		f.button.value = "abrirDjButton";
	}

	// Asocia el id de la DJ una vez escogido
	function asociarDj(djId) {
		var id = document.getElementById("djId");
		id.value = djId;
		refrescarFormato();
	}

	/**
	*	Los valores para el flag ser�n:
		0: No cumploe ningun criterio
	*   1: Cumple Criterio Totalmente Obtenido
	*   2: Cumple Criterio Cambio Clasificacion Arancelaria
	*   3: Otro Criterio
	**/
    function obtenerCriterioArancelario() {
    	var f = document.formulario;
    	var result = 0;
		if ($("#CALIFICACION\\.cumpleTotalmenteObtenido").val() == "S") {
			result = 1;
		} else if ($("#CALIFICACION\\.cumpleCriterioCambioClasif").val() == "S") {
			result = 2;
		} else if ($("#CALIFICACION\\.cumpleOtroCriterio").val() == "S") {
			result = 3;
		}

    	return result;
    }

    function verProducto(){
    	var f = document.formulario;
    	var idAcuerdo = f.idAcuerdo.value;
        var calificacionUoId = $("#CALIFICACION\\.calificacionUoId").val();
    	showPopWin(contextName+"/origen.htm?method=cargarFormProductoDj&calificacionUoId="+calificacionUoId+"&idAcuerdo="+idAcuerdo, 500, 300, null);
        }

</script>

<jlis:value name="CALIFICACION.cumpleTotalmenteObtenido" type="hidden" />
<jlis:value name="CALIFICACION.cumpleCriterioCambioClasif" type="hidden" />
<jlis:value name="CALIFICACION.cumpleOtroCriterio" type="hidden" />
<jlis:value name="CALIFICACION.paisIsoIdxAcuerdoInt" type="hidden" />
<jlis:value name="CALIFICACION.calificacion" type="hidden" />
<jlis:value name="flagCriterio" type="hidden" />



		<table class="form">
			<tr>
				<td colspan="2">
				
			    	<c:if test="${(sessionScope.USUARIO.tipoOrigen == 'ET' && (sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || 
			    	              sessionScope.USUARIO.rolActivo == 'CO.USUARIO.SUPERVISOR'  ||
			    	              sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'  || 
			    	              sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.HELP_DESK' ||  
			    	              sessionScope.USUARIO.rolActivo == 'CO.ADMIN.HELP_DESK' )) || 
			    	               (tieneRolAsignado == 'N' || tipoRolCalificacion == '2' || tipoRolCalificacion=='3'||tipoRolCalificacion=='4' ||tipoRolCalificacion=='5')||
			    	              (solicitoCalificacion =='S' && tipoRolCalificacion=='1' && reqValidacion == 'S' && (productorValidador == 'N' || mostrarBtnValidacionProd == '1'))||
			    	                (solicitoCalificacion =='S' && tipoRolCalificacion=='1' && reqValidacion =='N')||
			    	              (solicitoCalificacion =='S' && tipoRolCalificacion=='6' && reqValidacion == 'S' && (productorValidador == 'N' || mostrarBtnValidacionProd == '1'))||
			    	                (solicitoCalificacion =='S' && tipoRolCalificacion=='6' && reqValidacion =='N')||
			    	                (modoProductorValidador=='S' && (tipoRolCalificacion=='1' || tipoRolCalificacion=='6'))}">
			    	              
			    	  <tr>
						<td colspan="2" ><h3 class="psubtitle"><span><b><jlis:label key="co.title.declaracion_jurada" /></b></span></h3></td>
					  </tr>
					   <tr>
						<td colspan="2"> 
						
							<jlis:button id="DeclaracionJuradaButton" name="abrirDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Declaraci�n Jurada" alt="Pulse aqu� para crear o actualizar datos de la declaraci�n jurada" />						
							<span class="requiredValueClass">(*)</span>
						</td>
			          </tr>
					</c:if>
					

			<tr>
				<td colspan="2" ><h3 class="psubtitle"><span><b><jlis:label key="co.title.criterio_origen" /></b></span></h3></td>
			</tr>
			<tr>
				<td colspan="2" >
					<jlis:button code="CO.USUARIO.OPERACION" id="grabarCOButton" name="grabarCriterioCalificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar" alt="Pulse aqu� para grabar" />
				</td>
			</tr>
			<tr>
				<th width="20%" ><jlis:label key="co.label.dj.cumple_totalmente_obtenido" /></th>
				<td width="80%" >
					<jlis:value type="radio" name="CALIFICACION.criterioQueCumple" checkValue="1" defaultCheck="no" onClick="changeCriterio(this)" />
					<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MERCANCIA.CUMPLE_TOTALMENTE_OBTENIDO" />
				</td>
			</tr>
			<tr id="trCambioClasificacion" style="display:none" >
				<th><jlis:label key="co.label.dj.cumple_criterio_cambio_clasif" /></th>
				<td>
					<jlis:value type="radio" name="CALIFICACION.criterioQueCumple" checkValue="2" defaultCheck="no" onClick="changeCriterio(this)" />
					<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MERCANCIA.CUMPLE_CRITERIO_CAMBIO_CLASIF" />
				</td>
			</tr>
			<tr id="trCambioClasificacionOtroCriterio" >
				<th><jlis:label key="co.label.dj.evalua_otro_criterio" /></th>
				<td>
					<jlis:value type="radio" name="CALIFICACION.criterioQueCumple" checkValue="3" defaultCheck="no" onClick="changeCriterio(this)" />
				</td>
			</tr>
			<tr id="trCalificacion" style="display:none">
				<th><jlis:label key="co.label.numero_calificacion" /></th>
				<td id="tdCalificacion" class='labelClass' ></td>
			</tr>
			<tr id="trNorma" style="display:none" >
				<th><jlis:label key="co.label.dj.norma" /></th>
				<td nowrap><jlis:selectProperty name="CALIFICACION.secuenciaNorma" key="acuerdo_internacional.norma.secuencia" filter="${cmbFilter}" width="110" editable="yes" onChange="document.getElementById('flagCriterio').value = 2; cargarOrigenSecuencia();"/><span class="requiredValueClass">(*)</span>
				<!--  <a class="labelClass" style="text-decoration:underline;" href="http://www.mincetur.gob.pe/newweb/Portals/0/comercio/certf_origen/Criterios_de_Origen.pdf" target="_blank" >Criterios de Origen</a> -->
				<!-- GBT 20171004 ACTA CO-005-2017 -->
				<a class="labelClass" style="text-decoration:underline;" href="https://www.mincetur.gob.pe/wp-content/uploads/documentos/comercio_exterior/certificacion_de_origen/certificacion_por_entidades/exportadorautorizado/Criterios_de_Origen.pdf" target="_blank" >Criterios de Origen</a>
				</td>
			</tr>
			<tr id="trCriterioOrigen" style="display:none" >
				<th><jlis:label key="co.label.dj.criterio_origen" /></th>
				<td><jlis:selectProperty name="CALIFICACION.secuenciaOrigen" key="select.empty" editable="yes" onChange="document.getElementById('flagCriterio').value = 2; cargarOrigenCertificado();"/><span class="requiredValueClass">(*)</span></td>
			</tr>
			<tr id="trCriterioOrigenCertif" style="display:none" >
				<th><jlis:label key="co.label.dj.criterio_origen_certificado" /></th>
				<td><jlis:selectProperty name="CALIFICACION.criterioOrigenCertificado" key="select.empty" editable="yes"/><span class="requiredValueClass">(*)</span></td>
			</tr>
			<tr id="trDisposicion" style="display:none" >
				<th><jlis:label key="co.label.dj.disposicion" /></th>
				<td><jlis:selectProperty name="CALIFICACION.disposicionId" key="comun.disposicion.select" editable="yes"/></td>
			</tr>
			<tr>
				<td colspan="2" ><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_producto" /></b></span></h3></td>
			</tr>
			<tr id="trDjPartidaAcuerdo" style="display:none" >
				<th><jlis:label key="co.label.dj.${idAcuerdo}.naladisa" /></th>
				<td>
					<jlis:value name="CALIFICACION.partidaSegunAcuerdo" size="10" maxLength="10" type="text" onBlur="validarNaladisa(this)" />
					<%-- span class="requiredValueClass">(*)</span--%>&nbsp;
					<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MERCANCIA.PARTIDA_SEGUN_ACUERDO" />
				</td>
			</tr>
			<tr>
			<th><jlis:label key="co.label.calificacion.ver.productos" /></th>
			<td><h5><a class="labelClass" style="color:red; text-decoration:underline;" href="javascript:verProducto();">Ver</a></h5>
			</td>
			</tr>
			<tr id="trPorcentajeSegunCriterio" style="display:none" >
				<th><jlis:label key="co.label.dj.prctj_segun_criterio" /></th>
				<td>
					<jlis:value name="CALIFICACION.porcentajeSegunCriterio" size="22" maxLength="22" editable="no" onBlur="validarDoublePositivoEntDec(this,false,12,6)" type="text" style="text-align:right"  pattern="0.000000"/>
					<span id="spanPorcentajeSegunCriterio" style="display:none" class="requiredValueClass">(*)</span>
					<co:mostrarAyuda etiqueta="MCT001.${idAcuerdo}.DJ.MATERIAL.PORCENTAJE_VCR" />
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr class="requiredValueClass" >
				<td colspan="2" ><I><jlis:label key="co.label.dj.general.titulo_pie_pagina" /></I></td>
			</tr>
			<tr class="requiredValueClass" >
				<td colspan="2" ><I><jlis:label key="co.label.dj.general.pie_pagina" /></I></td>
			</tr>
		</table>
