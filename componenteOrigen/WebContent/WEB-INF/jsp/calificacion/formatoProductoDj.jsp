<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Producto DJ</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
	<script>
	$(document).ready(function() {
		tituloPopUp("Datos del Producto Declarados en la DJ");
	 });

    function cerrarPopUpWindow(){
    	window.parent.hidePopWin(false);
        }
		
	</script>
	
    <body id="contModal">
    	<input type="hidden" name="method" />
        <input type="hidden" name="button">
    	<jlis:button id="cerrarPopUpButton" name="cerrarPopUpWindow()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana" />

		<table class="form">
			<tr>
			<th><jlis:label key="co.label.calificacion.ver.productos.descripcion" /></th>
			<td>
			<jlis:textArea name="PRODUCTO.denominacion" rows="4" cols="42" editable="no"/>
			</td>
			</tr>
			<tr>
			<th><jlis:label key="co.label.calificacion.ver.productos.caracteristica" /></th>
			<td>
			<jlis:textArea name="PRODUCTO.caracteristica" rows="4" cols="42" editable="no"/>
			</td>
			</tr>
			<tr>
			<th><jlis:label key="co.label.calificacion.ver.productos.partida" /></th>
			<td>
			<jlis:textArea name="PRODUCTO.denominacionArancelaria" rows="4" cols="42" editable="no"/>
			</td>
			</tr>
			<tr>
			<th><jlis:label key="co.label.calificacion.ver.productos.unidad" /></th>
			<td>
			<jlis:value name="PRODUCTO.unidadMedida" size="30" maxLength="30" type="text" editable="no"/>
			</td>
			</tr>
		</table>
    </body>
</html>