<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<title>Sistema COMPONENTE ORIGEN - Certificado Origen</title>
<meta http-equiv="Pragma" content="no-cache" />
</head>
<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>

<jsp:include page="/WEB-INF/jsp/funciones.jsp" />

<script language="JavaScript">

		$(document).ready(function() {
		    initializetabcontent("maintab");

    	    if ($("#bloqueado").val() == "S") {
    	    	bloquearControles();
    	    }
    	    if($("#idAcuerdo").val()!=""){
    	    	$('#valores').show();
        	 }

    	    if($('#MCT004\\.aceptacion').val()=="S"){
    			$('#aceptacion').val('S');
    			$('#aceptacion').attr('checked', true);

       		}else{
       			$('#aceptacion').val('N');
       			$('#aceptacion').attr('checked', false);
           	}

    		if ($("#orden").val() != '') {
    			$("#lnkCOOriginal").attr("style", "display:");
    		}

    		$("#lnkCOOriginal").mouseover(function () {
    			$(this).css('cursor', 'hand');
    			$("#lnkCOOriginal").css('color', 'black');
    			$("#lnkCOOriginal").css('text-decoration', 'underline');
    		});

    		$("#lnkCOOriginal").mouseout(function () {
    			$(this).css('cursor', 'default');
    			$("#lnkCOOriginal").css('color', '#3A6EA5');
    			$("#lnkCOOriginal").css('text-decoration', '');
    		});
    		
            $('#CERTIFICADO_ORIGEN\\.direccionAdicional').attr('readonly',true);
            $('#CERTIFICADO_ORIGEN.direccionAdicional').removeClass("inputTextClass").addClass("readonlyInputTextClass");        
            $('#listaDireccionesAdicionales').attr('readonly',true);
            $('#listaDireccionesAdicionales').removeClass("inputTextClass").addClass("readonlyInputTextClass");
		});

	    function buscarCertificado() {
	        var f = document.formulario;
	        showPopWin(contextName+"/origen.htm?method=cargarBusquedaCO&formato="+f.formato.value, 800, 500, null);
	        f.button.value = "cancelarButton";
	    }

	    function cargarInformacion(){
	    	var f = document.formulario;
	    	$('#valores').show();
		}

		// Valida la informaci�n general del formato (referida al producto)
		function validarCampos() {

			var mensaje="Debe ingresar o seleccionar : ";
		    var ok = true;

		    /*if ( document.getElementById("MCT004.sustentoAdicional").value == "" ) {
		        mensaje +="\n -Sustento Adicional";
		        changeStyleClass("co.label.certificado.sustentoAdicional", "errorValueClass");
		    } else changeStyleClass("co.label.certificado.sustentoAdicional", "labelClass");*/

		    if ( document.getElementById("MCT004.aceptacion").value == "" || document.getElementById("MCT004.aceptacion").value =="N" ) {
		        mensaje +="\n -Acepto";
		        changeStyleClass("co.label.certificado.acepto", "errorValueClass");
		    } else changeStyleClass("co.label.certificado.acepto", "labelClass");

		    if ( mensaje != "Debe ingresar o seleccionar : " ) {
		        ok = false;
		        alert (mensaje);
		    }
		    return ok;
		}

		// Graba los datos de la anulacion certificado de origen
		function grabarDetalle() {
			var f = document.formulario;
			if ( validarCampos() ) {
				f.action = contextName+"/origen.htm";
				f.method.value="actualizaAnulacionCertificadoOrigen";
				f.button.value="GrabarDetalleButton";
			} else {
	            f.button.value="cancelarButton";
			}
		}

    	function aceptarSolicitud(){
    		if($('#aceptacion').is(':checked')){
    			$('#MCT004\\.aceptacion').val('S');
        		}else{
        			$('#MCT004\\.aceptacion').val('N');
            		}

        	}

	    function actualizar() {
	    	if ( document.getElementById('buscarButton') == undefined ) {
	    		var f = document.formulario;
	            f.action = contextName+"/origen.htm";
	            f.method.value="cargarInformacionOrden";
	            f.button.value="actualizarButton";
	            f.target = "_self";
	            f.submit();
	    	} else {
	    		window.hidePopWin(false);
	    		return false;
	    	}
	    }

		function verCertificadoOriginal(){
			var f = document.frmCertOriginal;

			f.orden.value = '${ordenIdOrigen}';
			f.mto.value = '${mtoOrigen}';
			f.formato.value = '${formatoOrigen}';

			f.action = contextName+"/origen.htm";
			f.method.value = "cargarInformacionOrden";
            f.submit();
		}

    </script>
	<body>
		<div id="body"><jsp:include page="/WEB-INF/jsp/header.jsp" /> <!-- CONTENT -->
		<div id="contp">
		<div id="cont"><jlis:modalWindow onClose="actualizar()" />
		<form name="frmCertOriginal" method="post" onSubmit="return validarSubmit();">
			<jlis:value type="hidden" name="method" />
			<jlis:value type="hidden" name="button" />
			<jlis:value type="hidden" name="orden" />
			<jlis:value type="hidden" name="mto" />
			<jlis:value type="hidden" name="formato" />
		</form>
		<form name="formulario" method="post" onSubmit="return validarSubmit();">
			<jlis:value type="hidden" name="method" />
			<jlis:value type="hidden" name="button" />
			<jlis:value type="hidden" name="drIdOrigen" />
			<jlis:value type="hidden" name="sdrOrigen" />
			<jlis:value type="hidden" name="MCT004.drIdOrigen" />
			<jlis:value type="hidden" name="MCT004.sdrOrigen" />
			<jlis:value type="hidden" name="MCT004.coId" />
			<jlis:value type="hidden" name="MCT004.aceptacion" />
			<jlis:value type="hidden" name="numeroSolicitud" />
            
			<co:validacionBotonesFormato formato="MCT004" />
            
			<jsp:include page="/WEB-INF/jsp/informacionGeneral.jsp" />
            
			<c:if test="${empty idFormatoEntidad}">
			 <table class="form">
	                <tr>
	                    <td colspan="4" ><jlis:button code="CO.USUARIO.OPERACION" id="buscarButton" name="buscarCertificado()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar Certificado Original" alt="Pulse aqu� para buscar el Certificado Original" /></td>
	                </tr>
	        </table>
	         </c:if>
			<div id="valores" style="padding: 10px; border: 1px solid black; display:none">
	         <table class="form">
	                <tr>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.drEntidad.anulacion" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="drEntidad" editable="no" type="text" size="50"/></td>
	                    <td>&nbsp;</td>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.pais_acuerdo" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombrePais" editable="no" type="text" size="50"/></td>
	                </tr>
	                <tr>
                        <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.tipoCertificado" />&nbsp;&nbsp;</th>
                        <td><jlis:value name="tipoCertificado" editable="no" type="text" size="50"/></td>
                        <td>&nbsp;</td>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.acuerdo" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombreAcuerdo" editable="no" type="text" size="50"/></td>
	                </tr>
	                <tr>
                        <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.drOrigen.anulacion" />&nbsp;&nbsp;</th>
                        <td><jlis:value name="drOrigen" editable="no" type="text" size="50"/></td>
                        <td>&nbsp;</td>
	                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.entidad_certificadora" />&nbsp;&nbsp;</th>
	                    <td><jlis:value name="nombreEntidad" editable="no" type="text" size="50"/></td>
	                </tr>
                    <tr>
                        <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.fechaGeneracion.anulacion" />&nbsp;&nbsp;</th>
                        <td><jlis:date form="formulario" name="fechaGeneracion" editable="no"  size="20" pattern="dd/MM/yyyy"/></td>
                        <td>&nbsp;</td>
                    </tr>
	                <tr>
	                    <th colspan="4"><a id="lnkCOOriginal" style="display:none;" onClick="javascript:verCertificadoOriginal()"><jlis:label key="co.label.drAbrirOrigen" /></a></th>
	                </tr>
	                <tr>
	                    <td colspan="2" >&nbsp;</td>
	                </tr>
	            </table>
	            </div>


			<div class="block btabs btable"><div class="blocka"><div class="blockb">
			<ul id="maintab" class="tabs">
				<li class="selected"><a href="#" rel="tabGeneral"><span>Datos del Solicitante </span></a></li>
				<c:if test="${!empty idFormatoEntidad}">
                    <c:if test="${sessionScope.USUARIO.roles['CO.SUNAT.ESPECIALISTA'] != 'CO.SUNAT.ESPECIALISTA'}">
						<li><a href="#" rel="tabCertificado"><span>Motivaci�n</span></a></li>
						<%--<c:if test="${numAdjuntos > 0}">  --%>
							<li><a href="#" rel="tabAdjuntos"><span>Requisitos Adjuntos</span></a></li>
						<%--</c:if> --%>
					</c:if>
                    <c:if test="${!empty suce && empty mtoSUCE && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.EVALUADOR' && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.SUPERVISOR'}">
	                    <%-- <li><a href="#" rel="tabSuce" ><span>Subsanaci�n SUCE</span></a></li> --%>
						<li><a href="#" rel="tabDocResolutivos"><span>Docs. Resolutivos</span></a></li>
					</c:if>
				</c:if>
			</ul>
			<div id="tabGeneral" class="tabcontent">
				<jsp:include page="/WEB-INF/jsp/solicitante.jsp" />
			</div>
			<c:if test="${!empty idFormatoEntidad}">
                <c:if test="${sessionScope.USUARIO.roles['CO.SUNAT.ESPECIALISTA'] != 'CO.SUNAT.ESPECIALISTA'}">
				<div id="tabCertificado" class="tabcontent">
				  	<table class="form">
						<tr>
							<td colspan="2"><jlis:button code="CO.USUARIO.OPERACION" id="GrabarDetalleButton" name="grabarDetalle()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Grabar" alt="Pulse aqu� para actualizar datos del producto" /></td>
						</tr>
						<tr><td colspan="2"><hr/></td></tr>
						<tr>
						<td colspan="2">
						<jlis:textArea rows="2" editable="no" name="descAnulacion" style="width:92%" />
						</td>
						</tr>
						<tr><th><jlis:label key="co.label.certificado.acepto" /></th>
							<td ><jlis:value type="checkbox" name="aceptacion" checkValue="S" onClick="aceptarSolicitud()" /><span class="requiredValueClass">(*)</span></td>
						</tr>
						<tr>
							<th><jlis:label key="co.label.certificado.sustentoAdicional" /></th>
							<td ><jlis:textArea name="MCT004.sustentoAdicional" rows="2" cols="90" onKeyUp="valida_longitud(this, 500);" onChange="valida_longitud(this, 500);"/></td>
						</tr>
					</table>
				</div>

				<div id="tabAdjuntos" class="tabcontent">
					<jsp:include page="/WEB-INF/jsp/adjuntosRequeridos.jsp" />
				</div>
				</c:if>
                <c:if test="${!empty suce && empty mtoSUCE && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.EVALUADOR' && sessionScope.USUARIO.rolActivo != 'CO.ENTIDAD.SUPERVISOR'}">
                	<%-- <div id="tabSuce" class="tabcontent"><jsp:include page="/WEB-INF/jsp/subsanacionesSuce.jsp" /></div> --%>
					<div id="tabDocResolutivos" class="tabcontent"><jsp:include page="/WEB-INF/jsp/docResolSuce.jsp" /></div>
				</c:if>

			</c:if>
			</div>
			</div>
			</div>
		</form>
		</div>
		</div>
		<jsp:include page="/WEB-INF/jsp/footer.jsp" /></div>
	</body>
</html>