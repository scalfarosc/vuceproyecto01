<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
	<title>VUCE - Autenticación</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-store" />
	<meta http-equiv="expires" content="-1" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	<!-- modified  -->
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resource/css/login_contingencia.css">
	<style>
		/* BODY */
		/* Seccion Main Body */
		.main-body { align-items: center; color: #726d6d; display: flex; flex-direction: column; }
		.main-body > .wrapper { margin: auto; }
		.main-body .titulo { letter-spacing: -0.68px; }
		.main-body .titulo > h1 { color: #1a1818; font: 34px/40px robotoBold; margin: 0; }
		.main-body .titulo > p { font: 16px/19px robotoRegular; line-height: 1.4; margin: 0 15px; max-width: 650px; padding-top: 4px; }
		.main-body .btn { border: 1px solid transparent; cursor: pointer; font-size: 14px; text-align: center; }

		/* Seccion mensaje contingencia */
		.main-body .formulario { margin: 30px auto 0; max-width: 350px; }
		.main-body .formulario > form > .btn { background-color: #0272bd; border-radius: 5px; color: white; font: 15px/48px robotoBold; padding: 0 55px; }

		@media all and (max-width: 958px) {
			section.main-body { padding: 80px 0; }
			.main-body .titulo > p, footer > .wrapper > small { align-items: stretch; }
		}
		@media all and (max-width: 800px) {
			.main-body { align-items: stretch; }
			.main-body > .wrapper { padding: 0 20px; }
		}
		@media screen and (max-width: 475px) {
			section.main-body { padding: 50px 0; }
			.main-body > .wrapper > .titulo { margin-bottom: 30px; }
			.main-body > .wrapper > .titulo > h1 { font: 28px/33px robotoBold; }
			.main-body > .wrapper > .titulo > p { font: 15px/18px robotoRegular; margin: 0; }

			.main-body .formulario { margin: 0 auto; max-width: 315px; }
			.main-body .formulario > form { margin: 0; max-width: 315px; padding: 0; }
			.main-body .formulario > form > .btn { padding: 0; }
			.main-body .formulario > form > input { max-width: 253px; width: 100%; }
		}
	</style>
	<script type="text/javascript">
		function irWeb() {
			window.location.href = "https://www.vuce.gob.pe";
		}
	</script>

</head>
<body>
<header>
	<div class="wrapper">
		<nav>
			<a href="http://beta.vuce.gob.pe/Paginas/Inicio.aspx" target="_blank">
				<img src="<%=request.getContextPath()%>/imagenes/svg/logo-vuce.svg" alt="VUCE" />
			</a>
			<a href="https://www.gob.pe/mincetur" target="_blank">
				<img src="<%=request.getContextPath()%>/imagenes/svg/logo-mincetur.svg" alt="Ministerio de Comercio Exterior y Turismo" width="235" height="46" />
			</a>
		</nav>
		<section>
			<a href="http://beta.vuce.gob.pe/Paginas/Preguntas-frecuentes.aspx" target="_blank">
				<span>Preguntas Frecuentes</span>
				<img src="<%=request.getContextPath()%>/imagenes/svg/icon-preguntas-frecuentes.svg" />
			</a>
			<div>
				<a href="mailto:vuceayuda@vuce.gob.pe">vuceayuda@vuce.gob.pe</a>
				<a href="tel:012071510">
					<span>Mesa de ayuda</span>
					<span>(01)207-1510</span>
					<img src="<%=request.getContextPath()%>/imagenes/svg/icon-mesa-de-ayuda.svg" />
				</a>
			</div>
		</section>
	</div>
</header>
<section class="main-body">
	<div class="wrapper">
		<div class="titulo">
			<h1>Estimado usuario</h1>
			<c:choose>
				<c:when test="${requestScope.textoCorreoEnviado!=null}">
					<p>${requestScope.textoCorreoEnviado}</p>
				</c:when>
				<c:otherwise>
					<p>VUCE te ha enviado un correo electr&oacute;nico con el enlace e instrucciones para ingresar al sistema. El tiempo de expiraci&oacute;n del enlace es de <strong>10 MINUTOS</strong>.</p>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="formulario">
			<form>
				<input class="btn" type="button" onclick="irWeb()" value="Ir a la p&aacute;gina de Inicio">
			</form>
		</div>
	</div>
</section>
<footer>
	<div class="wrapper">
		<div>
			<a href="http://beta.vuce.gob.pe/Paginas/Aviso-de-privacidad.aspx" target="_blank">Pol&iacute;ticas de privacidad</a>
			<a href="http://beta.vuce.gob.pe/Paginas/T%C3%A9rminos-y-Condiciones.aspx" target="_blank">Condiciones de uso</a>
		</div>
		<picture>
			<img src="<%=request.getContextPath()%>/imagenes/svg/iso-9001.svg" alt="ISO 9001" />
			<a href="https://www.gob.pe/mincetur" target="_blank">
				<img src="<%=request.getContextPath()%>/imagenes/svg/logo-mincetur.svg" alt="Ministerio de Comercio Exterior y Turismo" />
			</a>
		</picture><br />
		<hr />
		<small>
			&copy; Copyright 2020 - MINCETUR Todos los derechos reservados.
			Ante cualquier duda o problema contacte a
			<a href="tel:012071510">
				<span>Mesa de ayuda: (01)207-1510</span>
			</a>
			<a href="mailto:vuceayuda@vuce.gob.pe">vuceayuda@vuce.gob.pe</a>
		</small>
	</div>
</footer>
</body>
</html>
