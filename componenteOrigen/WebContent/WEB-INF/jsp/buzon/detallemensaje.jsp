<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema COMPONENTE ORIGEN</title>
	</head>

	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript">
        var contador = 0;

        function validarSubmit() {
            var f = document.formulario;
            if (f.button.value=="cerrarPopUpButton") {
            	return validaCerraPopUp();
            }
            return true;
	    }

	    function descargarAdjunto(keyValues, keyValuesField) {
	        var f = document.formulario;
	        var adjuntoId = document.getElementById(keyValues).value;
	        f.action = contextName+"/buzon.htm?method=descargarAdjunto&idAdjunto="+adjuntoId;
	        f.submit();
        }

        function verSuceAnt(frmlow, ordenId, mto) {
            var f = document.formulario;
            f.action = contextName+"/"+frmlow+".htm?opcion=S";
            f.method.value = "cargarInformacionOrden";
            f.mto.value = mto;
            f.orden.value = ordenId;
            f.target = window.parent.name;
            f.submit();
        }

        function verSuce() {

            var f = document.formulario;
            f.action = contextName+"/origen.htm";
            f.method.value = "cargarInformacionOrden";
            f.target = "_parent";
            f.submit();

            /*f.action = contextName+"/"+frmlow+".htm?opcion=S";
            f.method.value = "cargarInformacionOrden";
            f.mto.value = mto;
            f.orden.value = ordenId;
            f.target = window.parent.name;
            f.submit();*/
        }

        function imprimirCPB(idMensaje) {
            var f = document.formulario;
            f.action = contextName+"/buzon.htm";
            f.method.value = "imprimirCPB";
            f.button.value = "imprimirCPBButton";
            f.target = "_blank";
            f.submit();
        }

        function verConsultaTecnica(idCT, para, numeroCT) {
            var f = document.formulario;
            f.action = contextName+"/ct.htm?method=mostrarDetalleConsultaTecnica&idCT="+idCT+"&para="+para+"&numeroCT="+numeroCT;
            f.target = window.parent.name;
            f.submit();
        }

    </script>
	<body id="contModal">

    <form name="formulario" method="post" onsubmit="return validarSubmit();">
         <input type="hidden" name="method" />
         <input type="hidden" name="button">
         <jlis:value type="hidden" name="idMensaje" />
         <jlis:value name="mto" type="hidden" />
         <jlis:value name="orden" type="hidden" />
         <jlis:value name="formato" type="hidden" />
         <table class="form">
            <tr>
             <th colspan="2"><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana" /></th>
            </tr>
            <tr>
       	     <th><jlis:label key="co.label.buzon.de"/></th>
       		 <td><jlis:value name="MENSAJE.deTitulo" size="80" editable="no"/></td>
            </tr>
            <tr class="impar">
                 <th><jlis:label key="co.label.buzon.asunto"/></th>
                 <td><jlis:value name="MENSAJE.asunto" size="80" editable="no"/></td>
            </tr>
            <!-- <tr>
                 <th><jlis:label key="co.label.buzon.estado"/></th>
                 <td colspan="4"><jlis:value name="MENSAJE.estado" size="90"/></td>
            </tr> -->
             <tr>
                 <th><jlis:label key="co.label.buzon.fecha"/></th>
                 <td><jlis:value name="MENSAJE.fechaRegistro" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" size="80" editable="no"/></td>
            </tr>
            <tr class="impar">
                 <th colspan="2"><jlis:label key="co.label.buzon.mensaje"/></th>
            </tr>
            <tr>
               <th colspan="2"><jlis:textArea name="MENSAJE.mensaje" cols="115" rows="8" editable="no"/></th>
       	</tr>
       	<c:if test="${!empty link}" >
            <tr>
                 <td colspan="2" style="text-align:center"><a target="${target}" href="${link}" >${nombreLink}</a></td>
            </tr>
        </c:if>
        <c:if test="${!empty linkImpresionCPB}" >
            <tr>
                 <td colspan="2" style="text-align:center"><h5><a target="${targetImpresionCPB}" href="${linkImpresionCPB}" >${nombreLinkImpresionCPB}</a></h5></td>
            </tr>
        </c:if>
        </table>
        <c:if test="${mostrarNotificacionesMensaje=='S'}" >
        <jlis:table keyValueColumns="NOTIFICACION_ID" name="adjuntosMensaje" source="tNotificacionesMensaje" scope="request" pageSize="15" cellPadding="2" width="100%">
               <jlis:tr>
               	<jlis:td name="DESCRIPCI�N NOTIFICACION" nowrap="yes" />
               </jlis:tr>
               <jlis:columnStyle column="1" columnName="DESCRIPCION"/>
        </jlis:table>
        </c:if>
        <c:if test="${mostrarAdjuntosMensaje=='S'}" >
       	<jlis:table keyValueColumns="ADJUNTO_ID" name="adjuntosMensaje" source="tAdjuntosMensaje" scope="request" pageSize="15" cellPadding="2" width="100%">
               <jlis:tr>
               	<jlis:td name="ADJUNTO ID" nowrap="yes" />
               	<jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
               	<jlis:td name="ADJUNTO TIPO" nowrap="yes" />
               	<jlis:td name="ARCHIVO" nowrap="yes" />
                   <jlis:td name="DESCRIPCI�N" nowrap="yes" />
                  </jlis:tr>
               <jlis:columnStyle column="1" columnName="ADJUNTO_ID" hide="yes" />
               <jlis:columnStyle column="2" columnName="NOMBRE_ARCHIVO" editable="yes"/>
               <jlis:columnStyle column="3" columnName="ADJUNTO_TIPO" align="center" hide="yes"/>
               <jlis:columnStyle column="4" columnName="ARCHIVO" align="center" hide="yes"/>
               <jlis:columnStyle column="5" columnName="DESCRIPCION"  align="center" hide="yes"/>
              <jlis:tableButton column="2" type="link" onClick="descargarAdjunto" />
           </jlis:table>
        </c:if>
    </form>
    <script>
        window.parent.document.getElementById("popupTitle").innerHTML = "Detalle Mensaje";
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
    </script>
</body>
</html>