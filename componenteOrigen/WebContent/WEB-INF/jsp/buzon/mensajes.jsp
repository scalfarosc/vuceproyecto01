<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Principal</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <c:set var="activeBuzon" value="active" scope="request" />
    <script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
    <script language="JavaScript">

		function verDetalle(keyValues, keyValuesField) {
	        var f = document.formulario;
	        var mensajeId = document.getElementById(keyValues.split("|")[0]).value;
	        var destinatario = document.getElementById(keyValues.split("|")[1]).value;
	        var suceId = "";
	        if ( keyValues.split("|")[2] != undefined ) {
	        	suceId = document.getElementById(keyValues.split("|")[2]).value;
	        }
	        var orden = "";
	        if ( keyValues.split("|")[3] != undefined ) {
	        	orden = document.getElementById(keyValues.split("|")[3]).value;
	        }
	        var mto = "";
	        if ( keyValues.split("|")[4] != undefined ) {
	        	mto = document.getElementById(keyValues.split("|")[4]).value;
	        }
	        var formato = "";
	        if ( keyValues.split("|")[5] != undefined ) {
	        	formato = document.getElementById(keyValues.split("|")[5]).value;
	        }
	        showPopWin(contextName+"/buzon.htm?method=mostrarDetalleMensaje&idMensaje="+mensajeId +"&destinatario="+destinatario+"&suceId="+suceId+"&orden="+orden+"&mto="+mto+"&formato="+formato, 710, 500, null);
        }

        function mostrarMensajes(){
            var usuarioId = (document.getElementById("combousuario") ? $('#combousuario').val() : "");
            var f = document.formulario;
            f.action = contextName+"/buzon.htm";
            f.method.value = "listarMensajes";
            f.usuarioId.value = usuarioId;
            f.submit();
        }

        function actualizar() {
            var f = document.formulario;
            f.action = contextName+"/buzon.htm";
            f.method.value = "listarMensajes";
            f.submit();
        }

        function inactivarMensajes() {
        	var f = document.formulario;
			var cont = 0;
			var checkBoxesName = "seleccione";
			for(i=0 ; i<f.elements[checkBoxesName].length; i++){
				if(f.elements[checkBoxesName][i].checked == true){
					cont++;
				}
			}
			if (cont == 0) {
				alert('Debe seleccionar al menos un mensaje para poder Eliminar');
				return;
			} else {
	            f.action = contextName+"/buzon.htm";
	            f.method.value="inactivarMensajes";
	            f.submit();
			}
		}

        function alerta(){
        	alert('Hola');
        }

    </script>
    <body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
	<div id="pageTitle">
		<h1><strong><jlis:label  labelClass="pageTitle"  key="co.title.buzon" /></strong></h1>
	</div>
    	<jlis:modalWindow onClose="actualizar();"/>
	        <form name="formulario" method="post" onSubmit="return false;">
	            <input type="hidden" name="method" />
	            <input type="hidden" name="opcion" />
                <input type="hidden" name="usuarioId" />
		        <jlis:messageArea width="40%" />
	            <div style="text-align:left">
                    <jlis:label key="co.title.buzon.filtro.asunto" style="font-weight:bold"/>&nbsp;&nbsp;
                    <jlis:value name="filtroCadena" size="50" type="text"/>&nbsp;&nbsp;
                    <jlis:label key="co.label.buzon.filtro.fechaDesde" style="font-weight:bold"/>&nbsp;&nbsp;
                    <jlis:date form="formulario" name="fechaDesde" />&nbsp;&nbsp;
                    <jlis:label key="co.label.buzon.filtro.fechaHasta" style="font-weight:bold"/>&nbsp;&nbsp;
                    <jlis:date form="formulario" name="fechaHasta" />&nbsp;&nbsp;
	            </div>
	            <div style="text-align:left">
                    <jlis:label key="co.label.filtro.entidad" style="font-weight: bold;"/>&nbsp;&nbsp;
                    <jlis:selectProperty key="comun.entidad.select" name="filtroEntidad" />&nbsp;&nbsp;
                    <jlis:button id="buscarButton" name="mostrarMensajes()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Pulse aqu� para buscar" />
	            </div>
	            <div id="dvUno">
		            <jlis:table keyValueColumns="MENSAJE_ID,DESTINATARIO,SUCE_ID,ORDEN_ID,MTO,FORMATO" name="mensajesOrientacion" key="${keyGrilla}" filter="${filter}"
		                pageSize="15" validator="pe.gob.mincetur.vuce.co.web.util.BuzonRowValidator" method="validarEliminarMensaje" >
			            <jlis:tr>
			            	<jlis:td name="BUZON_ID" nowrap="yes" />
			            	<jlis:td name="MENSAJE_ID" nowrap="yes"  />
	                        <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
			            	<jlis:td name="DE" nowrap="yes" width="15%"/>
			            	<jlis:td name="ASUNTO" nowrap="yes" width="65%"/>
	                        </c:if>
	                        <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
	                        <jlis:td name="DE" nowrap="yes" width="15%"/>
	                        <jlis:td name="ASUNTO" nowrap="yes" width="65%"/>
	                        </c:if>
			                <jlis:td name="USUARIO2" nowrap="yes" />
		                    <jlis:td name="FECHA ENVIO" nowrap="yes" width="10%"/>
		                    <jlis:td name="&nbsp;" nowrap="yes" width="5%"/>
		                    <jlis:td name="VER" nowrap="yes" width="5%"/>
		                    <jlis:td name="DESTINATARIO" nowrap="yes" />
		                    <!-- jlis:td name="ELIMINAR" width="1%" style="checkbox=formulario,seleccione,on-off"  /> -->
		                </jlis:tr>
			            <jlis:columnStyle column="1" columnName="BUZON_ID" hide="yes" />
			            <jlis:columnStyle column="2" columnName="MENSAJE_ID" hide="yes" />
			            <jlis:columnStyle column="3" columnName="DE" />
			            <jlis:columnStyle column="4" columnName="ASUNTO" editable="yes"/>
			            <jlis:columnStyle column="5" columnName="USUARIO2"  align="center" hide="yes"  />
			            <jlis:columnStyle column="6" columnName="FECHA_REGISTRO"  align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss"/>
			            <jlis:columnStyle column="7" columnName="ESTADO"  align="center" validator="pe.gob.mincetur.vuce.co.web.util.BuzonCellValidator" />
			            <jlis:columnStyle column="8" columnName="VER" editable="yes" align="center" validator="pe.gob.mincetur.vuce.co.web.util.BuzonLinkCellValidator" hide="yes" />
			            <jlis:columnStyle column="9" columnName="DESTINATARIO" hide="yes" />
			            <jlis:columnStyle column="10" columnName="INACTIVAR" editable="yes" align="center" hide="yes" />
			            <jlis:columnStyle column="11" columnName="ESTALEIDO" hide="yes" />
			            <jlis:columnStyle column="12" columnName="SUCE_ID" hide="yes" />
			            <jlis:columnStyle column="13" columnName="ORDEN_ID" hide="yes" />
			            <jlis:columnStyle column="14" columnName="MTO" hide="yes" />
			            <jlis:columnStyle column="15" columnName="FORMATO" hide="yes" />
			            <jlis:tableButton column="10" type="checkbox"  name="seleccione" />
						<jlis:tableButton column="4" type="link" onClick="verDetalle" />
	                    <jlis:tableButton column="7" type="imageButton" />
	                    <jlis:tableButton column="8" type="imageButton" image="/co/imagenes/ver.gif" alt="Ver la solicitud" />
			        </jlis:table>
		        </div>
		    </form>
        </div></div>
        <jsp:include page="/WEB-INF/jsp/footer.jsp" /></div>
    </body>
</html>