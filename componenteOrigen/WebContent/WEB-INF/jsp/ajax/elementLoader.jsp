<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page contentType="text/xml;charset=ISO-8859-1" %>
<%@page import="org.jlis.core.list.*"%>
<%@page import="org.jlis.core.util.*"%>
<%@page import="org.jlis.web.util.*"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="java.util.*"%>
<DATA_ELEMENT>
<%
    HashUtil datos = RequestUtil.getParameter(request);
    HashUtil result = null;
    try {
        Object obj = Class.forName(Util.getAjaxPropertyKey(datos.getString("service"))).newInstance();
        Class parameterTypes[] = new Class[1];
        parameterTypes[0] = HashUtil.class;
        Object args[] = new Object[1];
        args[0] = datos;
        try {
            result = (HashUtil)(obj.getClass().getDeclaredMethod(datos.getString("serviceMethod"), parameterTypes).invoke(obj, args));
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }
    } catch (ClassNotFoundException ex) {
        ex.printStackTrace();
    } catch (IllegalAccessException ex) {
        ex.printStackTrace();
    } catch (InstantiationException ex) {
        ex.printStackTrace();
    }
    if (result!=null) {
        Enumeration en = result.keys();
        while (en.hasMoreElements()) {
            String key = (String)en.nextElement();
            Object value = result.get(key);
            if (value!=null) {
                //value = Util.replaceHtmlSpecialCharacters(value.toString());
                value = (value.toString().indexOf("&")!=-1) ? Util.replace(value.toString(),"&","&amp;") : value;
                value = (value.toString().indexOf("\"")!=-1) ? Util.replace(value.toString(),"\"","'") : value;

%>
    <FIELD name="<%=key%>" value="<%=value%>" />
<%
            }
        }
    }
%>
</DATA_ELEMENT>