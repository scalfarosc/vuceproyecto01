<html>
	<%@page import="org.jlis.core.util.*"%>
	<%@page import="org.jlis.core.list.*"%>
    <%@page import="java.lang.reflect.*"%>
    <%@page import="java.util.*"%>
<%
	String contextName = request.getContextPath();
    HashUtil datos = (HashUtil)request.getAttribute(Constantes.PARAMETRO);
    String div = (String)datos.remove("div");
    String url = (String)datos.remove("url");
    String func = (String)datos.remove("func");
    if (func==null || func.equals("")) func = "\"\"";
    if (datos.size()>0) {
        url += "?";
        Enumeration en = datos.keys();
        while (en.hasMoreElements()) {
            String key = (String)en.nextElement();
            String value = datos.getString(key);
            url += key+"="+value+"&";
        }
    }
%>
	<script src="<%=contextName%>/resource/js/functions.js"></script>
    <script>
        loadPageAJX("<%=url%>", "<%=div%>", "", <%=func%>);
    </script>
</html>