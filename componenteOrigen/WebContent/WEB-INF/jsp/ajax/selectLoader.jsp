<?xml version="1.0" encoding="ISO-8859-1"?>
<%@page contentType="text/xml;charset=ISO-8859-1" %>
<%@page import="org.jlis.core.bean.*"%>
<%@page import="org.jlis.core.list.*"%>
<%@page import="org.jlis.core.util.*"%>
<%@page import="org.jlis.web.util.*"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="java.util.*"%>
<%
    HashUtil datos = RequestUtil.getParameter(request);
    OptionList result = null;
    try {
        Object obj = Class.forName(Util.getAjaxPropertyKey(datos.getString("service"))).newInstance();
        Class parameterTypes[] = new Class[1];
        parameterTypes[0] = HashUtil.class;
        Object args[] = new Object[1];
        args[0] = datos;
        try {
            result = (OptionList)(obj.getClass().getDeclaredMethod(datos.getString("serviceMethod"), parameterTypes).invoke(obj, args));
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }
    } catch (ClassNotFoundException ex) {
        ex.printStackTrace();
    } catch (IllegalAccessException ex) {
        ex.printStackTrace();
    } catch (InstantiationException ex) {
        ex.printStackTrace();
    }
    if (result!=null) {%>
<DATA_LIST>
<%      for (int i=0; i < result.size(); i++) {
            Option op = result.getOption(i);
            String codigo = op.getCodigo()!=null?op.getCodigo():"";
            codigo = (codigo.indexOf("&")!=-1) ? Util.replace(codigo,"&","&amp;") : codigo;
            codigo = (codigo.indexOf("\"")!=-1) ? Util.replace(codigo,"\"","'") : codigo;
            
            String descripcion = op.getDescripcion()!=null?op.getDescripcion():"";
            descripcion = (descripcion.indexOf("&")!=-1) ? Util.replace(descripcion,"&","&amp;") : descripcion;
            descripcion = (descripcion.indexOf("\"")!=-1) ? Util.replace(descripcion,"\"","'") : descripcion;
%>
    <OPTION name="<%=codigo%>" value="<%=descripcion%>" />
<%
        }%>
</DATA_LIST>
<%  } else {%>
    <NULL />
<%  }%>