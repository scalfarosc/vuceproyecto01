
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Sistema Componente Origen - Principal (Monitoreo)</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex,nofollow" />
	</head>
    
    <c:set var="activeInicio" value="active" scope="request" />
    
    <body >
    <div id="body">
    <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
	
    <form name="formulario" method="post" onSubmit="return validarSubmit();">
        <input type="hidden" name="opcion" />
        <input type="hidden" name="method" />
        
		<p class="bienvenido">
			<strong>${sessionScope.USUARIO.nombreCompleto}</strong>,
			bienvenido al sistema VUCE, desde aqu� podr�s realizar tus operaciones y tambi�n monitorearlas.
		</p>
		<c:if test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' || 
                      sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO'}">
		<div class="welcome"><div><div>
            <h3>xxx</h3>
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
            <p>yyy (<a href="javascript:listarOrdenes();">${cantidadBorradoresOrdenes}</a>)</p>
            <p>zzz (<a href="javascript:listarOrdenes();">${cantidadSuces}</a>)</p>
            </c:if>
		</div></div></div>
        </c:if>
        </form>
        
        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>