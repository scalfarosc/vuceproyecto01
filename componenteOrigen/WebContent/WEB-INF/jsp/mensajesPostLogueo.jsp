<%@page import="org.jlis.core.config.ApplicationConfig"%>
<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Sistema COMPONENTE ORIGEN - Principal</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex,nofollow" />
	<script>
        function cancelar(){
        	var cookieValue = getCookie("idp");
        	var url = '<%=ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLSOL") %>';
        	if (cookieValue == "extranet") {
        		url = '<%=ApplicationConfig.getApplicationConfig().getProperty("applicationLogoutURLEXT") %>';
            }
        	window.location.href = url;
        }

        function getCookie(cname) {
        	  var name = cname + "=";
        	  var decodedCookie = decodeURIComponent(document.cookie);
        	  var ca = decodedCookie.split(';');
        	  for(var i = 0; i <ca.length; i++) {
        	    var c = ca[i];
        	    while (c.charAt(0) == ' ') {
        	      c = c.substring(1);
        	    }
        	    if (c.indexOf(name) == 0) {
        	      return c.substring(name.length, c.length);
        	    }
        	  }
        	  return "";
        	}
    </script>
    </head>

    <body >
    <div id="body">
    <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
        <input type="hidden" name="opcion" />
        <input type="hidden" name="method" />
        
		<jlis:messageArea width="40%" />
		
        <table>
            <tr>
                <td>
                   <input name="cancelar" class="buttonSubmitClass" alt="Pulse aqu� para cancelar el ingreso" onclick="cancelar()" value="Salir" style="width: 50px;"/>
                </td>
            </tr>
        </table>
        
        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>