<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
        <title>Sistema COMPONENTE ORIGEN - Listado de Declaraciones Juradas</title>
        <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <c:set var="activeDJ" value="active" scope="request" />
	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>

    <script>
        
	    function validarSubmit() {
	        var f = document.formulario;
	        if (f.button.value=="buscarButton") {
	            if (f.numero.value!="" && getSelectedRadioValue(f.opcionFiltro)=="") {
	                alert("Debe seleccionar la opci�n Solicitud o SUCE");
	                return false;
	            }
	        }
		    if (f.button.value=="mostrarDJ") {
	        	return false;
	        }
	        return true;
	    }
        
	    function buscar() {
	        var f = document.formulario;
	        f.action = contextName+"/origen.htm";
	        f.method.value = "listarDeclaracionJurada";
	        f.target = "_self";
	        f.button.value = "buscarButton";
	    }
        
		function seleccionOpcion(obj) {
			var f = document.formulario;
			if (obj.value=="C") {
				f.denominacion.value = "";
				//f.suce.value = "";
				f.filtroSolicitud.value = "";
			} else if(obj.value=="D") {
				f.declaracion.value = "";
				//f.suce.value = "";
				f.filtroSolicitud.value = "";
			} else if (obj.value=="SC") {
				f.declaracion.value = "";
				//f.suce.value = "";
				f.denominacion.value = "";
			} else {
				f.denominacion.value = "";
				f.declaracion.value = "";
				f.filtroSolicitud.value = "";
			}
		}
        
		function seleccionDeclaracion() {
			var f = document.formulario;
			f.denominacion.value = "";
			//f.suce.value = "";
			f.filtroSolicitud.value = "";
			document.getElementById("opcionFiltro_C").checked = true;
		}
        
		function seleccionDenominacion() {
			var f = document.formulario;
			f.declaracion.value = "";
			//f.suce.value = "";
			f.filtroSolicitud.value = "";
			document.getElementById("opcionFiltro_D").checked = true;
		}

		function seleccionSuce() {
			var f = document.formulario;
			f.denominacion.value = "";
			f.declaracion.value = "";
			f.filtroSolicitud.value = "";
			document.getElementById("opcionFiltro_S").checked = true;
		}

		function seleccionSolicitud() {
			var f = document.formulario;
			f.denominacion.value = "";
			//f.suce.value = "";
			f.declaracion.value = "";
			document.getElementById("opcionFiltro_SC").checked = true;
		}

		function mostrarDJ(keyValues, keyValuesField) {
	       	var f = document.formulario;
	       	var orden = document.getElementById(keyValues.split("|")[2]).value;
	       	var mto = document.getElementById(keyValues.split("|")[3]).value;
	       	var idAcuerdo = document.getElementById(keyValues.split("|")[4]).value;
	       	var bloqueado = "S";
	       	var djId = document.getElementById(keyValues.split("|")[1]).value;
	       	var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
	       	var formato = document.getElementById(keyValues.split("|")[5]).value;
	        showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&calificacionUoId="+calificacionUoId+"&djId="+djId+
	        		"&idAcuerdo="+idAcuerdo+"&formato="+formato+"&ver=1&bloqueado="+bloqueado+"&esDJRegistrada=S", 800, 550, null);
			f.button.value = "mostrarDJ";
		}

    	// Editar la Declaraci�n Jurada por un productor
    	function calificarValidacion(keyValues, keyValuesField) {
           	var f = document.formulario;
           	/*var orden = f.orden.value;
           	var mto = f.mto.value;
           	var idAcuerdo = f.idAcuerdo.value;
            var formato = f.formato.value;*/
            var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
            var djId = document.getElementById(keyValues.split("|")[1]).value;
           	var idAcuerdo = document.getElementById(keyValues.split("|")[2]).value;
           	var idEntidad = document.getElementById(keyValues.split("|")[3]).value;
    	    //showPopWin(contextName+"/origen.htm?method=mostrarCalificarValidacionDJ&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&formato="+formato.toLowerCase(), 800, 550, null);
    	    showPopWin(contextName+"/origen.htm?method=mostrarCalificarValidacionDJ&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad, 200, 150, null);
    		f.button.value = "calificarValidacionButton";
    	}
        
    	// Editar la declaraci�n jurada por un productor
    	function editarDeclaracionJuradaPendienteValidacion(keyValues, keyValuesField) {
           	var f = document.formulario;
           	//var orden = f.orden.value;
           	//var mto = f.mto.value;
            //var formato = f.formato.value;
            var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
            var djId = document.getElementById(keyValues.split("|")[1]).value;
           	var idAcuerdo = document.getElementById(keyValues.split("|")[2]).value;
           	var idEntidad = document.getElementById(keyValues.split("|")[3]).value;
	       	var acuerdo = document.getElementById(keyValues.split("|")[4]).value;
	       	var entidad = document.getElementById(keyValues.split("|")[5]).value;
	       	var nombrePais = document.getElementById(keyValues.split("|")[6]).value;
	       	var orden = document.getElementById(keyValues.split("|")[9]).value;
	       	//var ordenId = document.getElementById(keyValues.split("|")[9]).value;
	       	var mto = document.getElementById(keyValues.split("|")[8]).value;
    	    //showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&formato="+formato.toLowerCase()+"&modoProductorValidador=S", 800, 550, null);
    	    showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad+"&modoProductorValidador=S&acuerdo="+acuerdo+"&entidad="+entidad+"&nombrePais="+nombrePais+"&orden="+orden+"&mto="+mto, 800, 550, null);
    		f.button.value = "editarDeclaracionJuradaPendienteValidacionButton";
    	}

    	// Editar la declaraci�n jurada por un productor
    	function verDJAprobada(keyValues, keyValuesField) {
           	var f = document.formulario;
           	//var orden = f.orden.value;
           	//var mto = f.mto.value;
            //var formato = f.formato.value;
            var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
            var djId = document.getElementById(keyValues.split("|")[1]).value;
           	var idAcuerdo = document.getElementById(keyValues.split("|")[2]).value;
           	var idEntidad = document.getElementById(keyValues.split("|")[3]).value;
           	var acuerdo = document.getElementById(keyValues.split("|")[4]).value;
           	var entidad = document.getElementById(keyValues.split("|")[5]).value;
	       	var nombrePais = document.getElementById(keyValues.split("|")[6]).value;
    	    //showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&formato="+formato.toLowerCase()+"&modoProductorValidador=S", 800, 550, null);
    	    showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad+"&modoProductorValidador=S&acuerdo="+acuerdo+"&entidad="+entidad+"&nombrePais="+nombrePais, 800, 550, null);
    		f.button.value = "editarDeclaracionJuradaPendienteValidacionButton";
    	}

    	// Ver la declaraci�n jurada aprobada por un productor
    	function verDJModoLectura(keyValues, keyValuesField) {
           	var f = document.formulario;
           	//var orden = f.orden.value;
           	//var mto = f.mto.value;
            //var formato = f.formato.value;
            var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
            var djId = document.getElementById(keyValues.split("|")[1]).value;
           	var idAcuerdo = document.getElementById(keyValues.split("|")[2]).value;
           	var idEntidad = document.getElementById(keyValues.split("|")[3]).value;
	       	var acuerdo = document.getElementById(keyValues.split("|")[4]).value;
	       	var entidad = document.getElementById(keyValues.split("|")[5]).value;
	       	var nombrePais = document.getElementById(keyValues.split("|")[6]).value;
	       	var orden = document.getElementById(keyValues.split("|")[9]).value;
	       	var mto = document.getElementById(keyValues.split("|")[8]).value;

    	    //showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&formato="+formato.toLowerCase()+"&modoProductorValidador=S", 800, 550, null);
    	    showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&djId="+djId+"&idAcuerdo="+idAcuerdo+"&idEntidad="+idEntidad+"&modoLectura=S&modoValidadorLectura=S&acuerdo="+acuerdo+"&entidad="+entidad+"&nombrePais="+nombrePais, 800, 550, null);
    		f.button.value = "editarDeclaracionJuradaPendienteValidacionButton";
    	}



    	function actualizar() {
	 		//alert('entre papa');
	 		buscar();
	        /*var f = document.formulario;
	        f.action = contextName+"/origen.htm";
	        f.method.value = "listarDeclaracionJurada";
	        f.button.value = "buscarButton";
	        f.target = window.parent.name;*/
    	}

		function mostrarDJ(keyValues, keyValuesField) {
	       	var f = document.formulario;
	       	var orden = document.getElementById(keyValues.split("|")[2]).value;
	       	var mto = document.getElementById(keyValues.split("|")[3]).value;
	       	var idAcuerdo = document.getElementById(keyValues.split("|")[4]).value;
	       	var bloqueado = "S";
	       	var djId = document.getElementById(keyValues.split("|")[1]).value;
	       	var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
	       	var formato = document.getElementById(keyValues.split("|")[5]).value;
	        showPopWin(contextName+"/origen.htm?method=cargarDeclaracionJuradaForm&orden="+orden+"&mto="+mto+"&calificacionUoId="+calificacionUoId+"&djId="+djId+
	        		"&idAcuerdo="+idAcuerdo+"&formato="+formato+"&ver=1&bloqueado="+bloqueado+"&esDJRegistrada=S", 800, 550, null);
			f.button.value = "mostrarDJ";
		}

		// Carga la p�gina de edici�n de Mercanc�as
		function mostrarCalificacion(keyValues, keyValuesField){
			var f = document.formulario;
			
			//alert(keyValues);
	       	var calificacionUoId = document.getElementById(keyValues.split("|")[0]).value;
	       	var djId = document.getElementById(keyValues.split("|")[1]).value;
	       	var orden = document.getElementById(keyValues.split("|")[2]).value;
	       	var mto = document.getElementById(keyValues.split("|")[3]).value;
	       	var idAcuerdo = document.getElementById(keyValues.split("|")[4]).value;
	       	// En realidad no existe la mercanc�a, pero para efectos de reutilizaci�n, deber�a ser como si
	       	// se cargara el formulario al inicio
	       	var secuencia = 0;
	       	var formato = document.getElementById(keyValues.split("|")[5]).value;
	       	var acuerdo = document.getElementById(keyValues.split("|")[6]).value;
	       	var entidad = document.getElementById(keyValues.split("|")[7]).value;
	       	var nombrePais = document.getElementById(keyValues.split("|")[8]).value;

	       	var bloqueado = "S";
	       	// TO DO ORIGEN: Revisar para qu� serv�a el estado de la DJ en este caso
			//var estadoDJ = document.getElementById(keyValues.split("|")[1]).value;
			var estadoDJ = '';
	       	// TO DO ORIGEN: Revisar para qu� serv�a este campo en este caso
			//var puedeTransmitir = f.puedeTransmitir.value;
			var puedeTransmitir = '';
			
			showPopWin(contextName+"/origen.htm?method=cargarCertificadoOrigenMercanciaForm&calificacionUoId=" + calificacionUoId + "&djId=" + djId + "&orden=" + orden + "&mto=" + mto + "&secuencia=" + secuencia + "&idAcuerdo=" + idAcuerdo + "&formato=" + formato + "&bloqueado=" + bloqueado + "&estadoDJ=" + estadoDJ + "&puedeTransmitir=" + puedeTransmitir +"&esDJRegistrada=S&acuerdo="+acuerdo+"&entidad="+entidad+"&nombrePais="+nombrePais, 1100, 600, null);
		    
		}

    </script>
    <body>
    <jlis:modalWindow onClose="validaCerraPopUpNoAction()" />
    	<div id="body">
     		<jsp:include page="/WEB-INF/jsp/header.jsp" />

    		<!-- CONTENT -->
			<div id="contp"><div id="cont">
        		<form name="formulario" method="post">

					<jlis:value type="hidden" name="method" />
					<jlis:value type="hidden" name="orden" />
					<jlis:value type="hidden" name="mto" />
					<jlis:value type="hidden" name="formato" />
					<jlis:value type="hidden" name="seleccionado" />
					<jlis:value type="hidden" name="entidad" value="9" />
					<jlis:value type="hidden" name="numero" value="" />
					<jlis:value type="hidden" name="idTupa" />
					<jlis:value type="hidden" name="idFormato" />
					<jlis:value type="hidden" name="idAcuerdo" />
					<jlis:value type="hidden" name="idEntidad" />

					<jlis:value type="hidden" name="button" />

					<jlis:messageArea width="100%" />
			        <br/>

		            <div id="pageTitle">
            			<h1><strong><jlis:label labelClass="pageTitle"  key="co.title.declaraciones_juradas" /></strong></h1>
            		</div>

		            <br/>

		            <p align="left">
		            	<jlis:label key="co.label.acuerdo" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<%-- 20170131_GBT ACTA CO-004-16 Y CO-009-16--%>
		            	<%-- <jlis:selectProperty key="comun.acuerdo.select" name="filtroAcuerdo" style="defaultElement=2" onChange="listarDeclaracionJurada()" />&nbsp;&nbsp;--%>
		            	<jlis:selectProperty key="comun.acuerdo.todos.select" name="filtroAcuerdo" style="defaultElement=2" onChange="listarDeclaracionJurada()" />&nbsp;&nbsp;
		            	
		            	<%-- 20140624_JMC BUG 128--%>
		            	<c:if test="${! (sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'))}">
		            	
			            	<jlis:label key="co.label.entidad" style="font-weight: bold;"/>&nbsp;&nbsp;		            	
			            	<c:choose>
		      					<c:when test="${sessionScope.USUARIO.tipoOrigen == 'ET' && (
			                    sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' ||
			                    sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
			                    sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO')}">
									<jlis:selectProperty key="comun.entidad.select.all" name="filtroEntidad" style="defaultElement=2" onChange="listarDeclaracionJurada()" />&nbsp;&nbsp;
							 	</c:when>
								<c:otherwise>
									<jlis:selectProperty key="comun.entidad.select" name="filtroEntidad" style="defaultElement=2" onChange="listarDeclaracionJurada()" />&nbsp;&nbsp;
								</c:otherwise>
					  		</c:choose>	            	
		            	</c:if>
		            	<br/>
		            	<jlis:label key="co.label.estado" style="font-weight: bold;"/>&nbsp;&nbsp;
		            	<jlis:selectProperty key="comun.estadosCO_DJ.select" name="filtroEstadoEntidad" style="defaultElement=2,width=250px" onChange="listarDeclaracionJurada()" />&nbsp;&nbsp;
						<jlis:label key="co.label.nombreExportador" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="filtroNombreExportador" size="50" maxLength="100" />
					</p>
                    
		            <br/>
                    
					<p align="left">
						<jlis:value type="radio" name="opcionFiltro" checkValue="C" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.declaracion" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="declaracion" size="20" onClick="seleccionDeclaracion()" maxLength="20" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="D" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.denominacion" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="denominacion" size="50" onClick="seleccionDenominacion()" maxLength="50" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="SC" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.solicitud" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="filtroSolicitud" size="20" onClick="seleccionSolicitud()" maxLength="10" />&nbsp;&nbsp;
					</p>
                    
					<br/>
                    
					<p align="left">
						<%--jlis:value type="radio" name="opcionFiltro" checkValue="SC" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.solicitud" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="filtroSolicitud" onClick="seleccionSolicitud()" size="20" maxLength="10" />&nbsp;&nbsp;
						<jlis:value type="radio" name="opcionFiltro" checkValue="S" onClick="seleccionOpcion(this)" /><jlis:label key="co.label.sc.suce" style="font-weight: bold;"/>&nbsp;&nbsp;
						<jlis:value name="suce" size="20" onClick="seleccionSuce()" maxLength="10" />&nbsp;&nbsp;
						--%>
		                <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK' ||
		                    sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
		                    sessionScope.USUARIO.roles['CO.CENTRAL.SUPERVISOR_TECNICO'] == 'CO.CENTRAL.SUPERVISOR_TECNICO')}">
		                <jlis:label key="co.label.filtro.tipoDocumento" style="font-weight:bold"/>
		                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                <jlis:selectProperty name="tipoDocumento"  key="comun.tipo_documento.select"/>
		                <jlis:label key="co.label.filtro.numeroDocumento" style="font-weight:bold"/>&nbsp;&nbsp;
		                <jlis:value name="numeroDocumento" size="10" type="text"/>&nbsp;&nbsp;
		                <jlis:label key="co.title.filtro.nombre" style="font-weight:bold"/>&nbsp;&nbsp;
		                <jlis:value name="nombre" size="50" type="text"/>&nbsp;&nbsp;
		                </c:if>
		                
						<jlis:button id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar" alt="Pulse aqu� para buscar" />
					</p>
                    
					<br/>
                    
		            <ul id="maintab" class="tabs">
		            	<li id="liRegi" class="selected" onclick="seleccionarTab(0)" ><a href="#" rel="tabRegistrados"><span>DJs Calificadas</span></a></li>
		            	<%-- 20140624_JMC BUG 128--%>
		            	<c:if test="${! (sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'))}">		            	
		                <li id="liAsig" onclick="seleccionarTab(1)" ><a href="#" rel="tabAsignados"><span>DJ asignadas como Productor</span></a></li>
		                </c:if>
		            </ul>
		            <div id="tabRegistrados" class="tabcontent" >
		            
		            	<%-- 20140624_JMC BUG 128 --%>
		            	<c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR')}">
		                    
		                    <jlis:table keyValueColumns="CALIFICACION_UO_ID,DJID,ORDEN_ID_INICIAL,MTO_INICIAL,IDACUERDO,FORMATO_INICIAL,NOMBREACUERDO,NOMBREENTIDAD,NOMBREPAIS" name="DJ_REG_EVAL" 
			            	            key="${keyGrillaDeclaracionJurada}" filter="${filterGrilla}" pageSize="10" width="90%" navigationHeader="yes" >
								<jlis:tr>
								    <jlis:td name="N�MERO DJ" nowrap="yes" />
								    <jlis:td name="N�MERO SUCE" nowrap="yes" />
								    <jlis:td name="CALIFICACION UO ID" nowrap="yes" />
									<jlis:td name="ORDEN ID INICIAL" nowrap="yes" />
									<jlis:td name="MTO INICIAL" nowrap="yes" />
									<jlis:td name="FORMATO INICIAL" nowrap="yes" />
									<jlis:td name="FECHA INICIO VIGENCIA" nowrap="yes" />
									<jlis:td name="FECHA FIN VIGENCIA" nowrap="yes" />
									<jlis:td name="ENTIDAD" nowrap="yes" />
									<jlis:td name="ACUERDO COMERCIAL" nowrap="yes" width="7%"/>
									<jlis:td name="PAIS" nowrap="yes" />
									<jlis:td name="DENOMINACION" nowrap="yes" />
									<jlis:td name="PARTIDA ARANC." nowrap="yes" />
									<jlis:td name="DJ ID" nowrap="yes"  width="18%"/>
									<jlis:td name="ENTIDAD ID" nowrap="yes" width="20%"/>
									<jlis:td name="ACUERDO ID" nowrap="yes" width="12%" />
									
									
	                        <jlis:td name="T. DOC." nowrap="yes" width="5%"/>
	                        <jlis:td name="NRO DOC." nowrap="yes" width="5%"/>
	                        <jlis:td name="SOLICITANTE" nowrap="yes" width="15%"/>
									
									<jlis:td name="VIGENCIA" nowrap="yes" width="2%" />
									<jlis:td name="ESTADO ID" nowrap="yes" width="9%" />
									<jlis:td name="NUMERO DOCUMENTO" nowrap="yes" width="7%"/>
									<jlis:td name="TIPO DOCUMENTO" nowrap="yes" width="7%"/>
								</jlis:tr>
								<jlis:columnStyle column="1" columnName="CALIFICACION_UO" editable="yes" />
								<jlis:columnStyle column="2" columnName="NRO_SUCE"  />
								<jlis:columnStyle column="3" columnName="CALIFICACION_UO_ID" hide="yes" />
								<jlis:columnStyle column="4" columnName="ORDEN_ID_INICIAL" hide="yes" />
								<jlis:columnStyle column="5" columnName="MTO_INICIAL" hide="yes" />
								<jlis:columnStyle column="6" columnName="FORMATO_INICIAL" hide="yes" />
								<jlis:columnStyle column="7" columnName="FECHAINICIOVIGENCIA" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
								<jlis:columnStyle column="8" columnName="FECHAFINVIGENCIA" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
								<jlis:columnStyle column="9" columnName="NOMBREENTIDAD" hide="yes"/>
								<jlis:columnStyle column="10" columnName="NOMBREACUERDO" />
								<jlis:columnStyle column="11" columnName="NOMBREPAIS" />
								<jlis:columnStyle column="12" columnName="DENOMINACION" />
								<jlis:columnStyle column="13" columnName="PARTIDAARANCELARIA"  />
								<jlis:columnStyle column="14" columnName="DJID" hide="yes" />
								<jlis:columnStyle column="15" columnName="IDENTIDAD" hide="yes" />
								<jlis:columnStyle column="16" columnName="IDACUERDO" hide="yes" />
								
								
	                    <jlis:columnStyle column="17" columnName="TIPO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
	                    <jlis:columnStyle column="18" columnName="NUMERO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
	                    <jlis:columnStyle column="19" columnName="SOLICITANTE" hide="${hideDatosSoliciante}" />
								
								<jlis:columnStyle column="20" columnName="DESCRIPCIONESTADOREGISTRO" />
								<jlis:columnStyle column="21" columnName="ESTADOREGISTRO" hide="yes" />
								<jlis:columnStyle column="22" columnName="NUMERODOCUMENTO" hide="yes" />
								<jlis:columnStyle column="23" columnName="DOCUMENTOTIPO" hide="yes" />
								<jlis:tableButton column="1" type="link" onClick="mostrarCalificacion" />
						 	</jlis:table>
						 	
		                </c:if>
		                
						<%-- 20140624_JMC BUG 128--%>
		            	<c:if test="${! (sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'))}">
	
			            	<jlis:table keyValueColumns="CALIFICACION_UO_ID,DJID,ORDEN_ID_INICIAL,MTO_INICIAL,IDACUERDO,FORMATO_INICIAL,NOMBREACUERDO,NOMBREENTIDAD,NOMBREPAIS" name="DJ_REG" 
			            	            key="${keyGrillaDeclaracionJurada}" filter="${filterGrilla}" pageSize="10" width="90%" navigationHeader="yes" validator="pe.gob.mincetur.vuce.co.web.util.DJCalificadasRowValidator" >
								<jlis:tr>
								    <jlis:td name="N�MERO DJ" nowrap="yes" />
								    <jlis:td name="N�MERO SUCE" nowrap="yes" />
								    <jlis:td name="CALIFICACION UO ID" nowrap="yes" />
									<jlis:td name="ORDEN ID INICIAL" nowrap="yes" />
									<jlis:td name="MTO INICIAL" nowrap="yes" />
									<jlis:td name="FORMATO INICIAL" nowrap="yes" />
									<jlis:td name="FECHA INICIO VIGENCIA" nowrap="yes" />
									<jlis:td name="FECHA FIN VIGENCIA" nowrap="yes" />
									<jlis:td name="ENTIDAD (CERTIFICADORA)" nowrap="yes" />
									<jlis:td name="ACUERDO COMERCIAL" nowrap="yes" width="7%"/>
									<jlis:td name="PAIS" nowrap="yes" />
									<jlis:td name="DENOMINACION" nowrap="yes" />
									<jlis:td name="PARTIDA ARANCELARIA" nowrap="yes" />
									<jlis:td name="DJ ID" nowrap="yes"  width="18%"/>
									<jlis:td name="ENTIDAD ID" nowrap="yes" width="20%"/>
									<jlis:td name="ACUERDO ID" nowrap="yes" width="12%" />
									<jlis:td name="USUARIO ID" nowrap="yes" width="9%" />
									
	                        <jlis:td name="T. DOC." nowrap="yes" width="5%"/>
	                        <jlis:td name="NRO DOC." nowrap="yes" width="5%"/>
	                        <jlis:td name="SOLICITANTE" nowrap="yes" width="15%"/>
									
									<jlis:td name="VIGENCIA" nowrap="yes" width="3%" />
									<jlis:td name="ESTADO ID" nowrap="yes" width="9%" />
									<jlis:td name="NUMERO DOCUMENTO" nowrap="yes" width="7%"/>
									<jlis:td name="TIPO DOCUMENTO" nowrap="yes" width="7%"/>
									<jlis:td name="VIGENCIA DJ" nowrap="yes" width="7%"/>
								</jlis:tr>
								<jlis:columnStyle column="1" columnName="CALIFICACION_UO" />
								<jlis:columnStyle column="2" columnName="SUCE" />
								<jlis:columnStyle column="3" columnName="CALIFICACION_UO_ID" hide="yes" />
								<jlis:columnStyle column="4" columnName="ORDEN_ID_INICIAL" hide="yes" />
								<jlis:columnStyle column="5" columnName="MTO_INICIAL" hide="yes" />
								<jlis:columnStyle column="6" columnName="FORMATO_INICIAL" hide="yes" />
								<jlis:columnStyle column="7" columnName="FECHAINICIOVIGENCIA" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
								<jlis:columnStyle column="8" columnName="FECHAFINVIGENCIA" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
								<jlis:columnStyle column="9" columnName="NOMBREENTIDAD" />
								<jlis:columnStyle column="10" columnName="NOMBREACUERDO" />
								<jlis:columnStyle column="11" columnName="NOMBREPAIS" />
								<jlis:columnStyle column="12" columnName="DENOMINACION" />
								<jlis:columnStyle column="13" columnName="PARTIDAARANCELARIA"  />
								<jlis:columnStyle column="14" columnName="DJID" hide="yes" />
								<jlis:columnStyle column="15" columnName="IDENTIDAD" hide="yes" />
								<jlis:columnStyle column="16" columnName="IDACUERDO" hide="yes" />
								<jlis:columnStyle column="17" columnName="USUARIOID" hide="yes" />
								
	                    <jlis:columnStyle column="18" columnName="TIPO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
	                    <jlis:columnStyle column="19" columnName="NUMERO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
	                    <jlis:columnStyle column="20" columnName="SOLICITANTE" hide="${hideDatosSoliciante}" />
								
								<jlis:columnStyle column="21" columnName="DESCRIPCIONESTADOREGISTRO" />
								<jlis:columnStyle column="22" columnName="ESTADOREGISTRO" hide="yes" />
								<jlis:columnStyle column="23" columnName="NUMERODOCUMENTO" hide="yes" />
								<jlis:columnStyle column="24" columnName="DOCUMENTOTIPO" hide="yes" />
								<jlis:columnStyle column="25" columnName="VIGENCIA_DJ" hide="yes" />
								<jlis:tableButton column="1" type="link" onClick="mostrarCalificacion" />
						 	</jlis:table>

						</c:if>
			        </div>
					<%-- 20140624_JMC BUG 128 --%>
					<c:if test="${! (sessionScope.USUARIO.tipoOrigen == 'ET' && (
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' ||
		                    sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'))}">
			        <div id="tabAsignados" class="tabcontent">

		            	<jlis:table keyValueColumns="CALIFICACION_UO_ID,DJID,IDACUERDO,IDENTIDAD,NOMBREACUERDO,NOMBRE,NOMBREPAIS,ORDEN,MTO,IDORDEN" name="DJ_ASIG" 
		            	            key="calificacionOrigen.asignados.grilla" filter="${filterGrilla}" pageSize="10" width="90%" navigationHeader="yes" >
							<jlis:tr>
							    <jlis:td name="CALIFICACION UO ID" nowrap="yes" />
								<jlis:td name="SUCE" nowrap="yes" />
								<jlis:td name="SOLICITUD" nowrap="yes" />
								<jlis:td name="ORDENID" nowrap="yes" />
								<jlis:td name="MTO" nowrap="yes" />
								<jlis:td name="FECHA DE REGISTRO" nowrap="yes" />
								<jlis:td name="NOMBRE EXPORTADOR" nowrap="yes" />
								<jlis:td name="ENTIDAD (CERTIFICADORA)" nowrap="yes" />
								<jlis:td name="IDACUERDO" nowrap="yes" width="7%"/>
								<jlis:td name="ACUERDO COMERCIAL" nowrap="yes" width="7%"/>
								<jlis:td name="PAIS" nowrap="yes" />
								<jlis:td name="DENOMINACION" nowrap="yes" />
								<jlis:td name="UNIDAD MEDIDA" nowrap="yes"  width="18%"/>
								
                        <jlis:td name="T. DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="NRO DOC." nowrap="yes" width="5%"/>
                        <jlis:td name="SOLICITANTE" nowrap="yes" width="15%"/>
								
								<jlis:td name="ESTADO" nowrap="yes" width="20%"/>
								<jlis:td name="NUMERO DOCUMENTO" nowrap="yes" width="12%" />
								<jlis:td name="TIPO DOCUMENTO" nowrap="yes" width="9%" />
								<jlis:td name="DJ ID" nowrap="yes" width="9%" />
								<jlis:td name="PRODUCTOR VALIDADOR" nowrap="yes" width="7%"/>
								<jlis:td name="ENTIDAD ID" nowrap="yes" width="7%"/>
								<jlis:td name="ESTADO ID" nowrap="yes" width="7%"/>
								<jlis:td name="NOTIFICACION" nowrap="yes" width="7%"/>
								<jlis:td name="ACCION" nowrap="yes" width="7%"/>
							</jlis:tr>
							<jlis:columnStyle column="1" columnName="CALIFICACION_UO_ID" hide="yes" />
							<jlis:columnStyle column="2" columnName="SUCE" hide="yes" />
							<jlis:columnStyle column="3" columnName="ORDEN" />
							<jlis:columnStyle column="4" columnName="IDORDEN" hide="yes" />
							<jlis:columnStyle column="5" columnName="MTO" />
							<jlis:columnStyle column="6" columnName="FECHAREGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm" />
							<jlis:columnStyle column="7" columnName="NOMBREEXPORTADOR" />
							<jlis:columnStyle column="8" columnName="NOMBRE" />
							<jlis:columnStyle column="9" columnName="IDACUERDO" hide="yes" />
							<jlis:columnStyle column="10" columnName="NOMBREACUERDO" />
							<jlis:columnStyle column="11" columnName="NOMBREPAIS" />
							<jlis:columnStyle column="12" columnName="DENOMINACION" />
							<jlis:columnStyle column="13" columnName="NOMBREUNIDADMEDIDA" hide="yes" />
							
                    <jlis:columnStyle column="14" columnName="TIPO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="15" columnName="NUMERO_DOCUMENTO_SOLICITANTE" align="center" hide="${hideDatosSoliciante}" />
                    <jlis:columnStyle column="16" columnName="SOLICITANTE" hide="${hideDatosSoliciante}" />
							
							<jlis:columnStyle column="17" columnName="DESCRIPCIONESTADOREGISTRO" />
							<jlis:columnStyle column="18" columnName="NUMERODOCUMENTO" hide="yes" />
							<jlis:columnStyle column="19" columnName="DOCUMENTOTIPO" hide="yes" />
							<jlis:columnStyle column="20" columnName="DJID" hide="yes" />
							<jlis:columnStyle column="21" columnName="PRODUCTORVALIDADOR" hide="yes" />
							<jlis:columnStyle column="22" columnName="IDENTIDAD" hide="yes" />
							<jlis:columnStyle column="23" columnName="ESTADOREGISTRO" hide="yes" />
							<jlis:columnStyle column="24" columnName="ESMODIFICACION" validator="pe.gob.mincetur.vuce.co.web.util.ProductorValidadorAsignadosEstadosCellValidator" method="mostrarAlertNotif" editable="yes"  />
							<c:choose>
								<c:when test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
				                                sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
								<jlis:columnStyle column="25" columnName="ACCION" editable="yes" align="center" />
								</c:when>
								<c:otherwise>
								<jlis:columnStyle column="25" columnName="ACCION" validator="pe.gob.mincetur.vuce.co.web.util.ProductorValidadorAsignadosEstadosCellValidator" method="mostrarAccion" editable="yes" align="center" />
								</c:otherwise>
							</c:choose>
                    		<jlis:tableButton column="24" type="imageButton" image="" onClick="" alt="" />
                    		<c:choose>
								<c:when test="${sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL' ||
				                                sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK'}">
   							    <jlis:tableButton column="25" type="imageButton" image="/co/imagenes/ver.gif" onClick="verDJModoLectura" alt="" />
								</c:when>
								<c:otherwise>
   							    <jlis:tableButton column="25" type="imageButton" image="" onClick="" alt="" />
								</c:otherwise>
							</c:choose>
					 	</jlis:table>

			        </div>

					</c:if>



				    <script>

			         	if (getCookie('indiceTab') != undefined){
			         		if (getCookie('indiceTab') != '') {
			             		document.getElementById("seleccionado").value = getCookie('indiceTab');
			         		}
			         	}

			         	if (document.getElementById("seleccionado").value == '0') {
			     			setCookie('maintab', 'tabRegistrados');
			     		} else {
			     			setCookie('maintab', 'tabAsignados');
			     		}
			        	/*if (document.getElementById("seleccionado").value==1) {
		                	$("#liRegi").removeClass("selected").addClass("");
		                	$("#tabRegistrados").hide();
		                	$("#liAsig").addClass("selected");
		                	$("#tabAsignados").show();
		                } else {
		                	$("#liRegi").addClass("selected");
		                	$("#tabRegistrados").show();
		                	$("#liAsig").removeClass("selected").addClass("");
		                	$("#tabAsignados").hide();
		                }*/

			         	initializetabcontent("maintab");

		                $("#divCargando").hide();
		                $("#divPestanas").show();
			        </script>
			    </form>
        	</div></div>
         	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>