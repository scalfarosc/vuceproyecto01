<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Sistema VUCE - Principal</title>
    <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <script language="JavaScript">
    
        function listarTrazaOrden() {
            var f = document.formulario;
            f.action = contextName + "/traza.htm";
            f.method.value = "listarTrazas";
            f.tipo.value = "orientacionUsu";
            f.submit();
        }

        function listarTrazaSuce() {
            var f = document.formulario;
            f.action = contextName + "/traza.htm";
            f.method.value = "listarTrazas";
            f.tipo.value = "procedimientoAdm";
            f.submit();
        }
    </script>
     <body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
	<div id="pageTitle">
		<h1><strong><jlis:label  labelClass="pageTitle"  key="vuce.title.trazabilidad" /></strong></h1>
	</div>
    <form name="formulario" method="post" onSubmit="return validarSubmit();">
    	<input type="hidden" name="tipo" />
    	<input type="hidden" name="opcion" />
        <input type="hidden" name="method" />
        
        <div class="welcome"><div><div>
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
			<h3><a href="javascript:listarTrazaOrden();">Orientaci&oacute;n al Usuario </a></h3>
            <br/>
            </c:if>
            <c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET' || (sessionScope.USUARIO.tipoOrigen == 'ET' && sessionScope.USUARIO.roles['VUCE.ENTIDAD.EVALUADOR'] == 'VUCE.ENTIDAD.EVALUADOR')}">
			<h3><a href="javascript:listarTrazaSuce();">Procedimiento Administrativo</a></h3>
            </c:if>

		</div></div></div>
	    
        </form>
        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>