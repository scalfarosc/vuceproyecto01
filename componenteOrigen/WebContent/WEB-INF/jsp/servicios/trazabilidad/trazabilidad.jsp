<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
    <title>Sistema VUCE - Principal</title>
    <meta http-equiv="Pragma" content="no-cache" />
</head>
<c:set var="activeSR" value="active" scope="request" />
<script language="JavaScript">
    function validarSubmit() {
        var f = document.formulario;
        if (f.button.value=="consultaButton") {
            if (f.numero.value=="") {
                alert("Debe ingresar un N�mero de Solicitud");
                return false;
            }
        }
        return true;
    }

    function consultar() {
        var f = document.formulario;
        f.action = contextName + "/traza.htm";
        f.method.value = "listarTrazas";
        f.button.value = "consultaButton";
    }

    function regresarTrazabilidad() {
        var f = document.formulario;
        //var rutaopcion = f.rutaopcion.value;
        f.button.value = "regresarTrazabilidadButton";
        /* if (rutaopcion=="DJ")
            f.action = contextName+"/dj.htm";
        else if (rutaopcion=="SC")
            f.action = contextName+"/sc.htm";
        else if (rutaopcion=="CER")
        	f.action = contextName+"/cer.htm";
        else */
        	f.action = contextName+"/origen.htm";

        /*if (rutaopcion=="DJ")
            f.method.value="listarDDJJ";
        else if (rutaopcion=="SC")
        	f.method.value="listarSC";
        else if (rutaopcion=="CER")
        	f.method.value="listarCertificado";
        else */
        	f.method.value = "listarCertificadoOrigen";
    }

</script>

 <body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
       <form name="formulario" method="post" onsubmit="return validarSubmit();" >
        <input type="hidden" name="method" />
        <input type="hidden" name="button" />
		<c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
        	<jlis:value type="hidden" name="rutaopcion" />
		</c:if>
       	<jlis:value type="hidden" name="tipo" />

        <div id="pageTitle">
        <h1><strong><jlis:label labelClass="pageTitle"  key="co.title.trazabilidad" />
        <%-- <br>
        Tipo: ${tipo}
        <br>
        Nombre Columna: ${nombreColumna} --%>
        </strong>
        </h1>
        </div>
		<div style="text-align:left">
		<c:if test="${tipo=='0'}" >
			<table class="form" >
				<tr>
					<th style="width:5%" nowrap>Solicitud:</th>
	             	<td><jlis:value name="numeroOrden" size="15" editable="no" type="text"/></td>
				</tr>
				<tr>
					<th style="width:5%" nowrap>SUCE:</th>
					<td><jlis:value name="numeroSuce" size="15" editable="no" type="text"/></td>
				</tr>
		     	<tr>
			 		<td colspan="3"><jlis:button id="regresarTrazabilidadButton" name="regresarTrazabilidad()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para Regresar" /></td>
				</tr>
			</table>
			<br>
		</c:if>
		<c:if test="${tipo == '1'}" >
			<table class="form" >
				<tr>
					<c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
	             		<td><jlis:value type="radio" name="rutaopcion" checkValue="O" defaultCheck="yes" /></td>
	             	</c:if>
	             	<%-- <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
	             		<td><jlis:value type="radio" name="rutaopcion" checkValue="OE" /></td>
	             	</c:if> --%>
	             	<th style="width:5%" nowrap>Solicitud</th>
	             	<c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
	             		<td><jlis:value type="radio" name="rutaopcion" checkValue="S"/></td>
	             	</c:if>
	             	<%-- <c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
	             		<td><jlis:value type="radio" name="rutaopcion" checkValue="SE"/></td>
	             	</c:if> --%>
					<th style="width:5%" nowrap>SUCE</th>
			     	<th style="width:5%" nowrap>N&uacute;mero:</th>
				 	<td><jlis:value name="numero" size="15"/></td>
			 	 	<td width="100%"></td>
				</tr>
			</table>
		<br>
		
			<table class="form" >
				<tr>
					<td><jlis:button id="consultaButton" name="consultar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Consultar" alt="Pulse aqu� para Consultar" /></td>
					<td><jlis:button id="regresarTrazabilidadButton" name="regresarTrazabilidad()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para Regresar" /></td>
					<td width="100%"></td>
				</tr>
			</table>
			<br>
		</c:if>

		</div>
        <c:if test="${!empty numeroOrden}" >
            <div id="pageTitle">
                <h1><strong><jlis:value name="nombreFormato" editable="no" valueClass="pageTitle" /></strong></h1>
            </div>
        </c:if>
		<jlis:table keyValueColumns="traza_tce_id" name="trazas" source="listadoTrazabilidad" scope="request" pageSize="*" cellPadding="2" navigationHeader="no" >
            <jlis:tr>
            	<jlis:td name="${nombreColumna}" nowrap="yes" width="3%"/>
            	<jlis:td name="FECHA" nowrap="yes" width="5%"/>
		    	<jlis:td name="SECUENCIA" nowrap="yes" width="5%"/>
            	<jlis:td name="ETAPA" nowrap="yes" width="20%"/>
                <jlis:td name="DESCRIPCI�N DETALLADA" nowrap="yes" width="50%"/>
            </jlis:tr>
            <jlis:columnStyle column="1" columnName="orden" align="center" hide="yes"/>
            <jlis:columnStyle column="2" columnName="fecha" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" nowrap="yes"/>
            <jlis:columnStyle column="3" columnName="secuencia" hide="yes" align="center"/>
            <jlis:columnStyle column="4" columnName="etapa" />
            <jlis:columnStyle column="5" columnName="descripcion_detallada" />
        </jlis:table>
      </form>

        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>