<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
    <title>Sistema VUCE - Principal</title>
    <meta http-equiv="Pragma" content="no-cache" /> 
</head>
<script language="JavaScript">
    function consultar() {
        var f = document.formulario;
        f.action = contextName+"/traza.htm?method=listarTrazas&tipo=orientacionUsu";
    }
    
    function back() {
        var f = document.formulario;
        if(f.rutaopcion.value!="O") {
        	f.action = contextName+"/menuTraza.htm?method=menuTraza";
        }
        else {
	       	f.action = contextName+"/fmt.htm";
           	f.method.value="cargarListaOrdenes";
        }
   		f.button.value="regresarButton";        
    }
    
    function regresar() {
        var f = document.formulario;
        if (f.rutaopcion.value=="") 
            f.action = contextName+"/menuTraza.htm?method=menuTraza";
        else if (f.rutaopcion.value=="O" || f.rutaopcion.value=="S")
            f.action = contextName+"/fmt.htm";
        else if (f.rutaopcion.value=="OE" || f.rutaopcion.value=="SE")
            f.action = contextName+"/adment.htm";
        if (f.rutaopcion.value=="O" || f.rutaopcion.value=="OE")
            f.method.value="cargarListaOrdenes";
        else if (f.rutaopcion.value=="S" || f.rutaopcion.value=="SE")
            f.method.value="cargarListaSuces";
        f.button.value="regresarButton";
    }

</script>

 <body >
    <div id="body">
     <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
       <form name="formulario" method="post" >
        <input type="hidden" name="method" />
        <input type="hidden" name="button" />
        <input name="rutaopcion" value="${rutaopcion}" type="hidden" />
        <div id="pageTitle">
        <h1><strong><jlis:label labelClass="pageTitle"  key="vuce.title.trazabilidad.orientacionUsuario" /></strong></h1>
        </div>
		<div style="text-align:left">
		<table class="form" >
		 	<tr>
		     <th style="width:5%" nowrap>N&uacute;mero de Orden:</th>
			 <td><jlis:value name="numerorden" size="15"/></td>
		 	 <td width="100%"></td>
	     	</tr>
		</table>
		<br>
		<table class="form" >
		 	<tr>
			 	<td><jlis:button id="cargarButton" name="consultar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Consultar" alt="Pulse aqu� para Consultar" /></td>
			 	<td><jlis:button id="backButton" name="regresar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para Regresar" /></td>
			 	<td width="100%"></td>
		 	</tr>
		</table>
		<br>
		</div>
        <c:if test="${!empty numerorden}" >
            <div id="pageTitle">
                <h1><strong><jlis:value name="nombreFormato" editable="no" valueClass="pageTitle" /></strong></h1>
            </div>
        </c:if>
		<jlis:table keyValueColumns="traza_tce_id" name="trazas" source="trazaOrientacionUsu" scope="request" 
            pageSize="20" cellPadding="2">
            <jlis:tr>
            	<jlis:td name="ORDEN" nowrap="yes" width="3%"/>
            	<jlis:td name="FECHA" nowrap="yes" width="10%"/>
		    	<jlis:td name="SECUENCIA" nowrap="yes" width="3%"/>
            	<jlis:td name="ETAPA" nowrap="yes" width="5%"/>
                <jlis:td name="DESCRIPCI�N DETALLADA" nowrap="yes" width="25%"/>
                <jlis:td name="AREA" nowrap="yes" width="15%"/>
                <jlis:td name="SUB AREA" nowrap="yes" width="10%"/>
                <jlis:td name="RESPONSABLE" nowrap="yes" width="15%"/>
                <jlis:td name="FECHA INGRESO" nowrap="yes" width="15%"/>
                <jlis:td name="FECHA ESTIMADA SALIDA" nowrap="yes" width="15%"/>
            </jlis:tr>	            
            <jlis:columnStyle column="1" columnName="orden" align="center" hide="yes"/>
            <jlis:columnStyle column="2" columnName="fecha" align="center" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" nowrap="yes"/>	            
            <jlis:columnStyle column="3" columnName="secuencia" align="center"/>	
            <jlis:columnStyle column="4" columnName="etapa" />
            <jlis:columnStyle column="5" columnName="descripcion_detallada" />
            <jlis:columnStyle column="6" columnName="area"/>
            <jlis:columnStyle column="7" columnName="sub_area" align="center"/>
            <jlis:columnStyle column="8" columnName="responsable" />
            <jlis:columnStyle column="9" columnName="fecha_ingreso" type="dateTime" pattern="dd/MM/yyyy" />
            <jlis:columnStyle column="10" columnName="fecha_estimada_salida" type="dateTime" pattern="dd/MM/yyyy" />
        </jlis:table>	        
      </form>

        </div></div>
         <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>