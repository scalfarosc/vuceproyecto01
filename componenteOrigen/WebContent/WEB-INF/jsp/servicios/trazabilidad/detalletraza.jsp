<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
	<head>
	<title>Sistema VUCE</title>
	</head>
	
	<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
	<script language="JavaScript">

	    function descargarAdjunto(keyValues, keyValuesField) {
	        var f = document.formulario;
	        var adjuntoId = document.getElementById(keyValues).value;
	        f.action = contextName+"/buzon.htm?method=descargarAdjunto&idAdjunto="+adjuntoId;
	        f.submit();
        }
        
    </script>
	<body id="contModal">
    
        <form name="formulario" method="post"">
            <input type="hidden" name="method" />
            <input type="hidden" name="button">
  <table >
     <tr>
	     <th><jlis:label key="vuce.label.buzon.de"/></th>
		 <td colspan="4"><jlis:value name="MENSAJE.de" size="90"/></td>
     </tr>
     <tr>
          <th><jlis:label key="vuce.label.buzon.asunto"/></th>
          <td colspan="4"><jlis:value name="MENSAJE.asunto" size="90"/></td>
     </tr>
     <!-- <tr>
          <th><jlis:label key="vuce.label.buzon.estado"/></th>
          <td colspan="4"><jlis:value name="MENSAJE.estado" size="90"/></td>
     </tr> -->
      <tr>
          <th><jlis:label key="vuce.label.buzon.fecha"/></th>
          <td colspan="4"><jlis:value name="MENSAJE.fechaRegistro" type="dateTime" pattern="dd/MM/yyyy" size="90"/></td>
     </tr>
     <tr>
          <th colspan="5"><jlis:label key="vuce.label.buzon.mensaje"/></th>
          </tr>
     <tr>
        <th colspan="5"><jlis:textArea name="MENSAJE.mensaje" cols="115" rows="3" /></th>
	</tr>
	
</table> 

	<jlis:table keyValueColumns="ADJUNTO_ID" name="adjuntosMensaje" source="tAdjuntosMensaje" scope="request" pageSize="15" cellPadding="2" width="100%">
        <jlis:tr>
        	<jlis:td name="ADJUNTO ID" nowrap="yes" />
        	<jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
        	<jlis:td name="ADJUNTO TIPO" nowrap="yes" />
        	<jlis:td name="ARCHIVO" nowrap="yes" />
            <jlis:td name="DESCRIPCIÓN" nowrap="yes" />
           </jlis:tr>
        <jlis:columnStyle column="1" columnName="ADJUNTO_ID" hide="yes" />
        <jlis:columnStyle column="2" columnName="NOMBRE_ARCHIVO" editable="yes"/>
        <jlis:columnStyle column="3" columnName="ADJUNTO_TIPO" align="center" hide="yes"/>
        <jlis:columnStyle column="4" columnName="ARCHIVO" align="center" hide="yes"/>
        <jlis:columnStyle column="5" columnName="DESCRIPCION"  align="center" hide="yes"/>
       <jlis:tableButton column="2" type="link" onClick="descargarAdjunto" />
    </jlis:table>
				   

</form>
<script>
            window.parent.document.getElementById("popupTitle").innerHTML = "Detalle Mensaje";
            window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
        	
        </script>
</body>
</html>