<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
    <head>
    <title>Sistema CO - Buzon Electronico</title>
    <meta http-equiv="Pragma" content="no-cache" />
    </head>
    <c:set var="activeSR" value="active" scope="request" />
    <script language="JavaScript">

        function listarTrazas() {
            var f = document.formulario;
            f.action = contextName + "/traza.htm";
            f.tipo.value = "1";
            f.method.value = "listarTrazas";
            f.target = "_self";
            f.submit();
        }

        function consultarDR() {
            var f = document.formulario;
            f.action = contextName + "/consultaDR.htm?method=inicioConsultaDR";
            f.target = "_self";
            f.submit();
        }    
        
        function busquedaCOFD() {
            var f = document.formulario;
            f.action = contextName + "/origen.htm?method=busquedaCOFD";
            f.target = "_self";
            f.submit();
        } 
        
        
        function validarSubmit() {
            var f = document.formulario;
            return true;
        }
        
    </script>
    <body >
    <div id="body">
    <jsp:include page="/WEB-INF/jsp/header.jsp" />
    <!-- CONTENT -->
	<div id="contp"><div id="cont">
	<div id="pageTitle">
		<h1><strong><jlis:label  labelClass="pageTitle"  key="co.title.servicios" /></strong></h1>
	</div>
    <form name="formulario" method="post" onSubmit="return validarSubmit();">
       	<input type="hidden" name="tipo" />
    	<input type="hidden" name="opcion" />
        <input type="hidden" name="method" />    
        <div class="welcome"><div><div>
    
		 <c:if test="${(sessionScope.USUARIO.roles['CO.USUARIO.OPERACION'] == 'CO.USUARIO.OPERACION')||(sessionScope.USUARIO.roles['VUCE.SUNAT.ESPECIALISTA'] == 'VUCE.SUNAT.ESPECIALISTA')||
		 (sessionScope.USUARIO.roles['CO.ADMIN.HELP_DESK'] == 'CO.ADMIN.HELP_DESK')||(sessionScope.USUARIO.roles['VUCE.CENTRAL.OPERADOR_FUNCIONAL'] == 'VUCE.CENTRAL.OPERADOR_FUNCIONAL') ||
		 (sessionScope.USUARIO.roles['CO.CENTRAL.OPERADOR_FUNCIONAL'] == 'CO.CENTRAL.OPERADOR_FUNCIONAL')}">
		 	<c:if test="${(sessionScope.USUARIO.roles['CO.ENTIDAD.CONSULTA_DR'] != 'CO.ENTIDAD.CONSULTA_DR')}">
	        <h3><a href="javascript:busquedaCOFD();">B�squeda de Certificados de Origen con Firma Digital</a></h3>      
	         <br/>
	         </c:if> 
         </c:if>
         
         <c:if test="${(sessionScope.USUARIO.roles['CO.ENTIDAD.CONSULTA_DR'] != 'CO.ENTIDAD.CONSULTA_DR')}">
         <h3><a href="#" onClick="window.location = 'http://200.62.224.217/descarga/VUCE_SIGNATURE/VuceSignature_v1.0.exe';">Descargar Aplicaci�n Vuce Signature para la Firma Digital</a></h3>
         </c:if>
        <c:if test="${(sessionScope.USUARIO.roles['CO.ENTIDAD.CONSULTA_DR'] == 'CO.ENTIDAD.CONSULTA_DR')}">
	        <h3><a href="javascript:consultarDR();">CONSULTA DR</a></h3>      
	     </c:if> 

        
        <br/>
      
		</div></div></div>
	                
        </form>
        </div></div>
        <jsp:include page="/WEB-INF/jsp/footer.jsp" />
        </div>
    </body>
</html>