<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>

    <jlis:table keyValueColumns="DRID,SDR" name="DOCS_RESOLUTIVOS" source="tDocResolutivos" scope="request" pageSize="*" width="100%" navigationHeader="no"
        validator="pe.gob.mincetur.vuce.co.web.util.DocumentosResolutivosSUCEResolutorRowValidator" >
		<jlis:tr>
			<jlis:td name="NUMERO DOC" nowrap="yes" />
			<jlis:td name="DRID" nowrap="yes" />
			<jlis:td name="SEC" nowrap="yes" />
			<jlis:td name="DESCRIPCIÓN" nowrap="yes" />
            <jlis:td name="TIPO" nowrap="yes" width="88%"/>
			<jlis:td name="DATOS" nowrap="yes" width="4%"/>
			<jlis:td name="ADJUNTOS" nowrap="yes" width="4%"/>
            <jlis:td name="ANULADO" nowrap="yes" />
		</jlis:tr>
		<jlis:columnStyle column="1" columnName="DR" />
		<jlis:columnStyle column="2" columnName="DRID" hide="yes" />
		<jlis:columnStyle column="3" columnName="SDR" hide="yes" />
		<jlis:columnStyle column="4" columnName="DESCRIPCION" hide="yes" />
        <jlis:columnStyle column="5" columnName="TIPO" align="center" />
		<jlis:columnStyle column="6" columnName="DATOS" align="center" editable="yes" validator="pe.gob.mincetur.vuce.co.web.view.jsp.tag.validator.DocumentosResolutivosCellValidator" />
		<jlis:columnStyle column="7" columnName="ADJUNTOS" align="center" editable="no" hide="yes" />
        <jlis:columnStyle column="8" columnName="ANULADO" hide="yes" />
        <jlis:tableButton column="6" type="imageButton" image="/co/imagenes/ver.gif" onClick="verDatosDocResol" alt="Ver Datos del Documento Resolutivo" />
		<jlis:tableButton column="7" type="imageButton" image="/co/imagenes/adjuntos.gif" onClick="verAdjuntos" alt="Ver Adjuntos del Documento Resolutivo" />
	</jlis:table>
    <br/>
    <table style="margin: 0 auto; width: 96%"><tr><td class="labelClass" colspan="2">Leyenda:</td></tr><td class="codigoGrisClass" style="border: 1px solid black;" width="10px">&nbsp;&nbsp;</td><td class="labelClass">&nbsp;ANULADO</td></tr></table>
