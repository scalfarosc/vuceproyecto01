<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<table class="form">
  <tr>
    <td><h3 class="psubtitle"><span><b>
    	<c:if test="${ formato == 'MCT001' }" >
    		<jlis:label key="co.title.documentos_adjuntar_mct001"  />
    	</c:if>
    	<c:if test="${ formato != 'MCT001' }" >
    		<jlis:label key="co.title.documentos_adjuntar"  />
    	</c:if>
    </b></span></h3>	</td>
  </tr>
 <tr>
 	<td>&nbsp;</td>
 </tr>
</table>
<jlis:table name="ADJUNTO_REQUERIDO" keyValueColumns="ADJUNTO_REQUERIDO" key="formato.adjunto_requerido.grilla" 
    pageSize="*" navigationHeader="no" width="100%" filter="${filterADJ}">
	<jlis:tr>
		<jlis:td name="ADJUNTO REQUERIDO" nowrap="yes" />
		<jlis:td name="OBLIGATORIO" nowrap="yes" width="4%"/>
		<jlis:td name="DESCRIPCIÓN" nowrap="yes" width="92%"/>
		<jlis:td name="ADJUNTOS" nowrap="yes" width="4%"/>
		<jlis:td name="XX" />
	</jlis:tr>
	<jlis:columnStyle column="1" columnName="ADJUNTO_REQUERIDO" hide="yes" />
	<jlis:columnStyle column="2" columnName="OBLIGATORIO" align="center" />
	<jlis:columnStyle column="3" columnName="DESCRIPCION" editable="yes" />
	<jlis:columnStyle column="5" columnName="XX" hide="yes" />
	<jlis:tableButton column="3" type="link" onClick="adjuntarDocumento" />
	<jlis:columnStyle column="4" columnName="CONTEO" align="center" style="font-size:14px; color:blue; font-weight:600"/>
</jlis:table>
