<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${idioma}" scope="page"/>
<fmt:setBundle basename="label" var="labelBundle" scope="page"/>
<fmt:setBundle basename="message" var="messageBundle" scope="page"/>
<html>
    <head>
        <title>Sistema CO</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    	<script language="JavaScript">
    	var msgEnteroPositivo = "<fmt:message key='co.mensaje.ingresar_entero_positivo' bundle='${messageBundle}'/>";
    	
        $(document).ready(function() {
        	var codigoError = $("#codigoError").val();
        	if (codigoError != "") {
        		$("#imagenCaptcha").load(function() {
        			alert ("<fmt:message key='${codigoError}' bundle='${messageBundle}'/>");
            		// En caso error en texto captcha
            		if (codigoError == "vuce.error.texto_captcha_sugerencia") {
            			$("#codigoCaptcha").val("");
            		}
        		});
        	}
        	
        	// Cancelar evento fechaDrEntidad
			$("#fechaDrEntidad").attr("onchange", "");
			
			$("#filtroPais option").each(function(i,el){
		    	 if($("#idioma").val()=='en'&&$(this).val() == ""){
		    		 $(el).text('--Select Country--');
		    	 }
		     });
        });
        
        function validarSubmit() {
        	var f = document.formulario;
        	if (f.button.value == "cancelarButton") {
        		return false;
        	} else {
        		return true;
        	}
        }
        
        function validarConsulta() {
			var ok = true;
            var mensaje="<fmt:message key='co.mensaje.ingresar_seleccionar' bundle='${messageBundle}'/>";
            var formatoDR = new RegExp(/\d{4}-\d{2}-\d{7}/);
            
	            if ($("#filtroPais").val() == "") {
			    	 mensaje +="\n -<fmt:message key='consulta_dr.mensaje.consulta_dr.pais' bundle='${messageBundle}'/>";
			    	changeStyleClass("consulta_dr.label.pais", "errorValueClass");
			    } else changeStyleClass("consulta_dr.label.pais", "labelClass");

	            /*if ($("#filtroAcuerdo").val() == "") {
			    	 mensaje +="\n -<fmt:message key='consulta_dr.mensaje.consulta_dr.acuerdo_comercial' bundle='${messageBundle}'/>";
			    	changeStyleClass("consulta_dr.label.acuerdo_comercial", "errorValueClass");
			    } else changeStyleClass("consulta_dr.label.acuerdo_comercial", "labelClass");
				*/
			    if ($("#drEntidad").val() == "") {
			    	 mensaje +="\n -<fmt:message key='consulta_dr.mensaje.consulta_dr.dr_entidad' bundle='${messageBundle}'/>";
			    	changeStyleClass("consulta_dr.label.dr_entidad", "errorValueClass");
			    } else if (!formatoDR.test($("#drEntidad").val())) {
			    	mensaje += "\n -<fmt:message key='consulta_dr.error.formato_dr' bundle='${messageBundle}'/>";
			    	changeStyleClass("consulta_dr.label.dr_entidad", "errorValueClass");
			    } else {
			    	changeStyleClass("consulta_dr.label.dr_entidad", "labelClass");
			    }
		    if ($("#fechaDrEntidad").val() == "") {
		    	 mensaje +="\n -<fmt:message key='consulta_dr.mensaje.consulta_dr.fecha_dr_entidad' bundle='${messageBundle}'/>";
		    	changeStyleClass("consulta_dr.label.fecha_dr_entidad", "errorValueClass");
		    } else if (!validarFormatoFecha()) {
		    	mensaje +="\n -<fmt:message key='consulta_dr.error.formato_fecha' bundle='${messageBundle}'/>";
				changeStyleClass("consulta_dr.label.fecha_dr_entidad", "errorValueClass");
		    	
		    } else if (!validarMenorFechaActual()) {
		    	mensaje +="\n -<fmt:message key='consulta_dr.error.fecha_mayor_actual' bundle='${messageBundle}'/>";
				changeStyleClass("consulta_dr.label.fecha_dr_entidad", "errorValueClass");
		    } else changeStyleClass("consulta_dr.label.fecha_dr_entidad", "labelClass");

		    if ($("#codigoCaptcha").val() == "") {
		    	 mensaje +="\n -<fmt:message key='consulta_dr.mensaje.consulta_dr.texto_imagen' bundle='${messageBundle}'/>";
		    	changeStyleClass("consulta_dr.label.texto_imagen", "errorValueClass");
		    } else changeStyleClass("consulta_dr.label.texto_imagen", "labelClass");
		    
            if ( mensaje != "<fmt:message key='co.mensaje.ingresar_seleccionar' bundle='${messageBundle}'/>" ) {
                ok = false;
                alert(mensaje);
            }
            return ok;
        }

        function generarCodigoCaptcha() {
        	var random = Math.random();
        	var captchaId = $("#captchaId").val();
        	$("#imagenCaptcha").unbind("load");
        	document.getElementById("imagenCaptcha").src="/co/captcha.htm?method=generarCodigo&random=" + random + "&captchaId=" + captchaId;
        	return false;
        }
        
        function cambiarIdioma(idioma) {
        	var f = document.formulario
            f.action = contextName + "/" + "consultaDRPub.htm";
        	f.method.value="cambiarIdioma";
        	f.idioma.value = idioma;
        	f.submit();
        }

        function limpiarDR() {
        	var f = document.formulario;
        	$("#drEntidad").val("");
        	$("#fechaDrEntidad").val("");
        	$("#filtroPais").val("");
        	$("#codigoCaptcha").val("");
	        f.button.value="cancelarButton";
            return false;
        }
        
        function consultarDR() {
        	var f = document.formulario;
        	if (validarConsulta()) {
                f.action = contextName + "/" + "consultaDRPub.htm";
            	f.method.value="consultarDR";
            	f.button.value="consultarDR";
        	} else {
	        	f.button.value="cancelarButton";
	        }
            return false;
        }
        
        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formularioAdjuntos;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/consultaDRPub.htm";
            f.method.value="descargarConsultaDR";
            f.idAdjunto.value=adjuntoId;
            f.target = "_blank";
            f.submit();
        }
        
        function validarFormatoFecha() {
        	var resultado = false;
        	var fechaDrEntidad = $("#fechaDrEntidad").val();
        	var formatoFechaValido = new RegExp(/\d{2}\/\d{2}\/\d{4}/);
        	return formatoFechaValido.test(fechaDrEntidad);
        }
        
        function validarMenorFechaActual () {
        	var resultado = false;
        	var idioma = $("#idioma").val();
        	var fechaInicio = $("#fechaDrEntidad").val();
        	var fechaFin = $("#fechaActual").val();
            var strfecha = fechaInicio;
            var indice = strfecha.indexOf('-');
            if (indice==-1) indice = strfecha.indexOf('/');
            if (indice==-1) indice = strfecha.indexOf('.');
            var strSeparator = strfecha.substring(indice,indice+1); //find date separator
            
            var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
            if (arrayDate[0].length==1) arrayDate[0] = '0'+arrayDate[0];
            if (arrayDate[1].length==1) arrayDate[1] = '0'+arrayDate[1];
            if (arrayDate[2].length==2) arrayDate[2] = '20'+arrayDate[2];
            else if (arrayDate[2].length==1) arrayDate[2] = '200'+arrayDate[2];
            
            // Fecha Inicio segun idioma: es/en
            var fechaI = "";
            if (idioma == "en") {
            	fechaI = arrayDate[2]+arrayDate[0]+arrayDate[1]
            } else {
            	fechaI = arrayDate[2]+arrayDate[1]+arrayDate[0];
            }
            
        	// Fecha Fin (ya esta formateada yyyyMMdd)
            var fechaF = fechaFin;
            // Ahora comparo
            return (parseInt(fechaF) >= parseInt(fechaI));
        }

        </script>
    </head>
    <body>
		<div id="body">
		<jsp:include page="/WEB-INF/jsp/header.jsp" />
			<!-- Mensajes -->		
			<jlis:messageArea width="100%" />
		    <!-- CONTENT -->
			<div id="contp">
				<div id="cont">
				    <form name="formulario" method="post" onsubmit=" return validarSubmit();">
				        <input type="hidden" name="method" />
				        <input type="hidden" name="button">
				        <input type="hidden" name="random">
				        <jlis:value name="idioma" type="hidden" />
				        <jlis:value name="fechaActual" type="hidden" />
				        <jlis:value name="codigoError" type="hidden" />
				        
				        <jlis:value name="captchaId" type="hidden" />
						<table class="form" style="margin-left:50px;width:50%;border: 1px solid black;">
								<tr>
									<th colspan="7">
										<h3 class="psubtitle">
											<span><b><jlis:label key="consulta_dr.title.consulta_dr" language="${idioma}" /></b></span>
										</h3>
									</th>
								</tr>
								<tr>
									<th colspan="7">
										<h3 class="psubtitle" style="margin-top:1px;text-align:right;text-transform:none;">
											<a href="#" onclick="cambiarIdioma('en')" class="subtitlea"><span><jlis:label key="consulta_dr.label.idioma_ingles" language="${idioma}" /></span></a>
											<span class="subtitlea">|</span>
											<a href="#" onclick="cambiarIdioma('es')" class="subtitlea"><span><jlis:label key="consulta_dr.label.idioma_espanol" language="${idioma}" /></span></a>
										</h3>
									</th>
								</tr>
								<tr><td colspan="7"/></tr>
								<tr>
									<th nowrap="nowrap" colspan="1">
									    <%--jlis:value type="radio" name="opcionFiltro" checkValue="C" onClick="seleccionOpcion(this)" /--%>
										<jlis:label key="consulta_dr.label.dr_entidad" language="${idioma}" />
									</th>
									<th colspan="2">
										<jlis:value type="text" name="drEntidad" size="20" maxLength="15" /><span class="requiredValueClass">(*)</span>
									</th>
									<th nowrap="nowrap" style="text-align: right" colspan="1">
										<jlis:label key="consulta_dr.label.fecha_dr_entidad" language="${idioma}" />
									</th>
									<td nowrap="nowrap" style="padding-right: 10px" colspan="3">
										<jlis:date form="formulario" name="fechaDrEntidad" pattern="${formatoFecha}" calendarPattern="${formatoCalendario}" /><span class="requiredValueClass">(*)</span>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap" colspan="1">
										<jlis:label key="consulta_dr.label.pais" style="font-weight: bold;" language="${idioma}"/>
									</th>
									<th colspan="6">
									<c:if test="${idioma == 'es'}">
										<jlis:selectProperty key="comun.pais_iso.acuerdos_all.select" name="filtroPais"  editable="yes" /><span class="requiredValueClass">(*)</span>
									</c:if>
									<c:if test="${idioma == 'en'}">
										<jlis:selectProperty key="comun.pais_iso.acuerdos_all.en.select" name="filtroPais"  editable="yes" /><span class="requiredValueClass">(*)</span>
									</c:if>
									</th>
								</tr>
								<tr>
									<th colspan="1">
										<img id="imagenCaptcha" src="/co/captcha.htm?method=generarCodigo&random=${random}&captchaId=${captchaId}"/>
									</th>
									<th width="50%" colspan="2">
										<jlis:label key="consulta_dr.label.texto_imagen" language="${idioma}" />
										<jlis:value type="text" name="codigoCaptcha" size="20" maxLength="5" />
									</th>
									<td colspan="4"></td>		
								</tr>
								<tr>
									<th colspan="1">
										<jlis:label key="consulta_dr.label.mostrar_otra_imagen" language="${idioma}" />
										<a href="" onclick="return generarCodigoCaptcha()"><img id="imagenCaptcha" src="/co/imagenes/actualizar.gif"/></a>
									</th>
									<td colspan="6"></td>
								</tr>
								<!-- F GCHAVEZ -->
								<tr>
									<th colspan="3"><jlis:label key="consulta_dr.label.fecha_hora" language="${idioma}" />${fechaHoraActual}</th>
									<td colspan="4" style="text-align: right; padding-right: 10px">
										<input type="submit" id="limpiarDRButton" value="<fmt:message key='co.boton.limpiar' bundle='${labelBundle}' />" title="" class="buttonSubmitClass" onclick="limpiarDR();" >
										<input type="submit" id="consultarDRButton" value="<fmt:message key='co.boton.consultar' bundle='${labelBundle}' />" title="" class="buttonSubmitClass" onclick="consultarDR();" >
									</td>			
								</tr>
						</table>
						<c:if test="${requestScope.mostrarResultado}">
						    <jlis:table  name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntosDR" scope="request" pageSize="*" navigationHeader="no" width="95%" >
						          <jlis:tr>
						                <jlis:td name="ADJUNTO ID" nowrap="yes" />
						                <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
						                <c:if test="${empty idioma || idioma=='es'}">
						                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
						                </c:if>
						                <c:if test="${idioma=='en'}">
						                <jlis:td name="FILE NAME" nowrap="yes" />
						                </c:if>
						                <c:if test="${editarDR == 'S'}">
						                <jlis:td name="" width="1%" style="checkbox=formularioAdjuntos,seleccione,on-off"  />
						                </c:if>
						          </jlis:tr>
						            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
						            <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
						            <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes"  />
						            <c:if test="${editarDR == 'S'}">
						            <jlis:columnStyle column="4" columnName="SELECCIONE" align="center" editable="yes" />
						            <jlis:tableButton column="4" type="checkbox" name="seleccione" />
						            </c:if>
						            <c:if test="${editarDR != 'S'}">
						            <jlis:columnStyle column="4" columnName="SELECCIONE" hide="yes" />
						            </c:if>
						            <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
						    </jlis:table>
					    </c:if>
					</form>
				</div>
			</div>
			<jsp:include page="/WEB-INF/jsp/footer.jsp" />
		</div>
		<form name="formularioAdjuntos" method="post" enctype="multipart/form-data" onSubmit="return validarSubmitAdjuntos();">
		    <input type="hidden" name="method" />
		    <input type="hidden" name="idAdjunto">
		</form>
    </body>
</html>