<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/tags/jlis-framework" prefix="jlis"%>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>

    <script>
        $(document).ready(
            function() {
            	$("input[obligatory='yes']").after("<span class='requiredValueClass'>(*)</span>");
                initializetabcontent("maintab");
        	
            });

        function changeTipoCalif(obj){
        	if (obj.value == '1') {
        		$("calificacionUoId").val('');
        		try {changeStyleClass("co.label.calificacion.copia.existente", "labelClass");}catch(e){}
        	} else {
        		var idFormatoEntidad = document.getElementById("idFormatoEntidad").value;
        		var orden = document.getElementById("orden").value;
        		var mto = document.getElementById("mto").value;
        		var bloqueado = document.getElementById("bloqueado").value;
        		var idEntidadCertificadora = document.getElementById("idEntidadCertificadora").value;
        		var idAcuerdo = document.getElementById("idAcuerdo").value;
        		var formato = document.getElementById("formato").value;
        		var borrador = 1;// Permite crear una nuevacalificaci�n de Origen a partir de una existente

        		showPopWin(contextName+"/origen.htm?method=cargarCertificadoOrigenBuscDj&orden="+orden+"&mto="+mto+"&idFormatoEntidad="+idFormatoEntidad+"&entidad="+idEntidadCertificadora+"&acuerdo="+idAcuerdo+"&formato="+formato+"&borrador="+borrador, 800, 500, null);
        	}
        }

        function cargarDataDJ(calificacionUoId, borrador){
        	$("#calificacionUoId").val(calificacionUoId);
        	try {changeStyleClass("co.label.calificacion.copia.existente", "labelClass");}catch(e){}
        }
        
       //20170131_GBT ACTA CO-004-16 Y ACTA CO 009-16
        function estadoAcuerdoPaisInactivo(keyValues, keyValuesField){
        	var f = document.formulario;
        	f.button.value = "nuevaFacturaButton";
        	alert("No se puede procesar la solicitud, el acuerdo comercial con este pa�s no se encuentra vigente");
    	}

    </script>

    <jlis:value name="idAcuerdo" type="hidden" />
    <jlis:value name="idPais" type="hidden" />
    <jlis:value name="idEntidadCertificadora" type="hidden" />
    <jlis:value name="idSede" type="hidden" />
    <jlis:value name="coId" type="hidden" />
    <jlis:value name="idTupa" type="hidden" />
    <jlis:value name="idEntidad" type="hidden" />
    <jlis:value name="idFormato" type="hidden" />
    <jlis:value name="formato" type="hidden" />
    <jlis:value name="idFormatoEntidad" type="hidden" />
    <jlis:value name="tce" type="hidden" />
    <jlis:value name="orden" type="hidden" />
    <jlis:value name="mto" type="hidden" />
    <jlis:value name="suce" type="hidden" />
    <jlis:value name="bloqueado" type="hidden" />
    <jlis:value name="transmitido" type="hidden" />
    <jlis:value name="estadoRegistro" type="hidden" />
    <jlis:value name="puedeTransmitir" type="hidden" />
    <jlis:value name="puedeModificar" type="hidden" />
    <jlis:value name="cerrada" type="hidden" />
    <jlis:value name="suceCerrada" type="hidden" />
    <jlis:value name="tieneDr" type="hidden" />
    <jlis:value name="controller" type="hidden" />
    <jlis:value name="opcion" type="hidden" />
    <jlis:value name="vigente" type="hidden" />
    <jlis:value name="historico" type="hidden" />
    <jlis:value name="numRecargas" type="hidden" />
    <jlis:value name="pagActual" type="hidden" />
    <jlis:value name="certificadoOrigen" type="hidden" />
    <jlis:value name="certificadoReexportacion" type="hidden" />
	<jlis:value name="esDJRegistrada" type="hidden" />
    <jlis:value name="estadoAcuerdoPais" type="hidden" />
    <c:if test="${!empty mtoSUCE}">
    <jlis:value name="mtoSUCE" type="hidden" />
    <jlis:value name="modificacionSuceId" type="hidden" />
    <jlis:value name="mensajeId" type="hidden" />
    </c:if>
    <c:if test="${empty mtoSUCE}">
	<jlis:value type="hidden" name="calificacionUoId" />
    </c:if>
    <c:if test="${!empty numSuce && !empty mtoSUCE && utilizaModificacionSuceXMto == 'S'}">
    <jlis:value name="versionOriginal" type="hidden" />
    </c:if>

    <co:validacionBotonesFormato pagina="informacionGeneral" />
	<c:if test="${empty numSuce && mto > 1 && estadoRegistro == 'P'}">
    <div id="pageTitle">
        <h1><strong style="text-decoration:underline;">MODIFICACI�N DE SOLICITUD</strong></h1>
    </div>
	</c:if>
    <c:if test="${!empty numSuce && !empty mtoSUCE}">
    <div id="pageTitle">
        <h1><strong style="text-decoration:underline;">MODIFICACI�N DE SUCE</strong></h1>
    </div>
    </c:if>
    <div id="pageTitle">
        <h1><strong><jlis:value name="nombreFormato" editable="no" valueClass="pageTitle" /></strong></h1>
    </div>
    <jlis:messageArea width="100%" />

    <div class="topFieldset">
        <c:if test="${empty idFormatoEntidad}">
            <h3 class="psubtitle">
                <span>
                        <b><jlis:label key="co.title.datos_orden_inicial" /></b>
                </span>
            </h3>
            <jlis:value name="numOrden" type="hidden" />
        </c:if>

        <c:if test="${!empty numOrden and empty numSuce}">
            <h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_orden" /></b></span></h3>
            <br />
            <table>
                <tr>
                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.numero_orden" />&nbsp;&nbsp;</th>
                    <td><jlis:value name="numOrden" size="20" align="center" editable="no" type="text" /></td>
                    <td>&nbsp;</td>
                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.fecha_registro" />&nbsp;&nbsp;</th>
                    <td><jlis:value name="fechaRegistroOrden" size="20" editable="no" type="text" pattern="dd/MM/yyyy HH:mm:ss" align="center" /></td>
                    <td>&nbsp;</td>
                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.fecha_actualizacion" />&nbsp;&nbsp;</th>
                    <td><jlis:value name="fechaActualizacion" size="20" editable="no" type="text" pattern="dd/MM/yyyy HH:mm:ss" align="center" /></td>
                </tr>
            </table>
        </c:if>
     	<c:if test="${!empty numSuce}">
            <jlis:value name="numOrden" type="hidden" />
            <h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_suce" /></b></span></h3>
            <br/>
            <table>
                <tr>
                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.numero_suce" />&nbsp;&nbsp;</th>
                    <td><jlis:value name="numSuce" size="20" align="center" editable="no" type="text" /></td>
                    <td>&nbsp;</td>
                    <th style="font-weight: bold; font-size: 11px;"><jlis:label key="co.label.fecha_registro" />&nbsp;&nbsp;</th>
                    <td><jlis:value name="fechaRegistroSuce" size="20" editable="no" type="text" pattern="dd/MM/yyyy HH:mm:ss" align="center" /></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </c:if>
        </div>
		<c:if test="${sessionScope.USUARIO.tipoOrigen != 'ET'}">
	        <c:if test="${empty mtoSUCE}">
		        <div class="fbuttons">
		            <table align="left" width="100%">
		            	<c:if test="${!empty formato && ( formato == 'MCT005' || formato == 'mct005' ) && empty idFormatoEntidad}">
			                <tr>
			                    <td>
			                    	<table>
			                    		<tr>
			                    			<td>
						                    	<jlis:label key="co.label.calificacion.nueva" />
						                    	<jlis:value type="radio" name="tipoCalificacion" checkValue="1" defaultCheck="yes" onClick="changeTipoCalif(this)" />
											</td>
											<td>
												&nbsp;&nbsp;<jlis:label key="co.label.calificacion.copia.existente" />
												<jlis:value type="radio" name="tipoCalificacion" checkValue="2" defaultCheck="no" onClick="changeTipoCalif(this)" />
											</td>
										</tr>
									</table>
			                    </td>
			                </tr>
			                <tr><td>&nbsp;</td></tr>
		                </c:if>
		                <tr>
		                    <td>

		                    	<c:if test="${!empty formato && ( formato == 'MCT002' || formato == 'mct002' || formato == 'MCT004' || formato == 'mct004' ) }">
		                    		<jlis:value name="adjuntoIdFirma" type="hidden" />
		                    	</c:if>
		                        <c:if test="${empty idFormatoEntidad}">
		                        <jlis:button code="CO.USUARIO.OPERACION" id="nuevaButton" name="crearRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Guardar Formato " alt="Pulse aqu� para grabar el formato" />
		                        </c:if>
		                        <c:if test="${empty numSuce}">
		                        <jlis:button code="CO.USUARIO.OPERACION" id="transButton" name="transmitir()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para transmitir el formulario" />
		                        <%--<c:if test="${estadoRegistro == 'S'}">
		                        <jlis:button code="CO.USUARIO.OPERACION" id="modifButton" name="crearModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Crear Subsanaci�n" alt="Pulse aqu� para crear la subsanaci�n de la Solicitud" />
		                        </c:if>
		                        <c:if test="${estadoRegistro != 'S'}">
		                        <jlis:button code="CO.USUARIO.OPERACION" id="modifButton" name="crearModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Crear Modificaci�n" alt="Pulse aqu� para crear la modificaci�n de la Solicitud" />
		                        </c:if> --%>
		                        </c:if>
		                        <c:if test="${!empty numSuce || mto == 1 || (empty numSuce && mto > 1 && estadoRegistro != 'A' && estadoRegistro != 'B' )}">
		                        	<jlis:button code="CO.USUARIO.OPERACION" id="desisteButton" name="desistir()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Desistir Tr�mite" alt="Pulse aqu� para desistir el tr�mite" />
		                        </c:if>
		                        <%--<c:if test="${empty numSuce && mto > 1 && estadoRegistro == 'P'}">
		                        <jlis:button code="CO.USUARIO.OPERACION" id="cancelarModificacionOrdenButton" name="cancelarModificacionOrden()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cancelar Modificaci�n" alt="Pulse aqu� para cancelar la modificaci�n de Solicitud" />
		                        </c:if>
		                        <c:if test="${!empty numSuce}">
		                        <jlis:button editable="always" id="imprimirHojaResumenSUCEButton" name="imprimirHojaResumenSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Hoja Resumen de la SUCE" alt="Pulse aqu� para imprimir la Hoja Resumen de la SUCE" />
		                        </c:if>
		                        <c:if test="${!empty idFormatoEntidad}">
		                        <jlis:button editable="always" id="otraInformacionButton" name="otraInformacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="M�s Informaci�n" alt="Pulse aqu� para ver m�s informaci�n" />
		                        </c:if> --%>
			                    <c:if test="${!empty orden}">
			                    	<%-- <jlis:button editable="always" id="vistaPreviaSolicitudButton" name="imprimirHojaResumenAvance()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Vista Previa" alt="Pulse aqu� para generar una Vista Previa de la Solicitud" />--%>
							     <c:if test="${mto > 1 && !empty vigente && vigente != 'S'}">
							     	<jlis:button code="CO.USUARIO.OPERACION" id="eliminarSubsanacionButton" name="eliminarSubsanacionOrden()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar Borrador de Subsanaci�n" alt="Pulse aqu� para eliminar la Subsanaci�n de la Solicitud" />
							     </c:if>
			                    </c:if>
		                        <c:if test="${!empty idFormatoEntidad}">
			                        <jlis:button editable="always" id="otraInformacionButton" name="otraInformacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="M�s Informaci�n" alt="Pulse aqu� para ver m�s informaci�n" />
		                        </c:if>

		<%-- 					    <c:if test="${!empty idFormatoEntidad && (formato=='MCT005' || formato=='mct005')}">
		                              <jlis:button code="CO.USUARIO.OPERACION" id="DeclaracionJuradaButton" name="abrirDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Declaraci�n Jurada" alt="Pulse aqu� para crear o actualizar datos de la declaraci�n jurada" />
		                        </c:if> --%>
		                        <jlis:button id="regresarButton" editable="always" name="regresar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para regresar a la lista de Solicitudes y SUCEs" />
							</td>
		                    <c:if test="${!empty orden}">
								<td>
		                        	<c:choose>
		                        		<c:when test="${!empty formato && (formato == 'MCT001' || formato == 'mct001' || formato == 'MCT003' || formato == 'mct003')}">
		                        			<img id="imgVer" src="/co/imagenes/icono_pdf.png" onClick="imprimirHojaResumenAvance()" style="cursor: pointer;" height="25px" width="23px"  title="Pulse aqu� para generar una Vista Previa de la Solicitud" align="right"/>
				                    		&nbsp;
		                        		</c:when>
		                        		<c:when test="${!empty formato && ( formato != 'MCT005' && formato != 'mct005' ) }">
		                        			<img id="imgVer" src="/co/imagenes/icono_pdf.png" onClick="imprimirHojaResumenAvance()" style="cursor: pointer;" height="25px" width="23px"  title="Pulse aqu� para visualizar el certificado firmado de la solicitud original" align="right"/>
				                    	&nbsp;
		                        		</c:when>
		                        	</c:choose>
		                    	</td>
			                </c:if>
		                </tr>
		            </table>
		        </div>
	        </c:if>
        </c:if>
		<c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET'}">
	        <c:if test="${empty mtoSUCE}">
		        <div class="fbuttons">
		            <table align="left" width="100%">
		                <tr>
		                    <td align="right">
			                    <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR' && estadoRegistro == 'U'}">
				                    <jlis:button code="CO.ENTIDAD.SUPERVISOR" id="asignarButton" name="asignarEvaluador()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Asignar" alt="Pulse aqu� para asignar evaluador" />
				                    <%-- <c:if test="${!empty modificacionSuceXMts && modificacionSuceXMts =='S'}"> --%>
				                    <%-- 20170131_GBT ACTA CO-004-16 3.2.b Y ACTA CO 009-16--%>
				                    <c:if test="${!empty formato && formato != 'MCT004' && formato != 'mct004' && formato != 'MCT002' && formato != 'mct002' && estadoAcuerdoPais == 'I'}">
				                    	<jlis:button code="CO.ENTIDAD.SUPERVISOR" id="nuevaNotificacionSubsanacionButton" name="estadoAcuerdoPaisInactivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Nueva Notificaci�n" alt="Pulse aqu� para agregar una Nueva Notificaci�n de Subsanaci�n" />
				                    </c:if>
				                    <c:if test="${!empty formato && formato != 'MCT004' && formato != 'mct004' && formato != 'MCT002' && formato != 'mct002' && estadoAcuerdoPais != 'I' }">
				                    	<jlis:button code="CO.ENTIDAD.SUPERVISOR" id="nuevaNotificacionSubsanacionButton" name="nuevaNotificacionSubsanacionOrden()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Nueva Notificaci�n" alt="Pulse aqu� para agregar una Nueva Notificaci�n de Subsanaci�n" />
				                    </c:if>
				                    <%-- </c:if> --%>
				                    <%-- <jlis:button code="CO.ENTIDAD.SUPERVISOR" id="denegarButton" name="denegarSolicitud()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Denegar" alt="Pulse aqu� para denegar la solicitud" /> --%>
			                    </c:if>
			                    <%-- <c:if test="${!empty idFormatoEntidad && (formato=='MCT005' || formato=='mct005')}">
                              		<jlis:button code="CO.USUARIO.OPERACION" id="DeclaracionJuradaButton" name="abrirDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Declaraci�n Jurada" alt="Pulse aqu� para crear o actualizar datos de la declaraci�n jurada" />
                        		</c:if> --%>
		                        <jlis:button id="regresarButton" editable="always" name="listarSolicitudesCertificadoEntidad(1)" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para regresar a la lista de Certificados" />
			                    <%-- <jlis:button id="regresarButton" editable="always" name="regresar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para regresar a la lista de Certificados" /> --%>
			                    <c:if test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR' || sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.SUPERVISOR'|| sessionScope.USUARIO.rolActivo == 'CO.ADMIN.HELP_DESK'}">
			                    	<c:if test="${!empty orden}">
			                    		<%-- 20140620_JMC Se agrega por BUG 127 --%>
			                        	<c:if test="${!empty formato &&
			                        				  ( (formato == 'MCT001' || formato == 'mct001')  ||  (formato == 'MCT003' || formato == 'mct003') )}">
			                        			<img id="imgVer" src="/co/imagenes/icono_pdf.png" onClick="imprimirHojaResumenAvance()" style="cursor: pointer;" height="25px" width="23px"  title="Pulse aqu� para generar una Vista Previa de la SUCE" align="right"/>
					                    		&nbsp;
		                        		</c:if>
				                    </c:if>
			                    	<c:if test="${!empty por_evaluacion}">
			                        	<c:if test="${!empty numSuce && estadoRegistro == 'D'}">
					                    	<jlis:button id="aceptarDesestimientoSUCEButton" name="aceptarDesestimientoSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Aceptar Desistimiento" alt="Pulse aqu� para aceptar el desistimiento de la SUCE" />
					                    	<jlis:button id="rechazarDesestimientoSUCEButton" name="rechazarDesestimientoSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Rechazar Desistimiento" alt="Pulse aqu� para rechazar el desistimiento de la SUCE" />
										</c:if>
			                        	<jlis:button id="datosSUCEButton" editable="always" name="verDatosCertificado()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Datos de la SUCE" alt="Pulse aqu� para cargar los datos de la SUCE" />
									</c:if>
			                    </c:if>
		                    </td>
		                </tr>
		            </table>
		        </div>
	        </c:if>
        </c:if>
        <c:if test="${!empty numSuce && !empty mtoSUCE && utilizaModificacionSuceXMto == 'S'}">
        <div class="fbuttons">
            <table align="left" width="100%">
                <!-- <tr>
                    <td>
                    	numSuce: ${numSuce}
                    	</br>
                    	mtoSuce: ${mtoSUCE}
                    	</br>
                    	utilizaModificacionSuceXMto: ${utilizaModificacionSuceXMto}
                    	</br>
                    	vigente: ${vigente}
                    </td>
                </tr>  -->
                <tr>
                    <td>
                        <c:if test="${vigente == 'N'}" >
	                        <jlis:button code="CO.USUARIO.OPERACION" id="transButton" name="transmiteModificacionSuceMTO()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para transmitir el formulario" />
	                        <c:if test="${transmitido == 'N'}" >
	                        	<jlis:button code="CO.USUARIO.OPERACION" id="cancelarModificacionSUCEButton" name="cancelarModificacionSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cancelar Modificaci�n" alt="Pulse aqu� para desistir la modificaci�n de SUCE" />
	                        </c:if>
                        </c:if>
					    <%-- <c:if test="${!empty idFormatoEntidad && (formato=='MCT005' || formato=='mct005')}">
                              <jlis:button code="CO.USUARIO.OPERACION" id="DeclaracionJuradaButton" name="abrirDJ()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Declaraci�n Jurada" alt="Pulse aqu� para crear o actualizar datos de la declaraci�n jurada" />
                        </c:if> --%>
                        <c:choose>
                        	<c:when test="${sessionScope.USUARIO.rolActivo == 'CO.ENTIDAD.EVALUADOR'}">
                        		<jlis:button id="regresarButton" editable="always" name="listarSolicitudesCertificadoEntidad(1)" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar" alt="Pulse aqu� para regresar a la lista de Certificados" />
                        	</c:when>
                        	<c:otherwise>
                        		<jlis:button id="regresarButton" editable="always" name="regresarSuceOriginal()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Regresar a SUCE Original" alt="Pulse aqu� para regresar a la SUCE original" />
                        	</c:otherwise>
                        </c:choose>
                        <%-- 20140620_JMC Se agrega por BUG 127 --%>
						<td>
                       		<c:if test="${!empty formato && (formato == 'MCT001' || formato == 'mct001' || formato == 'MCT003' || formato == 'mct003')}">
                       			<img id="imgVer" src="/co/imagenes/icono_pdf.png" onClick="imprimirHojaResumenAvance()" style="cursor: pointer;" height="25px" width="23px"  title="Pulse aqu� para generar una Vista Previa de la Solicitud" align="right"/>
	                    		&nbsp;
                       		</c:if>
                    	</td>			           
                    </td>
                </tr>
            </table>
        </div>
        </c:if>
    <br/>
