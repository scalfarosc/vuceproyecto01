<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Busqueda de Certificados de Origen</title>
</head>
<script type="text/javascript">

	$(document).ready(function() {
		if ($.trim($("#pais").val()) != '') {
			cargarlistaAcuerdos(document.getElementById("pais"), '${acuerdo}');
      	}
	
  	});

    function retornar(keyValues, keyValuesField, row){
        var f = window.parent.document;

        var fecGeneracion = document.getElementById(keyValues.split('|')[14]).value;

        f.getElementById("nombreAcuerdo").value = document.getElementById(keyValues.split('|')[3]).value;
        f.getElementById("drEntidad").value = document.getElementById(keyValues.split('|')[1]).value;
        f.getElementById("nombrePais").value = document.getElementById(keyValues.split('|')[4]).value;
        f.getElementById("drOrigen").value = document.getElementById(keyValues.split('|')[0]).value;
        f.getElementById("nombreEntidad").value = document.getElementById(keyValues.split('|')[7]).value;
        f.getElementById("fechaGeneracion").value = (fecGeneracion.length > 10 ? fecGeneracion.substr(0, 10) : fecGeneracion);
        f.getElementById("idAcuerdo").value = document.getElementById(keyValues.split('|')[5]).value;
        f.getElementById("idPais").value = document.getElementById(keyValues.split('|')[6]).value;
        f.getElementById("idEntidadCertificadora").value = document.getElementById(keyValues.split('|')[8]).value;
        f.getElementById("idSede").value = document.getElementById(keyValues.split('|')[9]).value;
        f.getElementById("drIdOrigen").value = document.getElementById(keyValues.split('|')[10]).value;
        f.getElementById("sdrOrigen").value = document.getElementById(keyValues.split('|')[11]).value;
        
        if (f.getElementById("nombreSede") != undefined) {
          f.getElementById("nombreSede").value = document.getElementById(keyValues.split('|')[12]).value;
        }

        if (f.getElementById("MCT003.observacionEvalOrigen") != undefined) {
          f.getElementById("MCT003.observacionEvalOrigen").value = document.getElementById(keyValues.split('|')[13]).value;
        }
        
        var cer = document.getElementById(keyValues.split('|')[15]).value;
        f.getElementById("certificadoOrigen").value = (cer=="N" ? "S" : "N");
        f.getElementById("certificadoReexportacion").value = (cer=="S" ? "S" : "N");
        f.getElementById("tipoCertificado").value = (cer=="N" ? "Origen" : "Reexportaci�n");
        
        window.parent.cargarInformacion();
        window.parent.hidePopWin(false);
     }


    function validarSubmit() {
    var f = document.formulario;
    if (f.button.value=="cancelarButton") {
      return false;
        }
    if (f.button.value=="cerrarPopUpButton") {
            return validaCerraPopUpNoAction();
        }
    return true;
  }

  var idAcuerdo = '';
  function cargarlistaAcuerdos(obj, acuerdoId) {
    var f = document.formulario;
    //var filter = obj.value;
    //loadSelectAJX("co.ajax.certificado_origen.acuerdos_pais.selectLoader", "obtenerListadoAcuerdos", "select_acuerdo", null, filter, null, null, null);
    var filter ="pais="+obj.value;

    if (acuerdoId != undefined) {
    	idAcuerdo = acuerdoId;
    } else {
    	idAcuerdo = '';
    }

      loadSelectAJX("ajax.selectLoader", "loadList", "acuerdo", "certificado_origen.acuerdos_pais.select", filter, null, afterCargarlistaAcuerdos, null);
  }

  function afterCargarlistaAcuerdos() {
	  var f = document.formulario;
	  
	  $("#acuerdo").val(idAcuerdo);
	  validaAcuerdo(f.acuerdo);
	  
  }

  function buscar(){
     var f = document.formulario;
     if(validaCampos()) {
       f.action = contextName+"/origen.htm";
       f.method.value="cargarBusquedaCO";
       f.button.value="buscarButton";
     } else {
       f.button.value="cancelarButton";
     }
  }

    function validaCampos() {
      var f = document.formulario;
      if (f.desde.value=="" && f.hasta.value!="") {
        alert("Debe ingresar la Fecha desde");
        return false;
      }
      if (f.desde.value!="" && f.hasta.value=="") {
        alert("Debe ingresar la Fecha hasta");
        return false;
      }
      if (!validarFechasDiferencia(f.desde,f.hasta,true,2,"ME")) {
        return false;
      }
      return true;
    }

    function validaAcuerdo(obj) {
        var filter = obj.value;
        document.getElementById("aceptaReexportacion").value = "";
        loadElementAJX("co.ajax.certificado.aceptaReexportacion.elementLoader", "aceptaReexportacion", null, filter, null, afterValidaAcuerdo);
    }
    
    function afterValidaAcuerdo(){
    	var f = document.formulario;
        var aceptaReexportacion = document.getElementById("aceptaReexportacion").value;
        //inicializaTipoCertificado();
        if (aceptaReexportacion == "S") {
            $("#tr_tipoCertificado").show();
            if (f.tipoCertificado.value=="") {
            	$('input:radio[name=tipoCertificado]')[0].checked = true;
            }
        } else {
            $("#tr_tipoCertificado").hide();
            //document.getElementById("tipoCertificado").value = "";
            $('input:radio[name="tipoCertificado"]').removeAttr('checked');
        }
    }
    
    function inicializaTipoCertificado() {
        $("#tr_tipoCertificado").hide();
        $('input:radio[name=tipoCertificado]')[0].checked = true;
    }
    
</script>
<body id="contModal">
<jlis:messageArea width="100%" />
<form name="formulario" method="post" onSubmit="return validarSubmit();">
  	<input type="hidden" name="method" value=""/>
  	<input type="hidden" name="button" value="" />
	<jlis:value type="hidden" name="formato" />
	<jlis:value type="hidden" name="aceptaReexportacion" />

  <table class="form">
    <tr>
            <th><jlis:label key="co.label.co_busqueda.dr" /></th>
        <td><jlis:value name="dr" size="20" onBlur="validarEnteroPositivo(this,false)" type="text" /></td>
    </tr>
    <tr>
            <th><jlis:label key="co.label.co_busqueda.drEntidad" /></th>
      <td><jlis:value name="drEntidad" size="20" type="text" /></td>
    </tr>
    <tr>
            <th><jlis:label key="co.label.co_busqueda.fechaDesde" /></th>
      <td><jlis:date name="desde" form="formulario" /><jlis:label key="co.label.co_busqueda.fechaHasta" /><jlis:date name="hasta" form="formulario" /></td>
    </tr>
    <tr>
            <th><jlis:label key="co.label.co_busqueda.pais_acuerdo" /></th>
      <td><jlis:selectProperty key="comun.pais_iso.acuerdo_vigente.select" name="pais" style="align:right;" onChange="cargarlistaAcuerdos(this);" /></td>
    </tr>
    <tr>
            <th><jlis:label key="co.label.co_busqueda.acuerdo" /></th>
      <td><jlis:selectProperty key="certificado_origen.acuerdos_pais.select" filter="${filterPais}" name="acuerdo" style="align:right;" onChange="validaAcuerdo(this);" /></td>
    </tr>
        <!--tr>
            <th></th>
      <td><span id="spNotaAP" style="height: 30px; font-size: 12px; color:red">Nota: este procedimiento no est� habilitado para Certificados de Origen que fueron emitidos con Firma Digital.</span></td>
    </tr-->
    
    <tr id="tr_tipoCertificado" style="display:none">
      <td colspan="2">
          <jlis:value type="radio" name="tipoCertificado" checkValue="1" onClick="seleccionTipoCertificado()" /><jlis:label key="co.label.certificado_origen" style="font-weight: bold; align:left;" />&nbsp;
          <jlis:value type="radio" name="tipoCertificado" checkValue="2" onClick="seleccionTipoCertificado()" /><jlis:label key="co.label.certificado_reexportacion" style="font-weight: bold; align:left;" />
      </td>
    </tr>
    <tr>
        <td>
            <jlis:button id="buscarButton" name="buscar()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Buscar CO" alt="Pulse aqu� para iniciar la busqueda" />
            <jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
        </td>
        <td>
        <c:if test="${!empty formato && ( formato == 'MCT002' || formato == 'mct002' )}">
        <span style="color:dodgerblue;">
        Nota: En el Acuerdo Marco de la Alianza del Pac�fico. este procedimiento no est� habilitado para Certificados de Origen que fueron emitidos con Firma Digital
        </c:if></span>
        </td>
    </tr>
  </table>
  <br/>
        <jlis:table keyValueColumns="dr_origen,dr_entidad,fecha_vigencia,nombre_acuerdo,nombre_pais,acuerdo_internacional_id,pais_iso_id_x_acuerdo_int,nombre_entidad,entidad_id,sede_id,dr_id,sdr,sede_nombre,observacion_evaluador,fecha_registro,certificado_reexportacion" 
                    name="CO" source="tCertificadoOrigen" scope="request" pageSize="14" fixedHeader="yes" >
                <jlis:tr>
                    <jlis:td name="FORMATO" nowrap="yes" />
                    <jlis:td name="DR" nowrap="yes" />
                    <jlis:td name="CERTIFICADO ORIGEN" nowrap="yes"  />
                    <jlis:td name="USUARIO_ID" nowrap="yes"  />
                    <jlis:td name="DR_ID" nowrap="yes"  />
                    <jlis:td name="SDR" nowrap="yes"  />
                    <jlis:td name="FECHA VIGENCIA" nowrap="yes"  />
                    <jlis:td name="PAIS ACUERDO" nowrap="yes"  />
                    <jlis:td name="ACUERDO COMERCIAL" nowrap="yes"  />
                    <jlis:td name="ACUERDO_INTERNACIONAL_ID" nowrap="yes"  />
                    <jlis:td name="PAIS_ISO_ID_X_ACUERDO_INT" nowrap="yes"  />
                    <jlis:td name="ENTIDAD CERTIFICADORA" nowrap="yes"  />
                    <jlis:td name="ENTIDAD_ID" nowrap="yes"  />
                    <jlis:td name="SEDE_ID" nowrap="yes"  />
                    <jlis:td name="SEDE_NOMBRE" nowrap="yes"  />
                    <jlis:td name="OBSERVACION_EVALUADOR" nowrap="yes"  />
                    <jlis:td name="FECHA REGISTRO" nowrap="yes"  />
                    <jlis:td name="CERTIFICADO_REEXPORTACION" nowrap="yes" />
                    <jlis:td name="TIPO CERTIFICADO" nowrap="yes" />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="FORMATO"  align="center" />
                <jlis:columnStyle column="2" columnName="DR_ORIGEN" align="center" />
                <jlis:columnStyle column="3" columnName="DR_ENTIDAD" editable="yes" align="center" />
                <jlis:columnStyle column="4" columnName="USUARIO_ID" hide="yes" />
                <jlis:columnStyle column="5" columnName="DR_ID"  align="center" hide="yes"/>
                <jlis:columnStyle column="6" columnName="SDR"  align="center" hide="yes"/>
                <jlis:columnStyle column="7" columnName="FECHA_VIGENCIA"  align="center" type="dateTime"  pattern="dd/MM/yyyy" />
                <jlis:columnStyle column="8" columnName="NOMBRE_PAIS"  align="center" />
                <jlis:columnStyle column="9" columnName="NOMBRE_ACUERDO"  align="center" />
                <jlis:columnStyle column="10" columnName="ACUERDO_INTERNACIONAL_ID" align="center" hide="yes"/>
                <jlis:columnStyle column="11" columnName="PAIS_ISO_ID_X_ACUERDO_INT"  align="center" hide="yes"/>
                <jlis:columnStyle column="12" columnName="NOMBRE_ENTIDAD"  align="center"/>
                <jlis:columnStyle column="13" columnName="ENTIDAD_ID"  align="center" hide="yes"/>
                <jlis:columnStyle column="14" columnName="SEDE_ID"  align="center" hide="yes"/>
                <jlis:columnStyle column="15" columnName="SEDE_NOMBRE"  align="center" hide="yes"/>
                <jlis:columnStyle column="16" columnName="OBSERVACION_EVALUADOR"  align="center" hide="yes"/>
                <jlis:columnStyle column="17" columnName="FECHA_REGISTRO"  align="center" type="dateTime"  pattern="dd/MM/yyyy HH:mm" />
                <jlis:columnStyle column="18" columnName="CERTIFICADO_REEXPORTACION" align="center" hide="yes" />
                <jlis:columnStyle column="19" columnName="TIPO_CERTIFICADO" align="center" />
                <jlis:tableButton column="3" type="link" onClick="retornar" />
            </jlis:table>
</form>
    <script>
        window.parent.document.getElementById("popupTitle").innerHTML = 'B�squeda de Certificados de Origen';
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";

        $("#descripcion").focus();
    </script>
</body>
</html>
