<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/tags/jlis-framework" prefix="jlis" %>
<%@taglib uri="/tags/co-mincetur" prefix="co"%>

<jlis:value name="SOLICITANTE.usuarioId" type="hidden" />

<c:choose>
      <c:when test="${tipoPersona == '2'}">
      		<table class="form">
				<tr>
					<td colspan="8"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_solicitante"  /></b></span></h3></td>
				</tr>

				<tr>
					<th><jlis:label key="co.label.persona_tipo" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.tipoPersona" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.tipo_documento" /></th>
					<td><jlis:value name="SOLICITANTE.documentoTipoDesc" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.numero_documento" /></th>
					<td><jlis:value name="SOLICITANTE.numeroDocumento" editable="no" /></td>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.razon_social" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.nombre" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.domicilio_legal" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.direccion" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.departamento" /></th>
					<td colspan="7"><jlis:value name="departamento" editable="no" /><jlis:value type="hidden" name="departamentoId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.provincia" /></th>
					<td colspan="7"><jlis:value name="provincia" editable="no" /><jlis:value type="hidden" name="provinciaId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.distrito" /></th>
					<td colspan="7"><jlis:value name="distrito" editable="no" /><jlis:value type="hidden" name="distritoId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.telefono" /></th>
					<td><jlis:value name="SOLICITANTE.telefono" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.celular" /></th>
					<td><jlis:value name="SOLICITANTE.celular" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.fax" /></th>
					<td colspan="2"><jlis:value name="SOLICITANTE.fax" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.email" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.email" editable="no" /></td>
				</tr>				
			</table>
			
			<div id="divDireccionAdicional">
			<%-- Direccion Adicional --%>
				<c:if test="${!empty idFormatoEntidad}">
					<table class="form">
						   <tr><td colspan="7"><hr/></td></tr>
			               <tr>
			                   <th><jlis:label key="co.title.direccionAdicional" /></th>
			                   <td colspan="7">
			                       <jlis:selectSource name="listaDireccionesAdicionales" style="width:92%" source="listaDireccionesEA" scope="request" onChange="seleccionDireccionAdicional()" />			                       
			                   </td>
			               </tr>		              
			               <tr>
			                   <th><jlis:label key="co.label.direccion" /></th>
			                   <td colspan="7">
									<c:if test="${!empty formato && ( formato == 'MCT001' || formato == 'mct001' || formato == 'MCT003' || formato == 'mct003' || formato == 'MCT005' || formato == 'mct005') }">
				                        <jlis:value name="CERTIFICADO_ORIGEN.direccionAdicional" style="width:92%" maxLength="250" /><span class="requiredValueClass">(*)</span>		                       
										<jlis:button code="CO.USUARIO.OPERACION" id="actualizarDireccionAdicionalButton" name="actualizarDireccionAdicional()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar el Direcci�n" />
										<co:mostrarAyuda etiqueta="MCT001.SUCE.DATOS_TITULAR.TITULAR_DIRECCION" />
				            		</c:if>
			                   		<c:if test="${!empty formato && ( formato == 'MCT002' || formato == 'mct002' || formato == 'MCT004' || formato == 'mct004' ) }">
				                        <jlis:value name="CERTIFICADO_ORIGEN.direccionAdicional" style="width:92%" maxLength="250" editable="no" type="text"/><span class="requiredValueClass">(*)</span>
				                        <co:mostrarAyuda etiqueta="MCT001.SUCE.DATOS_TITULAR.TITULAR_DIRECCION" />
				            		</c:if>		                   
				            	</td>
			               </tr>
				
					</table>
				</c:if>	  
			</div>
            <jsp:include page="/WEB-INF/jsp/representante.jsp" />
  	    </c:when>
  	    
  	    
  		<c:otherwise>
		
    		<table class="form">
				<tr>
					<td colspan="8"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_solicitante"  /></b></span></h3></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.persona_tipo" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.tipoPersona" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.tipo_documento" /></th>
					<td><jlis:value name="SOLICITANTE.documentoTipoDesc" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.numero_documento" /></th>
					<td><jlis:value name="SOLICITANTE.numeroDocumento" editable="no" /></td>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.nombres_apellidos" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.nombre" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.domicilio_legal" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.direccion" editable="no" /></td>
				</tr>
				<tr >
					<th><jlis:label key="co.label.departamento" /></th>
					<td colspan="7"><jlis:value name="departamento" editable="no" /><jlis:value type="hidden" name="departamentoId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.provincia" /></th>
					<td colspan="7"><jlis:value name="provincia" editable="no" /><jlis:value type="hidden" name="provinciaId" /></td>
				</tr>

				<tr >
					<th><jlis:label key="co.label.distrito" /></th>
					<td colspan="7"><jlis:value name="distrito" editable="no" /><jlis:value type="hidden" name="distritoId" /></td>
				</tr>
				<!--tr >
					<th><jlis:label key="co.label.referencia" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.referencia" editable="no" /></td>
				</tr-->
				<tr>
					<th><jlis:label key="co.label.telefono" /></th>
					<td><jlis:value name="SOLICITANTE.telefono" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.celular" /></th>
					<td><jlis:value name="SOLICITANTE.celular" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.fax" /></th>
					<td colspan="3"><jlis:value name="SOLICITANTE.fax" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.email" /></th>
					<td colspan="7"><jlis:value name="SOLICITANTE.email" editable="no" /></td>
				</tr>
				
			</table>
			
			<div id="divDireccionAdicional">
			<%-- Direccion Adicional --%>
				<c:if test="${!empty idFormatoEntidad}">
					<table class="form">
						   <tr><td colspan="7"><hr/></td></tr>
			               <tr>
			                   <th><jlis:label key="co.title.direccionAdicional" /></th>
			                   <td colspan="7">
			                       <jlis:selectSource name="listaDireccionesAdicionales" style="width:92%" source="listaDireccionesEA" scope="request" onChange="seleccionDireccionAdicional()" />
			                   </td>
			               </tr>		              
			               <tr>
			                   <th><jlis:label key="co.label.direccion" /></th>
			                   <td colspan="7">
									<c:if test="${!empty formato && ( formato == 'MCT001' || formato == 'mct001' || formato == 'MCT003' || formato == 'mct003' || formato == 'MCT005' || formato == 'mct005') }">
				                        <jlis:value name="CERTIFICADO_ORIGEN.direccionAdicional" style="width:92%" maxLength="250" /><span class="requiredValueClass">(*)</span>		                       
										<jlis:button code="CO.USUARIO.OPERACION" id="actualizarDireccionAdicionalButton" name="actualizarDireccionAdicional()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar el Direcci�n" />
										<co:mostrarAyuda etiqueta="MCT001.SUCE.DATOS_TITULAR.TITULAR_DIRECCION" />
				            		</c:if>
			                   		<c:if test="${!empty formato && ( formato == 'MCT002' || formato == 'mct002' || formato == 'MCT004' || formato == 'mct004' ) }">
				                        <jlis:value name="CERTIFICADO_ORIGEN.direccionAdicional" style="width:92%" maxLength="250" editable="no" type="text"/><span class="requiredValueClass">(*)</span>
				                        <co:mostrarAyuda etiqueta="MCT001.SUCE.DATOS_TITULAR.TITULAR_DIRECCION" />
				            		</c:if>		                   
				            	</td>
			               </tr>
				
					</table>
				</c:if>	  
			</div>	
 		</c:otherwise>
</c:choose>

    		<table class="form">
				<tr>
					<td colspan="8"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_declarante" /></b></span></h3></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.tipo_documento" /></th>
					<td>
						<c:choose>
							<c:when test="${!empty requestScope['DECLARANTE.documentoTipoSecDesc']}">
								<jlis:value name="DECLARANTE.documentoTipoSecDesc" editable="no" />
							</c:when>
							<c:otherwise>
								<jlis:value name="DECLARANTE.documentoTipoDesc" editable="no" />
							</c:otherwise>
						</c:choose>
					</td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.numero_documento" /></th>
					<td>
						<c:choose>
							<c:when test="${!empty requestScope['DECLARANTE.numeroDocumentoSec']}">
								<jlis:value name="DECLARANTE.numeroDocumentoSec" editable="no" />
							</c:when>
							<c:otherwise>
								<jlis:value name="DECLARANTE.numeroDocumento" editable="no" />
							</c:otherwise>
						</c:choose>
					</td>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.nombres_apellidos" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.nombre" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.domicilio_legal" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.direccion" editable="no" /></td>
				</tr>
				<tr >
					<th><jlis:label key="co.label.departamento" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.departamento" editable="no" /><jlis:value type="hidden" name="DECLARANTE.departamentoId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.provincia" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.provincia" editable="no" /><jlis:value type="hidden" name="DECLARANTE.provinciaId" /></td>
				</tr>

				<tr >
					<th><jlis:label key="co.label.distrito" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.distrito" editable="no" /><jlis:value type="hidden" name="DECLARANTE.distritoId" /></td>
				</tr>
				<!--tr >
					<th><jlis:label key="co.label.referencia" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.referencia" editable="no" /></td>
				</tr-->
				<tr>
					<th><jlis:label key="co.label.telefono" /></th>
					<td><jlis:value name="DECLARANTE.telefono" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.celular" /></th>
					<td><jlis:value name="DECLARANTE.celular" editable="no" /></td>
					<td>&nbsp;</td>
					<th><jlis:label key="co.label.fax" /></th>
					<td colspan="2"><jlis:value name="DECLARANTE.fax" editable="no" /></td>
				</tr>
				<tr >
					<th><jlis:label key="co.label.email" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.email" editable="no" /></td>
				</tr>
				<tr >
					<th><jlis:label key="co.label.cargo" /></th>
					<td colspan="7"><jlis:value name="DECLARANTE.cargo" editable="no" /></td>
				</tr>
			</table>

<c:if test="${empresa == 'S'}">
            <jlis:value name="EMPRESA.id" type="hidden" />
            <table class="form">
				<tr>
					<td colspan="4"><h3 class="psubtitle"><span><b><jlis:label key="co.title.datos_empresa_externa"  /></b></span></h3></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.tipo_documento" /></th>
					<td><jlis:value name="EMPRESA.tipoDocDesc" editable="no" /></td>
					<th><jlis:label key="co.label.numero_documento" /></th>
					<td><jlis:value name="EMPRESA.numdoc" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.razon_social" /></th>
					<td colspan="3"><jlis:value name="EMPRESA.nombre" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.domicilio_legal" /></th>
					<td colspan="3"><jlis:value name="EMPRESA.direccion" editable="no" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.departamento" /></th>
					<td colspan="3"><jlis:value name="EMPRESA.departamento" editable="no" /><jlis:value type="hidden" name="departamentoId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.provincia" /></th>
					<td colspan="3"><jlis:value name="EMPRESA.provincia" editable="no" /><jlis:value type="hidden" name="provinciaId" /></td>
				</tr>
				<tr>
					<th><jlis:label key="co.label.distrito" /></th>
					<td colspan="3"><jlis:value name="EMPRESA.distrito" editable="no" /><jlis:value type="hidden" name="distritoId" /></td>
				</tr>
            </table>

</c:if>






