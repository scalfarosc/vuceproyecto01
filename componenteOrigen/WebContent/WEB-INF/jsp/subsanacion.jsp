<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
    String controller = (String)request.getAttribute("controller");
%>
<script language="JavaScript" src="<%=contextName%>/resource/js/subModal/subModal.js"></script>
<script type="text/javascript">

        var contador = 0;
        
        $(document).ready(function() {
        	
    	
            var id = document.getElementById("modificacionSuceId").value;
            if (id!="") {
                var transmitido = document.getElementById("transmitido").value;
                var bloqueada = document.getElementById("bloqueada").value;
                var estadoRegistro = document.getElementById("estadoRegistro").value;

                if (transmitido == "S" || bloqueada == "S" || estadoRegistro == 'I') {
                    bloquearControles();
                }
            }
            
        });
        
        function validarSubmit() {
            var f = document.formulario;
            
            
            if (f.button.value=="cancelarButton") {
                return false;
            }
            if (f.button.value=="cerrarPopUpButton") {
                return validaCerraPopUp();
            }
            if (f.button.value=="crearModifButton") {
                <c:if test="${tipo == 'S' && estadoRegistroSUCE == 'S'}">
                if (!validarSeleccionNotificacion()) {
                    return false;
                }
                </c:if>
                disableButton("crearModifButton", true);
            }
            if (f.button.value=="actualizarModifButton") {
                <c:if test="${tipo == 'S' && estadoRegistroSUCE == 'S'}">
                if (!validarSeleccionNotificacion()) {
                    return false;
                }
                </c:if>
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
            }
            if (f.button.value=="transmitirModifButton") {
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
            }
            if (f.button.value=="eliminarModifButton") {
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
            }
            if (f.button.value=="eliminarButton") {
                if (!confirm("�Seguro que desea eliminar el archivo?")) {
                    return false;
                }
            }
            if (f.button.value=="abrirModificacionSUCEButton") {
                disableButton("actualizarModifButton", true);
                disableButton("transmitirModifButton", true);
                disableButton("eliminarModifButton", true);
                disableButton("abrirModificacionSUCEButton", true);
            }
            return true;
        }

        function validarSeleccionNotificacion() {
        	var tableSize = parseInt(document.getElementById("tableSize_notificaciones").value);
            var ok = false;
            for (var i=1; i <= tableSize; i++) {
                ok = ok || document.getElementById("tableCheckBox_"+i+",notificaciones.SELECCIONE").checked;
            }
            if (!ok) {
                alert("Debe seleccionar una notificaci�n");
            }
            return ok;
        }

        function validarCampos(){
            var f = document.formulario;
            var ok = true;
            var mensaje="Debe ingresar : ";

            if (f.mensaje.value==""){
                mensaje +="\n -El mensaje";
                changeStyleClass("co.label.buzon.mensaje", "errorValueClass");
            }else changeStyleClass("co.label.buzon.mensaje", "labelClass");

            if(mensaje!="Debe ingresar : ") {
                ok= false;
                alert (mensaje);
            }
            return ok;
        }

        function guardarModificacion(){
            var f = document.formulario;
            //alert('estoy en guardarmodificacion')
            if (validarCampos()) {
                f.action = contextName+"/origen.htm";
                f.method.value="guardarSubsanacion";
                f.button.value="crearModifButton";
            }else{
                f.button.value="cancelarButton";
            }
            f.target = "_self";
        }

        function actualizarModificacion(){
            var f = document.formulario;
            f.action = contextName+"/origen.htm";
            f.method.value="actualizaSubsanacion";
            f.button.value="actualizarModifButton";
            f.target = "_self";
        }

        function eliminarModificacion(){
            var f = document.formulario;
            if (!confirm("�Est� seguro de desistir la Modificaci�n de SUCE?")) {
                f.button.value="cancelarButton";
               } else{
                   f.action = contextName+"/origen.htm";
                f.method.value="eliminarSubsanacion";
                f.button.value="eliminarModifButton";
                f.target = window.parent.name;
                window.parent.hidePopWin(false);
           }
        }

        function transmitirModificacion(){
            var f = document.formulario;
            f.action = contextName+"/origen.htm";
            f.method.value="transmiteSubsanacion";
            f.button.value="transmitirModifButton";
            f.target = "_self";
        }

        function cargarArchivo() {
            var f = document.formulario;
            if(f.archivo.value !=""){
                f.action = contextName+"/origen.htm";
                f.method.value="cargarArchivoSubsanacion";
                f.button.value="cargarButton";
            }else {
                alert("Debe Seleccionar alg�n Archivo");
                f.button.value="cancelarButton";
            }
            f.target = "_self";
        }

        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/origen.htm";
            f.method.value="descargarAdjunto";
            f.idAdjunto.value=adjuntoId;
            f.target = "_self";
            f.submit();
        }

        function cargarNotificacionDetalle(keyValues, keyValuesField) {
            var idNotificacion = document.getElementById(keyValues.split("|")[0]).value;
            var f = document.formulario;
            showPopWin(contextName+"/<%=controller%>?method=cargarNotificacionDetalle&idNotificacion="+idNotificacion, 600, 450, null);
        }

        function cargarNotificacionAdjuntos(keyValues, keyValuesField) {
            var idNotificacion = document.getElementById(keyValues.split("|")[0]).value;
            var f = document.formulario;
            showPopWin(contextName+"/origen.htm?method=cargarNotificacionAdjuntos&idNotificacion="+idNotificacion, 600, 450, null);
        }

        function eliminarRegistro() {
         var f = document.formulario;
         var indice = -1;
         if (f.seleccione!=null) {
             if (eval(f.seleccione.length)) {
                 for (i=0; i < f.seleccione.length; i++) {
                     if (f.seleccione[i].checked){
                         indice = i;
                     }
                 }
             } else {
                 if (f.seleccione.checked) {
                     indice = 0;
                 }
             }
         }
         if (indice!=-1) {
             f.action = contextName+"/origen.htm";
             f.method.value="eliminarAdjuntoSubsanacion";
             f.button.value="eliminarButton";
             f.target = "_self";
         } else {
             alert('Seleccione por lo menos un registro a eliminar');
             f.button.value = 'cancelarButton';
         }
     }

     function abrirModificacionSUCE() {
    	 var f = document.formulario;
    	 f.action = contextName+"/origen.htm";
         f.method.value="cargarInformacionOrden";
         f.button.value="abrirModificacionSUCEButton";
         //alert(f.mtoSUCE.value);
		 //alert(f.formato.value);

         f.mto.value = f.mtoSUCE.value;
         f.target = window.parent.name;
     }

     $(document).ready(function() {
         $('textarea').focus(function() {
             $(this).removeClass("inputTextClass").addClass("focusField");
         });
         $('textarea').blur(function() {
             if(this.value == this.defaultValue){
                 $(this).removeClass("focusField").addClass("inputTextClass");
             }
         });
     });

</script>

<body id="contModal">
    <jlis:messageArea width="100%" /><br/>
    <jlis:modalWindow />
    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
        <input type="hidden" name="idAdjunto">
        <jlis:value name="suce" type="hidden" />
        <jlis:value name="orden" type="hidden" />
        <jlis:value name="mto" type="hidden" />
        <jlis:value name="idFormato" type="hidden" />
        <jlis:value name="formato" type="hidden" />
        <jlis:value name="estadoRegistro" type="hidden" />
        <jlis:value name="estadoRegistroSUCE" type="hidden" />
        <jlis:value name="modificacionSuceId" type="hidden" />
        <jlis:value name="mensajeId" type="hidden" />
        <jlis:value name="transmitido" type="hidden" />
        <jlis:value name="controller" type="hidden" />
        <jlis:value name="seleccionarNotificacion" type="hidden" />
        <jlis:value name="tipo" type="hidden" />
        <jlis:value name="modificacionSuceXMto" type="hidden" />
        <jlis:value name="utilizaModificacionSuceXMto" type="hidden" />
        <jlis:value name="bloqueada" type="hidden" />
        <c:if test="${modificacionSuceXMto == 'S' && !empty mtoSUCE}">
        <jlis:value name="mtoSUCE" type="hidden" />
        <jlis:value name="idAcuerdo" type="hidden" />
        <jlis:value name="estadoAcuerdoPais" type="hidden" />
        </c:if>

        <vuce:validacionBotonesFormato pagina="suce" />

        <c:if test="${modificacionSuceXMto == 'N'}">
        <table>
             <tr>
             <%-- <c:if test="${desestimiento == 'N' && estadoRegistro != 'I'}"> --%>
             <c:if test="${estadoRegistro != 'I'}"> <!-- 1.No hay desestimiento en Origen, por eso comentamos la linea superior -->
	             <%-- <c:if test="${!empty modificacionSuceId}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="transmitirModifButton" name="transmitirModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para transmitir la modificaci�n de la SUCE" /></td>
	             </c:if> --%>
	             <c:if test="${empty modificacionSuceId}">
		             <td><jlis:button code="CO.USUARIO.OPERACION" id="crearModifButton" name="guardarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Grabar " alt="Pulse aqu� para crear la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <c:if test="${!empty modificacionSuceId}">
		             <td><jlis:button code="CO.USUARIO.OPERACION" id="actualizarModifButton" name="actualizarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar la modificaci�n de la SUCE" /></td>
		             <td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarModifButton" name="eliminarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar la modificaci�n de la SUCE" /></td>
	             </c:if>
             </c:if>
             <td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
             </td>
             </tr>
        </table>
        </c:if>

        <c:if test="${modificacionSuceXMto == 'S'}">
        <table>
             <tr>
             <%-- <c:if test="${desestimiento == 'N' && estadoRegistro != 'I'}"> --%>
             <c:if test="${estadoRegistro != 'I'}"> <!-- 2.No hay desestimiento en Origen, por eso comentamos la linea superior -->
	             <!--<c:if test="${!empty modificacionSuceId && transmitido == 'N'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="transmitirModifButton" name="transmitirModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Transmitir" alt="Pulse aqu� para transmitir la modificaci�n de la SUCE" /></td>
	             </c:if>-->
	             
	             <%-- 20170131_GBT ACTA CO-004-16 3.2.a Y ACTA CO 009-16--%>
	             <c:if test="${empty modificacionSuceId && estadoAcuerdoPais != 'I'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="crearModifButton" name="guardarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Grabar " alt="Pulse aqu� para crear la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <c:if test="${empty modificacionSuceId && estadoAcuerdoPais == 'I'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="crearModifButton" editable="NO" name="guardarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Grabar " alt="Pulse aqu� para crear la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <%-- 20170131_GBT ACTA CO-004-16 3.2.b Y ACTA CO 009-16--%>
	             <c:if test="${!empty modificacionSuceId && estadoRegistro == 'P' && bloqueada != 'S' && estadoAcuerdoPais != 'I'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="actualizarModifButton" name="actualizarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <c:if test="${!empty modificacionSuceId && estadoRegistro == 'P' && bloqueada != 'S' && estadoAcuerdoPais == 'I'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="actualizarModifButton" editable="NO" name="actualizarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Actualizar" alt="Pulse aqu� para actualizar la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <c:if test="${!empty modificacionSuceId && estadoRegistro != 'I' && estadoAcuerdoPais != 'I'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION,CO.ADMIN.HELP_DESK" id="abrirModificacionSUCEButton" name="abrirModificacionSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Modificaci�n de SUCE " alt="Pulse aqu� para abrir la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <c:if test="${!empty modificacionSuceId && estadoRegistro != 'I' && estadoAcuerdoPais == 'I'}">
	             	<td><jlis:button code="CO.USUARIO.OPERACION,CO.ADMIN.HELP_DESK" id="abrirModificacionSUCEButton" editable="NO" name="abrirModificacionSUCE()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Abrir Modificaci�n de SUCE " alt="Pulse aqu� para abrir la modificaci�n de la SUCE" /></td>
	             </c:if>
	             <%-- <c:if test="${transmitido == 'S' && estadoRegistro == 'G' && estadoRegistroSUCE != 'A'}"> --%>
	             <c:if test="${transmitido == 'S' && estadoRegistroSUCE != 'A' && (estadoModif == '' || estadoModif == 'P')}"> <!-- No hay pagos en Origen, no se necesita considerar el estado G -->
	             	<td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarModifButton" name="eliminarModificacion()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Desistir" alt="Pulse aqu� para desistir la modificaci�n de la SUCE" /></td>
	             </c:if>
             </c:if>
             <td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" />
             </td>
             </tr>
        </table>
        </c:if>

        <br>
        <table class="form" width="96%">
            <tr>
                <th align="left">
                    <jlis:label key="co.label.buzon.mensaje" />
<!--                     </br>
                    Estado registro: ${estadoRegistro}
                    </br>
                    Transmitido: ${transmitido}
                    </br>
                    Modificaci�n SUCE ID: ${modificacionSuceId}
                    </br>
                    Estado Registro SUCE: ${estadoRegistroSUCE}  -->
                </th>
            </tr>
            <tr>
                <td>
                    <jlis:textArea name="mensaje" rows="10" style="width:98%" />
                </td>
            </tr>
            <c:if test="${estadoRegistro == 'R'}">
            <tr>
                <th align="left">
                    <jlis:label key="co.label.buzon.mensaje_rechazo" />
                </th>
            </tr>
            <tr>
                <td>
                    <jlis:textArea name="motivoRechazo" rows="4" style="width:98%" editable="no" />
                </td>
            </tr>
            </c:if>
        </table>
        <br>

        <c:if test="${!empty modificacionSuceId && transmitido == 'N' && desestimiento == 'N' && bloqueada != 'S' && estadoRegistro != 'I'}">
            <table>
                 <tr>
                 	 <%-- 20170131_GBT ACTA CO-004-16 3.2.b Y ACTA CO 009-16--%>
                 	 <c:if test="${estadoAcuerdoPais != 'I'}">
                 	    <td><jlis:button code="CO.USUARIO.OPERACION" id="cargarButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
                        <td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
                     </c:if>
                 	 <c:if test="${estadoAcuerdoPais == 'I'}">
                 	    <td><jlis:button code="CO.USUARIO.OPERACION" id="cargarButton" editable="NO" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
                        <td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" editable="NO" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
                     </c:if>
                 </tr>
            </table>
            <table class="form">
                <tr>
                    <td>
                        <jlis:label key="co.label.documentos_adjuntar"/>
                        <jlis:label key="co.label.tipo_archivos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="file" id="archivo" name="archivo" size="80" />
                    </td>
                </tr>
            </table>
        </c:if>
        <c:if test="${!empty modificacionSuceId}">
            <jlis:table name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntos" scope="request" pageSize="*" fixedHeader="yes" navigationHeader="no" width="95%" >
              <jlis:tr>
                    <jlis:td name="ADJUNTO ID" nowrap="yes" />
                    <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
                    <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" />
                    <jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
                <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
                <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes"  />
                <jlis:columnStyle column="4" columnName="SELECCIONE" editable="yes" align="center" hide="${hide}"/>
                <jlis:tableButton column="4" type="checkbox" name="seleccione" />
                <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
            </jlis:table>
        </c:if>
        <c:if test="${tipo == 'S'}">
            <br/>
            <table class="form">
                <tr>
                    <th align="left">
                        <jlis:label labelClass="impar" key="co.label.notificaciones"/>
                    </th>
                </tr>
            </table>
            <jlis:table keyValueColumns="ID" name="notificaciones" source="tNotificaciones" scope="request" pageSize="*" width="60%" navigationHeader="no" >
                <jlis:tr>
                    <jlis:td name="ID" nowrap="yes" width="5%"/>
                    <jlis:td name="DE" nowrap="yes" width="5%"/>
                    <jlis:td name="MENSAJE" nowrap="yes" width="70%" />
                    <jlis:td name="FECHA REGISTRO" nowrap="yes" width="25%"/>
                    <jlis:td name="" width="1%" style="valueCheckbox=SELECCIONE,on-off,S,N"  />
                </jlis:tr>
                <jlis:columnStyle column="1" columnName="ID" hide="yes"/>
                <jlis:columnStyle column="2" columnName="DE" hide="yes" />
                <jlis:columnStyle column="3" columnName="MENSAJE" editable="yes" />
                <jlis:columnStyle column="4" columnName="FECHA_REGISTRO" type="dateTime" pattern="dd/MM/yyyy HH:mm:ss" />
                <jlis:columnStyle column="5" columnName="SELECCIONE" editable="${seleccionarNotificacion}" align="center" value="S" />
                <jlis:tableButton column="5" type="valueCheckbox" uncheckedValue="N" />
                <jlis:tableButton column="3" type="link" onClick="cargarNotificacionAdjuntos" />
              </jlis:table>
        </c:if>
    </form>
    <script>
        <c:if test="${tipo == 'S'}">
        	window.parent.document.getElementById("popupTitle").innerHTML = "Subsanaci�n de Notificaci�n de SUCE";
        </c:if>
        <%-- <c:if test="${tipo == 'M'}">
        window.parent.document.getElementById("popupTitle").innerHTML = "Escrito de SUCE";
        </c:if> --%>
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";
    </script>
</body>
</html>