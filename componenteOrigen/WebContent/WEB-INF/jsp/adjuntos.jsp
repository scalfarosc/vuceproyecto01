<%@include file="/WEB-INF/jsp/taglibs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript">

    function validarSubmit() {
        var f=document.formulario;
        if (f.button.value=="cancelarButton") {
            return false;
        }
        if (f.button.value=="cargarButton") {
            disableButton("cargarButton", true);
            disableButton("eliminarButton", true);
        }
        if (f.button.value=="eliminarButton") {
            disableButton("cargarButton", true);
            disableButton("eliminarButton", true);
        }
        if (f.button.value=="cerrarPopUpButton") {
        	var f = document.formulario;
        	f.idAdjunto.value="";
            f.method.value="";
	        <c:choose>
	    		<c:when test="${empty notifAdj}">
	        		validaCerraPopUp();
				</c:when>
				<c:otherwise>
	            	window.parent.hidePopWin(false);
	            	return false;
				</c:otherwise>
			</c:choose>
        }
        return true;
    }
        function cargarArchivo() {
            var f = document.formulario;
            if(f.archivo.value !=""){
                f.esDetalle.value = "N";
                f.action = contextName+"/origen.htm";
                f.method.value="cargarArchivo";
                f.button.value="cargarButton";
            }else {
                alert("Debe seleccionar alg�n archivo");
                f.button.value="cancelarButton";
            }
        }

/*        function cargarArchivoDetalle() {
            var f = document.formulario;
            if(f.archivo.value !=""){
                f.esDetalle.value = "S";
                f.action = contextName+"/fmt.htm";
                f.method.value="cargarArchivo";
                f.button.value="cargarButton";
            }else {
                alert("Debe Seleccionar alg�n Archivo");
                f.button.value="cancelarButton";
            }
        } */

        function descargarAdjunto(keyValues, keyValuesField) {
            var f = document.formulario;
            var adjuntoId = document.getElementById(keyValues.split("|")[0]).value;
            f.action = contextName+"/origen.htm";
            f.method.value="descargarAdjunto";
            f.idAdjunto.value=adjuntoId;
            f.submit();
        }

/*        function descargarAdjuntoDetalle(adjuntoId) {
            var f = document.formulario;
            f.action = contextName+"/fmt.htm";
            f.method.value="descargarAdjunto";
            f.idAdjunto.value=adjuntoId;
            f.submit();
        } */

        function eliminarRegistro() {
            var f = document.formulario;
            var indice = -1;
            if (f.seleccione!=null) {
                if (eval(f.seleccione.length)) {
                    for (i=0; i < f.seleccione.length; i++) {
                        if (f.seleccione[i].checked){
                            indice = i;
                        }
                    }
                } else {
                    if (f.seleccione.checked) {
                        indice = 0;
                    }
                }
            }
            if (indice!=-1) {
                f.action = contextName+"/origen.htm";
                f.method.value="eliminarAdjunto";
                f.button.value="eliminarButton";

            } else {
                alert('Seleccione por lo menos un registro a eliminar');
                f.button.value = 'cancelarButton';
            }
        }

        function seleccionDivNotas() {
            if (document.getElementById("idDivNotas").style.display == "none") {
            	document.getElementById("idDivNotas").style.display = "block";
            } else {
            	document.getElementById("idDivNotas").style.display = "none";
            }
        }

</script>

<body id="contModal">
    <jlis:messageArea width="100%" /><br/>
    <form name="formulario" method="post" enctype="multipart/form-data" onSubmit="return validarSubmit();">
        <input type="hidden" name="method" />
        <input type="hidden" name="button">
        <input type="hidden" name="idAdjunto">
        <jlis:value name="idFormatoEntidad" type="hidden" />
        <jlis:value name="idFormato" type="hidden" />
        <jlis:value name="formato" type="hidden" />
        <jlis:value name="adjunto" type="hidden" />
        <jlis:value name="tce" type="hidden" />
        <jlis:value name="orden" type="hidden" />
        <jlis:value name="mto" type="hidden" />
        <jlis:value name="bloqueado" type="hidden" />
        <jlis:value name="permiteAdicional" type="hidden" />
        <jlis:value name="transmitido" type="hidden" />
        <jlis:value name="controller" type="hidden" />
        <jlis:value name="esDetalle" type="hidden" />
        <h3 class="psubtitle"><span><b><jlis:value name="titulo" editable="no" /></b></span></h3>
        <c:if test="${(hide != 'no')}">
        	<table>
                 <tr>
                 	<td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" /></td>
                 </tr>
            </table>
        </c:if>
        <c:if test="${(hide == 'no')}">
            <table>
                 <tr>
                 <td><jlis:button code="CO.USUARIO.OPERACION" id="cargarButton" name="cargarArchivo()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" /></td>
                 <td><jlis:button code="CO.USUARIO.OPERACION" id="eliminarButton" name="eliminarRegistro()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Eliminar" alt="Pulse aqu� para eliminar los archivos seleccionados" /></td>
                 <td><jlis:button id="cerrarPopUpButton" name="cerrarPopUp()" type="BUTTON_JAVASCRIPT_SUBMIT" title=" Cerrar " alt="Pulse aqu� para cerrar la ventana sin guardar los cambios" /></td>
                 </tr>
            </table>
            <table>
                <tr>
                    <td>
                    <jlis:label key="co.label.documentos_adjuntar"/> <br />
                    <jlis:label key="co.label.tipo_archivos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                     <input type="file" id="archivo" name="archivo" size="87"  />
                    </td>
                </tr>
            </table>
        </c:if>
<%--<c:if test="${sessionScope.USUARIO.tipoOrigen == 'ET' && sessionScope.USUARIO.roles['VUCE.ENTIDAD.EVALUADOR'] == 'VUCE.ENTIDAD.EVALUADOR'}">
            <br/>
            <table>
                <tr>
                    <td>
                    <a href="#"><jlis:label key="vuce.label.resolutor.documentos_adjuntar" onClick="seleccionDivNotas()"/></a>
                    <a href="javascript:descargarAdjuntoDetalle(${idAdjuntoDetalle})">${nombreAdjuntoDetalle}</a>
                    </td>
                </tr>
                <tr style="display:none;" id="idDivNotas" >
                    <td>
                     <jlis:label key="vuce.label.tipo_archivos"/>
                     <input type="file" name="archivo" size="87"  />
                     <jlis:button code="VUCE.ENTIDAD.EVALUADOR" id="cargarButton" name="cargarArchivoDetalle()" type="BUTTON_JAVASCRIPT_SUBMIT" title="Cargar Archivo" alt="Pulse aqu� para cargar el archivo" />
                    </td>
                </tr>
            </table>
        </c:if>  --%>
        <br/>
        <jlis:table  name="ADJUNTOS" keyValueColumns="ADJUNTO_ID" source="tAdjuntos" scope="request"
            pageSize="*" navigationHeader="no" width="98%" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCopiadosTramitePadreRowValidator" >
            <jlis:tr>
                <jlis:td name="ADJUNTO ID" nowrap="yes" />
                <jlis:td name="ADJUNTO TIPO" nowrap="yes" />
                <jlis:td name="NOMBRE ARCHIVO" nowrap="yes" width="83%" />

                <c:if test="${empty notifAdj}">
	                <jlis:td name="TAMA�O (KB)" nowrap="yes" width="7%" />
	                <jlis:td name="FECHA CARGA" nowrap="yes" width="10%" />
                </c:if>

                <jlis:td name="" width="1%" style="checkbox=formulario,seleccione,on-off"  />


            </jlis:tr>
            <jlis:columnStyle column="1" columnName="ADJUNTO_ID" editable="no" hide="yes" />
            <jlis:columnStyle column="2" columnName="ADJUNTO_TIPO" editable="no" hide="yes" />
            <jlis:columnStyle column="3" columnName="NOMBRE_ARCHIVO" editable="yes" />
            <c:choose>
            	<c:when test="${empty notifAdj}">
            		<jlis:columnStyle column="4" columnName="TAMANO" />
		<%--            <c:if test="${controller == 'itp011.htm' || controller == 'dgm007.htm'}">
		            <jlis:columnStyle column="4" columnName="FECHA_CARGADO_EN_VUCE" type="dateTime" pattern="dd/MM/yyyy HH:mm" nowrap="yes" />
		            </c:if>
		            <c:if test="${controller != 'itp011.htm' && controller != 'dgm007.htm'}">
		            <jlis:columnStyle column="4" columnName="FECHA_CARGADO_EN_VUCE" hide="yes" />
		            </c:if>  --%>
		            <jlis:columnStyle column="5" columnName="FECHA_CARGADO_EN_VUCE" hide="yes" />
		            <jlis:columnStyle column="6" columnName="SELECCIONE" align="center" hide="${hide}" validator="pe.gob.mincetur.vuce.co.web.util.AdjuntosCellValidator" />
		            <jlis:tableButton column="6" type="checkbox" name="seleccione" />
            	</c:when>
            	<c:otherwise>
		            <jlis:columnStyle column="4" columnName="SELECCIONE" align="center" hide="${hide}" editable="yes" />
		            <jlis:tableButton column="4" type="checkbox" name="seleccione" />
            	</c:otherwise>
            </c:choose>

            <jlis:tableButton column="3" type="link" onClick="descargarAdjunto" highlight="no" />
        </jlis:table>
<%--         <c:if test="${controller == 'itp011.htm' || controller == 'dgm007.htm'}">
        <br/>
        <table style="margin: 0 auto; width: 96%"><tr><td class="labelClass" colspan="2">Leyenda:</td></tr><td class="codigoGrisClass" style="border: 1px solid black;" width="10px">&nbsp;&nbsp;</td><td class="labelClass">&nbsp;Archivos copiados desde tr�mite origen</td></tr></table>
        </c:if> --%>
    </form>
    <script>
        window.parent.document.getElementById("popupTitle").innerHTML = "Adjunto";
        window.parent.document.getElementById("popupTitle").className = "headerOptionClassModalWindow";

    </script>
</body>
</html>