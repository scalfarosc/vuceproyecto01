/************************************************************* ACUERDO PERU - SGP AUSTRALIA ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "-1";
var FILTRO_SECUENCIA_2 = "-1";
var FILTRO_SECUENCIA_3 = "-1";

var NUM_DIGITOS_PARTIDA = 4;

var alertaOptExportador = alertaOptExportadorSinValidacion;

/************* calificaci\u00F3n ORIGEN ************************/

//Realiza la inicializaci\u00F3n de controles seg\u00FAn el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {

	msgCriterio1 = 'El hecho que una mercanc\u00EDa sea completamente producida en el Per\u00FA, Bolivia, Ecuador o Colombia, o que todos los insumos sean originarios del Per\u00FA, Bolivia, Ecuador o Colombia, no significa que la mercanc\u00EDa sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	//$("#trCambioClasificacion").attr("style", "display:");

	//$("#trNorma").attr("style", "display:");
	//$("#trCriterioOrigen").attr("style", "display:");
	//$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

}

//Maneja los valores que dependen de los combos din\u00E1micos
//Se cre\u00F3 porque este debe ser invocado desde una funci\u00F3n as\u00EDncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {
}

//Validaciones de obligatoriedad para el criterio de calificaci\u00F3n
function validarCamposCriterioCalificacion(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.27.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.27.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.27.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n\u00FAmero de d\u00EDgitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.27.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.27.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.27.naladisa", "errorValueClass");
	} else {
		changeStyleClass("co.label.dj.27.naladisa", "labelClass");
	}
	*/
	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci\u00F3n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci\u00F3n de controles seg\u00FAn el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {
	alertaMaterial = 'Se entiende por material los insumos que se utilizan en procesos productivos, consumidos y/o incorporados en productos finales';
	msgCriterio1 = 'El hecho que una mercanc\u00EDa sea completamente producida en el Per\u00FA o Australia, o que todos los insumos sean originarios del Per\u00FA o Australia, no significa que la mercanc\u00EDa sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';
	$("#trValorUS").attr("style", "display:");
	$("#requieredValorUS").attr("style", "display:");

    $('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialAustralia );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de Australia");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu\u00ED para agregar un Nuevo Material Originario de Australia");

	if ($("#matPeruEmpty").val() == "1"){
		msgMaterialPeru = 'El material debe ser originario de Per\u00FA de acuerdo a lo establecido en el SGP-Australia, no por el hecho de que Ud. haya comprado un material en el mercado local o que el material haya sido producido en el Per\u00FA, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Per\u00FA a trav\u00E9s de documentaci\u00F3n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est\u00E1 seguro del origen del material, le sugerimos que lo declare como no originario';
	}

	if ($("#mat2doCompEmpty").val() == "1"){
		msgMaterial2doComp = 'El material debe ser originario de Australia de acuerdo a lo establecido en el SGP-Australia, no por el hecho de que Ud. haya comprado un material en el mercado de Australia o que el material haya sido producido en Australia, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Australia a trav\u00E9s de documentaci\u00F3n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est\u00E1 seguro del origen del material, le sugerimos que lo declare como no originario';
	}

}

// Funci\u00F3n puente para poder invocar a la funci\u00F3n general con el par\u00E1metro correcto
function nuevoMaterialAustralia() {
	if ( msgMaterial2doComp != '' ){
		alert(msgMaterial2doComp);
	}
	nuevoMaterial('AUS');
}

// Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "AUS"); // AUS: Australia
}

//Esta funci\u00F3n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

//Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return false;
}


/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci\u00F3n de controles de materiales seg\u00FAn el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
}

// Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;

	if (tipoPais == 'P') { // Si es Per\u00FA se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");

	} else if (tipoPais == 'AUS') { // Si es Australia
		$("#trNombreFabricante").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		$("#trProcedencia").attr("style", "display:");
		$("#DJ_MATERIAL\\.paisOrigen").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$("#DJ_MATERIAL\\.paisOrigen").attr("disabled", true);
		$("#spanPaisProcedencia").attr("style", "display:");
		$("#spanPaisOrigen").attr("style", "display:");
	} else { // Si es No Originario se muestra el pais de procedencia
		$("#trProcedencia").attr("style", "display:");
	}

	$("#spanValorUs").attr("style", "display:");

}


// Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa\u00EDs es Per\u00FA
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'AUS') { // Si el pa\u00EDs es Australia
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
	}

}

// Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {

    var ok = true;
    var f = document.formulario;
    var tipoPais = f.tipoPais.value;

    var mensaje="Debe ingresar o seleccionar : ";

    if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
        mensaje +="\n -La descripci\u00F3n del material.";
        changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
        mensaje +="\n -La partida arancelaria.";
        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
    } else {
    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
    }

    if ( tipoPais == 'AUS' ) {
	    if ( document.getElementById("DJ_MATERIAL.paisProcedencia").value == "" ) {
	        mensaje +="\n -El pa\u00EDs de procedencia.";
	        changeStyleClass("co.label.dj_material.pais_procedencia", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.pais_procedencia", "labelClass");
	    }
    }

    if (tipoPais == 'P' || tipoPais == 'AUS') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
    }

    if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n\u00FAmero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n\u00FAmero de documento del fabricante v\u00E1lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n\u00FAmero correcto de d\u00EDgitos para el n\u00FAmero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
    }

    if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida.";
        changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
    }

    if ( tipoPais == 'P' ) {
    	  if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
  	        mensaje +="\n -El Costo Unitario de la Mercanc�a en US$.";
  	        changeStyleClass("co.label.dj_material.valor_en_us_II", "errorValueClass");
  	    } else {
  	    	changeStyleClass("co.label.dj_material.valor_en_us_II", "labelClass");
  	    }
    }
    


    if (mensaje != "Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }

    return ok;
}

function habilitarXAcuerdo(){
	var tipoPais = document.formulario.tipoPais.value;

	if (tipoPais == 'AUS' ){
		$("#DJ_MATERIAL\\.paisOrigen").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		$("#DJ_MATERIAL\\.paisOrigen").attr("disabled", false);
	}
}

/************* DJ PRODUCTOR ************************/
function inicializacionDeclaracionJuradaProductor(){
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    return mensaje;
}