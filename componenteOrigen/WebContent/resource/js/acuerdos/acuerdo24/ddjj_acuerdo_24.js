/************************************************************* ACUERDO SGP Turquia ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "1";
var FILTRO_SECUENCIA_2 = "-1";
var FILTRO_SECUENCIA_3 = "2";

var NUM_DIGITOS_PARTIDA = 4;

var alertaOptExportador = alertaOptExportadorSinValidacion;

/************* CALIFICACI�N ORIGEN ************************/

//Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {

	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per�, o que todos los insumos sean originarios del Per�, no significa que la mercanc�a sea "totalmente obtenida". Ver Ayuda.';

	//$("#trCambioClasificacion").attr("style", "display:");

	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

}

//Maneja los valores que dependen de los combos din�micos
//Se cre� porque este debe ser invocado desde una funci�n as�ncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {
}


//Validaciones de obligatoriedad para el criterio de calificaci�n
function validarCamposCriterioCalificacion(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.14.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.14.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.14.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n�mero de d�gitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.14.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.14.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.14.naladisa", "errorValueClass");
	} else {
		changeStyleClass("co.label.dj.14.naladisa", "labelClass");
	}
	*/

	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci�n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {
	//$("#trDjPartidaAcuerdo").attr("style", "display:");
	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");
    $('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialUnionEuropea );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de Turqu�a");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de Turqu�a");
/*
    $('#nuevoMaterialTercerComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialOtrosPaises );
	$("#nuevoMaterialTercerComponenteButton").attr("value", "Adicionar Material Originario del Grupo Regional II");
	$("#nuevoMaterialTercerComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario  del Grupo Regional II");
*/
	$("#lnkPaisesG2").attr("style", "display:");

	$("#trValorUS").attr("style", "display:");
	$("#requieredValorUS").attr("style", "display:");
	if (obtenerCriterioArancelario() == 3) {
		$("#trPesoNetoMercancia").attr("style", "display:");
	}
}



//Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialUnionEuropea() {
	nuevoMaterial('TQ');
}

//Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "TQ"); // TQ: SGP TURQUIA
}
/*
//Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialOtrosPaises() {
	nuevoMaterial('OP');
}

//Edita el material del segundo componente segun el acuerdo
function editarMaterialTercerComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "OP"); // OP: OTROS PAISES
}
*/
//Esta funci�n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

//Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return false;
}

/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci�n de controles de materiales seg�n el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
//	alert("Se entiende por material a todo ingrediente, materia prima, componente o pieza, etc., utilizado en la fabricaci�n del producto");
}

//Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	//var secuenciaMaterial = document.getElementById("DJ_MATERIAL.secuenciaMaterial").value;

	if (tipoPais == 'P') { // Si es Per� se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");
		/*if (secuenciaMaterial != '') {
			$("#tablaSubtituloAdjunto").attr("style", "display:");
			$("#tablaBotonesAdjunto").attr("style", "display:");
			$("#tablaMaterialAdjuntos").attr("style", "display:");
		}*/
	} else if (tipoPais == 'TQ') { // Si es TQ se muestra s�lo el nombre del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trProcedencia").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		/*if (secuenciaMaterial != '') {
			$("#tablaSubtituloAdjunto").attr("style", "display:");
			$("#tablaBotonesAdjunto").attr("style", "display:");
			$("#tablaMaterialAdjuntos").attr("style", "display:");
		}*/
		cargarPaisesOrigenUE();
	} else if (tipoPais == 'OP') { // Si es OP se muestra todos
		$("#trNombreFabricante").attr("style", "display:");
		$("#trProcedencia").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		$("#spanPaisProcedencia").attr("style", "display:");
		$("#spanPaisOrigen").attr("style", "display:");
		cargarPaisesOrigenOtros();
		//cargarPaisesProcedencia();
	} else { // Si es No Originario se muestra el pais de procedencia y se muestra el peso
		$("#trProcedencia").attr("style", "display:");
		if ($("#criterioArancelario").val() == '3') { // Si esta marcado otro criterio, debemos mostrar el peso
			$("#trPesoMaterial").attr("style", "display:");
		}
	}

	// Controles comunes a todos
	//$("#trMaterialPartidaAcuerdo").attr("style", "display:");
	$("#spanValorUs").attr("style", "display:");

}

//Cargar pa�ses de UE
function cargarPaisesOrigenUE() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisOrigen").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisOrigen", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesOrigenUE, null);
}

//Despu�s de cargar los pa�ses de UE
function afterCargarPaisesOrigenUE() {
	document.getElementById("DJ_MATERIAL.paisOrigen").value = document.getElementById("tempPaisOrigen").value;
}


//Cargar pa�ses origen
function cargarPaisesOrigenOtros() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisOrigen").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisOrigen", "comun.pais_beneficiario_x_acuerdo_excluyendo_acuerdo.select", filter, null, afterCargarPaisesOrigen, null);
}

//Despu�s de cargar los pa�ses de origen
function afterCargarPaisesOrigen() {
	document.getElementById("DJ_MATERIAL.paisOrigen").value = document.getElementById("tempPaisOrigen").value;
}

//Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa�s es Per�
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'TQ') { // Si el pa�s es TURQUIA
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
	}

}

//Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {

  var ok = true;
  var f = document.formulario;
  var tipoPais = f.tipoPais.value;

  var mensaje="Debe ingresar o seleccionar : ";

  if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
      mensaje +="\n -La descripci�n del material.";
      changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
  } else {
  	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
  }

  if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
      mensaje +="\n -La partida arancelaria.";
      changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
  } else {
  	changeStyleClass("co.label.partida_arancelaria", "labelClass");
  }

  if (tipoPais == 'P' || tipoPais == 'TQ') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
  }

  if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n�mero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n�mero de documento del fabricante v�lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n�mero correcto de d�gitos para el n�mero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
  }

  if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
      mensaje +="\n -La unidad de medida.";
      changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
  } else {
  	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
  }

  if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
      mensaje +="\n -La cantidad.";
      changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
  } else {
  	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
  }

  if (tipoPais == 'N') {
	  // Si cumple otro criterio, el peso es obligatorio
	  if ($("#criterioArancelario").val() == '3') {
	  	    if ( document.getElementById("DJ_MATERIAL.pesoNeto").value == "" ) {
	  	        mensaje +="\n -El peso neto.";
	  	        changeStyleClass("co.label.dj_material.peso_material", "errorValueClass");
	  	    } else {
	  	    	changeStyleClass("co.label.dj_material.peso_material", "labelClass");
	  	    }
	  }
  }

  // El valor US es obligatorio si NO est� marcado el check de clasificaci�n arancelaria
	if ($("#criterioArancelario").val() != '2') {
		
	    if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
	        mensaje +="\n -El valor en US$.";
	        changeStyleClass("co.label.dj_material.valor_en_us", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.valor_en_us", "labelClass");
	    }

	}
	


  if (mensaje != "Debe ingresar o seleccionar : ") {
      ok = false;
      alert(mensaje);
  }

  return ok;
}

function habilitarXAcuerdo(){
	/*var tipoPais = document.formulario.tipoPais.value;

	if (tipoPais == 'PNM' ){
		$("#trOrigen").attr("style", "display:");
	}
	*/
}

/************* DJ PRODUCTOR ************************/
function inicializacionDeclaracionJuradaProductor(){
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    return mensaje;
}
