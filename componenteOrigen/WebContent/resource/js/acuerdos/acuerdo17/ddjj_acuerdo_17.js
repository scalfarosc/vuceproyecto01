/************************************************************* ACUERDO PERU - ALC PERU COREA ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "1";
var FILTRO_SECUENCIA_2 = "3";
var FILTRO_SECUENCIA_3 = "2,4,5,6";

var NUM_DIGITOS_PARTIDA = 6;

var alertaOptExportador = alertaOptExportadorSinValidacion;

/************* CALIFICACI�N ORIGEN ************************/

//Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {

	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per� o Corea, o que todos los insumos sean originarios del Per� o Corea, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	$("#trCambioClasificacion").attr("style", "display:");

	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

}

// Maneja los valores que dependen de los combos din�micos
// Se cre� porque este debe ser invocado desde una funci�n as�ncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {

	var secOrigen = document.getElementById("CALIFICACION.secuenciaOrigen").value;
	if ( secOrigen != '' && ( secOrigen == '4' || secOrigen == '5' ) ) { // Si no es un nuevo y es 4 o 5, debe aparecer y ser obligatorio
		$("#trPorcentajeSegunCriterio").attr("style", "display:");
		$("#spanPorcentajeSegunCriterio").attr("style", "display:");

		$("#CALIFICACION\\.porcentajeSegunCriterio").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		$("#CALIFICACION\\.porcentajeSegunCriterio").attr("disabled", false);
        $("#CALIFICACION\\.porcentajeSegunCriterio" ).attr("readonly", false);
	} else { // Caso contrario debe limpiarse (porque por defecto esta oculto)
		document.getElementById("CALIFICACION.porcentajeSegunCriterio").value = '';
		$("#spanPorcentajeSegunCriterio").attr("style", "display:none");
		$("#trPorcentajeSegunCriterio").attr("style", "display:none");

		$("#CALIFICACION\\.porcentajeSegunCriterio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$("#CALIFICACION\\.porcentajeSegunCriterio").attr("disabled", true);
        $("#CALIFICACION\\.porcentajeSegunCriterio" ).attr("readonly", false);
	}

}


//Validaciones de obligatoriedad para el criterio de calificaci�n
function validarCamposCriterioCalificacion() {

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.17.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.17.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.17.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n�mero de d�gitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.17.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.17.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.17.naladisa", "errorValueClass");
	} else {
		changeStyleClass("co.label.dj.17.naladisa", "labelClass");
	}
	*/
	var secOrigen = document.getElementById("CALIFICACION.secuenciaOrigen").value;
	if ( secOrigen != '' && ( secOrigen == '4' || secOrigen == '5' ) ) { // Si no es un nuevo y es 4 o 5, debe aparecer y ser obligatorio
	    if ( document.getElementById("CALIFICACION.porcentajeSegunCriterio").value == "" ) {
	        mensaje +="\n -El Porcentaje de VCR.";
	        changeStyleClass("co.label.dj.prctj_segun_criterio", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj.prctj_segun_criterio", "labelClass");
	    }
	}

	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci�n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {

	/*var flagEstadoDj = document.getElementById("estadoDj").value;
	if ( flagEstadoDj == 'T' || flagEstadoDj == 'L' || flagEstadoDj == 'A' || flagEstadoDj == 'R' ) { // Dicho campo se activa s�lo cuando se ha transmitido
		$("#trPorcentajeSegunCriterio").attr("style", "display:");
	}
	if ( flagEstadoDj != 'Q' ) { // Activamos los demas campos de otras pesta�as cuando al menos est� grabada la DJ
		//$("#trPorcentajeSegunCriterio").attr("style", "display:");
		$("#trPesoNetoMercancia").attr("style", "display:");
	}

	$("#trCambioClasificacion").attr("style", "display:");
	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");
	$("#tMantProductorPie").attr("style", "display:");
	var secOrigen = document.getElementById("secuenciaOrigen").value;

	if ( document.getElementById("DJ.cumpleOtroCriterio").value == 'S' && ( secOrigen == 4 || secOrigen == 5 ) ) { // En base a ciertas condiciones, activamos el peso

		// Activamos los campos porcentaje VCR y peso Mercanc�a
		//$("#DJ\\.porcentajeSegunCriterio").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		//$("#DJ\\.porcentajeSegunCriterio").attr("disabled", false);
		//$("#spanPorcentajeSegunCriterio").attr("style", "display:"); // Activamos la obligatoriedad

		$("#DJ\\.pesoNetoMercancia").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		$("#DJ\\.pesoNetoMercancia").attr("disabled", false);
	} else {

		// Activamos los campos porcentaje VCR y peso Mercanc�a
		//document.getElementById("DJ.porcentajeSegunCriterio").value = '';
		//$("#DJ\\.porcentajeSegunCriterio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		//$("#DJ\\.porcentajeSegunCriterio").attr("disabled", true);
		//$("#spanPorcentajeSegunCriterio").attr("style", "display:none"); // Desactivamos la obligatoriedad
		// Luego de limpiarlo, lo ocultamos
		$("#trPorcentajeSegunCriterio").attr("style", "display:none");

		document.getElementById("DJ.pesoNetoMercancia").value = '';
		$("#DJ\\.pesoNetoMercancia").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$("#DJ\\.pesoNetoMercancia").attr("disabled", true);
		// Luego de limpiarlo, lo ocultamos
		$("#trPesoNetoMercancia").attr("style", "display:none");
	}*/

	alertaMaterial = 'Se entiende por materiales a las materias primas, insumos, ingredientes, productos intermedios, partes, componentes, piezas y mercanc�as que se empleen en la producci�n de otra mercanc�a';

	var secOrigen = document.getElementById("secuenciaOrigen").value;
	if (document.getElementById("tabMaterial") != undefined) {
		if ( secOrigen != '' && ( secOrigen == '4' || secOrigen == '5' ) ) {
			$("#trPesoNetoMercancia").attr("style", "display:");
		} else {
			document.getElementById("DJ.pesoNetoMercancia").value = '';
			$("#requiredPesoNetoMercancia").attr("style", "display:none");
		}
	}

    $('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialCorea );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de Corea");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de Corea");

	if ($("#matPeruEmpty").val() == "1") {
		msgMaterialPeru = 'El material debe ser originario de Per� de acuerdo a lo establecido en el Acuerdo, no por el hecho de que Ud. haya comprado un material en el mercado local o que el material haya sido producido en el Per�, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Per� a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}

	if ($("#mat2doCompEmpty").val() == "1"){
		msgMaterial2doComp = 'El material debe ser originario de Corea de acuerdo a lo establecido en el Acuerdo, no por el hecho de que Ud. haya comprado un material en el mercado coreano o que el material haya sido producido en Corea, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Corea a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}

}

// Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialCorea() {
	if (msgMaterial2doComp != ''){
		alert(msgMaterial2doComp);
	}
	nuevoMaterial('C');
}

// Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "C"); // C: Corea
}

//Esta funci�n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

//Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return false;
}


/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci�n de controles de materiales seg�n el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
}

// Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;

	if (tipoPais == 'P') { // Si es Per� se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");
	} else if (tipoPais == 'C') { // Si es Corea se muestra s�lo el nombre del fabricante
		$("#trNombreFabricante").attr("style", "display:");
	} else { // Si es No Originario se muestra el pais de procedencia
		$("#trProcedencia").attr("style", "display:");
		$("#trPesoMaterial").attr("style", "display:");
		var secOrigen = document.getElementById("secuenciaOrigen").value;
		if ( secOrigen != '' && ( secOrigen == 4 || secOrigen == 5 ) ) { // En base a ciertas condiciones, activamos el peso, no obligatorio
			$("#trPesoMaterial").attr("style", "display:");
		} else { // Caso contrario se debe ocultar el campo
			document.getElementById("DJ_MATERIAL.pesoNeto").value = '';
			$("#trPesoMaterial").attr("style", "display:none");
		}
		// Por la manera como fue implementada, debemos ocultar el flag de obligatoriedad
		$("#spanPesoMaterial").attr("style", "display:none");
	}

	$("#spanValorUs").attr("style", "display:");

}


// Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa�s es Per�
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'C') { // Si el pa�s es J
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
	}

}

// Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {

    var ok = true;
    var f = document.formulario;
    var tipoPais = f.tipoPais.value;

    var mensaje="Debe ingresar o seleccionar : ";

    if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
        mensaje +="\n -La descripci�n del material.";
        changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
        mensaje +="\n -La partida arancelaria.";
        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
    } else {
    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
    }

    if (tipoPais == 'P' || tipoPais == 'C') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
    }

    if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n�mero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n�mero de documento del fabricante v�lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n�mero correcto de d�gitos para el n�mero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
    }

    if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida.";
        changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
    }

    // El valor US es obligatorio siempre seg�n el Excel
    if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
        mensaje +="\n -El valor en US$.";
        changeStyleClass("co.label.dj_material.valor_en_us", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.valor_en_us", "labelClass");
    }

   
    
    if (mensaje != "Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }

    return ok;
}

function habilitarXAcuerdo(){
	var tipoPais = document.formulario.tipoPais.value;

	if (tipoPais == 'PNM' ){
		$("#trOrigen").attr("style", "display:");
	}
}

/************* DJ PRODUCTOR ************************/
function inicializacionDeclaracionJuradaProductor(){
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    return mensaje;
}