/********* SOLICITUD ACUERDO TLC - VENEZUELA***************/

var PAIS_PERU = '168';
var NUM_DIGITOS_PARTIDA = 8;

// Inicializa los controles para el formato TLC - VENEZUELA
function inicializacionSolicitudSegunAcuerdo() {
	//Importador
	$(".trDatosImportador").attr("style", "display:");
	$("#trDatosImportadorPais").attr("style", "display:none");		
	$("#trDatosComplementariosImportadorImportadorTelefono").attr("style", "display:");
	$("#trDatosComplementariosImportadorImportadorEmail").attr("style", "display:");

	//Medio de Transporte
	$(".trMediosTransporteRuta").attr("style", "display:");
	$("#trMediosTransporteRutaFecha").attr("style", "display:none");
	$("#trMediosTransporteRutaModoTransCargaNumeroTransporte").attr("style", "display:none");
	$("#trMediosTransporteRutaPuertoDescarga").attr("style", "display:none");
	$("#trMediosTransporteRutaModoTransCarga").attr("style", "display:none");
	$("#trMediosTransporteRutaModoTransDescarga").attr("style", "display:none");
	
	$("#spanSustentoAdicionalRequired").attr("style", "display:none");
	
	var modoTransCargaId = $("#CERTIFICADO_ORIGEN\\.modoTransCargaId").val();
	if ( modoTransCargaId != '') {
		document.getElementById("modoTransCargaDescarga").value = modoTransCargaId;
		cargarPuertosCarga();
		//cargarPuertosDescarga();
	}
}

//Funci�n ajax que puebla el combo de puertos de carga
function cargarPuertosCarga() {

	if ($("#modoTransCargaDescarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "select.empty", null, null, null, null);
	} else {
		var filter = "paisIsoId=" + PAIS_PERU + "|modoTransporte=" + $("#modoTransCargaDescarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "comun.puerto.select", filter, null, afterCargarPuertosCarga, null);
	}

}

//Despues de cargar puertos de carga
function afterCargarPuertosCarga() {
    var puertoCargaId = $("#CERTIFICADO_ORIGEN\\.puertoCargaId").val();
	if ( puertoCargaId != '') {
		document.getElementById("puertoCarga").value = puertoCargaId;
	}
}

//Funci�n ajax que puebla el combo de puertos de descarga
function cargarPuertosDescarga() {
}

//Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {
	document.getElementById("CERTIFICADO_ORIGEN.modoTransCargaId").value = document.getElementById("modoTransCargaDescarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoCargaId").value = document.getElementById("puertoCarga").value;

}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

    mensaje += mensajeAdicional;
    
    if ( $.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorNombre").value) == "" ) {
        mensaje +="\n - El nombre del importador";
        changeStyleClass("co.label.certificado.importador_nombre", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_nombre", "labelClass");
    }

    if ( $.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorDireccion").value) == "" ) {
        mensaje +="\n - La direcci�n del importador";
        changeStyleClass("co.label.certificado.importador_direccion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_direccion", "labelClass");
    }
    
    
    if ( $.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorTelefono").value) == "" ) {
        mensaje +="\n - Tel�fono";
        changeStyleClass("co.label.certificado.importador_telefono", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_telefono", "labelClass");
    }

    
    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert (mensaje);
    }
    return ok;
}


/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo TLC VENEZUELA
function inicializacionFacturaSolicitudSegunAcuerdo() {
	$(".trOperadorTercerPais").attr("style", "display:");
	$(".trNombreOperadorTercerPais").attr("style", "display:");
	$(".trDireccionOperadorTercerPais").attr("style", "display:");
	$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	manejaNombreOperador(document.getElementById("CO_FACTURA.indicadorTercerPais"));
}

//Maneja la coherencia entre el check de operador de tercer pais y el nombre del operador
function manejaNombreOperador(combo) {
	// Si esta seleccionado el checkbox, debemos activar la caja de texto, caso contrario debe estar desactivada
	if ( combo.value == 'S' ) {
		$(".trNombreOperadorTercerPais").attr("style", "display:");
        $("#spanNombreOperador").attr("style", "display:");
		$(".trDireccionOperadorTercerPais").attr("style", "display:");
        //$("#spanDireccionOperador").attr("style", "display:");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	} else {
		$(".trNombreOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.nombreOperadorTercerPais" ).val("");
        $("#spanNombreOperador").attr("style", "display:none");
		$(".trDireccionOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.direccionOperadorTercerPais" ).val("");
        //$("#spanDireccionOperador").attr("style", "display:none");
        $("#CO_FACTURA\\.tieneFacturaTercerPais").val("N");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:none");
	}

}
// Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado(){

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";
    

    if (document.getElementById("CO_FACTURA.indicadorTercerPais").value == 'S') {
        if (document.getElementById("CO_FACTURA.nombreOperadorTercerPais").value == "") {
            mensaje +="\n -El nombre del operador del tercer pa�s";
            changeStyleClass("co.label.factura.nombre_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.nombre_operador", "labelClass");
        }
    }

    if (document.getElementById("CO_FACTURA.numero").value == "") {
        mensaje +="\n -El n�mero";
        changeStyleClass("co.label.factura.numero", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.numero", "labelClass");
    }

    if (document.getElementById("CO_FACTURA.fecha").value == ""){
        mensaje +="\n -La fecha";
        changeStyleClass("co.label.factura.fecha", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.fecha", "labelClass");
    }

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

    if (mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* MERCANCIA ***************/

// Inicializa los controles para la mercanc�a del acuerdo TLC VENEZUELA
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trMercanciaFactura").attr("style", "display:");
	//$("#trMercanciaUm").attr("style", "display:");
	$("#trMercanciaValor").attr("style", "display:");
	$("#trMercanciaUmFisicaSegunFactura").attr("style", "display:");
	$("#trMercanciaPartidaAcuerdo").attr("style", "display:");
	
	$("#CO_MERCANCIA\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);
	
   	//$("#selectFacturas").attr('disabled',true);
	//$("#selectFacturas").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	asignarValorFactura(document.getElementById("selectFacturas"));
	
	//$("#trMercanciaCantidad").attr("style", "display:");	
}

function valida_longitud_naladisa(obj,validarLongitud) {
	valida_longitud_naladisa_digitos(obj,validarLongitud,NUM_DIGITOS_PARTIDA);
}

// Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
	/*if (document.getElementById("selectFacturas").options.length > 1) {
		document.getElementById("selectFacturas").selectedIndex = 1;
       	$("#selectFacturas").attr('disabled',true);
		$("#selectFacturas").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	}
	*/
	asignarValorFactura(document.getElementById("selectFacturas"));
}

//Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }

    if ( $.trim(document.getElementById("CO_MERCANCIA.partidaSegunAcuerdo").value) == "" ) {
        mensaje +="\n -"+document.getElementById("co.label.co_mercancia.13.naladisa").innerHTML;
        changeStyleClass("co.label.co_mercancia.13.naladisa", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.13.naladisa", "labelClass");
    }
    
    /*if (document.getElementById("CO_MERCANCIA.naladisa").value==""){
        mensaje +="\n -Naladisa";
        changeStyleClass("co.label.co_mercancia.naladisa", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.naladisa", "labelClass");

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value==""){
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.um_fisica_id", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.um_fisica_id", "labelClass");

    if (document.getElementById("CO_MERCANCIA.peso").value==""){
        mensaje +="\n -El peso";
        changeStyleClass("co.label.co_mercancia.peso", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.peso", "labelClass");

    if (document.getElementById("CO_MERCANCIA.cantidad").value==""){
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.cantidad", "labelClass");

    if (document.getElementById("CO_MERCANCIA.tipoEnvase").value==""){
        mensaje +="\n -El tipo de envase";
        changeStyleClass("co.label.co_mercancia.tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.tipo_envase", "labelClass");

    if (document.getElementById("CO_MERCANCIA.descripcionTipoEnvase").value==""){
        mensaje +="\n -La descripci�n del tipo de envase";
        changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "labelClass");
*/
    if (document.getElementById("selectFacturas").value==""){
        mensaje +="\n -N�mero de Factura";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");
    
    
    
    if (document.getElementById("CO_MERCANCIA.valorFacturadoUs").value==""){
        mensaje +="\n -Valor Facturado (US$)";
        changeStyleClass("co.label.co_mercancia.valor_facturado", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.valor_facturado", "labelClass");
    
    if (document.getElementById("CO_MERCANCIA.umFisicaSegunFactura").value==""){
        mensaje +="\n -Unidad F�sica seg�n Factura";
        changeStyleClass("co.label.co_mercancia.um_fisica_segun_factura", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.um_fisica_segun_factura", "labelClass");

    
    

    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo TLC VENEZUELA
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {

	$("#trNroCertifEntidad").attr("style", "display:none");
	$("#trFechaEmision").attr("style", "display:");
	if ( $("#codTipoDr").val() != 'R' ) {
		$("#trFechaVigencia").attr("style", "display:");
	}

}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo TLC VENEZUELA
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");
}
