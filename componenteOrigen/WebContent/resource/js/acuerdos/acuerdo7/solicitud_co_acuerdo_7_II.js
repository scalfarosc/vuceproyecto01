/********* SOLICITUD ***************/

var PAIS_PERU = '168';
var NUM_DIGITOS_PARTIDA = 6;

// Inicializa los controles para el formato TLC Per� - Singapur
function inicializacionSolicitudSegunAcuerdo() {
	$(".trDatosImportador").attr("style", "display:");
	$("#trMediosTransporteRutaModoTransCarga").attr("style", "display:none");
	$("#trMediosTransporteRutaModoTransDescarga").attr("style", "display:none");
	cargarPaisesImportador();

	var modoTransCargaId = $("#CERTIFICADO_ORIGEN\\.modoTransCargaId").val();
	if ( modoTransCargaId != '') {
		document.getElementById("modoTransCargaDescarga").value = modoTransCargaId;
		cargarPuertosCarga();
		cargarPuertosDescarga();
	}
	/*var modoTransDescargaId = $("#CERTIFICADO_ORIGEN\\.modoTransDescargaId").val();
	if ( modoTransDescargaId != '') {
		document.getElementById("modoTransDescarga").value = modoTransDescargaId;
		cargarPuertosDescarga();
	}*/
}

// Cargar pa�ses del importador
function cargarPaisesImportador() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisImportador", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesImportador, null);
}

// Despues de cargar pa�ses del importador
function afterCargarPaisesImportador() {
	var idPais = $("#idPais").val();
	document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;
	document.getElementById("selectPaisImportador").value = idPais;
	$("#selectPaisImportador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	$("#selectPaisImportador").attr("disabled", true);
}

// Funci�n ajax que puebla el combo de puertos de carga
function cargarPuertosCarga() {

	if ($("#modoTransCargaDescarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "select.empty", null, null, null, null);
	} else {
		var filter = "paisIsoId=" + PAIS_PERU + "|modoTransporte=" + $("#modoTransCargaDescarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "comun.puerto.select", filter, null, afterCargarPuertosCarga, null);
	}

}

// Despues de cargar puertos de carga
function afterCargarPuertosCarga() {
    var puertoCargaId = $("#CERTIFICADO_ORIGEN\\.puertoCargaId").val();
	if ( puertoCargaId != '') {
		document.getElementById("puertoCarga").value = puertoCargaId;
	}
}

//Funci�n ajax que puebla el combo de puertos de descarga
function cargarPuertosDescarga() {

	if ($("#modoTransCargaDescarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "select.empty", null, null, null, null);
	} else {
		var f = document.formulario;
		var filter = "paisIsoId=" + f.idPais.value + "|modoTransporte=" + $("#modoTransCargaDescarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "comun.puerto.select", filter, null, afterCargarPuertosDescarga, null);
	}

}

//Despues de cargar puertos de descarga
function afterCargarPuertosDescarga() {
    var puertoDescargaId = $("#CERTIFICADO_ORIGEN\\.puertoDescargaId").val();
	if ( puertoDescargaId != '') {
		document.getElementById("puertoDescarga").value = puertoDescargaId;
	}
}

// Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {

	document.getElementById("CERTIFICADO_ORIGEN.modoTransCargaId").value = document.getElementById("modoTransCargaDescarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoCargaId").value = document.getElementById("puertoCarga").value;
	//document.getElementById("CERTIFICADO_ORIGEN.modoTransDescargaId").value = document.getElementById("modoTransDescarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoDescargaId").value = document.getElementById("puertoDescarga").value;

}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

    mensaje += mensajeAdicional;

    if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorNombre").value) == "") {
    	mensaje +="\n -El nombre del importador.";
    	changeStyleClass("co.label.certificado.importador_nombre", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_nombre", "labelClass");
    }
    if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorDireccion").value) == "") {
    	mensaje +="\n -La direcci�n del importador.";
    	changeStyleClass("co.label.certificado.importador_direccion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_direccion", "labelClass");
    }

    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert(mensaje);
    }
    return ok;
}

/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo Per� - Jap�n
function inicializacionFacturaSolicitudSegunAcuerdo() {
	$(".trOperadorTercerPais").attr("style", "display:");
	$(".trNombreOperadorTercerPais").attr("style", "display:");
	$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	manejaNombreOperador(document.getElementById("CO_FACTURA.indicadorTercerPais"));
}

// Maneja la coherencia entre el check de operador de tercer pais y el nombre del operador
function manejaNombreOperador(combo) {
	// Si esta seleccionado el checkbox, debemos activar la caja de texto, caso contrario debe estar desactivada
	if ( combo.value == 'S' ) {
		$(".trNombreOperadorTercerPais").attr("style", "display:");
        $("#spanNombreOperador").attr("style", "display:");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	} else {
		$(".trNombreOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.nombreOperadorTercerPais" ).val("");
        $("#spanNombreOperador").attr("style", "display:none");
        $("#CO_FACTURA\\.tieneFacturaTercerPais").val("N");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:none");
	}

}

// Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if (document.getElementById("CO_FACTURA.indicadorTercerPais").value == 'S') {
        if (document.getElementById("CO_FACTURA.nombreOperadorTercerPais").value == "") {
            mensaje +="\n -El nombre del operador del tercer pa�s";
            changeStyleClass("co.label.factura.nombre_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.nombre_operador", "labelClass");
        }
    }

    if (document.getElementById("CO_FACTURA.numero").value == "") {
        mensaje +="\n -El n�mero";
        changeStyleClass("co.label.factura.numero", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.numero", "labelClass");
    }

    if (document.getElementById("CO_FACTURA.fecha").value == ""){
        mensaje +="\n -La fecha";
        changeStyleClass("co.label.factura.fecha", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.fecha", "labelClass");
    }

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

    if (mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* MERCANCIA ***************/

// Inicializa los controles para la mercanc�a del acuerdo Per� - Jap�n
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trMercanciaFactura").attr("style", "display:");
	$("#trMercanciaPartidaAcuerdo").attr("style", "display:");
	
	$("#CO_MERCANCIA\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);
}

function valida_longitud_naladisa(obj,validarLongitud) {
	valida_longitud_naladisa_digitos(obj,validarLongitud,NUM_DIGITOS_PARTIDA);
}

// Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
}

// Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n.";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }

    if ( $.trim(document.getElementById("CO_MERCANCIA.partidaSegunAcuerdo").value) == "" ) {
        mensaje +="\n -"+document.getElementById("co.label.co_mercancia.7.naladisa").innerHTML;
        changeStyleClass("co.label.co_mercancia.7.naladisa", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.7.naladisa", "labelClass");
    }
    
    if (document.getElementById("selectFacturas").value == '') {
        mensaje +="\n -La factura asociada.";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");
    }

    if ( mensaje!="Debe ingresar o seleccionar : " ) {
        ok = false;
        alert(mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo Per� - Singapur
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {
	$("#trNombreImportador").attr("style", "display:");
	$("#trDireccionImportador").attr("style", "display:");
	$("#trPaisImportador").attr("style", "display:");

	$("#trNroCertifEntidad").attr("style", "display:none");
	$("#trDatosImportacion").attr("style", "display:none");
	$("#trDatosIniciales").attr("style", "display:");
	$(".trObservaciones").attr("style", "display:");
	$(".trDatosImportacion2").attr("style", "display:");
}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo Per� - Singapur
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");
}

