/************************************************************* ACUERDO PERU - SGP NORUEGA ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "1";
var FILTRO_SECUENCIA_2 = "-1";
var FILTRO_SECUENCIA_3 = "2";

var NUM_DIGITOS_PARTIDA = 4;

var alertaOptExportador = alertaOptExportadorConValidacion;

/************* CALIFICACI�N ORIGEN ************************/

//Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {

	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per�, Bolivia, Ecuador o Colombia, o que todos los insumos sean originarios del Per�, Bolivia, Ecuador o Colombia, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	//$("#trCambioClasificacion").attr("style", "display:");

	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

}

//Maneja los valores que dependen de los combos din�micos
//Se cre� porque este debe ser invocado desde una funci�n as�ncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {
}

//Validaciones de obligatoriedad para el criterio de calificaci�n
function validarCamposCriterioCalificacion(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.11.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.11.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.11.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n�mero de d�gitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.11.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.11.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.11.naladisa", "errorValueClass");
	} else {
		  changeStyleClass("co.label.dj.11.naladisa", "labelClass");
	}
	*/
	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci�n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {
	alertaOptExportador = "Ud. Deber� registrar la informaci�n del productor";
	alertaMaterial = 'Se entiende por material a todo ingrediente, materia prima, componente o parte, etc., usado en la fabricaci�n de un producto';
	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per� o Noruega, o que todos los insumos sean originarios del Per� o Noruega, no significa que la mercanc�a sea "totalmente obtenida". Ver Ayuda.';

	//$("#trCalificacion").attr("style", "display:");
	//$("#trDjPartidaAcuerdo").attr("style", "display:");
	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trPorcentajeSegunCriterio").attr("style", "display:");
	var flagEstadoDj = document.getElementById("estadoDj").value;
	if ( flagEstadoDj == 'T' || flagEstadoDj == 'L' || flagEstadoDj == 'A' || flagEstadoDj == 'R' ) { // Dicho campo se activa s�lo cuando se ha transmitido
		$("#trValorUSPrecUnit").attr("style", "display:");
	}

	$("#trValorUS").attr("style", "display:");
	$("#requieredValorUS").attr("style", "display:");

	mostrarNotaProductores();


	$('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialNoruega );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de Noruega");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de Noruega");

    $('#nuevoMaterialTercerComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialNoruegaAdicionales );
	$("#nuevoMaterialTercerComponenteButton").attr("value", "Adicionar Material Originario de los paises de Suiza o la UE");
	$("#nuevoMaterialTercerComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de los paises de Suiza o la UE");

	if (obtenerCriterioArancelario() == 3) {
		$("#trPesoNetoMercancia").attr("style", "display:");
		//$("#DJ\\.trPesoNetoMercancia").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		//$("#DJ\\.trPesoNetoMercancia").attr("disabled", false);
	} /*else {
		$("#DJ\\.trPesoNetoMercancia").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$("#DJ\\.trPesoNetoMercancia").attr("disabled", true);
	}*/

	if ($("#matPeruEmpty").val() == "1"){
		msgMaterialPeru = 'El material debe ser originario de Per� de acuerdo a lo establecido en el SGP-Noruega, no por el hecho de que Ud. haya comprado un material en el mercado local o que el material haya sido producido en el Per�, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Per� a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}

	if ($("#mat2doCompEmpty").val() == "1"){
		msgMaterial2doComp = 'El material debe ser originario de Noruega de acuerdo a lo establecido en el SGP-Noruega, no por el hecho de que Ud. haya comprado un material en el mercado de Noruega o que el material haya sido producido en Noruega, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Noruega a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}

	if ($("#mat3erCompEmpty").val() == "1"){
		msgMaterial3erComp = 'El material debe ser originario de los paises de Suiza o la UE de acuerdo a lo establecido en el SGP-Noruega, no por el hecho de que Ud. haya comprado un material en el mercado de los paises de Suiza o la UE o que el material haya sido producido en los paises de Suiza o la UE, significa que califica como originario. Para ello debe asegurarse que el material sea originario de los paises de Suiza o la UE a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}
}

// Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialNoruega() {
	if (msgMaterial2doComp != ''){
		alert(msgMaterial2doComp);
	}
	nuevoMaterial('NR');
}

//Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialNoruegaAdicionales() {
	if (msgMaterial3erComp != ''){
		alert(msgMaterial3erComp);
	}
	nuevoMaterial('NRT');
}

// Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "NR"); // NR: Noruega
}

//Edita el material del segundo componente segun el acuerdo
function editarMaterialTercerComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "NRT"); // NRT: Noruega Tercer Componente (paises de Suiza o la UE)
}

//Esta funci�n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

//Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return true;
}


/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci�n de controles de materiales seg�n el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
}

// Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;

	if (tipoPais == 'P') { // Si es Per� se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");

	} else if (tipoPais == 'NR') { // Si es NR se muestra s�lo el nombre del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trProcedencia").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		$("#spanPaisProcedencia").attr("style", "display:");
		$("#spanPaisOrigen").attr("style", "display:");
	} else if (tipoPais == 'NRT') { // Si es NRT Noruega Tercer Componente (paises de Suiza o la UE)
		$("#trNombreFabricante").attr("style", "display:");
		$("#trProcedencia").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		$("#spanPaisProcedencia").attr("style", "display:");
		$("#spanPaisOrigen").attr("style", "display:");
		cargarPaisesOrigenOtros();
		//cargarPaisesProcedencia();
	} else { // Si es No Originario se muestra el pais de procedencia
		$("#trProcedencia").attr("style", "display:");

		if ($("#criterioArancelario").val() == '3') { // Si esta marcado otro criterio, debemos mostrar el peso
			$("#trPesoMaterial").attr("style", "display:");
			$("#spanPesoMaterial").attr("style", "display:none");
		} else {
			$("#DJ_MATERIAL\\.pesoNeto").val("");
		}

		//cargarPaisesProcedencia();
	}

	$("#spanValorUs").attr("style", "display:");

}

// Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa�s es Per�
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'NR') { // Si el pa�s es Noruega
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
        $("#DJ_MATERIAL\\.paisOrigen" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
        $("#DJ_MATERIAL\\.paisOrigen" ).attr("disabled",true);
	}

}

// Cargar pa�ses procedencia
function cargarPaisesProcedencia() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo + "|idPaisExcluido=160"
	document.getElementById("tempPaisProcedencia").value = document.getElementById("DJ_MATERIAL.paisProcedencia").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisProcedencia", "comun.pais_beneficiario_x_acuerdo.select", filter, null, afterCargarPaisesProcedencia, null);
}

// Despu�s de cargar los pa�ses de procedencia
function afterCargarPaisesProcedencia() {
	document.getElementById("DJ_MATERIAL.paisProcedencia").value = document.getElementById("tempPaisProcedencia").value;
}

//Cargar pa�ses origen
function cargarPaisesOrigenOtros() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo + "|idPaisExcluido=160";
	document.getElementById("tempPaisOrigen").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisOrigen", "comun.pais_beneficiario_x_acuerdo.select", filter, null, afterCargarPaisesOrigen, null);
}

//Despu�s de cargar los pa�ses de origen
function afterCargarPaisesOrigen() {
	document.getElementById("DJ_MATERIAL.paisOrigen").value = document.getElementById("tempPaisOrigen").value;
}

// Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {

    var ok = true;
    var f = document.formulario;
    var tipoPais = f.tipoPais.value;

    var mensaje="Debe ingresar o seleccionar : ";

    if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
        mensaje +="\n -La descripci�n del material.";
        changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
        mensaje +="\n -La partida arancelaria.";
        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
    } else {
    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
    }

    if (tipoPais == 'NR' || tipoPais == 'NRT') {
	    if ( document.getElementById("DJ_MATERIAL.paisProcedencia").value == "" ) {
	        mensaje +="\n -El pa�s de procedencia.";
	        changeStyleClass("co.label.dj_material.pais_procedencia", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.pais_procedencia", "labelClass");
	    }
    }

    if (tipoPais == 'NR' || tipoPais == 'NRT') {
	    if ( document.getElementById("DJ_MATERIAL.paisOrigen").value == "" ) {
	        mensaje +="\n -El pa�s de origen.";
	        changeStyleClass("co.label.dj_material.pais_origen", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.pais_origen", "labelClass");
	    }
    }

    if (tipoPais == 'P' || tipoPais == 'NR' || tipoPais == 'NRT') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
    }

    if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n�mero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n�mero de documento del fabricante v�lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n�mero correcto de d�gitos para el n�mero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			} else {
	    				changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
    }

    if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida.";
        changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
    }

    // El valor US es obligatorio si NO est� marcado el check de clasificaci�n arancelaria
	//if ($("#criterioArancelario").val() != '2') {


	    if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
	        mensaje +="\n -El valor en US$.";
	        changeStyleClass("co.label.dj_material.valor_en_us", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.valor_en_us", "labelClass");
	    }

	//}
	    


    if (mensaje != "Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }

    return ok;
}

function habilitarXAcuerdo(){
	var tipoPais = document.formulario.tipoPais.value;

	if (tipoPais == 'NR' ){
		$("#DJ_MATERIAL\\.paisOrigen" ).attr("disabled",false);
	}
}

/************* DJ PRODUCTOR ************************/
function inicializacionDeclaracionJuradaProductor(){
	//$("#trEsValidador").attr("style", "display:");
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    return mensaje;
}

