/********* SOLICITUD ***************/

var PAIS_PERU = '168';

var NUM_DIGITOS_PARTIDA = 4;

// Inicializa los controles para el formato Per� - SGPC
function inicializacionSolicitudSegunAcuerdo() {
	$(".trDatosImportador").attr("style", "display:");
	$(".trMediosTransporteRuta").attr("style", "display:");
	$("#trMediosTransporteRutaModoTransCarga").attr("style", "display:none");
	$("#trMediosTransporteRutaModoTransDescarga").attr("style", "display:none");
	$(".trSalioMercancia").attr("style", "display:");
	$("#spanImportadorNombre").hide();
	$("#spanImportadorDireccion").hide();

	$("#spanSustentoAdicionalRequired").attr("style", "display:none");
	$("#spanImportadorNombreRequired").attr("style", "display:none");
	$("#spanImportadorDireccionRequired").attr("style", "display:none");
	$("#spanSelectPaisImportador").attr("style", "display:none");

	cargarPaisesImportador();

	var modoTransCargaId = $("#CERTIFICADO_ORIGEN\\.modoTransCargaId").val();
	if ( modoTransCargaId != '') {
		document.getElementById("modoTransCargaDescarga").value = modoTransCargaId;
		cargarPuertosCarga();
		cargarPuertosDescarga();
	}
	/*var modoTransDescargaId = $("#CERTIFICADO_ORIGEN\\.modoTransDescargaId").val();
	if ( modoTransDescargaId != '') {
		document.getElementById("modoTransDescarga").value = modoTransDescargaId;
		cargarPuertosDescarga();
	}*/
}

// Cargar pa�ses del importador
function cargarPaisesImportador() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisImportador", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesImportador, null);
}

// Despues de cargar pa�ses del importador
function afterCargarPaisesImportador() {
	var idPais = $("#idPais").val();
	document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;
	document.getElementById("selectPaisImportador").value = idPais;
	$("#selectPaisImportador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	$("#selectPaisImportador").attr("disabled", true);
}

// Funci�n ajax que puebla el combo de puertos de carga
function cargarPuertosCarga() {

	if ($("#modoTransCargaDescarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "select.empty", null, null, null, null);
	} else {
		var filter = "paisIsoId=" + PAIS_PERU + "|modoTransporte=" + $("#modoTransCargaDescarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "comun.puerto.select", filter, null, afterCargarPuertosCarga, null);
	}

}

// Despues de cargar puertos de carga
function afterCargarPuertosCarga() {
    var puertoCargaId = $("#CERTIFICADO_ORIGEN\\.puertoCargaId").val();
	if ( puertoCargaId != '') {
		document.getElementById("puertoCarga").value = puertoCargaId;
	}
}

//Funci�n ajax que puebla el combo de puertos de descarga
function cargarPuertosDescarga() {

	if ($("#modoTransCargaDescarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "select.empty", null, null, null, null);
	} else {
		var f = document.formulario;
		var filter = "paisIsoId=" + f.idPais.value + "|modoTransporte=" + $("#modoTransCargaDescarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "comun.puerto.select", filter, null, afterCargarPuertosDescarga, null);
	}

}

//Despues de cargar puertos de descarga
function afterCargarPuertosDescarga() {
    var puertoDescargaId = $("#CERTIFICADO_ORIGEN\\.puertoDescargaId").val();
	if ( puertoDescargaId != '') {
		document.getElementById("puertoDescarga").value = puertoDescargaId;
	}
}

// Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {

	document.getElementById("CERTIFICADO_ORIGEN.modoTransCargaId").value = document.getElementById("modoTransCargaDescarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoCargaId").value = document.getElementById("puertoCarga").value;
	//document.getElementById("CERTIFICADO_ORIGEN.modoTransDescargaId").value = document.getElementById("modoTransDescarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoDescargaId").value = document.getElementById("puertoDescarga").value;

}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

    mensaje += mensajeAdicional;

    // Al menos uno de los datos de transporte debe ser ingresado
	var fechaTransporte = document.getElementById("CERTIFICADO_ORIGEN.fechaPartidaTransporte").value;
	var numeroTransporte = document.getElementById("CERTIFICADO_ORIGEN.numeroTransporte").value;
	var puertoCarga = document.getElementById("puertoCarga").value;
	var puertoDescarga = document.getElementById("puertoDescarga").value;

	if (fechaTransporte == '' && numeroTransporte == '' && puertoCarga == '' && puertoDescarga == '') {
		mensaje +="\n -Alguno de los datos de transporte (si selecciona el tipo de puerto tiene tambien deber� seleccionar el puerto).";
	}

    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert (mensaje);
    }
    return ok;
}

/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo Per� - SGPC
function inicializacionFacturaSolicitudSegunAcuerdo() {
	$(".trOperadorTercerPais").attr("style", "display:");
	$(".trNombreOperadorTercerPais").attr("style", "display:");
	$(".trDireccionOperadorTercerPais").attr("style", "display:");
	$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	manejaNombreOperador(document.getElementById("CO_FACTURA.indicadorTercerPais"));
}

//Maneja la coherencia entre el check de operador de tercer pais y el nombre del operador
function manejaNombreOperador(combo) {
	// Si esta seleccionado el checkbox, debemos activar la caja de texto, caso contrario debe estar desactivada
	if ( combo.value == 'S' ) {
		$(".trNombreOperadorTercerPais").attr("style", "display:");
        $("#spanNombreOperador").attr("style", "display:");
		$(".trDireccionOperadorTercerPais").attr("style", "display:");
        $("#spanDireccionOperador").attr("style", "display:");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	} else {
		$(".trNombreOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.nombreOperadorTercerPais" ).val("");
        $("#spanNombreOperador").attr("style", "display:none");
		$(".trDireccionOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.direccionOperadorTercerPais" ).val("");
        $("#spanDireccionOperador").attr("style", "display:none");
        $("#CO_FACTURA\\.tieneFacturaTercerPais").val("N");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:none");
	}
}


// Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado(){

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if (document.getElementById("CO_FACTURA.indicadorTercerPais").value == 'S') {
        if (document.getElementById("CO_FACTURA.nombreOperadorTercerPais").value == "") {
            mensaje +="\n -El nombre del operador del tercer pa�s";
            changeStyleClass("co.label.factura.nombre_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.nombre_operador", "labelClass");
        }
        if (document.getElementById("CO_FACTURA.direccionOperadorTercerPais").value == "") {
            mensaje +="\n -La direcci�n del operador del tercer pa�s";
            changeStyleClass("co.label.factura.direccion_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.direccion_operador", "labelClass");
        }

    }

    if (document.getElementById("CO_FACTURA.numero").value == "") {
        mensaje +="\n -El n�mero";
        changeStyleClass("co.label.factura.numero", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.numero", "labelClass");
    }

    if (document.getElementById("CO_FACTURA.fecha").value == ""){
        mensaje +="\n -La fecha";
        changeStyleClass("co.label.factura.fecha", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.fecha", "labelClass");
    }

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

    if (mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* MERCANCIA ***************/

//Inicializa los controles para la mercanc�a del acuerdo Per� - SGCP
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaMarca").attr("style", "display:");
	$("#trMercanciaTipoBulto").attr("style", "display:");
	$("#trMercanciaCantidadBulto").attr("style", "display:");
	$("#trMercanciaCantidad").attr("style", "display:");
	$("#trMercanciaUm").attr("style", "display:");
	$("#trMercanciaFactura").attr("style", "display:");
    // Estas inicializaciones son necesarias pero para todos los acuerdos
    if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == '' ) {
    	document.getElementById("CO_MERCANCIA.esMercanciaGranel").value = 'N';
    }

	mostrarNotaProductores();

    // Manejamos la l�gica asociada
    logicaMercanciaGranel(document.getElementById("CO_MERCANCIA.esMercanciaGranel").value);
}

//Maneja la l�gica esperada para el caso de mercancias a granel
function logicaMercanciaGranel(esMercanciaGranel) {
	if ( esMercanciaGranel == 'S' ) {
		document.getElementById("CO_MERCANCIA.cantidadBulto").value = '';
		$("#trMercanciaCantidadBulto").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.marcasNumeros").value = '';
		$("#trMercanciaMarca").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.tipoBulto").value = 'IN BULK';
		// hacer readonly la clase de paquete
        $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
        $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", true);
	} else {
		$("#trMercanciaCantidadBulto").attr("style", "display:");
		$("#trMercanciaMarca").attr("style", "display:");
		// quitamos el readonly de la clase de paquete
        if ( document.getElementById("CO_MERCANCIA.tipoBulto").value == 'IN BULK' ) { // S�lo para este caso limpiamos la informaci�n
        	document.getElementById("CO_MERCANCIA.tipoBulto").value = '';
        }
        $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("readonlyInputTextClass").addClass("inputTextClass");
        $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", false);
	}
}

//Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
}

//Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";
	var obj = document.getElementById("CO_MERCANCIA.descripcion");
    var descripcion = obj.value.replace(/\s+$/g,'');

    if (descripcion == '') {
        mensaje +="\n -La descripci�n";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }

	if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) == "" ) {
		mensaje +="\n -La clase de paquetes.";
		changeStyleClass("co.label.co_mercancia.tipo_bulto", "errorValueClass");
	} else {
		changeStyleClass("co.label.co_mercancia.tipo_bulto", "labelClass");
	}

	if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == 'N' ) { // S�lo cuando no es a granel este campo es visible y obligatorio
		if ( document.getElementById("CO_MERCANCIA.cantidadBulto").value == '' ) {
		    mensaje +="\n -La cantidad de paquetes.";
		    changeStyleClass("co.label.co_mercancia.cantidad_bulto", "errorValueClass");
		} else {
			changeStyleClass("co.label.co_mercancia.cantidad_bulto", "labelClass");
		}

	    if (document.getElementById("CO_MERCANCIA.marcasNumeros").value=="") {
	        mensaje +="\n -Marcas y n�mero de paquete";
	        changeStyleClass("co.label.co_mercancia.marca", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.co_mercancia.marca", "labelClass");
	    }

	    if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) != "" ) {
			var contenidoTipoBulto = document.getElementById("CO_MERCANCIA.tipoBulto").value.toUpperCase();
			var inBulkIngles = 'IN BULK';
			var inBulkEspanol = 'A GRANEL';
			if ( contenidoTipoBulto.indexOf(inBulkIngles) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'IN BULK' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "labelClass");
			}
			if ( contenidoTipoBulto.indexOf(inBulkEspanol) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'A GRANEL' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "labelClass");
			}
	    }
	}

    if (document.getElementById("CO_MERCANCIA.cantidad").value=="") {
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.cantidad", "labelClass");
    }

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value=="") {
        mensaje +="\n -La unidad de medida";
        changeStyleClass("co.label.co_mercancia.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.um_fisica_id", "labelClass");
    }

    if (document.getElementById("selectFacturas").value == '') {
        mensaje +="\n -La factura asociada.";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");
    }

    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo Per� - Noruega
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {
	$("#trNroCertifOrigen").attr("style", "display:none");
	$("#trEmitidoPosteriori").attr("style", "display:");
	$("#trNombreImportador").attr("style", "display:");
	$("#trDireccionImportador").attr("style", "display:");
	$("#trPaisImportador").attr("style", "display:");
	$("#trFechaPartida").attr("style", "display:");
	$("#trNumeroTransporte").attr("style", "display:");
	$("#trPuertoCarga").attr("style", "display:");
	$("#trPuertoDescarga").attr("style", "display:");

	$("#trDatosImportacion").attr("style", "display:none");
	$("#trDatosIniciales").attr("style", "display:");
	$(".trDatosImportacion2").attr("style", "display:");
	$(".trMediosTransporte").attr("style", "display:");
	$(".trObservaciones").attr("style", "display:");
}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo Per� - Noruega
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaDrMarca").attr("style", "display:");
	$("#trMercanciaDrTipoBulto").attr("style", "display:");
	$("#trMercanciaDrCantidadBulto").attr("style", "display:");
	$("#trMercanciaDrCantidad").attr("style", "display:");
	$("#trMercanciaDrUnidadMedida").attr("style", "display:");
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");
	if ( document.getElementById("MERCANCIA.ES_MERCANCIA_GRANEL").value == 'SI' ) { // Si el indicador de mercanc�a a granel es si, ocultamos la cantidad y la marca
		$("#trMercanciaDrMarca").attr("style", "display:none");
		$("#trMercanciaDrCantidadBulto").attr("style", "display:none");
	}
}
