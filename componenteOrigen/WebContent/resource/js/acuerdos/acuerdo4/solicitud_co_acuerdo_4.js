/********* SOLICITUD ***************/

var NUM_DIGITOS_PARTIDA = 8;

// Inicializa los controles para el formato ACE Per� - Chile
function inicializacionSolicitudSegunAcuerdo() {
	$("#spanSustentoAdicionalRequired").attr("style", "display:none");
}

//Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {
}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {
	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

    mensaje += mensajeAdicional;

    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert (mensaje);
    }
    return ok;
}


/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo ACE Per� - Chile
function inicializacionFacturaSolicitudSegunAcuerdo() {
}

// Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado(){

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if (document.getElementById("CO_FACTURA.numero").value == "") {
        mensaje +="\n -El n�mero";
        changeStyleClass("co.label.factura.numero", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.numero", "labelClass");
    }

    if (document.getElementById("CO_FACTURA.fecha").value == ""){
        mensaje +="\n -La fecha";
        changeStyleClass("co.label.factura.fecha", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.fecha", "labelClass");
    }

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

    if (mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* MERCANCIA ***************/

// Inicializa los controles para la mercanc�a del acuerdo ACE Per� - Chile
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trMercanciaPartidaAcuerdo").attr("style", "display:");
	$("#trMercanciaFactura").attr("style", "display:");
   	//$("#selectFacturas").attr('disabled',true);
	//$("#selectFacturas").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	
	$("#CO_MERCANCIA\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);
	
	asignarValorFactura(document.getElementById("selectFacturas"));
}

function valida_longitud_naladisa(obj,validarLongitud) {
	valida_longitud_naladisa_digitos(obj,validarLongitud,NUM_DIGITOS_PARTIDA);
}

// Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
	if (document.getElementById("selectFacturas").options.length > 1) {
		document.getElementById("selectFacturas").selectedIndex = 1;
       	//$("#selectFacturas").attr('disabled',true);
		//$("#selectFacturas").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	}
	asignarValorFactura(document.getElementById("selectFacturas"));
}

//Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n";
        changeStyleClass("co.label.mercancia.busqueda.denominacion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.mercancia.busqueda.denominacion", "labelClass");
    }

    if ( $.trim(document.getElementById("CO_MERCANCIA.partidaSegunAcuerdo").value) == "" ) {
        mensaje +="\n -"+document.getElementById("co.label.co_mercancia.4.naladisa").innerHTML;
        changeStyleClass("co.label.co_mercancia.4.naladisa", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.4.naladisa", "labelClass");
    }
    
    /*if (document.getElementById("CO_MERCANCIA.naladisa").value==""){
        mensaje +="\n -Naladisa";
        changeStyleClass("co.label.co_mercancia.naladisa", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.naladisa", "labelClass");

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value==""){
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.um_fisica_id", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.um_fisica_id", "labelClass");

    if (document.getElementById("CO_MERCANCIA.peso").value==""){
        mensaje +="\n -El peso";
        changeStyleClass("co.label.co_mercancia.peso", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.peso", "labelClass");

    if (document.getElementById("CO_MERCANCIA.cantidad").value==""){
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.cantidad", "labelClass");

    if (document.getElementById("CO_MERCANCIA.tipoEnvase").value==""){
        mensaje +="\n -El tipo de envase";
        changeStyleClass("co.label.co_mercancia.tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.tipo_envase", "labelClass");

    if (document.getElementById("CO_MERCANCIA.descripcionTipoEnvase").value==""){
        mensaje +="\n -La descripci�n del tipo de envase";
        changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "labelClass");
*/
    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo ACE Per� - Chile
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {

	$("#trNroCertifEntidad").attr("style", "display:none");
	$("#trFechaEmision").attr("style", "display:");
	if ( $("#codTipoDr").val() != 'R' ) {
		$("#trFechaVigencia").attr("style", "display:");
	}

}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo Per� - Chile
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");
}
