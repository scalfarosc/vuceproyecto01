/********* SOLICITUD ***************/

var PAIS_PERU = '168';
var NUM_DIGITOS_PARTIDA = 6;

// Inicializa los controles para el formato AAE Per� - Jap�n
function inicializacionSolicitudSegunAcuerdo() {
	$(".trDatosImportador").attr("style", "display:");
	$(".trMediosTransporteRuta").attr("style", "display:");
	//$("#trMediosTransporteRutaModoTransCarga").attr("style", "display:none"); //20140610_JMC: Se comenta por el PQT-08
	//$("#trMediosTransporteRutaModoTransDescarga").attr("style", "display:none"); //20140610_JMC: Se comenta por el PQT-08
	$("#trMediosTransporteRutaModoTransCargaDescarga").attr("style", "display:none"); // 20140610_JMC: Se agrega por el PQT-08
	$("#trAlMenosUnDatoMediosTransporte").attr("style", "display:"); // 20140610_JMC: Se agrega por el PQT-08
	$("#trLinkAyudaPuertoDescarga").attr("style", "display:"); // 20140610_JMC: Se agrega por el PQT-08
	
	$(".trSalioMercancia").attr("style", "display:");
	$(".trInformacionProductorIncluida").attr("style", "display:");
	
	$("#spanFechaPartidaTransporte_2").show(); // 20140610_JMC: Se agrega por el PQT-08
	$("#spanNumeroTransporte_2").show(); // 20140610_JMC: Se agrega por el PQT-08
	$("#spanTipoPuertoCarga_2").show(); // 20140610_JMC: Se agrega por el PQT-08
    
	/*
	$("#spanFechaPartidaTransporte").show();
	$("#spanNumeroTransporte").show();
	$("#spanTipoPuertoCarga").show();	
	$("#spanPuertoCarga").show();
	$("#spanTipoPuertoDescarga").show();
	$("#spanPuertoDescarga").show();
	*/
	$("#spanSustentoAdicionalRequired").attr("style", "display:none");

	cargarPaisesImportador();
	
	//20140610_JMC: Se agrega por el PQT-08
	var modoTransCargaId = $("#CERTIFICADO_ORIGEN\\.modoTransCargaId").val();
	if ( modoTransCargaId != '') {
		document.getElementById("modoTransCarga").value = modoTransCargaId;
		cargarPuertosCarga();
	}
	//20140610_JMC: Se agrega por el PQT-08
	var modoTransDescargaId = $("#CERTIFICADO_ORIGEN\\.modoTransDescargaId").val();
	if ( modoTransDescargaId != '') {
		document.getElementById("modoTransDescarga").value = modoTransDescargaId;
		cargarPuertosDescarga();
	}


}

// Cargar pa�ses del importador
function cargarPaisesImportador() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisImportador", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesImportador, null);
}

// Despues de cargar pa�ses del importador
function afterCargarPaisesImportador() {
	var idPais = $("#idPais").val();
	document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;
	document.getElementById("selectPaisImportador").value = idPais;
	$("#selectPaisImportador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	$("#selectPaisImportador").attr("disabled", true);
}

// Funci�n ajax que puebla el combo de puertos de carga 
function cargarPuertosCarga() {

	if ($("#modoTransCarga").val() == '') { //20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "select.empty", null, null, null, null);
		
	} else {		
		var filter = "paisIsoId=" + PAIS_PERU + "|modoTransporte=" + $("#modoTransCarga").val(); //20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "comun.puerto.select", filter, null, afterCargarPuertosCarga, null);
	}

}

// Despues de cargar puertos de carga
function afterCargarPuertosCarga() {
    var puertoCargaId = $("#CERTIFICADO_ORIGEN\\.puertoCargaId").val();
	if ( puertoCargaId != '') {
		document.getElementById("puertoCarga").value = puertoCargaId;
	}
}

//Funci�n ajax que puebla el combo de puertos de descarga
function cargarPuertosDescarga() {

	if ($("#modoTransDescarga").val() == '') {//20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "select.empty", null, null, null, null);
	} else {
		var f = document.formulario;
		var filter = "paisIsoId=" + f.idPais.value + "|modoTransporte=" + $("#modoTransDescarga").val();//20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "comun.puerto.select", filter, null, afterCargarPuertosDescarga, null);
	}

}

//Despues de cargar puertos de descarga
function afterCargarPuertosDescarga() {
    var puertoDescargaId = $("#CERTIFICADO_ORIGEN\\.puertoDescargaId").val();
	if ( puertoDescargaId != '') {
		document.getElementById("puertoDescarga").value = puertoDescargaId;
	}
}

// Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {

	document.getElementById("CERTIFICADO_ORIGEN.modoTransCargaId").value = document.getElementById("modoTransCarga").value;// 20140610_JMC: Se actualiza por el PQT-08	
	document.getElementById("CERTIFICADO_ORIGEN.puertoCargaId").value = document.getElementById("puertoCarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.modoTransDescargaId").value = document.getElementById("modoTransDescarga").value;//20140610_JMC: Se agrega por el PQT-08
	document.getElementById("CERTIFICADO_ORIGEN.puertoDescargaId").value = document.getElementById("puertoDescarga").value;

}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

	//20140610_JMC: Se actualiza por el PQT-08
    mensaje += mensajeAdicional;
    if (document.getElementById("CERTIFICADO_ORIGEN.importadorNombre").value == "") {
        mensaje +="\n -El nombre del importador";
        changeStyleClass("co.label.certificado.importador_nombre", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_nombre", "labelClass");
    }

    if (document.getElementById("CERTIFICADO_ORIGEN.importadorDireccion").value == "") {
        mensaje +="\n -La direcci�n del importador";
        changeStyleClass("co.label.certificado.importador_direccion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_direccion", "labelClass");
    }

    // Al menos uno de los datos de transporte debe ser ingresado
	var fechaTransporte = document.getElementById("CERTIFICADO_ORIGEN.fechaPartidaTransporte").value;
	var numeroTransporte = document.getElementById("CERTIFICADO_ORIGEN.numeroTransporte").value;
	var puertoCarga = document.getElementById("puertoCarga").value;
	var puertoDescarga = document.getElementById("puertoDescarga").value;

	if (fechaTransporte == '' && numeroTransporte == '' && puertoCarga == '' && puertoDescarga == '') {
		mensaje +="\n -Alguno de los datos de transporte (si selecciona el tipo de puerto tiene tambien deber� seleccionar el puerto).";
	}
    
    
    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert (mensaje);
    }
    return ok;
}

/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo Per� - Jap�n
function inicializacionFacturaSolicitudSegunAcuerdo() {
	$(".trOperadorTercerPais").attr("style", "display:");
	$(".trNombreOperadorTercerPais").attr("style", "display:");
	$(".trDireccionOperadorTercerPais").attr("style", "display:");
	$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	manejaNombreOperador(document.getElementById("CO_FACTURA.indicadorTercerPais"));
}

// Maneja la coherencia entre el check de operador de tercer pais y el nombre del operador
function manejaNombreOperador(combo) {
	// Si esta seleccionado el checkbox, debemos activar la caja de texto, caso contrario debe estar desactivada
	if ( combo.value == 'S' ) {
		$(".trNombreOperadorTercerPais").attr("style", "display:");
        $("#spanNombreOperador").attr("style", "display:");
		$(".trDireccionOperadorTercerPais").attr("style", "display:");
        $("#spanDireccionOperador").attr("style", "display:");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	} else {
		$(".trNombreOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.nombreOperadorTercerPais" ).val("");
        $("#spanNombreOperador").attr("style", "display:none");
		$(".trDireccionOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.direccionOperadorTercerPais" ).val("");
        $("#spanDireccionOperador").attr("style", "display:none");
        $("#CO_FACTURA\\.tieneFacturaTercerPais").val("N");
		$(".trTieneFacturaOperadorTercerPais").attr("style", "display:none");
	}

}

// Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if (document.getElementById("CO_FACTURA.indicadorTercerPais").value == 'S') {
        if (document.getElementById("CO_FACTURA.nombreOperadorTercerPais").value == "") {
            mensaje +="\n -El nombre del operador del tercer pa�s";
            changeStyleClass("co.label.factura.nombre_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.nombre_operador", "labelClass");
        }
        if (document.getElementById("CO_FACTURA.direccionOperadorTercerPais").value == "") {
            mensaje +="\n -La direcci�n del operador del tercer pa�s";
            changeStyleClass("co.label.factura.direccion_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.direccion_operador", "labelClass");
        }
    }

    if (document.getElementById("CO_FACTURA.numero").value == "") {
        mensaje +="\n -El n�mero";
        changeStyleClass("co.label.factura.numero", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.numero", "labelClass");
    }

    if (document.getElementById("CO_FACTURA.fecha").value == ""){
        mensaje +="\n -La fecha";
        changeStyleClass("co.label.factura.fecha", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.fecha", "labelClass");
    }

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

    if (mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* MERCANCIA ***************/

// Inicializa los controles para la mercanc�a del acuerdo Per� - Jap�n
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaTipoBulto").attr("style", "display:");
	$("#trMercanciaCantidadBulto").attr("style", "display:");
	$("#trMercanciaCantidad").attr("style", "display:");
	$("#trMercanciaUm").attr("style", "display:");
	$("#trMercanciaFactura").attr("style", "display:");
	$("#trMercanciaValor").attr("style", "display:");
	$("#trMercanciaPartidaAcuerdo").attr("style", "display:");
    
	$("#CO_MERCANCIA\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

    // Estas inicializaciones son necesarias pero para todos los acuerdos
    if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == '' ) {
    	document.getElementById("CO_MERCANCIA.esMercanciaGranel").value = 'N';
    }

    // Manejamos la l�gica asociada
    logicaMercanciaGranel(document.getElementById("CO_MERCANCIA.esMercanciaGranel").value);
}

function valida_longitud_naladisa(obj,validarLongitud) {
	valida_longitud_naladisa_digitos(obj,validarLongitud,NUM_DIGITOS_PARTIDA);
}

//Maneja la l�gica esperada para el caso de mercancias a granel
function logicaMercanciaGranel(esMercanciaGranel) {
	if ( esMercanciaGranel == 'S' ) {
		document.getElementById("CO_MERCANCIA.cantidadBulto").value = '';
		$("#trMercanciaCantidadBulto").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.tipoBulto").value = 'IN BULK';
		// hacer readonly la clase de paquete
     $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
     $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", true);
	} else {
		$("#trMercanciaCantidadBulto").attr("style", "display:");
		// quitamos el readonly de la clase de paquete
     if ( document.getElementById("CO_MERCANCIA.tipoBulto").value == 'IN BULK' ) { // S�lo para este caso limpiamos la informaci�n
     	document.getElementById("CO_MERCANCIA.tipoBulto").value = '';
     }
     $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("readonlyInputTextClass").addClass("inputTextClass");
     $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", false);
	}
}

// Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
}

// Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n.";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }

    if ( $.trim(document.getElementById("CO_MERCANCIA.partidaSegunAcuerdo").value) == "" ) {
        mensaje +="\n -"+document.getElementById("co.label.co_mercancia.15.naladisa").innerHTML;
        changeStyleClass("co.label.co_mercancia.15.naladisa", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.15.naladisa", "labelClass");
    }
    
	if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) == "" ) {
		mensaje +="\n -La clase de paquetes.";
		changeStyleClass("co.label.co_mercancia.tipo_bulto.15", "errorValueClass");
	} else {
		changeStyleClass("co.label.co_mercancia.tipo_bulto.15", "labelClass");
	}

	if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == 'N' ) { // S�lo cuando no es a granel este campo es visible y obligatorio
		if ( document.getElementById("CO_MERCANCIA.cantidadBulto").value == '' ) {
		    mensaje +="\n -La cantidad de paquetes.";
		    changeStyleClass("co.label.co_mercancia.cantidad_bulto.15", "errorValueClass");
		} else {
			changeStyleClass("co.label.co_mercancia.cantidad_bulto.15", "labelClass");
		}

		if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) != "" ) {
			var contenidoTipoBulto = document.getElementById("CO_MERCANCIA.tipoBulto").value.toUpperCase();
			var inBulkIngles = 'IN BULK';
			var inBulkEspanol = 'A GRANEL';
			if ( contenidoTipoBulto.indexOf(inBulkIngles) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'IN BULK' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto.15", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto.15", "labelClass");
			}
			if ( contenidoTipoBulto.indexOf(inBulkEspanol) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'A GRANEL' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto.15", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto.15", "labelClass");
			}
		}
	}

    if (document.getElementById("CO_MERCANCIA.cantidad").value == '') {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.co_mercancia.cantidad.15", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.cantidad.15", "labelClass");
    }

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value == '') {
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.um_fisica_id.15", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.um_fisica_id.15", "labelClass");
    }

    if (document.getElementById("selectFacturas").value == '') {
        mensaje +="\n -La factura asociada.";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");
    }

    if (document.getElementById("CO_MERCANCIA.valorFacturadoUs").value == '') {
        mensaje +="\n -El valor facturado.";
        changeStyleClass("co.label.co_mercancia.valor_facturado", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.valor_facturado", "labelClass");
    }

    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo Per� - Jap�n
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {
	$("#trEmitidoPosteriori").attr("style", "display:");
	$("#trNombreImportador").attr("style", "display:");
	$("#trDireccionImportador").attr("style", "display:");
	$("#trPaisImportador").attr("style", "display:");
	$("#trFechaPartida").attr("style", "display:");
	$("#trNumeroTransporte").attr("style", "display:");
	$("#trPuertoCarga").attr("style", "display:");
	$("#trPuertoDescarga").attr("style", "display:");
	$("#trTieneProductor").attr("style", "display:");
	
	$("#trModoTransporteCarga").attr("style", "display:"); // 20140610_JMC PQT-08 
	$("#trModoTransporteDescarga").attr("style", "display:"); // 20140610_JMC PQT-08

	$("#trNroCertifEntidad").attr("style", "display:none");
	$("#trDatosImportacion").attr("style", "display:none");
	$("#trDatosIniciales").attr("style", "display:");
	$(".trObservaciones").attr("style", "display:");
	
	$("#trFechaFirmaExportador").attr("style", "display:"); // 20151020_JFR: Acta.34:Firmas
	$(".trInformacionAdicionalFirma").attr("style", "display:"); // 20151020_JFR: Acta.34:Firmas
	
	$(".trDatosImportacion2").attr("style", "display:");	
}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo Per� - Jap�n
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaDrTipoBulto").attr("style", "display:");
	$("#trMercanciaDrCantidadBulto").attr("style", "display:");
	$("#trMercanciaDrCantidad").attr("style", "display:");
	$("#trMercanciaDrUnidadMedida").attr("style", "display:");
	$("#trMercanciaDrValorFacturado").attr("style", "display:");
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");
	$(".lblPesoBruto").attr("style", "display:");

	if ( document.getElementById("MERCANCIA.ES_MERCANCIA_GRANEL").value == 'SI' ) { // Si el indicador de mercanc�a a granel es si, ocultamos la cantidad
		$("#trMercanciaDrCantidadBulto").attr("style", "display:none");
	}

}

