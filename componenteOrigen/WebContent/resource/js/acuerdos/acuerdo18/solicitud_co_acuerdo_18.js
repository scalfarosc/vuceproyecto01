/********* SOLICITUD ***************/

var PAIS_PERU = '168';

// Inicializa los controles para el formato Per� - EFTA
function inicializacionSolicitudSegunAcuerdo() {
	$(".trDatosImportador").attr("style", "display:");
	$(".trMediosTransporteRuta").attr("style", "display:");
	//$("#trMediosTransporteRutaModoTransCarga").attr("style", "display:none"); //20140610_JMC: Se comenta por el PQT-08
	//$("#trMediosTransporteRutaModoTransDescarga").attr("style", "display:none"); //20140610_JMC: Se comenta por el PQT-08
	
	$("#trMediosTransporteRutaModoTransCargaDescarga").attr("style", "display:none"); //20140610_JMC: Se agrega por el PQT-08
	$("#trAlMenosUnDatoMediosTransporte").attr("style", "display:"); // 20140610_JMC: Se agrega por el PQT-08
	$("#spanPaisDescarga").show(); // 20141209_RPC: Acta UE/AELC
	$("#trLinkAyudaPuertoDescarga").attr("style", "display:"); // 20140610_JMC: Se agrega por el PQT-08
	
	$(".trSalioMercancia").attr("style", "display:");
	
	$("#spanFechaPartidaTransporte_2").show(); // 20140610_JMC: Se agrega por el PQT-08
	$("#spanNumeroTransporte_2").show(); // 20140610_JMC: Se agrega por el PQT-08
	$("#spanTipoPuertoCarga_2").show(); // 20140610_JMC: Se agrega por el PQT-08
	
	$("#spanSelectPaisImportador").attr("style", "display:none");
	$("#spanImportadorNombre").attr("style", "display:none");
	$("#spanImportadorDireccion").attr("style", "display:none");

	if ($("#formato").val() == 'mct003' || $("#formato").val() == 'MCT003') {
		$("#spanImportadorNombreRequired").attr("style", "display:none");
		$("#spanImportadorDireccionRequired").attr("style", "display:none");
		$("#spanSelectPaisImportadorRequired").attr("style", "display:none");
	}

	cargarPaisesImportador();
	cargarPaisesDescarga(); // 20141209_RPC: Acta UE/AELC

	//20140610_JMC: Se agrega por el PQT-08
	var modoTransCargaId = $("#CERTIFICADO_ORIGEN\\.modoTransCargaId").val();
	if ( modoTransCargaId != '') {
		document.getElementById("modoTransCarga").value = modoTransCargaId;
		cargarPuertosCarga();
	}
	//20140610_JMC: Se agrega por el PQT-08
	var modoTransDescargaId = $("#CERTIFICADO_ORIGEN\\.modoTransDescargaId").val();
	if ( modoTransDescargaId != '') {
		document.getElementById("modoTransDescarga").value = modoTransDescargaId;
		cargarPuertosDescarga();
	}

	// 20141209_RPC: Acta UE/AELC (Permite editar paisImportador y se visualiza paisDescarga)	
	$('#selectPaisImportador').change( function(){		
		// 20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas)
		var valor = $(this).val().split("|");
		var idPais = valor[0];
		var idRU = valor[1];
		
		document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;
		
		if (idRU!="0") {
			document.getElementById("CERTIFICADO_ORIGEN.secuenciaPaisImportador").value = idRU;
		} else {
			document.getElementById("CERTIFICADO_ORIGEN.secuenciaPaisImportador").value = "";
		}
	});
	
	$("#selectPaisDescarga").change( function(){		
		// 20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas)
		var valor = $(this).val().split("|");
		var idPais = valor[0];
		var idRU = valor[1];
		
		document.getElementById("CERTIFICADO_ORIGEN.paisDescargaId").value = idPais;
		
		if (idRU!="0") {
			document.getElementById("CERTIFICADO_ORIGEN.secuenciaPaisDescarga").value = idRU;
		} else {
			document.getElementById("CERTIFICADO_ORIGEN.secuenciaPaisDescarga").value = "";
		}
		
		cargarPuertosDescarga();
	});
}

//Cargar Paises Union Europea y AELC // 20141209_RPC: Acta UE/AELC
function cargarPaisesDescarga(seleccionManual) {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisDescarga", "comun.acuerdo_x_pais_importador.select", filter, null, afterCargarPaisesDescarga, null);
}

//20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas), se anadio secuenciaPaisDescarga
function afterCargarPaisesDescarga() { // 20141209_RPC: Acta UE/AELC
	var idPais = $("#idPais").val();
	var paisDescargaId = $("#CERTIFICADO_ORIGEN\\.paisDescargaId").val();
	var secuenciaPaisDescarga = $("#CERTIFICADO_ORIGEN\\.secuenciaPaisDescarga").val();
	if (secuenciaPaisDescarga=="") secuenciaPaisDescarga = "0";
	
	if (paisDescargaId!="") {
		document.getElementById("selectPaisDescarga").value = paisDescargaId+"|"+secuenciaPaisDescarga;
	} else if (idPais!="") {
		document.getElementById("selectPaisDescarga").value = idPais+"|"+secuenciaPaisDescarga;
	}
}

// Cargar pa�ses del importador
function cargarPaisesImportador() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisImportador", "comun.acuerdo_x_pais_importador.select", filter, null, afterCargarPaisesImportador, null); // 20141209_RPC: Acta UE/AELC // 20141209_RPC: Acta UE/AELC (Anterior: comun.pais_iso_x_acuerdo.select)
}

// Despues de cargar pa�ses del importador
//20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas), se anadio secuenciaPaisImportador
function afterCargarPaisesImportador() {
	var idPais = $("#idPais").val();
	var importadorPais = $("#CERTIFICADO_ORIGEN\\.importadorPais").val();
	var secuenciaPaisImportador = $("#CERTIFICADO_ORIGEN\\.secuenciaPaisImportador").val();
	if (secuenciaPaisImportador=="") secuenciaPaisImportador = "0";
	
	// 20141209_RPC: Acta UE/AELC
	if(importadorPais != "") {
		document.getElementById("selectPaisImportador").value = importadorPais+"|"+secuenciaPaisImportador;
	} else {
		document.getElementById("selectPaisImportador").value = idPais+"|"+secuenciaPaisImportador;
		document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;		
	}
	// 20141209_RPC: Acta UE/AELC
	// $("#selectPaisImportador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	// $("#selectPaisImportador").attr("disabled", true);
}

//Funci�n ajax que puebla el combo de puertos de carga
function cargarPuertosCarga() {

	if ($("#modoTransCarga").val() == '') { //20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "select.empty", null, null, null, null);
	} else {
		var filter = "paisIsoId=" + PAIS_PERU + "|modoTransporte=" + $("#modoTransCarga").val(); //20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "comun.puerto.select", filter, null, afterCargarPuertosCarga, null);
	}

}

//Despues de cargar puertos de carga
function afterCargarPuertosCarga() {
 var puertoCargaId = $("#CERTIFICADO_ORIGEN\\.puertoCargaId").val();
	if ( puertoCargaId != '') {
		document.getElementById("puertoCarga").value = puertoCargaId;
	}
}

//Despues de cargar puertos de carga
function afterCargarPuertosDescarga() {
    var puertoDescargaId = $("#CERTIFICADO_ORIGEN\\.puertoDescargaId").val();
	if ( puertoDescargaId != '') {
		document.getElementById("puertoDescarga").value = puertoDescargaId;
	}
}

//Funci�n ajax que puebla el combo de puertos de descarga
function cargarPuertosDescarga() {

	if ($("#modoTransDescarga").val() == '') {//20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "select.empty", null, null, null, null);
	} else {
		// 20141209_RPC: Acta UE/AELC
		//var f = document.formulario;
		var idPais = $("#idPais").val();
		var paisDescargaId = $("#CERTIFICADO_ORIGEN\\.paisDescargaId").val();
		var secuenciaPaisDescarga = $("#CERTIFICADO_ORIGEN\\.secuenciaPaisDescarga").val();
		if (secuenciaPaisDescarga=="") secuenciaPaisDescarga = "0";
		
		var paisIsoId = "-1";
		if (paisDescargaId!="") {
			paisIsoId = paisDescargaId;
		} else if (idPais!="") {
			paisIsoId = idPais;
		}
		var filter = "paisIsoId=" + paisIsoId + "|modoTransporte=" + $("#modoTransDescarga").val()+"|secuenciaPais="+secuenciaPaisDescarga;//20140610_JMC: Se actualiza por el PQT-08
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "comun.puerto.select", filter, null, afterCargarPuertosDescarga, null);
	}

}

//Despues de cargar puertos de descarga
function afterCargarPuertosDescarga() {
  var puertoDescargaId = $("#CERTIFICADO_ORIGEN\\.puertoDescargaId").val();
	if ( puertoDescargaId != '') {
		document.getElementById("puertoDescarga").value = puertoDescargaId;
	}
}

// Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {
	document.getElementById("CERTIFICADO_ORIGEN.modoTransCargaId").value = document.getElementById("modoTransCarga").value;// 20140610_JMC: Se actualiza por el PQT-08
	document.getElementById("CERTIFICADO_ORIGEN.puertoCargaId").value = document.getElementById("puertoCarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.modoTransDescargaId").value = document.getElementById("modoTransDescarga").value;//20140610_JMC: Se agrega por el PQT-08
	document.getElementById("CERTIFICADO_ORIGEN.puertoDescargaId").value = document.getElementById("puertoDescarga").value;
	
	// 20141209_RPC: Acta UE/AELC
	//20151103_JFR: Acta CO_005_2015 - 37 (Regiones Ultraperifericas), se anadio secuenciaPaisDescarga
	var valor = document.getElementById("selectPaisDescarga").value.split("|");
	var idPais = valor[0];
	var idRU = valor[1];
	
	document.getElementById("CERTIFICADO_ORIGEN.paisDescargaId").value = idPais; // 20140522_JFR: Se agrega por el PQT-11

	if (idRU!="0") {
		document.getElementById("CERTIFICADO_ORIGEN.secuenciaPaisDescarga").value = idRU;
	}
}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;
    
    //20140610_JMC: Se actualiza por el PQT-08
    mensaje += mensajeAdicional;
    if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorNombre").value) == "") {
    	mensaje +="\n -El nombre del importador.";
    	changeStyleClass("co.label.certificado.importador_nombre", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_nombre", "labelClass");
    }
    if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorDireccion").value) == "") {
    	mensaje +="\n -La direcci�n del importador.";
    	changeStyleClass("co.label.certificado.importador_direccion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_direccion", "labelClass");
    }

    // Al menos uno de los datos de transporte debe ser ingresado
	var fechaTransporte = document.getElementById("CERTIFICADO_ORIGEN.fechaPartidaTransporte").value;
	var numeroTransporte = document.getElementById("CERTIFICADO_ORIGEN.numeroTransporte").value;
	var puertoCarga = document.getElementById("puertoCarga").value;
	var puertoDescarga = document.getElementById("puertoDescarga").value;

	if (fechaTransporte == '' && numeroTransporte == '' && puertoCarga == '' && puertoDescarga == '') {
		mensaje +="\n -Alguno de los datos de transporte (si selecciona el tipo de puerto tiene tambien deber� seleccionar el puerto).";
	}

    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert(mensaje);
    }
    return ok;
}

/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo Per� - Mexico
function inicializacionFacturaSolicitudSegunAcuerdo() {
	// 20150610_RPC: Acta.33: Mostrar Factura Acuerdos AELC/UE
	$(".trMostrarFactura").attr("style", "display:");
	// Mostrar Factura por defecto seleccionado
	if (document.getElementById("CO_FACTURA.mostrarFactura").value != "N") {
		document.getElementById("mostrarFactura").checked = true;
	}
}

//Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	if (document.getElementById("CO_FACTURA.numero").value == "") {
	    mensaje +="\n -El n�mero";
	    changeStyleClass("co.label.factura.numero", "errorValueClass");
	} else {
		changeStyleClass("co.label.factura.numero", "labelClass");
	}

	if (document.getElementById("CO_FACTURA.fecha").value == ""){
	    mensaje +="\n -La fecha";
	    changeStyleClass("co.label.factura.fecha", "errorValueClass");
	} else {
		changeStyleClass("co.label.factura.fecha", "labelClass");
	}

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

	if (mensaje!="Debe ingresar o seleccionar : ") {
	    ok = false;
	    alert(mensaje);
	}
	return ok;

}

/********* MERCANCIA ***************/

//Inicializa los controles para la mercanc�a del acuerdo
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trMercanciaFactura").attr("style", "display:");
	$("#spanFatura").attr("style", "display:");

	$("#trEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaMarca").attr("style", "display:");
	$("#trMercanciaTipoBulto").attr("style", "display:");
	$("#trMercanciaCantidadBulto").attr("style", "display:");
	$("#trMercanciaCantidad").attr("style", "display:");
	$("#trMercanciaUm").attr("style", "display:");

    // Estas inicializaciones son necesarias pero para todos los acuerdos
    if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == '' ) {
    	document.getElementById("CO_MERCANCIA.esMercanciaGranel").value = 'N';
    }

    // Manejamos la l�gica asociada
    logicaMercanciaGranel(document.getElementById("CO_MERCANCIA.esMercanciaGranel").value);

}

//Maneja la l�gica esperada para el caso de mercancias a granel
function logicaMercanciaGranel(esMercanciaGranel) {
	if ( esMercanciaGranel == 'S' ) {
		document.getElementById("CO_MERCANCIA.cantidadBulto").value = '';
		$("#trMercanciaCantidadBulto").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.marcasNumeros").value = '';
		$("#trMercanciaMarca").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.tipoBulto").value = 'IN BULK';
		// hacer readonly la clase de paquete
        $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
        $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", true);
	} else {
		$("#trMercanciaCantidadBulto").attr("style", "display:");
		$("#trMercanciaMarca").attr("style", "display:");
		// quitamos el readonly de la clase de paquete
        if ( document.getElementById("CO_MERCANCIA.tipoBulto").value == 'IN BULK' ) { // S�lo para este caso limpiamos la informaci�n
        	document.getElementById("CO_MERCANCIA.tipoBulto").value = '';
        }
        $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("readonlyInputTextClass").addClass("inputTextClass");
        $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", false);
	}
}

//Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
}

//Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }

	if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) == "" ) {
		mensaje +="\n -La clase de paquetes.";
		changeStyleClass("co.label.co_mercancia.tipo_bulto", "errorValueClass");
	} else {
		changeStyleClass("co.label.co_mercancia.tipo_bulto", "labelClass");
	}

	if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == 'N' ) { // S�lo cuando no es a granel este campo es visible y obligatorio
		if ( document.getElementById("CO_MERCANCIA.cantidadBulto").value == '' ) {
		    mensaje +="\n -La cantidad de paquetes.";
		    changeStyleClass("co.label.co_mercancia.cantidad_bulto", "errorValueClass");
		} else {
			changeStyleClass("co.label.co_mercancia.cantidad_bulto", "labelClass");
		}

	    if (document.getElementById("CO_MERCANCIA.marcasNumeros").value=="") {
	        mensaje +="\n -Marcas y n�mero de paquete";
	        changeStyleClass("co.label.co_mercancia.marca", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.co_mercancia.marca", "labelClass");
	    }

	    if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) != "" ) {
			var contenidoTipoBulto = document.getElementById("CO_MERCANCIA.tipoBulto").value.toUpperCase();
			var inBulkIngles = 'IN BULK';
			var inBulkEspanol = 'A GRANEL';
			if ( contenidoTipoBulto.indexOf(inBulkIngles) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'IN BULK' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "labelClass");
			}
			if ( contenidoTipoBulto.indexOf(inBulkEspanol) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'A GRANEL' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto", "labelClass");
			}
	    }
	}

    if (document.getElementById("CO_MERCANCIA.cantidad").value=="") {
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.cantidad", "labelClass");
    }

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value=="") {
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.um_fisica_id", "labelClass");
    }

    if (document.getElementById("selectFacturas").value == '') {
        mensaje +="\n -La factura asociada.";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");
    }


    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo EFTA
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {
	$("#trEmitidoPosteriori").attr("style", "display:");
	//$("#trNroCertifOrigen").attr("style", "display:none");
	$("#trNombreImportador").attr("style", "display:");
	$("#trDireccionImportador").attr("style", "display:");
	$("#trPaisImportador").attr("style", "display:");
	$("#trFechaPartida").attr("style", "display:");
	$("#trNumeroTransporte").attr("style", "display:");
	$("#trPuertoCarga").attr("style", "display:");
	$("#trPuertoDescarga").attr("style", "display:");
	
	$("#trModoTransporteCarga").attr("style", "display:"); // 20140610_JMC PQT-08 
	$("#trModoTransporteDescarga").attr("style", "display:"); // 20140610_JMC PQT-08
	$("#spanPaisDescarga").attr("style", "display:"); // 20141209_RPC: Acta UE/AELC
	
	$("#trDatosImportacion").attr("style", "display:none");
	$("#trDatosIniciales").attr("style", "display:");
	$(".trObservaciones").attr("style", "display:");
	$(".trDatosImportacion2").attr("style", "display:");
	$(".trMediosTransporte").attr("style", "display:");
	
	$("#nroReferenciaCertificado").attr('maxlength','10');//20141022_JMC: 31.Acta AELC

	$("#trFechaFirmaExportador").attr("style", "display:"); // 20150612_RPC: Acta.34:Firmas
	$(".trInformacionAdicionalFirma").attr("style", "display:"); // 20150612_RPC: Acta.34:Firmas	
}

/********* MERCANCIA EN EL DOCUMENTO RESOLUTIVO *********/
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaDrMarca").attr("style", "display:");
	$("#trMercanciaDrTipoBulto").attr("style", "display:");
	$("#trMercanciaDrCantidadBulto").attr("style", "display:");
	$("#trMercanciaDrCantidad").attr("style", "display:");
	$("#trMercanciaDrUnidadMedida").attr("style", "display:");
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");

	$("#trNorma").attr("style", "display:none");
	$("#trCriterioOrigen").attr("style", "display:none");
	$("#trCriterioOrigenCertif").attr("style", "display:none");

	if ( document.getElementById("MERCANCIA.ES_MERCANCIA_GRANEL").value == 'SI' ) { // Si el indicador de mercanc�a a granel es si, ocultamos la cantidad y la marca
		$("#trMercanciaDrMarca").attr("style", "display:none");
		$("#trMercanciaDrCantidadBulto").attr("style", "display:none");
	}

}