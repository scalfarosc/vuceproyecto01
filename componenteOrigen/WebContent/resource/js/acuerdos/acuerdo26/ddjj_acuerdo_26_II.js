/************************************************************* ACUERDO PERU - ALIANZA PACIFICO ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "1";
var FILTRO_SECUENCIA_2 = "5";
var FILTRO_SECUENCIA_3 = "2,3,4,6";

var NUM_DIGITOS_PARTIDA = 6;

var alertaOptExportador = alertaOptExportadorSinValidacion;

/************* CALIFICACI�N ORIGEN ************************/

//Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {

	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per�, Chile, Colombia o Mexico, o que todos los insumos sean originarios del Per�, Chile, Colombia o Mexico, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	$("#trCambioClasificacion").attr("style", "display:");

	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");
	$("#trDisposicion").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

}

//Maneja los valores que dependen de los combos din�micos
//Se cre� porque este debe ser invocado desde una funci�n as�ncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {
}

//Validaciones de obligatoriedad para el criterio de calificaci�n
function validarCamposCriterioCalificacion(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.20.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.20.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.20.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n�mero de d�gitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.20.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.20.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.20.naladisa", "errorValueClass");
	} else {
		changeStyleClass("co.label.dj.20.naladisa", "labelClass");
	}
	*/
	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci�n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {
	alertaMaterial = 'Se entiende por material a una mercanc�a que es utilizada en la producci�n de otra mercanc�a, incluyendo cualquier componente, ingrediente, materia prima, parte o pieza';
	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per�, Chile, Colombia o Mexico, o que todos los insumos sean originarios del Per�, Chile, Colombia o Mexico, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	$("#trPesoNetoMercancia").attr("style", "display:");

	$("#trCambioClasificacion").attr("style", "display:");
	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");
	if (document.getElementById("DJ.cumpleOtroCriterio").value == 'S') { // Si esta marcado otro criterio, debe ser editable, caso contrario, debe ser no editable
		document.getElementById("DJ.pesoNetoMercancia").value = '';
		$("#DJ\\.pesoNetoMercancia").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$("#DJ\\.pesoNetoMercancia").attr("disabled", true);
	} else {
		$("#DJ\\.pesoNetoMercancia").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		$("#DJ\\.pesoNetoMercancia").attr("disabled", false);
	}
    /*$('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialSgpJapon );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de Jap�n");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de Jap�n");*/

    $('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialALIANZA );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de Chile, Colombia o Mexico");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de Chile, Colombia o Mexico");
    $('#nuevoMaterialTercerComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialAPAdicionales );
	$("#nuevoMaterialTercerComponenteButton").attr("value", "Adicionar Material Originario de Chile, Colombia o Mexico");
	$("#nuevoMaterialTercerComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de Chile, Colombia o Mexico");

	if ($("#matPeruEmpty").val() == "1"){
		msgMaterialPeru = 'El material debe ser originario de Per� de acuerdo a lo establecido en el Acuerdo, no por el hecho de que Ud. haya comprado un material en el mercado local o que el material haya sido producido en el Per�, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Per� a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}

	if ($("#mat2doCompEmpty").val() == "1"){
		msgMaterial2doComp = 'El material debe ser originario de Chile, Colombia o Mexico de acuerdo a lo establecido en el Acuerdo, no por el hecho de que Ud. haya comprado un material en el mercado de Chile, Colombia o Mexico o que el material haya sido producido en Chile, Colombia o Mexico, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Chile, Colombia o Mexico a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}
}

//Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialALIANZA() {
	if (msgMaterial2doComp != ''){
		alert(msgMaterial2doComp);
	}
	nuevoMaterial('AP');
}

//Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialAPAdicionales() {
	nuevoMaterial('APT');
}

//Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField) {
	editarMaterial(keyValues, keyValuesField, "AP"); // AP: ALIANZA PACIFICO
}

//Edita el material del segundo componente segun el acuerdo
function editarMaterialTercerComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "APT"); // APT: ALIANZA PACIFICO Tercer Componente
}

// Esta funci�n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

//Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return false;
}


/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci�n de controles de materiales seg�n el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
}

// Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;

	if (tipoPais == 'P') { // Si es Per� se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");
		
	} else if (tipoPais == 'AP') { // Si es Alianza Pac�fico se muestra s�lo el nombre del fabricante
		$("#trProcedencia").attr("style", "display:");
		$("#trNombreFabricante").attr("style", "display:");
		cargarPaisesProcedencia();
		cargarPaisesOrigen();
		
	} else if (tipoPais == 'APTT'){ // Si es Originario de Chile, Colombia o Mexico
		$("#trProcedencia").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		$("#trNombreFabricante").attr("style", "display:");
		cargarPaisesOrigenOtros();
	} else { // Si es No Originario se muestra el pais de procedencia y se muestra el peso
		$("#trProcedencia").attr("style", "display:");
		$("#trPesoMaterial").attr("style", "display:");
		$("#spanPesoMaterial").attr("style", "display:none")
		//cargarPaisesProcedenciaNoOriginario();
	}

	$("#spanValorUs").attr("style", "display:");

}

// Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa�s es Per�
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'AP') { // Si el pa�s es ALIANZA PACIFICO
		//document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
	} else if (tipoPais == 'APT') { // Si el pa�s es Costa Rica y Panama
		//document.getElementById("DJ_MATERIAL.paisProcedencia").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
		//$('#DJ_MATERIAL\\.paisOrigen').removeAttr("onChange").unbind('change').change( asignarPaisProcedencia );
	}else {
		$('#DJ_MATERIAL\\.paisOrigen').removeAttr("onChange").unbind('change');
	}


}

//Cargar pa�ses procedencia
function cargarPaisesProcedencia() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisProcedencia").value = document.getElementById("DJ_MATERIAL.paisProcedencia").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisProcedencia", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesProcedencia, null);
}

function cargarPaisesProcedenciaNoOriginario() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisProcedencia").value = document.getElementById("DJ_MATERIAL.paisProcedencia").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisProcedencia", "comun.pais_iso_no_originario_x_acuerdo.select", filter, null, afterCargarPaisesProcedencia, null);
}

// Despu�s de cargar los pa�ses de procedencia
function afterCargarPaisesProcedencia() {
	document.getElementById("DJ_MATERIAL.paisProcedencia").value = document.getElementById("tempPaisProcedencia").value;
}

//Cargar pa�ses origen
function cargarPaisesOrigen() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisOrigen").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisOrigen", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesOrigen, null);
}

//Despu�s de cargar los pa�ses de origen
function afterCargarPaisesOrigen() {
	document.getElementById("DJ_MATERIAL.paisOrigen").value = document.getElementById("tempPaisOrigen").value;
}

//Cargar pa�ses origen
function cargarPaisesOrigenOtros() {
	var idAcuerdos = PAISES_CENTRO_AMERICA;
	var filter = "paisesCod=" + idAcuerdos;
	document.getElementById("tempPaisOrigen").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisOrigen", "comun.pais_iso.masivo.select", filter, null, afterCargarPaisesOrigen, null);
}

//Cargar paises de productor
function cargarPaisesProductor() {	
	var idAcuerdo = document.getElementById("idAcuerdo").value;		
	var filter = "idAcuerdo=" + idAcuerdo + "|idPaisAcumulado=PE"; //Se agregan Peru
	document.getElementById("tempPaisProductor").value = document.getElementById("DJ_PRODUCTOR.pais").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_PRODUCTOR.pais", "comun.pais_iso_alfa2_x_acuerdo.select", filter, null, afterCargarPaisesProductor, null);
}
//Despues de cargar los paises de procedencia
function afterCargarPaisesProductor() {
	document.getElementById("DJ_PRODUCTOR.pais").value = document.getElementById("tempPaisProductor").value;
}

//Para poder asignar el pa�s de procedencia en base a un pa�s de origen
function asignarPaisProductor() {
	document.getElementById("DJ_PRODUCTOR.pais").value = 'PE';
	//$("#DJ_PRODUCTOR\\.pais").attr('disabled',true);
	
}

// Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {
    var ok = true;
    var f = document.formulario;
    var tipoPais = f.tipoPais.value;

    var mensaje="Debe ingresar o seleccionar : ";

    if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
        mensaje +="\n -La descripci�n del material.";
        changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
        mensaje +="\n -La partida arancelaria.";
        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
    } else {
    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
    }

    if (tipoPais == 'P' || tipoPais == 'AP') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
    }

    if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n�mero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n�mero de documento del fabricante v�lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8;
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n�mero correcto de d�gitos para el n�mero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
    }

    if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida.";
        changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
    }


    // El valor US es obligatorio si NO est� marcado el check de clasificaci�n arancelaria
	if ($("#criterioArancelario").val() != '2') {

	    if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
	        mensaje +="\n -El valor en US$.";
	        changeStyleClass("co.label.dj_material.valor_en_us", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.valor_en_us", "labelClass");
	    }

	}
	


    if (mensaje != "Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }

    return ok;
}

function habilitarXAcuerdo(){
	var tipoPais = document.formulario.tipoPais.value;
	var idPaisSegundoComponente = document.formulario.idPaisSegundoComponente.value;

	if (tipoPais == 'AP' ){
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
		$("#trOrigen").attr("style", "display:");
	}

}

/************* DJ PRODUCTOR ************************/
//VERSION 2
function inicializacionDeclaracionJuradaProductor(){	
	$("#trProductorTelefono").attr("style", "display:");
	$("#trProductorEmail").attr("style", "display:");
	$("#trProductorPais").attr("style", "display:"); // JMC 19/06/2017 Version 2 Alianza Pacifico	
	$("#trProductorCiudad").attr("style", "display:"); // JMC 19/06/2017 Version 2 Alianza Pacifico
	cargarPaisesProductor();
	
	
	$('#DJ_PRODUCTOR\\.documentoTipo').change( function(){
		if($(this).val()==1){
			asignarPaisProductor();
		}
		
	});
	
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    if (document.getElementById("DJ_PRODUCTOR.telefono").value==""){
        mensaje +="\n -El tel�fono del productor";
        changeStyleClass("co.label.usuario_dj.telefono", "errorValueClass");
    }else changeStyleClass("co.label.usuario_dj.telefono", "labelClass");

    return mensaje;
}

