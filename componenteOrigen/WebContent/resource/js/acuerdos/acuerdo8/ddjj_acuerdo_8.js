/************************************************************* ACUERDO PERU - TLC PERU CHINA ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "1";
var FILTRO_SECUENCIA_2 = "3";
var FILTRO_SECUENCIA_3 = "2,4";

var NUM_DIGITOS_PARTIDA = 6;

var alertaOptExportador = alertaOptExportadorSinValidacion;

/************* CALIFICACI�N ORIGEN ************************/

//Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {

	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per� o China, o que todos los insumos sean originarios del Per� o China, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	$("#trCambioClasificacion").attr("style", "display:");

	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

	$("#trPorcentajeSegunCriterio").attr("style", "display:");

}

//Maneja los valores que dependen de los combos din�micos
//Se cre� porque este debe ser invocado desde una funci�n as�ncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {
    
	var secOrigen = document.getElementById("CALIFICACION.secuenciaOrigen").value;
	if ( secOrigen != '' && ( secOrigen == '3' 
		|| secOrigen == '4' // 20140926JFR: Acta China
			) ) { // Si no la secuencia origen es REO
		$('#CALIFICACION\\.porcentajeSegunCriterio').removeClass("readonlyInputTextClass").addClass("inputTextClass");
		$('#CALIFICACION\\.porcentajeSegunCriterio').attr("readonly", false);
		$('#CALIFICACION\\.porcentajeSegunCriterio').attr("disabled", false);
        
		// 20140926JFR: Acta China
		if (secOrigen == '4') {
			$('#spanPorcentajeSegunCriterio').attr("style", "display:");
		}
	} else { // Caso contrario debe limpiarse (porque por defecto esta oculto)
		$('#CALIFICACION\\.porcentajeSegunCriterio').removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$('#CALIFICACION\\.porcentajeSegunCriterio').attr("readonly", true);
		$('#CALIFICACION\\.porcentajeSegunCriterio').attr("disabled", true);

		$('#spanPorcentajeSegunCriterio').attr("style", "display:none");  // 20140926JFR: Acta China
	}

	// 20141223_RPC: Acta Modificaciones TLC Peru–China: Mostrar nuevamente Porcentaje de VCR
	if (secOrigen != '3') {
		$('#trPorcentajeSegunCriterio').show();		
	}
}


//Validaciones de obligatoriedad para el criterio de calificaci�n
function validarCamposCriterioCalificacion(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.8.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.8.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.8.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n�mero de d�gitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.8.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.8.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.8.naladisa", "errorValueClass");
	} else {
		changeStyleClass("co.label.dj.8.naladisa", "labelClass");
	}
	*/
	
	// 20140926JFR: Acta China
	var secOrigen = document.getElementById("CALIFICACION.secuenciaOrigen").value;
	if ( secOrigen != '' && ( secOrigen == '4') ) { // Si no es un nuevo y es 4, debe ser obligatorio
	    if ( document.getElementById("CALIFICACION.porcentajeSegunCriterio").value == "" ) {
	        mensaje +="\n -El Porcentaje de VCR.";
	        changeStyleClass("co.label.dj.prctj_segun_criterio", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj.prctj_segun_criterio", "labelClass");
	    }
	}

	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci�n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {

	// 20141223_RPC: Acta Modificaciones TLC Peru–China: No mostrar Porcentaje de VCR en DDJJ
	// $("#trPorcentajeSegunCriterio").attr("style", "display:");
	$("#trCambioClasificacion").attr("style", "display:");
	$("#solicitarValidacionButton").attr("style", "display:none");
	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");
	var secOrigen = document.getElementById("secuenciaOrigen").value;
	if ( secOrigen != '3' 
		&& secOrigen != '4'  // 20140926JFR: Acta China
		&& document.getElementById("DJ.porcentajeSegunCriterio") != null ) {
		document.getElementById("DJ.porcentajeSegunCriterio").value = '';
		$("#DJ\\.porcentajeSegunCriterio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
		$("#DJ\\.porcentajeSegunCriterio").attr("disabled", true);
	} else {
		$("#DJ\\.porcentajeSegunCriterio").removeClass("readonlyInputTextClass").addClass("inputTextClass");
		$("#DJ\\.porcentajeSegunCriterio").attr("disabled", false);
	}
    $('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialChina );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de China");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de China");
}

//Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialChina() {
	nuevoMaterial('CH');
}

//Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField) {
	editarMaterial(keyValues, keyValuesField, "CH"); // CH: China
}

//Esta funci�n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

//Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return false;
}


/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci�n de controles de materiales seg�n el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
}

// Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	//var secuenciaMaterial = document.getElementById("DJ_MATERIAL.secuenciaMaterial").value;

	if (tipoPais == 'P') { // Si es Per� se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");
		/*if (secuenciaMaterial != '') {
			$("#tablaSubtituloAdjunto").attr("style", "display:");
			$("#tablaBotonesAdjunto").attr("style", "display:");
			$("#tablaMaterialAdjuntos").attr("style", "display:");
		}*/
	} else if (tipoPais == 'CH') { // Si es China se muestra s�lo el nombre del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		/*if (secuenciaMaterial != '') {
			$("#tablaSubtituloAdjunto").attr("style", "display:");
			$("#tablaBotonesAdjunto").attr("style", "display:");
			$("#tablaMaterialAdjuntos").attr("style", "display:");
		}*/
	} else { // Si es No Originario se muestra el pais de procedencia y se muestra el peso
		$("#trProcedencia").attr("style", "display:");
		if ($("#criterioArancelario").val() == '3') { // Si esta marcado otro criterio, debemos mostrar el peso
			$("#trPesoMaterial").attr("style", "display:");
		}
	}

	$("#spanValorUs").attr("style", "display:");

	// Controles comunes a todos
	//$("#trMaterialPartidaAcuerdo").attr("style", "display:");

}


// Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa�s es Per�
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'CH') { // Si el pa�s es China
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
	}

}

// Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {

    var ok = true;
    var f = document.formulario;
    var tipoPais = f.tipoPais.value;

    var mensaje="Debe ingresar o seleccionar : ";

    if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
        mensaje +="\n -La descripci�n del material.";
        changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
        mensaje +="\n -La partida arancelaria.";
        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
    } else {
    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
    }

    if (tipoPais == 'P' || tipoPais == 'CH') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
    }

    if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n�mero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n�mero de documento del fabricante v�lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n�mero correcto de d�gitos para el n�mero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
    }

    if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida.";
        changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
    }

    if (tipoPais == 'N') {
    	// Si cumple principio otro criterio, el peso es obligatorio
    	if ($("#criterioArancelario").val() == '3') {
    	    if ( document.getElementById("DJ_MATERIAL.pesoNeto").value == "" ) {
    	        mensaje +="\n -El peso neto.";
    	        changeStyleClass("co.label.dj_material.peso_material", "errorValueClass");
    	    } else {
    	    	changeStyleClass("co.label.dj_material.peso_material", "labelClass");
    	    }
    	}
    }

    // El valor US es obligatorio si NO est� marcado el check de clasificaci�n arancelaria
	if ($("#criterioArancelario").val() != '2') {

	    if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
	        mensaje +="\n -El valor en US$.";
	        changeStyleClass("co.label.dj_material.valor_en_us", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.valor_en_us", "labelClass");
	    }

	}


    if (mensaje != "Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }

    return ok;
}

function habilitarXAcuerdo(){
}

/************* DJ PRODUCTOR ************************/
function inicializacionDeclaracionJuradaProductor(){
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    return mensaje;
}

