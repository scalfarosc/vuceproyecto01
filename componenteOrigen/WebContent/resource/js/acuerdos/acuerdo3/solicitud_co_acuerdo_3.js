/********* SOLICITUD ***************/

var NUM_DIGITOS_PARTIDA = 8;

// Inicializa los controles para el formato ACE Per� - Cuba
function inicializacionSolicitudSegunAcuerdo() {
	$(".trDatosImportador").attr("style", "display:");
	cargarPaisesImportador();
}

//Cargar pa�ses del importador
function cargarPaisesImportador() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisImportador", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesImportador, null);
}

//Despues de cargar pa�ses del importador
function afterCargarPaisesImportador() {
	var idPais = $("#idPais").val();
	document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;
	document.getElementById("selectPaisImportador").value = idPais;
	$("#selectPaisImportador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	$("#selectPaisImportador").attr("disabled", true);
}

//Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {
}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

    mensaje += mensajeAdicional;

    if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorNombre").value) == "") {
    	mensaje +="\n -El nombre del importador.";
    	changeStyleClass("co.label.certificado.importador_nombre", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_nombre", "labelClass");
    }

    if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.importadorDireccion").value) == "") {
    	mensaje +="\n -La direcci�n del importador.";
    	changeStyleClass("co.label.certificado.importador_direccion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_direccion", "labelClass");
    }

    /*if ($.trim(document.getElementById("CERTIFICADO_ORIGEN.observacion").value)=="") {
        mensaje +="\n -Las observaciones";
        changeStyleClass("co.label.certificado.observacion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.observacion", "labelClass");
    }*/

    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert (mensaje);
    }
    return ok;
}

/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo Per� - Cuba
function inicializacionFacturaSolicitudSegunAcuerdo() {
	$(".trOperadorTercerPais").attr("style", "display:");
	$(".trNombreOperadorTercerPais").attr("style", "display:");
	$(".trDireccionOperadorTercerPais").attr("style", "display:");
	$(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	manejaNombreOperador(document.getElementById("CO_FACTURA.indicadorTercerPais"));
}

//Maneja la coherencia entre el check de operador de tercer pais y el nombre del operador
function manejaNombreOperador(combo) {
	// Si esta seleccionado el checkbox, debemos activar la caja de texto, caso contrario debe estar desactivada
	if ( combo.value == 'S' ) {
		$(".trNombreOperadorTercerPais").attr("style", "display:");
        $("#spanNombreOperador").attr("style", "display:");
		$(".trDireccionOperadorTercerPais").attr("style", "display:");
        $("#spanDireccionOperador").attr("style", "display:");
        $(".trTieneFacturaOperadorTercerPais").attr("style", "display:");
	} else {
		$(".trNombreOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.nombreOperadorTercerPais" ).val("");
        $("#spanNombreOperador").attr("style", "display:none");
		$(".trDireccionOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.direccionOperadorTercerPais" ).val("");
        $("#spanDireccionOperador").attr("style", "display:none");
        $(".trTieneFacturaOperadorTercerPais").attr("style", "display:none");
        $("#CO_FACTURA\\.tieneFacturaTercerPais").val("N");
	}

}

//Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado(){

  var ok = true;
  var mensaje="Debe ingresar o seleccionar : ";

  if (document.getElementById("CO_FACTURA.indicadorTercerPais").value == 'S') {
      if (document.getElementById("CO_FACTURA.nombreOperadorTercerPais").value == "") {
          mensaje +="\n -El nombre del operador del tercer pa�s";
          changeStyleClass("co.label.factura.nombre_operador", "errorValueClass");
      } else {
      	changeStyleClass("co.label.factura.nombre_operador", "labelClass");
      }
      if (document.getElementById("CO_FACTURA.direccionOperadorTercerPais").value == "") {
          mensaje +="\n -La direcci�n del operador del tercer pa�s";
          changeStyleClass("co.label.factura.direccion_operador", "errorValueClass");
      } else {
      	changeStyleClass("co.label.factura.direccion_operador", "labelClass");
      }
  }

  if (document.getElementById("CO_FACTURA.numero").value == "") {
      mensaje +="\n -El n�mero";
      changeStyleClass("co.label.factura.numero", "errorValueClass");
  } else {
  	changeStyleClass("co.label.factura.numero", "labelClass");
  }

  if (document.getElementById("CO_FACTURA.fecha").value == ""){
      mensaje +="\n -La fecha";
      changeStyleClass("co.label.factura.fecha", "errorValueClass");
  } else {
  	changeStyleClass("co.label.factura.fecha", "labelClass");
  }

  if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
  }

  if (mensaje!="Debe ingresar o seleccionar : ") {
      ok= false;
      alert (mensaje);
  }
  return ok;
}

/********* MERCANCIA ***************/

// Inicializa los controles para la mercanc�a del acuerdo ACE Per� - Cuba
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trMercanciaFactura").attr("style", "display:");
	$("#trMercanciaCantidad").attr("style", "display:");
	$("#trMercanciaUm").attr("style", "display:");
	$("#trMercanciaValor").attr("style", "display:");
	$("#trMercanciaPartidaAcuerdo").attr("style", "display:");
	
	$("#CO_MERCANCIA\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);
	
	asignarValorFactura(document.getElementById("selectFacturas"));
}

function valida_longitud_naladisa(obj,validarLongitud) {
	valida_longitud_naladisa_digitos(obj,validarLongitud,NUM_DIGITOS_PARTIDA);
}

// Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
	asignarValorFactura(document.getElementById("selectFacturas"));
}

//Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }

    if ( $.trim(document.getElementById("CO_MERCANCIA.partidaSegunAcuerdo").value) == "" ) {
        mensaje +="\n -"+document.getElementById("co.label.co_mercancia.3.naladisa").innerHTML;
        changeStyleClass("co.label.co_mercancia.3.naladisa", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.3.naladisa", "labelClass");
    }
    
    if (document.getElementById("CO_MERCANCIA.cantidad").value==""){
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.cantidad", "labelClass");

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value==""){
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.um_fisica_id", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.um_fisica_id", "labelClass");

    if (document.getElementById("selectFacturas").value==""){
        mensaje +="\n -El n�mero de factura";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");

    if (document.getElementById("CO_MERCANCIA.valorFacturadoUs").value==""){
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.valor_facturado", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.valor_facturado", "labelClass");

    /*if (document.getElementById("CO_MERCANCIA.naladisa").value==""){
        mensaje +="\n -Naladisa";
        changeStyleClass("co.label.co_mercancia.naladisa", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.naladisa", "labelClass");

    if (document.getElementById("CO_MERCANCIA.peso").value==""){
        mensaje +="\n -El peso";
        changeStyleClass("co.label.co_mercancia.peso", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.peso", "labelClass");

    if (document.getElementById("CO_MERCANCIA.tipoEnvase").value==""){
        mensaje +="\n -El tipo de envase";
        changeStyleClass("co.label.co_mercancia.tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.tipo_envase", "labelClass");

    if (document.getElementById("CO_MERCANCIA.descripcionTipoEnvase").value==""){
        mensaje +="\n -La descripci�n del tipo de envase";
        changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "labelClass");
*/
    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo ACE Per� - Cuba
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {
	$("#trNroCertifEntidad").attr("style", "display:none");
	$("#trDatosImportacion").attr("style", "display:none");
	$("#trDatosIniciales").attr("style", "display:");
	$(".trObservaciones").attr("style", "display:");
}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo Per� - Cuba
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrCantidad").attr("style", "display:");
	$("#trMercanciaDrUnidadMedida").attr("style", "display:");
	$("#trMercanciaDrValorFacturado").attr("style", "display:");
}
