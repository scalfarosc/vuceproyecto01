/************************************************************* ACUERDO PERU - SGPC ***********************************************************************/

var PAIS_PERU = '168';

var FILTRO_SECUENCIA_1 = "1";
var FILTRO_SECUENCIA_2 = "-1";
var FILTRO_SECUENCIA_3 = "2,3,4";

var NUM_DIGITOS_PARTIDA = 8;

var alertaOptExportador = alertaOptExportadorSinValidacion;

/************* CALIFICACI�N ORIGEN ************************/

//Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionCalificacionOrigenSegunAcuerdo() {
    
	msgCriterio1 = 'El hecho que una mercanc�a sea completamente producida en el Per�, Bolivia, Ecuador o Colombia, o que todos los insumos sean originarios del Per�, Bolivia, Ecuador o Colombia, no significa que la mercanc�a sea "totalmente obtenida" o "enteramente producida". Ver Ayuda.';

	//$("#trCambioClasificacion").attr("style", "display:");
	//$("#trPorcentajeSegunCriterio").attr("style", "display:");

	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");

	//$("#trDjPartidaAcuerdo").attr("style", "display:");

	$("#CALIFICACION\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);

	mostrarNotaProductores();

}

//Maneja los valores que dependen de los combos din�micos
//Se cre� porque este debe ser invocado desde una funci�n as�ncrona
function manejarValoresDependientesDeCombosDinamicosSegunAcuerdo() {
}

//Validaciones de obligatoriedad para el criterio de calificaci�n
function validarCamposCriterioCalificacion(){

	var ok = true;
	var mensaje="Debe ingresar o seleccionar : ";

	var criterioOrigen = getSelectedRadioValue(document.formulario.elements["CALIFICACION.criterioQueCumple"]);
	if ( criterioOrigen == '' ) { // No hay radio de criterio seleccionado
	    mensaje +="\n -El Criterio de Origen";
	}

	if ( ($("#trCriterioOrigenCertif").attr("style") == "display:" || $("#trCriterioOrigenCertif").attr("style") == "") || $("#trCriterioOrigenCertif").attr("style") == "" ) {

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaNorma").value) == "" ) {
		        mensaje +="\n -La norma";
		        changeStyleClass("co.label.dj.norma", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.norma", "labelClass");
		    }

		    if ( $.trim(document.getElementById("CALIFICACION.secuenciaOrigen").value) == "" ) {
		        mensaje +="\n -El criterio de origen";
		        changeStyleClass("co.label.dj.criterio_origen", "errorValueClass");
		    } else {
		    	changeStyleClass("co.label.dj.criterio_origen", "labelClass");
		    }

	}
	/*
	if ( ($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value) == "" ) {
	    mensaje +="\n -El campo " + $.trim(document.getElementById('co.label.dj.1.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.1.naladisa').innerHTML).length - 1);
	    changeStyleClass("co.label.dj.1.naladisa", "errorValueClass");
	} else if (($("#trDjPartidaAcuerdo").attr("style") == 'display:' || $("#trDjPartidaAcuerdo").attr("style") == "") && $.trim(document.getElementById("CALIFICACION.partidaSegunAcuerdo").value).length != NUM_DIGITOS_PARTIDA){
		mensaje +="\n -El n�mero de d�gitos correcto para el campo " + $.trim(document.getElementById('co.label.dj.1.naladisa').innerHTML).substring(0, $.trim(document.getElementById('co.label.dj.1.naladisa').innerHTML).length - 1) ;
		changeStyleClass("co.label.dj.1.naladisa", "errorValueClass");
	} else {
		changeStyleClass("co.label.dj.1.naladisa", "labelClass");
	}
	*/
	if( mensaje != "Debe ingresar o seleccionar : " ) {
	    ok = false;
	    alert(mensaje);
	}

	return ok;
}

//Confirmaciones adicionales que la grabaci�n de criterio deba recibir
function confirmacionesCriterioCalificacion() {
	return true;
}

/************* GENERAL DECLARACION JURADA ************************/

// Realiza la inicializaci�n de controles seg�n el acuerdo
function inicializacionDeclaracionJuradaSegunAcuerdo() {
	alertaMaterial = 'Se entiende por materiales a las materias primas, los insumos, los productos intermedios, las partes y piezas, componentes, subensamblajes que se incorporen en la obtenci�n de otra mercanc�a';

	//$("#trDjPartidaAcuerdo").attr("style", "display:");
	//$("#trPorcentajeSegunCriterio").attr("style", "display:");
	$("#trNorma").attr("style", "display:");
	$("#trCriterioOrigen").attr("style", "display:");
	$("#trCriterioOrigenCertif").attr("style", "display:");
	$("#lnkPaises2doComponente").attr("style", "display:");

	$("#DJ\\.porcentajeSegunCriterio").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	$("#DJ\\.porcentajeSegunCriterio").attr("disabled", true);
    $('#nuevoMaterialSegundoComponenteButton').removeAttr("onClick").unbind('click').click( nuevoMaterialSGPC );
	$("#nuevoMaterialSegundoComponenteButton").attr("value", "Adicionar Material Originario de los pa�ses del SGPC");
	$("#nuevoMaterialSegundoComponenteButton").attr("title", "Pulse aqu� para agregar un Nuevo Material Originario de los pa�ses del SGPC");

	if ($("#matPeruEmpty").val() == "1"){
		msgMaterialPeru = 'El material debe ser originario de Per� de acuerdo a lo establecido en el Acuerdo, no por el hecho de que Ud. haya comprado un material en el mercado local o que el material haya sido producido en el Per�, significa que califica como originario. Para ello debe asegurarse que el material sea originario de Per� a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}

	if ($("#mat2doCompEmpty").val() == "1"){
		msgMaterial2doComp = 'El material debe ser originario de alguno de los paises beneficiarios de acuerdo a lo establecido en el SGPC, no por el hecho de que Ud. haya comprado un material en el mercado de ese segundo Pais o que el material haya sido producido en ese Segundo Pais, significa que califica como originario. Para ello debe asegurarse que el material sea originario de alguno de los paises beneficiarios a trav�s de documentaci�n tales como: Declaraciones Juradas del productor, affidavits, entre otros. Si Ud. no est� seguro del origen del material, le sugerimos que lo declare como no originario';
	}
}

// Funci�n puente para poder invocar a la funci�n general con el par�metro correcto
function nuevoMaterialSGPC() {
	if (msgMaterial2doComp != ''){
		alert(msgMaterial2doComp);
	}

	nuevoMaterial('SG');
}

// Edita el material del segundo componente segun el acuerdo
function editarMaterialSegundoComponente(keyValues, keyValuesField){
	editarMaterial(keyValues, keyValuesField, "SG"); // SG: SGPC
}

//Esta funci�n manipula datos al momento de seleccionar subpartida, es dependiente del acuerdo
function cargarDataPartidaSegunAcuerdo() {
}

// Indica si el evaluador debe validar la partida segun acuerdo
function validarPartidaSegunAcuerdo(){
	return false;
}


/************* MATERIALES DECLARACION JURADA ***********************/

//Realiza la inicializaci�n de controles de materiales seg�n el acuerdo
function inicializacionDeclaracionJuradaMaterialSegunAcuerdo() {
	mostrarControles();
	asignarValores();
}

// Oculta los controles de acuerdo al pais y al acuerdo
function mostrarControles() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;

	if (tipoPais == 'P') { // Si es Per� se muestran todos los datos del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trDocumentoTipoFabricante").attr("style", "display:");
		$("#trDocumentoNumeroFabricante").attr("style", "display:");
	} else if (tipoPais == 'SG') { // Si es SGPC se muestra s�lo el nombre del fabricante
		$("#trNombreFabricante").attr("style", "display:");
		$("#trProcedencia").attr("style", "display:");
		$("#trOrigen").attr("style", "display:");
		$("#spanPaisProcedencia").attr("style", "display:");
		$("#spanPaisOrigen").attr("style", "display:");
		cargarPaisesProcedencia();
		cargarPaisesOrigen();
	} else { // Si es No Originario se muestra el pais de procedencia
		$("#trProcedencia").attr("style", "display:");
		$("#spanPaisProcedencia").attr("style", "display:");
		cargarPaisesProcedenciaNoOriginario();
		//cargarPaisesProcedencia();
	}

	// Controles comunes a todos
	//$("#trMaterialPartidaAcuerdo").attr("style", "display:");
	$("#spanValorUs").attr("style", "display:");

}

// Realiza asignacion de valores de acuerdo al tipo de pais
function asignarValores() {

	var f = document.formulario;
	var tipoPais = f.tipoPais.value;
	//var idPaisSegundoComponente = f.idPaisSegundoComponente.value;

	if (tipoPais == 'P') { // Si el pa�s es Per�
		document.getElementById("DJ_MATERIAL.paisProcedencia").value = PAIS_PERU;
		document.getElementById("DJ_MATERIAL.paisOrigen").value = PAIS_PERU;
	} else if (tipoPais == 'SG') { // Si el pa�s es SGPC
		//document.getElementById("DJ_MATERIAL.paisProcedencia").value = idPaisSegundoComponente;
		//document.getElementById("DJ_MATERIAL.paisOrigen").value = idPaisSegundoComponente;
	}

}

// Cargar pa�ses procedencia
function cargarPaisesProcedencia() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisProcedencia").value = document.getElementById("DJ_MATERIAL.paisProcedencia").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisProcedencia", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesProcedencia, null);
}

function cargarPaisesProcedenciaNoOriginario() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisProcedencia").value = document.getElementById("DJ_MATERIAL.paisProcedencia").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisProcedencia", "comun.pais_iso_no_originario_x_acuerdo.select", filter, null, afterCargarPaisesProcedencia, null);
}

// Despu�s de cargar los pa�ses de procedencia
function afterCargarPaisesProcedencia() {
	document.getElementById("DJ_MATERIAL.paisProcedencia").value = document.getElementById("tempPaisProcedencia").value;
}

// Cargar pa�ses origen
function cargarPaisesOrigen() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	document.getElementById("tempPaisOrigen").value = document.getElementById("DJ_MATERIAL.paisOrigen").value;
	loadSelectAJX("ajax.selectLoader", "loadList", "DJ_MATERIAL.paisOrigen", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesOrigen, null);
}

//Despu�s de cargar los pa�ses de origen
function afterCargarPaisesOrigen() {
	document.getElementById("DJ_MATERIAL.paisOrigen").value = document.getElementById("tempPaisOrigen").value;
}

// Validaciones de obligatoriedad
function validarCamposDeclaracionJuradaMaterial() {

    var ok = true;
    var f = document.formulario;
    var tipoPais = f.tipoPais.value;

    var mensaje="Debe ingresar o seleccionar : ";

    if ( document.getElementById("DJ_MATERIAL.descripcion").value == "" ) {
        mensaje +="\n -La descripci�n del material.";
        changeStyleClass("co.label.dj_material.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.descripcion", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.partidaArancelaria").value == "" ) {
        mensaje +="\n -La partida arancelaria.";
        changeStyleClass("co.label.partida_arancelaria", "errorValueClass");
    } else {
    	changeStyleClass("co.label.partida_arancelaria", "labelClass");
    }

    if (tipoPais == 'P' || tipoPais == 'SG') {
	    if ( document.getElementById("DJ_MATERIAL.fabricanteNombre").value == "" ) {
	        mensaje +="\n -El nombre del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_nombre", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_nombre", "labelClass");
	    }
    }

    if (tipoPais != 'P') {
	    if ( document.getElementById("DJ_MATERIAL.paisProcedencia").value == "" ) {
	        mensaje +="\n -El pa�s de procedencia.";
	        changeStyleClass("co.label.dj_material.pais_procedencia", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.pais_procedencia", "labelClass");
	    }
    }

    if (tipoPais == 'SG') {
	    if ( document.getElementById("DJ_MATERIAL.paisOrigen").value == "" ) {
	        mensaje +="\n -El pa�s de origen.";
	        changeStyleClass("co.label.dj_material.pais_origen", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.pais_origen", "labelClass");
	    }
    }

    if (tipoPais == 'P') {

	    if ( document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "" ) {
	        mensaje +="\n -El tipo de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.fabricante_documento_tipo", "labelClass");
	    }

	    if ( document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value == "" ) {
	        mensaje +="\n -El n�mero de documento del fabricante.";
	        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    } else {

	    	if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value != "" ) {
	    		if (!validarEnteroPositivo(document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento"),false,"Debe ingresar un numero entero positivo en el campo documento")) {
	    			mensaje +="\n -Un n�mero de documento del fabricante v�lido.";
			        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    		} else {

	    			var numCaracteres = 8
	    			if (document.getElementById("DJ_MATERIAL.fabricanteDocumentoTipo").value == "1"){
	    				numCaracteres = 11;
	    			}

	    			if (document.getElementById("DJ_MATERIAL.fabricanteNumeroDocumento").value.length != numCaracteres) {
	    				mensaje +="\n -Un n�mero correcto de d�gitos para el n�mero de documento del fabricante.";
				        changeStyleClass("co.label.dj_material.fabricante_numero_documento", "errorValueClass");
	    			}
	    		}
	    	} else {
		    	changeStyleClass("co.label.dj_material.fabricante_numero_documento", "labelClass");
	    	}
	    }
    }

    if ( document.getElementById("DJ_MATERIAL.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida.";
        changeStyleClass("co.label.dj_material.um_fisica_id", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.um_fisica_id", "labelClass");
    }

    if ( document.getElementById("DJ_MATERIAL.cantidad").value == "" ) {
        mensaje +="\n -La cantidad.";
        changeStyleClass("co.label.dj_material.cantidad", "errorValueClass");
    } else {
    	changeStyleClass("co.label.dj_material.cantidad", "labelClass");
    }

    // El valor US es obligatorio si NO est� marcado el check de clasificaci�n arancelaria
	if ($("#criterioArancelario").val() != '2') {


	    if ( document.getElementById("DJ_MATERIAL.valorUs").value == "" ) {
	        mensaje +="\n -El Costo Unitario de la Mercanc�a en US$.";
	        changeStyleClass("co.label.dj_material.valor_en_us_II", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.dj_material.valor_en_us_II", "labelClass");
	    }

	}


    if (mensaje != "Debe ingresar o seleccionar : ") {
        ok = false;
        alert(mensaje);
    }

    return ok;
}

function habilitarXAcuerdo(){
	var tipoPais = document.formulario.tipoPais.value;

	if (tipoPais == 'PNM' ){
		$("#trOrigen").attr("style", "display:");
	}
}

/************* DJ PRODUCTOR ************************/
function inicializacionDeclaracionJuradaProductor(){
}

function verificarEsValidador(){
	return false;
}

function validaProdXAcuerdo(mensaje) {
    return mensaje;
}