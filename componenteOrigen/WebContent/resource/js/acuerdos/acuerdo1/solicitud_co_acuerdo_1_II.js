/********* SOLICITUD ***************/

var PAIS_PERU = '168';
var NUM_DIGITOS_PARTIDA = 8;

// Inicializa los controles para el formato Per� - SGPC
function inicializacionSolicitudSegunAcuerdo() {
	$(".trDatosImportador").attr("style", "display:");
	$(".trMediosTransporteRuta").attr("style", "display:");
	$("#trMediosTransporteRutaModoTransCargaDescarga").attr("style", "display:none"); // JMC_20140522_PQT-07
	$("#trAlMenosUnDatoMediosTransporte").attr("style", "display:"); // JMC_20140522_PQT-07
	$("#trLinkAyudaPuertoDescarga").attr("style", "display:"); // JMC_20140522_PQT-07
	
	$("#spanFechaPartidaTransporte_2").show(); // JMC_20140522_PQT-07
	$("#spanNumeroTransporte_2").show(); // JMC_20140522_PQT-07
	$("#spanTipoPuertoCarga_2").show(); // JMC_20140522_PQT-07

	
	cargarPaisesImportador();
	
	// JMC_20140522_PQT-07  Carga la data de los puertos de carga y descarga
	var modoTransCargaId = $("#CERTIFICADO_ORIGEN\\.modoTransCargaId").val();
	if ( modoTransCargaId != '') {
		document.getElementById("modoTransCarga").value = modoTransCargaId;
		cargarPuertosCarga();
	}
	var modoTransDescargaId = $("#CERTIFICADO_ORIGEN\\.modoTransDescargaId").val();
	if ( modoTransDescargaId != '') {
		document.getElementById("modoTransDescarga").value = modoTransDescargaId;
		cargarPuertosDescarga();
	}
}

// Cargar pa�ses del importador
function cargarPaisesImportador() {
	var idAcuerdo = document.getElementById("idAcuerdo").value;
	var filter = "idAcuerdo=" + idAcuerdo;
	loadSelectAJX("ajax.selectLoader", "loadList", "selectPaisImportador", "comun.pais_iso_x_acuerdo.select", filter, null, afterCargarPaisesImportador, null);
}

// Despues de cargar pa�ses del importador
function afterCargarPaisesImportador() {
	var idPais = $("#idPais").val();
	document.getElementById("CERTIFICADO_ORIGEN.importadorPais").value = idPais;
	document.getElementById("selectPaisImportador").value = idPais;
	$("#selectPaisImportador").removeClass("inputTextClass").addClass("readonlyInputTextClass");
	$("#selectPaisImportador").attr("disabled", true);
}

// Funci�n ajax que puebla el combo de puertos de carga
function cargarPuertosCarga() {

	if ($("#modoTransCarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "select.empty", null, null, null, null);
		
	} else {		
		var filter = "paisIsoId=" + PAIS_PERU + "|modoTransporte=" + $("#modoTransCarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoCarga", "comun.puerto.select", filter, null, afterCargarPuertosCarga, null);
	}
}

// Despues de cargar puertos de carga
function afterCargarPuertosCarga() {
    var puertoCargaId = $("#CERTIFICADO_ORIGEN\\.puertoCargaId").val();
	if ( puertoCargaId != '') {
		document.getElementById("puertoCarga").value = puertoCargaId;
	}
}

//Funci�n ajax que puebla el combo de puertos de descarga
function cargarPuertosDescarga() {

	if ($("#modoTransDescarga").val() == '') {
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "select.empty", null, null, null, null);
	} else {
		var f = document.formulario;
		var filter = "paisIsoId=" + f.idPais.value + "|modoTransporte=" + $("#modoTransDescarga").val();
		loadSelectAJX("ajax.selectLoader", "loadList", "puertoDescarga", "comun.puerto.select", filter, null, afterCargarPuertosDescarga, null);
	}

}

//Despues de cargar puertos de descarga
function afterCargarPuertosDescarga() {
    var puertoDescargaId = $("#CERTIFICADO_ORIGEN\\.puertoDescargaId").val();
	if ( puertoDescargaId != '') {
		document.getElementById("puertoDescarga").value = puertoDescargaId;
	}
}

// Completa la informaci�n antes de enviar al servidor, de ser necesario
function completarInformacionSubmit() {
	document.getElementById("CERTIFICADO_ORIGEN.modoTransCargaId").value = document.getElementById("modoTransCarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoCargaId").value = document.getElementById("puertoCarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.modoTransDescargaId").value = document.getElementById("modoTransDescarga").value;
	document.getElementById("CERTIFICADO_ORIGEN.puertoDescargaId").value = document.getElementById("puertoDescarga").value;
}

// Valida la informaci�n general de la solicitud.
function validarCamposSolicitudCertificado(mensajeAdicional) {

	var mensaje="Debe ingresar o seleccionar : ";
    var ok = true;

    mensaje += mensajeAdicional;

    if (document.getElementById("CERTIFICADO_ORIGEN.importadorNombre").value == "") {
        mensaje +="\n -El nombre del importador";
        changeStyleClass("co.label.certificado.importador_nombre", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_nombre", "labelClass");
    }

    if (document.getElementById("CERTIFICADO_ORIGEN.importadorDireccion").value == "") {
        mensaje +="\n -La direcci�n del importador";
        changeStyleClass("co.label.certificado.importador_direccion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.importador_direccion", "labelClass");
    }

    // Al menos uno de los datos de transporte debe ser ingresado
	var fechaTransporte = document.getElementById("CERTIFICADO_ORIGEN.fechaPartidaTransporte").value;
	var numeroTransporte = document.getElementById("CERTIFICADO_ORIGEN.numeroTransporte").value;
	var puertoCarga = document.getElementById("puertoCarga").value;
	var puertoDescarga = document.getElementById("puertoDescarga").value;

	if (fechaTransporte == '' && numeroTransporte == '' && puertoCarga == '' && puertoDescarga == '') {
		mensaje +="\n -Alguno de los datos de transporte (si selecciona el tipo de puerto tiene tambien deber� seleccionar el puerto).";
	}

    if ( mensaje != "Debe ingresar o seleccionar : " ) {
        ok = false;
        alert (mensaje);
    }
    return ok;
}

/********* FACTURA ***************/

//Inicializa los controles para la factura del acuerdo Per� - SGPC
function inicializacionFacturaSolicitudSegunAcuerdo() {
}

//Validaci�n de obligatoriedad de campos de la Factura
/*function validarCamposFacturaSolicitudCertificado(){

  var ok = true;
  var mensaje="Debe ingresar o seleccionar : ";

  if (document.getElementById("CO_FACTURA.numero").value == "") {
      mensaje +="\n -El n�mero";
      changeStyleClass("co.label.factura.numero", "errorValueClass");
  } else {
  	changeStyleClass("co.label.factura.numero", "labelClass");
  }

  if (document.getElementById("CO_FACTURA.fecha").value == ""){
      mensaje +="\n -La fecha";
      changeStyleClass("co.label.factura.fecha", "errorValueClass");
  } else {
  	changeStyleClass("co.label.factura.fecha", "labelClass");
  }

  if (mensaje!="Debe ingresar o seleccionar : ") {
      ok= false;
      alert (mensaje);
  }
  return ok;
}*/

// Validaci�n de obligatoriedad de campos de la Factura
function validarCamposFacturaSolicitudCertificado(){

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if (document.getElementById("CO_FACTURA.indicadorTercerPais").value == 'S') {
        if (document.getElementById("CO_FACTURA.nombreOperadorTercerPais").value == "") {
            mensaje +="\n -El nombre del operador del tercer pa�s";
            changeStyleClass("co.label.factura.nombre_operador", "errorValueClass");
        } else {
        	changeStyleClass("co.label.factura.nombre_operador", "labelClass");
        }
    }

    if (document.getElementById("CO_FACTURA.numero").value == "") {
        mensaje +="\n -El n�mero";
        changeStyleClass("co.label.factura.numero", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.numero", "labelClass");
    }

    if (document.getElementById("CO_FACTURA.fecha").value == ""){
        mensaje +="\n -La fecha";
        changeStyleClass("co.label.factura.fecha", "errorValueClass");
    } else {
    	changeStyleClass("co.label.factura.fecha", "labelClass");
    }

    if (document.getElementById('faltaAdjunto').value == 'S') {
	    if(document.formulario.archivo.value ==""){
	    	mensaje +="\n -El archivo de la factura";
	    }
    }

    if (mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* MERCANCIA ***************/

//Inicializa los controles para la mercanc�a del acuerdo Per� - SGPC
function inicializacionMercanciaSolicitudSegunAcuerdo() {
	$("#trEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaMarca").attr("style", "display:");
	$("#trMercanciaTipoBulto").attr("style", "display:");
	$("#trMercanciaCantidadBulto").attr("style", "display:");
	$("#trMercanciaCantidad").attr("style", "display:");
	$("#trMercanciaUm").attr("style", "display:");
	$("#trMercanciaFactura").attr("style", "display:");
	$("#trMercanciaPartidaAcuerdo").attr("style", "display:");
    
	$("#CO_MERCANCIA\\.partidaSegunAcuerdo").attr("maxlength",NUM_DIGITOS_PARTIDA);
	
    // Estas inicializaciones son necesarias pero para todos los acuerdos
    if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == '' ) {
    	document.getElementById("CO_MERCANCIA.esMercanciaGranel").value = 'N';
    }
    
    // Manejamos la l�gica asociada
    logicaMercanciaGranel(document.getElementById("CO_MERCANCIA.esMercanciaGranel").value);
}

function valida_longitud_naladisa(obj,validarLongitud) {
	valida_longitud_naladisa_digitos(obj,validarLongitud,NUM_DIGITOS_PARTIDA);
}

//Maneja la l�gica esperada para el caso de mercancias a granel
function logicaMercanciaGranel(esMercanciaGranel) {
	if ( esMercanciaGranel == 'S' ) {
		document.getElementById("CO_MERCANCIA.cantidadBulto").value = '';
		$("#trMercanciaCantidadBulto").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.marcasNumeros").value = '';
		$("#trMercanciaMarca").attr("style", "display:none");
		document.getElementById("CO_MERCANCIA.tipoBulto").value = 'IN BULK';
		// hacer readonly la clase de paquete
        $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("inputTextClass").addClass("readonlyInputTextClass");
        $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", true);
	} else {
		$("#trMercanciaCantidadBulto").attr("style", "display:");
		$("#trMercanciaMarca").attr("style", "display:");
		// quitamos el readonly de la clase de paquete
        if ( document.getElementById("CO_MERCANCIA.tipoBulto").value == 'IN BULK' ) { // S�lo para este caso limpiamos la informaci�n
        	document.getElementById("CO_MERCANCIA.tipoBulto").value = '';
        }
        $("#CO_MERCANCIA\\.tipoBulto" ).removeClass("readonlyInputTextClass").addClass("inputTextClass");
        $("#CO_MERCANCIA\\.tipoBulto" ).attr("readonly", false);
	}
}

//Completa datos del certificado en la Mercanc�a, de ser necesario, seg�n acuerdo
function completarDatosCertificadoMercancia() {
}

//Validaci�n de obligatoriedad de campos de la mercanc�a
function validarCamposMercanciaSolicitudCertificado() {

    var ok = true;
    var mensaje="Debe ingresar o seleccionar : ";

    if ( $.trim(document.getElementById("CO_MERCANCIA.descripcion").value) == "" ) {
        mensaje +="\n -La descripci�n";
        changeStyleClass("co.label.co_mercancia.descripcion", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.descripcion", "labelClass");
    }
    
    if ( $.trim(document.getElementById("CO_MERCANCIA.partidaSegunAcuerdo").value) == "" ) {
        mensaje +="\n -"+document.getElementById("co.label.co_mercancia.1.naladisa").innerHTML;
        changeStyleClass("co.label.co_mercancia.1.naladisa", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.1.naladisa", "labelClass");
    }
    
    if ( $.trim(document.getElementById("CO_MERCANCIA.esMercanciaGranel").value) == "" ) {
        mensaje +="\n -Si la mercanc�a declarada es a granel o no";
        changeStyleClass("co.label.certificado.es_mercancia_granel", "errorValueClass");
    } else {
    	changeStyleClass("co.label.certificado.es_mercancia_granel", "labelClass");
    }

	if ( document.getElementById("CO_MERCANCIA.esMercanciaGranel").value == 'N' ) { // S�lo cuando no es a granel este campo es visible y obligatorio
		if ( document.getElementById("CO_MERCANCIA.cantidadBulto").value == '' ) {
		    mensaje +="\n -La cantidad de paquetes.";
		    changeStyleClass("co.label.co_mercancia.cantidad_bulto.1", "errorValueClass");
		} else {
			changeStyleClass("co.label.co_mercancia.cantidad_bulto.1", "labelClass");
		}

	    if (document.getElementById("CO_MERCANCIA.marcasNumeros").value=="") {
	        mensaje +="\n -Marcas y n�mero de paquete";
	        changeStyleClass("co.label.co_mercancia.marca.1", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.co_mercancia.marca.1", "labelClass");
	    }

	    if ( $.trim(document.getElementById("CO_MERCANCIA.tipoBulto").value) == "" ) {

	        mensaje +="\n -El tipo de bulto";
	        changeStyleClass("co.label.co_mercancia.tipo_bulto.1", "errorValueClass");

	    } else {

			var contenidoTipoBulto = document.getElementById("CO_MERCANCIA.tipoBulto").value.toUpperCase();
			var inBulkIngles = 'IN BULK';
			var inBulkEspanol = 'A GRANEL';
			if ( contenidoTipoBulto.indexOf(inBulkIngles) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'IN BULK' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto.1", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto.1", "labelClass");
			}
			if ( contenidoTipoBulto.indexOf(inBulkEspanol) != -1 ) {
				mensaje +="\n -La clase de paquetes no puede contener el texto 'A GRANEL' ";
				changeStyleClass("co.label.co_mercancia.tipo_bulto.1", "errorValueClass");
			} else {
				changeStyleClass("co.label.co_mercancia.tipo_bulto.1", "labelClass");
			}
	    }

	    if ( $.trim(document.getElementById("CO_MERCANCIA.cantidadBulto").value) == "" ) {
	        mensaje +="\n -La cantidad de bultos";
	        changeStyleClass("co.label.co_mercancia.cantidad_bulto.1", "errorValueClass");
	    } else {
	    	changeStyleClass("co.label.co_mercancia.cantidad_bulto.1", "labelClass");
	    }

	}

    if ( document.getElementById("CO_MERCANCIA.cantidad").value == "" ) {
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad.1", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.cantidad.1", "labelClass");
    }

    if ( document.getElementById("CO_MERCANCIA.umFisicaId").value == "" ) {
        mensaje +="\n -La unidad de medida";
        changeStyleClass("co.label.co_mercancia.um_fisica_id.1", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.um_fisica_id.1", "labelClass");
    }

    if ( document.getElementById("selectFacturas").value == "" ) {
        mensaje +="\n -La factura";
        changeStyleClass("co.label.co_mercancia.secuencia_factura", "errorValueClass");
    } else {
    	changeStyleClass("co.label.co_mercancia.secuencia_factura", "labelClass");
    }


    /*if (document.getElementById("CO_MERCANCIA.naladisa").value==""){
        mensaje +="\n -Naladisa";
        changeStyleClass("co.label.co_mercancia.naladisa", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.naladisa", "labelClass");

    if (document.getElementById("CO_MERCANCIA.umFisicaId").value==""){
        mensaje +="\n -La unidad f�sica";
        changeStyleClass("co.label.co_mercancia.um_fisica_id", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.um_fisica_id", "labelClass");

    if (document.getElementById("CO_MERCANCIA.peso").value==""){
        mensaje +="\n -El peso";
        changeStyleClass("co.label.co_mercancia.peso", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.peso", "labelClass");

    if (document.getElementById("CO_MERCANCIA.cantidad").value==""){
        mensaje +="\n -La cantidad";
        changeStyleClass("co.label.co_mercancia.cantidad", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.cantidad", "labelClass");

    if (document.getElementById("CO_MERCANCIA.tipoEnvase").value==""){
        mensaje +="\n -El tipo de envase";
        changeStyleClass("co.label.co_mercancia.tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.tipo_envase", "labelClass");

    if (document.getElementById("CO_MERCANCIA.descripcionTipoEnvase").value==""){
        mensaje +="\n -La descripci�n del tipo de envase";
        changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "errorValueClass");
    }else changeStyleClass("co.label.co_mercancia.descripcion_tipo_envase", "labelClass");
*/
    if(mensaje!="Debe ingresar o seleccionar : ") {
        ok= false;
        alert (mensaje);
    }
    return ok;
}

/********* DOCUMENTO RESOLUTIVO ***************/

//Inicializa los controles para el documento resolutivo del acuerdo Per� - SGCP
function inicializacionDocumentoResolutivoSolicitudSegunAcuerdo() {
	$("#trNombreImportador").attr("style", "display:");
	$("#trDireccionImportador").attr("style", "display:");
	$("#trPaisImportador").attr("style", "display:");
	$("#trFechaPartida").attr("style", "display:");
	$("#trNumeroTransporte").attr("style", "display:");
	$("#trPuertoCarga").attr("style", "display:");
	$("#trPuertoDescarga").attr("style", "display:");
	
	$("#trModoTransporteCarga").attr("style", "display:"); // JMC_20140528_PQT-07 
	$("#trModoTransporteDescarga").attr("style", "display:"); // JMC_20140528_PQT-07 


	$("#trNroCertifEntidad").attr("style", "display:none");
	$("#trDatosImportacion").attr("style", "display:none");
	$("#trDatosIniciales").attr("style", "display:");
	$(".trDatosImportacion2").attr("style", "display:");
	$(".trMediosTransporte").attr("style", "display:");
	$(".trObservaciones").attr("style", "display:");
}

/********* DOCUMENTO RESOLUTIVO - MERCANC�A ***************/
//Inicializa los controles para el documento resolutivo del acuerdo Per� - SGCP
function inicializacionDocumentoResolutivoSolicitudMercanciaSegunAcuerdo() {
	$("#trMercanciaDrSubpartidaArancelaria").attr("style", "display:");
	$("#trMercanciaDrDenominacion").attr("style", "display:");
	$("#trMercanciaDrEsMercanciaGranel").attr("style", "display:");
	$("#trMercanciaDrMarca").attr("style", "display:");
	$("#trMercanciaDrTipoBulto").attr("style", "display:");
	$("#trMercanciaDrCantidadBulto").attr("style", "display:");
	$("#trMercanciaDrCantidad").attr("style", "display:");
	$("#trMercanciaDrUnidadMedida").attr("style", "display:");
	$("#trMercanciaDrNumeroFactura").attr("style", "display:");
	$("#trMercanciaDrPorcentajeCriterio").attr("style", "display:");
	$(".lblPesoBruto").attr("style", "display:");
	if ( document.getElementById("MERCANCIA.ES_MERCANCIA_GRANEL").value == 'SI' ) { // Si el indicador de mercanc�a a granel es si, ocultamos la cantidad y la marca
		$("#trMercanciaDrMarca").attr("style", "display:none");
		$("#trMercanciaDrCantidadBulto").attr("style", "display:none");
	}
}
