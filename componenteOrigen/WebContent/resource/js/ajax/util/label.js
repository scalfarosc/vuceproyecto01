function fillLabels (status, statusText, responseText, responseXML) {
    var xml = responseXML;
    var ELEMENT_NODE = 1;
    var root = xml.getElementsByTagName('DATA_ELEMENT').item(0);
    for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
       var node = root.childNodes.item(iNode);
       if (node.nodeType == ELEMENT_NODE) {
           var name = node.getAttribute('name').toString();
           var value = node.getAttribute('value').toString();
           changeLabelText(name, value);
        }
    }
    try {
        this._func();
    } catch(e) {
    }
}

function loadElementAJX(service, serviceMethod, key, filter, module, func) {
    var asynchronousElement = new Asynchronous("", func);
    asynchronousElement.complete = fillLabels;
    asynchronousElement.call("ajx.ajx?method=elementLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
}

function loadElementSyncAJX(service, serviceMethod, key, filter, module) {
    var synchronousElement = new Synchronous("");
    synchronousElement.call("ajx.ajx?method=elementLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
    
    var xml = synchronousElement.getResponseXML();
    var ELEMENT_NODE = 1;
    var root = xml.getElementsByTagName('DATA_ELEMENT').item(0);
    for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
       var node = root.childNodes.item(iNode);
       if (node.nodeType == ELEMENT_NODE) {
           var name = node.getAttribute('name').toString();
           var value = node.getAttribute('value').toString();
           changeLabelText(name, value);
        }
    }
}
