function fillSelect (status, statusText, responseText, responseXML) {
    var xml = responseXML;
    var ELEMENT_NODE = 1;
    var dataList = xml.getElementsByTagName('DATA_LIST');
    var root = dataList.item(0);
    clearSelect(this._div, this._defaultElement);
    if (root!=null) {
        for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
            var node = root.childNodes.item(iNode);
            if (node.nodeType == ELEMENT_NODE) {
                var name = node.getAttribute('name').toString();
                var value = node.getAttribute('value').toString();
                addSelectOption(this._div, name, value);
            }
        }
    }
    try {
        this._func();
    } catch(e) {
    }
}

function loadSelectAJX(service, serviceMethod, id, key, filter, module, func, defaultElement) {
    var asynchronousSelect = new Asynchronous(id, func, defaultElement);
    asynchronousSelect.complete = fillSelect;
    asynchronousSelect.call("ajx.ajx?method=selectLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
}

function loadSelectSyncAJX(service, serviceMethod, id, key, filter, module, defaultElement) {
    var synchronousSelect = new Synchronous(id, null, defaultElement);
    synchronousSelect.call("ajx.ajx?method=selectLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
    
    var xml = synchronousSelect.getResponseXML();
    var ELEMENT_NODE = 1;
    var dataList = xml.getElementsByTagName('DATA_LIST');
    var root = dataList.item(0);
    clearSelect(synchronousSelect.getDiv(), synchronousSelect.getDefaultElement());
    if (root!=null) {
        for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
            var node = root.childNodes.item(iNode);
            if (node.nodeType == ELEMENT_NODE) {
                var name = node.getAttribute('name').toString();
                var value = node.getAttribute('value').toString();
                addSelectOption(synchronousSelect.getDiv(), name, value);
            }
        }
    }
    return synchronousElement;
}
