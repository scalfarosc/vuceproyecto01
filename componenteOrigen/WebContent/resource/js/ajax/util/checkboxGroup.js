function fillCheckboxGroup (status, statusText, responseText, responseXML) {
    var xml = responseXML;
    var ELEMENT_NODE = 1;
    var root = xml.getElementsByTagName('DATA_LIST').item(0);
    var onChange = document.getElementById(this._div).onChange;
    var style = document.getElementById(this._div).style;
    clearCheckboxGroup(this._div);
    var index = 0;
    if (root!=null) {
        for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
            var node = root.childNodes.item(iNode);
            if (node.nodeType == ELEMENT_NODE) {
                var name = node.getAttribute('name').toString();
                var value = node.getAttribute('value').toString();
                addCheckbox(this._div, name, value, index, onChange, style);
                index++;
            }
        }
    }
    try {
        this._func();
    } catch(e) {
    }
}

function loadCheckboxGroupAJX(service, serviceMethod, idGroup, id, key, keyEmpty, filter, module, func) {
    var asynchronousSelect = new Asynchronous(id, func);
    asynchronousSelect.complete = fillCheckboxGroup;
    if (filter==null) filter = "";
    var auxFilter = "";
    var ok = true;
    var index = 0;
    while (ok) {
        try {
            if (eval(document.getElementById(idGroup+"_"+index))) {
                if (document.getElementById(idGroup+"_"+index).checked) {
                    if (auxFilter!="") auxFilter = auxFilter + ",";
                    auxFilter = auxFilter + document.getElementById(idGroup+"_"+index).value;
                }
            } else {
                ok = false;
            }
            index++;
        } catch(e) {
            ok = false;
        }
    }
    if (filter!="" && auxFilter!="") filter = filter + "|";
    if (auxFilter!="") {
        if (filter!="") {
            auxFilter = "["+(filter.split("|").length)+"]=#"+auxFilter;
        } else {
            auxFilter = "[1]=#"+auxFilter;
        }
    }
    filter = filter + auxFilter;
    if (auxFilter!="") {
        asynchronousSelect.call("ajx.ajx?method=selectLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
    } else {
        asynchronousSelect.call("ajx.ajx?method=selectLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+keyEmpty+"&filter="+filter);
    }
}
