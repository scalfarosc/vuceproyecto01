function fillValuesList (status, statusText, responseText, responseXML) {
    var xml = responseXML;
    var ELEMENT_NODE = 1;
    var root = xml.getElementsByTagName('DATA_LIST').item(0);
    var onClick = "";
    clearValuesList(this._div);
    var index = 0;
    if (root!=null) {
        for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
            var node = root.childNodes.item(iNode);
            if (node.nodeType == ELEMENT_NODE) {
                var value = node.getAttribute("value").toString();
                onClick = "selectElementValuesList(\""+this._div+"_"+index+"\", \""+this._div+"\")";
                addValueOfList(this._div, value, index, onClick);
                index++;
            }
        }
    }
    if (index==0) {
        document.getElementById("valuesList_"+this._div).style.display="none";
    } else {
        document.getElementById("valuesList_"+this._div).style.display="block";
    }
    try {
        this._func();
    } catch(e) {
    }
}

function loadValuesListAJX(service, serviceMethod, obj, minSize, id, key, filter, module, func) {
    var auxFilter = obj.value;
    if (auxFilter.length < minSize) {
        document.getElementById("valuesList_"+id).style.display="none";
        clearValuesList(id);
        return;
    }
    var asynchronousSelect = new Asynchronous(id, func);
    asynchronousSelect.complete = fillValuesList;
    if (filter==null) filter = "";
    var ok = true;
    var index = 0;
    if (filter!="" && auxFilter!="") filter = filter + "|";
    if (auxFilter!="") {
        if (filter!="") {
            auxFilter = "["+(filter.split("|").length)+"]="+auxFilter;
        } else {
            auxFilter = "[1]="+auxFilter;
        }
    }
    filter = filter + auxFilter;
    asynchronousSelect.call("ajx.ajx?method=valuesListLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
}
