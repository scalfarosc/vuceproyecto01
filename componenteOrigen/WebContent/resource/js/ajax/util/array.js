function fillArray (status, statusText, responseText, responseXML) {
	var xml = responseXML;
    var ELEMENT_NODE = 1;
    var root = xml.getElementsByTagName('DATA_ELEMENT').item(0);
    this._array = new Array();
    //var index = 0;
    for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
       var node = root.childNodes.item(iNode);
       if (node.nodeType == ELEMENT_NODE) {
           var name = node.getAttribute('name').toString();
           var value = node.getAttribute('value').toString();
           this._array[name] = value;
        }
    }
    try {
        this._func();
    } catch(e) {
    }
}

function loadArrayAJX(service, serviceMethod, key, filter, module, func) {
    var asynchronousElement = new Asynchronous("", func);
    asynchronousElement.complete = fillArray;
    asynchronousElement.call("ajx.ajx?method=elementLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
    return asynchronousElement;
}

function loadArraySyncAJX(service, serviceMethod, key, filter, module) {
    var synchronousElement = new Synchronous("");
    synchronousElement.call("ajx.ajx?method=elementLoader&service="+service+"&serviceMethod="+serviceMethod+"&module="+module+"&key="+key+"&filter="+filter);
    
    var xml = synchronousElement.getResponseXML();
	var ELEMENT_NODE = 1;
    var root = xml.getElementsByTagName('DATA_ELEMENT').item(0);
    var array = new Array();
	for (var iNode = 0; iNode < root.childNodes.length; iNode++) {
       var node = root.childNodes.item(iNode);
       if (node.nodeType == ELEMENT_NODE) {
           var name = node.getAttribute('name').toString();
           var value = node.getAttribute('value').toString();
           array[name] = value;
        }
    }
    synchronousElement.setArray(array);
    return synchronousElement;
}
