function fillPage (status, statusText, responseText, responseXML) {
	changeLabelText(this._div, responseText);
    try {
        this._func();
    } catch(e) {
    }
    try {
        eval(responseText);
    } catch(e) {}
}

function loadPageAJX(page, div, filter, func) {
    var asynchronousPage = new Asynchronous(div, func);
    asynchronousPage.complete = fillPage;
    asynchronousPage.call(page+"&filter="+filter);
}
