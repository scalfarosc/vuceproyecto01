function Synchronous(div, defaultElement) {
    this._xmlhttp = new FactoryXMLHttpRequest();
    this._div = div;
    this._array = null;
    this._defaultElement = defaultElement;
}

function Synchronous_call(url) {
    var instance = this;
    this._xmlhttp.open('GET', url+'&ms='+(new Date().getTime()), false);
    this._xmlhttp.send(null);
    this._xmlhttp.responseXML;
}

function Synchronous_getArray() {
    return this._array;
}

function Synchronous_setArray(array) {
    this._array = array;
}

function Synchronous_getDiv() {
    return this._div;
}

function Synchronous_setDiv(div) {
    this._div = div;
}

function Synchronous_getDefaultElement() {
    return this._defaultElement;
}

function Synchronous_setDefaultElement(defaultElement) {
    this._defaultElement = defaultElement;
}

function Synchronous_getResponseText() {
    return this._xmlhttp.responseText;
}

function Synchronous_getResponseXML() {
    return this._xmlhttp.responseXML;
}

Synchronous.prototype.call = Synchronous_call;

Synchronous.prototype.getArray = Synchronous_getArray;
Synchronous.prototype.setArray = Synchronous_setArray;
Synchronous.prototype.getResponseText = Synchronous_getResponseText;
Synchronous.prototype.getResponseXML = Synchronous_getResponseXML;
Synchronous.prototype.getDiv = Synchronous_getDiv;
Synchronous.prototype.setDiv = Synchronous_setDiv;
Synchronous.prototype.getDefaultElement = Synchronous_getDefaultElement;
Synchronous.prototype.setDefaultElement = Synchronous_setDefaultElement;
