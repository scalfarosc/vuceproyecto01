// Autor: Javier Florindez Reategui

netscape="";
ver = navigator.appVersion; len = ver.length;
for (iln = 0; iln < len; iln++ ) if (ver.charAt(iln) =="(") break;
netscape = (ver.charAt(iln+1).toUpperCase() != "C");

var contador = 0;


function enableDisableValue(id, action, editableClass, readonlyClass) {
    try {
        //if (eval(document.getElementById(id).readOnly)) {
            document.getElementById(id).readOnly = !action;
        //} else {
        //    document.getElementById(id).disabled = !action;
        //}
        changeStyleClass(id, (action ? editableClass : readonlyClass));
    } catch(e) {
    }
}

function enableDisable(id, action) {
    try {
        document.getElementById(id).disabled = !action;
    } catch(e) {
    }
}

function disableButton(id, action) {
    try {
    	if (document.getElementById(id)) {
            document.getElementById(id).disabled = action;
            document.getElementById(id).className = (action ? 'buttonSubmitDisabledClass' : 'buttonSubmitClass');
        }
    } catch(e) {
    }
}

function changeLabelText(id, text) {
    var str = new String(id);
    var obj = null;
    if (document.getElementById(str)) {
        obj = document.getElementById(str);
    } else
    if (document.getElementById(str.toUpperCase())) {
        obj = document.getElementById(str.toUpperCase());
    } else
    if (document.getElementById(str.toLowerCase())) {
        obj = document.getElementById(str.toLowerCase());
    } else
    if (window.parent && window.parent.document.getElementById(str)) {
        obj = window.parent.document.getElementById(str);
    } else
    if (window.parent && window.parent.document.getElementById(str.toUpperCase())) {
        obj = window.parent.document.getElementById(str.toUpperCase());
    } else
    if (window.parent && window.parent.document.getElementById(str.toLowerCase())) {
        obj = window.parent.document.getElementById(str.toLowerCase());
    }
    if (obj!=null) {
        if (obj.type && obj.type.indexOf("select")!=-1) { // It's a select
            selectSelectedOption(obj, text);
        } else {
            var ok = true;
            try {obj.innerHTML = text;}catch(e){ok = false;}
            try {obj.value = text;}catch(e){ok = false;}
            if (!ok && id.indexOf("select_")==-1) {
                changeLabelText("select_"+id, text);
            }
        }
    }
}

function clearSelect(id, defaultElement) {
    if (defaultElement=="" || defaultElement==null) defaultElement = "--Seleccione--";
    var select = (typeof(id) == "object") ? id : document.getElementById(id);
    try {
        if (document.getElementById(select.id+"Temp")) document.getElementById(select.id+"Temp").value="";
    } catch(e) {
    }
    while(select.length > 0) {
        select.remove(0);
    }
    addSelectOption(id, "", defaultElement);
}

function addSelectOption(id, code, description) {
    var select = (typeof(id) == "object") ? id : document.getElementById(id);
    var option = document.createElement('option');
    option.text = description;
    option.value = code;
    try {
        select.add(option, null); // standards compliant; doesn't work in IE
    } catch(ex) {
        select.add(option); // IE only
    }
}

function clearCheckboxGroup(id) {
    document.getElementById("checkboxGroup_"+id).innerHTML = "";
}

function addCheckbox(id, code, description, index, onChange, style) {
    var select = document.getElementById(id);
    try {
        document.getElementById('checkboxGroup_'+id).innerHTML += "<input type='checkbox' id='"+id+"_"+index+"' name='"+id+"' "+onChange+style+" value='"+code+"' >"+
                                "<label for='"+id+"_"+index+"' >"+description+"</label><br>";
    } catch(ex) {
        alert(ex);
    }
}

function clearValuesList(id) {
    document.getElementById("valuesList_"+id).innerHTML = "";
}

function addValueOfList(id, description, index, onClick) {
    try {
        var width = document.getElementById(id).offsetWidth;
        var label = "<div style='cursor:pointer;cursor:hand;padding:3px;width:"+width+"px;'"+
                    "onmouseover='this.style.background=\"#73AAE1\"'"+
                    "onmouseout='this.style.background=\"#FFFFFF\"' onClick='"+onClick+";' >"+
                    "<label id='"+id+"_"+index+"' style='cursor:pointer;cursor:hand;' >"+description+"</label></div>"+
                    "<br style='clear:both;line-height:0;display:block;'>";
        document.getElementById("valuesList_"+id).innerHTML += label;
    } catch(ex) {
        alert(ex);
    }
}

function hideValuesList(id) {
    setTimeout('document.getElementById("valuesList_'+id+'").style.display="none";', 200);
}

function selectElementValuesList(labelId, inputId) {
    document.getElementById(inputId).value = document.getElementById(labelId).innerHTML;
    document.getElementById("valuesList_"+inputId).style.display = "none";
    clearValuesList(inputId);
}

function changeStyleClass(id, className) {
    document.getElementById(id).className = className;
}

function getArrayValue(array, name) {
    var value = array[name];
    if (value==null || value == "undefined") value = "";
    return value;
}

function getScreenSize() {
    var screenW = 640, screenH = 480;
    if (parseInt(navigator.appVersion) > 3) {
       screenW = screen.width;
       screenH = screen.height;
    } else if (navigator.appName == "Netscape" && parseInt(navigator.appVersion)==3 && navigator.javaEnabled()) {
       var jToolkit = java.awt.Toolkit.getDefaultToolkit();
       var jScreenSize = jToolkit.getScreenSize();
       screenW = jScreenSize.width;
       screenH = jScreenSize.height;
    }
    return screenW+","+screenH;
}

/*function selectRow(obj) {
    var currentrow = obj.parentNode.parentNode;
    alert(obj.parentNode.parentNode.className);
    if(obj.checked) {
        currentrow.className += 'Selected';
    } else {
        currentrow.className = currentrow.className.replace(/Selected/g,'');
    }
}*/

/*function selectRow(fila, tableName) {
    var tableSize = parseInt(document.getElementById('tableSize_'+tableName).value);
    var tds = document.getElementById(fila+'.'+tableName).childNodes;
    for (var i=0; i < tableSize; i++) {
        try {
            if (fila == (i+1)) {
                tds = document.getElementById((i+1)+'.'+tableName).childNodes;
                for (var j=0; j < tds.length; j++) {
                    tds[j].className = 'cellClassSelected';
                }
            } else {
                tds = document.getElementById((i+1)+'.'+tableName).childNodes;
                for (var j=0; j < tds.length; j++) {
                    tds[j].className = 'cellClass';
                }
            }
        } catch(e) {}
    }
}
*/
function selectRow(fila, tableName) {
    var tdsRoot = document.getElementById(fila+"."+tableName).parentNode.childNodes;
    var pageNumber = parseInt(eval(document.getElementById('txtPage_'+tableName)) ? document.getElementById('txtPage_'+tableName).value : 1);
    var tablePageSize = parseInt(document.getElementById('tablePageSize_'+tableName).value);
    fila = fila - tablePageSize*(pageNumber-1);

    var tableSize = tdsRoot.length;
    var cont = 0;
    for (var i=0; i < tableSize; i++) {
        try {
            if (tdsRoot[i].nodeName=="TR") {
                tds = tdsRoot[i].childNodes;
                if (fila == (i+1)) {
                    for (var j=0; j < tds.length; j++) {
                        if (tds[j].nodeName=="TD") {
                            tds[j].className = "cellClassSelected";
                        }
                    }
                } else {
                    for (var j=0; j < tds.length; j++) {
                        if (tds[j].nodeName=="TD" && tds[j].className == "cellClassSelected" ) {
                            tds[j].className = "cellClass";
                        }
                    }
                }
            } else {
                fila++;
            }
        } catch(e) {}
    }
}

function putColorOnRow(fila, tableName, className) {
    var row = document.getElementById(fila+"."+tableName).childNodes;
    for (var i=0; i < row.length; i++) {
        if (row[i].nodeName=="TD") {
            row[i].className = className;
        }
    }
}

function openCalendarOnGrid(rowNumber, cellName) {
    var pattern = "%d/%m/%Y";
    var time = "24";
    return showCalendar(cellName, pattern, time, true);
}

function selectRadioButtonOnRow(fila, tableName, form, radioButtonName) {
    var tableSize = parseInt(document.getElementById('tableSize_'+tableName).value);
    //var tds = document.getElementById(fila+'.'+tableName).childNodes;
    for (var i=0; i < tableSize; i++) {
        try {
            if (fila == (i+1)) {
                if (tableSize > 1) {
                    document.forms[form].elements[radioButtonName][i].checked = true;
                } else {
                    document.getElementById(radioButtonName).checked = true;
                }
                /*tds = document.getElementById((i+1)+'.'+tableName).childNodes;
                for (var j=0; j < tds.length; j++) {
                    tds[j].className = 'cellClassSelected';
                }*/
            } else {
                if (tableSize > 1) {
                    document.forms[form].elements[radioButtonName][i].checked = false;
                } else {
                    document.getElementById(radioButtonName).checked = false;
                }
                /*tds = document.getElementById((i+1)+'.'+tableName).childNodes;
                for (var j=0; j < tds.length; j++) {
                    tds[j].className = 'cellClass';
                }*/
            }
        } catch(e) {}
    }
}

function selectAllCheckBoxes(form, checkBoxesName, checked) {
    var f = document.forms[form];
    if (eval(f.elements[checkBoxesName])) {
        if (!eval(f.elements[checkBoxesName].length)) { // Solo hay 1 checkbox
            if (f.elements[checkBoxesName].disabled) return;
            f.elements[checkBoxesName].checked = checked;
            if (eval(f.elements[checkBoxesName].onchange))
                f.elements[checkBoxesName].onchange();
            if (eval(f.elements[checkBoxesName].onclick))
                f.elements[checkBoxesName].onclick();
        } else {
            for (i=0 ; i<f.elements[checkBoxesName].length; i++) {
                if (f.elements[checkBoxesName][i].disabled) continue;
                f.elements[checkBoxesName][i].checked = checked;
                if (eval(f.elements[checkBoxesName][i].onchange))
                    f.elements[checkBoxesName][i].onchange();
                if (eval(f.elements[checkBoxesName][i].onclick))
                    f.elements[checkBoxesName][i].onclick();
            }
        }
    }
}

function invertSelectionAllCheckBoxes(form, checkBoxesName) {
    var f = document.forms[form];
    if (eval(f.elements[checkBoxesName])) {
        if (!eval(f.elements[checkBoxesName].length)) { // Solo hay 1 checkbox
            if (f.elements[checkBoxesName].disabled) return;
            if (f.elements[checkBoxesName].checked) {
                f.elements[checkBoxesName].checked = false;
            } else {
                f.elements[checkBoxesName].checked = true;
            }
            if (eval(f.elements[checkBoxesName].onchange))
                f.elements[checkBoxesName].onchange();
            if (eval(f.elements[checkBoxesName].onclick))
                f.elements[checkBoxesName].onclick();
        } else {
            for (i=0 ; i<f.elements[checkBoxesName].length; i++) {
                if (f.elements[checkBoxesName][i].disabled) continue;
                if (f.elements[checkBoxesName][i].checked) {
                    f.elements[checkBoxesName][i].checked = false;
                } else {
                    f.elements[checkBoxesName][i].checked = true;
                }
                if (eval(f.elements[checkBoxesName][i].onchange))
                    f.elements[checkBoxesName][i].onchange();
                if (eval(f.elements[checkBoxesName][i].onclick))
                    f.elements[checkBoxesName][i].onclick();
            }
        }
    }
}

function selectAllValueCheckBoxes(inicio, fin, totalFilas, column, tableName, checked, checkedValue, uncheckedValue) {
    for (i=inicio; i <= fin && i <= totalFilas; i++) {
        if (document.getElementById('tableCheckBox_'+i+','+tableName+'.'+column).disabled) continue;
        document.getElementById('tableCheckBox_'+i+','+tableName+'.'+column).checked = checked;
        if (checked && checkedValue!='') {
            document.getElementById(i+','+tableName+'.'+column).value = checkedValue;
        } else if (!checked && uncheckedValue!='') {
            document.getElementById(i+','+tableName+'.'+column).value = uncheckedValue;
        }

        if (eval(document.getElementById('tableCheckBox_'+i+','+tableName+'.'+column).onclick))
            document.getElementById('tableCheckBox_'+i+','+tableName+'.'+column).onclick();
        if (eval(document.getElementById('tableCheckBox_'+i+','+tableName+'.'+column).onchange))
            document.getElementById('tableCheckBox_'+i+','+tableName+'.'+column).onchange();
    }
}

function invertSelectionValueCheckBoxes(inicio, fin, totalFilas, column, tableName, checkedValue, uncheckedValue) {
    for (i=inicio; i <= fin && i <= totalFilas; i++) {
        if (document.getElementById('tableCheckBox_'+i+','+column+tableName).disabled) continue;
        if (document.getElementById('tableCheckBox_'+i+','+column+tableName).checked) {
            document.getElementById('tableCheckBox_'+i+','+column+tableName).checked = false;
            document.getElementById(i+','+column+tableName).value = uncheckedValue;
            if (eval(document.getElementById('tableCheckBox_'+i+','+column+tableName).onclick))
                document.getElementById('tableCheckBox_'+i+','+column+tableName).onclick();
            if (eval(document.getElementById('tableCheckBox_'+i+','+column+tableName).onchange))
                document.getElementById('tableCheckBox_'+i+','+column+tableName).onchange();
        } else {
            document.getElementById('tableCheckBox_'+i+','+column+tableName).checked = true;
            document.getElementById(i+','+column+tableName).value = checkedValue;
            if (eval(document.getElementById('tableCheckBox_'+i+','+column+tableName).onclick))
                document.getElementById('tableCheckBox_'+i+','+column+tableName).onclick();
            if (eval(document.getElementById('tableCheckBox_'+i+','+column+tableName).onchange))
                document.getElementById('tableCheckBox_'+i+','+column+tableName).onchange();
        }
    }
}

function showHideLeftPanel(obj, framesetName, columns) {
    var frameset = window.parent.document.getElementById(framesetName);
    if (frameset.cols != '0,*') {
        obj.src = '/pad/imagen/rightArrow.gif';
        obj.alt = 'Mostrar Panel Izquierdo';
        frameset.cols = '0,*';
    } else {
        obj.src = '/pad/imagen/leftArrow.gif';
        obj.alt = 'Ocultar Panel Izquierdo';
        frameset.cols = columns+',*';
    }
}

function showMessage(divId) {
    if (document.getElementById(divId).style.display!='none') {
        document.getElementById(divId).style.display='none';
    } else {
        document.getElementById(divId).style.display='block';
    }
}

function showMessageZone(divId) {
    if (document.getElementById(divId).style.display!='none') {
        document.getElementById(divId).style.display='none';
    } else {
        document.getElementById(divId).style.display='block';
    }
}

function showMessage(divId, state) {
    document.getElementById(divId).style.display = (state ? 'block' : 'none');
}

function showPopUp(divId) {
    document.getElementById(divId).style.display = 'block';
    var theBody = document.getElementsByTagName('BODY')[0];
    if (document.getElementById(divId+"mask")==null) {
        var popup = document.createElement('div');
        popup.id = divId+"mask";
        popup.className = "popupWindowMask";
        theBody.appendChild(popup);
    }
    var gPopup = document.getElementById(divId+"mask");
    var scTop = parseInt(theBody.scrollTop, 10);
    var scLeft = parseInt(theBody.scrollLeft, 10);
    gPopup.style.display = "block";
    gPopup.style.top = scTop + "px";
    gPopup.style.left = scLeft + "px";
    var fullHeight = getViewportHeight();
    var fullWidth = getViewportWidth();

    // Determine what's bigger, scrollHeight or fullHeight / width
    if (fullHeight > theBody.scrollHeight) {
        popHeight = fullHeight;
    } else {
        popHeight = theBody.scrollHeight;
    }
    if (fullWidth > theBody.scrollWidth) {
        popWidth = fullWidth;
    } else {
        popWidth = theBody.scrollWidth;
    }
    gPopup.style.height = popHeight + "px";
    gPopup.style.width = popWidth + "px";
}

function hidePopUp(divId) {
    var theBody = document.getElementsByTagName('BODY')[0];
    theBody.style.overflow = "";
    var gPopup = document.getElementById(divId+"mask");
    if (gPopup == null) {
        return;
    }
    gPopup.style.display = "none";
    document.getElementById(divId).style.display = "none";
}

function js_date(strfecha,objRegExp) {
    var objRegExp = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{1,4}$/;

    //check to see if in correct format
    if (!objRegExp.test(strfecha))
        return false; //doesn't match pattern, bad date
    else {
        var indice = strfecha.indexOf('-');
        if (indice==-1) indice = strfecha.indexOf('/');
        if (indice==-1) indice = strfecha.indexOf('.');
        var strSeparator = strfecha.substring(indice,indice+1); //find date separator

        var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
        //create a lookup for months not equal to Feb.
        var arrayLookup = { '01' : 31,'03' : 31,'04' : 30,'05' : 31,'06' : 30,'07' : 31,
                            '08' : 31,'09' : 30,'10' : 31,'11' : 30,'12' : 31,
                            '1'  : 31,'3'  : 31,'4'  : 30,'5'  : 31,'6'  : 30,'7'  : 31,
                            '8' : 31,'9' : 30};
        var intDay = parseInt(arrayDate[0],10);
        //check if month value and day value agree
        if (arrayLookup[arrayDate[1]] != null) {
            if (intDay <= arrayLookup[arrayDate[1]] && intDay != 0)
                return true; //found in lookup table, good date
        } else {
            //check for February
            var intYear = parseInt(arrayDate[2],10);
            var intMonth = parseInt(arrayDate[1],10);
            if (intMonth==2 && ((intYear % 4 == 0 && intDay <= 29) || (intYear % 4 != 0 && intDay <=28)) && intDay !=0)
            return true; //Feb. had valid number of days
        }
    }
    return false; //any other values, bad date
}

function js_hourMinSeg(strHora,objRegExp) {
    var objRegExp = /^\d{1,2}(\:)\d{1,2}\1\d{1,2}$/;
    //check to see if in correct format
    if (!objRegExp.test(strHora)) {
        return false; //doesn't match pattern, bad hour
    }
    return true;
}

function js_monthYear(strfecha,objRegExp) {
    var objRegExp = /^\d{1,2}(\-|\/|\.)\d{1,4}$/;

    //check to see if in correct format
    if (!objRegExp.test(strfecha)) {
        alert(objRegExp);
        return false; //doesn't match pattern, bad date
    } else {
        var indice = strfecha.indexOf('-');
        if (indice==-1) indice = strfecha.indexOf('/');
        if (indice==-1) indice = strfecha.indexOf('.');
        var strSeparator = strfecha.substring(indice,indice+1); //find date separator

        var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
        var intMonth = parseInt(arrayDate[0],10);
        //check if month value and day value agree
        return intMonth <= 12;
    }
    return false; //any other values, bad date
}

// Numeros con decimales, positivos
function js_dec_pos(number) {
    var objRegExp = /^[+]?([0-9]+([\.|,][0-9]+)?)$/;
    return objRegExp.test(number);
}

//Numeros con decimales, positivos, cantidad enteros, cantidad decimales
function js_dec_pos_ent_dec(number,cantEnt,cantDec) {
    var objRegExp = new RegExp('^(0|(([1-9]{1})([0-9]{0,'+(cantEnt-1)+'})))([\.][0-9]{1,'+cantDec+'})?$');
    return objRegExp.test(number);
}

//Numeros con decimales, positivos, cantidad enteros, cantidad decimales
function js_dec_neg_ent_dec(number,cantEnt,cantDec) {
    var objRegExp = new RegExp('^[\+|-]?(0|(([1-9]{1})([0-9]{0,'+(cantEnt-1)+'})))([\.][0-9]{1,'+cantDec+'})?$');
    return objRegExp.test(number);
}

// Numeros con decimales, positivos y negativos
function js_dec_posneg(number) {
    var objRegExp = /^[+-]?([0-9]+([\.|,][0-9]+)?)$/;
    return objRegExp.test(number);
}

// Numeros enteros positivos
function js_int_pos(number) {
    var objRegExp = /^[+]?([0-9]+([0-9]+)?)$/;
    return objRegExp.test(number);
}

// Numeros enteros positivos y negativos
function js_int_posneg(number) {
    var objRegExp = /^[+-]?([0-9]+([0-9]+)?)$/;
    return objRegExp.test(number);
}

// Un e-mail
function js_email(email) {
    var objRegExp = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})$/;
    return objRegExp.test(email);
}

// Lista de mails separados por comas
function js_email_list(emailList) {
    var objRegExp = /^((([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4}))+[,]?)?$/;
    return objRegExp.test(emailList);
}

function js_email2(emailStr) {
    var emailPat=/^(.+)@(.+)$/;
    var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars="\[^\\s" + specialChars + "\]";
    var quotedUser="(\"[^\"]*\")";
    var ipDomainPat=/^[(d{1,3}).(d{1,3}).(d{1,3}).(d{1,3})]$/;
    var atom=validChars + '+';
    var word="(" + atom + "|" + quotedUser + ")";
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
    var matchArray=emailStr.match(emailPat);
    if (matchArray==null) {
        alert("La direccion de correo parece ser invalida (verifique las @ y .)");
        return false;
    }
    var user=matchArray[1];
    var domain=matchArray[2];

    if (user.match(userPat)==null) {
        alert("El nombre de usuario parece ser invalido.");
        return false;
    }

    var IPArray=domain.match(ipDomainPat);
    if (IPArray!=null) {
          for (var i=1;i<=4;i++) {
            if (IPArray[i]>255) {
                alert("La direccion IP de destino no es v�lida");
                return false;
            }
        }
        return true;
    }

    var domainArray=domain.match(domainPat);
    if (domainArray==null) {
        alert("El dominio no parece ser valido.");
        return false;
    }
    var atomPat=new RegExp(atom,"g");
    var domArr=domain.match(atomPat);
    var len=domArr.length;
    /*
    if (domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>3) {
       alert("Las direcciones deben terminar con dominios de tres letras, o el codigo de pais de dos letras.");
       return false;
    }*/
    if (len<2) {
       var errStr="Dominio no v�lido";
       alert(errStr);
       return false;
    }
    return true;
}

function formatNumber(num, numDec, decSep, thousandSep) {
    if (num instanceof String) num = parseFloat(num);
    var arg;
    var Dec;
    Dec = Math.pow(10, numDec);
    if (typeof(num) == 'undefined') return;
    if (typeof(decSep) == 'undefined') decSep = '.';
    if (typeof(thousandSep) == 'undefined') thousandSep = ',';
    if (thousandSep == '.')
        arg=/./g;
    else
        if (thousandSep == ',') arg=/,/g;
    if (typeof(arg) != 'undefined') num = num.toString().replace(arg,'');
    num = num.toString().replace(/,/g, '.');
    if (isNaN(num)) num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * Dec + 0.50000000001);
    cents = num % Dec;
    num = Math.floor(num/Dec).toString();
    if (cents < (Dec / 10)) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + thousandSep + num.substring(num.length - (4 * i + 3));
    if (Dec == 1)
        return (((sign)? '': '-') + num);
    else
       return (((sign)? '': '-') + num + decSep + cents);
}

function EvaluateText(cadena, obj){
    opc = false;
    if (cadena == "%d")
    if (event.keyCode > 47 && event.keyCode < 58)
        opc = true;
    if (cadena == "%f"){
        if (event.keyCode > 47 && event.keyCode < 58)
          opc = true;
        if (obj.value.search("[.*]") == -1 && obj.value.length != 0)
            if (event.keyCode == 46)
                opc = true;
    }
    if(opc == false)
        event.returnValue = false;
}

function mOvr(src,clrOver){
  if (!src.contains(event.fromElement)) src.bgColor = clrOver;
}

function mOut(src,clrIn){
  if (!src.contains(event.toElement)) src.bgColor = clrIn;
}

function js_show_save(argTxtMsg){
   if (argTxtMsg=='') alert('Informacion ha sido grabada exitosamente.');
   else alert(argTxtMsg);
   }

function js_change_check(txtcampo) {
   var strestado="";
   strestado=txtcampo.value;
   if (strestado=="ON")
      txtcampo.value="OFF";
   else
      txtcampo.value="ON";
}

function js_format_numeric(txtcampo) {
   //Valida que solo sean numeros y punto decimal
   var valid = "0123456789"
   var ok = "yes";
   var temp;
   var campo_precio=txtcampo.value;
   for (var i=0; i<campo_precio.length; i++) {
       temp = "" + campo_precio.substring(i, i+1);
       if (valid.indexOf(temp) == "-1") ok = "no";
   }
   if (ok == "no") {
       alert("Solo ingrese al registro valores enteros.");
       txtcampo.value="0";
       txtcampo.focus();
       txtcampo.select();
   }
   else {
       var strtemporal="0000000000" + campo_precio;
       txtcampo.value=strtemporal.substring(strtemporal.length-10);
   }
}

function js_consis_hour(strtiempo) {
// Valida si el tiempo es HH24:MI:SS (tiempo militar)
   if (strtiempo == null) {
       alert("El tiempo no es un formato valido. Debe ser HH:MM:SS (tiempo militar).");
       return false;
   }
   var inthora = strtiempo.substring(0,2);
   var intminuto = strtiempo.substring(3,5);
   var intsegundo = strtiempo.substring(6,8);
   var strsepara01 = strtiempo.substring(2,3);
   var strsepara02 = strtiempo.substring(5,6);

   if (strtiempo.length<2 ||strtiempo.length>8) {
       alert("El tiempo no es un formato valido. Debe ser HH:MM:SS.");
       return false;
   }

   if ((strsepara01!="" && strsepara01!=":") || (strsepara02!="" && strsepara02!=":")) {
       alert("El tiempo no es un formato valido. Debe ser HH:MM:SS.");
       return false;
   }
   if (strsepara01==":" && intminuto=='') {
       alert("El tiempo no es un formato valido. Debe ser HH:MM:SS.");
       return false;
   }
   if (strsepara02==":" && intsegundo=='') {
       alert("El tiempo no es un formato valido. Debe ser HH:MM:SS.");
       return false;
   }

   if (inthora < 0  || inthora > 23) {
       alert("La hora deber� ser entre 1 y 23.");
       return false;
   }

   if (intminuto<0 || intminuto > 59)  {
       alert ("El minuto deber� ser entre 0 and 59.");
       return false;
   }

   if (intsegundo<0 || intsegundo > 59) {
       alert ("El segundo deber� ser entre 0 and 59.");
       return false;
   }
   return true;
}

function js_valid_date(txtcampo) {
   var strcampo=txtcampo.value;
   if (js_date(strcampo)==false) {
       alert("La fecha ingresada no tiene un formato valido. Debe ser dd/MM/yyyy");
       txtcampo.value="";
       txtcampo.focus();
   }
}

function js_valid_hour(txtcampo) {
   var strcampo=txtcampo.value;
   if (js_consis_hour(strcampo)==false) {
       txtcampo.value="";
       txtcampo.focus();
   }
}

function js_position_left(intAncho) {
   var intleft = parseInt((screen.width - intAncho)/2);
   //if ((screen.width == 800) && (screen.height == 600))
   //    intleft=100;
   //if ((screen.width == 1024) && (screen.height == 768))
   //    intleft=200;
   return intleft;
}

function js_position_top(intAlto) {
   var inttop = parseInt((screen.height - intAlto)/2);
   return inttop;
}

function js_valid_numeric(inttipo,txtcampo) {
   //Valida que solo sean numeros y punto decimal
   var strmensaje="";
   if (inttipo==1) {
       var valid = "0123456789,";
       strmensaje="Ingrese valores enteros positivos";
   }
   if (inttipo==2){
       var valid = "0123456789,-";
       strmensaje="Ingrese valores enteros";
   }
   if (inttipo==3){
       var valid = "0123456789,.";
       strmensaje="Ingrese numeros enteros o con decimales, positivos";
   }
   if (inttipo==4){
       var valid = "0123456789,-.";
       strmensaje="Ingrese numeros enteros o con decimales";
   }
   var ok = "yes";
   var temp;
   var campo_precio=txtcampo.value;
   for (var i=0; i<campo_precio.length; i++) {
       temp = "" + campo_precio.substring(i, i+1);
       if (valid.indexOf(temp) == "-1") ok = "no";
   }
   if (ok == "no") {
       alert(strmensaje);
       if (inttipo==1)
           txtcampo.value="0";
       if (inttipo==2)
           txtcampo.value="0";
       if (inttipo==3)
           txtcampo.value="0";
       txtcampo.select();
       txtcampo.focus();
   }
}

function js_validate(intTipo,obj) {
   if (intTipo==1 ||intTipo==2 || intTipo==3 || intTipo==4)
      js_valid_numeric(intTipo,obj);
   if (intTipo==5)
      js_valid_date(obj);
}

function callDetail(keyValues,keyValuesField,form,action,method,target) {
    document.forms[form].elements[keyValuesField].value = keyValues;
    document.forms[form].action=action;
    document.forms[form].elements['method'].value=method;
    document.forms[form].target=target;
    document.forms[form].submit();
}

function CallDetalle(nombreAction, cd, tg) {
    this.document.frmrejilla.camposDetalle.value = cd;
//    alert(this.document.frmrejilla.camposDetalle.value);
//    alert("/"+nombreAction);
    this.document.frmrejilla.action = "/etes/"+nombreAction;
    this.document.frmrejilla.target = tg;
    this.document.frmrejilla.submit();
//    document.location.href = "/"+nombreAction;
    //window.location.replace("/"+nombreAction);
}

function js_change_mine(intTipo, txtcampo) {
    var vMINE=txtcampo;
    var vResultado="";
    var vAuxiliar="";
    if (intTipo==1) {
    for (var i=0 ; i < vMINE.length; i++) {
        vAuxiliar=vMINE.substring(i,i+1);
        if (vAuxiliar == "&" ||
        vAuxiliar == "%" ||
        vAuxiliar == "=" ||
        vAuxiliar == "+" ||
        vAuxiliar == "-" ||
        vAuxiliar == "?" ||
        vAuxiliar == "#" ||
        vAuxiliar == "/" ) {
            if (vAuxiliar == "&" )
            vResultado=vResultado + "%26";
            if (vAuxiliar == "%" )
            vResultado=vResultado + "%25";
            if (vAuxiliar == "=" )
            vResultado=vResultado + "%3D";
            if (vAuxiliar == "+" )
            vResultado=vResultado + "%2B";
            if (vAuxiliar == "-" )
            vResultado=vResultado + "%2D";
            if (vAuxiliar == "?" )
            vResultado=vResultado + "%3F";
            if (vAuxiliar == "#" )
            vResultado=vResultado + "%23";
            if (vAuxiliar == "/" )
            vResultado=vResultado + "%2F";
        }
        else
          vResultado = vResultado + vAuxiliar;
    }
    }
    return vResultado;
}

function replaceHtmlSpecialCharacters(value) {
    if (value.indexOf("'")!=-1) value = value.replace(/'/g, "&#39;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&aacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&eacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&iacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&oacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&uacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&Aacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&Eacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&Iacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&Oacute;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&Uacute;");
    if (value.indexOf('"')!=-1) value = value.replace(/\"/g, "&quot;");
    if (value.indexOf("�")!=-1) value = value.replace(/�/g, "&euro;");
    return value;
}

function unReplaceHtmlSpecialCharacters(value) {
    if (value.indexOf("&#39;")!=-1) value = value.replace(/\b&#39;/g, "'");
    if (value.indexOf("&aacute;")!=-1) value = value.replace(/\b&aacute;/g, "�");
    if (value.indexOf("&eacute;")!=-1) value = value.replace(/\b&eacute;/g, "�");
    if (value.indexOf("&iacute;")!=-1) value = value.replace(/\b&iacute;/g, "�");
    if (value.indexOf("&oacute;")!=-1) value = value.replace(/\b&oacute;/g, "�");
    if (value.indexOf("&uacute;")!=-1) value = value.replace(/\b&uacute;/g, "�");
    if (value.indexOf("&Aacute;")!=-1) value = value.replace(/\b&Aacute;/g, "�");
    if (value.indexOf("&Eacute;")!=-1) value = value.replace(/\b&Eacute;/g, "�");
    if (value.indexOf("&Iacute;")!=-1) value = value.replace(/\b&Iacute;/g, "�");
    if (value.indexOf("&Oacute;")!=-1) value = value.replace(/\b&Oacute;/g, "�");
    if (value.indexOf("&Uacute;")!=-1) value = value.replace(/\b&Uacute;/g, "�");
    if (value.indexOf("&quot;")!=-1) value = value.replace(/\b&quot;/g, '"');
    if (value.indexOf("&euro;")!=-1) value = value.replace(/\b&euro;/g, '�');
    return value;
}

function keyDown(event, obj) {
    var k = (netscape) ? event.which : window.event.keyCode;
    if (k==13) {
        //alert("presiono enter");
        if (netscape) {
        	var obj = event.currentTarget.activeElement;
        	if (obj.type=="text") {
            	event.preventDefault();
            	obj.blur();
            } else if (obj.type!="textarea" && obj.type!="button" && obj.type!="submit") {
            	event.preventDefault();
            }
        } else {
        	if (window.event.srcElement.nodeName!="TEXTAREA" && window.event.srcElement.nodeName!="INPUT") {
                window.event.keyCode = 9;
            }
        }
    } else if (k==116) {
        //alert('F5');
        window.event.keyCode = 9;//window.event.consume();
        return false;
    }
}

document.onkeydown = keyDown;
if (netscape) document.captureEvents(Event.KEYDOWN|Event.KEYUP);

function roundNumber(number, decimals) {
    var newnumber;
    newnumber = Math.round(number*Math.pow(10,decimals))/Math.pow(10,decimals);
    return newnumber;
}

function exportarCVS(form, key, filter, table, scope) {
    var f = document.forms[form];
    f.action = 'genericTable.do';
    f.method.value = 'exportToCVS';
    if (key!=null) f.key.value = key;
    if (filter!=null) f.filter.value = filter;
    if (table!=null) f.table.value = table;
    if (scope!=null) f.scope.value = scope;
    f.submit();
}

// Inicio - DIV Mensaje y Barra de Progreso
var mInterval;
function startProgressBar() {
    reset();
    mInterval = setInterval("doProgress()",10);
}

function reset() {
    document.getElementById("mask").style.left = "0px";
    document.getElementById("mask").style.width = document.getElementById("mContainer").offsetWidth + "px";
    document.getElementById("progressIndicator").style.zIndex  = 10;
    document.getElementById("mask").style.display = "block";
    //document.getElementById("progressIndicator").innerHTML = "0%";
}

function doProgress() {
    curWidth = parseInt(document.getElementById("mask").offsetWidth);
    curLeft = parseInt(document.getElementById("mask").offsetLeft);
    curWidth --;
    curLeft ++;

    if(curLeft == 246) {
        reset();
        //clearInterval(mInterval);
        //document.getElementById("mask").style.display = "none";
        return;
    }

    document.getElementById("mask").style.left = curLeft + "px";
    if(parseInt(document.getElementById("mask").offsetWidth)>10)document.getElementById("mask").style.width = curWidth + "px";
    //document.getElementById("progressIndicator").innerHTML = Math.floor((curLeft / parseInt(document.getElementById("gradient").offsetWidth))*100) + "%";
}

var ns=(document.layers);
var ie=(document.all);
var w3=(document.getElementById && !ie);
var calunit=ns? "" : "px"

function showMessageWindow(messageDiv) {
    var div = document.getElementById(messageDiv).style;
    div.visibility="visible";
    if (ie) {
        documentWidth = truebody().offsetWidth/2+truebody().scrollLeft-20;
        documentHeight = truebody().offsetHeight/2+truebody().scrollTop-10;
    } else if (ns){
        documentWidth = window.innerWidth/2+window.pageXOffset-20;
        documentHeight = window.innerHeight/2+window.pageYOffset-10;
    } else if (w3) {
        documentWidth = self.innerWidth/2+window.pageXOffset-20;
        documentHeight = self.innerHeight/2+window.pageYOffset-10;
    }
    div.left = documentWidth-200+calunit;
    div.top = documentHeight-200+calunit;
    startProgressBar();
}

function truebody(){
    return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
// Fin - DIV Mensaje y Barra de Progreso


function getCursorPosition(field) {
    var iCaretPos = 0;
    if (document.selection) { // IE Support
        // Set focus on the element
        field.focus();
        // To get cursor position, get empty selection range
        var oSel = document.selection.createRange();
        // Move selection start to 0 position
        oSel.moveStart ('character', -field.value.length);
        // The caret position is selection length
        iCaretPos = oSel.text.length;
    } else if (field.selectionStart || field.selectionStart == '0') { // Firefox support
        iCaretPos = field.selectionStart;
    }
    // Return results
    return (iCaretPos);
}

function keyDownGrilla(e, field) {
    var oEvent = window.event ? window.event : e;
    var fila = field.name.split(',')[0];
    if (fila.indexOf(':')>0) fila = fila.split(':');
    var columna = field.name.split(',')[1];
    /*var tabla = '';
    if (field.name.split(',').length==3) {
        var tabla = ','+field.name.split(',')[2];
    }*/
    var nuevaFila = fila;
    /*var nuevaColumna = columna;
    if (oEvent.keyCode == 37) { // Tecla Izquierda
        if (getCursorPosition(field) == 0) {
            nuevaColumna = (parseInt(columna)-1);
            while ((parseInt(nuevaColumna) > 1 && (document.getElementById(nuevaFila+','+nuevaColumna+tabla).disabled ||
                   document.getElementById(nuevaFila+','+nuevaColumna+tabla).readOnly)) ||
                   (parseInt(nuevaColumna) > 1 && document.getElementById(nuevaFila+','+nuevaColumna+tabla).type=='hidden')) {
                nuevaColumna = (parseInt(nuevaColumna)-1);
            }
            if (nuevaColumna==0) {
                nuevaColumna = parseInt(columna);
            }
        }
    }
    if (oEvent.keyCode == 39) { // Tecla Derecha
        if (getCursorPosition(field) == field.value.length) {
            nuevaColumna = (parseInt(columna)+1);
            try {
                while ((document.getElementById(nuevaFila+','+nuevaColumna+tabla).disabled ||
                       document.getElementById(nuevaFila+','+nuevaColumna+tabla).readOnly) ||
                       document.getElementById(nuevaFila+','+nuevaColumna+tabla).type=='hidden') {
                    nuevaColumna = (parseInt(nuevaColumna)+1);
                }
            } catch(ex) {
                nuevaColumna = parseInt(columna);
            }
        }
    }*/
    if (oEvent.keyCode == 38) { // Tecla Arriba
        nuevaFila = (parseInt(fila)-1);
        try {
            while ((parseInt(nuevaFila) > 1 && (document.getElementById(nuevaFila+','+columna).disabled ||
                   document.getElementById(nuevaFila+','+columna).readOnly)) ||
                   (parseInt(nuevaFila) > 1 && document.getElementById(nuevaFila+','+columna).type=='hidden')) {
                nuevaFila = (parseInt(nuevaFila)-1);
            }
            if (nuevaFila==0) {
                nuevaFila = parseInt(fila);
            }
        } catch(ex) {
            nuevaFila = parseInt(fila);
        }
    }
    if (oEvent.keyCode == 40 || oEvent.keyCode == 13) { // Tecla Abajo
        nuevaFila = (parseInt(fila)+1);
        try {
            while (document.getElementById(nuevaFila+','+columna).disabled ||
                   document.getElementById(nuevaFila+','+columna).readOnly ||
                   document.getElementById(nuevaFila+','+columna).type=='hidden') {
                nuevaFila = (parseInt(nuevaFila)+1);
            }
        } catch(ex) {
            nuevaFila = parseInt(fila);
        }
    }
    try {
        document.getElementById(nuevaFila+','+columna).focus();
    } catch(ex) {
    }
    if (oEvent.keyCode == 13) window.event.returnValue = false;
}

function getSelectedRadio(buttonGroup) {
   // returns the array number of the selected radio button or -1 if no button is selected
   try {
       if (buttonGroup[0]) { // if the button group is an array (one button is not an array)
          for (var i=0; i<buttonGroup.length; i++) {
             if (buttonGroup[i].checked) {
                return i
             }
          }
       } else {
          if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
       }
   } catch(e) {
   }
   // if we get to this point, no radio button is selected
   return -1;
} // Ends the "getSelectedRadio" function

function getSelectedRadioValue(buttonGroup) {
   // returns the value of the selected radio button or "" if no button is selected
   var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
         return buttonGroup[i].value;
      } else { // The button group is just the one button, and it is checked
         return buttonGroup.value;
      }
   }
} // Ends the "getSelectedRadioValue" function

function getElementFormValue(f, name) {
    for (var i=0; i < f.elements.length; i++) {
        if (f.elements[i].name==name) {
            return f.elements[i].value;
        }
    }
}

function getCellValue(row, tableName, columnName) {
    return document.getElementById(row+","+tableName+"."+columnName).value;
}

function setCellValue(row, tableName, columnName, value) {
    document.getElementById(row+","+tableName+"."+columnName).value = value;
}

function getPageCode() {
    var pageCode = '';
    try {
        pageCode = document.getElementById('PAGE_CODE').value;
    } catch(e) {
    }
    return pageCode;
}

function validateCodification(name, tempName) {
    if (document.getElementById(tempName).value=="" || document.getElementById(name).value=="") {
        document.getElementById(tempName).value="";
        document.getElementById(name).selectedIndex = 0;
    }
}

function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
    return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
    return stringToTrim.replace(/\s+$/,"");
}

function getRowByKeyValues(f, keyValues) {
    var i=0;
    var row = -1;
    for (i=0; i < f.elements.length && row==-1; i++) {
        var name = f.elements[i].name;
        if (name!=null && name!="undefined" && name.indexOf("KEY")!=-1) {
            var split = name.split("|");
            var j=0;
            var ok = true;
            for (j=0; j < split.length; j++) {
                ok = ok && (split[j].indexOf(name)!=-1);
            }
            if (ok) {
                row = name.substring(0, 1);
            }
        }
    }
    return row;
}

function selectSelectedOption(select, value) {
    if (select!=null) {
        for (var i=0; i < select.options.length; i++) {
            if (select.options[i].value == value) {
                select.options[i].selected = true;
                break;
            } else if ((select.options[i].text ? select.options[i].text : select.options[i].textContent) == value) {
                select.options[i].selected = true;
                break;
            }
        }
    }
}

function selectSelectedOptionInDisabledSelect(id, value) {
	var newId = id.replace(".","_");
    selectSelectedOption(document.getElementById("select_"+newId), value);
}

function selectSelectedOptionInGrid(id, value) {
    selectSelectedOption(document.getElementById("select_"+id), value);
    if (document.getElementById("select_"+id)!=null && document.getElementById(id)!=null) {
        document.getElementById(id).value = value;
    }
}

function getSelectedOptionInGrid(id) {
    var select = document.getElementById("select_"+id);
    return select.options[select.selectedIndex].value;
}

function getSelectedOptionText(select) {
    var op = select.options[select.selectedIndex];
    if (op.value=="") return "";
    if (op.text) return op.text;
    else return op.textContent;
}

function getSelectValue(name) {
    var select = document.getElementById(name);
    return select.options[select.selectedIndex].value;
}

// For asociative arrays only
function arrayContainsKey(array, key) {
    for (var k in array) {
        if (k == key) return true;
    }
    return false;
}

function validaCambios(){
    $('input[type="text"],textarea').blur(function() {
    	if (this.value == this.defaultValue) {
    		if (this.getAttribute("dataFld")!="R") {
        	    $(this).removeClass("focusField").addClass("inputTextClass");
                contador--;
                this.setAttribute("dataFld", "R");
    		}
        } else {
    		if (this.getAttribute("dataFld")!="C") {
        	    $(this).removeClass("inputTextClass").addClass("focusField");
                contador++;
                this.setAttribute("dataFld", "C");
    		}
        }
    });
    $('select').blur(function() {
    	var defaultSelected = "";
    	$(this).find('option').each(function(i, opt) {
    	    if (opt.value!="" && opt.defaultSelected) {
    	        defaultSelected = opt.value;
    	    }
    	});
    	if (this.value == defaultSelected) {
    		if (this.getAttribute("dataFld")!="R") {
	            $(this).removeClass("focusField").addClass("inputTextClass");
	            contador--;
                this.setAttribute("dataFld", "R");
    		}
        } else {
    		if (this.getAttribute("dataFld")!="C") {
	        	$(this).removeClass("inputTextClass").addClass("focusField");
	            contador++;
                this.setAttribute("dataFld", "C");
    		}
        }
    });
}


function cerrarPopUp() {
    var f = document.formulario;
    f.button.value="cerrarPopUpButton";
}

function cerrarPopUpFirma() {
    var f = document.formulario;
    f.button.value="cerrarPopUpButtonFirma";
}

function validaCerraPopUp(){
    if (contador == undefined) {
        window.parent.hidePopWin(false);
        window.parent.actualizar();
        return false;
    }
    if (contador > 0) {
        if (confirm("Existe información actualizada que aún no ha sido guardada. Desea cerrar sin guardar los cambios?")) {
            window.parent.hidePopWin(false);
            window.parent.actualizar();
        }
        return false;
    } else {
        window.parent.hidePopWin(false);
        window.parent.actualizar();
        return false;
    }
    return true;
}

function validaCerraPopUpNoAction(){
	window.parent.hidePopWin(false);
    return false;
}

function validaCambioTab() {
	if (contador != undefined && contador > 0) {
        alert("Existe información actualizada que aún no ha sido guardada. Guárdela antes de cambiar de pestaña");
        //alert(contador);
        return false;
    }
	return true;
}

function bloquearControles(){
    $('input[type="text"],textarea').attr('readonly',true).addClass("readonlyInputTextClass");
    $('input[type="radio"],input[type="checkbox"],select').attr('disabled',true);
    $("#rolActivo").attr('disabled',false).removeClass("readonlyInputTextClass").addClass("inputTextClass");
}

function tituloPopUp(titulo){
    $("#popupTitle",window.parent.document).html(titulo);
    $("#popupTitle",window.parent.document).addClass("headerOptionClassModalWindow");
}

//Funcion que valida la cantidad de caracteres ingresados en un TEXTAREA o TEXT
function valida_longitud(obj, caracteres){
    var num_caracteres_permitidos = caracteres;
       num_caracteres = obj.value.length;
       mensaje = obj.value;
           if (num_caracteres  > num_caracteres_permitidos) {
        mensaje = mensaje.substring(0, num_caracteres_permitidos);
        alert("Ingresar m�ximo " + num_caracteres_permitidos + " caracteres.");
        obj.value = mensaje;
    }
}

//Funci�n que indica si un determinado tag esta oculto o no existe en el jsp activo
function tagOculto(tagName){
	var res = false;
	var nTag = "#" + tagName;
	if (document.getElementById(tagName) == undefined) {
		res = true;
	} else if ($(nTag).attr("style").toLowerCase().replace(/\s/g,'').indexOf('display:none') > -1 ){
		res = true;
	}
	return res;
}

//Funcion que registra en el cookie de la aplicacion el indice del tab seleccionado
function seleccionarTab(indice){
	//reemplazarPrmtPaginacionJLIS('seleccionado', indice);
	//document.getElementById('indiceTab').value=indice;
	setCookie('indiceTab', indice);
}

function js_ContieneCaracter(control, etiqueta, mensajeError) {
	if (control != undefined){
		if (!/[a-zA-Z]+/.test(control.value)){
			changeStyleClass(etiqueta, "errorValueClass");
		    alert(mensajeError);
		    return false;
		}
	}

	return true;
}

function js_ContieneNumero(control, etiqueta, mensajeError) {
	if (control != undefined){
		if (!/[0-9]+/.test(control.value)){
			changeStyleClass(etiqueta, "errorValueClass");
		    alert(mensajeError);
		    return false;
		}
	}

	return true;
}

function js_ContieneAlfaNumerico(control, etiqueta, mensajeError) {
	if (control != undefined){
		if (!/\w+/.test(control.value)){
			changeStyleClass(etiqueta, "errorValueClass");
		    alert(mensajeError);
		    return false;
		}
	}

	return true;
}


function isVisible(el){
    // returns true iff el and all its ancestors are visible        
    return el.style.display !== 'none' && el.style.visibility !== 'hidden'
    && (el.parentElement? isVisible(el.parentElement): true)
};