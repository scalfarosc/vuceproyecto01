
function setFocus(obj) {
    try {
        //setTimeout('document.getElementById(\"'+obj.name+'\").focus();',50);
        document.getElementById(obj.name).focus();
    } catch(e) {
    }
}

function validarCampoObligatorioLabel(idCampo, idLabel) {
    var obj = document.getElementById(idCampo);
    try {
        var nombreCampo = "";
        if (idLabel!="" && document.getElementById(idLabel)!=null) {
            var nombreCampo = new String(document.getElementById(idLabel).innerHTML);
            if (nombreCampo.indexOf(':') == nombreCampo.length-1) {
                nombreCampo = nombreCampo.substring(0,nombreCampo.length-1);
            }
        }
        if (obj.value=='') {
            obj.style.background = '#FFFF00';
            changeStyleClass(idLabel, "errorValueClass");
            if (nombreCampo!="") {
                alert('El campo "'+nombreCampo+'" no puede estar vac�o.');
            } else {
                alert('El campo no puede estar vac�o.');
            }
            setFocus(obj);
            obj.value='';
            return false;
        }
        changeStyleClass(idLabel, "labelClass");
    } catch(e) {
    }
    return true;
}

function validarObligatorio(obj,obligatorio) {
	if (obligatorio) {
        if (obj.value=='') {
        	obj.className = 'alertField';
            alert('El campo no puede estar vac�o.');
            setFocus(obj);
            obj.value='';
            return false;
        }
    }
	obj.className = 'focusField';
    return obj.value!='';
}

function validarDouble(obj,obligatorio,msj) {
    if (validarObligatorio(obj,obligatorio)) {
        if (!js_dec_posneg(obj.value)) {
        	obj.className = 'alertField';
            if (msj==null) msj = 'Debe ingresar un numero con decimales.';
            alert(msj);
            setFocus(obj);
            if (obligatorio) obj.value=0.0; else obj.value="";
            obj.select();
            return false;
        }
    }
    obj.className = 'focusField';
    return true;
}

function validarDoublePositivo(obj,obligatorio,msj) {
    if (validarObligatorio(obj,obligatorio)) {
        if (!js_dec_pos(obj.value)) {
        	obj.className = 'alertField';
            if (msj==null) msj = 'Debe ingresar un numero con decimales, positivo.';
            alert(msj);
            setFocus(obj);
            if (obligatorio) obj.value=0.0; else obj.value="";
            obj.select();
            return false;
        }
    }
    obj.className = 'focusField';
    return true;
}

function validarDoublePositivoEntDec(obj,obligatorio,cantEnt,cantDec,msj) {
    if (validarObligatorio(obj,obligatorio)) {
    	if (!js_dec_pos_ent_dec(obj.value,cantEnt,cantDec)) {
        	obj.className = 'alertField';
            if (msj==null) msj = 'Debe ingresar un numero con '+cantEnt+' enteros y '+cantDec+' decimales como maximo, positivo.';
            alert(msj);
            setFocus(obj);
            if (obligatorio) obj.value=0.0; else obj.value="";
            obj.select();
            return false;
        }
    }
    obj.className = 'focusField';
    return true;
}

function validarDoubleNegativoEntDec(obj,obligatorio,cantEnt,cantDec,msj) {
    if (validarObligatorio(obj,obligatorio)) {
    	if (!js_dec_neg_ent_dec(obj.value,cantEnt,cantDec)) {
        	obj.className = 'alertField';
            if (msj==null) msj = 'Debe ingresar un numero con '+cantEnt+' enteros y '+cantDec+' decimales como maximo, positivo o negativo.';
            alert(msj);
            setFocus(obj);
            if (obligatorio) obj.value=0.0; else obj.value="";
            obj.select();
            return false;
        }
    }
    obj.className = 'focusField';
    return true;
}

function validarEntero(obj,obligatorio,msj) {
    if (validarObligatorio(obj,obligatorio)) {
        if (!js_int_posneg(obj.value)) {
        	obj.className = 'alertField';
            if (msj==null) msj = 'Debe ingresar un numero entero.';
            alert(msj);
            setFocus(obj);
            if (obligatorio) obj.value=0; else obj.value="";
            obj.select();
            return false;
        }
    }
    obj.className = 'focusField';
    return true;
}

function validarEnteroPositivo(obj,obligatorio,msj) {
    if (validarObligatorio(obj,obligatorio)) {
        if (!js_int_pos(obj.value)) {
            obj.className = 'alertField';
            if (msj==null) msj = 'Debe ingresar un numero entero positivo.';
            alert(msj);
            setFocus(obj);
            if (obligatorio) obj.value=0; else obj.value="";
            obj.select();
            return false;
        }
    }
    
    obj.className = 'focusField';
    return true;
}

function validarFechaHora(obj,obligatorio) {
    if (obj.value!='' && validarObligatorio(obj,obligatorio)) {
        var fecha = obj.value.split(' ')[0];
        var hora = obj.value.split(' ')[1];
        if (!js_consis_hour(hora)) {
            obj.style.background = '#FFFF00';
            alert('Debe ingresar una hora v�lida.');
            setFocus(obj);
            obj.value='';
            return false;
        }
        if (!js_date(fecha)) {
            obj.style.background = '#FFFF00';
            alert('Debe ingresar una fecha v�lida.');
            setFocus(obj);
            obj.value='';
            return false;
        } else {
            var strfecha = fecha;
            var indice = strfecha.indexOf('-');
            if (indice==-1) indice = strfecha.indexOf('/');
            if (indice==-1) indice = strfecha.indexOf('.');
            var strSeparator = strfecha.substring(indice,indice+1); //find date separator
            
            var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
            if (arrayDate[0].length==1) arrayDate[0] = '0'+arrayDate[0];
            if (arrayDate[1].length==1) arrayDate[1] = '0'+arrayDate[1];
            if (arrayDate[2].length==2) arrayDate[2] = '20'+arrayDate[2];
            else if (arrayDate[2].length==1) arrayDate[2] = '200'+arrayDate[2];
            fecha = arrayDate[0]+'/'+arrayDate[1]+'/'+arrayDate[2];
        }
        obj.value = fecha+' '+hora;
    }
    obj.style.background = '#FFFFFF';
    return true;
}

function validarFecha(obj,obligatorio) {
    if (obj.value!='' && validarObligatorio(obj,obligatorio)) {
        if (!js_date(obj.value)) {
            obj.style.background = '#FFFF00';
            alert('Debe ingresar una fecha v�lida.');
            setFocus(obj);
            obj.value='';
            return false;
        } else {
            var strfecha = obj.value;
            var indice = strfecha.indexOf('-');
            if (indice==-1) indice = strfecha.indexOf('/');
            if (indice==-1) indice = strfecha.indexOf('.');
            var strSeparator = strfecha.substring(indice,indice+1); //find date separator
            
            var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
            if (arrayDate[0].length==1) arrayDate[0] = '0'+arrayDate[0];
            if (arrayDate[1].length==1) arrayDate[1] = '0'+arrayDate[1];
            if (arrayDate[2].length==2) arrayDate[2] = '20'+arrayDate[2];
            else if (arrayDate[2].length==1) arrayDate[2] = '200'+arrayDate[2];
            obj.value = arrayDate[0]+'/'+arrayDate[1]+'/'+arrayDate[2];
        }
    }
    obj.style.background = '#FFFFFF';
    return true;
}

function validarHora(obj,obligatorio) {
    if (validarObligatorio(obj,obligatorio)) {
        if (!js_consis_hour(obj.value)) {
            obj.style.background = '#FFFF00';
            setFocus(obj);
            obj.value='';
            return false;
        }
    }
    obj.style.background = '#FFFFFF';
    return true;
}

function validarMesAnio(obj,obligatorio) {
    if (obj.value!='' && validarObligatorio(obj,obligatorio)) {
        if (!js_monthYear(obj.value)) {
            obj.style.background = '#FFFF00';
            alert('Debe ingresar un mes y a�o v�lidos.');
            setFocus(obj);
            obj.value='';
            return false;
        } else {
            var strfecha = obj.value;
            var indice = strfecha.indexOf('-');
            if (indice==-1) indice = strfecha.indexOf('/');
            if (indice==-1) indice = strfecha.indexOf('.');
            var strSeparator = strfecha.substring(indice,indice+1); //find date separator
            
            var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
            if (arrayDate[0].length==1) arrayDate[0] = '0'+arrayDate[0];
            else if (arrayDate[1].length==1) arrayDate[1] = '200'+arrayDate[1];
            obj.value = arrayDate[0]+'/'+arrayDate[1];
        }
    }
    obj.style.background = '#FFFFFF';
    return true;
}

function validarCampoObligatorio(obj) {
    if (obj.value=='') {
        obj.style.background = '#FFFF00';
        alert('El campo no puede estar vac�o.');
        setFocus(obj);
        return false;
    }
    return true;
}

function validarEmail(obj,obligatorio) {
    if (validarObligatorio(obj,obligatorio)) {
        if (!js_email2(obj.value)) {
            obj.style.background = '#FFFF00';
            //alert('Debe ingresar una direcci�n de correo electr�nico.');
            setFocus(obj);
            if (obligatorio) obj.value='';
            obj.select();
            return;
        }
    }
    obj.style.background = '#FFFFFF';
}

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date2_ms - date1_ms);
    
    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY);

}

function validarFechasDiferencia(obj1, obj2, obligatorio,maximo,unidad) {
	var fecha1 = null;
	var fecha2 = null;
	
	// Validamos la fecha 1
    if (obj1.value!='' && validarObligatorio(obj1,obligatorio)) {
        if (!js_date(obj1.value)) {
            obj1.style.background = '#FFFF00';
            alert('Debe ingresar una fecha valida.');
            setFocus(obj1);
            obj1.value='';
            return false;
        } else {
            var strfecha = obj1.value;
            var indice = strfecha.indexOf('-');
            if (indice==-1) indice = strfecha.indexOf('/');
            if (indice==-1) indice = strfecha.indexOf('.');
            var strSeparator = strfecha.substring(indice,indice+1); //find date separator
            
            var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
            if (arrayDate[0].length==1) arrayDate[0] = '0'+arrayDate[0];
            if (arrayDate[1].length==1) arrayDate[1] = '0'+arrayDate[1];
            if (arrayDate[2].length==2) arrayDate[2] = '20'+arrayDate[2];
            else if (arrayDate[2].length==1) arrayDate[2] = '200'+arrayDate[2];
            
            obj1.value = arrayDate[0]+'/'+arrayDate[1]+'/'+arrayDate[2];
            
            fecha1 = new Date();
            fecha1.setYear(arrayDate[2] + 1);
            fecha1.setMonth(arrayDate[1]-1);
            fecha1.setDate(arrayDate[0]);
        }
    } else if (obj1.value=='') {
    	return true;
    }
    obj1.style.background = '#FFFFFF';
    
    // Validamos la fecha 2
    if (obj2.value!='' && validarObligatorio(obj2,obligatorio)) {
        if (!js_date(obj2.value)) {
        	obj2.style.background = '#FFFF00';
            alert('Debe ingresar una fecha v�lida.');
            setFocus(obj2);
            obj2.value='';
            return false;
        } else {
            var strfecha = obj2.value;
            var indice = strfecha.indexOf('-');
            if (indice==-1) indice = strfecha.indexOf('/');
            if (indice==-1) indice = strfecha.indexOf('.');
            var strSeparator = strfecha.substring(indice,indice+1); //find date separator
            
            var arrayDate = strfecha.split(strSeparator); //split date into month, day, year
            if (arrayDate[0].length==1) arrayDate[0] = '0'+arrayDate[0];
            if (arrayDate[1].length==1) arrayDate[1] = '0'+arrayDate[1];
            if (arrayDate[2].length==2) arrayDate[2] = '20'+arrayDate[2];
            else if (arrayDate[2].length==1) arrayDate[2] = '200'+arrayDate[2];
            
            obj2.value = arrayDate[0]+'/'+arrayDate[1]+'/'+arrayDate[2];
            
            fecha2 = new Date();
            fecha2.setYear(arrayDate[2] + 1);
            fecha2.setMonth(arrayDate[1]-1);
            fecha2.setDate(arrayDate[0]);
        }
    } else if (obj2.value=='') {
    	return true;
    }
    obj2.style.background = '#FFFFFF';
    
    // Ahora validamos que la diferencia de fechas sea menor o igual al maximo
    
    switch(unidad) {
        case 'D':
    	    if (days_between(fecha1, fecha2) > maximo) {
    	        alert("El rango de fechas debe ser como m�ximo de "+maximo+" d�as.");
    	        return false;
            }
    	    break;
        case 'M':
    	    if (Math.round((days_between(fecha1, fecha2))/30) > maximo) {
    	        alert("El rango de fechas debe ser como m�ximo de "+maximo+" meses.");
    	        return false;
            }
    	    break;
        case 'ME':
    	    if (((days_between(fecha1, fecha2))/30) > maximo) {
    	        alert("El rango de fechas debe ser como m�ximo de "+maximo+" meses.");
    	        return false;
            }
    	    break;
    	default:
    		alert("Error en la definición de la regla.");
    	    return false;
    }
    return true;
}

