
// Valores de radio buttons
var contadorRadioUno = 0;
var contadorRadioDos = 0;
var contadorRadioTres = 0;

var msgCriterio1 = '';
var msgCriterio2 = '';

var alertaOptExportadorConValidacion = "Ud. Deber� registrar la informacion del productor. El Productor deber� validar la informaci�n contenida en la DJ";
var alertaOptExportadorSinValidacion = "Ud. Deber� registrar la informacion del productor.";

function changeCriterio(item) {

	//var controlRadio = document.getElementById("DJ.criterioQueCumple");
	//var contador = 0;

	if (item.value == '1') {
		$("#DJ\\.cumpleTotalmenteObtenido").val("S");
		$("#DJ\\.cumpleCriterioCambioClasif").val("N");
		$("#DJ\\.cumpleOtroCriterio").val("N");
		//alert(contadorRadioUno);
		cargarCriterioOrigen(item.value, contadorRadioUno);
		/*if (contadorRadioUno > 0) {
    		$("#DJ\\.cumpleTotalmenteObtenido").val("N");
    		$("#DJ\\.cumpleCriterioCambioClasif").val("N");
    		$("#DJ\\.cumpleOtroCriterio").val("N");
			item.checked = false;
			contadorRadioUno = 0;
		} else {*/
			if (msgCriterio1 != '') {
				alert(msgCriterio1);
			}
			contadorRadioUno++;
		//}
		//contador = contadorRadioUno;
	} else if (item.value == '2') {
		$("#DJ\\.cumpleTotalmenteObtenido").val("N");
		$("#DJ\\.cumpleCriterioCambioClasif").val("S");
		$("#DJ\\.cumpleOtroCriterio").val("N");
		//alert(contadorRadioDos);
		cargarCriterioOrigen(item.value, contadorRadioDos);
		/*if (contadorRadioDos > 0) {
    		$("#DJ\\.cumpleTotalmenteObtenido").val("N");
    		$("#DJ\\.cumpleCriterioCambioClasif").val("N");
    		$("#DJ\\.cumpleOtroCriterio").val("N");
			item.checked = false;
			contadorRadioDos = 0;
		} else {*/
			if (msgCriterio2 != '') {
				alert(msgCriterio2);
			}

			contadorRadioDos++;
		//}
		//contador = contadorRadioDos;
	} else if (item.value == '3') {
		$("#DJ\\.cumpleTotalmenteObtenido").val("N");
		$("#DJ\\.cumpleCriterioCambioClasif").val("N");
		$("#DJ\\.cumpleOtroCriterio").val("S");
		cargarCriterioOrigen(item.value, contadorRadioTres);
		//alert(contadorRadioTres);
		/*if (contadorRadioTres > 0) {
    		$("#DJ\\.cumpleTotalmenteObtenido").val("N");
    		$("#DJ\\.cumpleCriterioCambioClasif").val("N");
    		$("#DJ\\.cumpleOtroCriterio").val("N");
			item.checked = false;
			contadorRadioTres = 0;
		} else {*/
			contadorRadioTres++;
		//}
		//contador = contadorRadioTres;
	}

	/*if (contador == 0) {
		mostrarTagMercancia(false);
	} else {
		mostrarTagMercancia($("#DJ\\.cumpleTotalmenteObtenido").val() == 'S');
	}*/

}

// Limpia los contadores al hacer click en otr
function limpiaContadores(item){

	if (item.value == '1') { // Si hemos cambiado al uno, el contador de dos se debe anular
		contadorRadioDos = 0;
		contadorRadioTres = 0;
	} else if (item.value == '2') { // Si hemos elegido el dos, los demas se deben anular
		contadorRadioUno = 0;
		contadorRadioTres = 0;
	} else if (item.value == '3') { // Si hemos elegido el tres, los dem�s se deben anular
		contadorRadioUno = 0;
		contadorRadioDos = 0;
	}

}

// Auto completa con ceros un control
function autoComplete(control, num){
	if (control.value != undefined) {
		if (control.value.length < num){
			for(var i = control.value.length; i < num; i++){
				control.value = "0" + control.value;
			}
		}
	}
}

// Valida que la Naladisa sea de 8 d�gitos
function validarNaladisa(control) {
	if ("" != control.value) {
		if (!validarEnteroPositivo(control,false, "El dato Naladisa debe ser un n�mero entero positivo de " + (NUM_DIGITOS_PARTIDA != undefined? NUM_DIGITOS_PARTIDA: "8" )+ " d�gitos")) {
  			obj.value = "";
  		} else {
  			if (NUM_DIGITOS_PARTIDA != undefined) {
  				autoComplete(control, NUM_DIGITOS_PARTIDA);
  			} else {
  				autoComplete(control, 8);
  			}
		}
	}
}