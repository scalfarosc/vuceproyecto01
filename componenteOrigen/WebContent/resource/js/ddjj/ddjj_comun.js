
// Valores de radio buttons
var contadorRadioUno = 0;
var contadorRadioDos = 0;
var contadorRadioTres = 0;

var msgCriterio1 = '';
var msgCriterio2 = '';

var alertaOptExportadorConValidacion = "Ud. Deber� registrar la informaci�n del productor. El Productor deber� validar la informaci�n contenida en la DJ";
var alertaOptExportadorSinValidacion = "Ud. Deber� registrar la informaci�n del productor.";

function changeCriterio(item) {
	if (item.value == '1') {
		$("#CALIFICACION\\.cumpleTotalmenteObtenido").val("S");
		$("#CALIFICACION\\.cumpleCriterioCambioClasif").val("N");
		$("#CALIFICACION\\.cumpleOtroCriterio").val("N");
		cargarCombosDefecto();
		if (msgCriterio1 != '') {
			alert(msgCriterio1);
		}
	} else if (item.value == '2') {
		$("#CALIFICACION\\.cumpleTotalmenteObtenido").val("N");
		$("#CALIFICACION\\.cumpleCriterioCambioClasif").val("S");
		$("#CALIFICACION\\.cumpleOtroCriterio").val("N");
		cargarCombosDefecto();
		if (msgCriterio2 != '') {
			alert(msgCriterio2);
		}
	} else if (item.value == '3') {
		$("#CALIFICACION\\.cumpleTotalmenteObtenido").val("N");
		$("#CALIFICACION\\.cumpleCriterioCambioClasif").val("N");
		$("#CALIFICACION\\.cumpleOtroCriterio").val("S");
		cargarCombosDefecto();
	}
	// flagCriterio = 2 : Proviene del cambio de los combos
	document.getElementById("flagCriterio").value = 2;

}

function cargarCombosDefecto(){
	if ($("#trNorma").attr("style") == 'display:' || $("#trNorma").attr("style") == '' && $("#CALIFICACION\\.secuenciaNorma").attr("options").length == 2 && $("#CALIFICACION\\.secuenciaNorma").attr("options")[0].selected) {
		$("#CALIFICACION\\.secuenciaNorma").attr("options")[1].selected = true;
	}
	cargarOrigenSecuencia();
}

// Limpia los contadores al hacer click en otr
function limpiaContadores(item){

	if (item.value == '1') { // Si hemos cambiado al uno, el contador de dos se debe anular
		contadorRadioDos = 0;
		contadorRadioTres = 0;
	} else if (item.value == '2') { // Si hemos elegido el dos, los demas se deben anular
		contadorRadioUno = 0;
		contadorRadioTres = 0;
	} else if (item.value == '3') { // Si hemos elegido el tres, los dem�s se deben anular
		contadorRadioUno = 0;
		contadorRadioDos = 0;
	}

}

// Auto completa con ceros un control
function autoComplete(control, num){
	if (control.value != undefined) {
		if (control.value.length < num){
			for(var i = control.value.length; i < num; i++){
				control.value = "0" + control.value;
			}
		}
	}
}

// Valida que la Naladisa sea de 8 d�gitos
function validarNaladisa(control) {
	if ("" != control.value) {
		if (!validarEnteroPositivo(control,false, "El dato Naladisa debe ser un n�mero entero positivo de " + (NUM_DIGITOS_PARTIDA != undefined? NUM_DIGITOS_PARTIDA: "8" )+ " d�gitos")) {
  			obj.value = "";
  		} else {
  			if (NUM_DIGITOS_PARTIDA != undefined) {
  				autoComplete(control, NUM_DIGITOS_PARTIDA);
  			} else {
  				autoComplete(control, 8);
  			}
		}
	}
}

function determinarTipoRol(){
	var rol = '0';

	if ( $("#CALIFICACION\\.tipoRolCalificacion_1") != undefined ) {
		if ($("#CALIFICACION\\.tipoRolCalificacion_1").attr("checked")) {
			rol = '1';
		}
	}

	if ( $("#CALIFICACION\\.tipoRolCalificacion_2") != undefined ) {
		if ($("#CALIFICACION\\.tipoRolCalificacion_2").attr("checked")) {
			rol = '2';
		}
	}

	if ( $("#CALIFICACION\\.tipoRolCalificacion_3") != undefined ) {
		if ($("#CALIFICACION\\.tipoRolCalificacion_3").attr("checked")) {
			rol = '3';
		}
	}

	if ( $("#CALIFICACION\\.tipoRolCalificacion_4") != undefined ) {
		if ($("#CALIFICACION\\.tipoRolCalificacion_4").attr("checked")) {
			rol = '4';
		}
	}

	if ( $("#CALIFICACION\\.tipoRolCalificacion_5") != undefined ) {
		if ($("#CALIFICACION\\.tipoRolCalificacion_5").attr("checked")) {
			rol = '5';
		}
	}

	if ( $("#CALIFICACION\\.tipoRolCalificacion_6") != undefined ) {
		if ($("#CALIFICACION\\.tipoRolCalificacion_6").attr("checked")) {
			rol = '6';
		}
	}

	return rol;

}

function mostrarNotaProductores(){

	var tipoRol = determinarTipoRol();
	if (tipoRol == '1' || tipoRol == '5' || tipoRol == '6' ) {
		$("#tMantProductorPie").attr("style", "display:");
	}

}
