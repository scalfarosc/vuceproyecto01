
// Autocompleta la subpartida areancelaria
function autocompletarPartida(control) {
	if ("" != control.value) {
		if (control.value.length < 10){
  			autoComplete(control, 10);
  		}
	}
}

//Auto completa con ceros un control
function autoComplete(control, num){
	if (control.value != undefined) {
		if (control.value.length < num){
			for(var i = control.value.length; i < num; i++){
				control.value = "0" + control.value;
			}
		}
	}
}

function valida_longitud_naladisa_digitos(obj,validarLongitud,digitos) {
	if (validarEnteroPositivo(obj,false,"El c�digo s�lo debe incluir n�meros")) {
		if (validarLongitud && obj.value.length!=digitos) {
		    alert("El c�digo debe tener "+digitos+" d�gitos num�ricos");
		    obj.value = "";
		}
	}
}
